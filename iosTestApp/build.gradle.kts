/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */

plugins {
    kotlin("jvm")
    id("robovm")
}


dependencies {
    api(project(":common"))
    api(project(":ios"))
    listOf(
        ThreetenBp,
        Rxjava3,
        Gson,
        Ktorm,
        KtormSqlite,
        //Rxjava3Kotlin,
        Kotlin,
        KotlinReflect,
        Retrofit,
        RetrofitGson,
        RetrofitRxJava3,
        XLayout,
        XLayoutIos,
        Robovm,
        RobovmCocoaTouch,
        RobopodsMapbox
    ).forEach { implementation(it()) }

}



tasks.register("run64") {
    doLast {
        project.setProperty("robovm.arch", "arm64")
        project.setProperty("robovm.iosSignIdentity", LocalProperties["develSignIdentity"])
        project.setProperty("robovm.iosProvisioningProfile", LocalProperties["develProvisioningProfile"])
        //println("profile = $develProvisioningProfile, signing=$develSignIdentity")
    }
    finalizedBy("launchIOSDevice")
}
tasks.register("run32") {
    doLast {
        project.setProperty("robovm.arch", "thumbv7")
        project.setProperty("robovm.iosSignIdentity", LocalProperties["develSignIdentity"])
        project.setProperty("robovm.iosProvisioningProfile", LocalProperties["develProvisioningProfile"])
    }
    finalizedBy("launchIOSDevice")
}

tasks.register("runiPad") {
    doLast {
        project.setProperty("robovm.device.name", "iPad Pro (11-inch)")
    }
    finalizedBy("launchIPadSimulator")
}

tasks.register("ipa") {
    val profile:String = LocalProperties["releaseProvisioningProfile"]
    doLast {
        project.setProperty("robovm.iosSignIdentity", LocalProperties["releaseSignIdentity"])
        project.setProperty("robovm.iosProvisioningProfile", profile)
        println("using provisioning profile $profile")
    }
    finalizedBy("createIPA")
}


val robovmProperties = PropertiesFile(File(projectDir, "robovm.properties"))

tasks.register("updateBuildNumber") {
    doLast {
        robovmProperties.set(
            mapOf(
                "app.build" to project.buildNumber
                "app.version" to project.appVersion
            )
        )
    }
}

tasks.named("createIPA") {
    dependsOn("updateBuildNumber")
}


val cjAppStoreApiKey: String = LocalProperties["cjAppStoreApiKey"]
val cjAppStoreIssuerId: String = LocalProperties["cjAppStoreIssuerId"]
val appName: String = robovmProperties["app.name"]

tasks.register<Exec>("verifyIPA") {
    dependsOn("ipa")
    executable("xcrun")
    args(
        "altool", "-v", "--apiKey", cjAppStoreApiKey, "--apiIssuer",
        cjAppStoreIssuerId, "-f", "${buildDir}/robovm/${appName}.ipa"
    )
}

val roboDir = "$buildDir/robovm"
val archiveDir = "$projectDir/archives"
val ipa = "${appName}.ipa"
val dsym = "${appName}.app.dSYM"

tasks.register<Exec>("archiveDsym") {
    workingDir(roboDir)
    executable("zip")
    args("-r", "--no-dir-entries", "$archiveDir/${AppVersions.buildNumber}.zip", ipa, dsym)
}

// publish the IPA to the App Store
// also archive the ipa and dSYM for later use.

tasks.register<Exec>("publishIPA") {
    dependsOn("verifyIPA")
    finalizedBy("archiveDsym")
    executable("xcrun")
    args(
        "altool", "--upload-app", "--apiKey", cjAppStoreApiKey, "--apiIssuer",
        cjAppStoreIssuerId, "-f", "$roboDir/$ipa"
    )
}
