package com.controlj.testapp.view

import com.controlj.ble.BleDevice
import com.controlj.ble.BleScanResult
import com.controlj.ble.BleScanner
import com.controlj.rx.observeBy
import com.controlj.ui.UiAction
import com.controlj.view.BluetoothListPresenter
import com.controlj.view.MenuEntry
import io.reactivex.rxjava3.core.Single

class CJBluetoothPresenter : BluetoothListPresenter() {
    override fun isOneOfOurs(scanResult: BleScanResult): Boolean {
        return scanResult.type != BleDevice.Type.UNKNOWN
    }

    override fun onItemSelected(item: BleScanResult?) {
        item?.let {
            showMenu(
                "Bluetooth Device", listOf(
                    MenuEntry("Connect", { doConnect(it) })
                ), it
            )
        }
    }

    private fun doConnect(item: BleScanResult) {
        BleScanner().getDevice(item)?.apply {
            if (isConnected) {
                UiAction.toast("Device already connected")
                return
            }
            getConnector(false)
                .flatMap { discoverServices }
                .flatMap { services ->
                    Single.concat(
                        services.map { service ->
                            service.characteristics.map { discoverDescriptors(it) }
                        }.flatten()
                    ).toList()
                        .map { services }
                }
                .flatMap { services ->
                    disconnector.onErrorReturnItem(this).map { services }
                }
                .observeBy { services ->
                    services.forEach { service ->
                        println("Service: ${service.uuid}")
                        service.characteristics.forEach { characteristic ->
                            println("    Characteristic: ${characteristic.uuid}")
                            characteristic.descriptors?.forEach { descriptor ->
                                println("        Descriptor: ${descriptor}")
                            }
                        }
                    }
                    UiAction.toast("Complete")
                }
        }
    }

    override fun textValues(item: BleScanResult): List<String> {
        val result = super.textValues(item).toMutableList()
        result[DEV_ADDRESS] = "${item.type.name}: ${result[DEV_ADDRESS]}"
        return result
    }
}
