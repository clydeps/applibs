/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */

package com.controlj.testapp.view


import com.controlj.data.Location
import com.controlj.framework.ApplicationBusy
import com.controlj.framework.ApplicationState
import com.controlj.framework.IosBackgroundServiceProvider.isIos13
import com.controlj.graphics.CRect
import com.controlj.ios.view.IosForegroundServiceProvider
import com.controlj.ios.view.MapboxController
import com.controlj.ios.view.SlideViewController
import com.controlj.location.LocationProvider
import com.controlj.location.LocationSource
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.observeOnMainBy
import com.controlj.settings.DataStore
import com.controlj.stratux.Stratux
import com.controlj.testapp.app.MainMenu
import com.controlj.traffic.TrafficSource
import com.controlj.ui.UiAction
import com.controlj.ui.ViewModelStore
import com.controlj.view.ForegroundServiceProvider
import com.controlj.view.AppPermission
import com.controlj.view.RadarMapViewModel
import com.controlj.view.SlideViewPresenter
import com.controlj.view.ViewController
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import org.robovm.apple.coregraphics.CGAffineTransform
import org.robovm.apple.coregraphics.CGPoint
import org.robovm.apple.uikit.UIActivityIndicatorView
import org.robovm.apple.uikit.UIActivityIndicatorViewStyle
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIView
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 23/4/18.
 */
class PageController() :
    SlideViewController(SlideViewPresenter.Edges.ShortEdges), ViewController {

    class MapModel : RadarMapViewModel() {
        override fun onLocation(value: Location) {
            if (!ApplicationState.current.data.visible)
                super.onLocation(value)
        }
    }

    override var bounds: CRect
        get() = CRect(0.0, 0.0, bounds.width, bounds.height)
        set(_) {}

    override fun redrawForeground(rect: CRect) {
        view.setNeedsDisplay()
    }

    override fun requestRedraw(rect: CRect) {
        view.setNeedsDisplay()
    }

    private val mapViewModel: RadarMapViewModel = ViewModelStore.loadViewModel(MapModel::class.java)
    private val fgsp: ForegroundServiceProvider = IosForegroundServiceProvider(this)
    private val mapView = MapboxController()
    private var disposables = CompositeDisposable()
    private var busyDisposable = Disposable.disposed()
    override val wantFullScreen: Boolean
        get() = false
    private val mapViewShowing: Boolean
        get() = presenter.stateStack.isEmpty()

    @Suppress("DEPRECATION")
    private val busyIndicator = UIActivityIndicatorView(
        if (isIos13)
            UIActivityIndicatorViewStyle.Large
        else
            UIActivityIndicatorViewStyle.WhiteLarge
    ).also {
        it.color = UIColor.blue()
        it.backgroundColor = UIColor.fromWhiteAlpha(1.0, 0.5)
        it.transform = CGAffineTransform.createScale(1.5, 1.5)
    }

    private val navView: UIView = UIView().apply {
        backgroundColor = UIColor.green()
    }

    private val soarView = UIView().apply {
        backgroundColor = UIColor.yellow()
    }

    private fun onStart() {
        setBusy(false)
        disposables.add(ApplicationBusy.listener.observeOnMainBy { setBusy(it) })
        disposables.add(UiAction.observer.observeOnMainBy(::logException) { processAction(it) })
        LocationSource.add(LocationProvider())
        LocationSource.add(Stratux)
        TrafficSource.add(Stratux)
    }

    private fun updateState(previous: ApplicationState, value: ApplicationState) {
        when {
            value.visible && disposables.size() == 0 -> {
                onStart()
            }
            !value.visible -> {
                onStop()
            }
        }
    }

    private fun onStop() {
        setBusy(false)
        LocationSource.remove(LocationProvider())
        LocationSource.remove(Stratux)
        TrafficSource.remove(Stratux)
        disposables.clear()
    }

    private fun processAction(action: UiAction) {
        logMsg("processAction $action on ${Thread.currentThread()}")
        if (fgsp.processAction(action))
            return
        when (action) {
            MainMenu.Action.Map -> {
                if (!mapViewShowing) {
                    presenter.popState()
                    showController(null)
                }
                presenter.state = SlideViewPresenter.State.Normal
            }
            MainMenu.Action.Settings -> {
                //showController(TaskController::class.java)
            }
            MainMenu.Action.Bluetooth -> {
                showController(CJBluetoothController::class.java)
            }
            MainMenu.Action.Permissions -> {
                AppPermission.requestPermissions(
                    listOf(
                        AppPermission.BLUETOOTH,
                        AppPermission.LOCATION_ALWAYS
                    )
                ).subscribe()
            }
            else -> logMsg("Can't handle action $action")
        }
    }

    /**
     * Set the busy indicator going, adding it to the child controller view if present.
     */
    private fun setBusy(state: Boolean) {
        if (state) {
            if (busyDisposable.isDisposed) {
                busyDisposable = Single.timer(500L, TimeUnit.MILLISECONDS)
                    .observeOnMainBy {
                        if (!busyIndicator.isDescendantOf(view)) {
                            view.addSubview(busyIndicator)
                            busyIndicator.center = CGPoint(view.bounds.width / 2, view.bounds.height / 2)
                            busyIndicator.startAnimating()
                        }
                    }
            }
        } else {
            busyDisposable.dispose()
            busyIndicator.stopAnimating()
            if (busyIndicator.isDescendantOf(view)) {
                busyIndicator.removeFromSuperview()
            }
        }
    }

    @Suppress("CheckResult")
    override fun viewDidLoad() {
        super.viewDidLoad()
        view.setAutoresizesSubviews(false)
        addChildViewController(mapView)
        setViews(mapView.view, navView, soarView)
        presenter.state = SlideViewPresenter.State.values()[DataStore().getInt(STATE_KEY, 0)]
        ApplicationState.current.changeObserver.subscribe { updateState(it.first, it.second) }
        presenter.listener = object : SlideViewPresenter.Listener {
            override fun onStateChange(state: SlideViewPresenter.State, stackSize: Int) {
                mapViewModel.paused = !(ApplicationState.current.data.visible && stackSize == 0
                    && state == SlideViewPresenter.State.Normal)
            }
        }
    }


    companion object {
        const val STATE_KEY = "mainpage_state"
    }

}
