/*
 * Copyright (c) 2021.  Control-J Pty Ltd
 * All rights reserved
 */

package com.controlj.testapp.app

import com.controlj.logging.CJLog
import com.controlj.ui.ButtonDialogItem
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.ui.UiAction

object MainMenu : DialogData(Type.MODAL, "Testapp") {
    enum class Action(
        val title: String,
        val image: String = "",
        val enabled: () -> Boolean = { true },
        val color: () -> Int = { 0 },
        val action: (UiAction, DialogItem?) -> Unit = { actionItem, _ -> UiAction.sendAction(actionItem) }
    ) : UiAction {
        Map("Map", "menu_map"),
        Settings("Settings", "menu_settings"),
        Bluetooth("Bluetooth", "menu_bluetooth"),
        Permissions("Check permissions", "menu_settings")
    }

    init {
        Action.values().forEach { menuAction ->
            addItem(object : ButtonDialogItem(
                menuAction.title,
                image = menuAction.image,
                action = { CJLog.debug("action ${menuAction.title}"); menuAction.action(menuAction, it); true }
            ) {
                @Suppress("UNUSED_PARAMETER")
                override var isEnabled: Boolean
                    get() = menuAction.enabled.invoke()
                    set(value) {}
            })
        }
    }
}
