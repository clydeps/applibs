/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */

package com.controlj.testapp.app

import com.controlj.framework.IosBaseApp
import com.controlj.framework.IosBleManager
import com.controlj.framework.IosDataStore
import com.controlj.framework.IosDeviceLocationProvider
import com.controlj.framework.IosKeyboardStatus
import com.controlj.framework.IosNotificationPresenter
import com.controlj.framework.IosSoundPlayer
import com.controlj.ios.graphics.IOSGraphicsFactory
import com.controlj.ios.view.NavigationDrawerController
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.MainScheduler
import com.controlj.shim.IosCxFactory
import com.controlj.testapp.view.PageController
import com.controlj.view.NavigationDrawer
import org.robovm.apple.foundation.NSArray
import org.robovm.apple.foundation.NSAutoreleasePool
import org.robovm.apple.foundation.NSError
import org.robovm.apple.foundation.NSSet
import org.robovm.apple.foundation.NSURL
import org.robovm.apple.foundation.NSUserDefaults
import org.robovm.apple.uikit.UIApplication
import org.robovm.apple.uikit.UIApplicationLaunchOptions
import org.robovm.apple.uikit.UIApplicationOpenURLOptions
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.usernotifications.UNAuthorizationOptions
import org.robovm.apple.usernotifications.UNNotificationAction
import org.robovm.apple.usernotifications.UNNotificationActionOptions
import org.robovm.apple.usernotifications.UNNotificationCategory
import org.robovm.apple.usernotifications.UNNotificationCategoryOptions
import org.robovm.apple.usernotifications.UNUserNotificationCenter
import reactivex.ios.schedulers.IosSchedulers

class TestAppIos : IosBaseApp() {


    //private lateinit var mainPageController: MainPageController
    private lateinit var navigationDrawerController: NavigationDrawerController

    //lateinit var delegate: IosBlueMAX
    override val identifier: String = "TestApp"

    override fun didFinishLaunching(application: UIApplication, launchOptions: UIApplicationLaunchOptions?): Boolean {
        super.didFinishLaunching(application, launchOptions)

        // for debug purposes, log messages from the layout system
        //  if(isDebugBuild)
        //     CJLogView.logger = CJLog.logger
        /**
         * Initialize standard objects
         */
        MainScheduler.instance = IosSchedulers.mainThread()
        IosCxFactory()
        IOSGraphicsFactory
        IosKeyboardStatus
        IosSoundPlayer
        IosNotificationPresenter
        IosBleManager

        val pageController = PageController()

        NSUserDefaults.getStandardUserDefaults().put("MGLMapboxMetricsEnabled", false)
        navigationDrawerController = NavigationDrawerController(NavigationDrawer.Position.LEFT, MainMenu)
        navigationDrawerController.view.backgroundColor = UIColor.darkGray()
        window.rootViewController = navigationDrawerController
        //window.addSubview(navigationDrawerController.view)
        navigationDrawerController.view.frame = window.frame
        navigationDrawerController.addChildViewController(pageController)
        // Set the view controller as the root controller for the window.
        // Make the window visible.
        window.makeKeyAndVisible()
        val options = UNAuthorizationOptions.with(
            UNAuthorizationOptions.Alert,
            UNAuthorizationOptions.Sound
        )
        val block = { success: Boolean, _: NSError? ->
            logMsg("Notification permission $success")
        }
        UNUserNotificationCenter.currentNotificationCenter().requestAuthorization(options, block)
        val action = UNNotificationAction("OPEN_TESTAPP", "Open TestApp", UNNotificationActionOptions.Foreground)
        val category = UNNotificationCategory("TestApp", NSArray(action), NSArray(), UNNotificationCategoryOptions.None)
        UNUserNotificationCenter.currentNotificationCenter().setNotificationCategories(NSSet(category))
        IosDeviceLocationProvider
        return true
    }


    override fun didEnterBackground(application: UIApplication) {
        super.didEnterBackground(application)
        NSUserDefaults.getStandardUserDefaults().synchronize()
    }

    override fun openURL(app: UIApplication, url: NSURL, options: UIApplicationOpenURLOptions): Boolean {
        //delegate.onUri(URI(url.toString()))
        return true
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            val pool = NSAutoreleasePool()
            UIApplication.main<UIApplication, TestAppIos>(args, null, TestAppIos::class.java)
            pool.close()
        }
    }
}
