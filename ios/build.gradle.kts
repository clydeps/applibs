/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */

plugins {
    kotlin("jvm")
    id("robovm")
    id("org.jetbrains.dokka")
}

val dokkaJar by tasks.creating(Jar::class) {
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
    dependsOn(tasks.dokkaHtml)
}


dependencies {
    implementation(project(":common"))
    listOf(
        ThreetenBp,
        Rxjava3,
        Gson,
        Ktorm,
        KtormSqlite,
        //Rxjava3Kotlin,
        Kotlin,
        KotlinReflect,
        Retrofit,
        RetrofitGson,
        RetrofitRxJava3,
        XLayout,
        XLayoutIos,
        Robovm,
        RobovmCocoaTouch,
        CJLog,
        RobopodsMapbox
    ).forEach { implementation(it()) }

    listOf(
        Junit,
        Mockk
    ).forEach { testImplementation(it()) }
}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
            artifact(dokkaJar)
            groupId = rootProject.group as String
            version = rootProject.version as String
            artifactId = "ios"

            pom {
                name.set("Applibs iOS")
                description.set("iOS specific application libraries")
                licenses {
                    license {
                        name.set("Copyright (C) 2021 Control-J Pty Ltd. All rights reserved")
                    }
                }
                developers {
                    developer {
                        id.set("clyde")
                        name.set("Clyde Stubbs")
                    }
                }
                scm {
                    connection.set("scm:git:git@gitlab.com:clydeps/applibs.git")
                }
            }

            versionMapping {
                usage("java-api") {
                    fromResolutionOf("runtimeClasspath")
                }
                usage("java-runtime") {
                    fromResolutionResult()
                }
            }
        }
    }
}

task<Exec>("updateIosDeviceList") {
    commandLine(
        "curl", "https://gist.githubusercontent.com/adamawolf/3048717/raw/Apple_mobile_device_types.txt",
        "--output", "$projectDir/resources/assets/iosDeviceList.txt"
    )
}

