package android.os;

/**
 * Created by clyde on 31/12/18.
 */

public class Build {
    public final static class VERSION {
        public final static int SDK_INT = 21; // android 5.0
    }
}
