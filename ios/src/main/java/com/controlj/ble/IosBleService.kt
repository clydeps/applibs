package com.controlj.ble

import org.robovm.apple.corebluetooth.CBService

/**
 * Created by clyde on 9/5/17.
 */

class IosBleService(private val device: IosBleDevice, val service: CBService) : BleGattService() {

    var discoveryComplete = false
    override val uuid: String
        get() = service.uuid.uuidString

    override val characteristics: List<BleCharacteristic> by lazy {
        service.characteristics?.map { IosBleCharacteristic(device, it) } ?: listOf()
    }
}
