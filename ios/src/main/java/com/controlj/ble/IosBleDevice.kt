package com.controlj.ble

import com.controlj.framework.IosBleManager
import com.controlj.logging.CJLog.logMsg
import org.robovm.apple.corebluetooth.CBCharacteristic
import org.robovm.apple.corebluetooth.CBCharacteristicWriteType
import org.robovm.apple.corebluetooth.CBPeripheral
import org.robovm.apple.corebluetooth.CBPeripheralDelegateAdapter
import org.robovm.apple.corebluetooth.CBPeripheralState
import org.robovm.apple.corebluetooth.CBService
import org.robovm.apple.foundation.NSArray
import org.robovm.apple.foundation.NSData
import org.robovm.apple.foundation.NSError
import org.robovm.apple.foundation.NSNumber

/**
 * Created by clyde on 9/5/17.
 */

class IosBleDevice(val peripheral: CBPeripheral) : BleDevice(
    peripheral.identifier.toString(), peripheral.name
        ?: "Unknown"
) {
    private var iosServices = listOf<IosBleService>()

    private val delegate = object : CBPeripheralDelegateAdapter() {
        override fun didReadRSSI(peripheral: CBPeripheral?, RSSI: NSNumber?, error: NSError?) {
            if (error != null || RSSI == null)
                onRssiError(error.toString())
            else
                onRssiRead(RSSI.intValue())
        }

        override fun didUpdateValue(peripheral: CBPeripheral, characteristic: CBCharacteristic?, error: NSError?) {
            if (characteristic != null) {
                val ch = getBleCharacteristic(characteristic.uuid.uuidString)
                if (ch != null) {
                    if (characteristic.isNotifying) {
                        if (error != null)
                            onNotificationError(error.toString())
                        else
                            onNotification(ch, characteristic.value.bytes)
                    }
                    if (error != null)
                        onReadError(ch, error.toString())
                    else
                        onReadCharacteristic(ch, characteristic.value.bytes)
                }
            }
        }

        override fun didUpdateName(peripheral: CBPeripheral) {
            logMsg("Device did update name")
        }

        /**
         * Take the next service for which we need to discover characteristics and initiate it.
         * If there is no such service, declare discovery complete
         */
        private fun discoverNext() {
            iosServices.firstOrNull { !it.discoveryComplete }?.let {
                peripheral.discoverCharacteristics(null, it.service)
            } ?: onServicesDiscovered(iosServices)
        }

        override fun didDiscoverServices(peripheral: CBPeripheral, error: NSError?) {
            error?.let {
                onDiscoveryError(it.toString())
                return
            }
            val services = peripheral.services
            iosServices = services.map { IosBleService(this@IosBleDevice, it) }
            discoverNext()
        }

        override fun didDiscoverCharacteristics(peripheral: CBPeripheral, service: CBService, error: NSError?) {
            error?.let {
                onDiscoveryError(it.toString())
                return
            }
            iosServices.filter { it.service == service }.forEach { it.discoveryComplete = true }
            discoverNext()
        }

        override fun didDiscoverDescriptors(
            peripheral: CBPeripheral,
            characteristic: CBCharacteristic,
            error: NSError?
        ) {
            error?.let { return onDescriptorsError(it.toString()) }
            getCharacteristic(characteristic.uuid.uuidString)?.let { blechar ->
                return onDescriptorsRead(blechar, characteristic.descriptors.map { it.uuid.uuidString })
            }
            onDescriptorsError("Characteristic $characteristic not found for descriptors")
        }

        override fun didWriteValue(peripheral: CBPeripheral, characteristic: CBCharacteristic, error: NSError?) {
            error?.let { return onWriteError(it.toString()) }
            onWriteCharacteristic(getBleCharacteristic(characteristic.uuid.uuidString))
        }

        override fun didModifyServices(peripheral: CBPeripheral, invalidatedServices: NSArray<CBService>) {
            val str = invalidatedServices.joinToString { it.toString() }
            logMsg("Device modified services: $str")
            // re-discover
            peripheral.discoverServices(null)
        }
    }

    init {
        peripheral.delegate = delegate
        peripheral.readRSSI()
    }

    override fun readCharacteristic(characteristic: BleCharacteristic) {
        this.peripheral.readValue((characteristic as IosBleCharacteristic).characteristic)
    }

    override fun writeCharacteristic(characteristic: BleCharacteristic, data: ByteArray) {
        if (characteristic.writeType == BleCharacteristic.WriteType.NO_RESPONSE) {
            this.peripheral.writeValue(
                NSData(data),
                (characteristic as IosBleCharacteristic).characteristic,
                CBCharacteristicWriteType.WithoutResponse
            )
            onWriteCharacteristic(getBleCharacteristic(characteristic.uuid))
        } else
            this.peripheral.writeValue(
                NSData(data),
                (characteristic as IosBleCharacteristic).characteristic,
                CBCharacteristicWriteType.WithResponse
            )
    }

    override fun connect(automatic: Boolean) {
        when (peripheral.state) {
            CBPeripheralState.Connected -> onConnected()
            else -> IosBleManager.connect(this)
        }
    }

    override fun disconnect() {
        super.disconnect()
        return when (peripheral.state) {
            CBPeripheralState.Connected, CBPeripheralState.Connecting -> IosBleManager.disconnect(this)
            else -> onDisconnected()
        }
    }

    override fun discover() {
        peripheral.discoverServices(null)
    }

    override fun requestMtu(mtu: Int) {
        onMtuChange(96)    // how to increase this?
    }

    override fun triggerRssiRead() {
        peripheral.readRSSI()
    }

    override fun setPriority(priority: Priority): Boolean {
        return false
    }

    override fun setNotification(characteristic: BleCharacteristic, enable: Boolean) {
        //logMsg("Setting notification %s %s", bleCharacteristic.getUuid(), Boolean.toString(b));
        val iosBleCharacteristic = characteristic as IosBleCharacteristic
        if (peripheral.state == CBPeripheralState.Connected)
            peripheral.setNotifyValue(enable, iosBleCharacteristic.characteristic)
        else if (enable)
            logMsg("%s: Attempt to enable notifications when not in connected state", name)
        onNotificationSet(iosBleCharacteristic)
    }

    override fun startReadDescriptors(characteristic: BleCharacteristic) {
        peripheral.discoverDescriptors((characteristic as IosBleCharacteristic).characteristic)
    }
}
