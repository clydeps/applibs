package com.controlj.ble

import com.controlj.framework.IosBleManager
import io.reactivex.rxjava3.core.Single
import org.robovm.apple.corebluetooth.CBCharacteristic

/**
 * Created by clyde on 9/5/17.
 */

class IosBleCharacteristic(override val device: IosBleDevice, val characteristic: CBCharacteristic) :
    BleCharacteristic(device, characteristic.uuid.uuidString) {
}
