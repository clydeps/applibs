package com.controlj.ble

import com.controlj.framework.IosBleManager
import org.robovm.apple.corebluetooth.CBAdvertisementData
import org.robovm.apple.corebluetooth.CBPeripheral
import java.nio.ByteBuffer
import java.nio.ByteOrder

/**
 * Created by clyde on 9/5/17.
 */

class IosBleScanResult(
    peripheral: CBPeripheral,
    advertismentData: CBAdvertisementData,
    manufData: ByteArray,
    rssi: Int = -200
) : BleScanResult(
    peripheral.identifier.toString(),
    advertismentData.localName ?: "Unknown",
    rssi
) {

    private val manufBuf = ByteBuffer.wrap(manufData).order(ByteOrder.LITTLE_ENDIAN)

    override val isConnected: Boolean
        get() = IosBleManager.getDevice(address)?.isConnected == true
    override val connectable: Boolean = advertismentData.isConnectable

    override fun getManufacturerData(manuf: Int): ByteArray {
        manufBuf.rewind()
        if (manufBuf.remaining() >= 2 && (manufBuf.short.toInt() and 0xFFFF) == manuf) {
            val bytes = ByteArray(manufBuf.remaining())
            manufBuf.get(bytes)
            return bytes
        }
        return byteArrayOf()
    }
}
