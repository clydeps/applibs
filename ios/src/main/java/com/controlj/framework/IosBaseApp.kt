package com.controlj.framework

import com.controlj.framework.IosBackgroundServiceProvider.isIos13
import com.controlj.logging.CJLog
import com.controlj.logging.CJLog.logMsg
import com.controlj.logging.CrashLogger
import com.controlj.logging.FileLogger
import com.controlj.logging.HttpLogger
import com.controlj.ui.ViewModelStore
import com.controlj.view.ViewContext
import org.robovm.apple.foundation.NSUserDefaults
import org.robovm.apple.uikit.UIApplication
import org.robovm.apple.uikit.UIApplicationDelegateAdapter
import org.robovm.apple.uikit.UIApplicationLaunchOptions
import org.robovm.apple.uikit.UIInterfaceOrientationMask
import org.robovm.apple.uikit.UIWindow
import java.io.File

/**
 * Created by clyde on 14/4/18.
 */

abstract class IosBaseApp : UIApplicationDelegateAdapter() {

    /**
     * Identifies this app, used for logfile names etc.
     */
    abstract val identifier: String

    override fun didFinishLaunching(application: UIApplication, launchOptions: UIApplicationLaunchOptions?): Boolean {
        IosBackgroundServiceProvider
        IosFilePaths
        IOSCrashLogger.init()
        IosDataStore(NSUserDefaults.getStandardUserDefaults())
        Thread.setDefaultUncaughtExceptionHandler(IOSCrashLogger)
        CJLog.deviceId = this.javaClass.simpleName + "-" + IosBackgroundServiceProvider.deviceName.replace(' ', '_')
        CJLog.isDebug = IosBackgroundServiceProvider.isDebugBuild
        CJLog.buildNumber = IosBackgroundServiceProvider.build
        CJLog.versionString = IosBackgroundServiceProvider.version
        CJLog.add(FileLogger(File(IosFilePaths.tempDirectory, "$identifier.log")))
        CJLog.captureErr()
        CJLog.add(FoundationLogger())
        // do http logging only for simulator or ad-hoc builds
        if (IosBackgroundServiceProvider.isDebugBuild)
            CJLog.add(HttpLogger())
        logMsg("device is ${IosBackgroundServiceProvider.model}")
        ViewModelStore.setup(FilePaths().dataDirectory)
        // Create a new window at screen size.
        window = UIWindow()
        ApplicationState.current.data = ApplicationState.Background
        return true
    }

    override fun willTerminate(application: UIApplication) {
        super.willTerminate(application)
        ApplicationState.current.data = ApplicationState.Stopped
        logMsg("Application terminating")
        CrashLogger.close()
        CJLog.close()
    }

    override fun didEnterBackground(application: UIApplication) {
        super.didEnterBackground(application)
        ApplicationState.current.data = ApplicationState.Background
    }

    override fun willEnterForeground(application: UIApplication) {
        super.willEnterForeground(application)
        ApplicationState.current.data = ApplicationState.Visible
    }

    override fun didBecomeActive(application: UIApplication) {
        super.didBecomeActive(application)
        ApplicationState.current.data = ApplicationState.Active
    }

    override fun willResignActive(application: UIApplication) {
        super.willResignActive(application)
        ApplicationState.current.data = ApplicationState.Visible
    }

    override fun didReceiveMemoryWarning(application: UIApplication) {
        logMsg("Did receive memory warning")
        super.didReceiveMemoryWarning(application)
    }

    // allow any interface orientation. The splash screen is shown only in portrait.
    override fun getSupportedInterfaceOrientations(
        application: UIApplication,
        window: UIWindow?
    ): UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.All
    }

    companion object {
        lateinit var viewContext: ViewContext

        @Suppress("DEPRECATION")
            /**
             * get the application's key window. Since we do not use scenes, there is only one, and it's the window
             * created and stored by the application delegate.
             */
        val keyWindow
            get() = UIApplication.getSharedApplication().delegate.window
    }
}
