package com.controlj.framework

/**
 * Created by clyde on 2/7/18.
 */
class IosDevice(val model: String, val description: String) {

    companion object {
        const val FILE_NAME = "iosDeviceList.txt"


        val map = FilePaths().openAsset(FILE_NAME)?.use { stream ->
            stream.reader()
                    .readLines()
                    .map { line -> line.split(" : ", limit = 2) }
                    .filter { it.size == 2 }
                    .map { IosDevice(it[0], it[1]) }
                    .associateBy { it.model }
        } ?: mapOf()

        fun lookup(mode: String): IosDevice = map.getOrElse(mode) { IosDevice(mode, "Unknown device") }
    }

    override fun toString(): String {
        return "IosDevice(model='$model', description='$description')"
    }

}
