package com.controlj.framework

import com.controlj.ble.BleDevice
import com.controlj.ble.BleScanner
import com.controlj.ble.IosBleDevice
import com.controlj.ble.IosBleScanResult
import com.controlj.logging.CJLog.logMsg
import org.robovm.apple.corebluetooth.CBAdvertisementData
import org.robovm.apple.corebluetooth.CBCentralManager
import org.robovm.apple.corebluetooth.CBCentralManagerDelegateAdapter
import org.robovm.apple.corebluetooth.CBCentralManagerOptions
import org.robovm.apple.corebluetooth.CBConnectPeripheralOptions
import org.robovm.apple.corebluetooth.CBManagerState
import org.robovm.apple.corebluetooth.CBPeripheral
import org.robovm.apple.corebluetooth.CBUUID
import org.robovm.apple.foundation.NSArray
import org.robovm.apple.foundation.NSError
import org.robovm.apple.foundation.NSNumber
import org.robovm.apple.foundation.NSUUID
import reactivex.ios.schedulers.IosSchedulers

object IosBleManager : BleScanner(IosSchedulers.mainThread()) {
    private val delegate: ScannerDelegate = ScannerDelegate()
    private val adapter by lazy {
        CBCentralManager(
            delegate,
            null,
            CBCentralManagerOptions().setShowsPowerAlert(true)
        )
    }
    private val deviceList = mutableMapOf<String, IosBleDevice>()
    private val uuidRegex =
        Regex("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", RegexOption.IGNORE_CASE)

    override val isBleSupported: Boolean
        get() = !IosBackgroundServiceProvider.isSimulator && adapter.state != CBManagerState.Unsupported


    override val currentState: AdapterState
        get() = when (adapter.state) {
            CBManagerState.Unsupported -> AdapterState.Unsupported
            CBManagerState.Unauthorized -> AdapterState.Unauthorised
            CBManagerState.PoweredOff -> AdapterState.PoweredOff
            CBManagerState.PoweredOn -> AdapterState.Ready
            else -> AdapterState.PoweringOn
        }

    override fun getDevice(address: String): IosBleDevice? {
        if (deviceList.containsKey(address))
            return deviceList[address]
        if (!address.matches(uuidRegex))
            return null
        val list = adapter.retrievePeripheralsWithId(NSArray(NSUUID(address)))
        if (list != null && list.size != 0) {
            val bleDevice = IosBleDevice(list[0])
            deviceList[address] = bleDevice
            return bleDevice
        }
        return null
    }

    override fun getConnectedDevices(vararg addresses: String): List<BleDevice> {
        val uuids = if (addresses.isEmpty())
            null
        else
            NSArray(addresses.map { CBUUID(it) })
        return adapter.retrieveConnectedPeripherals(uuids)
            .mapNotNull { getDevice(it.identifier.toString()) }
    }

    override fun stopScan() {
        logMsg("Stopping scan")
        if (adapter.isScanning)
            adapter.stopScan()
    }

    override fun startScan() {
        if (!adapter.isScanning) {
            logMsg("Starting scan")
            adapter.scanForPeripherals(null, null)
        }
    }

    fun connect(device: IosBleDevice) {
        val peripheral = device.peripheral
        val options = CBConnectPeripheralOptions()
        options.setNotifiesOnConnection(true)
        options.setNotifiesOnDisconnection(true)
        options.setNotifiesOnNotification(true)
        logMsg("Calling connect on ${device.name}, thread=${Thread.currentThread().name}")
        adapter.connectPeripheral(peripheral, options)
    }

    fun disconnect(device: IosBleDevice) {
        logMsg("ios cancelPeripheralConnection")
        adapter.cancelPeripheralConnection(device.peripheral)
    }

    internal class ScannerDelegate : CBCentralManagerDelegateAdapter() {
        override fun didUpdateState(cbCentralManager: CBCentralManager) {
            logMsg("didUpdateState to ${cbCentralManager.state}")
            onStateChange()
        }

        override fun didDiscoverPeripheral(
            cbCentralManager: CBCentralManager,
            cbPeripheral: CBPeripheral,
            cbAdvertisementData: CBAdvertisementData,
            rssi: NSNumber
        ) {
            val manufacturerData = cbAdvertisementData.manufacturerData
            val data: ByteArray = manufacturerData?.bytes ?: byteArrayOf()
            val bleScanResult = IosBleScanResult(
                cbPeripheral,
                cbAdvertisementData,
                data,
                rssi.intValue()
            )
            onNextScanResult(bleScanResult)
        }

        override fun didConnectPeripheral(cbCentralManager: CBCentralManager, cbPeripheral: CBPeripheral) {
            val device = deviceList[cbPeripheral.identifier.toString()]
            //logMsg("didConnect(%s)", cbPeripheral.getIdentifier().toString());
            if (device != null) {
                device.onConnected()
                logMsg("device ${device.name} connected")
            }
        }

        override fun didFailToConnectPeripheral(
            cbCentralManager: CBCentralManager,
            cbPeripheral: CBPeripheral,
            nsError: NSError
        ) {
            logMsg("didFailToConnect")
            val device = deviceList[cbPeripheral.identifier.toString()]
            device?.onDisconnected()
        }

        override fun didDisconnectPeripheral(
            cbCentralManager: CBCentralManager,
            cbPeripheral: CBPeripheral,
            nsError: NSError?
        ) {
            deviceList[cbPeripheral.identifier.toString()]?.let { device ->
                logMsg("device ${device.name} disconnected: error=$nsError")
                device.onDisconnected()
            } ?: logMsg("Disconnected unknown device ${cbPeripheral.identifier}")
        }
    }

    init {
        init()
    }
}
