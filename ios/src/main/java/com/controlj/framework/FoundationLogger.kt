package com.controlj.framework

import com.controlj.logging.Destination
import org.robovm.apple.foundation.Foundation
import java.io.File

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 28/1/17
 * Time: 1:48 PM
 */
class FoundationLogger : Destination {
    override fun sendMessage(deviceID: String, message: String) {
        // remove the timestamp and trailing newline, since the foundation logger adds it.
        Foundation.log(message.removeSuffix("\n").substringAfter(": "))
    }
}
