package com.controlj.framework

import com.controlj.data.Progress
import com.controlj.event.EventBus
import com.controlj.framework.IosBackgroundServiceProvider.didComplete
import com.controlj.logging.CJLog.logMsg
import io.reactivex.rxjava3.core.ObservableEmitter
import org.apache.http.HttpException
import org.robovm.apple.foundation.NSData
import org.robovm.apple.foundation.NSError
import org.robovm.apple.foundation.NSHTTPURLResponse
import org.robovm.apple.foundation.NSURLResponse
import org.robovm.apple.foundation.NSURLSession
import org.robovm.apple.foundation.NSURLSessionDataDelegateAdapter
import org.robovm.apple.foundation.NSURLSessionDataTask
import org.robovm.apple.foundation.NSURLSessionResponseDisposition
import org.robovm.apple.foundation.NSURLSessionTask
import org.robovm.objc.annotation.Method
import org.robovm.objc.block.VoidBlock1
import java.util.concurrent.ConcurrentSkipListMap

/**
 * Create a delegate to monitor an NSURLSession
 * @param identifier The identifier
 * @param completionHandler completion handler
 */

class SessionDelegate internal constructor(
    val identifier: String,
    val completionHandler: Runnable?
) : NSURLSessionDataDelegateAdapter() {

    /**
     * The emitters for the various tasks associated with this delegate
     */
    private val taskEmitters = ConcurrentSkipListMap<Long, ObservableEmitter<Progress>>()

    fun addTask(identifier: Long, emitter: ObservableEmitter<Progress>) {
        taskEmitters[identifier] = emitter
    }

    fun removeTask(identifier: Long): ObservableEmitter<Progress>? {
        return taskEmitters.remove(identifier)
    }

    override fun didSendBodyData(
        session: NSURLSession,
        task: NSURLSessionTask,
        bytesSent: Long,
        totalBytesSent: Long,
        totalBytesExpectedToSend: Long
    ) {
        logMsg("$identifier: sent $bytesSent, expected $totalBytesExpectedToSend")
        taskEmitters[task.taskIdentifier]?.let {
            if (it.isDisposed) {
                removeTask(task.taskIdentifier)
                task.cancel()
            } else
                it.onNext(Progress("Uploading", ((totalBytesSent * 100) / totalBytesExpectedToSend).toInt()))
        }
    }

    override fun didComplete(session: NSURLSession, task: NSURLSessionTask, error: NSError?) {
        logMsg("$identifier: completed, error=$error")
        removeTask(task.taskIdentifier)?.apply {
            if (error != null)
                onError(Exception(error.toString()))
            else {
                onNext(Progress("Complete", 100, true))
                onComplete()
            }
        }
    }

    @Method(selector = "URLSession:dataTask:didReceiveData:")
    fun didReceiveData(
        @Suppress("UNUSED_PARAMETER") session: NSURLSession,
        dataTask: NSURLSessionTask?,
        data: NSData?
    ) {
        if (dataTask != null) {
            val bytes = data?.bytes ?: byteArrayOf()
            val status = (dataTask.response as? NSHTTPURLResponse)?.statusCode ?: -1
            logMsg(String(bytes.take(256).toByteArray()))
            // broadcast the result
            EventBus.postEvent(
                BackgroundServiceProvider.Response(dataTask.taskDescription, status.toInt()), String(bytes)
            )
        } else
            logMsg("session=$session, dataTask = $dataTask, data=$data")
    }

    override fun didFinishEvents(session: NSURLSession) {
        logMsg("didFinishEvents")
        completionHandler?.run()
        session.didComplete()
    }

    override fun didReceiveResponse(
        session: NSURLSession,
        dataTask: NSURLSessionDataTask,
        response: NSURLResponse,
        completionHandler: VoidBlock1<NSURLSessionResponseDisposition>?
    ) {
        (response as? NSHTTPURLResponse)?.apply {
            logMsg("StatusCode is $statusCode")
            if (statusCode >= 300) {
                taskEmitters[dataTask.taskIdentifier]?.onError(HttpException("Error Status: $statusCode"))
            }
        }
        completionHandler?.invoke(NSURLSessionResponseDisposition.Allow)
    }
}

fun NSURLSession.addTask(task: NSURLSessionTask, emitter: ObservableEmitter<Progress>) {
    (delegate as? SessionDelegate)?.apply {
        addTask(task.taskIdentifier, emitter)
    }
}
