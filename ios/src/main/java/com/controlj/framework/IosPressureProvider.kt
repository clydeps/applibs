package com.controlj.framework

import com.controlj.pressure.PressureProvider
import com.controlj.pressure.PressureSource.DEVICE_PRIORITY
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import org.robovm.apple.coremotion.CMAltimeter
import org.robovm.apple.foundation.NSOperationQueue

class IosPressureProvider : PressureProvider {
    private val altimeter = CMAltimeter()

    override val pressureObserver: Observable<Double> by lazy {
        Observable.create { e: ObservableEmitter<Double> ->
            if (CMAltimeter.isRelativeAltitudeAvailable()) {
                e.setCancellable {
                    altimeter.stopRelativeAltitudeUpdates()
                }
                altimeter.startRelativeAltitudeUpdates(NSOperationQueue.getMainQueue()) { cmAltitudeData, _ ->
                    e.onNext(cmAltitudeData.pressure.floatValue() * 10.0)
                }
            }
        }.share()
    }

    override val priority: Int = DEVICE_PRIORITY
}
