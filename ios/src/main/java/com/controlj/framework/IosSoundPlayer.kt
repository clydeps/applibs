package com.controlj.framework

import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import org.robovm.apple.avfoundation.AVAudioPlayer
import org.robovm.apple.avfoundation.AVAudioPlayerDelegateAdapter
import org.robovm.apple.avfoundation.AVAudioSession
import org.robovm.apple.avfoundation.AVAudioSessionCategory
import org.robovm.apple.avfoundation.AVAudioSessionCategoryOptions
import org.robovm.apple.avfoundation.AVSpeechSynthesisVoice
import org.robovm.apple.avfoundation.AVSpeechSynthesizer
import org.robovm.apple.avfoundation.AVSpeechSynthesizerDelegateAdapter
import org.robovm.apple.avfoundation.AVSpeechUtterance
import org.robovm.apple.foundation.NSBundle

object IosSoundPlayer : SoundPlayer {
    private val soundMap = HashMap<String, AVAudioPlayer>()

    private val session get() = AVAudioSession.getSharedInstance()

    private val soundDelegate = object : AVAudioPlayerDelegateAdapter() {
        override fun didFinishPlaying(player: AVAudioPlayer, p1: Boolean) {
            SoundPlayer.scheduler.scheduleDirect {
                try {
                    SoundPlayer.endSound()
                } catch (e: Exception) {
                    logException(e)
                }
            }
        }
    }

    private val speechDelegate = object : AVSpeechSynthesizerDelegateAdapter() {
        override fun didFinishSpeechUtterance(synthesizer: AVSpeechSynthesizer, utterance: AVSpeechUtterance) {
            try {
                SoundPlayer.endSound()
            } catch (e: Exception) {
                logException(e)
            }
        }
    }

    init {
        try {
            session.setCategory(
                AVAudioSessionCategory.Playback,
                AVAudioSessionCategoryOptions.with(AVAudioSessionCategoryOptions.DuckOthers)
            )
        } catch (ex: Exception) {
            logException(ex)
        }
        SoundPlayer.instance = this
    }

    override fun playSound(sound: String) {
        logMsg("Playing sound $sound via ${session.currentRoute.outputs.joinToString { it.portName }}")
        if (IosBackgroundServiceProvider.isSimulator) return
        try {
            val player = soundMap.getOrPut(sound) {
                val url = NSBundle.getMainBundle().findResourceURL(sound, ".mp3")
                if (url == null) {
                    logMsg("Can't find file $sound.mp3")
                    return
                }
                AVAudioPlayer(url).also {
                    it.delegate = soundDelegate
                }
            }
            player.currentTime = 0.0
            player.play()
        } catch (e: Exception) {
            logException(e)
            return
        }
    }

    private val synth by lazy { AVSpeechSynthesizer().apply { delegate = speechDelegate } }

    override fun speak(text: String) {
        AVSpeechUtterance(text).apply {
            voice = AVSpeechSynthesisVoice(SoundPlayer.language)
            synth.enqueueSpeakUtterance(this)
        }
    }

    override fun startSession() {
        session.setActive(true)
    }

    override fun endSession() {
        session.setActive(false)
    }
}
