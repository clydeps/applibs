package com.controlj.framework

import com.controlj.logging.CJLog.logMsg
import org.robovm.apple.foundation.NSBundle
import org.robovm.apple.foundation.NSFileManager
import org.robovm.apple.foundation.NSPathUtilities
import org.robovm.apple.foundation.NSSearchPathDirectory
import org.robovm.apple.foundation.NSSearchPathDomainMask
import java.io.File
import java.io.InputStream

object IosFilePaths : FilePaths {
    private fun getDirectory(searchPath: NSSearchPathDirectory): File {
        val x = NSFileManager.getDefaultManager().getURLsForDirectory(searchPath, NSSearchPathDomainMask.UserDomainMask)
        if (x.isNotEmpty()) {
            val url = x.last()
            val dir = File(url.path)
            //noinspection ResultOfMethodCallIgnored
            dir.mkdirs()
            return dir
        }
        return File(".")
    }

    val assetsPath: File by lazy {
        val bundle = NSBundle.getMainBundle()
        File(bundle.resourcePath, "assets")
    }

    override val dataDirectory: File by lazy { getDirectory(NSSearchPathDirectory.DocumentDirectory) }
    override val databaseDirectory: File by lazy { getDirectory(NSSearchPathDirectory.ApplicationSupportDirectory) }
    override val tempDirectory: File by lazy {
        File(
            if (IosBackgroundServiceProvider.isIos10)
                NSFileManager.getDefaultManager().temporaryDirectory.filePathURL.path
            else
                NSPathUtilities.getTemporaryDirectory()
        ).also { it.mkdirs() }
    }

    override fun listAssets(path: String): List<String> {
        return File(assetsPath, path).list()?.toList() ?: listOf()
    }

    override fun openAsset(name: String): InputStream? {
        return try {
            val f = File(assetsPath, name)
            f.inputStream()
        } catch (ex: Exception) {
            logMsg(ex.toString())
            null
        }
    }


    init {
        FilePaths.init(this)
    }
}
