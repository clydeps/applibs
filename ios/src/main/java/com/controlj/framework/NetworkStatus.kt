package com.controlj.framework

import com.controlj.framework.IosBackgroundServiceProvider.isIos12
import com.controlj.rx.DisposedEmitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import org.robovm.apple.dispatch.DispatchQueue
import org.robovm.apple.foundation.NSOperatingSystemVersion
import org.robovm.apple.foundation.NSProcessInfo
import org.robovm.apple.network.NWPathMonitor
import org.robovm.apple.network.NWPathStatus
import java.net.InetAddress
import java.util.concurrent.TimeUnit

object NetworkStatus {

    const val NETWORK_INTERVAL = 60L     // check network this often if required.
    val NETWORK_TIME_UNIT = TimeUnit.SECONDS
    const val NETWORK_TARGET = "google.com"

    var networkEmitter: ObservableEmitter<Boolean> = DisposedEmitter()
        private set

    var networkReady = false

    /**
     * Listen for changes in network status.
     */
    val networkObserver = Observable.create<Boolean> {
        networkEmitter = it
    }.share()

    init {
        if (isIos12) {
            try {
                val monitor = NWPathMonitor()
                monitor.setQueue(DispatchQueue.getMainQueue())
                monitor.setUpdateHandler { path ->
                    networkReady = path.status == NWPathStatus.satisfied
                    networkEmitter.onNext(networkReady)
                }
                monitor.start()
            } catch (ex: Throwable) {
                //logException(ex)
            }
        }
    }


    /** Check network availability for the given target. This will emit on initial call, every 60 seconds,
     * or when there is a change in network status. It queries both the platform network monitor and does
     * a DNS lookup to confirm actual reachability.
     *
     */
    fun isNetworkAvailable(target: String = NETWORK_TARGET): Observable<Boolean> {
        return Observable.merge(
            networkObserver, Observable.interval(
                0L,
                NETWORK_INTERVAL,
                NETWORK_TIME_UNIT,
                Schedulers.io()
            )
        )
            .map {
                if (!networkReady)
                    false
                else
                    try {
                        InetAddress.getByName(target)
                        true
                    } catch (ex: Exception) {
                        false
                    }
            }
    }

}
