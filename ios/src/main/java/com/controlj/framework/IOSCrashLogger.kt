package com.controlj.framework

import com.controlj.logging.CrashLogger
import org.robovm.apple.foundation.Foundation
import org.robovm.apple.uikit.UIDevice
import java.io.PrintWriter
import java.io.StringWriter
import java.util.Date

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 30/1/17
 * Time: 10:39 AM
 */
object IOSCrashLogger : CrashLogger {
    override fun uncaughtException(t: Thread, e: Throwable) {
        Foundation.log("Uncaught exception")
        Foundation.log(e.toString())
        Foundation.log(t.toString())
        val sw = StringWriter()
        e.printStackTrace(PrintWriter(sw))
        Foundation.log(sw.toString())
        super.uncaughtException(t, e)
    }

    fun init() {
        val dev = UIDevice.getCurrentDevice()
        initialise(mapOf(
                "PHONE_MODEL" to IosBackgroundServiceProvider.model.model,
                "PHONE_DESCRIPTION" to IosBackgroundServiceProvider.model.description,
                "SYSTEM_VERSION" to dev.systemVersion,
                "SYSTEM_NAME" to dev.systemName,
                "ORIENTATION" to dev.orientation.toString(),
                "STARTED_AT" to Date().toString(),
        ))
    }
}
