package com.controlj.framework

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.Location
import com.controlj.data.Position
import com.controlj.framework.IosBackgroundServiceProvider.isIos13
import com.controlj.location.GpsLocation
import com.controlj.location.LocationProvider
import com.controlj.location.Region
import com.controlj.location.RegionMonitor
import com.controlj.logging.CJLog.logMsg
import com.controlj.nmea.GpsFix
import com.controlj.nmea.GpsStatus
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import com.controlj.settings.BooleanSetting
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Single
import org.robovm.apple.corelocation.CLAuthorizationStatus
import org.robovm.apple.corelocation.CLBeacon
import org.robovm.apple.corelocation.CLBeaconIdentityConstraint
import org.robovm.apple.corelocation.CLBeaconRegion
import org.robovm.apple.corelocation.CLCircularRegion
import org.robovm.apple.corelocation.CLLocation
import org.robovm.apple.corelocation.CLLocationCoordinate2D
import org.robovm.apple.corelocation.CLLocationManager
import org.robovm.apple.corelocation.CLLocationManagerDelegateAdapter
import org.robovm.apple.corelocation.CLRegion
import org.robovm.apple.corelocation.CLRegionState
import org.robovm.apple.foundation.NSArray
import org.robovm.apple.foundation.NSError
import org.robovm.apple.foundation.NSUUID
import org.threeten.bp.Instant
import java.util.concurrent.ConcurrentSkipListMap

/**
 * Ios implementation of location provider and region monitor
 */
object IosDeviceLocationProvider : LocationProvider, RegionMonitor {
    override var priority: Int = Location.DEVICE_PRIORITY
    override val name: String = "iOS"
    override var authorisationStatus = LocationProvider.AuthorisationStatus.Unknown

    private var locationEnabled = false
    private var locationDenied = false
    private var locationEmitter: ObservableEmitter<Location> = DisposedEmitter()
    override var isConnected: Boolean = false
        private set
    private val callbackMap = ConcurrentSkipListMap<String, (Region.Event) -> Unit>()
    private var authEmitter: ObservableEmitter<LocationProvider.AuthorisationStatus> = DisposedEmitter()

    private val locationManager: CLLocationManager by lazy {
        CLLocationManager().also {
            logMsg("Created CLLocationManager on thread ${Thread.currentThread()}")
            it.delegate = delegate
        }
    }

    // monitor authorisation changes
    private val authObserver = Observable.create { e ->
        authEmitter = e
        locationManager     // trigger initialization
        authEmitter.onNext(authorisationStatus)
    }
        .subscribeOn(MainScheduler())
        .share()

    override fun checkPermission(): Single<LocationProvider.AuthorisationStatus> {
        return authObserver
            .filter { it != LocationProvider.AuthorisationStatus.Unknown }
            .take(1)
            .single(LocationProvider.AuthorisationStatus.Unknown)
    }

    override fun askPermission(
        rationale: String,
        requiredStatus: LocationProvider.AuthorisationStatus
    ): Single<LocationProvider.AuthorisationStatus> {
        if (authorisationStatus >= requiredStatus)
            return Single.just(authorisationStatus)
        return checkPermission()
            .doOnSubscribe {
                logMsg("Asking for $requiredStatus")
                when (requiredStatus) {
                    LocationProvider.AuthorisationStatus.Always -> locationManager.requestAlwaysAuthorization()
                    LocationProvider.AuthorisationStatus.WhileInUse -> locationManager.requestWhenInUseAuthorization()
                    else -> Unit
                }
            }
            .subscribeOn(MainScheduler())
    }

    private val statusMap = mapOf(
        CLAuthorizationStatus.NotDetermined to LocationProvider.AuthorisationStatus.Unknown,
        CLAuthorizationStatus.Denied to LocationProvider.AuthorisationStatus.None,
        CLAuthorizationStatus.AuthorizedWhenInUse to LocationProvider.AuthorisationStatus.WhileInUse,
        CLAuthorizationStatus.AuthorizedAlways to LocationProvider.AuthorisationStatus.Always,
    )

    private val CLAuthorizationStatus.fromClstatus get() = statusMap[this] ?: LocationProvider.AuthorisationStatus.None

    private fun checkStatus(status: CLAuthorizationStatus) {
        authorisationStatus = status.fromClstatus
        logMsg("Location authorization status is $status")
        when (status) {
            CLAuthorizationStatus.AuthorizedWhenInUse, CLAuthorizationStatus.AuthorizedAlways -> {
                if (locationEnabled) {
                    logMsg("Starting position updates")
                    locationManager.requestLocation()
                    locationManager.startUpdatingLocation()
                    locationManager.startMonitoringSignificantLocationChanges()
                }
            }

            CLAuthorizationStatus.Denied -> locationDenied = true
            else -> Unit
        }
        authEmitter.onNext(authorisationStatus)
    }

    override val locationObserver: Observable<Location> = Observable.create<Location> { emitter ->
        locationEmitter = emitter
        emitter.setCancellable { isConnected = false }

        if (locationDenied)
            emitter.onError(Throwable("Location permission denied"))
        emitter.setCancellable {
            locationManager.stopUpdatingLocation()
            locationManager.stopMonitoringSignificantLocationChanges()
        }
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }.subscribeOn(MainScheduler.instance).share()

    override val enabled = BooleanSetting("iosLocation", "iOS", "iOS device location", true)

    val delegate = object : CLLocationManagerDelegateAdapter() {
        override fun didFail(clLocationManager: CLLocationManager, nsError: NSError) {
            logMsg("Location monitoring failed: $nsError")
            isConnected = false
        }

        override fun monitoringDidFail(manager: CLLocationManager?, region: CLRegion?, error: NSError?) {
            logMsg("Region Monitoring failed for $region: $error")
        }

        override fun didExitRegion(manager: CLLocationManager?, region: CLRegion) {
            notifyEvent(region.toRegion, false)
        }

        override fun didEnterRegion(manager: CLLocationManager, region: CLRegion) {
            notifyEvent(region.toRegion, true)
        }

        // iOS14 and abovve
        override fun didRangeBeacons(
            manager: CLLocationManager?,
            beacons: NSArray<CLBeacon>,
            beaconConstraint: CLBeaconIdentityConstraint?
        ) {
            beacons.forEach { notifyEvent(it.toRegion, true) }
        }

        @Deprecated("Deprecated in Java")
        override fun didRangeBeacons(
            manager: CLLocationManager?,
            beacons: NSArray<CLBeacon>,
            region: CLBeaconRegion?
        ) {
            region?.let { notifyEvent(it.toRegion, true) }
        }

        override fun didDetermineState(manager: CLLocationManager, state: CLRegionState, region: CLRegion) {
            notifyEvent(region.toRegion, state == CLRegionState.Inside)
        }

        /**
         * iOS 14 and above
         */
        override fun locationManagerDidChangeAuthorization(manager: CLLocationManager?) {
            logMsg("locationManagerDidChangeAuthorization")
            checkStatus(locationManager.authorizationStatus())
        }

        @Deprecated("Deprecated in Java")
        override fun didChangeAuthorizationStatus(
            clLocationManager: CLLocationManager,
            clAuthorizationStatus: CLAuthorizationStatus
        ) {
            checkStatus(clAuthorizationStatus)
        }

        override fun didUpdateLocations(manager: CLLocationManager, locations: NSArray<CLLocation>) {
            val loc = locations.last()
            if (!isConnected) {
                logMsg("new Location $loc")
                isConnected = true
            }
            locationEmitter.onNext(loc.toLocation())
        }
    }

    private fun notifyEvent(region: Region, inside: Boolean) {
        logMsg("Event for $region: inside=$inside")
        BackgroundServiceProvider.runBackground("regionMonitor", {
            callbackMap[region.identifier]?.invoke(Region.Event(region, inside))
        })
    }

    override fun startRegionMonitor(region: Region, callback: (Region.Event) -> Unit) {
        if (authorisationStatus != LocationProvider.AuthorisationStatus.Always)
            locationManager.requestAlwaysAuthorization()
        callbackMap[region.identifier] = callback
        val nsRegion = region.toClRegion()
        nsRegion.setNotifiesOnEntry(true)
        nsRegion.setNotifiesOnExit(true)
        locationManager.startMonitoring(nsRegion)
        logMsg("Started monitoring $region")
    }

    override fun stopRegionMonitor(region: Region) {
        logMsg("Stop monitoring $region")
        locationManager.stopMonitoring(region.toClRegion())
        callbackMap.remove(region.identifier)
    }

    override fun clearAllRegions() {
        locationManager.monitoredRegions.forEach {
            logMsg("Stopped monitoring $it")
            locationManager.stopMonitoring(it)
        }
    }

    init {
        LocationProvider.deviceProvider = this
        RegionMonitor.instance = this
    }
}

fun CLLocation.toLocation(): Location {
    val loc = Location.of(
        coordinate.latitude,
        coordinate.longitude,
        altitude,
        speed,
        course,
        IosDeviceLocationProvider.creator,
        Instant.ofEpochMilli((timestamp.timeIntervalSince1970 * 1000).toLong()),
        horizontalAccuracy
    )
    return GpsLocation.of(loc, toGpsStatus())
}

fun CLLocation.toGpsStatus(): GpsStatus {
    val fix = when {
        verticalAccuracy > 0 -> GpsFix.ThreeD
        horizontalAccuracy > 0 -> GpsFix.TwoD
        else -> GpsFix.None
    }
    val satellites =
        when (fix) {
            GpsFix.None -> 0
            GpsFix.TwoD -> 4
            else -> 5
        }
    return GpsStatus.of(
        fix,
        INVALID_DATA,
        INVALID_DATA,
        satellites,
        satellites
    )
}

val CLRegion.toRegion: Region
    get() {
        @Suppress("DEPRECATION")
        return when (this) {
            is CLBeaconRegion -> Region.Beacon(
                (if (isIos13) uuid else proximityUUID).asString(),
                major?.intValue(),
                minor?.intValue()
            )

            is CLCircularRegion -> Region.Circular(Position.of(center.latitude, center.longitude), radius)
            else -> error("unknown region type")
        }
    }

val CLBeacon.toRegion: Region
    get() = Region.Beacon(uuid.asString(), major.intValue(), minor.intValue())


@Suppress("DEPRECATION")
fun Region.toClRegion(): CLRegion {
    return when (this) {
        is Region.Beacon -> {
            val major = this.major
            val minor = this.minor
            val uuid = NSUUID(this.uuid)
            when {
                major == null -> if (isIos13)
                    CLBeaconRegion.createUsingUUID(uuid, identifier)
                else
                    CLBeaconRegion(uuid, identifier)

                minor == null -> if (isIos13)
                    CLBeaconRegion.createUsingUUID(
                        uuid,
                        major.toShort(),
                        identifier
                    )
                else
                    CLBeaconRegion(
                        uuid,
                        major.toShort(),
                        identifier
                    )

                else -> if (isIos13)
                    CLBeaconRegion.createUsingUUID(
                        uuid,
                        major.toShort(),
                        minor.toShort(),
                        identifier
                    )
                else
                    CLBeaconRegion(
                        uuid,
                        major.toShort(),
                        minor.toShort(),
                        identifier
                    )
            }.also { it.setNotifiesEntryStateOnDisplay(true) }
        }

        is Region.Circular -> CLCircularRegion(
            CLLocationCoordinate2D(center.latitude, center.longitude),
            radius,
            identifier
        )

        else -> throw IllegalArgumentException("Unknown region type: ${javaClass.canonicalName}")
    }
}

fun Region.Beacon.toClConstraint(): CLBeaconIdentityConstraint {
    val major = this.major
    val minor = this.minor
    val uuid = NSUUID(this.uuid)
    return when {
        major == null -> CLBeaconIdentityConstraint(uuid)
        minor == null -> CLBeaconIdentityConstraint(uuid, major.toShort())
        else -> CLBeaconIdentityConstraint(uuid, major.toShort(), minor.toShort())
    }
}

@Suppress("DEPRECATION")
fun Region.Beacon.toBeaconRegion(): CLBeaconRegion {
    val major = this.major
    val minor = this.minor
    val uuid = NSUUID(this.uuid)
    return when {
        major == null -> CLBeaconRegion(uuid, identifier)
        minor == null -> CLBeaconRegion(uuid, major.toShort(), identifier)
        else -> CLBeaconRegion(uuid, major.toShort(), minor.toShort(), identifier)
    }
}
