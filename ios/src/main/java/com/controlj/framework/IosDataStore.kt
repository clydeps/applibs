package com.controlj.framework

import com.controlj.logging.CJLog.logException
import com.controlj.settings.DataStore
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.robovm.apple.foundation.NSNumber
import org.robovm.apple.foundation.NSString
import org.robovm.apple.foundation.NSUserDefaults
import java.lang.reflect.Type

/**
 * Created by clyde on 9/5/17.
 */

class IosDataStore(private val userDefaults: NSUserDefaults) : DataStore {
    override val allEntries: Map<String, *>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    override val observeChanges: Observable<String>
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
    private val gson = Gson()
    inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object : TypeToken<T>() {}.type)

    init {
        init()
    }

    override fun putLong(key: String, value: Long) {
        userDefaults.put(key, NSNumber.valueOf(value))
    }

    override fun getLong(key: String, dflt: Long): Long {
        val obj = userDefaults.get(key) ?: return dflt
        when(obj) {
            is NSNumber -> return obj.longValue()
            is NSString -> return obj.toString().toLong()
        }
        return userDefaults.getLong(key)
    }

    override fun getInt(key: String, dflt: Int): Int {
        userDefaults.get(key) ?: return dflt
        return userDefaults.getInt(key)
    }

    override fun remove(key: String) {
        userDefaults.remove(key)
    }

    override fun getIntList(key: String): List<Int> {
        return gson.fromJson<List<Int>>(getString(key)) ?: listOf()
    }

    override fun getStringList(key: String): List<String> {
        return gson.fromJson<List<String>>(getString(key)) ?: listOf()
    }

    override fun getBoolean(name: String): Boolean {
        return userDefaults.getBoolean(name)
    }

    override fun putBoolean(key: String, value: Boolean) {
        userDefaults.put(key, value)
    }

    override fun putStringList(key: String, values: List<String>) {
        putString(key, gson.toJson(values))
    }

    override fun putIntList(key: String, values: List<Int>) {
        putString(key, gson.toJson(values))

    }

    override fun contains(key: String): Boolean {
        return userDefaults.asDictionary().keys.contains(NSString(key))
    }

    override fun putObject(key: String, o: Any) {
        putString(key, gson.toJson(o))

    }

    override fun <T> getObject(key: String, type: Type): T? {
        return null
    }

    override fun <T> getObject(key: String, clazz: Class<T>): T? {
        val s = userDefaults.getString(key) ?: return null
        try {
            return gson.fromJson(s, clazz)
        } catch (ex: JsonSyntaxException) {
            return null
        }
    }

    override fun getFloat(key: String, dflt: Float): Float {
        return 0f
    }

    override fun getString(key: String, dflt: String): String {
        return userDefaults.getString(key) ?: dflt
    }

    override fun putInt(key: String, value: Int) {
        userDefaults.put(key, value)
    }

    override fun putFloat(key: String, value: Float) {
        userDefaults.put(key, value)
    }

    override fun putString(key: String, value: String) {
        userDefaults.put(key, value)
    }

    override val allStringEntries: Map<String, String>
        get() {
            val dict = userDefaults.asDictionary()
            val map = HashMap<String, String>()
            for (nsString in dict.keys) {
                val objec = dict[nsString]
                if (objec is NSString)
                    map[nsString.toString()] = dict.getString(nsString)
            }
            return map
        }

    override val isDebugFeatures: Boolean
        get() {
            return getBoolean(DataStore.DEBUG_FEATURES)
        }

    @Suppress("CheckResult")
    override fun commit() {
        Completable.complete()
                .observeOn(Schedulers.io())
                .subscribe(
                        {
                            userDefaults.synchronize()
                            //debug("Prefs committed")
                        },
                        {
                            logException(it)
                        }
                )
    }
}
