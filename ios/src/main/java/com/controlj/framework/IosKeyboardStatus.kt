package com.controlj.framework

import com.controlj.ios.view.asCrect
import com.controlj.ui.KeyboardStatus
import org.robovm.apple.foundation.NSNumber
import org.robovm.apple.uikit.UIKeyboardAnimation
import org.robovm.apple.uikit.UIWindow

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-03-22
 * Time: 06:44
 *
 * This object tracks the keyboard state in iOS.
 */
object IosKeyboardStatus : KeyboardStatus() {

    operator fun invoke(): IosKeyboardStatus {
        return this
    }
    init {
        // Listen for keyboard notifications
        UIWindow.Notifications.observeKeyboardWillShow {
            keyboardState = State.OPENING
            animationDuration = it.animationDuration
            animationCurve = (it.get(UIKeyboardAnimation.Keys.AnimationCurve()) as NSNumber).longValue()
        }
        UIWindow.Notifications.observeKeyboardDidShow {
            keyboardState = State.OPEN
        }
        UIWindow.Notifications.observeKeyboardWillHide {
            keyboardState = State.CLOSING
            animationDuration = it.animationDuration
            animationCurve = (it.get(UIKeyboardAnimation.Keys.AnimationCurve()) as NSNumber).longValue()
        }
        UIWindow.Notifications.observeKeyboardDidHide {
            keyboardState = State.CLOSED
        }
        UIWindow.Notifications.observeKeyboardWillChangeFrame { dict ->
            keyboardFrame = dict.endFrame.asCrect()
        }
    }
}
