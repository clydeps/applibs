@file:Suppress("DEPRECATION")

package com.controlj.framework

import com.controlj.data.Message
import com.controlj.framework.IosBackgroundServiceProvider.isIos10
import com.controlj.ios.toNSArray
import com.controlj.logging.CJLog
import com.controlj.rx.MainScheduler
import com.controlj.ui.NotificationPresenter
import org.robovm.apple.foundation.NSArray
import org.robovm.apple.foundation.NSError
import org.robovm.apple.foundation.NSObject
import org.robovm.apple.foundation.NSSet
import org.robovm.apple.uikit.UIApplication
import org.robovm.apple.uikit.UILocalNotification
import org.robovm.apple.uikit.UIUserNotificationSettings
import org.robovm.apple.uikit.UIUserNotificationType
import org.robovm.apple.usernotifications.UNAuthorizationOptions
import org.robovm.apple.usernotifications.UNAuthorizationStatus
import org.robovm.apple.usernotifications.UNMutableNotificationContent
import org.robovm.apple.usernotifications.UNNotification
import org.robovm.apple.usernotifications.UNNotificationAction
import org.robovm.apple.usernotifications.UNNotificationActionOptions
import org.robovm.apple.usernotifications.UNNotificationCategory
import org.robovm.apple.usernotifications.UNNotificationCategoryOptions
import org.robovm.apple.usernotifications.UNNotificationPresentationOptions
import org.robovm.apple.usernotifications.UNNotificationRequest
import org.robovm.apple.usernotifications.UNNotificationResponse
import org.robovm.apple.usernotifications.UNUserNotificationCenter
import org.robovm.apple.usernotifications.UNUserNotificationCenterDelegate
import org.robovm.objc.block.VoidBlock1

object IosNotificationPresenter : NotificationPresenter {

    operator fun invoke(): IosNotificationPresenter {
        return this
    }

    // used on ios 10 and later
    private val center by lazy {
        UNUserNotificationCenter.currentNotificationCenter().apply {
            if (isIos10) {
                delegate = object : NSObject(), UNUserNotificationCenterDelegate {
                    override fun willPresentNotification(
                        center: UNUserNotificationCenter,
                        notification: UNNotification,
                        handler: VoidBlock1<UNNotificationPresentationOptions>
                    ) {
                        handler(UNNotificationPresentationOptions.with(UNNotificationPresentationOptions.Alert))
                    }

                    override fun didReceiveNotificationResponse(
                        center: UNUserNotificationCenter,
                        response: UNNotificationResponse?,
                        handler: Runnable
                    ) {
                        handler.run()
                    }

                    override fun openSettings(p0: UNUserNotificationCenter?, p1: UNNotification?) {
                    }
                }
                val name = DeviceInformation().appName
                val options = UNAuthorizationOptions.with(
                    UNAuthorizationOptions.Alert,
                    UNAuthorizationOptions.Sound
                )
                val block = { success: Boolean, error: NSError? ->
                    CJLog.logMsg("Notification permission $success $error")
                }
                requestAuthorization(options, block)
                val action =
                    UNNotificationAction("OPEN_$name", "Open $name", UNNotificationActionOptions.Foreground)
                val category =
                    UNNotificationCategory(name, NSArray(action), NSArray(), UNNotificationCategoryOptions.None)
                CJLog.logMsg("Setting categories")
                setNotificationCategories(NSSet(category))
            } else {
                val types = UIUserNotificationType.with(UIUserNotificationType.Alert, UIUserNotificationType.Sound)
                UIApplication.getSharedApplication()
                    .registerUserNotificationSettings(UIUserNotificationSettings(types, null))
            }
        }
    }

    private val notificationMap by lazy {
        mutableMapOf<Message, UILocalNotification>()
    }

    init {
            init()
    }


    override fun updateNotifications() {
        val messages = NotificationPresenter.messageList
        if (isIos10) {
            if (messages.isEmpty()) {
                center.removeAllDeliveredNotifications()
                center.removeAllPendingNotificationRequests()
                return
            }
            center.getNotificationSettings { settings ->
                if (settings.authorizationStatus == UNAuthorizationStatus.Authorized) {
                    center.getDeliveredNotifications {
                        MainScheduler.instance.scheduleDirect {
                            sendNotifications(messages, it.toList())
                        }
                    }
                }
            }
        } else {
            with(UIApplication.getSharedApplication()) {
                if (messages.isEmpty()) {
                    cancelAllLocalNotifications()
                    return
                }
                sendUINotifications(messages)
            }
        }
    }

    private fun sendNotifications(messages: List<Message>, existing: List<UNNotification>) {
        // remove any notifications no longer in the list
        val map = messages.map { it.text to it }.toMap()
        val stale = existing.filter { !map.containsKey(it.request.identifier) }
            .map { it.request.identifier }
        val current = existing.filter { map.containsKey(it.request.identifier) }
            .map { it.request.identifier }.toSet()
        val newMessages = messages.filter { !current.contains(it.text) }
        center.removePendingNotificationRequests(stale.toTypedArray().toNSArray())
        center.removeDeliveredNotifications(stale.toTypedArray().toNSArray())
        // send any new notifications
        newMessages.forEach { message ->
            val content = UNMutableNotificationContent()
            content.title = message.text
            content.body = message.subText
            content.subtitle = ""
            content.categoryIdentifier = DeviceInformation().appName
            center.addNotificationRequest(
                UNNotificationRequest(
                    message.text,
                    content,
                    null
                )
            ) { }
            // Play the sound directly. Bluetooth and notification sounds don't seem to mix well.
            if (message.priority.sound.isNotBlank())
                SoundPlayer.playSound(message.priority.sound)
        }
    }

    /**
     * use legacy functions on ios 9.x
     */
    private fun sendUINotifications(messages: List<Message>) {
        with(UIApplication.getSharedApplication()) {
            val stale = notificationMap.filterKeys { !messages.contains(it) }
            stale.forEach {
                cancelLocalNotification(it.value)
                notificationMap.remove(it.key)
            }
            messages.filterNot { notificationMap.containsKey(it) }.forEach { message ->
                UILocalNotification().apply {
                    alertTitle = message.text
                    alertBody = message.subText
                    category = DeviceInformation().appName
                    notificationMap.put(message, this)
                    presentLocalNotificationNow(this)
                }
            }
        }
    }
}
