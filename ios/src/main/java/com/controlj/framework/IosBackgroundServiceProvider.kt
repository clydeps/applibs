package com.controlj.framework

import com.controlj.data.Progress
import com.controlj.location.LocationProvider
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.observeBy
import com.controlj.utility.FormRequest
import com.controlj.view.AppPermission
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Single
import libcore.io.Libcore
import org.robovm.apple.backgroundtasks.BGProcessingTaskRequest
import org.robovm.apple.backgroundtasks.BGTaskScheduler
import org.robovm.apple.corebluetooth.CBCentralManager
import org.robovm.apple.corebluetooth.CBManager
import org.robovm.apple.corebluetooth.CBManagerAuthorization
import org.robovm.apple.corebluetooth.CBManagerState
import org.robovm.apple.dispatch.DispatchQueue
import org.robovm.apple.foundation.NSBundle
import org.robovm.apple.foundation.NSDate
import org.robovm.apple.foundation.NSError
import org.robovm.apple.foundation.NSMutableURLRequest
import org.robovm.apple.foundation.NSOperatingSystemVersion
import org.robovm.apple.foundation.NSOperationQueue
import org.robovm.apple.foundation.NSProcessInfo
import org.robovm.apple.foundation.NSURL
import org.robovm.apple.foundation.NSURLRequestCachePolicy
import org.robovm.apple.foundation.NSURLSession
import org.robovm.apple.foundation.NSURLSessionConfiguration
import org.robovm.apple.foundation.NSURLSessionTaskState
import org.robovm.apple.network.NWPathMonitor
import org.robovm.apple.network.NWPathStatus
import org.robovm.apple.uikit.UIApplication
import org.robovm.apple.uikit.UIDevice
import org.threeten.bp.DateTimeUtils
import org.threeten.bp.Instant
import java.util.concurrent.ConcurrentSkipListMap

object IosBackgroundServiceProvider : BackgroundServiceProvider, DeviceInformation {

    private const val TIMEOUT_SECS = 60.0

    private val sessionMap = ConcurrentSkipListMap<String, NSURLSession>()

    fun NSURLSession.didComplete() {
        sessionMap.remove(configuration.identifier)
        invalidateAndCancel()
    }

    private fun getSession(
        identifier: String,
        completionHandler: Runnable? = null
    ): NSURLSession {
        return sessionMap.getOrPut(identifier) {
            logMsg("Creating session $identifier")
            NSURLSession(
                NSURLSessionConfiguration.getBackgroundSessionConfiguration(identifier).apply {
                    if (isIos11)
                        isWaitsForConnectivity = true
                },
                SessionDelegate(identifier, completionHandler),
                NSOperationQueue.getMainQueue()
            )
        }
    }

    private val is13Point1 by lazy { isOSVersionOrLater(13, 1) }

    private val isBluetoothEnabled: Boolean
        get() {
            if (is13Point1)
                return CBManager.authorization() == CBManagerAuthorization.AllowedAlways
            if (isIos13) return CBCentralManager().state != CBManagerState.Unauthorized
            return true
        }

    override fun isPermitted(appPermission: AppPermission): Single<Boolean> {
        return when (appPermission) {
            AppPermission.BLUETOOTH -> Single.just(isBluetoothEnabled)
            AppPermission.LOCATION_ALWAYS -> IosDeviceLocationProvider.checkPermission()
                .map { it == LocationProvider.AuthorisationStatus.Always }
            AppPermission.LOCATION_IN_USE -> IosDeviceLocationProvider.checkPermission()
                .map { it >= LocationProvider.AuthorisationStatus.WhileInUse }
        }
    }

    override fun postRequest(postData: FormRequest, identifier: String, emitter: ObservableEmitter<Progress>?) {
        val urlRequest = NSMutableURLRequest(
            NSURL(postData.url),
            NSURLRequestCachePolicy.ReloadIgnoringCacheData,
            TIMEOUT_SECS
        )
        urlRequest.httpMethod = postData.method
        postData.headers.forEach { urlRequest.addHTTPHeaderField(it.key, it.value) }
        val file = postData.fileData()
        getSession(identifier).apply {
            newUploadTask(urlRequest, NSURL(file)).apply {
                taskDescription = identifier
                if (emitter != null)
                    addTask(this, emitter)
                resume()
            }
        }
        file.delete()
        logMsg("Started postRequest $identifier for url ${urlRequest.url}")
    }

    override val batteryLevel: Double?
        get() {
            val level = UIDevice.getCurrentDevice().batteryLevel
            if (level < 0f)
                return null
            return level.toDouble()
        }


    /**
     * resume a session
     */

    fun resumeSession(identifier: String, completionHandler: Runnable?) {
        getSession(identifier, completionHandler = completionHandler)
    }

    // Ios implementation of a task token.
    data class IosTaskToken(val data: Long) : BackgroundServiceProvider.TaskToken

    override fun startBackgroundTask(identifier: String): IosTaskToken? {
        val token = UIApplication.getSharedApplication().beginBackgroundTask(identifier) {
            logMsg("Background task $identifier expired before completion")
        }
        if (token == UIApplication.getInvalidBackgroundTask())
            return null
        return IosTaskToken(token)
    }

    override fun endBackgroundTask(token: BackgroundServiceProvider.TaskToken) {
        token as IosTaskToken
        UIApplication.getSharedApplication().endBackgroundTask(token.data)
    }

    /**
     * Get the status of a session. It is assumed there is only one task in the session.
     */

    override fun getStatus(identifier: String): Single<TaskStatus> {
        return Single.create { emitter ->
            val session = NSURLSession(
                NSURLSessionConfiguration.getBackgroundSessionConfiguration(identifier),
                null,
                NSOperationQueue.getMainQueue()
            )
            session.getAllTasks { tasks ->
                val task = tasks.firstOrNull()
                emitter.onSuccess(
                    when (task?.state) {
                        NSURLSessionTaskState.Completed -> {
                            if (task.error == null) TaskStatus.Failed else TaskStatus.Completed
                        }
                        NSURLSessionTaskState.Running -> TaskStatus.Running
                        NSURLSessionTaskState.Canceling -> TaskStatus.Failed
                        else -> TaskStatus.Unknown
                    }
                )
            }
        }
    }

    /**
     * register to execute the task corresponding to the given identifier.
     * Must be called once from didFinishLaunching() in the Application class.
     * @param action The task identifier - should be the name of a class implementing ProcessWorker
     */

    fun registerTask(action: String) {
        BGTaskScheduler.getSharedScheduler().registerForTask(action, null) { task ->
            logMsg("Processing task $action started")
            try {
                ProcessWorker.runProcess(action)
                    .observeBy(
                        onError = {
                            logMsg("Processing task $action failed: $it")
                            task.setTaskCompleted(false)
                        }) {
                        task.setTaskCompleted(true)
                    }
            } catch (ex: Exception) {
                logMsg("Processing task $action failed: $ex")
                task.setTaskCompleted(false)
            }
        }
    }

    override fun scheduleProcessingTask(
        action: String,
        atTime: Instant,
        requiresNetwork: Boolean,
        longRunning: Boolean
    ): Boolean {
        if(!isIos13)
            return false
        val request = BGProcessingTaskRequest(action)
        if (requiresNetwork)
            request.requiresNetworkConnectivity()
        request.earliestBeginDate = NSDate(DateTimeUtils.toDate(atTime))

        val error = NSError.NSErrorPtr()
        // register the task if not already done.
        BGTaskScheduler.getSharedScheduler().submitTaskRequest(request, error)
        error.get()?.let {
            throw IllegalArgumentException("Could not schedule $action: $it")
        }
        logMsg("Scheduled $action to run at: $atTime")
        return true
    }

    override fun cancelProcessingTask(action: String) {
        BGTaskScheduler.getSharedScheduler().cancelTaskRequest(action)
    }

    fun isOSVersionOrLater(major: Int, minor: Int = 0): Boolean {
        return NSProcessInfo.getSharedProcessInfo()
            .isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(major.toLong(), minor.toLong(), 0))
    }

    val isIos15 by lazy { isOSVersionOrLater(15) }
    val isIos14 by lazy { isOSVersionOrLater(14) }
    val isIos13 by lazy { isOSVersionOrLater(13) }
    val isIos12 by lazy { isOSVersionOrLater(12) }
    val isIos11 by lazy { isOSVersionOrLater(11) }
    val isIos10 by lazy { isOSVersionOrLater(10) }
    val isSimulator: Boolean by lazy {
        NSProcessInfo.getSharedProcessInfo().environment.containsKey("SIMULATOR_DEVICE_NAME")
    }
    override val appBuild: Int
        get() = build
    override val appVersion: String
        get() = version
    override val appName: String by lazy {
        UIApplication.getSharedApplication().delegate.let {
            if (it is IosBaseApp)
                it.identifier
            else
                it.javaClass.simpleName
        }
    }
    override val deviceName: String by lazy {
        if (isSimulator) NSProcessInfo.getSharedProcessInfo().environment["SIMULATOR_DEVICE_NAME"].toString()
        else UIDevice.getCurrentDevice().identifierForVendor.asString().takeLast(8)
    }

    val model: IosDevice by lazy {
        IosDevice.lookup(Libcore.os.uname().machine)
    }
    val isDebugBuild: Boolean by lazy {
        isSimulator || NSBundle.getMainBundle().findResourcePath("embedded", "mobileprovision") != null
    }
    val build: Int by lazy {
        (NSBundle.getMainBundle().infoDictionary["CFBundleVersion"] ?: "??").toString().toInt()
    }
    val version: String by lazy {
        (NSBundle.getMainBundle().infoDictionary["CFBundleShortVersionString"] ?: "??.??").toString()
    }

    override val networkObserver: Observable<Boolean> = if (isIos12)
        Observable.create { emitter ->
            try {
                val monitor = NWPathMonitor()
                monitor.setQueue(DispatchQueue.getMainQueue())
                monitor.setUpdateHandler { path ->
                    emitter.onNext(path.status == NWPathStatus.satisfied)
                    logMsg("network path.status = ${path.status}")
                }
                emitter.setCancellable { monitor.cancel() }
                monitor.start()
            } catch (ex: Throwable) {
                logException(ex)
            }
        } else super.networkObserver

    override fun init() {
        super<BackgroundServiceProvider>.init()
        super<DeviceInformation>.init()
    }

    init {
        init()
        BackgroundServiceProvider.instance = this
    }
}



