package com.controlj.ios.graphics

import com.controlj.graphics.CRect
import com.controlj.graphics.Path

import org.robovm.apple.coregraphics.CGAffineTransform
import org.robovm.apple.coregraphics.CGMutablePath
import org.robovm.apple.coregraphics.CGRect

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 26/1/17
 * Time: 10:19 PM
 */
class IOSPath : Path {
    var path = CGMutablePath.createMutable()

    override fun isEmpty(): Boolean {
        return path.isEmpty
    }
    override fun moveTo(x: Double, y: Double) {
        path.moveToPoint(null, x, y)
    }

    override fun lineTo(x: Double, y: Double) {
        path.addLineToPoint(null, x, y)
    }

    override fun addRoundedRect(bounds: CRect, xRadius: Double, yRadius: Double) {
        path.addRoundedRect(CGAffineTransform.Identity(), CGRect(bounds.left, bounds.top, bounds.width, bounds.height), xRadius * 2, yRadius * 2)
    }

    override fun addArc(x: Double, y: Double, radius: Double, startAngle: Double, sweepAngle: Double) {
        path.addArc(CGAffineTransform.Identity(), x, y, radius, Math.toRadians(startAngle), Math.toRadians(startAngle + sweepAngle), sweepAngle < 0)
    }

    override fun closePath() {
        path.closeSubpath()
    }

    override fun clear() {
        path = CGMutablePath.createMutable()
    }
}
