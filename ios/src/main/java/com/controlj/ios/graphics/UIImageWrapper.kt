package com.controlj.ios.graphics

import com.controlj.graphics.CImage
import com.controlj.ios.uIImage
import org.robovm.apple.coregraphics.CGImage
import org.robovm.apple.uikit.UIImage

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 27/1/17
 * Time: 4:55 PM
 */
class UIImageWrapper(val uiImage: UIImage) : CImage {

    constructor(image: CGImage): this(UIImage(image))

    constructor(filename: String) : this(filename.uIImage)

}
