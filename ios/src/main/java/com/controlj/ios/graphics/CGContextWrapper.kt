package com.controlj.ios.graphics

import com.controlj.graphics.CImage
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.Path
import com.controlj.graphics.Point
import com.controlj.graphics.TextStyle
import com.controlj.ios.view.toCGRect
import org.robovm.apple.coregraphics.CGBitmapContext
import org.robovm.apple.coregraphics.CGBlendMode
import org.robovm.apple.coregraphics.CGContext
import org.robovm.apple.coregraphics.CGLineCap
import org.robovm.apple.coregraphics.CGPathDrawingMode
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIGraphics

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 25/1/17
 * Time: 12:15 PM
 */
class CGContextWrapper : GraphicsContext {

    private var context: CGContext? = null
    private val colorMap = HashMap<Int, UIColor>()

    fun setContext(context: CGContext) {
        this.context = context
    }

    private fun getColor(argb: Int): UIColor {
        return colorMap.getOrPut(argb) { colorFromArgb(argb) }
    }

    override val bitmap: CImage
        get() = UIImageWrapper((context as CGBitmapContext).toImage())

    override var fillColor: Int = 0
        set(value) {
            field = value
            context?.setFillColor(getColor(value).cgColor)
        }

    override var strokeColor: Int = 0
        set(value) {
            field = value
            context?.setStrokeColor(getColor(value).cgColor)
        }

    override fun fillRect(bounds: CRect) {
        context?.fillRect(bounds.toCGRect())
    }

    override fun strokeRect(bounds: CRect) {
        context?.strokeRect(bounds.toCGRect())
    }

    override fun saveState() {
        context?.saveGState()
    }

    override fun restoreState() {
        context?.restoreGState()
    }

    override var lineWidth: Double = 1.0
        set(value) {
            field = value
            context?.setLineWidth(value)
        }

    override var lineCap: GraphicsContext.LineCap = GraphicsContext.LineCap.butt
        set(value) {
            field = value
            context?.setLineCap(
                when (value) {
                    GraphicsContext.LineCap.butt -> CGLineCap.Butt
                    GraphicsContext.LineCap.square -> CGLineCap.Square
                    GraphicsContext.LineCap.round -> CGLineCap.Round
                }
            )
        }

    override fun drawArc(x: Double, y: Double, radius: Double, startAngle: Double, endAngle: Double) {
        context?.addArc(x, y, radius, Math.toRadians(startAngle), Math.toRadians(endAngle), 0)
        context?.drawPath(CGPathDrawingMode.Stroke)
    }

    override fun drawArc(center: Point, radius: Double, startAngle: Double, endAngle: Double) {
        drawArc(center.x.toDouble(), center.y.toDouble(), radius, startAngle, endAngle)
    }

    override fun rotateBy(degrees: Double) {
        context?.rotateCTM(Math.toRadians(degrees))
    }

    override fun drawLine(x1: Double, y1: Double, x2: Double, y2: Double) {
        context?.moveToPoint(x1, y1)
        context?.addLineToPoint(x2, y2)
        context?.strokePath()
    }

    override fun drawText(text: String, bounds: CRect, style: TextStyle, color: Int) {
        val iosTextStyle = IosTextStyle.getTextStyle(style)
        val nsString = IOSGraphicsFactory().getNSString(text)
        val textHeight = iosTextStyle.height
        val attrs = iosTextStyle.attrs
        attrs.foregroundColor = getColor(color)
        UIGraphics.pushContext(context)
        nsString.draw(
            CGRect(
                bounds.left, bounds.top + (bounds.height - textHeight) / 2,
                bounds.width, textHeight
            ), attrs
        )
        UIGraphics.popContext()
    }

    override fun clip(bounds: CRect) {
        context?.clipToRect(bounds.toCGRect())
    }

    override fun fillEllipse(bounds: CRect) {
        context?.fillEllipseInRect(bounds.toCGRect())
    }

    override fun scaleBy(x: Double, y: Double) {
        context?.scaleCTM(x, y)
    }

    override fun translateBy(x: Double, y: Double) {
        context?.translateCTM(x, y)
    }

    override fun fillPath(path: Path) {
        context?.addPath((path as IOSPath).path)
        context?.drawPath(CGPathDrawingMode.Fill)
    }

    override fun drawImage(image: CImage, bounds: CRect, tintColor: Int?) {
        if (image is UIImageWrapper)
            context?.let { ctx ->
                val imageRect = bounds.toCGRect()
                UIGraphics.pushContext(context)
                image.uiImage.draw(imageRect)
                tintColor?.let {
                    fillColor = it
                    ctx.setBlendMode(CGBlendMode.Color)
                    ctx.fillRect(imageRect)
                }
                UIGraphics.popContext()
            }
    }

    override fun strokePath(path: Path) {
        context?.addPath((path as IOSPath).path)
        context?.drawPath(CGPathDrawingMode.Stroke)
    }

    companion object {

        fun colorFromArgb(argb: Int): UIColor {
            return UIColor(
                (argb shr 16 and 0xFF) / 255.0,
                (argb shr 8 and 0xFF) / 255.0,
                (argb and 0xFF) / 255.0,
                (argb shr 24 and 0xFF) / 255.0
            )
        }
    }
}

