package com.controlj.ios.graphics;

/**
 * Created by clyde on 17/5/17.
 * <p>
 * // calculate an arc tangential to the lines between points 1 and 2, and 2 and 3
 */

public class TangentArc {
    public TangentArc(float x1, float y1, float x2, float y2, float x3, float y3) {
        double a = Math.tan(Math.atan2(y2 - y1, x2 - x1) + Math.PI / 4);
        double b = Math.tan(Math.atan2(y3 - y2, x3 - x2) + Math.PI / 4);
        double c = y1 - x1 * a;
        double d = y3 - x3 * b;
        double xr = (d - c) / (a - b);
        double yr = a * (d - c) / (a - b) + c;
    }
}
