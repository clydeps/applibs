package com.controlj.ios.graphics

import com.controlj.graphics.CImage
import com.controlj.graphics.CRect
import com.controlj.graphics.CSize
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.GraphicsFactory
import com.controlj.graphics.Path
import com.controlj.graphics.TextStyle
import com.controlj.ios.graphics.IosTextStyle.Companion.getTextStyle
import com.controlj.ios.uIImage
import org.robovm.apple.coregraphics.CGBitmapContext
import org.robovm.apple.coregraphics.CGColorSpace
import org.robovm.apple.coregraphics.CGImageAlphaInfo
import org.robovm.apple.foundation.NSString
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIImageRenderingMode
import org.robovm.apple.uikit.UIScreen
import java.util.HashMap

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 26/1/17
 * Time: 10:20 PM
 */
object IOSGraphicsFactory : GraphicsFactory {

    operator fun invoke(): IOSGraphicsFactory {
        return this
    }

    private val strings = HashMap<String, NSString>()
    override val textFactor: Double = 0.7

    override val screenBounds: CRect
        get() {
            val screenBounds = UIScreen.getMainScreen().bounds
            return CRect(
                screenBounds.x,
                screenBounds.y,
                screenBounds.x + screenBounds.width,
                screenBounds.y + screenBounds.height
            )
        }

    init {
        init()
    }

    fun getNSString(text: String): NSString {
        if (strings.containsKey(text))
            return strings[text]!!
        val nsString = NSString(text)
        strings[text] = nsString
        return nsString
    }

    override fun createPath(): Path {
        return IOSPath()
    }

    override fun getImageBuffer(width: Int, height: Int): GraphicsContext {
        val context = CGBitmapContext.create(
            width.toLong(),
            height.toLong(),
            8,
            0,
            CGColorSpace.createDeviceRGB(),
            CGImageAlphaInfo.PremultipliedFirst
        )
        val gc = CGContextWrapper()
        gc.setContext(context)
        return gc
    }

    override fun getTextSize(text: String, style: TextStyle): CSize {
        val iosTextStyle = getTextStyle(style)
        val nsString = getNSString(text)
        val box = nsString.getSize(iosTextStyle.attrs)
        return CSize(box.width, box.height)
    }

    override fun getTextBounds(text: String, style: TextStyle): CRect {
        val box = getTextSize(text, style)
        return CRect(0.0, 0.0, box.width, box.height)
    }

    override fun pointsToPixels(points: Double): Int {
        return Math.ceil(points * textFactor).toInt()
    }

    override fun loadImage(filename: String, isTemplate: Boolean): CImage {
        val image = filename.uIImage
        if (isTemplate)
            return UIImageWrapper(image.newImage(UIImageRenderingMode.AlwaysTemplate))
        return UIImageWrapper(image)
    }

    fun colorFromArgb(argb: Int): UIColor {
        return UIColor(
            (argb shr 16 and 0xFF) / 255.0,
            (argb shr 8 and 0xFF) / 255.0,
            (argb and 0xFF) / 255.0,
            (argb shr 24 and 0xFF) / 255.0
        )
    }
}
