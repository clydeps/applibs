package com.controlj.ios.graphics

import com.controlj.graphics.TextStyle
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.coregraphics.CGSize
import org.robovm.apple.foundation.NSAttributedString
import org.robovm.apple.uikit.*
import java.util.*

/**
 * Created by clyde on 11/5/17.
 */

class IosTextStyle private constructor(style: TextStyle) : TextStyle(style.fontFamily, style.fontStyle, style.alignment, style.size) {
    var attrs = NSAttributedStringAttributes()
    var height: Double = 0.0
    /**
     * Get a typeface from the supplied style. Cache typefaces and return cached versions
     * where possible.
     *
     * @return a Typeface corresponding to the supplied style
     */
    //Foundation.log("getUIFont, style = " + style.toString());
    val uiFont: UIFont
        get() {
            return typeFaces.getOrPut(this) {
                faceNames[fontFamily]?.let { facename: String ->
                    UIFont.getFont(when (fontStyle) {
                        TextStyle.FontStyle.Bold -> "$facename-Bold"
                        TextStyle.FontStyle.Italic -> "$facename-Italic"
                        TextStyle.FontStyle.BoldItalic -> "$facename-BoldItalic"
                        else -> facename
                    }, size)
                } ?: when (fontStyle) {
                    TextStyle.FontStyle.Bold ->
                        UIFont.getBoldSystemFont(size)
                    TextStyle.FontStyle.Italic, TextStyle.FontStyle.BoldItalic ->
                        UIFont.getItalicSystemFont(size)
                    else ->
                        UIFont.getSystemFont(size)
                }
            }
        }

    fun getTextBoundingRect(width: Double, height: Double, text: String): CGRect {
        val string = NSAttributedString(text, NSAttributedStringAttributes().setFont(uiFont))
        return string.getBoundingRect(CGSize(width, height),
                NSStringDrawingOptions.with(NSStringDrawingOptions.UsesLineFragmentOrigin,
                        NSStringDrawingOptions.UsesFontLeading), null)

    }

    companion object {
        private val typeFaces = HashMap<TextStyle, UIFont>()
        private val faceNames = HashMap<TextStyle.FontFamily, String>()
        private val attrmap = HashMap<TextStyle, IosTextStyle>()
        private val left: NSMutableParagraphStyle = NSMutableParagraphStyle()
        private val center: NSMutableParagraphStyle = NSMutableParagraphStyle()
        private val right: NSMutableParagraphStyle = NSMutableParagraphStyle()

        init {
            faceNames[TextStyle.FontFamily.Monospaced] = "Menlo"
            faceNames[TextStyle.FontFamily.SansSerif] = "HelveticaNeue"
            faceNames[TextStyle.FontFamily.Serif] = "Palatino"
            left.alignment = NSTextAlignment.Left
            center.alignment = NSTextAlignment.Center
            right.alignment = NSTextAlignment.Right
        }

        fun getTextStyle(style: TextStyle): IosTextStyle {
            return attrmap.getOrPut(style) {
                val iosTextStyle = IosTextStyle(style)
                val attrs = iosTextStyle.attrs
                val font = iosTextStyle.uiFont
                attrs.font = font
                iosTextStyle.height = (font.ascender - font.descender)

                val paraStyle: NSMutableParagraphStyle = when (style.alignment) {
                    TextStyle.Alignment.Center -> center
                    TextStyle.Alignment.Right -> right
                    else -> left
                }
                attrs.paragraphStyle = paraStyle
                iosTextStyle
            }
        }
    }
}
