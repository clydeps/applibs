package com.controlj.ios

import com.controlj.shim.CxRect
import org.robovm.apple.coregraphics.CGPoint
import org.robovm.apple.foundation.NSArray
import org.robovm.apple.foundation.NSDictionary
import org.robovm.apple.foundation.NSIndexPath
import org.robovm.apple.foundation.NSObject
import org.robovm.apple.foundation.NSString
import org.robovm.apple.uikit.UIGestureRecognizer
import org.robovm.apple.uikit.UIImage
import org.robovm.apple.uikit.UIImageView
import org.robovm.apple.uikit.UITapGestureRecognizer
import org.robovm.apple.uikit.UITextField
import org.robovm.apple.uikit.UIView
import org.robovm.objc.Selector

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-04-01
 * Time: 19:22
 */
/**
 * Select all the text in a text field.
 */
fun UITextField.selectAll() {
    performSelector(Selector.register("selectAll"), null, 0.0)
}

fun Int.indexPathArray(count: Int): NSArray<NSIndexPath> {
    return NSArray(List<NSIndexPath>(count) { idx -> NSIndexPath.row((idx + this).toLong(), 0L) })
}

fun Iterable<NSIndexPath>.string(): String {
    return this.joinToString(",", "[", "]") { it.row.toString() }
}

fun Iterable<Pair<NSIndexPath, NSIndexPath>>.pairString(): String {
    return this.joinToString(",", "[", "]") { "${it.first}->${it.second}" }
}

inline fun <reified V : NSObject> Map<String, V>.toDictionary(): NSDictionary<NSString, V> {
    return NSDictionary(this.map { NSString(it.key) to it.value }.toMap())
}

fun Array<String>.toNSArray(): NSArray<NSString> {
    return NSArray.fromStrings(*this)
}

fun List<String>.toNSArray(): NSArray<NSString> {
    return NSArray.fromStrings(*this.toTypedArray())
}

fun UIView.addTapListener(listener: (UIGestureRecognizer) -> Unit) {
    isUserInteractionEnabled = true
    addGestureRecognizer(UITapGestureRecognizer(listener))
}

fun CxRect.contains(position: CGPoint): Boolean {
    return position.x >= minX && position.x < maxX && position.y >= minY && position.y < maxY
}

val String.uIImage: UIImage
    get() = UIImage.getImage(this) ?: error("image $this not found")

fun UIImageView(name: String): UIImageView = UIImageView(name.uIImage)
