package com.controlj.ios

import com.controlj.framework.IosBackgroundServiceProvider.isIos13
import com.controlj.framework.SystemColors
import com.controlj.graphics.CColor
import org.robovm.apple.uikit.UIColor
import kotlin.reflect.KProperty

object ColorCompatibility {

    private class SystemColor(
        val red: Double,
        val green: Double,
        val blue: Double,
        val alpha: Double,
        val system: () -> UIColor?
    ) {
        operator fun getValue(thisRef: Any?, property: KProperty<*>): UIColor {
            return (if (isIos13) system() else null) ?: UIColor(red, green, blue, alpha)
        }
    }

    val label: UIColor by SystemColor(
        0.0,
        0.0,
        0.0,
        1.0
    ) { UIColor.label() }
    val secondaryLabel: UIColor by SystemColor(
        0.23529411764705882,
        0.23529411764705882,
        0.2627450980392157,
        0.6
    ) { UIColor.secondaryLabel() }
    val tertiaryLabel: UIColor by SystemColor(
        0.23529411764705882,
        0.23529411764705882,
        0.2627450980392157,
        0.3
    ) { UIColor.tertiaryLabel() }
    val quaternaryLabel: UIColor by SystemColor(
        0.23529411764705882,
        0.23529411764705882,
        0.2627450980392157,
        0.18
    ) { UIColor.quaternaryLabel() }
    val systemFill: UIColor by SystemColor(
        0.47058823529411764,
        0.47058823529411764,
        0.5019607843137255,
        0.2
    ) { UIColor.systemFill() }
    val secondarySystemFill: UIColor by SystemColor(
        0.47058823529411764,
        0.47058823529411764,
        0.5019607843137255,
        0.16
    ) { UIColor.secondarySystemFill() }
    val tertiarySystemFill: UIColor by SystemColor(
        0.4627450980392157,
        0.4627450980392157,
        0.5019607843137255,
        0.12
    ) { UIColor.tertiarySystemFill() }
    val quaternarySystemFill: UIColor by SystemColor(
        0.4549019607843137,
        0.4549019607843137,
        0.5019607843137255,
        0.08
    ) { UIColor.quaternarySystemFill() }
    val placeholderText: UIColor by SystemColor(
        0.23529411764705882,
        0.23529411764705882,
        0.2627450980392157,
        0.3
    ) { UIColor.placeholderText() }
    val systemBackground: UIColor
        get() {
            return (if (isIos13) {
                UIColor.systemBackground()
            } else
                null) ?: UIColor(1.0, 1.0, 1.0, 1.0)
        }
    val secondarySystemBackground: UIColor
        by SystemColor(
            0.9490196078431372,
            0.9490196078431372,
            0.9686274509803922,
            1.0
        ) { UIColor.secondarySystemBackground() }
    val tertiarySystemBackground: UIColor
        by SystemColor(1.0, 1.0, 1.0, 1.0) { UIColor.tertiarySystemBackground() }
    val systemGroupedBackground: UIColor
        by SystemColor(
            0.9490196078431372,
            0.9490196078431372,
            0.9686274509803922,
            1.0
        ) { UIColor.systemGroupedBackground() }
    val secondarySystemGroupedBackground: UIColor
        by SystemColor(1.0, 1.0, 1.0, 1.0) { UIColor.secondarySystemGroupedBackground() }
    val tertiarySystemGroupedBackground: UIColor
        by SystemColor(
            0.9490196078431372,
            0.9490196078431372,
            0.9686274509803922,
            1.0
        ) { UIColor.tertiarySystemGroupedBackground() }
    val separator: UIColor
        by SystemColor(0.23529411764705882, 0.23529411764705882, 0.2627450980392157, 0.29) { UIColor.separator() }
    val opaqueSeparator: UIColor
        by SystemColor(0.7764705882352941, 0.7764705882352941, 0.7843137254901961, 1.0) { UIColor.opaqueSeparator() }
    val link: UIColor
        by SystemColor(0.0, 0.47843137254901963, 1.0, 1.0) { UIColor.link() }
    val systemIndigo: UIColor
        by SystemColor(0.34509803921568627, 0.33725490196078434, 0.8392156862745098, 1.0) { UIColor.systemIndigo() }
    val systemGray2: UIColor
        by SystemColor(0.6823529411764706, 0.6823529411764706, 0.6980392156862745, 1.0) { UIColor.systemGray2() }
    val systemGray3: UIColor
        by SystemColor(0.7803921568627451, 0.7803921568627451, 0.8, 1.0) { UIColor.systemGray3() }
    val systemGray4: UIColor
        by SystemColor(0.8196078431372549, 0.8196078431372549, 0.8392156862745098, 1.0) { UIColor.systemGray4() }
    val systemGray5: UIColor
        by SystemColor(0.8980392156862745, 0.8980392156862745, 0.9176470588235294, 1.0) { UIColor.systemGray5() }
    val systemGray6: UIColor
        by SystemColor(0.9490196078431372, 0.9490196078431372, 0.9686274509803922, 1.0) { UIColor.systemGray6() }
}

object IosSystemColors : SystemColors {
    init {
        init()
    }

    override val label get() = ColorCompatibility.label.ccolor

    override val secondaryLabel get() = ColorCompatibility.secondaryLabel.ccolor

    override val systemFill get() = ColorCompatibility.systemFill.ccolor

    override val secondarySystemFill get() = ColorCompatibility.secondarySystemFill.ccolor

    override val placeholderText get() = ColorCompatibility.placeholderText.ccolor

    override val systemBackground get() = ColorCompatibility.systemBackground.ccolor

    override val secondarySystemBackground get() = ColorCompatibility.secondarySystemBackground.ccolor

    override val systemGroupedBackground get() = ColorCompatibility.systemGroupedBackground.ccolor

    override val secondarySystemGroupedBackground get() = ColorCompatibility.secondarySystemGroupedBackground.ccolor

    override val separator get() = ColorCompatibility.separator.ccolor

    override val opaqueSeparator get() = ColorCompatibility.opaqueSeparator.ccolor

    override val link get() = ColorCompatibility.link.ccolor

    override val systemGray2 get() = ColorCompatibility.systemGray2.ccolor

    override val systemGray3 get() = ColorCompatibility.systemGray3.ccolor

    override val systemGray4 get() = ColorCompatibility.systemGray4.ccolor

    override val systemGray5 get() = ColorCompatibility.systemGray5.ccolor

    override val systemGray6 get() = ColorCompatibility.systemGray6.ccolor

    val UIColor.ccolor: Int
        get() = CColor.argb(rgba[3], rgba[0], rgba[1], rgba[2])
}

