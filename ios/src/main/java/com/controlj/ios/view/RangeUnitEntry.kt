package com.controlj.ios.view

import com.controlj.ios.ColorCompatibility
import com.controlj.layout.Gravity
import com.controlj.layout.HorizontalGroup
import com.controlj.layout.Layout
import com.controlj.layout.VerticalGroup
import com.controlj.rx.MainScheduler
import com.controlj.settings.RangeSet
import com.controlj.settings.RangeState
import com.controlj.shim.addView
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.ui.RangeUnitDialogItem
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UILabel
import org.robovm.apple.uikit.UITextField
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-04-20
 * Time: 13:57
 */
class RangeUnitEntry(private val rangeItem: RangeUnitDialogItem, dialogData: DialogData) : LabelledEntry(rangeItem, dialogData) {

    private val rangeGroups = rangeItem.rangeSet().ranges.mapIndexed { index, _ -> RangeGroup(index == 0) }
    private val rangeVert = VerticalGroup(Layout(widthMode = Layout.Mode.MatchParent))
    private val spinner: Spinner = Spinner(rangeItem.choices) {
        if (it != rangeItem.selected) {
            rangeItem.selected = it
            refresh()
        }
    }

    inner class RangeGroup(first: Boolean = false) : HorizontalGroup(Layout(widthMode = Layout.Mode.MatchParent)) {
        private val startField = UITextField()
        private val endField = UITextField()
        private val startDelegate = TextFieldDelegate(startField, DialogItem.InputStyle.SIGNEDDECIMAL)
        private val endDelegate = TextFieldDelegate(endField, DialogItem.InputStyle.SIGNEDDECIMAL)

        private val colorSpinner = object : Spinner(RangeState.values().toList(), { update() }) {
            override fun getDropdownBackgroundColor(row: Int): UIColor {
                return RangeState.values()[row].color.toUIColor()
            }

            override fun getDropdownSelectedBackgroundColor(row: Int): UIColor {
                return getDropdownBackgroundColor(row)
            }

            override var selected: Int
                get() = super.selected
                set(value) {
                    super.selected = value
                    label.backgroundColor = getDropdownBackgroundColor(value)
                }

            init {
                ddTextColor = UIColor.black()
            }

        }

        init {
            name = "RangeGroup"
            startField.backgroundColor = ColorCompatibility.secondarySystemBackground
            startField.textColor = ColorCompatibility.label
            startField.isUserInteractionEnabled = first
            endField.backgroundColor = startField.backgroundColor
            endField.textColor = ColorCompatibility.label
            endField.textAlignment = NSTextAlignment.Left
            val spacer = UILabel()
            spacer.text = " - "
            addView(startField, Layout(widthMode = Layout.Mode.Weighted, gravity = Gravity.MiddleRight))
            addView(spacer, Layout(gravity = Gravity.Center))
            addView(endField, Layout(widthMode = Layout.Mode.Weighted, gravity = Gravity.MiddleLeft))
            addView(colorSpinner, Layout(widthMode = Layout.Mode.Weighted, weight = 1.5))
            @Suppress("CheckResult")
            startDelegate.observe.mergeWith(endDelegate.observe)
                    .debounce(500, TimeUnit.MILLISECONDS, MainScheduler.instance)
                    .subscribe { update() }
        }

        /**
         * Check if this accurately mirrors the given rangeInfo
         */
        fun isSame(info: RangeSet.RangeInfo): Boolean {
            return colorSpinner.selected == info.colorIndex &&
                    startField.text.toDoubleOrNull() == info.start.toDoubleOrNull() &&
                    endField.text.toDoubleOrNull() == info.end.toDoubleOrNull()
        }

        /**
         * Get or Set the current data in a convenient package
         */
        var currentData: RangeSet.RangeInfo
            get() = RangeSet.RangeInfo(
                    startField.text,
                    endField.text,
                    RangeState.values()[colorSpinner.selected.coerceIn(colorSpinner.list.indices)]
            )
            set(info) {
                if (!isSame(info)) {
                    startField.text = info.start
                    endField.text = info.end
                    colorSpinner.selected = info.colorIndex
                }
            }
    }

    init {
        //logMsg("got list of %d choices", choices.size)
        widget = spinner
        spinner.list = rangeItem.choices
        //spinner.backgroundColor = ColorCompatibility.secondaryBackgroundColor
        spinner.font = item.textStyle.uiFont
        spinner.selected = rangeItem.selected
        rangeVert.addView(spinner, Layout(widthMode = Layout.Mode.MatchParent, gravity = Gravity.Center))
        rangeGroups.forEach { rangeVert.add(it) }
        contentGroup.add(rangeVert)
        refresh()
    }

    override fun refresh(): Boolean {
        var changed = super.refresh()
        if (spinner.isUserInteractionEnabled != item.isEnabled) {
            changed = true
            spinner.isUserInteractionEnabled = item.isEnabled
        }
        if (spinner.selected != rangeItem.selected) {
            changed = true
            spinner.selected = rangeItem.selected
        }
        val infos = rangeItem.rangeSet().rangeInfos
        rangeGroups.forEachIndexed { index, rangeGroup ->
            if (!rangeGroup.isSame(infos[index])) {
                changed = true
                rangeGroup.currentData = infos[index]
            }
        }
        return changed
    }

    fun update() {
        rangeItem.rangeSet().rangeInfos = rangeGroups.map { it.currentData }
        rangeItem.onChanged()
        refresh()
    }
}
