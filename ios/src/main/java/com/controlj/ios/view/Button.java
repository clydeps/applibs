package com.controlj.ios.view;

import org.robovm.apple.coreanimation.CALayer;
import org.robovm.apple.uikit.UIButton;
import org.robovm.apple.uikit.UIButtonType;
import org.robovm.apple.uikit.UIColor;
import org.robovm.apple.uikit.UIControlState;
import org.robovm.apple.uikit.UIEdgeInsets;
import org.robovm.apple.uikit.UIFont;
import org.robovm.apple.uikit.UIFontWeight;

/**
 * Created by clyde on 16/5/17.
 */

public class Button extends UIButton {
    public Button() {
        super(UIButtonType.RoundedRect);
        setBackgroundColor(UIColor.black());
        CALayer layer = getLayer();
        layer.setCornerRadius(8);
        layer.setBorderColor(UIColor.blue().getCGColor());
        layer.setBorderWidth(2);
        setTitleColor(UIColor.white(), UIControlState.Normal);
        setTitleColor(UIColor.gray(), UIControlState.Disabled);
        setTitleEdgeInsets(new UIEdgeInsets(-5, -10, -5, -10));
        getTitleLabel().setFont(UIFont.getSystemFont(20, UIFontWeight.Bold));

    }
}
