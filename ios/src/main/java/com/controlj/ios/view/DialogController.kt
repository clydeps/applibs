package com.controlj.ios.view

import com.controlj.framework.IosBaseApp
import com.controlj.graphics.CRect
import com.controlj.ios.contains
import com.controlj.layout.Gravity
import com.controlj.layout.Layout
import com.controlj.layout.UxScrollViewHost
import com.controlj.logging.CJLog.debug
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedSingleEmitter
import com.controlj.rx.MainScheduler
import com.controlj.shim.IosCxRect
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.ui.KeyboardStatus
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.disposables.Disposable
import org.robovm.apple.coregraphics.CGPoint
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.coregraphics.CGSize
import org.robovm.apple.foundation.NSSet
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIEvent
import org.robovm.apple.uikit.UIModalPresentationStyle
import org.robovm.apple.uikit.UITouch
import org.robovm.apple.uikit.UIView
import org.robovm.apple.uikit.UIViewAnimationOptions
import org.robovm.apple.uikit.UIViewController
import org.robovm.apple.uikit.UIViewControllerTransitionCoordinator
import java.util.Stack

/**
 * Created by clyde on 29/6/17.
 */

class DialogController(private val controller: UIViewController) : UIViewController() {

    internal var dialogs = Stack<DialogView>()
    private val scrollView = object : UxScrollViewHost(Layout(
            widthMode = Layout.Mode.WrapContent,
            heightMode = Layout.Mode.WrapContent,
            gravity = Gravity.Center
    )) {
        // touches outside the dialog frame will close the dialog, it it is of type Closeable
        override fun touchesBegan(touches: NSSet<UITouch>, event: UIEvent) {
            val touch = touches.first()
            dialogs.peek().dialogData.let { dialogData ->
                textPopup?.let { popup ->
                    val locn = touch.getLocationInView(popup)
                    if (!popup.bounds.contains(locn))
                        hideTextPopup()
                    return
                }
                if (dialogData.type.closeable &&
                        frameGroup.childViews.firstOrNull()?.frame?.contains(
                                touches.first().getLocationInView(this)) == false)
                    dialogData.dismiss()
            }
            super.touchesBegan(touches, event)
        }
    }

    @Suppress("DEPRECATION")
    private val screenSize
        get() = IosBaseApp.keyWindow.bounds.size

    private var disposable = Disposable.disposed()
    private var textPopup: EditTextPopup? = null
    val isShowing: Boolean
        get() = dialogs.isNotEmpty()

    init {
        scrollView.backgroundColor = UIColor(0.0, 0.0, 0.0, 0.5)
    }

    override fun viewWillTransitionToSize(cgSize: CGSize, uiViewControllerTransitionCoordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(cgSize, uiViewControllerTransitionCoordinator)
        uiViewControllerTransitionCoordinator.animateAlongsideTransition({
            setPosition(cgSize, 0.0)
        }) {}
    }


    private fun setPosition(size: CGSize, deltaHeight: Double) {
        val newSize = CGSize(size.width, size.height - deltaHeight)
        val newFrame = CGRect(CGPoint.Zero(), newSize)
        view.frame = newFrame
        scrollView.frame = view.bounds
        textPopup?.frame = view.bounds
        view.setNeedsDisplay()
    }

    private var didMoveView = false
    private fun keyboardChanged(kbHeight: Double) {
        val textfield = TextFieldDelegate.currentTextField
        scrollView.frameGroup.childViews.firstOrNull()?.let {
            if (kbHeight == 0.0 && didMoveView || it.frame.maxY > screenSize.height - kbHeight) {
                setPosition(screenSize, kbHeight)
                didMoveView = kbHeight != 0.0
                if (textfield != null && kbHeight != 0.0)
                    scrollView.scrollRectToVisible(textfield.frame, true)
            }
        }
    }

    override fun viewDidAppear(p0: Boolean) {
        super.viewDidAppear(p0)
        scrollView.scrollsToTop()
    }

    override fun viewWillAppear(animated: Boolean) {
        setPosition(screenSize, 0.0)
        super.viewWillAppear(animated)
        disposable.dispose()
        disposable = KeyboardStatus.observable.subscribe { state ->
            when (state) {
                KeyboardStatus.State.OPENING, KeyboardStatus.State.OPEN ->
                    keyboardChanged(KeyboardStatus.keyboardFrame.height)
                else -> keyboardChanged(0.0)
            }
        }
    }

    /**
     * Dismiss the presented view controller.
     */
    internal fun dismissController(animated: Boolean) {
        hideTextPopup()
        controller.dismissViewController(animated) { onDismissComplete() }
    }

    /**
     * Called when the view dismissal is complete
     */
    private fun onDismissComplete() {
        disposable.dispose()
        scrollView.removeFromSuperview()
        scrollView.frameGroup.removeAll()
        showTopDialog()
    }

    /**
     * Request showing the dialog. Push it onto the stack and request show.
     */
    fun show(dv: DialogView) {
        dialogs.push(dv)
        showTopDialog()
    }

    /**
     * Show the top dialog.
     * If there is already a dialog showing, dismiss it (it remains in the stack)
     */
    private fun showTopDialog() {
        while (dialogs.isNotEmpty() && !dialogs.peek().dialogData.isShowing) {
            val dv = dialogs.pop()
            logMsg("Refused to show ${dv.name} as it is disposed")
        }
        if (dialogs.isEmpty())
            return
        controller.presentedViewController?.let { subController ->
            when {
                subController.isBeingDismissed -> Unit      // just be patient.
                else -> dismissController(false)
            }
            return
        }
        val dv = dialogs.peek()
        scrollView.frameGroup.removeAll()
        scrollView.setCanCancelContentTouches(true)
        scrollView.frameGroup.add(dv)
        dv.dialogData.shower = Shower(dv)
        dv.redraw()
        view.addSubview(scrollView)
        view.setClipsToBounds(true)
        scrollView.setClipsToBounds(true)
        modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        controller.presentViewController(this, true, null)
    }

    private fun hideTextPopup() {
        textPopup?.done()       // removes from superview and executes callback
        textPopup = null
    }

    internal inner class Shower(private val dv: DialogView) : DialogData.DialogShower {
        private var emitter: SingleEmitter<String> = DisposedSingleEmitter()

        override fun showTextBox(
                title: String,
                value: String,
                style: DialogItem.InputStyle,
                source: CRect,
                timeout: Long
        ): Single<String> {
            return Single.create<String> {
                emitter = it
                val popup = EditTextPopup(title, value, style, timeout) { emitter.onSuccess(it) }
                textPopup = popup
                view.addSubview(popup)
                val offset = dv.frame.origin
                popup.frame = source.toCGRect().offset(offset.x, offset.y)
                UIView.animate(
                        0.25,
                        0.0,
                        UIViewAnimationOptions.CurveEaseOut,
                        {
                            popup.frame = (dv.frame as IosCxRect).cgRect
                        })
                {
                }
            }.subscribeOn(MainScheduler())
        }

        // refresh the dialog presentation
        override fun refresh(source: DialogItem?) {
            dv.redraw()
        }

        /**
         * External dismiss request. If it is the top dialog, dismiss the view and remove it.
         * If it's not the top dialog, just remove it from the stack.
         */
        override fun dismiss() {
            if (dialogs.isNotEmpty() && dv === dialogs.peek()) {
                dialogs.pop()
                debug("Dismissing ${dv.name}")
                dismissController(true)
            } else
                dialogs.remove(dv)
        }
    }
}
