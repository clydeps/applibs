package com.controlj.ios.view

import com.controlj.graphics.TextStyle
import com.controlj.ios.ColorCompatibility
import com.controlj.layout.Gravity
import com.controlj.layout.Layout
import com.controlj.layout.VerticalGroup
import com.controlj.shim.addView
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.widget.UILabelPadded
import org.robovm.apple.uikit.NSLineBreakMode
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIEdgeInsets

open class LabelledEntry(item: DialogItem, dialogData: DialogData) : BasicEntry(item, dialogData) {
    val label = UILabelPadded()
    val labelLayout: Layout
    val contentGroup = VerticalGroup(Layout(widthMode = Layout.Mode.Weighted)).apply { name = "contentGroup" }

    init {
        label.padding = UIEdgeInsets(PADDING, PADDING, PADDING, PADDING)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.WordWrapping
        label.font = item.textStyle.uiFont
        if (item.style.labelled && dialogData.labelWidth != 0.0) {
            labelLayout = Layout(
                    width = dialogData.labelWidth + PADDING * 3,
                    heightMode = Layout.Mode.WrapContent,
                    gravity = Gravity.MiddleRight
            )
            label.textAlignment = NSTextAlignment.Right
            addView(label, labelLayout)
            add(contentGroup)
        } else {
            labelLayout = Layout(widthMode = Layout.Mode.Weighted, gravity = Gravity.MiddleLeft)
            label.textAlignment = item.textStyle.alignment.asNSTextAlignment()
            addView(label, labelLayout)
        }
    }

    override var enabled: Boolean
        get() = super.enabled
        set(value) {
            super.enabled = value
            label.alpha = if (value) 1.0 else 0.4
        }

    override fun refresh(): Boolean {
        label.textColor = ColorCompatibility.label
        label.backgroundColor = ColorCompatibility.secondarySystemBackground
        var changed = super.refresh()
        if (label.text != item.text) {
            changed = true
            label.text = item.text
        }
        return changed
    }

    companion object {
        const val PADDING = 4.0
    }
}

private fun TextStyle.Alignment.asNSTextAlignment(): NSTextAlignment {
    return when (this) {
        TextStyle.Alignment.Right -> NSTextAlignment.Right
        TextStyle.Alignment.Center -> NSTextAlignment.Center
        else -> NSTextAlignment.Left
    }
}
