package com.controlj.ios.view

import com.controlj.ios.ColorCompatibility
import com.controlj.ios.uIImage
import com.controlj.layout.Gravity
import com.controlj.layout.Layout
import com.controlj.shim.addView
import com.controlj.ui.DialogData
import com.controlj.ui.TextValueItem
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIButton
import org.robovm.apple.uikit.UIButtonType
import org.robovm.apple.uikit.UIControlState
import org.robovm.apple.uikit.UIImageRenderingMode
import org.robovm.apple.uikit.UITextBorderStyle
import org.robovm.apple.uikit.UITextField
import org.robovm.apple.uikit.UITextView
import org.robovm.apple.uikit.UIViewContentMode

class TextValueEntry(item: TextValueItem, dialogData: DialogData) : LabelledEntry(item, dialogData) {
    val textField = UITextField()

    init {
        widget = textField
        val delegate = TextFieldDelegate(textField, item.inputStyle)
        textField.borderStyle = UITextBorderStyle.Bezel
        textField.backgroundColor = ColorCompatibility.systemBackground
        textField.textColor = ColorCompatibility.label
        textField.text = item.value
        textField.placeholder = item.hint
        textField.textAlignment = if (item.inputStyle.rightAlign)
            NSTextAlignment.Right
        else
            NSTextAlignment.Natural
        textField.font = item.textStyle.uiFont
        @Suppress("CheckResult")
        delegate.observe.filter { it == TextFieldDelegate.Event.ENDEDITING || it == TextFieldDelegate.Event.CHANGED }
            .subscribe {
                val text = textField.text
                if (text != item.value) {
                    item.value = textField.text
                    item.onChanged()
                    dialogData.refresh()
                }
            }
        if (!delegate.isPasswordVisible) {
            val ib = UIButton(UIButtonType.System)
            ib.setImage(eyeImage, UIControlState.Normal)
            ib.isUserInteractionEnabled = true
            ib.contentMode = UIViewContentMode.ScaleAspectFit
            ib.addOnTouchUpInsideListener { _, _ ->
                delegate.isPasswordVisible = !delegate.isPasswordVisible
            }
            contentGroup.add(horizontalGroup {
                layout.widthMode = Layout.Mode.MatchParent
                addView(textField, Layout(widthMode = Layout.Mode.MatchParent, gravity = Gravity.MiddleRight))
                addView(ib, Layout(margin = 4.0, gravity = Gravity.Center))
            })

        } else
            contentGroup.addView(textField, Layout(widthMode = Layout.Mode.MatchParent, gravity = Gravity.MiddleRight))
    }

    override fun refresh(): Boolean {
        var changed = super.refresh()
        if (textField.text != item.value) {
            changed = true
            textField.text = item.value
        }
        if (textField.isEnabled != item.isEnabled) {
            changed = true
            textField.isEnabled = item.isEnabled
        }
        return changed
    }

    companion object {
        val eyeImage by lazy { "eye".uIImage.newImage(UIImageRenderingMode.AlwaysTemplate) }
    }
}
