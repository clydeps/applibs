package com.controlj.ios.view

import com.controlj.ios.ColorCompatibility
import com.controlj.ios.uIImage
import com.controlj.layout.Gravity
import com.controlj.layout.HorizontalGroup
import com.controlj.layout.Layout
import com.controlj.shim.CxRect
import com.controlj.shim.IosCxColor
import com.controlj.shim.IosCxRect
import com.controlj.shim.UxColor
import com.controlj.shim.addView
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import org.robovm.apple.uikit.UIButton
import org.robovm.apple.uikit.UIButtonType
import org.robovm.apple.uikit.UIControlState
import org.robovm.apple.uikit.UIImage
import org.robovm.apple.uikit.UIImageRenderingMode
import org.robovm.apple.uikit.UIView
import org.robovm.apple.uikit.UIViewContentMode

open class BasicEntry(final override val item: DialogItem, final override val dialogData: DialogData) :
        HorizontalGroup(Layout.layout {
            widthMode = Layout.Mode.MatchParent
            margin = DialogEntry.marginSize
        }, item.style.name), DialogEntry {

    open var onClick: (() -> Unit)? = null
    var imageButton: UIButton? = null
    var widget: UIView? = null

    override var enabled: Boolean = true
        set(value) {
            field = value
            widget?.isUserInteractionEnabled = value
            widget?.alpha = if (value) 1.0 else 0.4
            imageButton?.isEnabled = value
        }

    override val rect: CxRect
        get() {
            imageButton?.let { return IosCxRect(it.frame) }
            widget?.let { return IosCxRect(it.frame) }
            return frame
        }

    init {
        spacing = DialogEntry.marginSize
        if (item.image.isNotBlank()) {
            val image = item.image.uIImage.newImage(UIImageRenderingMode.AlwaysTemplate)
            val ib = UIButton(UIButtonType.System)
            ib.setImage(image, UIControlState.Normal)
            ib.isUserInteractionEnabled = true
            ib.contentMode = UIViewContentMode.ScaleAspectFit
            imageButton = ib
            addView(ib, Layout(
                    width = dialogData.imageSize,
                    height = dialogData.imageSize,
                    gravity = Gravity.Center,
                    margin = 4.0
            ))
        }
    }

    override fun refresh(): Boolean {
        // update background color here in case dark mode has changed.
        layer.backgroundColor = IosCxColor(ColorCompatibility.systemBackground)
        if (enabled != item.isEnabled) {
            enabled = item.isEnabled
            return true
        }
        return false
    }
}
