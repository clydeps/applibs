package com.controlj.ios.view

import com.controlj.ios.ColorCompatibility
import com.controlj.ios.selectAll
import com.controlj.layout.Gravity
import com.controlj.layout.HorizontalGroup
import com.controlj.layout.IosUxHost
import com.controlj.layout.Layout
import com.controlj.layout.Layout.Companion.layout
import com.controlj.layout.VerticalGroup
import com.controlj.shim.IosCxColor
import com.controlj.shim.addView
import com.controlj.ui.DialogItem
import org.robovm.apple.coreanimation.CALayer
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIButton
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIControlState
import org.robovm.apple.uikit.UIFont
import org.robovm.apple.uikit.UILabel
import org.robovm.apple.uikit.UITextField
import org.robovm.apple.uikit.UITextFieldDidEndEditingReason
import org.robovm.apple.uikit.UIViewAutoresizing

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-03-22
 * Time: 12:47
 */
class EditTextPopup(
        title: String,
        text: String,
        style: DialogItem.InputStyle,
        timeout: Long = 0L,
        val callback: (String) -> Unit
) : IosUxHost() {

    private val button = UIButton()
    val textField = UITextField()
    val delegate = object : TextFieldDelegate(textField, style, timeout) {
        override fun didBeginEditing(textField: UITextField) {
            super.didBeginEditing(textField)
            textField.selectAll()
        }

        override fun didEndEditing(textField: UITextField, reason: UITextFieldDidEndEditingReason) {
            super.didEndEditing(textField, reason)
            val result = textField.text
            removeFromSuperview()
            callback(result)
        }
    }

    init {
        setTranslatesAutoresizingMaskIntoConstraints(false)
        autoresizingMask = UIViewAutoresizing.None
        backgroundColor = UIColor.fromRGBA(0.0, 0.0, 0.0, 0.0)

        textField.text = text
        textField.font = UIFont.getBoldSystemFont(35.0)
        textField.tintColor = tintColor
        val heading = UILabel()
        heading.textAlignment = NSTextAlignment.Center
        heading.textColor = tintColor
        heading.alpha = 1.0
        heading.text = title
        heading.font = UIFont.getSystemFont(20.0)
        val vertGroup = VerticalGroup.verticalGroup {
            layer.backgroundColor = IosCxColor(ColorCompatibility.secondarySystemBackground)
            layout.gravity = Gravity.Center
        }
        val horgroup = HorizontalGroup(Layout(widthMode = Layout.Mode.MatchParent))

        vertGroup.addView(
                heading,
                layout {
                    widthMode = Layout.Mode.MatchParent
                    heightMode = Layout.Mode.WrapContent
                    margin = 25.0
                    gravity = Gravity.Center
                }
        )
        horgroup.addView(
                textField,
                layout {
                    widthMode = Layout.Mode.Weighted
                    weight = 1.0
                    heightMode = Layout.Mode.WrapContent
                    margin = 25.0
                    gravity = Gravity.MiddleLeft
                }
        )
        button.setTitle("\u2705", UIControlState.Normal)
        button.titleLabel.font = UIFont.getSystemFont(40.0)
        button.addOnTouchUpInsideListener { _, _ -> done() }
        horgroup.addView(
                button,
                layout {
                    widthMode = Layout.Mode.Weighted
                    weight = 1.0
                    heightMode = Layout.Mode.WrapContent
                    margin = 25.0
                    gravity = Gravity.MiddleRight
                }
        )
        vertGroup.add(horgroup)
        val layer = CALayer()
        layer.backgroundColor = UIColor.white().cgColor
        layer.cornerRadius = 5.0
        textField.backgroundColor = ColorCompatibility.systemBackground
        frameGroup.add(vertGroup)
        // dismiss on tap outside the text area
    }

    override fun layoutSubviews() {
        super.layoutSubviews()
        textField.becomeFirstResponder()
    }

    fun done() {
        textField.resignFirstResponder()
    }
}
