package com.controlj.ios.view

import com.controlj.graphics.TextStyle
import com.controlj.graphics.alpha
import com.controlj.graphics.blue
import com.controlj.graphics.green
import com.controlj.graphics.red
import com.controlj.ios.ColorCompatibility
import com.controlj.ios.UIImageView
import com.controlj.ios.graphics.IosTextStyle
import com.controlj.layout.FrameGroup
import com.controlj.layout.Gravity
import com.controlj.layout.HorizontalGroup
import com.controlj.layout.Layout
import com.controlj.layout.VerticalGroup
import com.controlj.shim.CxRect
import com.controlj.shim.IosCxColor
import com.controlj.shim.IosCxLayer
import com.controlj.shim.IosCxRect
import com.controlj.shim.addView
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIButton
import org.robovm.apple.uikit.UIButtonType
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIControlState
import org.robovm.apple.uikit.UIFont
import org.robovm.apple.uikit.UILabel
import org.robovm.apple.uikit.UITapGestureRecognizer
import kotlin.math.ceil

/**
 * Created by clyde on 3/7/17.
 *
 * A view to display a dialog. The dialog is built up as a [VerticalGroup] of items taken from [dialogData].
 * The [dialogWidth] will be used to size the width of the default layout, which can be overridden by providing
 * a [layout] parameter. In any case half [dialogWidth] is used as the maximum width of a label in the list.
 */

class DialogView(
    val dialogData: DialogData,
    private val dialogWidth: Double = 360.0,
    layout: Layout = Layout(
        width = dialogWidth,
        widthMode = Layout.Mode.Absolute,
        gravity = Gravity.Center
    )
) : VerticalGroup(layout) {
    var textSize = 18.0             // default size of text (e.g. for labels)
    var buttonTextSize = 30.0       // default button size (applies to OK, CANCEL etc.
    var padding = 8.0               // Provides padding between the items and the edge of the dialog view

    private val titleView = UILabel()
    private val viewEntries: ArrayList<DialogEntry> = ArrayList(dialogData.items.size)
    private val buttonsEntries: MutableMap<DialogItem, UIButton> = mutableMapOf()

    val buttonHeight by lazy {
        ceil((IosTextStyle.getTextStyle(TextStyle(size = buttonTextSize)).height + padding * 2))
    }

    override fun onAttach(host: com.controlj.layout.UxHost) {
        super.onAttach(host)
        (layer as? IosCxLayer)?.let { layer ->
            if (layout.widthMode == Layout.Mode.Absolute) {
                layer.caLayer.cornerRadius = DialogEntry.marginSize
                layer.caLayer.borderColor = ColorCompatibility.secondaryLabel.cgColor
                layer.caLayer.borderWidth = 1.5
                layer.caLayer.setMasksToBounds(true)
            }
        }
    }

    init {
        titleView.text = dialogData.title
        titleView.textAlignment = NSTextAlignment.Center
        titleView.textColor = titleView.tintColor
        titleView.font = UIFont.getSystemFont(textSize)
        titleView.alpha = 1.0
        add(FrameGroup(Layout.matchWidth()).also { frame ->
            frame.addView(titleView, Layout.layout { gravity = Gravity.Center; margin = 4.0 })
            if (dialogData.closeButton) {
                frame.addView(UIImageView("close_x").also {
                    it.addGestureRecognizer(UITapGestureRecognizer { dialogData.dismiss() })
                    it.isUserInteractionEnabled = true
                }, Layout.layout {
                    gravity = Gravity.MiddleRight
                    margin = 5.0
                })
            }
        })
        addDivider()

        // build a list of items
        viewEntries.addAll(dialogData.items.map { item ->
            DialogEntry.create(item, dialogData)
        })
        viewEntries.forEach { add(it) }

        // create the buttonbar if we have buttons
        val buttons = listOfNotNull(dialogData.negative, dialogData.neutral, dialogData.positive)
        if (buttons.isNotEmpty()) {
            addDivider()
            val buttonBar = HorizontalGroup(Layout(widthMode = Layout.Mode.MatchParent))
            buttons.forEach {
                val button = UIButton(UIButtonType.System)
                button.titleLabel.font = UIFont.getSystemFont(buttonTextSize)
                button.setTitle(it.text, UIControlState.Normal)
                button.addOnTouchUpInsideListener { _, _ -> dialogData.onItemPressed(it) }
                if (it.tintColor != 0)
                    button.tintColor = it.tintColor.toUIColor()
                if (buttonBar.childViews.isNotEmpty())
                    buttonBar.addDivider()
                buttonBar.addView(
                    button, Layout(
                        widthMode = Layout.Mode.Weighted,
                        weight = 1.0,
                        gravity = Gravity.Center
                    )
                )
                buttonsEntries[it] = button
            }
            add(buttonBar)
        }
    }

    fun rectFor(item: DialogItem): CxRect {
        return viewEntries.find { it.item == item }?.rect ?: IosCxRect()

    }

    fun redraw() {
        layer.backgroundColor = IosCxColor(ColorCompatibility.systemBackground)
        titleView.text = dialogData.title
        viewEntries.forEach { entry -> entry.refresh() }
        buttonsEntries.forEach {
            it.value.setTitle(it.key.text, UIControlState.Normal)
            it.value.isEnabled = it.key.isEnabled
        }
        parent?.invalidate()
    }
}

fun Int.toUIColor(): UIColor {
    return UIColor(red / 255.0, green / 255.0, blue / 255.0, alpha / 255.0)
}

