package com.controlj.ios.view

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 9/11/19
 * Time: 14:58
 *
 * Allow control over the visibility of a view
 */
interface VisibleView {
    var isVisible: Boolean
}