package com.controlj.ios.view

import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.viewmodel.DialogListModel
import org.robovm.apple.foundation.NSArray
import org.robovm.apple.uikit.UITableViewRowAnimation
import org.robovm.apple.uikit.UIView
import org.robovm.apple.uikit.UIViewAutoresizing

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-03-27
 * Time: 10:52
 *
 * A UIViewcontroller to display a settings list, under control of a SettingsPresenter
 */
abstract class DialogDataController<T : DialogListModel> : TableController<DialogItem, T>(), DialogData.DialogShower {
    override lateinit var presenter: T
    private val items = mutableSetOf<DialogEntry>()
    override val refreshOnDrag: Boolean = false
    override fun dismiss() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private inner class CellView(item: DialogItem, val dialogEntry: DialogEntry) : BaseRowView(item) {
        init {
            add(dialogEntry)
        }
    }

    override fun getRowLayout(item: DialogItem): BaseRowView {
        val entry = DialogEntry.create(item, presenter.dialogData)
        entry.refresh()
        return CellView(item, entry)
    }

    override fun loadView() {
        uiTableView.tableFooterView = UIView()
        view = uiTableView
        view.autoresizingMask = UIViewAutoresizing.None
        title = presenter.dialogData.title
    }

    override fun viewDidDisappear(animated: Boolean) {
        super.viewWillDisappear(animated)
        presenter.dialogData.shower = null
    }

    override fun connectPresenter() {
        items.clear()
        super.connectPresenter()
        presenter.dialogData.shower = this
        presenter.dialogData.refresh()
    }

    /**
     * For all visible rows, refresh the DialogEntry then redraw all changed rows.
     */
    override fun refresh(source: DialogItem?) {
        val array = uiTableView.indexPathsForVisibleRows
                .map { index -> (uiTableView.getCellForRow(index) as? TableRowCellView)?.let { index to it } }
                .filterNotNull()
                .filter { (it.second.view as? DialogDataController<*>.CellView)?.dialogEntry?.refresh() == true }
                .map { it.first }
        if (array.isNotEmpty())
            uiTableView.reloadRows(NSArray(array), UITableViewRowAnimation.None)
    }
}
