package com.controlj.ios.view

import com.controlj.ios.ColorCompatibility
import com.controlj.layout.Gravity
import com.controlj.layout.Layout
import com.controlj.shim.addView
import com.controlj.ui.ButtonDialogItem
import com.controlj.ui.DialogData
import org.robovm.apple.uikit.UIButton
import org.robovm.apple.uikit.UIButtonType
import org.robovm.apple.uikit.UIControlContentHorizontalAlignment
import org.robovm.apple.uikit.UIControlState
import org.robovm.apple.uikit.UIFont

class ButtonEntry(item: ButtonDialogItem, dialogData: DialogData) : BasicEntry(item, dialogData) {
    val textButton = object : UIButton(UIButtonType.Custom) {
        override fun setHighlighted(state: Boolean) {
            super.setHighlighted(state)
            backgroundColor =
                    if (state) ColorCompatibility.secondarySystemBackground else ColorCompatibility.systemBackground
        }
    }

    init {
        widget = textButton
        textButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Center
        textButton.titleLabel.font = UIFont.getSystemFont(24.0)
        textButton.setTitleColor(ColorCompatibility.label, UIControlState.Normal)
        textButton.setTitleColor(ColorCompatibility.secondaryLabel, UIControlState.Disabled)
        textButton.addOnTouchUpInsideListener { _, _ ->
            dialogData.onItemPressed(item)
        }
        imageButton?.addOnTouchUpInsideListener { _, _ ->
            dialogData.onItemPressed(item)
        }
        addView(textButton, Layout(
                widthMode = Layout.Mode.Weighted,
                weight = 1.0,
                gravity = Gravity.MiddleLeft
        ))
    }

    override fun refresh(): Boolean {
        var changed = super.refresh()
        item as ButtonDialogItem
        imageButton?.tintColor = item.tintColor.toUIColor()
        if (textButton.getTitle(UIControlState.Normal) != item.text) {
            textButton.setTitle(item.text, UIControlState.Normal)
            changed = true
        }
        if (textButton.isEnabled != item.isEnabled) {
            changed = true
            textButton.isEnabled = item.isEnabled
        }
        return changed
    }
}
