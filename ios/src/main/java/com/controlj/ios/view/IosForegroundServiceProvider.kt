package com.controlj.ios.view

import com.controlj.framework.IosBackgroundServiceProvider
import com.controlj.framework.SystemColors
import com.controlj.ios.IosSystemColors
import com.controlj.ui.DialogData
import com.controlj.ui.UiAction
import com.controlj.view.ForegroundServiceProvider
import org.robovm.apple.foundation.NSURL
import org.robovm.apple.uikit.UIApplication
import org.robovm.apple.uikit.UIDocumentInteractionController
import org.robovm.apple.uikit.UIViewController
import java.io.File

class IosForegroundServiceProvider(private val controller: UIViewController) : ForegroundServiceProvider() {

    private val dialogController by lazy { DialogController(controller) }

    override var keepScreenOn: Boolean
        get() = UIApplication.getSharedApplication().isIdleTimerDisabled
        set(value) {
            UIApplication.getSharedApplication().isIdleTimerDisabled = value
        }

    override fun showDialog(dialogData: DialogData) {
        dialogController.show(DialogView(dialogData, 360.0))
    }

    @Suppress("DEPRECATION")
    override fun openUrl(url: String): Boolean {
        val nsurl = NSURL(url)
        if (!UIApplication.getSharedApplication().canOpenURL(nsurl))
            return false
        if (IosBackgroundServiceProvider.isIos10)
            UIApplication.getSharedApplication().openURL(nsurl, null, null)
        else
            UIApplication.getSharedApplication().openURL(nsurl)
        return true
    }

    override fun openFile(file: File, mimeType: String) {
        val interactionController = UIDocumentInteractionController(NSURL(file))
        interactionController.presentPreview(true)
    }

    override fun toast(message: String, duration: UiAction.ToastDuration) {
        Toast(message, if (duration === UiAction.ToastDuration.LONG) Toast.Settings.LONG else Toast.Settings.SHORT)
    }

    override val systemColors: SystemColors = IosSystemColors


    override fun openAppSettings() {
        openUrl(UIApplication.getOpenSettingsURLString())
    }

    init {
        init()
    }
}
