package com.controlj.ios.view

import com.controlj.graphics.CPoint
import com.controlj.graphics.CRect
import com.controlj.ios.graphics.CGContextWrapper
import com.controlj.view.ViewController
import com.controlj.widget.CView
import org.robovm.apple.coreanimation.CALayer
import org.robovm.apple.coreanimation.CALayerDelegateAdapter
import org.robovm.apple.coreanimation.CATransaction
import org.robovm.apple.coregraphics.CGContext
import org.robovm.apple.coregraphics.CGPoint
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.coregraphics.CGSize
import org.robovm.apple.foundation.NSSet
import org.robovm.apple.uikit.UIEvent
import org.robovm.apple.uikit.UIGestureRecognizerState
import org.robovm.apple.uikit.UIGraphics
import org.robovm.apple.uikit.UILongPressGestureRecognizer
import org.robovm.apple.uikit.UIPanGestureRecognizer
import org.robovm.apple.uikit.UIPinchGestureRecognizer
import org.robovm.apple.uikit.UIScreen
import org.robovm.apple.uikit.UITapGestureRecognizer
import org.robovm.apple.uikit.UITouch
import org.robovm.apple.uikit.UIView
import org.robovm.rt.bro.annotation.ByVal
import kotlin.math.abs
import kotlin.math.sqrt

/**
 * Created by clydestubbs on 7/07/2017.
 */

open class CViewWrapper(cview: CView? = null) : UIView(), ViewController {
    protected val gc = CGContextWrapper()
    protected val backGc = CGContextWrapper()
    protected val topLayer = CALayer()
    override var bounds: CRect
        get() = super.getBounds().asCrect()
        set(value) {
            super.setBounds(value.toCGRect())
        }

    init {
        topLayer.delegate = object : CALayerDelegateAdapter() {

            override fun drawLayer(caLayer: CALayer, cgContext: CGContext) {
                gc.setContext(cgContext)
                cView?.draw(gc)
            }

        }
        topLayer.contentsScale = UIScreen.getMainScreen().scale     // make same resolution as the background
        topLayer.actions = mapOf()
        topLayer.frame = getBounds()
        layer.addSublayer(topLayer)
    }

    override fun getSizeThatFits(p0: CGSize?): CGSize {
        return CGSize(cView?.preferredWidth ?: 0.0, cView?.preferredHeight ?: 0.0)
    }

    private var lastPanLocation = CGPoint()
    var cView: CView? = cview
        set(value) {
            if (field != value) {
                field?.parent = null
                field = value
            }
        }

    override fun willMoveToSuperview(parentView: UIView?) {
        super.willMoveToSuperview(parentView)
        if (parentView == null)
            gestureRecognizers.forEach { removeGestureRecognizer(it) }
        else cView?.let { value ->
            value.parent = this
            isUserInteractionEnabled = true
            addGestureRecognizer(UITapGestureRecognizer { recognizer ->
                val point = recognizer.getLocationInView(this)
                //debug("onClick ${point.x}, ${point.y}")
                value.onClick(CPoint(point.x, point.y))
            })
            addGestureRecognizer(UILongPressGestureRecognizer { recognizer ->
                val point = recognizer.getLocationInView(this)
                val cPoint = CPoint(point.x, point.y)
                //debug("long press at $cPoint state ${recognizer.state}")
                value.onLongPress(cPoint, recognizer.state == UIGestureRecognizerState.Ended)
            })

            if (value is CView.PanListener)
                addGestureRecognizer(UIPanGestureRecognizer { recognizer ->
                    when (recognizer.state) {
                        UIGestureRecognizerState.Began -> lastPanLocation = recognizer.getLocationInView(this)
                        UIGestureRecognizerState.Changed -> {
                            val loc = recognizer.getLocationInView(this)
                            value.onPan(loc.asCPoint(), loc.x - lastPanLocation.x, loc.y - lastPanLocation.y)
                            lastPanLocation = loc
                        }
                        else -> {
                        }
                    }

                })
            addGestureRecognizer(UIPinchGestureRecognizer { recognizer ->
                recognizer as UIPinchGestureRecognizer
                if (recognizer.numberOfTouches == 2L) when (recognizer.state) {
                    UIGestureRecognizerState.Began,
                    UIGestureRecognizerState.Changed -> {
                        val scale = recognizer.scale - 1
                        recognizer.scale = 1.0
                        val point0 = recognizer.getLocationOfTouch(0, this)
                        val point1 = recognizer.getLocationOfTouch(1, this)
                        val dy = abs(point1.y - point0.y)
                        val dx = abs(point1.x - point0.x)
                        val hippo = sqrt(dy * dy + dx * dx)
                        val center = CPoint((point1.x + point0.x) / 2, (point1.y + point0.y) / 2)
                        cView?.onZoom(center, 1 + scale * dx / hippo, 1 + scale * dy / hippo)
                    }
                    else -> Unit
                }
            })

        }
    }

    override fun touchesBegan(nsSet: NSSet<UITouch>, uiEvent: UIEvent) {
        onTouch(uiEvent.getTouches(this).any(), false)
        super.touchesBegan(nsSet, uiEvent)
    }

    override fun touchesEnded(nsSet: NSSet<UITouch>, uiEvent: UIEvent) {
        onTouch(uiEvent.getTouches(this).any(), true)
        super.touchesEnded(nsSet, uiEvent)
    }

    override fun touchesMoved(nsSet: NSSet<UITouch>, uiEvent: UIEvent) {
        onTouch(uiEvent.getTouches(this).any(), false)
        super.touchesMoved(nsSet, uiEvent)
    }

    private fun onTouch(any: UITouch, ended: Boolean) {
        val pos = any.getLocationInView(this)
        cView?.onTouch(CPoint(pos.x, pos.y), !ended)
    }

    override fun draw(@ByVal cgRect: CGRect) {
        //CJLog.logMsg("draw background");
        super.draw(cgRect)
        backGc.setContext(UIGraphics.getCurrentContext())
        cView?.drawBackground(backGc)
    }

    override fun layoutSubviews() {
        super.layoutSubviews()
        setAnimationsEnabled(true)
        cView?.bounds = bounds
        val anim = layer.getAnimation("bounds.size")
        CATransaction.begin()
        anim?.apply {
            CATransaction.setAnimationTimingFunction(timingFunction)
            CATransaction.setAnimationDuration(duration)
        }
        topLayer.frame = super.getBounds()
        CATransaction.commit()
        requestRedraw()
    }

    fun getcView(): CView? {
        return cView
    }

    override fun requestRedraw(rect: CRect) {
        val cgrect = rect.toCGRect()
        setNeedsDisplay(cgrect)
        topLayer.setNeedsDisplay(cgrect)
    }

    override fun redrawForeground(rect: CRect) {
        topLayer.setNeedsDisplay(rect.toCGRect())
    }
}

fun CGRect.asCrect(): CRect {
    return CRect(x, y, maxX, maxY)
}

fun CGPoint.asCPoint(): CPoint {
    return CPoint(x, y)
}
