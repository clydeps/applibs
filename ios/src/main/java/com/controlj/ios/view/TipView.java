package com.controlj.ios.view;

import com.controlj.graphics.TextStyle;

import org.robovm.apple.coregraphics.CGAffineTransform;
import org.robovm.apple.coregraphics.CGRect;
import org.robovm.apple.uikit.UIColor;
import org.robovm.apple.uikit.UITapGestureRecognizer;
import org.robovm.apple.uikit.UIView;
import org.robovm.apple.uikit.UIViewAnimationOptions;

/**
 * Created by clyde on 23/8/17.
 */

public class TipView extends UIView {

    private final Preferences preferences;
    private final String text;
    private boolean animated;
    private UIView superView;
    private DismissListener listener;

    public enum ArrowPosition {
        ANY,
        TOP,
        BOTTOM,
        RIGHT,
        LEFT
    }

    static public class Preferences {

        public class Drawing {
            public float cornerRadius = 5, arrowHeight = 5, arrowWidth = 10;
            public int foregroundColor = 0xFFFFFFFF, backgroundColor = 0xFFFF0000, borderColor = 0x00FFFFFF;
            public ArrowPosition arrowPosition = ArrowPosition.ANY;
            public TextStyle textStyle = new TextStyle(TextStyle.FontFamily.SystemDefault, TextStyle.FontStyle.Normal, TextStyle.Alignment.Center, 15);
            public float borderWidth = 0;
        }

        public class Positioning {
            float bubbleHInset = 1, bubbleVInset = 1, textHInset = 10, textVInset = 10, maxWidth = 200;
        }

        public class Animating {
            CGAffineTransform dismissTransform = CGAffineTransform.createScale(0.1f, 0.1f);
            CGAffineTransform showFinalTransform = CGAffineTransform.Identity();
            float springDamping = 0.7f, springVelocity = 0.7f;
            float initialAlpha = 0, dismissFinalAlpha = 0;
            float showDuration = 0.7f;
            float dismissDuration = 0.7f;
            boolean dismissOnTap = true;
        }

        public Drawing drawing = new Drawing();
        public Positioning positioning = new Positioning();
        public Animating animating = new Animating();

        public boolean hasBorder() {
            return drawing.borderWidth > 0 && drawing.borderWidth != 0;
        }
    }

    public interface DismissListener {
        void onDismiss(TipView tipView);
    }

    public TipView(Preferences preferences, String text) {
        super(CGRect.Zero());
        this.text = text;
        if(preferences == null)
            preferences = new Preferences();
        this.preferences = preferences;
        setBackgroundColor(UIColor.clear());
    }

    public static void show(UIView view, CGRect rect, String text) {
        show(true, view, rect, text, null, null);
    }

    public static void show(boolean animated, UIView view, CGRect rect, String text, Preferences preferences, DismissListener listener) {
        TipView tipView = new TipView(preferences, text);
    }

    public void show(boolean animated, UIView view, CGRect rect, DismissListener listener) {
        this.animated = animated;
        this.listener = listener;
        addGestureRecognizer(new UITapGestureRecognizer((v) -> {
            if(preferences.animating.dismissOnTap)
                dismiss();
        }));
        view.addSubview(this);
        if(animated) {
            UIView.animate(preferences.animating.showDuration, 0,
                preferences.animating.springDamping, preferences.animating.springVelocity,
                UIViewAnimationOptions.CurveEaseInOut,
                () -> {
                    setTransform(preferences.animating.showFinalTransform);
                    setAlpha(1);
                }, null);
        } else {
            setTransform(preferences.animating.showFinalTransform);
            setAlpha(1);
        }
    }

    public void dismiss() {
        if(animated)
            UIView.animate(preferences.animating.dismissDuration, 0,
                preferences.animating.springDamping, preferences.animating.springVelocity,
                UIViewAnimationOptions.CurveEaseInOut,
                () -> {
                    setTransform(preferences.animating.dismissTransform);
                    setAlpha(preferences.animating.dismissFinalAlpha);
                }, (v) -> {
                    if(listener != null)
                        listener.onDismiss(this);
                    removeFromSuperview();
                });
        else {
            if(listener != null)
                listener.onDismiss(this);
            removeFromSuperview();
        }
    }
}

/**
 * NotificationCenter.default.addObserver(self, selector: #selector(handleRotation), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
 * }
 * <p>
 * deinit
 * {
 * NotificationCenter.default.removeObserver(self)
 * }
 * <p>
 * /**
 * NSCoding not supported. Use init(text, preferences, delegate) instead!
 * required public init?(coder aDecoder: NSCoder) {
 * fatalError("NSCoding not supported. Use init(text, preferences, delegate) instead!")
 * }
 * <p>
 * // MARK: - Rotation support -
 * <p>
 * func handleRotation() {
 * guard let sview = superview
 * , presentingView != nil else { return }
 * <p>
 * UIView.animate(withDuration: 0.3, animations: { _ in
 * self.arrange(withinSuperview: sview)
 * self.setNeedsDisplay()
 * })
 * }
 * <p>
 * // MARK: - Private methods -
 * <p>
 * fileprivate func computeFrame(arrowPosition position: ArrowPosition, refViewFrame: CGRect, superviewFrame: CGRect) -> CGRect {
 * var xOrigin: CGFloat = 0
 * var yOrigin: CGFloat = 0
 * <p>
 * switch position {
 * case .top, .any:
 * xOrigin = refViewFrame.center.x - contentSize.width / 2
 * yOrigin = refViewFrame.y + refViewFrame.height
 * case .bottom:
 * xOrigin = refViewFrame.center.x - contentSize.width / 2
 * yOrigin = refViewFrame.y - contentSize.height
 * case .right:
 * xOrigin = refViewFrame.x - contentSize.width
 * yOrigin = refViewFrame.center.y - contentSize.height / 2
 * case .left:
 * xOrigin = refViewFrame.x + refViewFrame.width
 * yOrigin = refViewFrame.y - contentSize.height / 2
 * }
 * <p>
 * var frame = CGRect(x: xOrigin, y: yOrigin, width: contentSize.width, height: contentSize.height)
 * adjustFrame(&frame, forSuperviewFrame: superviewFrame)
 * return frame
 * }
 * <p>
 * fileprivate func adjustFrame(_ frame: inout CGRect, forSuperviewFrame superviewFrame: CGRect) {
 * <p>
 * // adjust horizontally
 * if frame.x < 0 {
 * frame.x =  0
 * } else if frame.maxX > superviewFrame.width {
 * frame.x = superviewFrame.width - frame.width
 * }
 * <p>
 * //adjust vertically
 * if frame.y < 0 {
 * frame.y = 0
 * } else if frame.maxY > superviewFrame.maxY {
 * frame.y = superviewFrame.height - frame.height
 * }
 * }
 * <p>
 * fileprivate func isFrameValid(_ frame: CGRect, forRefViewFrame: CGRect, withinSuperviewFrame: CGRect) -> Bool {
 * return !frame.intersects(forRefViewFrame)
 * }
 * <p>
 * fileprivate func arrange(withinSuperview superview: UIView) {
 * <p>
 * var position = preferences.drawing.arrowPosition
 * <p>
 * let refViewFrame = presentingView!.convert(presentingView!.bounds, to: superview);
 * <p>
 * let superviewFrame: CGRect
 * if let scrollview = superview as? UIScrollView {
 * superviewFrame = CGRect(origin: scrollview.frame.origin, size: scrollview.contentSize)
 * } else {
 * superviewFrame = superview.frame
 * }
 * <p>
 * var frame = computeFrame(arrowPosition: position, refViewFrame: refViewFrame, superviewFrame: superviewFrame)
 * <p>
 * if !isFrameValid(frame, forRefViewFrame: refViewFrame, withinSuperviewFrame: superviewFrame) {
 * for value in ArrowPosition.allValues where value != position {
 * let newFrame = computeFrame(arrowPosition: value, refViewFrame: refViewFrame, superviewFrame: superviewFrame)
 * if isFrameValid(newFrame, forRefViewFrame: refViewFrame, withinSuperviewFrame: superviewFrame) {
 * <p>
 * if position != .any {
 * print("[EasyTipView - Info] The arrow position you chose <\(position)> could not be applied. Instead, position <\(value)> has been applied! Please specify position <\(ArrowPosition.any)> if you want EasyTipView to choose a position for you.")
 * }
 * <p>
 * frame = newFrame
 * position = value
 * preferences.drawing.arrowPosition = value
 * break
 * }
 * }
 * }
 * <p>
 * var arrowTipXOrigin: CGFloat
 * <p>
 * switch position {
 * case .bottom, .top, .any:
 * if frame.width < refViewFrame.width {
 * arrowTipXOrigin = contentSize.width / 2
 * } else {
 * arrowTipXOrigin = abs(frame.x - refViewFrame.x) + refViewFrame.width / 2
 * }
 * <p>
 * arrowTip = CGPoint(x: arrowTipXOrigin, y: position == .bottom ? contentSize.height - preferences.positioning.bubbleVInset :  preferences.positioning.bubbleVInset)
 * case .right, .left:
 * if frame.height < refViewFrame.height {
 * arrowTipXOrigin = contentSize.height / 2
 * } else {
 * arrowTipXOrigin = abs(frame.y - refViewFrame.y) + refViewFrame.height / 2
 * }
 * <p>
 * arrowTip = CGPoint(x: preferences.drawing.arrowPosition == .left ? preferences.positioning.bubbleVInset : contentSize.width - preferences.positioning.bubbleVInset, y: arrowTipXOrigin)
 * }
 * self.frame = frame
 * }
 * <p>
 * // MARK:- Callbacks -
 * <p>
 * func handleTap() {
 * dismiss()
 * }
 * <p>
 * // MARK:- Drawing -
 * <p>
 * fileprivate func drawBubble(_ bubbleFrame: CGRect, arrowPosition: ArrowPosition,  context: CGContext) {
 * <p>
 * let arrowWidth = preferences.drawing.arrowWidth
 * let arrowHeight = preferences.drawing.arrowHeight
 * let cornerRadius = preferences.drawing.cornerRadius
 * <p>
 * let contourPath = CGMutablePath()
 * <p>
 * contourPath.move(to: CGPoint(x: arrowTip.x, y: arrowTip.y))
 * <p>
 * switch arrowPosition {
 * case .bottom, .top, .any:
 * <p>
 * contourPath.addLine(to: CGPoint(x: arrowTip.x - arrowWidth / 2, y: arrowTip.y + (arrowPosition == .bottom ? -1 : 1) * arrowHeight))
 * if arrowPosition == .bottom {
 * drawBubbleBottomShape(bubbleFrame, cornerRadius: cornerRadius, path: contourPath)
 * } else {
 * drawBubbleTopShape(bubbleFrame, cornerRadius: cornerRadius, path: contourPath)
 * }
 * contourPath.addLine(to: CGPoint(x: arrowTip.x + arrowWidth / 2, y: arrowTip.y + (arrowPosition == .bottom ? -1 : 1) * arrowHeight))
 * <p>
 * case .right, .left:
 * <p>
 * contourPath.addLine(to: CGPoint(x: arrowTip.x + (arrowPosition == .right ? -1 : 1) * arrowHeight, y: arrowTip.y - arrowWidth / 2))
 * <p>
 * if arrowPosition == .right {
 * drawBubbleRightShape(bubbleFrame, cornerRadius: cornerRadius, path: contourPath)
 * } else {
 * drawBubbleLeftShape(bubbleFrame, cornerRadius: cornerRadius, path: contourPath)
 * }
 * <p>
 * contourPath.addLine(to: CGPoint(x: arrowTip.x + (arrowPosition == .right ? -1 : 1) * arrowHeight, y: arrowTip.y + arrowWidth / 2))
 * }
 * <p>
 * contourPath.closeSubpath()
 * context.addPath(contourPath)
 * context.clip()
 * <p>
 * paintBubble(context)
 * <p>
 * if preferences.hasBorder {
 * drawBorder(contourPath, context: context)
 * }
 * }
 * <p>
 * fileprivate func drawBubbleBottomShape(_ frame: CGRect, cornerRadius: CGFloat, path: CGMutablePath) {
 * <p>
 * path.addArc(tangent1End: CGPoint(x: frame.x, y: frame.y + frame.height), tangent2End: CGPoint(x: frame.x, y: frame.y), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x, y: frame.y), tangent2End: CGPoint(x: frame.x + frame.width, y: frame.y), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x + frame.width, y: frame.y), tangent2End: CGPoint(x: frame.x + frame.width, y: frame.y + frame.height), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x + frame.width, y: frame.y + frame.height), tangent2End: CGPoint(x: frame.x, y: frame.y + frame.height), radius: cornerRadius)
 * }
 * <p>
 * fileprivate func drawBubbleTopShape(_ frame: CGRect, cornerRadius: CGFloat, path: CGMutablePath) {
 * <p>
 * path.addArc(tangent1End: CGPoint(x: frame.x, y: frame.y), tangent2End: CGPoint(x: frame.x, y: frame.y + frame.height), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x, y:  frame.y + frame.height), tangent2End: CGPoint(x: frame.x + frame.width, y: frame.y + frame.height), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x + frame.width, y: frame.y + frame.height), tangent2End: CGPoint(x: frame.x + frame.width, y: frame.y), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x + frame.width, y: frame.y), tangent2End: CGPoint(x: frame.x, y: frame.y), radius: cornerRadius)
 * }
 * <p>
 * fileprivate func drawBubbleRightShape(_ frame: CGRect, cornerRadius: CGFloat, path: CGMutablePath) {
 * <p>
 * path.addArc(tangent1End: CGPoint(x: frame.x + frame.width, y: frame.y), tangent2End: CGPoint(x: frame.x, y: frame.y), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x, y: frame.y), tangent2End: CGPoint(x: frame.x, y: frame.y + frame.height), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x, y: frame.y + frame.height), tangent2End: CGPoint(x: frame.x + frame.width, y: frame.y + frame.height), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x + frame.width, y: frame.y + frame.height), tangent2End: CGPoint(x: frame.x + frame.width, y: frame.height), radius: cornerRadius)
 * <p>
 * }
 * <p>
 * fileprivate func drawBubbleLeftShape(_ frame: CGRect, cornerRadius: CGFloat, path: CGMutablePath) {
 * <p>
 * path.addArc(tangent1End: CGPoint(x: frame.x, y: frame.y), tangent2End: CGPoint(x: frame.x + frame.width, y: frame.y), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x + frame.width, y: frame.y), tangent2End: CGPoint(x: frame.x + frame.width, y: frame.y + frame.height), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x + frame.width, y: frame.y + frame.height), tangent2End: CGPoint(x: frame.x, y: frame.y + frame.height), radius: cornerRadius)
 * path.addArc(tangent1End: CGPoint(x: frame.x, y: frame.y + frame.height), tangent2End: CGPoint(x: frame.x, y: frame.y), radius: cornerRadius)
 * }
 * <p>
 * fileprivate func paintBubble(_ context: CGContext) {
 * context.fillColor = preferences.drawing.backgroundColor.cgColor
 * context.fill(bounds)
 * }
 * <p>
 * fileprivate func drawBorder(_ borderPath: CGPath, context: CGContext) {
 * context.addPath(borderPath)
 * context.strokeColor = preferences.drawing.borderColor.cgColor
 * context.lineWidth = preferences.drawing.borderWidth
 * context.strokePath()
 * }
 * <p>
 * fileprivate func drawText(_ bubbleFrame: CGRect, context : CGContext) {
 * let paragraphStyle = NSMutableParagraphStyle()
 * paragraphStyle.alignment = preferences.drawing.textAlignment
 * paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
 * <p>
 * <p>
 * let textRect = CGRect(x: bubbleFrame.origin.x + (bubbleFrame.size.width - textSize.width) / 2, y: bubbleFrame.origin.y + (bubbleFrame.size.height - textSize.height) / 2, width: textSize.width, height: textSize.height)
 * <p>
 * <p>
 * text.draw(in: textRect, withAttributes: [NSFontAttributeName : preferences.drawing.font, NSForegroundColorAttributeName : preferences.drawing.foregroundColor, NSParagraphStyleAttributeName : paragraphStyle])
 * }
 * <p>
 * override open func draw(_ rect: CGRect) {
 * <p>
 * let arrowPosition = preferences.drawing.arrowPosition
 * let bubbleWidth: CGFloat
 * let bubbleHeight: CGFloat
 * let bubbleXOrigin: CGFloat
 * let bubbleYOrigin: CGFloat
 * switch arrowPosition {
 * case .bottom, .top, .any:
 * <p>
 * bubbleWidth = contentSize.width - 2 * preferences.positioning.bubbleHInset
 * bubbleHeight = contentSize.height - 2 * preferences.positioning.bubbleVInset - preferences.drawing.arrowHeight
 * <p>
 * bubbleXOrigin = preferences.positioning.bubbleHInset
 * bubbleYOrigin = arrowPosition == .bottom ? preferences.positioning.bubbleVInset : preferences.positioning.bubbleVInset + preferences.drawing.arrowHeight
 * <p>
 * case .left, .right:
 * <p>
 * bubbleWidth = contentSize.width - 2 * preferences.positioning.bubbleHInset - preferences.drawing.arrowHeight
 * bubbleHeight = contentSize.height - 2 * preferences.positioning.bubbleVInset
 * <p>
 * bubbleXOrigin = arrowPosition == .right ? preferences.positioning.bubbleHInset : preferences.positioning.bubbleHInset + preferences.drawing.arrowHeight
 * bubbleYOrigin = preferences.positioning.bubbleVInset
 * <p>
 * }
 * let bubbleFrame = CGRect(x: bubbleXOrigin, y: bubbleYOrigin, width: bubbleWidth, height: bubbleHeight)
 * <p>
 * let context = UIGraphicsGetCurrentContext()!
 * context.saveGState ()
 * <p>
 * drawBubble(bubbleFrame, arrowPosition: preferences.drawing.arrowPosition, context: context)
 * drawText(bubbleFrame, context: context)
 * <p>
 * context.restoreGState()
 * }
 * }
 */
