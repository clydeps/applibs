package com.controlj.ios.view

import com.controlj.framework.ApplicationState
import com.controlj.ios.ColorCompatibility
import com.controlj.ios.addTapListener
import com.controlj.ios.uIImage
import com.controlj.layout.Gravity
import com.controlj.layout.HorizontalGroup
import com.controlj.layout.IosUxHost
import com.controlj.layout.Layout
import com.controlj.settings.Setting
import com.controlj.shim.addView
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIActivityIndicatorView
import org.robovm.apple.uikit.UIButton
import org.robovm.apple.uikit.UIControlState
import org.robovm.apple.uikit.UIImage
import org.robovm.apple.uikit.UILabel

/**
 * Created by clyde on 13/6/18.
 *
 * This class provides a navigation bar with a title, a back arrow and a busy indicator.
 */
class NavBar : IosUxHost() {

    val titleView = UILabel()
    var title: String = ""
        set(value) {
            field = value
            titleView.text = value
            setNeedsLayout()
        }
    var listener: () -> Unit = {}
    val button: UIButton = UIButton()
    val button1: UIButton = UIButton()
    val spinner: UIActivityIndicatorView = UIActivityIndicatorView()

    init {
        backgroundColor = ColorCompatibility.secondarySystemBackground
        titleView.textColor = tintColor
        titleView.textAlignment = NSTextAlignment.Center
        button.setImage(CHEVRON.uIImage, UIControlState.Normal)
        button1.setTitle("Back", UIControlState.Normal)
        button1.setTitleColor(tintColor, UIControlState.Normal)
        button.addOnTouchUpInsideListener { _, _ -> listener() }
        button1.addOnTouchUpInsideListener { _, _ -> listener() }
        val buttonLayout = HorizontalGroup(Layout.layout {
            gravity = Gravity.MiddleLeft
        })
        buttonLayout.addView(button, Layout.layout {
            gravity = Gravity.MiddleLeft
            margins.right = 8.0
            margins.left = 8.0
        })
        buttonLayout.addView(button1, Layout.layout {
            gravity = Gravity.MiddleLeft
        })
        frameGroup.add(buttonLayout)
        frameGroup.addView(titleView, Layout(
                widthMode = Layout.Mode.WrapContent,
                gravity = Gravity.Center
        ))
        frameGroup.addView(spinner, Layout(
                widthMode = Layout.Mode.WrapContent,
                heightMode = Layout.Mode.MatchParent,
                gravity = Gravity.MiddleRight
        ))
        spinner.setHidesWhenStopped(true)
        titleView.addTapListener { Setting.shakeSauceBottle() }
    }

    /**
     * Set the state of the busy indicator
     * @param state True to show and animate the busy indicator
     */
    fun setBusy(state: Boolean) {
        if (state)
            spinner.startAnimating()
        else
            spinner.stopAnimating()
    }

    companion object {
        const val HEIGHT = 44.0
        const val ANIMATION_DURATION = 0.5
        const val CHEVRON = "chevron"

        // A global navbar, if available
        var instance: NavBar? = null
    }
}

