package com.controlj.ios.view

import com.controlj.ios.ColorCompatibility
import com.controlj.layout.Gravity
import com.controlj.layout.HorizontalGroup
import com.controlj.layout.Layout
import com.controlj.shim.addView
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.ui.ValueDialogItem
import com.controlj.widget.UITextFieldPadded
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIEdgeInsets
import org.robovm.apple.uikit.UITextBorderStyle

class ValueItemEntry(val valueItem: ValueDialogItem, dialogData: DialogData) : LabelledEntry(valueItem, dialogData) {

    private val valueField = UITextFieldPadded(UIEdgeInsets(PADDING, PADDING, PADDING, PADDING))
    private val delegate = TextFieldDelegate(valueField, DialogItem.InputStyle.SIGNEDDECIMAL) {
        valueItem.convertedValue = it
        valueItem.onChanged()
        true
    }
    private val spinner: Spinner = Spinner(valueItem.unitList) {
        if (it != valueItem.selected) {
            valueItem.selected = it
            refresh()
        }
    }

    init {
        widget = spinner
        valueField.backgroundColor = ColorCompatibility.systemBackground
        valueField.textColor = ColorCompatibility.label
        valueField.isUserInteractionEnabled = true
        valueField.borderStyle = UITextBorderStyle.Bezel
        val group = HorizontalGroup(Layout(widthMode = Layout.Mode.MatchParent, gravity = Gravity.MiddleRight))
        group.addView(valueField, Layout(widthMode = Layout.Mode.Weighted, gravity = Gravity.MiddleRight))
        group.addView(spinner)
        contentGroup.add(group)
    }

    override fun refresh(): Boolean {
        var changed = super.refresh()
        if (spinner.selected != valueItem.selected) {
            changed = true
            spinner.selected = valueItem.selected
        }
        if (delegate.setText(valueItem.convertedValue))
            changed = true
        return changed
    }
}
