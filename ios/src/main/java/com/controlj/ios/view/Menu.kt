package com.controlj.ios.view

import com.controlj.rx.MainScheduler
import com.controlj.shim.IosCxRect
import com.controlj.view.MenuEntry
import io.reactivex.rxjava3.core.Completable
import org.robovm.apple.uikit.*
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-08
 * Time: 13:43
 */
object IosMenu {
    /**
     * Show a menu with the supplied title and list.
     */
    fun showMenu(
            uiViewController: UIViewController,
            title: String,
            items: List<MenuEntry>,
            source: IosCxRect?
    ) {

        var topController: UIViewController = uiViewController
        while (uiViewController.presentedViewController != null)
            topController = uiViewController.presentedViewController
        val choice = UIAlertController(title, null, UIAlertControllerStyle.ActionSheet)
        val disposable = Completable.timer(10L, TimeUnit.SECONDS, MainScheduler.instance)
                .subscribe {
                    //debug("timeout: choice.isbeingdismissed = ${choice.isBeingDismissed}")
                    if (!choice.isBeingDismissed && uiViewController.presentedViewController == choice)
                        uiViewController.dismissViewController(true) {}
                }
        items.forEach { entry ->
            val action = UIAlertAction(
                    if(entry.selected) "${entry.title}  \u2713" else entry.title,
                    UIAlertActionStyle.Default
            ) {
                disposable.dispose()
                entry.action()
            }
            action.isEnabled = entry.enabled
            choice.addAction(action)
            if (entry.selected)
                choice.preferredAction = action
        }
        // the cancel button will not be shown, clicking outside will cancel the popover
        choice.addAction(UIAlertAction(
                "Cancel",
                UIAlertActionStyle.Cancel
        ) { disposable.dispose() })
        topController.presentViewController(choice, true, null)
        val popver = choice.popoverPresentationController
        if (popver != null) {
            popver.sourceView = uiViewController.view
            if (source != null) {
                popver.sourceRect = source.cgRect
            }
            popver.permittedArrowDirections = UIPopoverArrowDirection.Any
        }
    }
}
