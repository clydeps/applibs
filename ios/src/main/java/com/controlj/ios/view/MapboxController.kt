package com.controlj.ios.view

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.Position
import com.controlj.framework.ApplicationState
import com.controlj.framework.IosBackgroundServiceProvider.isIos11
import com.controlj.ios.UIImageView
import com.controlj.ios.graphics.IOSGraphicsFactory
import com.controlj.ios.toNSArray
import com.controlj.logging.CJLog
import com.controlj.logging.CJLog.debug
import com.controlj.mapping.Skysight
import com.controlj.route.TrackSource
import com.controlj.rx.DisposedSingleEmitter
import com.controlj.rx.MainScheduler
import com.controlj.shim.IosCxRect
import com.controlj.ui.ViewModelStore
import com.controlj.view.MapViewModel
import com.controlj.view.RadarMapViewModel
import com.controlj.view.RouteMapViewModel
import com.controlj.view.icon
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.disposables.Disposable
import org.robovm.apple.coreanimation.CAMediaTimingFunction
import org.robovm.apple.coreanimation.CAMediaTimingFunctionName
import org.robovm.apple.corefoundation.CFDate
import org.robovm.apple.coregraphics.CGAffineTransform
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.coregraphics.CGVector
import org.robovm.apple.corelocation.CLLocationCoordinate2D
import org.robovm.apple.foundation.NSArray
import org.robovm.apple.foundation.NSDictionary
import org.robovm.apple.foundation.NSExpression
import org.robovm.apple.foundation.NSNumber
import org.robovm.apple.foundation.NSObject
import org.robovm.apple.foundation.NSString
import org.robovm.apple.foundation.NSURL
import org.robovm.apple.foundation.NSURLSessionConfiguration
import org.robovm.apple.uikit.NSLayoutConstraint
import org.robovm.apple.uikit.NSValueExtensions.create
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIEdgeInsets
import org.robovm.apple.uikit.UIFont
import org.robovm.apple.uikit.UIImage
import org.robovm.apple.uikit.UILabel
import org.robovm.apple.uikit.UITapGestureRecognizer
import org.robovm.apple.uikit.UIView
import org.robovm.apple.uikit.UIViewController
import org.robovm.pods.mapbox.MGLAnnotation
import org.robovm.pods.mapbox.MGLAnnotationView
import org.robovm.pods.mapbox.MGLCoordinateBounds
import org.robovm.pods.mapbox.MGLFillStyleLayer
import org.robovm.pods.mapbox.MGLGeometry
import org.robovm.pods.mapbox.MGLMapCamera
import org.robovm.pods.mapbox.MGLMapView
import org.robovm.pods.mapbox.MGLMapViewDelegateAdapter
import org.robovm.pods.mapbox.MGLNetworkConfiguration
import org.robovm.pods.mapbox.MGLPointAnnotation
import org.robovm.pods.mapbox.MGLPointFeature
import org.robovm.pods.mapbox.MGLPolyline
import org.robovm.pods.mapbox.MGLRasterStyleLayer
import org.robovm.pods.mapbox.MGLRasterTileSource
import org.robovm.pods.mapbox.MGLShape
import org.robovm.pods.mapbox.MGLShapeCollectionFeature
import org.robovm.pods.mapbox.MGLShapeSource
import org.robovm.pods.mapbox.MGLSource
import org.robovm.pods.mapbox.MGLSymbolStyleLayer
import org.robovm.pods.mapbox.MGLTextAnchor
import org.robovm.pods.mapbox.MGLTileSourceOption
import org.robovm.pods.mapbox.MGLTransition
import org.robovm.pods.mapbox.MGLVectorTileSource
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 8/4/18.
 */
class MapboxController : UIViewController() {

    init {
        // must be done before the mapview is created
        val config = NSURLSessionConfiguration.getDefaultSessionConfiguration()
        config.setGetAdditionalHTTPHeaders(Skysight.headers)
        MGLNetworkConfiguration.getSharedManager().sessionConfiguration = config
    }
    /**
     * The presenter for this view.
     */
    private val presenter: RadarMapViewModel = ViewModelStore.loadViewModel(RadarMapViewModel::class.java)

    private data class TrackLine(val line: MGLPolyline, val color: UIColor, val width: Double)

    private val lineMap = mutableMapOf<TrackSource.TrackKey, TrackLine>()
    private val trackMap = mutableMapOf<MGLPolyline, TrackLine>()
    private val posAnnotation: MGLPointAnnotation = MGLPointAnnotation()
    private val mapView = MGLMapView(CGRect(0.0, 0.0, 1.0, 1.0))
    private val locationButton = UIImageView("map_location")
    private val layerButton = UIImageView("map_layers")
    private val noteText = UILabel()

    private var activeLine: MGLPolyline? = null
    private val routeLines = ArrayList<MGLPolyline>()
    private var lastUpdateTimestamp = 0.0        // the last time the location was updated
    private var lastUpdateInterval = 0.5        // the last known update interval
    private var disposable = Disposable.disposed()
    private val timingFunction = CAMediaTimingFunction(CAMediaTimingFunctionName.Linear)
    private var mapLoaded = false
    private var mapEmitter: SingleEmitter<MGLMapView> = DisposedSingleEmitter()
    private val flightMarker = FlightMarker(presenter.flightMarkerFilename)
    private val trafficSource =
        MGLShapeSource("traffic", NSArray(), NSDictionary(mapOf<NSString, NSObject>()))


    private val delegate = object : MGLMapViewDelegateAdapter() {
        override fun shouldChange(mapView: MGLMapView, oldCamera: MGLMapCamera?, newCamera: MGLMapCamera): Boolean {
            presenter.onViewChanged(
                newCamera.centerCoordinate.toPosition(),
                MGLGeometry.zoomLevelForAltitude(
                    newCamera.altitude,
                    newCamera.pitch,
                    newCamera.centerCoordinate.latitude,
                    mapView.bounds.size
                ),
                newCamera.pitch
            )
            return true
        }

        override fun didFinishRenderingFrame(mapView: MGLMapView, fullyRendered: Boolean) {
            val frac = ((CFDate.getCurrentAbsoluteTime() - lastUpdateTimestamp) / lastUpdateInterval).coerceAtMost(1.0)
            if (flightMarker.position.valid)
                posAnnotation.coordinate = flightMarker.interpolatedLocation(frac)
        }

        override fun strokeColorForShapeAnnotation(mapView: MGLMapView, annotation: MGLShape): UIColor {
            if (activeLine == annotation)
                return UIColor.magenta()
            if (routeLines.contains(annotation))
                return UIColor.white()
            trackMap[annotation]?.let {
                return it.color
            }
            return UIColor.red()
        }

        override fun lineWidthForPolylineAnnotation(mapView: MGLMapView?, annotation: MGLPolyline?): Double {
            trackMap[annotation]?.let {
                return it.width
            }
            return 3.0
        }

        override fun viewForAnnotation(mapView: MGLMapView, annotation: MGLAnnotation): MGLAnnotationView? {
            return if (annotation == posAnnotation) flightMarker else null
        }

        override fun mapViewDidFinishLoadingMap(mglmapView: MGLMapView) {
            super.mapViewDidFinishLoadingMap(mglmapView)
            mglmapView.addAnnotation(posAnnotation)
            mapLoaded = true
            mapEmitter.onSuccess(mglmapView)
            //mglmapView.logoView.isHidden = true
        }

        override fun mapViewWillStartLoadingMap(mapView: MGLMapView?) {
            super.mapViewWillStartLoadingMap(mapView)
            mapLoaded = false
        }
    }

    class FlightMarker(filename: String) : MGLAnnotationView() {
        private val imageView = UIImageView(filename)
        var rotation: Double = 0.0
            set(value) {
                field = value
                imageView.transform = CGAffineTransform.createRotation(Math.toRadians(rotation))
            }

        var lastLocation: Position = Position.invalid
        var position: Position = Position.invalid
            set(value) {
                lastLocation = field
                field = value
            }

        fun interpolatedLocation(frac: Double): CLLocationCoordinate2D {
            return if (lastLocation.valid)
                lastLocation.interpolate(position, frac).asCLLocation2D()
            else
                position.asCLLocation2D()
        }

        init {
            frame = CGRect(0.0, 0.0, 60.0, 60.0)
            addSubview(imageView)
            imageView.frame = bounds
            imageView.setTranslatesAutoresizingMaskIntoConstraints(false)
            NSLayoutConstraint.activateConstraints(
                NSArray(
                    imageView.centerXAnchor.equalTo(centerXAnchor),
                    imageView.centerYAnchor.equalTo(centerYAnchor)
                )
            )
            backgroundColor = UIColor.clear()
            val config = NSURLSessionConfiguration.getDefaultSessionConfiguration()
            config.setGetAdditionalHTTPHeaders(Skysight.headers)
            MGLNetworkConfiguration.getSharedManager().sessionConfiguration = config
        }
    }

    private fun waitForMap(): Single<MGLMapView> {
        if (mapLoaded)
            return Single.just(mapView)
        return Single.create { emitter ->
            mapEmitter = emitter
        }
    }

    private fun showLine(track: RouteMapViewModel.ShowLineEvent) {
        if (track.points.isEmpty()) {
            lineMap.remove(track.key)?.let {
                mapView.removeOverlay(it.line)
                trackMap.remove(it.line)
            }
            return
        }
        val points = track.points.map { it.asCLLocation2D() }.toTypedArray()
        val trackLine = TrackLine(MGLPolyline.polyLine(points), track.color.toUIColor(), track.width)
        trackMap[trackLine.line] = trackLine
        mapView.addOverlay(trackLine.line)
        lineMap.put(track.key, trackLine)?.let {
            MainScheduler().scheduleDirect(
                {
                    mapView.removeOverlay(it.line)
                    trackMap.remove(it.line)
                }, 20, TimeUnit.MILLISECONDS
            )
        }
    }

    private fun processTraffic(event: RouteMapViewModel.TrafficEvent) {
        val shapes = event.targets.map { target ->
            MGLPointFeature().apply {
                coordinate = target.asCLLocation2D()
                attributes = NSDictionary(
                    mapOf<NSString, NSObject>(
                        NSString("text") to NSString(target.shortString),
                        NSString("icon") to NSString(target.icon.name),
                        NSString("rotation") to NSNumber.valueOf(target.track - mapView.camera.heading)
                    )
                )
            }
        }
        trafficSource.shape = MGLShapeCollectionFeature.shapeCollection(NSArray(shapes))
    }

    private fun processMapEvent(event: RouteMapViewModel.RouteMapViewEvent) {
        when (event) {
            is RouteMapViewModel.ShowLineEvent -> showLine(event)
            is RouteMapViewModel.ShowRouteEvent -> {
                routeLines.forEach { mapView.removeOverlay(it) }
                routeLines.clear()
                event.route.points.forEachIndexed { index, routePoint ->
                    if (index != 0) {
                        val line = MGLPolyline.polyLine(
                            arrayOf(
                                event.route.points[index - 1].asCLLocation2D(),
                                routePoint.asCLLocation2D()
                            )
                        )
                        routeLines.add(line)
                        if (index == event.route.active)
                            activeLine = line
                    }
                }
                routeLines.forEach { mapView.addOverlay(it) }
            }
            is RouteMapViewModel.ShowLayerEvent -> {
                mapView.style.layer(event.layer)?.isVisible = event.visible
            }
            is RouteMapViewModel.RemoveSourceEvent -> {
                val source = mapView.style.source(event.name)
                if (source != null)
                    mapView.style.removeSource(source)
            }
            is RouteMapViewModel.RemoveLayerEvent -> {
                val layer = mapView.style.layer(event.name)
                if (layer != null)
                    mapView.style.removeLayer(layer)
            }
            is RouteMapViewModel.SetLayerOpacityEvent -> {
                val layer = mapView.style.layer(event.name)
                val number = NSExpression.createForConstantValue(NSNumber.valueOf(event.opacity))
                when (layer) {
                    is MGLFillStyleLayer -> layer.fillOpacity = number
                    is MGLRasterStyleLayer -> layer.rasterOpacity = number
                }
            }
            is RouteMapViewModel.AddFillLayerEvent -> {
                if (mapView.style.layer(event.name) == null) {
                    val mglsource = mapView.style.source(event.source)
                    val layer = MGLFillStyleLayer(event.name, mglsource)
                    layer.sourceLayerIdentifier = event.sourceLayer
                    val stops = NSDictionary(event.fillColors.map { entry ->
                        NSNumber.valueOf(entry.key) to IOSGraphicsFactory().colorFromArgb(entry.value or 0xF0000000.toInt())
                    }.toMap())
                    layer.fillColor = NSExpression(
                        "mgl_interpolate:withCurveType:parameters:stops:(${event.parameter}, 'linear', nil, %@)",
                        NSArray(stops)
                    )
                    layer.fillOpacityTransition = MGLTransition(0.0, 0.0)
                    layer.isVisible = true
                    if (event.below.isNotBlank()) {
                        val layerBelow = mapView.style.layer(event.below)
                        mapView.style.insertLayerBelow(layer, layerBelow)
                    } else
                        mapView.style.addLayer(layer)
                }
            }
            is RouteMapViewModel.AddRasterLayerEvent -> {
                if (mapView.style.layer(event.name) == null) {
                    val mglsource: MGLSource? = mapView.style.source(event.source)
                    val layer = MGLRasterStyleLayer(event.name, mglsource)
                    mapView.style.layer(event.below)?.let {
                        mapView.style.insertLayerBelow(layer, it)
                    } ?: mapView.style.addLayer(layer)

                }
            }
            is RouteMapViewModel.AddRadarSourceEvent -> {
                if (mapView.style.source(event.source.name) == null) {
                    val tsource = event.source
                    val tileSet = MGLRasterTileSource(
                        event.source.name, tsource.tiles.toNSArray(),
                        NSDictionary.fromStringMap(
                            mapOf(
                                MGLTileSourceOption.Keys.MaximumZoomLevel()
                                    .toString() to NSNumber.valueOf(tsource.maxZoom),
                                MGLTileSourceOption.Keys.MinimumZoomLevel()
                                    .toString() to NSNumber.valueOf(tsource.minZoom)
                            )
                        )
                    )
                    mapView.style.addSource(tileSet)
                }
            }
            is RouteMapViewModel.AddSourceEvent -> {
                if (mapView.style.source(event.name) == null)
                    mapView.style.addSource(
                        if (event.vector)
                            MGLVectorTileSource(event.name, NSURL(event.url))
                        else
                            MGLRasterTileSource(event.name, NSURL(event.url))
                    )
            }
            else -> debug("Unknown radar event $event")
        }
    }

    private fun addTiles(map: MGLMapView, event: RouteMapViewModel.AddTilesEvent) {
        map.style?.let { style ->
            if (!style.sources.map { it.identifier }.contains(event.name)) {
                val tsource = event.source
                val source = MGLVectorTileSource(
                    event.name, tsource.tiles.toNSArray(),
                    NSDictionary.fromStringMap(
                        mapOf(
                            MGLTileSourceOption.Keys.MaximumZoomLevel().toString() to NSNumber.valueOf(tsource.maxZoom),
                            MGLTileSourceOption.Keys.MinimumZoomLevel().toString() to NSNumber.valueOf(tsource.minZoom)
                        )
                    )
                )
                style.addSource(source)
            }
        }
    }


    private fun processMapEvent(event: MapViewModel.MapViewEvent) {
        when (event) {
            is MapViewModel.SetNoteTextEvent -> noteText.text = event.text
            is MapViewModel.BoundsEvent -> {
                val bounds = MGLCoordinateBounds(
                    CLLocationCoordinate2D(event.bounds.south, event.bounds.west),
                    CLLocationCoordinate2D(event.bounds.north, event.bounds.east)
                )
                mapView.setVisibleCoordinateBounds(bounds, UIEdgeInsets(15.0, 15.0, 15.0, 15.0), true) {}
            }
            is MapViewModel.OffsetEvent -> {
                mapView.contentInset = UIEdgeInsets(
                    event.top * view.bounds.height,
                    event.left * view.bounds.width,
                    0.0,
                    0.0
                )
            }
            is MapViewModel.CenterEvent -> {
                val newCamera = mapView.camera
                if (event.direction != INVALID_DATA)
                    newCamera.heading = event.direction
                newCamera.pitch = event.pitch
                if (event.center.valid) {
                    newCamera.centerCoordinate = CLLocationCoordinate2D(event.center.latitude, event.center.longitude)
                    if (event.zoom != INVALID_DATA)
                        newCamera.altitude = MGLGeometry.altitudeForZoomLevel(
                            event.zoom, event.pitch, event.center.latitude, view.bounds.size
                        )
                }
                mapView.setCamera(newCamera, event.animation.toMillis() / 1000.0, timingFunction)
            }
            is MapViewModel.MarkerPositionEvent -> {
                if (event.location.heading >= -180.0)
                    flightMarker.rotation = event.location.heading - (mapView.camera?.heading
                        ?: 0.0)
                flightMarker.position = Position.from(event.location)
                if (flightMarker.lastLocation.valid)
                    posAnnotation.coordinate = flightMarker.lastLocation.asCLLocation2D()
                if (lastUpdateTimestamp != 0.0)
                    lastUpdateInterval = (CFDate.getCurrentAbsoluteTime() - lastUpdateTimestamp).coerceAtMost(2.0)
                lastUpdateTimestamp = CFDate.getCurrentAbsoluteTime()
            }
            is MapViewModel.MenuEvent -> {
                val where = event.where
                val rect = if (where is UIView) IosCxRect(where.frame) else null
                IosMenu.showMenu(this, event.title, event.entries, rect)
            }
            else -> debug("Unknown map event $event")
        }
    }

    override fun viewDidLoad() {
        @Suppress("DEPRECATION")
        if (isIos11)
            mapView.setAutomaticallyAdjustsContentInset(false)
        else
            setAutomaticallyAdjustsScrollViewInsets(false)
        //mapView.autoresizingMask = UIViewAutoresizing.with(FlexibleWidth, FlexibleHeight)
        mapView.isRotateEnabled = false
        mapView.attributionButton.isHidden = true
        mapView.styleURL = NSURL(presenter.style)
        mapView.delegate = delegate
        //mapView.addView(logo)
        mapView.compassView.isUserInteractionEnabled = false
        layerButton.isUserInteractionEnabled = true
        layerButton.setTranslatesAutoresizingMaskIntoConstraints(false)
        layerButton.addGestureRecognizer(UITapGestureRecognizer { presenter.onLayerClick() })
        view.addSubview(layerButton)
        NSLayoutConstraint.activateConstraints(
            NSArray(
                layerButton.leftAnchor.equalTo(view.leftAnchor, 4.0),
                layerButton.topAnchor.equalTo(view.topAnchor, 4.0)
            )
        )
        locationButton.isUserInteractionEnabled = true
        locationButton.setTranslatesAutoresizingMaskIntoConstraints(false)
        locationButton.addGestureRecognizer(UITapGestureRecognizer { presenter.onLocationClick(locationButton) })
        view.addSubview(locationButton)
        NSLayoutConstraint.activateConstraints(
            NSArray(
                locationButton.rightAnchor.equalTo(view.rightAnchor, -4.0),
                locationButton.bottomAnchor.equalTo(view.bottomAnchor, -4.0)
            )
        )
        noteText.font = UIFont.getSystemFont(14.0)
        noteText.textColor = UIColor.black()
        noteText.backgroundColor = UIColor.clear()
        noteText.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.addSubview(noteText)
        NSLayoutConstraint.activateConstraints(
            NSArray(
                noteText.centerXAnchor.equalTo(view.centerXAnchor),
                noteText.topAnchor.equalTo(view.topAnchor, 4.0)
            )
        )

    }

    override fun loadView() {
        view = mapView
    }

    override fun viewWillAppear(animated: Boolean) {
        super.viewWillAppear(animated)
        disposable = waitForMap()
            .flatMapObservable {
                // add the traffic icon images
                it.style.apply {
                    RouteMapViewModel.TrafficIcon.values().forEach {
                        setImage(UIImage.getImage(it.imageFile), it.name)
                    }
                    addSource(trafficSource)
                    val trafficLayer = MGLSymbolStyleLayer("traffic", trafficSource)
                    trafficLayer.iconImageName = NSExpression.createForKeyPath("icon")
                    trafficLayer.text = NSExpression.createForKeyPath("text")
                    trafficLayer.iconRotation = NSExpression.createForKeyPath("rotation")
                    trafficLayer.iconAllowsOverlap = NSExpression.createForConstantValue(NSNumber.valueOf(true))
                    trafficLayer.textAnchor =
                        NSExpression.createForConstantValue(NSString(MGLTextAnchor.BottomLeft.name))
                    trafficLayer.textOffset =
                        NSExpression.createForConstantValue(create(CGVector(1.0, -1.0)))
                    addLayer(trafficLayer)
                }
                presenter.observable
            }
            .filter { ApplicationState.current.data.visible && !view.bounds.isEmpty }
            .subscribe(
                {
                    when (it) {
                        is RouteMapViewModel.AddTilesEvent -> addTiles(mapView, it)
                        is RouteMapViewModel.RouteMapViewEvent -> processMapEvent(it)
                        is RouteMapViewModel.TrafficEvent -> processTraffic(it)
                        else -> processMapEvent(it)
                    }
                }, CJLog::logException
            )
    }

    override fun viewWillDisappear(animated: Boolean) {
        super.viewWillDisappear(animated)
        disposable.dispose()
    }
}

private fun Position.asCLLocation2D(): CLLocationCoordinate2D {
    return CLLocationCoordinate2D(latitude, longitude)
}

//private fun Location.asCLLocation(): CLLocation {
//return CLLocation(this.asCLLocation2D(), altitude, horizontalAccuracy, verticalAccuracy, track, speed, NSDate(timestamp.toEpochMilli() / 1000.0))
//}

//private fun UIEdgeInsets.string(): String {
//return "left=$left, top=$top, right=$right, bottom=$bottom"
//}

private fun CLLocationCoordinate2D?.toPosition(): Position {
    if (this == null)
        return Position.invalid
    return Position.of(latitude, longitude)
}
