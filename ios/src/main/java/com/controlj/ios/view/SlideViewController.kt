package com.controlj.ios.view

import com.controlj.animation.AnimationController
import com.controlj.framework.ApplicationBusy
import com.controlj.framework.ApplicationState
import com.controlj.framework.IosBackgroundServiceProvider.isIos10
import com.controlj.framework.IosBackgroundServiceProvider.isIos13
import com.controlj.framework.IosBackgroundServiceProvider.isIos15
import com.controlj.graphics.CRect
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.observeOnMainBy
import com.controlj.ui.UiAction
import com.controlj.view.ForegroundServiceProvider
import com.controlj.view.SlideViewPresenter
import com.controlj.view.SwipeListener
import io.reactivex.rxjava3.disposables.CompositeDisposable
import org.robovm.apple.coreanimation.CAAnimation
import org.robovm.apple.coreanimation.CADisplayLink
import org.robovm.apple.coreanimation.CAFrameRateRange
import org.robovm.apple.coregraphics.CGAffineTransform
import org.robovm.apple.coregraphics.CGPoint
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.coregraphics.CGSize
import org.robovm.apple.foundation.NSRunLoop
import org.robovm.apple.foundation.NSRunLoopMode
import org.robovm.apple.uikit.UIActivityIndicatorView
import org.robovm.apple.uikit.UIActivityIndicatorViewStyle
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIScreen
import org.robovm.apple.uikit.UISwipeGestureRecognizer
import org.robovm.apple.uikit.UISwipeGestureRecognizerDirection
import org.robovm.apple.uikit.UIView
import org.robovm.apple.uikit.UIViewAnimationOptions
import org.robovm.apple.uikit.UIViewController
import org.robovm.apple.uikit.UIViewControllerTransitionCoordinator

/**
 * Created by clyde on 15/4/18.
 */
abstract class SlideViewController(edges: SlideViewPresenter.Edges = SlideViewPresenter.Edges.ShortEdges) :
    UIViewController(), SlideViewPresenter.View {

    protected val views = ArrayList<UIView>()
    private val navBar = NavBar()
    protected var presenter: SlideViewPresenter
    protected var childViewController: UIViewController? = null
    protected var displayLink: CADisplayLink? = null
    protected var disposables = CompositeDisposable()
    protected val fgsp: ForegroundServiceProvider = IosForegroundServiceProvider(this)
    open protected val useAnimation = false      // subclasses should override this if animation is desired

    var barTitle: String
        get() = navBar.title
        set(value) {
            navBar.title = value
        }

    val recognizers: MutableList<UISwipeGestureRecognizer> = ArrayList()
    private val centerView = UIView()

    fun addSwipeRecognizer(listener: SwipeListener, uiview: UIView, direction: SwipeListener.Direction) {
        val recognizer = UISwipeGestureRecognizer { listener.onSwipe(direction) }
        recognizer.direction = when (direction) {
            SwipeListener.Direction.Left -> UISwipeGestureRecognizerDirection.Left
            SwipeListener.Direction.Up -> UISwipeGestureRecognizerDirection.Up
            SwipeListener.Direction.Down -> UISwipeGestureRecognizerDirection.Down
            SwipeListener.Direction.Right -> UISwipeGestureRecognizerDirection.Right
        }
        recognizers.add(recognizer)
        uiview.addGestureRecognizer(recognizer)
    }

    @Suppress("DEPRECATION")
    private val busyIndicator = UIActivityIndicatorView(
        if (isIos13) UIActivityIndicatorViewStyle.Large else UIActivityIndicatorViewStyle.WhiteLarge
    ).also {
        it.color = UIColor.blue()
        it.backgroundColor = UIColor.fromWhiteAlpha(1.0, 0.5)
        it.transform = CGAffineTransform.createScale(1.5, 1.5)
    }

    open protected fun processAction(action: UiAction): Boolean {
        logMsg("processAction $action")
        return fgsp.processAction(action)
    }

    /**
     * Set the busy indicator going, adding it to the child controller view if present.
     */
    open protected fun setBusy(state: Boolean) {
        if (state) {
            if (!busyIndicator.isDescendantOf(view)) {
                view.addSubview(busyIndicator)
                busyIndicator.center = CGPoint(view.bounds.width / 2, view.bounds.height / 2)
                busyIndicator.startAnimating()
            }
        } else {
            busyIndicator.stopAnimating()
            if (busyIndicator.isDescendantOf(view)) {
                busyIndicator.removeFromSuperview()
            }
        }
    }

    override fun viewDidLoad() {
        super.viewDidLoad()
        ApplicationState.current.changeObserver.subscribe { updateState(it.second) }
    }

    @Suppress("DEPRECATION")
    open protected fun updateState(value: ApplicationState) {
        // transition from visible to active
        // become visible
        when {
            value.visible && disposables.size() == 0 -> {
                setBusy(false)
                disposables.add(ApplicationBusy.listener.observeOnMainBy { setBusy(it) })
                disposables.add(UiAction.observer.observeOnMainBy(::logException) { processAction(it) })

                displayLink?.invalidate()
                if (useAnimation)
                    displayLink = CADisplayLink {
                        AnimationController.onFrame(CAAnimation.getCurrentMediaTime())
                    }.apply {
                        if (isIos15)
                            preferredFrameRateRange = CAFrameRateRange(5f, 20f, 20f)
                        else
                            preferredFramesPerSecond = 20
                        addToRunLoop(NSRunLoop.getCurrent(), NSRunLoopMode.Default)
                    }
            }

            !value.visible -> {
                setBusy(false)
                displayLink?.invalidate()
                disposables.clear()
            }
        }
    }

    fun setViews(mview: UIView, view1: UIView, view2: UIView) {
        recognizers.clear()
        views.forEach { it.removeFromSuperview() }
        centerView.subviews.forEach { it.removeFromSuperview() }
        centerView.addSubview(mview)
        views.clear()
        views.add(mview)
        views.add(view1)
        views.add(view2)
        view.addSubview(view1)
        view.addSubview(view2)
        presenter.swipeListeners.forEachIndexed { idx, listener ->
            addSwipeRecognizer(listener, views[idx + 1], SwipeListener.Direction.Left)
            addSwipeRecognizer(listener, views[idx + 1], SwipeListener.Direction.Right)
            addSwipeRecognizer(listener, views[idx + 1], SwipeListener.Direction.Up)
            addSwipeRecognizer(listener, views[idx + 1], SwipeListener.Direction.Down)
        }
    }

    /**
     * Width of the first subview as a fraction
     */
    var leftFrac: Double = 0.12

    /**
     * Width of the second subview.
     */
    var rightFrac: Double = 0.12

    init {
        presenter = SlideViewPresenter(this, edges, leftFrac, rightFrac)
        navBar.listener = { showController(null) }
    }

    override fun loadView() {
        view = object : UIView(UIScreen.getMainScreen().bounds) {
            override fun layoutSubviews() {
                presenter.onMeasure(bounds.width.toInt(), bounds.height.toInt())
                val centerBounds = presenter.childRects[0].toCGRect()
                centerView.frame = centerBounds
                views[0].frame = centerView.bounds
                views[1].frame = presenter.childRects[1].toCGRect()
                views[2].frame = presenter.childRects[2].toCGRect()
                navBar.frame = CGRect(CGPoint.Zero(), CGSize(centerBounds.width, NavBar.HEIGHT))
                centerView.subviews.filterNot { it == navBar || it == views[0] }.forEach {
                    it.frame = CGRect(
                        0.0,
                        navBar.bounds.height,
                        centerBounds.width,
                        centerBounds.height - navBar.bounds.height
                    )
                }
            }
        }
        view.setClipsToBounds(true)
        view.addSubview(centerView)
    }

    override fun doLayout() {
        UIView.animate(
            SlideViewPresenter.ANIMATION_DURATION,
            0.0,
            UIViewAnimationOptions.CurveEaseOut,
            {
                view.setNeedsLayout()
                view.layoutIfNeeded()
            })
        {
        }
    }

    override fun viewWillTransitionToSize(newSize: CGSize, coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(newSize, coordinator)
        coordinator.animateAlongsideTransition({
            view.frame = CGRect(CGPoint.Zero(), newSize)
            view.setNeedsLayout()
            view.layoutIfNeeded()
        }) {
        }
    }

    protected open fun showController(controllerClass: Class<out UIViewController>?) {
        if (childViewController?.javaClass != controllerClass) {
            TextFieldDelegate.hideKeyboard()
            if (controllerClass == null) {
                childViewController?.let {
                    UIView.animate(0.5, 0.0, UIViewAnimationOptions.CurveEaseOut, {
                        childViewController?.apply {
                            view.frame = view.frame.offset(-view.bounds.width, 0.0)
                            navBar.frame = navBar.frame.offset(-view.bounds.width, 0.0)
                        }
                    }) {
                        childViewController?.view?.removeFromSuperview()
                        childViewController?.removeFromParentViewController()
                        childViewController = null
                        NavBar.instance = null
                        navBar.removeFromSuperview()
                        presenter.popState()
                    }
                }
                return
            }
            val controller = controllerClass.newInstance()
            if (childViewController == null) presenter.pushState()
            presenter.state = SlideViewPresenter.State.Normal
            presenter.onMeasure(view.bounds.width.toInt(), view.bounds.height.toInt())
            controller?.apply {
                view.frame = CGRect(
                    centerView.bounds.minX - centerView.bounds.width,
                    centerView.bounds.minY + NavBar.HEIGHT,
                    centerView.bounds.width,
                    centerView.bounds.height - NavBar.HEIGHT
                )
            }
            UIView.animate(0.5, 0.0, UIViewAnimationOptions.CurveEaseOut, {
                navBar.removeFromSuperview()
                centerView.addSubview(navBar)
                navBar.title = controller.title ?: ""
                navBar.setBusy(false)
                NavBar.instance = navBar
                addChildViewController(controller)
                centerView.addSubview(controller.view)
                view.setNeedsLayout()
                view.layoutIfNeeded()
                childViewController?.apply {
                    view.frame = view.frame.offset(view.bounds.width, 0.0)
                }
            }) {
                childViewController?.view?.removeFromSuperview()
                childViewController?.removeFromParentViewController()
                childViewController = controller
            }
        } else
            presenter.state = SlideViewPresenter.State.Normal
    }
}

fun CRect.toCGRect(): CGRect {
    return CGRect(left, top, width, height)
}
