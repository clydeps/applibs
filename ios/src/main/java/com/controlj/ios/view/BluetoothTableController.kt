package com.controlj.ios.view

import com.controlj.ble.BleScanResult
import com.controlj.ios.ColorCompatibility
import com.controlj.ios.uIImage
import com.controlj.layout.Gravity
import com.controlj.layout.HorizontalGroup
import com.controlj.layout.Layout
import com.controlj.layout.VerticalGroup.Companion.verticalGroup
import com.controlj.layout.View
import com.controlj.shim.CxPoint
import com.controlj.shim.IosCxColor
import com.controlj.shim.addView
import com.controlj.view.BluetoothListPresenter
import com.controlj.view.ListPresenter
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIFont
import org.robovm.apple.uikit.UIImage
import org.robovm.apple.uikit.UIImageView
import org.robovm.apple.uikit.UILabel
import java.util.HashMap

/**
 * Created by clyde on 4/5/17.
 */

open class BluetoothTableController() : TableController<BleScanResult, BluetoothListPresenter>() {
    override val presenter = BluetoothListPresenter()
    private val signalBars = HashMap<String, UIImage>()
    private val bluetooth: UIImage = "Bluetooth".uIImage

    override fun loadView() {
        uiTableView.setDelaysContentTouches(false)
        uiTableView.backgroundColor = ColorCompatibility.systemBackground
        title = "Bluetooth Management"
        BleScanResult.signalBarNames.forEach { signalBars.put(it, it.uIImage) }
        view = uiTableView
    }

    private inner class RowView(item: BleScanResult) : BaseRowView(item) {
        val blueImage: UIImageView = UIImageView()
        val nameView: UILabel = UILabel()
        val addressView: UILabel = UILabel()
        val signalText: UILabel = UILabel()
        val signalImage: UIImageView = UIImageView()
        override var events: Set<View.Event> = setOf(View.Event.TAP)

        override fun onTap(position: CxPoint): Boolean {
            presenter.onItemSelected(item)
            return true
        }

        init {
            signalText.textColor = ColorCompatibility.label
            add(HorizontalGroup.horizontalGroup {
                name = "outer"
                spacing = 4.0
                layout.widthMode = Layout.Mode.MatchParent
                layout.margin = 2.0
                layer.backgroundColor = IosCxColor(ColorCompatibility.systemBackground)
                addView(
                    blueImage, Layout(width = 36.0, height = 36.0, gravity = Gravity.Center),
                    name = "blueImage"
                )
                add(verticalGroup {
                    name = "signalLayout"
                    layout.height = 72.0
                    layout.heightMode = Layout.Mode.Absolute
                    layout.margin = 4.0
                    spacing = 4.0
                    layout.widthMode = Layout.Mode.Weighted
                    layout.weight = 0.15
                    addView(
                        signalImage, Layout(
                            width = 16.0,
                            height = 16.0,
                            gravity = Gravity.Center
                        ), name = "signaImage"
                    )
                    addView(
                        signalText, Layout(
                            widthMode = Layout.Mode.MatchParent,
                            heightMode = Layout.Mode.Weighted,
                            gravity = Gravity.Center,
                            weight = 1.0
                        ), name = "signalText"
                    )
                })
                addDivider()
                add(verticalGroup {
                    name = "textGroup"
                    spacing = 4.0
                    layout.height = 72.0
                    layout.heightMode = Layout.Mode.Absolute
                    layout.widthMode = Layout.Mode.Weighted
                    layout.weight = 1.0
                    addView(
                        nameView, Layout(
                            heightMode = Layout.Mode.Weighted,
                            weight = 1.0,
                            widthMode = Layout.Mode.MatchParent
                        ), name = "nameView"
                    )
                    addView(
                        addressView, Layout(
                            heightMode = Layout.Mode.Weighted,
                            weight = 1.0,
                            widthMode = Layout.Mode.MatchParent
                        ), name = "addressView"
                    )
                })
                nameView.textAlignment = NSTextAlignment.Left
                addressView.textAlignment = NSTextAlignment.Left
            })
            val data = presenter.textValues(item)
            if (data.isEmpty()) {
                nameView.text = "No Bluetooth devices found"
                nameView.textColor = ColorCompatibility.placeholderText
                addressView.text = "Pull down to refresh"
                addressView.textColor = ColorCompatibility.placeholderText
                signalText.text = ""
                blueImage.image = null
                signalImage.isHidden = true
            } else {
                nameView.text = data[BluetoothListPresenter.DEV_NAME]
                nameView.textColor = ColorCompatibility.label
                addressView.text = data[BluetoothListPresenter.DEV_ADDRESS]
                addressView.textColor = ColorCompatibility.label
                signalText.text = data[BluetoothListPresenter.DEV_RSSI]
                signalImage.image = signalBars[data[BluetoothListPresenter.DEV_BARS]]
                blueImage.image = bluetooth
                blueImage.isHidden = false
                signalImage.isHidden = false
            }
        }
    }

    private inner class HeaderView(rowIndex: ListPresenter.RowIndex) :
        HorizontalGroup(Layout(widthMode = Layout.Mode.MatchParent, gravity = Gravity.Center)) {
        init {
            layer.backgroundColor = IosCxColor(ColorCompatibility.secondarySystemBackground)
            spacing = 4.0
            UILabel().also {
                it.textColor = it.tintColor
                it.font = UIFont.getBoldSystemFont(16.0)
                val textValues = presenter.textValues(rowIndex.section)
                it.text = textValues[BluetoothListPresenter.DEV_NAME]
                addView(it, Layout(
                    widthMode = Layout.Mode.Weighted,
                    weight = 40.0,
                    gravity = Gravity.MiddleLeft
                ).also { it.margins.left = 24.0 }, "header"
                )
            }
        }


    }

    override fun getHeaderLayout(rowIndex: ListPresenter.RowIndex): View {
        return HeaderView(rowIndex)
    }

    override fun getRowLayout(item: BleScanResult): BaseRowView {
        return RowView(item)
    }

    override fun prefersStatusBarHidden(): Boolean {
        return true
    }

    companion object {
        private val MIN_ROWS = 1
    }

}
