package com.controlj.ios.view

import com.controlj.framework.IosBackgroundServiceProvider.isIos13
import com.controlj.layout.Gravity
import com.controlj.layout.Layout
import com.controlj.shim.addView
import com.controlj.ui.DialogData
import com.controlj.ui.StatusDialogItem
import org.robovm.apple.uikit.UIActivityIndicatorView
import org.robovm.apple.uikit.UIActivityIndicatorViewStyle

class StatusEntry(item: StatusDialogItem, dialogData: DialogData) : LabelledEntry(item, dialogData) {
    @Suppress("DEPRECATION")
    val busyView = UIActivityIndicatorView(
            if (isIos13) UIActivityIndicatorViewStyle.Medium else UIActivityIndicatorViewStyle.Gray)

    init {
        busyView.setHidesWhenStopped(true)
        addView(busyView, Layout(gravity = Gravity.Center))
    }

    override fun refresh(): Boolean {
        super.refresh()
        if ((item as? StatusDialogItem)?.busy == true)
            busyView.startAnimating()
        else
            busyView.stopAnimating()
        return true
    }
}
