package com.controlj.ios.view

import com.controlj.ios.ColorCompatibility
import com.controlj.layout.Gravity
import com.controlj.layout.Layout
import com.controlj.shim.addView
import com.controlj.ui.DialogData
import com.controlj.ui.TextValueItem
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UITextView
import org.robovm.apple.uikit.UITextViewDelegateAdapter

class TextViewEntry(item: TextValueItem, dialogData: DialogData) : LabelledEntry(item, dialogData) {
    val textField = UITextView()

    init {
        widget = textField
        textField.apply {

            layer.borderColor = ColorCompatibility.secondaryLabel.cgColor
            backgroundColor = ColorCompatibility.systemBackground
            textColor = ColorCompatibility.label
            text = item.value
            textAlignment = if (item.inputStyle.rightAlign)
                NSTextAlignment.Right
            else
                NSTextAlignment.Natural
            font = item.textStyle.uiFont
            contentGroup.addView(
                this, Layout(
                    heightMode = Layout.Mode.Absolute,
                    height = item.lines * font.lineHeight * 1.1,
                    widthMode = Layout.Mode.MatchParent,
                    gravity = Gravity.MiddleRight
                )
            )
            setDelegate(object : UITextViewDelegateAdapter() {
                override fun didEndEditing(textView: UITextView?) {
                    if (text != item.value) {
                        item.value = textField.text
                        item.onChanged()
                        dialogData.refresh()
                    }
                }
            })
        }
    }

    override fun refresh(): Boolean {
        var changed = super.refresh()
        if (textField.text != item.value) {
            changed = true
            textField.text = item.value
        }
        if (textField.isEditable != item.isEnabled) {
            changed = true
            textField.isEditable = item.isEnabled
        }
        return changed
    }
}
