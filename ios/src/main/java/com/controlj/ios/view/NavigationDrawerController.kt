package com.controlj.ios.view

import com.controlj.framework.IosBackgroundServiceProvider
import com.controlj.graphics.CRect
import com.controlj.layout.Gravity
import com.controlj.layout.Layout
import com.controlj.layout.Layout.Companion.MAX_DIMENSION
import com.controlj.layout.UxScrollViewHost
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.MainScheduler
import com.controlj.shim.IosCxRect
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.view.MenuEntry
import com.controlj.view.NavigationDrawer
import com.controlj.view.NavigationDrawer.Companion.containerViewMaxAlpha
import com.controlj.view.NavigationDrawer.Companion.drawerAnimationDuration
import com.controlj.view.NavigationDrawer.Position
import com.controlj.view.NavigationDrawer.State
import io.reactivex.rxjava3.core.Single
import org.robovm.apple.coregraphics.CGPoint
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.coregraphics.CGSize
import org.robovm.apple.uikit.UIApplication
import org.robovm.apple.uikit.UIApplicationState
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIEdgeInsets
import org.robovm.apple.uikit.UIGestureRecognizer
import org.robovm.apple.uikit.UIGestureRecognizerDelegateAdapter
import org.robovm.apple.uikit.UIGestureRecognizerState
import org.robovm.apple.uikit.UIRectEdge
import org.robovm.apple.uikit.UIScreen
import org.robovm.apple.uikit.UIScreenEdgePanGestureRecognizer
import org.robovm.apple.uikit.UIStatusBarStyle
import org.robovm.apple.uikit.UISwipeGestureRecognizer
import org.robovm.apple.uikit.UISwipeGestureRecognizerDirection
import org.robovm.apple.uikit.UITapGestureRecognizer
import org.robovm.apple.uikit.UIView
import org.robovm.apple.uikit.UIViewAnimationOptions
import org.robovm.apple.uikit.UIViewAnimationOptions.BeginFromCurrentState
import org.robovm.apple.uikit.UIViewAnimationOptions.CurveEaseOut
import org.robovm.apple.uikit.UIViewController
import org.robovm.apple.uikit.UIViewControllerTransitionCoordinator
import kotlin.math.abs
import kotlin.math.max

/**
 * Created by clyde on 13/4/18.
 */
open class NavigationDrawerController(
        position: Position = Position.LEFT,
        final override var menu: DialogData
) : UIViewController(), NavigationDrawer, DialogData.DialogShower {

    companion object {
        const val DRAWER_WIDTH = 300.0
    }

    var drawerWidth = DRAWER_WIDTH
    private var dialogView: DialogView = DialogView(menu, drawerWidth)
    private val overlayView = UIView(UIScreen.getMainScreen().bounds)
    private val drawerView = object : UxScrollViewHost() {
        override fun touchesShouldCancelInContentView(uiview: UIView?): Boolean {
            return true
        }
    }
    private val grDelegate = object : UIGestureRecognizerDelegateAdapter() {
        override fun shouldRecognizeSimultaneously(gestureRecognizer: UIGestureRecognizer?, otherGestureRecognizer: UIGestureRecognizer?): Boolean {
            return true
        }
    }
    private var closedFrame = CGRect()
    private var openFrame = CGRect()
    private var firstX = 0.0
    var position: Position = position
        set(value) {
            if (field != value) {
                if (state != State.UNDEFINED)
                    view.setNeedsLayout()
                field = value
            }
        }
    var state: State = State.UNDEFINED
        set(value) {
            menu.refresh()
            when (value) {
                State.CLOSED, State.UNDEFINED -> {
                    //overlayView.isHidden = true
                    drawerView.frame = closedFrame
                    drawerView.frameGroup.layout.width = closedFrame.width
                }
                State.OPENING -> {
                    overlayView.isHidden = false
                }
                State.OPEN -> {
                    drawerView.frame = openFrame
                    drawerView.frameGroup.layout.width = openFrame.width
                    overlayView.isHidden = false
                }
                else -> {
                }
            }
            field = value
            view.setNeedsDisplay()
        }

    init {
        dialogView.layout.gravity = Gravity.TopLeft
        dialogView.layout.heightMode = Layout.Mode.MatchParent
        drawerView.setShowsHorizontalScrollIndicator(false)
        drawerView.setShowsVerticalScrollIndicator(false)
    }

    override fun getPreferredStatusBarStyle(): UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

    @Suppress("DEPRECATION")
    private fun getInsets(): UIEdgeInsets {
        return if (IosBackgroundServiceProvider.isIos11)
            view.safeAreaInsets
        else
            UIEdgeInsets(UIApplication.getSharedApplication().statusBarFrame.height, 0.0, 0.0, 0.0)
    }

    override fun viewWillTransitionToSize(newSize: CGSize, coordinator: UIViewControllerTransitionCoordinator?) {
        super.viewWillTransitionToSize(newSize, coordinator)
        // suppress spurious calls when going into background
        if (UIApplication.getSharedApplication().applicationState != UIApplicationState.Background) {
            logMsg("viewWillTransition to size %s", newSize.toString())
            coordinator?.animateAlongsideTransition({
                view.frame = CGRect(CGPoint.Zero(), newSize)
                view.setNeedsLayout()
                view.layoutIfNeeded()
            }) {
                state = state
                view.setNeedsLayout()
                view.layoutIfNeeded()
            }
        }
    }

    private var childFrame = CGRect.Zero()

    private fun setFrames() {
        val inner = view.bounds.inset(getInsets())
        overlayView.frame = inner
        val imageWidth = dialogView.buttonHeight
        val barWidth = if (inner.width < 640) 0.0 else imageWidth
        //debug("aspect ratio = $aspectRatio, barWidth = $barWidth")
        dialogView.onMeasure(DRAWER_WIDTH, MAX_DIMENSION)
        drawerView.contentSize = CGSize(imageWidth, max(dialogView.measuredSize.height, inner.height))
        when (position) {
            Position.LEFT -> {
                closedFrame = CGRect(inner.minX, inner.minY, barWidth, inner.height)
                openFrame = CGRect(inner.minX, inner.minY, DRAWER_WIDTH, inner.height)
                childFrame = CGRect(inner.minX + barWidth, inner.minY, inner.width - barWidth, inner.height)
            }
            Position.RIGHT -> {
                closedFrame = CGRect(inner.maxX - barWidth, inner.minY, barWidth, inner.height)
                openFrame = CGRect(inner.maxX - DRAWER_WIDTH, inner.minY, DRAWER_WIDTH, inner.height)
                childFrame = CGRect(inner.minX, inner.minY, inner.width - barWidth, inner.height)
            }
        }
        //    debug("setframes(): main %s, child %s, closed %s, open %s", view.frame.toString(), childFrame.toString(), closedFrame.toString(), openFrame.toString())
    }

    override fun viewWillLayoutSubviews() {
        setFrames()
        childViewControllers.forEach {
            if (it.view.frame != childFrame)
                it.view.frame = childFrame
        }
        state = state
    }

    override fun loadView() {
        view = UIView(UIScreen.getMainScreen().bounds)
        state = State.CLOSED
    }

    val edgeSwipeOpener = UIScreenEdgePanGestureRecognizer(this::swipeOpen)
    val tapCloser = UITapGestureRecognizer { closeDrawer() }
    val swipeCloser = UISwipeGestureRecognizer { closeDrawer() }
    val swipeOpener = UISwipeGestureRecognizer { openDrawer() }

    override fun viewDidLoad() {
        super.viewDidLoad()
        //logMsg("viewDidLoad")
        view.backgroundColor = UIColor.blue()
        view.alpha = 1.0
        drawerView.frameGroup.add(dialogView)
        menu.shower = this
        state = State.CLOSED
        overlayView.backgroundColor = UIColor.black()
        overlayView.alpha = 0.0
        drawerView.alpha = 1.0
        edgeSwipeOpener.delegate = grDelegate
        swipeOpener.delegate = grDelegate
        swipeCloser.delegate = grDelegate
        view.setTranslatesAutoresizingMaskIntoConstraints(false)
        drawerView.setTranslatesAutoresizingMaskIntoConstraints(false)
        overlayView.setTranslatesAutoresizingMaskIntoConstraints(false)
    }

    override fun viewWillAppear(p0: Boolean) {
        super.viewWillAppear(p0)
        view.addSubview(overlayView)
        view.addSubview(drawerView)
        edgeSwipeOpener.edges = if (position == Position.LEFT) UIRectEdge.Left else UIRectEdge.Right
        view.addGestureRecognizer(edgeSwipeOpener)
        overlayView.addGestureRecognizer(tapCloser)
        swipeCloser.direction = if (position == Position.LEFT) UISwipeGestureRecognizerDirection.Left else UISwipeGestureRecognizerDirection.Right
        drawerView.addGestureRecognizer(swipeCloser)
        drawerView.setDelaysContentTouches(false)
        swipeOpener.direction = if (position == Position.RIGHT) UISwipeGestureRecognizerDirection.Left else UISwipeGestureRecognizerDirection.Right
        drawerView.addGestureRecognizer(swipeOpener)
        closeDrawer()
    }

    override fun viewWillDisappear(p0: Boolean) {
        super.viewWillDisappear(p0)
        listOf(view, drawerView, overlayView).forEach { v ->
            v.gestureRecognizers.forEach { v.removeGestureRecognizer(it) }
        }
        view.subviews.remove(overlayView)
        view.subviews.remove(drawerView)
    }

    override fun addChildViewController(child: UIViewController) {
        super.addChildViewController(child)
        if (view.subviews.contains(overlayView)) {
            view.subviews.remove(overlayView)
            view.subviews.remove(drawerView)
        }
        view.addSubview(child.view)
        view.addSubview(overlayView)
        view.addSubview(drawerView)
        child.didMoveToParentViewController(this)
    }

    private var openFraction: Double = 0.0
        set(value) {
            val frac = value.coerceAtMost(1.0)
            field = frac
            val dwidth = closedFrame.width + (openFrame.width - closedFrame.width) * frac
            overlayView.alpha = frac * containerViewMaxAlpha
            when (position) {
                Position.LEFT -> {
                    drawerView.frame = CGRect(openFrame.origin, CGSize(dwidth, openFrame.height))
                }
                Position.RIGHT -> {
                    drawerView.frame = CGRect(CGPoint(openFrame.maxX - dwidth, openFrame.minY), CGSize(dwidth, openFrame.height))
                }
            }
            drawerView.setNeedsDisplay()
        }
        get() = (drawerView.bounds.width - closedFrame.width) / (openFrame.width - closedFrame.width)

    fun openDrawer() {
        when (state) {
            State.CLOSED, State.CLOSING, State.OPENING -> {
                state = State.OPENING
                drawerView.layer.removeAllAnimations()
                UIView.animate(drawerAnimationDuration, 0.0, UIViewAnimationOptions.with(CurveEaseOut, BeginFromCurrentState), {
                    drawerView.frame = openFrame
                    overlayView.alpha = containerViewMaxAlpha
                }) {
                    state = State.OPEN
                }
            }
            else -> {
            }
        }
    }

    fun closeDrawer() {
        //debug("Drawer closing")
        when (state) {
            State.OPEN, State.OPENING -> {
                val frac = if (state == State.OPEN) 1.0 else openFraction
                state = State.CLOSING
                drawerView.layer.removeAllAnimations()
                UIView.animate(drawerAnimationDuration * frac, 0.0, UIViewAnimationOptions.with(CurveEaseOut, BeginFromCurrentState), {
                    drawerView.frame = closedFrame
                    overlayView.alpha = 0.0
                }) {
                    state = State.CLOSED
                }
            }
            else -> {
            }
        }
    }

    /**
     * Called when a swipe from the
     */
    fun swipeOpen(recognizer: UIGestureRecognizer) {
        if (recognizer is UIScreenEdgePanGestureRecognizer) {
            //logMsg("swipe state %s, velocit %s", recognizer.state.toString(), recognizer.getVelocity(view).toString())
            when (recognizer.state) {
                UIGestureRecognizerState.Changed -> {
                    val x = recognizer.getLocationInView(overlayView).x
                    openFraction = abs(x - firstX) / (drawerWidth - closedFrame.width)
                }
                UIGestureRecognizerState.Began -> {
                    state = State.OPENING
                    firstX = recognizer.getLocationInView(overlayView).x
                }
                UIGestureRecognizerState.Ended -> {
                    if (openFraction >= 0.4 || recognizer.getVelocity(view).x > 200.0)
                        MainScheduler(this::openDrawer)
                    else
                        MainScheduler(this::closeDrawer)
                }
                UIGestureRecognizerState.Cancelled -> MainScheduler(this::closeDrawer)
                else -> {
                }
            }
        }
    }

    /**
     * Show a menu with the supplied title and list.
     */
    override fun showMenu(title: String, items: List<MenuEntry>, source: DialogItem?) {
        val rect: IosCxRect? = if (source != null)
            dialogView.rectFor(source) as IosCxRect
        else
            null
        IosMenu.showMenu(this, title, items, rect)
    }

    override fun dismiss() {
        closeDrawer()
    }

    override fun refresh(source: DialogItem?) {
        dialogView.redraw()
        view.setNeedsDisplay()
    }

    override fun showTextBox(
            title: String,
            value: String,
            style: DialogItem.InputStyle,
            source: CRect,
            timeout: Long
    ): Single<String> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
