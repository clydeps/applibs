package com.controlj.ios.view

import com.controlj.ios.selectAll
import com.controlj.layout.Gravity
import com.controlj.layout.HorizontalGroup
import com.controlj.layout.Layout
import com.controlj.settings.Setting
import com.controlj.settings.UnitSetting
import com.controlj.shim.addView
import com.controlj.ui.DialogItem
import com.controlj.utility.Units
import org.robovm.apple.uikit.UITextField
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 4/1/19.
 */
class UnitValueView(
        category: String = "",
        decimalPlaces: Int = 0,
        val readOnly: Boolean = false,
        layout: Layout = Layout()
) : HorizontalGroup(layout) {
    private var backingValue: Double = 0.0
    var value: Double
        get() = backingValue
        set(value) {
            backingValue = value
            setValueText()
            if (!readOnly && selectAllOnFocus)
                valueText.selectAll()
        }
    var decimalPlaces: Int = decimalPlaces
        set(places) {
            if (places != field) {
                field = places
                setValueText()
            }
        }
    var selectAllOnFocus = true
    var listener: ((Units.Unit, Double) -> Unit)? = null
    private val valueText = UITextField()
    private val delegate = TextFieldDelegate(valueText, DialogItem.InputStyle.SIGNEDDECIMAL)
    val selector = Spinner(listOf<Any>()) {
        setValueText()
        listener?.invoke(currentUnit, value)
    }
    private val _units = mutableListOf(Units.Unit.IDENTITY)
    var currentUnit: Units.Unit
        get() = _units[selector.selected]
        set(value) {
            selector.selected = _units.indexOf(value)
            setValueText()
        }

    var enabled: Boolean
        get() = selector.isUserInteractionEnabled
        set(value) {
            selector.isUserInteractionEnabled = value
            valueText.isEnabled = value
        }
    var units: Iterable<Units.Unit>
        get() = _units
        set(value) {
            _units.clear()
            _units.addAll(value)
            selector.list = _units
        }

    fun setUnits(unitSetting: UnitSetting) {
        units = unitSetting.choices
        currentUnit = unitSetting.value
    }

    init {
        addView(valueText, Layout(widthMode = Layout.Mode.Weighted, gravity = Gravity.Center))
        addView(selector, Layout(gravity = Gravity.MiddleRight))
        if (category.isNotBlank()) {
            val unitSetting = Setting.get<Setting<*>>(category)
            if (unitSetting is UnitSetting) setUnits(unitSetting)
        }
        @Suppress("CHECK_RESULT")
        delegate.observe.debounce(500, TimeUnit.MILLISECONDS).subscribe { update() }
    }

    private fun update() {
        val value = valueText.text.toDoubleOrNull()
        if (value != null && value != backingValue) {
            backingValue = value
        }
    }

    private fun setValueText() {
        val newText = currentUnit.toString(backingValue, decimalPlaces)
        if (newText.toDoubleOrNull() != valueText.text.toDoubleOrNull()) {
            delegate.setText(newText)
        }
    }

    override fun toString(): String {
        val v = currentUnit.convertTo(value)
        return currentUnit.toString(v, decimalPlaces) + " " + currentUnit.unitName
    }
}
