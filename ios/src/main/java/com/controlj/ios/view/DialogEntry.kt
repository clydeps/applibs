package com.controlj.ios.view

import com.controlj.graphics.TextStyle
import com.controlj.ios.ColorCompatibility
import com.controlj.ios.graphics.IosTextStyle
import com.controlj.layout.FrameGroup
import com.controlj.layout.Gravity
import com.controlj.layout.IosUxHost
import com.controlj.layout.Layout
import com.controlj.layout.VerticalGroup
import com.controlj.layout.View
import com.controlj.layout.ViewGroup
import com.controlj.shim.CxRect
import com.controlj.shim.IosCxColor
import com.controlj.shim.IosCxRect
import com.controlj.shim.UxColor
import com.controlj.shim.addView
import com.controlj.ui.ButtonDialogItem
import com.controlj.ui.CViewItem
import com.controlj.ui.CheckboxItem
import com.controlj.ui.ClickableDialogItem
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.ui.GroupDialogItem
import com.controlj.ui.NumericDialogItem
import com.controlj.ui.ProgressDialogItem
import com.controlj.ui.RangeUnitDialogItem
import com.controlj.ui.SeparatorDialogItem
import com.controlj.ui.SpinnerDialogItem
import com.controlj.ui.StatusDialogItem
import com.controlj.ui.TextValueItem
import com.controlj.ui.ValueDialogItem
import com.controlj.widget.Checkbox
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIControlContentHorizontalAlignment
import org.robovm.apple.uikit.UIFont
import org.robovm.apple.uikit.UILabel
import org.robovm.apple.uikit.UIProgressView
import org.robovm.apple.uikit.UIProgressViewStyle
import org.robovm.apple.uikit.UITapGestureRecognizer

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-03-27
 * Time: 18:38
 *
 * Class for dialog/settings views
 *
 * The [item] is a [DialogItem] which provides the appearance and behaviour for the entry;
 * the [dialogData] is the containing dialog - needed for callbacks etc.
 * The class is an instance of a [ViewGroup] which can be added to a layout, via a [IosUxHost]
 */
interface DialogEntry : View {
    val item: DialogItem
    val dialogData: DialogData
    var enabled: Boolean
    val rect: CxRect
        get() = IosCxRect()

    /**
     * Update the view from the item. Return true if any changes were made
     */
    fun refresh(): Boolean

    companion object {
        var marginSize = 5.0

        /**
         * Factory method to create a [DialogEntry] from the a [DialogItem] item
         */
        fun create(item: DialogItem, dialogData: DialogData): DialogEntry {
            return when (item) {
                is RangeUnitDialogItem -> RangeUnitEntry(item, dialogData)
                is CViewItem -> create(item, dialogData)
                is ProgressDialogItem -> create(item, dialogData)
                is ClickableDialogItem -> create(item, dialogData)
                is NumericDialogItem -> create(item, dialogData)
                is SeparatorDialogItem -> create(item, dialogData)
                is ButtonDialogItem -> ButtonEntry(item, dialogData)
                is TextValueItem -> {
                    if (item.inputStyle == DialogItem.InputStyle.MULTI_LINE)
                        TextViewEntry(item, dialogData)
                    else
                        TextValueEntry(item, dialogData)
                }
                is SpinnerDialogItem<*> -> SpinnerEntry(item, dialogData)
                is CheckboxItem -> create(item, dialogData)
                is ValueDialogItem -> ValueItemEntry(item, dialogData)
                is GroupDialogItem -> create(item, dialogData)
                is StatusDialogItem -> StatusEntry(item, dialogData)
                else -> {
                    require(
                        item.style == DialogItem.Style.PLAIN
                    ) { "Creating DialogEntry from unhandled style ${item.style}, class is ${item.javaClass.name}" }
                    LabelledEntry(item, dialogData)
                }
            }
        }

        /**
         * Factory method to create a [DialogEntry] from the [CViewItem]
         */
        private fun create(cviewItem: CViewItem, dialogData: DialogData): DialogEntry {
            return object : BasicEntry(cviewItem, dialogData) {
                private val wrapper = CViewWrapper(cviewItem.cView)

                init {
                    addView(wrapper, Layout(widthMode = Layout.Mode.Weighted, gravity = Gravity.Center))
                }

                override fun refresh(): Boolean {
                    wrapper.redrawForeground()
                    return true
                }
            }
        }

        /**
         * Factory method to create a [DialogEntry] from the [ProgressDialogItem]
         */
        private fun create(item: ProgressDialogItem, dialogData: DialogData): DialogEntry {
            return object : BasicEntry(item, dialogData) {
                val view = UIProgressView(UIProgressViewStyle.Bar)

                init {
                    widget = view
                    addView(view, Layout(widthMode = Layout.Mode.Weighted))
                }

                override fun refresh(): Boolean {
                    super.refresh()
                    view.progress = item.progress / 100f
                    return true
                }
            }
        }

        /**
         * Factory method to create a [DialogEntry] from the a [GroupDialogItem] item
         */
        private fun create(item: GroupDialogItem, dialogData: DialogData): DialogEntry {
            return object : LabelledEntry(item, dialogData) {
                init {
                    layout.gravity = Gravity.MiddleLeft
                    label.textColor = label.tintColor
                    layer.backgroundColor = IosCxColor(ColorCompatibility.secondarySystemBackground)
                    label.font = item.textStyle.uiFont
                    contentGroup.layout.weight = 0.0
                }

                override fun refresh(): Boolean {
                    var changed = super.refresh()
                    if (label.isUserInteractionEnabled != item.isEnabled) {
                        changed = true
                        label.isUserInteractionEnabled = item.isEnabled
                    }
                    return changed
                }
            }
        }

        /**
         * Factory method to create a [DialogEntry] from the a [ClickableDialogItem] item
         */
        private fun create(item: ClickableDialogItem, dialogData: DialogData): DialogEntry {
            return object : LabelledEntry(item, dialogData) {
                init {
                    label.textColor = label.tintColor
                    label.addGestureRecognizer(UITapGestureRecognizer { item.onChanged() })
                }

                override fun refresh(): Boolean {
                    var changed = super.refresh()
                    if (label.isUserInteractionEnabled != item.isEnabled) {
                        changed = true
                        label.isUserInteractionEnabled = item.isEnabled
                    }
                    return changed
                }
            }
        }

        /**
         * Factory method to create a [DialogEntry] from the [NumericDialogItem]
         */
        private fun create(item: NumericDialogItem, dialogData: DialogData): DialogEntry {
            return object : LabelledEntry(item, dialogData) {
                val view = UILabel()

                init {
                    widget = view
                    view.textAlignment = NSTextAlignment.Right
                    view.font = item.textStyle.uiFont
                    view.backgroundColor = ColorCompatibility.secondarySystemBackground
                    view.text = item.value
                    view.textColor = ColorCompatibility.label
                    addView(view, Layout(widthMode = Layout.Mode.Weighted, gravity = Gravity.MiddleRight))
                }

                override fun refresh(): Boolean {
                    var changed = super.refresh()
                    if (view.text != item.value) {
                        changed = true
                        view.text = item.value
                    }
                    return changed
                }
            }
        }

        /**
         * Factory method to create a [DialogEntry] from the [SeparatorDialogItem]
         */
        private fun create(item: SeparatorDialogItem, dialogData: DialogData): DialogEntry {
            return object : FrameGroup(VerticalGroup.dividerLayout()), DialogEntry {
                override fun refresh(): Boolean = false
                override val item: DialogItem = item
                override val dialogData: DialogData = dialogData
                override var enabled: Boolean = false

                init {
                    name = "Divider"
                    layer.backgroundColor = UxColor.darkGray()
                }
            }
        }

        /**
         * Factory method to create a [DialogEntry] from the [CheckboxItem]
         */
        private fun create(item: CheckboxItem, dialogData: DialogData): DialogEntry {
            return object : LabelledEntry(item, dialogData) {
                val checkbox = Checkbox().also { it.borderColor = ColorCompatibility.label }

                init {
                    widget = checkbox
                    checkbox.checked = item.isChecked
                    checkbox.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Right
                    //height = Math.ceil((sw.intrinsicContentSize.height + padding * 2))
                    checkbox.addOnValueChangedListener {
                        if (item.isChecked != checkbox.checked) {
                            item.isChecked = checkbox.checked
                            dialogData.refresh()
                        }
                    }

                    contentGroup.addView(checkbox, Layout.layout {
                        gravity = Gravity.MiddleRight
                        margin = PADDING
                    })
                }

                override fun refresh(): Boolean {
                    var changed = super.refresh()
                    if (checkbox.checked != item.isChecked) {
                        changed = true
                        checkbox.checked = item.isChecked
                    }
                    return changed
                }
            }
        }

    }
}

val TextStyle.uiFont: UIFont
    get() = IosTextStyle.getTextStyle(this).uiFont
