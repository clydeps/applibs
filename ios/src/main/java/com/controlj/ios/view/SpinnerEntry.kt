package com.controlj.ios.view

import com.controlj.ios.ColorCompatibility
import com.controlj.layout.Gravity
import com.controlj.layout.Layout
import com.controlj.shim.addView
import com.controlj.ui.DialogData
import com.controlj.ui.SpinnerDialogItem

class SpinnerEntry(item: SpinnerDialogItem<*>, dialogData: DialogData) : LabelledEntry(item, dialogData) {
    val spinner: Spinner = Spinner(item.choices) {
        if (item.selected != it) {
            item.selected = it
            dialogData.refresh()
        }
    }

    init {
        //logMsg("got list of %d choices", choices.size)
        widget = spinner
        spinner.backgroundColor = ColorCompatibility.systemBackground
        spinner.font = item.textStyle.uiFont
        spinner.selected = item.selected
        contentGroup.addView(spinner, Layout(widthMode = Layout.Mode.MatchParent, gravity = Gravity.MiddleRight))
    }

    override fun refresh(): Boolean {
        var changed = super.refresh()
        item as SpinnerDialogItem<*>
        if (spinner.isUserInteractionEnabled != item.isEnabled) {
            changed = true
            spinner.isUserInteractionEnabled = item.isEnabled
        }
        spinner.list = item.choices
        if (item.choices.isNotEmpty()) {
            val newSel = item.selected.coerceIn(item.choices.indices)
            if (spinner.selected != newSel) {
                changed = true
                spinner.selected = newSel
            }
        }
        return changed
    }
}
