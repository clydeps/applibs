package com.controlj.ios.view

import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIScreen
import org.robovm.apple.uikit.UIView
import org.robovm.apple.uikit.UIViewController
import java.util.Stack

/**
 * Created by clyde on 7/6/18.
 */
class NavBarController(val rootViewController: UIViewController) : UIViewController() {
    private val stack: Stack<UIViewController> = Stack()
    private val navbar = UIView(CGRect(0.0, 0.0, 100.0, NAVBAR_HEIGHT))

    init {
        pushViewController(rootViewController)
    }

    fun popViewController() {
        if (stack.size > 1)
            doPop()
    }

    private fun doAnimation(action: () -> Unit, completion: () -> Unit = {}) {
        action()
        completion()
        /*
        UIView.animate(
                ANIMATION_DURATION,
                0.0,
                UIViewAnimationOptions.CurveEaseOut,
                {
                    action()
                    view.layoutIfNeeded()
                })
        { completion() }
*/
    }

    private fun doPop() {
        if (stack.size > 1)
            stack.pop().apply {
                doAnimation(
                        { view.removeFromSuperview() })
            }
    }

    fun pushViewController(controller: UIViewController) {
        //addChildViewController(controller)
        view.addSubview(controller.view)
        controller.view.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.setNeedsLayout()
        view.layoutIfNeeded()
        doAnimation({
            stack.push(controller)
            view.setNeedsLayout()
            view.layoutIfNeeded()
        })
    }

    fun replaceTopViewController(controller: UIViewController) {
        doAnimation({
            stack.pop().apply {
                //removeFromParentViewController()
                view.removeFromSuperview()
            }
            //addChildViewController(controller)
            view.addSubview(controller.view)
            controller.view.setTranslatesAutoresizingMaskIntoConstraints(false)
            stack.push(controller)
        })
    }

    override fun viewWillLayoutSubviews() {
        if (view.bounds.width == 0.0 || view.bounds.height == 0.0)
            return
        val contentRect: CGRect
        if (stack.size <= 0) {
            navbar.frame = CGRect.Zero()
            contentRect = view.bounds
        } else {
            navbar.frame = CGRect(0.0, 0.0, view.bounds.width, NAVBAR_HEIGHT)
            contentRect = CGRect(0.0, NAVBAR_HEIGHT, view.bounds.width, view.bounds.height - NAVBAR_HEIGHT)
        }
        //val shiftedRect = CGRect(CGPoint(-contentRect.width, 0.0), contentRect.size)
        view.subviews.drop(1).forEach {
            it.frame = contentRect //else shiftedRect
            it.setNeedsLayout()
        }
    }

    override fun viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(navbar)
        view.backgroundColor = UIColor.yellow()
        view.setTranslatesAutoresizingMaskIntoConstraints(false)
        navbar.setTranslatesAutoresizingMaskIntoConstraints(false)
    }

    override fun loadView() {
        view = UIView(UIScreen.getMainScreen().bounds)
    }

    val visibleViewController: UIViewController
        get() = stack.peek()
    val size: Int
        get() = stack.size

    companion object {
        const val NAVBAR_HEIGHT = 44.0
        const val ANIMATION_DURATION = 0.5
    }


}
