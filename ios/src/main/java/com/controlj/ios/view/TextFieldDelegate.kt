package com.controlj.ios.view

import com.controlj.ios.selectAll
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import com.controlj.ui.DialogItem
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.disposables.Disposable
import org.robovm.apple.foundation.NSArray
import org.robovm.apple.foundation.NSNotificationCenter
import org.robovm.apple.foundation.NSObject
import org.robovm.apple.foundation.NSRange
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIBarButtonItem
import org.robovm.apple.uikit.UIBarButtonItemGroup
import org.robovm.apple.uikit.UIBarButtonItemStyle
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIControl
import org.robovm.apple.uikit.UIKeyboardType
import org.robovm.apple.uikit.UITextAutocorrectionType
import org.robovm.apple.uikit.UITextField
import org.robovm.apple.uikit.UITextFieldDelegateAdapter
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-03-22
 * Time: 11:52
 *
 * A UITextField delegate that notifies a subscriber when a [textField] starts and ends editing.
 * An optional [timeout] can be set, if no keystrokes are received for that duration the view will
 * automatically end editing.
 * Also sets the textfield properties based on the input style.
 * Handles adding a +/- button to the assistant bar for signed int (since the iOS decimal keypad has no sign key)
 */
open class TextFieldDelegate(
        private val textField: UITextField,     // the textfield to be managed
        val style: DialogItem.InputStyle,   // the text editing style to use
        private val timeout: Long = 0,           // if the view should close after timeout
        val validator: (String) -> Boolean = { true }    // to validate input
) : UITextFieldDelegateAdapter(), UIControl.OnEditingChangedListener {

    enum class Event {
        STARTEDITING,
        ENDEDITING,
        CHANGED
    }

    companion object {
        /**
         * This allows other objects to find out which textfield is currently set.
         */
        var currentTextField: UITextField? = null
            private set

        /**
         * Hide the keyboard.
         */
        fun hideKeyboard() {
            currentTextField?.resignFirstResponder()
        }

        private val clearColor = UIColor.clear().cgColor
        private val errorColor = UIColor.red().cgColor
    }

    /**
     * Should the textfield resign as first responder when enter is pressed?
     */

    var shouldReturn: Boolean = timeout != 0L

    /**
     * Controls password visibility
     */

    var isPasswordVisible: Boolean
        get() = !textField.isSecureTextEntry
        set(value) {
            textField.isSecureTextEntry = !value
        }

    /**
     * Handles the timeout
     */
    private var timerDisposable: Disposable = Disposable.disposed()
    private var emitter: ObservableEmitter<Event> = DisposedEmitter()
    private var updating: Boolean = false

    init {
        when (style) {
            DialogItem.InputStyle.PASSWORD -> textField.isSecureTextEntry = true
            DialogItem.InputStyle.EMAIL -> textField.keyboardType = UIKeyboardType.EmailAddress
            DialogItem.InputStyle.INTEGER, DialogItem.InputStyle.SIGNEDINT,
            DialogItem.InputStyle.DECIMAL, DialogItem.InputStyle.SIGNEDDECIMAL -> {
                textField.textAlignment = NSTextAlignment.Right
                textField.keyboardType = UIKeyboardType.DecimalPad
                // if it requires the +/- button, do so later
                if (style.signed) {
                    val minus = UIBarButtonItem("+/-", UIBarButtonItemStyle.Plain) {
                        var text = textField.text
                        text = if (text.startsWith("-"))
                            text.substring(1)
                        else
                            "-$text"
                        textField.text = text
                    }
                    val group = UIBarButtonItemGroup(NSArray(minus), null)
                    val assistantItem = textField.inputAssistantItem
                    assistantItem.trailingBarButtonGroups = NSArray(group)
                }
            }
            DialogItem.InputStyle.PHONE -> textField.keyboardType = UIKeyboardType.PhonePad
            else -> Unit
        }
        textField.autocorrectionType = if (style.autoCorrect)
            UITextAutocorrectionType.Default
        else
            UITextAutocorrectionType.No
        textField.delegate = this
    }

    val observe: Observable<Event> by lazy {
        Observable.create<Event> { emitter = it }.share()
    }

    fun setText(text: String): Boolean {
        if (textField.text != text) {
            updating = true
            val pos = textField.selectedTextRange
            textField.text = text
            if (textField.isUserInteractionEnabled)
                textField.selectedTextRange = pos
            updating = false
            return true
        }
        return false
    }

    private fun startTimer(textField: UITextField) {
        if (timeout > 0)
            timerDisposable = Completable.timer(timeout, TimeUnit.MILLISECONDS, MainScheduler.instance)
                    .subscribe { textField.resignFirstResponder() }
    }

    // pressing return should close the box
    override fun shouldReturn(textField: UITextField): Boolean {
        if (shouldReturn)
            textField.resignFirstResponder()
        return false
    }

    private var observer: NSObject? = null

    override fun shouldChangeCharacters(textField: UITextField, range: NSRange, string: String): Boolean {
        if (style.validChars.isEmpty()) return true
        return string.all { style.validChars.contains(it) }
    }

    /**
     * Prevent loss of focus if the contents is invalid
     */
    override fun shouldEndEditing(textField: UITextField): Boolean {
        return validator(textField.text)
    }

    override fun onEditingChanged(textField: UIControl) {
        if (textField is UITextField) {
            textField.layer.borderWidth = 1.0
            textField.layer.borderColor = if (validator(textField.text)) clearColor else errorColor
        }
        if (!updating)
            emitter.onNext(Event.CHANGED)
    }

    // called when editing ends on a field
    override fun didEndEditing(textField: UITextField) {
        super.didEndEditing(textField)
        timerDisposable.dispose()
        observer?.let {
            NSNotificationCenter.getDefaultCenter().removeObserver(it)
        }
        textField.removeListener(this)
        emitter.onNext(Event.ENDEDITING)
        currentTextField = null
    }

    // called when editing starts on a field
    override fun didBeginEditing(textField: UITextField) {
        super.didBeginEditing(textField)
        textField.selectAll()
        currentTextField = textField
        startTimer(textField)
        observer = (UITextField.Notifications.observeTextDidChange(textField) { startTimer(textField) }) as? NSObject
        emitter.onNext(Event.STARTEDITING)
        textField.addOnEditingChangedListener(this)
    }
}
