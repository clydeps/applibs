package com.controlj.ios.view

import com.controlj.ios.ColorCompatibility
import com.controlj.ios.indexPathArray
import com.controlj.layout.FrameGroup
import com.controlj.layout.Gravity
import com.controlj.layout.IosUxHost
import com.controlj.layout.Layout
import com.controlj.layout.View
import com.controlj.logging.CJLog.debug
import com.controlj.shim.IosCxColor
import com.controlj.shim.IosCxRect
import com.controlj.shim.IosUxView
import com.controlj.ui.KeyboardStatus
import com.controlj.view.ListPresenter
import io.reactivex.rxjava3.disposables.Disposable
import org.robovm.apple.coregraphics.CGPoint
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.coregraphics.CGSize
import org.robovm.apple.foundation.NSIndexPath
import org.robovm.apple.uikit.UIEdgeInsets
import org.robovm.apple.uikit.UIFont
import org.robovm.apple.uikit.UIGestureRecognizer
import org.robovm.apple.uikit.UIGestureRecognizerDelegateAdapter
import org.robovm.apple.uikit.UILabel
import org.robovm.apple.uikit.UIScrollView
import org.robovm.apple.uikit.UITableView
import org.robovm.apple.uikit.UITableViewCell
import org.robovm.apple.uikit.UITableViewModel
import org.robovm.apple.uikit.UITableViewRowAnimation
import org.robovm.apple.uikit.UITableViewScrollPosition
import org.robovm.apple.uikit.UITableViewStyle
import org.robovm.apple.uikit.UITapGestureRecognizer
import org.robovm.apple.uikit.UITouch
import org.robovm.apple.uikit.UIView
import org.robovm.apple.uikit.UIViewAnimationOptions
import org.robovm.apple.uikit.UIViewController
import org.robovm.apple.uikit.UIViewControllerTransitionCoordinator

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-18
 * Time: 17:13
 *
 * Base class for table controllers.
 *
 * Type parameters:
 *
 * @param <V> The kind of object that this table lists
 * @param <T> The type of the ViewModel (presenter) that drives the behaviour of this table.
 */
abstract class TableController<V : Comparable<V>, T : ListPresenter<V>> : UIViewController() {

    companion object {
        const val ROW_HEIGHT = 50.0
        const val HEADER_HEIGHT = 50.0
    }

    protected open val refreshOnDrag = true             // true if dragging down should refresh the table
    protected abstract val presenter: T                           // the viewmodel presenter. This implements behaviour
    protected val heightMap = mutableMapOf<V, Double>()      // a map of items to views

    protected var disposable: Disposable = Disposable.disposed()     // disposable for the presenter subscription
    private var kbdDisposable: Disposable = Disposable.disposed()     // disposable for keyboard events
    protected var lastTouchedRow: NSIndexPath? = null

    protected open inner class BaseRowView(val item: V) : FrameGroup(Layout.layout {
        widthMode = Layout.Mode.MatchParent
        heightMode = Layout.Mode.WrapContent
        margin = 2.0
    }, name = "RowFrame") {
        init {
            layer.backgroundColor = IosCxColor(ColorCompatibility.systemBackground)
        }
    }

    /**
     * Override this function to generate views for rows.
     */
    protected open fun getRowLayout(item: V): BaseRowView {
        return BaseRowView(item)
    }

    protected open fun getHeaderLayout(rowIndex: ListPresenter.RowIndex): View {
        val label = UILabel()
        label.text = presenter.textValues(rowIndex.section).first()
        label.addGestureRecognizer(UITapGestureRecognizer { presenter.onHeaderClicked(rowIndex.section) })
        label.isUserInteractionEnabled = true
        label.font = UIFont.getBoldSystemFont(16.0)
        label.textColor = label.tintColor
        return IosUxView(
            label, Layout(
                widthMode = Layout.Mode.MatchParent,
                heightMode = Layout.Mode.Absolute,
                height = HEADER_HEIGHT,
                gravity = Gravity.MiddleLeft
            )
        ).apply {
            paddingBottom = 4.0
            paddingLeft = 4.0
            paddingTop = 4.0
            paddingRight = 4.0
            backgroundColor = IosCxColor(ColorCompatibility.secondarySystemBackground)
        }
    }

    protected open inner class TableModel : UITableViewModel() {
        override fun getNumberOfRowsInSection(tableView: UITableView?, section: Long): Long =
            presenter.totalRows.toLong()

        override fun getNumberOfSections(tableView: UITableView?): Long = 1

        override fun getCellForRow(tableView: UITableView, indexPath: NSIndexPath): UITableViewCell {
            val rowIndex = presenter.fromGlobal(indexPath.row)
            val item = presenter.itemAt(rowIndex)
            val view = if (item != null)
                getRowLayout(item).also {
                    it.onMeasure(uiTableView.bounds.width, Layout.MAX_DIMENSION)
                    heightMap[item] = it.measuredSize.height
                }
            else
                getHeaderLayout(rowIndex)
            return TableRowCellView(view)
        }

        override fun getEstimatedHeightForHeader(tableView: UITableView?, section: Long): Double = 0.0

        override fun getHeightForHeader(tableView: UITableView?, section: Long): Double = 0.0

        override fun getEstimatedHeightForRow(tableView: UITableView?, indexPath: NSIndexPath?) = ROW_HEIGHT

        override fun getHeightForRow(uitable: UITableView, p1: NSIndexPath) =
            getHeightForRow(presenter.fromGlobal(p1.row))

        override fun willEndDragging(scrollView: UIScrollView, velocity: CGPoint, targetContentOffset: CGPoint) {
            if (refreshOnDrag && scrollView.contentOffset.y < -20)
                presenter.startRefresh()
        }
    }

    protected open fun getHeightForRow(rowIndex: ListPresenter.RowIndex): Double {
        if (presenter.isHeader(rowIndex))
            return HEADER_HEIGHT
        return presenter.itemAt(rowIndex)?.let { item -> heightMap[item] } ?: ROW_HEIGHT
    }

    open class TableRowCellView(val view: View) : UITableViewCell() {
        val host = IosUxHost(view).also { contentView.addSubview(it) }

        init {
            backgroundColor = ColorCompatibility.systemBackground
        }

        override fun layoutSubviews() {
            host.frame = contentView.bounds
        }
    }

    /**
     * This is the presenter for the implementing controller
     */

    protected val model = TableModel()
    protected val uiTableView: UITableView = UITableView(CGRect(), UITableViewStyle.Plain)
    private val additions = mutableListOf<ListPresenter.ChangeRange>()
    private val deletions = mutableListOf<ListPresenter.ChangeRange>()
    private var changeDepth = 0

    // have to handle this to get new row cells after rotation
    override fun viewWillTransitionToSize(p0: CGSize, coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(p0, coordinator)
        coordinator.animateAlongsideTransition({
        }) {
            notifyDataSetChanged()
        }
    }

    open fun onCustomEvent(event: ListPresenter.ChangeRange) {
    }

    open fun showMenu(menuEvent: ListPresenter<V>.MenuEvent) {
        val index = presenter.indexOf(menuEvent.item)
        if (index != null) {
            val rect = uiTableView.getRectForRow(NSIndexPath.row(presenter.toGlobal(index).toLong(), 0L))
            val viewRect = uiTableView.convertRectToView(rect, uiTableView.superview)
            IosMenu.showMenu(this, menuEvent.title, menuEvent.choices, IosCxRect(viewRect))
        }
    }

    override fun viewDidLoad() {
        uiTableView.model = model
        uiTableView.rowHeight = UITableView.getAutomaticDimension()
        uiTableView.estimatedRowHeight = ROW_HEIGHT
        uiTableView.setAllowsSelection(false)
        uiTableView.addGestureRecognizer(UIGestureRecognizer().also {
            it.delegate = object : UIGestureRecognizerDelegateAdapter() {
                override fun shouldReceiveTouch(gestureRecognizer: UIGestureRecognizer?, touch: UITouch?): Boolean {
                    lastTouchedRow = uiTableView.getIndexPathForRow(touch?.getLocationInView(uiTableView))
                    return false
                }
            }
        })
        notifyDataSetChanged()
    }


    protected open fun connectPresenter() {
        disposable.dispose()
        disposable = presenter.changeObserver.subscribe(this::onListEvent)
    }

    override fun viewWillDisappear(animated: Boolean) {
        disposable.dispose()
        kbdDisposable.dispose()
        super.viewWillDisappear(animated)
    }

    override fun viewWillAppear(p0: Boolean) {
        kbdDisposable.dispose()
        kbdDisposable = KeyboardStatus.observable.subscribe { keyboardChanged() }
        connectPresenter()
    }

    private fun keyboardChanged() {
        if (KeyboardStatus.keyboardFrame.height == 0.0)
            return
        val curve = KeyboardStatus.animationCurve
        // get the keyboard top in our reference frame
        val point = view.window.convertPointToView(
            CGPoint(KeyboardStatus.keyboardFrame.left, KeyboardStatus.keyboardFrame.top),
            view
        )
        // inset our bottom by the difference between the keyboard top and our bottom.
        val insets = UIEdgeInsets(0.0, 0.0, (view.bounds.maxY - point.y).coerceAtLeast(0.0), 0.0)
        UIView.animate(
            KeyboardStatus.animationDuration,
            0.0,
            UIViewAnimationOptions(if (curve is Long) curve shl 16 else 0L),
            {
                uiTableView.contentInset = insets
                lastTouchedRow?.let {
                    uiTableView.scrollToRow(it, UITableViewScrollPosition.None, false)
                }
            }, {})
    }

    private var inBlock = false
    protected open fun onListEvent(event: ListPresenter.ViewEvent?) {
        @Suppress("UNCHECKED_CAST")
        when (event) {
            is ListPresenter<*>.MenuEvent -> showMenu(event as ListPresenter<V>.MenuEvent)
            is ListPresenter.ChangeRange -> {
                //debug("change ${event.reason}, ${event.start}, ${event.length}")
                when (event.reason) {
                    ListPresenter.ChangeReason.BUSY -> {
                        NavBar.instance?.setBusy(true)
                    }
                    ListPresenter.ChangeReason.IDLE -> {
                        NavBar.instance?.setBusy(false)
                        updateButtons()
                    }
                    ListPresenter.ChangeReason.STARTED -> {
                        inBlock = true
                        //uiTableView.beginUpdates()
                    }
                    ListPresenter.ChangeReason.ENDED -> {
                        //processUpdates()
                        //uiTableView.endUpdates()
                        inBlock = false
                        uiTableView.reloadData()
                        updateButtons()
                    }
                    ListPresenter.ChangeReason.CHANGED -> {
                        if (!inBlock) {
                            uiTableView.reloadRows(
                                event.start.indexPathArray(event.length),
                                UITableViewRowAnimation.None
                            )
                            updateButtons()
                        }
                    }
                    ListPresenter.ChangeReason.ADDED -> {
                        if (!inBlock) {
                            uiTableView.insertRows(
                                event.start.indexPathArray(event.length),
                                UITableViewRowAnimation.Automatic
                            )
                            updateButtons()
                        }
                    }
                    ListPresenter.ChangeReason.REMOVED -> {
                        if (!inBlock) {
                            uiTableView.deleteRows(
                                event.start.indexPathArray(event.length),
                                UITableViewRowAnimation.Automatic
                            )
                            updateButtons()
                        }
                    }
                    ListPresenter.ChangeReason.REFRESH -> uiTableView.reloadData()
                    ListPresenter.ChangeReason.CUSTOM -> onCustomEvent(event)
                }
            }
        }
    }

    protected fun processUpdates() {
        debug("Beginning updates; rows = ${model.getNumberOfRowsInSection(uiTableView, 0L)}")
        debug("deletions: " + deletions.joinToString { "${it.start}(${it.length})" })
        deletions.forEach {
            uiTableView.deleteRows(
                it.start.indexPathArray(it.length),
                UITableViewRowAnimation.Automatic
            )
        }
        debug("addition: " + additions.joinToString { "${it.start}(${it.length})" })
        var accum = 0
        additions.sortedBy { it.start }.forEach {
            uiTableView.insertRows((it.start + accum).indexPathArray(it.length), UITableViewRowAnimation.Automatic)
            accum += it.length
        }
        additions.clear()
        deletions.clear()
        debug("Ended updates; rows = ${model.getNumberOfRowsInSection(uiTableView, 0L)}")
        updateButtons()
    }

    protected fun notifyDataSetChanged() {
        uiTableView.reloadData()
        updateButtons()
    }

    protected open fun updateButtons() {

    }

    private fun onRefreshComplete() {
        //CJLog.logMsg("onrefreshcomplete: ")
    }
}
