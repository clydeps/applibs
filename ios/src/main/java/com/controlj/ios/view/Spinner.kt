package com.controlj.ios.view

import com.controlj.framework.IosBackgroundServiceProvider.isIos14
import com.controlj.ios.ColorCompatibility
import com.controlj.ui.KeyboardStatus
import com.controlj.widget.UILabelPadded
import org.robovm.apple.coregraphics.CGAffineTransform
import org.robovm.apple.coregraphics.CGPoint
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.coregraphics.CGSize
import org.robovm.apple.foundation.NSIndexPath
import org.robovm.apple.foundation.NSSet
import org.robovm.apple.foundation.NSString
import org.robovm.apple.uikit.NSAttributedStringAttributes
import org.robovm.apple.uikit.NSTextAlignment
import org.robovm.apple.uikit.UIApplication
import org.robovm.apple.uikit.UIBezierPath
import org.robovm.apple.uikit.UIColor
import org.robovm.apple.uikit.UIEdgeInsets
import org.robovm.apple.uikit.UIEvent
import org.robovm.apple.uikit.UIFont
import org.robovm.apple.uikit.UIGestureRecognizer
import org.robovm.apple.uikit.UIGestureRecognizerDelegateAdapter
import org.robovm.apple.uikit.UIGraphics
import org.robovm.apple.uikit.UIImageView
import org.robovm.apple.uikit.UITableView
import org.robovm.apple.uikit.UITableViewCell
import org.robovm.apple.uikit.UITableViewCellSeparatorStyle
import org.robovm.apple.uikit.UITableViewCellStyle
import org.robovm.apple.uikit.UITableViewModel
import org.robovm.apple.uikit.UITableViewScrollPosition
import org.robovm.apple.uikit.UITapGestureRecognizer
import org.robovm.apple.uikit.UITouch
import org.robovm.apple.uikit.UIView
import org.robovm.apple.uikit.UIViewController
import kotlin.math.PI

/**
 * Created by clyde on 2/8/17.
 */

/**
 * A spinner, modelled after the Android spinner
 */
open class Spinner(
        var list: List<*> = listOf<String>(),
        private val listener: ((Int) -> Unit)?
) : UIView() {

    companion object {
        private val triangle: UIBezierPath =

                UIBezierPath().apply {
                    move(CGPoint(50.0, 80.0))
                    addLine(CGPoint(20.0, 50.0))
                    addLine(CGPoint(80.0, 50.0))
                    closePath()
                }
        private const val HEIGHT = 45
        private const val MAX_DURATION = 0.3    // maximum time to open the spinner
        private const val PER_LINE_DURATION = 0.05    // time per line to open spinner
        private const val PADDING = 4.0
    }

    var padding = PADDING
    private var lineColor = UIColor.lightGray()
    protected val label = UILabelPadded(UIEdgeInsets(0.0, padding, 0.0, padding)).also {
        it.textAlignment = NSTextAlignment.Right
        it.numberOfLines = 1
    }
    private val image = UIImageView()
    private var initialFrame = CGRect()
    private var finalFrame: CGRect = CGRect()

    // dropdown parameters

    private val ddMaxSize = 300.0
    var ddColor: UIColor = ColorCompatibility.secondarySystemBackground
    var ddTextColor = ColorCompatibility.label
    var ddSelectedBackgroundColor: UIColor = ColorCompatibility.systemBackground
    var ddTextSelectedColor = ColorCompatibility.label
    var selectedFont = UIFont.getBoldSystemFont(20.0)
    var unselectedFont = UIFont.getSystemFont(17.0)

    protected open fun getDropdownBackgroundColor(row: Int): UIColor {
        return ddColor
    }

    protected open fun getDropdownSelectedBackgroundColor(row: Int): UIColor {
        return ddSelectedBackgroundColor
    }

    var font: UIFont
        get() = label.font
        set(value) {
            label.font = value
        }

    open var selected: Int = -1
        set(value) {
            //debug("selected set to $value, was $field")
            field = value
            if (value in (list.indices))
                label.text = list[value].toString()
            else
                label.text = "Choose one"
            label.textColor = ddTextColor
            setNeedsDisplay()
        }

    private var chooser: UITableView = UITableView().also {
        it.isUserInteractionEnabled = true
        it.setShowsHorizontalScrollIndicator(false)
        it.setShowsVerticalScrollIndicator(true)
        it.separatorStyle = UITableViewCellSeparatorStyle.None
        it.tableFooterView = UIView()

    }
    private var dismisser: UIView = object : UIView() {
        override fun touchesBegan(p0: NSSet<UITouch>?, p1: UIEvent?) {
            closeSpinner()
            super.touchesBegan(p0, p1)
        }
    }
            .also { it.backgroundColor = UIColor.clear() }
    private var shadow: UIView = UIView()
    private val animDuration: Double
    private var isSpinnerOpen: Boolean = false
    override fun getIntrinsicContentSize(): CGSize {
        val attrs = NSAttributedStringAttributes().setFont(selectedFont)
        val size: CGSize = (if (list.isEmpty()) listOf("Choose one") else list)
                .map { NSString(it.toString().padEnd(1, ' ')).getSize(attrs) }.maxByOrNull { it.width }
                ?: CGSize()
        return CGSize(size.width + padding * 4 + size.height, size.height + padding * 2)
    }

    override fun getSizeThatFits(available: CGSize): CGSize {
        val ics = intrinsicContentSize
        //debug("Spinner getSizeThatFits $ics")
        return CGSize(ics.width.coerceAtMost(available.width), ics.height)
    }

    init {
        isUserInteractionEnabled = true
        backgroundColor = UIColor.clear()
        animDuration = Math.max(MAX_DURATION, list.size * PER_LINE_DURATION)
        chooser.layer.cornerRadius = 5.0

        dismisser.addGestureRecognizer(UIGestureRecognizer().also {
            it.delegate = object : UIGestureRecognizerDelegateAdapter() {
                override fun shouldReceiveTouch(gestureRecognizer: UIGestureRecognizer?, touch: UITouch?): Boolean {
                    closeSpinner()
                    return false
                }
            }
        })
        shadow.layer.shadowOffset = CGSize(3.0, 3.0)
        shadow.layer.shadowColor = ColorCompatibility.systemBackground.cgColor
        shadow.layer.shadowOpacity = 0.2f
        shadow.layer.shadowRadius = 5.0
        shadow.layer.cornerRadius = 5.0
        shadow.layer.setMasksToBounds(false)
        shadow.setClipsToBounds(false)
        chooser.model = object : UITableViewModel() {

            override fun getNumberOfRowsInSection(tableView: UITableView?, section: Long): Long {
                return list.size.toLong()
            }

            override fun getCellForRow(tableView: UITableView, indexPath: NSIndexPath): UITableViewCell {
                val cell = object : UITableViewCell(UITableViewCellStyle.Default, "Cell") {
                    override fun setSelected(selected: Boolean, animated: Boolean) {
                        super.setHighlighted(selected)
                        if (selected)
                            textLabel.font = selectedFont
                        else
                            textLabel.font = unselectedFont
                    }
                }
                cell.contentView.backgroundColor = getDropdownBackgroundColor(indexPath.row)
                // TODO - update this to use contentConfiguration
                cell.textLabel.backgroundColor = UIColor.clear()
                cell.textLabel.text = list[indexPath.row].toString()
                cell.textLabel.textColor = ddTextColor
                cell.textLabel.highlightedTextColor = ddTextSelectedColor
                cell.textLabel.textAlignment = NSTextAlignment.Right
                cell.selectedBackgroundView = UIView()
                cell.selectedBackgroundView.backgroundColor = getDropdownSelectedBackgroundColor(indexPath.row)
                return cell
            }

            override fun getNumberOfSections(tableView: UITableView?): Long {
                return 1
            }

            override fun didSelectRow(tableView: UITableView?, indexPath: NSIndexPath) {
                selected = indexPath.row
                closeSpinner()
                listener?.invoke(selected)
            }
        }
    }

    override fun willMoveToSuperview(superView: UIView?) {
        super.willMoveToSuperview(superView)
        if (superView != null) {
            addSubview(label)
            addSubview(image)
            addGestureRecognizer(UITapGestureRecognizer {
                if (list.isNotEmpty() && !isSpinnerOpen) {
                    val rootView = getViewController()?.view
                            ?: UIApplication.getSharedApplication().delegate.window.rootViewController.view
                    if (!KeyboardStatus.whenClosed(this::openSpinner))
                        rootView?.endEditing(false)
                }
            })
        } else {
            image.removeFromSuperview()
            label.removeFromSuperview()
        }
    }

    override fun layoutSubviews() {
        super.layoutSubviews()
        if (isSpinnerOpen)
            closeSpinner()
        if (!bounds.isEmpty) {
            val textInsets = UIEdgeInsets(padding, padding, padding, bounds.height)
            label.frame = bounds.inset(textInsets)
            image.frame = bounds.inset(UIEdgeInsets(0.0, bounds.width - bounds.height, 0.0, 0.0))
            val shifted = triangle.copy() as UIBezierPath
            val scale = (image.bounds.height) / 100.0
            shifted.applyTransform(CGAffineTransform.createScale(scale, scale))
            UIGraphics.beginImageContext(image.bounds.size)
            val ctx = UIGraphics.getCurrentContext()
            ctx.scaleCTM(scale, scale)
            ctx.setFillColor(lineColor.cgColor)
            triangle.fill()
            image.image = UIGraphics.getImageFromCurrentImageContext()
            UIGraphics.endImageContext()
        }
    }

    fun UIView.getViewController(): UIViewController? {
        var v = this
        while (v.nextResponder is UIView)
            v = v.nextResponder as UIView
        (v.nextResponder as? UIViewController)?.let { return it }
        return null
    }

    private fun openSpinner() {
        var heightTableView = (HEIGHT * list.size).toDouble()
        val rootView = getViewController()?.view
                ?: UIApplication.getSharedApplication().delegate.window.rootViewController.view
        // get the position of the view in global coordinates
        val globalRect = convertRectToView(label.bounds, rootView)
        dismisser.frame = rootView.bounds
        val bottom: Boolean
        val frameHeight = rootView.frame.height
        if (globalRect.maxY + heightTableView < frameHeight) {
            bottom = true
        } else if (globalRect.y - heightTableView > 0) {
            bottom = false
        } else {
            //find best direction
            val margeTop = globalRect.y
            val margeBot = frameHeight - globalRect.maxY

            if (margeTop < margeBot) {
                bottom = true
                heightTableView = margeBot - 5
            } else {
                bottom = false
                heightTableView = margeTop - 5
            }
        }
        if (heightTableView > ddMaxSize)
            heightTableView = ddMaxSize
        chooser.backgroundColor = ddColor
        val textSize = intrinsicContentSize
        val startY: Double
        if (bottom) {
            startY = globalRect.minY
            finalFrame = CGRect(globalRect.maxX - textSize.width, startY, textSize.width, heightTableView)
        } else {
            startY = globalRect.maxY
            finalFrame = CGRect(globalRect.maxX - textSize.width, startY - heightTableView, textSize.width, heightTableView)
        }
        initialFrame = CGRect(finalFrame.x, startY, textSize.width, 0.0)
        chooser.frame = initialFrame
        image.transform = CGAffineTransform.Identity()
        rootView.addSubview(dismisser)
        rootView.addSubview(shadow)
        rootView.addSubview(chooser)
        rootView.bringSubviewToFront(chooser)
        animate(animDuration, {
            image.transform = CGAffineTransform.createRotation(-PI * .999)
            chooser.frame = finalFrame
            shadow.frame = finalFrame
            shadow.layer.shadowOpacity = 0.5f
            if (selected >= 0)
                chooser.selectRow(NSIndexPath(longArrayOf(0, selected.toLong())), false, UITableViewScrollPosition.None)
        }) {
            isSpinnerOpen = true
            rootView.bringSubviewToFront(chooser)
            if (selected >= 0)
                chooser.selectRow(NSIndexPath(longArrayOf(0, selected.toLong())), false, UITableViewScrollPosition.None)
        }
    }

    private fun closeSpinner() {
        if (isSpinnerOpen) {
            layer.removeAllAnimations()
            isSpinnerOpen = false
            animate(animDuration, {
                image.transform = CGAffineTransform.Identity()
                chooser.frame = initialFrame
                shadow.layer.shadowOpacity = 0f
            }) {
                shadow.removeFromSuperview()
                chooser.removeFromSuperview()
                dismisser.removeFromSuperview()
            }
        }
    }
}

