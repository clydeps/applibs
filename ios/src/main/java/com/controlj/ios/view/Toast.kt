package com.controlj.ios.view

import com.controlj.graphics.TextStyle
import com.controlj.ios.graphics.CGContextWrapper
import com.controlj.ios.graphics.IosTextStyle
import io.reactivex.rxjava3.core.Completable
import org.robovm.apple.coregraphics.CGPoint
import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.coregraphics.CGSize
import org.robovm.apple.uikit.*
import reactivex.ios.schedulers.IosSchedulers
import java.util.concurrent.TimeUnit
import kotlin.math.ceil
import kotlin.math.roundToInt

/**
 * Created by clyde on 23/6/17.
 */

class Toast(text: String, duration: Long) {
    private val button: UIButton
    val settings = Settings()

    enum class Gravity {
        BOTTOM,
        TOP,
        CENTER
    }

    class Settings {
        var duration: Long = LONG
        var textSize = 18.0
        var cornerRadius = 8.0
        var color = 0x96101010.toInt()
        var gravity = Gravity.BOTTOM
        var isUseShadow = true

        companion object {
            var SHORT = 2000L // ms
            var LONG = 4000L // ms
        }
    }

    init {
        settings.duration = duration
        val style = IosTextStyle.getTextStyle(TextStyle(alignment = TextStyle.Alignment.Center, size = settings.textSize))
        val size = style.getTextBoundingRect(WIDTH, 1000.0, text).size
        val label = UILabel(CGRect(0.0, 0.0, size.width, size.height))
        label.backgroundColor = UIColor.clear()
        label.textColor = UIColor.white()
        label.font = style.uiFont
        label.text = text
        label.numberOfLines = 0
        if (settings.isUseShadow) {
            label.shadowColor = UIColor.darkGray()
            label.shadowOffset = CGSize(1.0, 1.0)
        }
        button = UIButton(UIButtonType.Custom)
        button.frame = CGRect(0.0, 0.0, size.width + PADDING * 2, size.height + PADDING * 2)
        label.center = CGPoint(button.frame.size.width / 2, button.frame.size.height / 2)
        val lbfrm = label.frame
        lbfrm.origin.x = ceil(lbfrm.origin.x)
        lbfrm.origin.y = ceil(lbfrm.origin.y)
        label.frame = lbfrm
        button.addSubview(label)
        button.backgroundColor = CGContextWrapper.colorFromArgb(settings.color)
        button.layer.cornerRadius = settings.cornerRadius
        button.layer.setMasksToBounds(true)
        val window = UIApplication.getSharedApplication().windows[0]
        val width = (window.frame.size.width / 2).roundToInt().toDouble()
        val height = window.frame.size.height
        when (settings.gravity) {
            Gravity.TOP -> button.center = CGPoint(width, OFFSET)

            Gravity.CENTER -> button.center = CGPoint(width, (height / 2).roundToInt().toDouble())

            else -> button.center = CGPoint(width, height - OFFSET)
        }
        button.tag = TOAST_TAG.toLong()
        val currentToast = window.getViewWithTag(TOAST_TAG.toLong())
        currentToast?.removeFromSuperview()
        button.alpha = 0.0
        window.addSubview(button)
        UIView.animate(0.5) { button.alpha = 1.0 }
        val disposable = Completable.timer(settings.duration, TimeUnit.MILLISECONDS)
                .observeOn(IosSchedulers.mainThread())
                .doOnComplete { UIView.animate(0.5) { button.alpha = 0.0 } }
                .andThen(Completable.timer(500, TimeUnit.MILLISECONDS))
                .observeOn(IosSchedulers.mainThread())
                .doFinally(button::removeFromSuperview)
                .subscribe()
        button.addOnTouchDownListener { _, _ -> disposable.dispose() }
    }

    companion object {

        const val OFFSET = 200.0
        const val WIDTH = 280.0

        private const val PADDING = 10
        private const val TOAST_TAG = 0x5587542
    }
}
