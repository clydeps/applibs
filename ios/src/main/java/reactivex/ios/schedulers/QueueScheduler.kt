/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package reactivex.ios.schedulers

import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import org.robovm.apple.dispatch.DispatchQueue
import org.robovm.apple.foundation.NSBlockOperation
import org.robovm.apple.foundation.NSOperationQueue
import java.util.concurrent.TimeUnit

internal class QueueScheduler(private val operationQueue: NSOperationQueue) : Scheduler() {
    private val dispatchQueue: DispatchQueue = DispatchQueue.getMainQueue()
    override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
        val scheduled = ScheduledRunnable(operationQueue, RxJavaPlugins.onSchedule(run))
        dispatchQueue.after(delay, unit, scheduled)
        return scheduled
    }

    override fun createWorker(): Worker {
        return HandlerWorker(operationQueue, dispatchQueue)
    }

    private class HandlerWorker internal constructor(private val queue: NSOperationQueue, private val dispatchQueue: DispatchQueue) : Worker() {

        @Volatile
        private var disposed = false
        override fun schedule(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
            if (disposed) return Disposable.disposed()
            val scheduled = ScheduledRunnable(queue, RxJavaPlugins.onSchedule(run))
            dispatchQueue.after(delay, unit, scheduled)
            // Re-check disposed state for removing in case we were racing a call to dispose().
            if (disposed) {
                scheduled.dispose()
                return Disposable.disposed()
            }
            return scheduled
        }

        override fun dispose() {
            disposed = true
        }

        override fun isDisposed(): Boolean {
            return disposed
        }

    }

    private class ScheduledRunnable internal constructor(private val queue: NSOperationQueue, delegate: Runnable?) : Runnable, Disposable {
        private val blockOperation: NSBlockOperation = NSBlockOperation(delegate)

        @Volatile
        private var disposed = false
        override fun run() {
            if (!disposed) try {
                queue.addOperation(blockOperation)
            } catch (t: Throwable) {
                val ie = IllegalStateException("Fatal Exception thrown on Scheduler.", t)
                RxJavaPlugins.onError(ie)
                val thread = Thread.currentThread()
                thread.uncaughtExceptionHandler.uncaughtException(thread, ie)
            }
        }

        override fun dispose() {
            disposed = true
            blockOperation.cancel()
        }

        override fun isDisposed(): Boolean {
            return disposed
        }

    }
}
