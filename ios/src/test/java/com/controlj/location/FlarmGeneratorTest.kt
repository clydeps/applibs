package com.controlj.location

import com.controlj.data.Location
import com.controlj.nmea.FlarmGenerator
import com.controlj.nmea.GpsFix
import com.controlj.nmea.GpsStatus
import com.controlj.traffic.TrafficTarget
import com.controlj.utility.instantNow
import com.controlj.viewmodel.Listenable
import com.controlj.viewmodel.ListenableValue
import io.reactivex.rxjava3.core.Observable
import junit.framework.TestCase
import org.junit.Test
import org.threeten.bp.Instant

class FlarmGeneratorTest : TestCase() {

    val creator = Location.Creator("test", Location.LOW_PRIORITY)
    val location = Location.of(-28.08, 150.0, 1000.0, 15.0, 10.0, creator, Instant.EPOCH)
    val status = GpsStatus.of(GpsFix.ThreeD, 1000.0, 1100.0, 5, 6)

    val provider = object : LocationProvider {
        override val name: String = "TestProvider"
        override val locationObserver: Observable<Location>
            get() = Observable.just(GpsLocation.of(location, status))
        override var priority: Int = 1
        override val enabled: ListenableValue<Boolean> = Listenable(true)
        override val isConnected: Boolean = true
        override val authorisationStatus: LocationProvider.AuthorisationStatus =
            LocationProvider.AuthorisationStatus.Always

    }

    @Test
    fun testGpgga() {
        val result = FlarmGenerator.gpgga(GpsLocation.of(location, status))
        assertEquals("\$GPGGA,000000,2804.80,S,15000.00,E,1,05,40.0,1000.0,M,,M,,*43", result.trim())
    }

    @Test
    fun testPflau() {
        LocationSource.add(provider)
        LocationSource.lastLocation = location
        assertTrue(Location.equals(location, LocationSource.lastLocation))
        val tloc = location.translate(20.0, 1000.0)
        val target = TrafficTarget.of(
            tloc.latitude,
            tloc.longitude,
            creator,
            30.0,
            350.0,
            1100.0,
            instantNow,
            20.0,
            40.0,
            TrafficTarget.EmitterAddress(TrafficTarget.AddressType.AdsbSelf, 0x123454),
            TrafficTarget.EmitterCategory.Large,
            "N1234", 10.0, isExtrapolated = false
        )
        val result = FlarmGenerator.pflau(location, target, 1).trim()
        assertEquals("\$PFLAU,1,1,1,1,1,10,2,100,997,123454*4F", result)

    }
}
