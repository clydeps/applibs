pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}
include(":common")
include(":android")
include(":ios")
include(":testapp")
include("GdlSimulator")
