package com.controlj.utility

import org.junit.Assert.assertEquals
import org.junit.Test

class UnitsTest
{
    @Test
    fun pAltTest() {
        val pressure = Units.Unit.PALT.convertFrom(1500.0)
        val altitude = Units.Unit.PALT.convertTo(pressure)
        assertEquals(1500.0, altitude, .1)
    }
}
