package com.controlj.view

import android.app.Activity
import android.os.Bundle

class TestActivity : Activity() {

    companion object {
        var layout = com.controlj.Utilities.test.R.layout.test_test
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)
    }

}
