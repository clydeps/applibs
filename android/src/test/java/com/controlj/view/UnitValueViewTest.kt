package com.controlj.view

import android.widget.EditText
import android.widget.Spinner
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withSpinnerText
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.controlj.Utilities.test.R
import com.controlj.rx.MainScheduler
import com.controlj.settings.AviationUnitSettings
import com.controlj.settings.MapDataStore
import com.controlj.utility.SetEditTextSelectionAction
import com.controlj.utility.Units
import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.hamcrest.CoreMatchers.instanceOf
import org.junit.Assert.assertEquals
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class UnitValueViewTest {

    @get:Rule
    val rule = ActivityScenarioRule(TestActivity::class.java)

    companion object {
        @JvmStatic
        @BeforeClass
        fun setup() {
            MapDataStore(true)
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { TestScheduler() }
            MainScheduler.instance = AndroidSchedulers.mainThread()
            assertEquals(Units.Unit.NM, AviationUnitSettings.distanceUnitSetting.value)
            TestActivity.layout = R.layout.unitview_test
        }
    }

    @Test
    fun testSelectionBounds() {
        onView(instanceOf(EditText::class.java)).perform(replaceText("90"))
        onView(instanceOf(EditText::class.java)).check(matches(withText("90")))
        onView(instanceOf(Spinner::class.java)).check(matches(withSpinnerText("nm")))
        onData(instanceOf(Units.Unit::class.java)).atPosition(2).perform(click())
        Espresso.pressBack()
        onView(instanceOf(Spinner::class.java)).check(matches(withSpinnerText("km")))
        onView(instanceOf(EditText::class.java)).check(matches(withText("167")))

        // testing NAIP-46
        onView(instanceOf(EditText::class.java)).perform(SetEditTextSelectionAction(3))
        onData(instanceOf(Units.Unit::class.java)).atPosition(0).perform(click())
        Espresso.pressBack()
        onView(instanceOf(Spinner::class.java)).check(matches(withSpinnerText("nm")))
        onView(instanceOf(EditText::class.java)).check(matches(withText("90")))
    }
}
