package com.controlj.ble

import android.bluetooth.BluetoothGattService

/**
 * Created by clyde on 15/5/17.
 */
class AndroidBleGattService(val device: AndroidBleDevice, val service: BluetoothGattService) : BleGattService() {
    override val uuid: String
        get() = service.uuid.toString()

    override val characteristics: List<BleCharacteristic>  by lazy {
        service.characteristics.map { AndroidBleCharacteristic(device, it) }
    }
}
