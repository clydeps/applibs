@file:Suppress("DEPRECATION")

package com.controlj.ble

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter.STATE_CONNECTED
import android.bluetooth.BluetoothAdapter.STATE_DISCONNECTED
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGatt.GATT_SUCCESS
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothStatusCodes.SUCCESS
import android.content.Context
import android.os.Build
import com.controlj.logging.CJLog.logMsg
import java.util.UUID

/**
 * Created by clyde on 14/5/17.
 * This is the android implementation of a Ble device
 */

@SuppressLint("MissingPermission")
class AndroidBleDevice private constructor(
    private var device: BluetoothDevice,
    private val context: Context
) : BleDevice(device.address, device.name ?: "") {
    private var gatt: BluetoothGatt? = null

    private var discoverRetries = 0
    private val gattCallback = object : BluetoothGattCallback() {
        override fun onReadRemoteRssi(gatt: BluetoothGatt?, rssi: Int, status: Int) {
            if (status != GATT_SUCCESS)
                onRssiError("RSSI error: $status")
            else
                onRssiRead(rssi)
        }

        override fun onConnectionStateChange(gattCallback: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gattCallback, status, newState)
            //logMsg("GATT = %s", gatt.toString());
            when (newState) {
                STATE_DISCONNECTED -> {
                    logMsg("Connection state change to DISCONNECTED data %d", status)
                    gattCallback.close()
                    gatt?.close()
                    gatt = null
                    onDisconnected()
                }

                STATE_CONNECTED -> {
                    logMsg("Connection state change to CONNECTED data %d", status)
                    gatt = gattCallback
                    onConnected()
                }

                else -> logMsg("Connection state change to %d ??? data %d", newState, status)
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(gatt, status)
            when {
                status == GATT_SUCCESS && gatt.services.isNotEmpty() -> servicesDiscovered()
                status != GATT_SUCCESS -> onDiscoveryError(status.toString())
                discoverRetries++ >= MAX_RETRIES -> onDiscoveryError("too many retries")
                else -> if (!gatt.discoverServices()) onDiscoveryError("request failed")
            }
        }

        @Deprecated("Backwards compatibility")
        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            super.onCharacteristicRead(gatt, characteristic, status)
            //logMsg("onCharacteristicRead: %s", characteristic.getUuid());
            val ch = getBleCharacteristic(characteristic.uuid.toString())
            if (ch != null)
                if (status != GATT_SUCCESS)
                    onReadError(ch, status.toString())
                else
                    onReadCharacteristic(ch, characteristic.value)
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            value: ByteArray,
            status: Int
        ) {
            super.onCharacteristicRead(gatt, characteristic, value, status)
            //logMsg("onCharacteristicRead: %s", characteristic.getUuid());
            val ch = getBleCharacteristic(characteristic.uuid.toString())
            if (ch != null)
                if (status != GATT_SUCCESS)
                    onReadError(ch, status.toString())
                else
                    onReadCharacteristic(ch, value)
        }

        override fun onCharacteristicWrite(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            //logMsg("onCharacteristicWrite: %s, status %d", characteristic.getUuid(), status);
            val ch = getBleCharacteristic(characteristic.uuid.toString())
            if (ch != null)
                if (status != GATT_SUCCESS)
                    onWriteError("Write error: $status")
                else
                    onWriteCharacteristic(getBleCharacteristic(characteristic.uuid.toString()))
        }

        @Deprecated("Backwards compatibility")
        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
        ) {
            super.onCharacteristicChanged(gatt, characteristic)
            //logMsg("onCharacteristicChanged: %s", characteristic.getUuid());
            val ch = getBleCharacteristic(characteristic.uuid.toString())
            if (ch != null)
                onNotification(ch, characteristic.value)
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            value: ByteArray
        ) {
            super.onCharacteristicChanged(gatt, characteristic, value)
            //logMsg("onCharacteristicChanged: %s", characteristic.getUuid());
            val ch = getBleCharacteristic(characteristic.uuid.toString())
            if (ch != null)
                onNotification(ch, value)
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            val ch = getBleCharacteristic(descriptor.characteristic.uuid.toString())
            if (ch != null && descriptor.uuid == CLIENT_CHARACTERISTIC_CONFIG)
                onNotificationSet(ch)
            //logMsg("Descriptor write result %d", status);
        }

        override fun onReliableWriteCompleted(gatt: BluetoothGatt, status: Int) {
            super.onReliableWriteCompleted(gatt, status)
            logMsg("Descriptor ReliableWrite result %d", status)
        }

        override fun onMtuChanged(gatt: BluetoothGatt, mtu: Int, status: Int) {
            logMsg("Mtu changed to %d, data = %d", mtu, status)
            if (status == GATT_SUCCESS)
                onMtuChange(mtu)
            else
                onMtuError("Mtu set error $status")
        }

    }

    private fun servicesDiscovered() {
        val serviceList = gatt?.services?.map { AndroidBleGattService(this, it) } ?: listOf()
        onServicesDiscovered(serviceList)
    }

    private fun setDevice(device: BluetoothDevice) {
        this.device = device
        name = device.name ?: "Unknown"
    }

    override fun connect(automatic: Boolean) {
        device.connectGatt(context, automatic, gattCallback, BluetoothDevice.TRANSPORT_LE)
    }

    override fun disconnect() {
        super.disconnect()
        gatt?.disconnect()
    }

    override fun discover() {
        gatt?.let {
            discoverRetries = 0
            logMsg("Discover()")
            val services = it.services
            if (services != null && services.isNotEmpty())
                servicesDiscovered()
            else
                it.discoverServices()
        } ?: onDiscoveryError("Not connected")
    }

    override fun setNotification(characteristic: BleCharacteristic, enable: Boolean) {
        characteristic as AndroidBleCharacteristic
        if (gatt?.setCharacteristicNotification(characteristic.characteristic, enable) != true) {
            onNotificationSetError("Failed to enable notification")
            return
        }
        characteristic.characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG)?.let {
            val value =
                if (enable)
                    BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                else
                    BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                val result = gatt?.writeDescriptor(it, value)
                if (result != SUCCESS) {
                    logMsg("Error $result writing descriptor for ${characteristic.uuid}")
                    onNotificationSetError("Error writing descriptor")
                }
            } else {
                it.value = value
                if (gatt?.writeDescriptor(it) != true) {

                    logMsg("Error writing descriptor for ${characteristic.uuid}")
                    onNotificationSetError("Error writing descriptor")
                }
            }
            return
        }
        logMsg("No config descriptor for %s", characteristic.uuid)
        onNotificationSetError("No config descriptor")
    }

    override fun startReadDescriptors(characteristic: BleCharacteristic) {
        characteristic as AndroidBleCharacteristic
        onDescriptorsRead(characteristic, characteristic.characteristic.descriptors.map { it.uuid.toString() })
    }

    override fun readCharacteristic(characteristic: BleCharacteristic) {
        characteristic as AndroidBleCharacteristic
        if (gatt?.readCharacteristic(characteristic.characteristic) != true)
            onReadError(characteristic, "Error reading characteristic: " + characteristic.uuid)
    }

    override fun writeCharacteristic(characteristic: BleCharacteristic, data: ByteArray) {
        characteristic as AndroidBleCharacteristic
        //logMsg("Writing characteristic %s, %d bytes", characteristic.getUuid().toString(), data.length);
        characteristic.characteristic.value = data
        if (gatt?.writeCharacteristic(characteristic.characteristic) != true)
            onWriteError("Error writing characteristic: " + characteristic.uuid)
        //if(bleCharacteristic.getWriteType() == BleCharacteristic.WriteType.NO_RESPONSE)
        //  onWriteCharacteristic();
    }

    override fun requestMtu(mtu: Int) {
        if (gatt?.requestMtu(mtu) != true)
            onMtuError("Failed to request mtu $mtu")
    }

    override fun triggerRssiRead() {
        if (gatt?.readRemoteRssi() != true)
            onRssiError("Failed to request RSSI")
    }

    override fun setPriority(priority: Priority): Boolean {
        return gatt?.requestConnectionPriority(priority.ordinal) ?: false
    }

    companion object {
        private const val MAX_RETRIES = 3
        private val CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
        private val deviceMap = HashMap<String, AndroidBleDevice>()

        fun getDevice(btDevice: BluetoothDevice, context: Context): AndroidBleDevice {
            val address = btDevice.address.uppercase()
            val dev: AndroidBleDevice = deviceMap.getOrPut(address) { AndroidBleDevice(btDevice, context) }
            dev.setDevice(btDevice)
            return dev
        }
    }
}
