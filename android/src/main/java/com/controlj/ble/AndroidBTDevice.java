package com.controlj.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.controlj.logging.CJLog;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;


/**
 * Created by clyde on 4/6/17.
 */

public class AndroidBTDevice {

    static public List<BluetoothDevice> getBondedDevices() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> devs = adapter.getBondedDevices();
        return new ArrayList<>(devs);
    }

    static public Single<BluetoothSocket> getSocketConnector(BluetoothDevice device) {
        Single<BluetoothSocket> single = Single.create(e -> {
            BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
            socket.connect();
            e.onSuccess(socket);
        });
        return single.subscribeOn(Schedulers.io());
    }

    static public void writeToPrinter(BluetoothDevice device, String data) {
        //noinspection ResultOfMethodCallIgnored
        getSocketConnector(device)
            .observeOn(Schedulers.io())
            .subscribe(socket -> {
                    OutputStream stream = socket.getOutputStream();
                    OutputStreamWriter writer = new OutputStreamWriter(stream);
                    writer.write(data);
                    writer.flush();
                    writer.close();
                    stream.close();
                    Thread.sleep(500);
                    socket.close();
                },
                CJLog::logException);
    }

    static public BluetoothDevice getDevice(String address) {
        return BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
    }
}
