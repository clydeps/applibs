package com.controlj.ble

import android.bluetooth.BluetoothGattCharacteristic

/**
 * Created by clyde on 15/5/17.
 */

class AndroidBleCharacteristic(override val device: AndroidBleDevice, val characteristic: BluetoothGattCharacteristic) :
    BleCharacteristic(device, BleDevice.uuidCanon(characteristic.uuid.toString())) {

    override var writeType: WriteType
        get() = super.writeType
        set(writeType) {
            super.writeType = writeType
            when (writeType) {
                WriteType.NO_RESPONSE -> characteristic.writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE
                WriteType.SIGNED -> characteristic.writeType = BluetoothGattCharacteristic.PROPERTY_SIGNED_WRITE
                else -> characteristic.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
            }
        }
}
