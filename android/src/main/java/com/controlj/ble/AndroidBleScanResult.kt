package com.controlj.ble

import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanResult
import android.os.Build

/**
 * Created by clyde on 14/5/17.
 */
class AndroidBleScanResult(
    val dev: BluetoothDevice,
    private val scanResult: ScanResult,
    rssi: Int
) : BleScanResult(
    dev.address,
    scanResult.scanRecord?.deviceName ?: dev.name ?: "Unknown",
    rssi
) {

    override val isConnected: Boolean
        get() = BleScanner().getConnectedDevices(address).isNotEmpty()
    override val connectable: Boolean =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            scanResult.isConnectable
        else super.connectable

    override fun getManufacturerData(manuf: Int): ByteArray {
        return scanResult.scanRecord?.getManufacturerSpecificData(manuf) ?: byteArrayOf()
    }

    override val manufacturers: Iterable<Int>
        get() {
            return scanResult.scanRecord?.manufacturerSpecificData?.let { data ->
                (0 until data.size()).map { data.keyAt(it) }
            } ?: listOf()
        }
}
