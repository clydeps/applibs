package com.controlj.ble

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothProfile
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import com.controlj.logging.CJLog.logException
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers

/**
 * Created by clyde on 2/5/17.
 */

@Suppress("NewApi")
@SuppressLint("MissingPermission")
class AndroidBleScanner(val context: Context) : BleScanner(AndroidSchedulers.mainThread()) {
    internal var adapter: BluetoothAdapter
    private val manager: BluetoothManager
    private var scanner: BluetoothLeScanner? = null

    override val currentState: AdapterState
        get() = when (adapter.state) {
            BluetoothAdapter.STATE_ON -> AdapterState.Ready
            BluetoothAdapter.STATE_TURNING_ON -> AdapterState.PoweringOn
            else -> AdapterState.PoweredOff
        }

    private val callback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            val record = result.scanRecord
            val device = result.device
            if (device != null && record != null) {
                val bleScanResult = AndroidBleScanResult(device, result, result.rssi)
                onNextScanResult(bleScanResult)
            }
        }

        override fun onBatchScanResults(results: List<ScanResult>) {
            results.forEach { res ->
                onScanResult(0, res)
            }
        }

        override fun onScanFailed(errorCode: Int) {
            val error: BleScanException.ScanError
            when (errorCode) {
                SCAN_FAILED_APPLICATION_REGISTRATION_FAILED -> error =
                    BleScanException.ScanError.APP_REGISTRATION_FAILED
                SCAN_FAILED_FEATURE_UNSUPPORTED -> error = BleScanException.ScanError.UNSUPPORTED_FEATURE
                SCAN_FAILED_INTERNAL_ERROR -> error = BleScanException.ScanError.SCAN_INTERNAL_ERROR
                SCAN_FAILED_ALREADY_STARTED -> error = BleScanException.ScanError.SCAN_ALREADY_STARTED
                else -> error = BleScanException.ScanError.UNKNOWN_ERROR
            }
            onScanError(BleScanException(error))
        }
    }

    init {
        manager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        adapter = manager.adapter
        scanner = adapter.bluetoothLeScanner
        context.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                onStateChange()
            }
        }, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
        init()
    }

    override fun getDevice(address: String): BleDevice? {
        try {
            return AndroidBleDevice.getDevice(adapter.getRemoteDevice(address), context)
        } catch (ex: Exception) {
            logException(ex)
            return null
        }
    }

    override fun startScan() {
        scanner?.startScan(callback)
    }

    override fun getConnectedDevices(vararg addresses: String): List<BleDevice> {
        return manager.getDevicesMatchingConnectionStates(
            BluetoothProfile.GATT,
            intArrayOf(
                BluetoothProfile.STATE_CONNECTED,
                BluetoothProfile.STATE_CONNECTING,
                BluetoothProfile.STATE_DISCONNECTING,
            )
        )
            .filter { addresses.isEmpty() || addresses.contains(it.address) }
            .map { AndroidBleDevice.getDevice(it, context) }
    }

    override fun stopScan() {
        scanner?.stopScan(callback)
    }

    override val isBleSupported: Boolean by lazy {
        context.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)
    }
}
