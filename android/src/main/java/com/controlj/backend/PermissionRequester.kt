package com.controlj.backend

import android.app.Activity
import android.content.DialogInterface
import android.content.pm.PackageManager
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.controlj.Utilities.R
import com.tbruyelle.rxpermissions3.RxPermissions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single

object PermissionRequester {

    data class RequestSet(
        val permissions: List<String>,
        val rationale: String,
    )

    fun RequestSet.asker(activity: Activity): Single<List<String>> {
        val granted = permissions.filter {
            ContextCompat.checkSelfPermission(activity, it) == PackageManager.PERMISSION_GRANTED
        }

        val perms = (permissions.toList() - granted)
        val requestString = perms.joinToString(
            separator = "\n",
            prefix = activity.getString(R.string.permission_preamble),
            postfix = "\n" + rationale
        ) { "\u2022 " + with(activity.packageManager) { getPermissionInfo(it, 0).loadLabel(this) } } +
                if (perms.size != 1)
                    "\nDo you want to grant these permissions?"
                else
                    "\nDo you want to grant this permission?"
        return if (perms.isEmpty())
            Single.just(granted)
        else
            ask(requestString, "Yes", "Not now", activity).filter { it }
                .flatMapSingle {
                    RxPermissions(activity as FragmentActivity).request(*perms.toTypedArray()).first(false)
                }
                .map { ok ->
                    if (ok)
                        permissions
                    else
                        granted
                }
                .defaultIfEmpty(granted)
                .doAfterSuccess {
                    val now = System.currentTimeMillis()
                    lastAsked.putAll(perms.map { it to now })
                }
    }

    /**
     * Cache requests and share them so as not to duplicate
     */
    private val requestMap = mutableMapOf<RequestSet, Single<List<String>>>()

    /**
     * keep in memory a record of when we last asked
     */
    private val lastAsked = mutableMapOf<String, Long>()

    /**
     * Request permission for a set of things
     * @param activity The activity to present the dialog on
     * @param rationale A string describing why we need the permissions
     * @param permissions The list of permissions required
     * @param noDelay True if we should override the usual 1 day delay between requests
     */
    fun requestPermission(
        activity: Activity,
        rationale: String,
        vararg permissions: String,
        noDelay: Boolean = false
    ): Single<List<String>> {
        val now = System.currentTimeMillis()
        val perms = permissions.filter { noDelay || now >= (lastAsked[it] ?: 0L) + PERM_REQUEST_DELAY }
        val req = RequestSet(perms, rationale)
        return requestMap.getOrPut(req) { req.asker(activity).toObservable().share().singleOrError() }
    }

    fun ask(question: String, yes: String, no: String?, context: Activity): Single<Boolean> {
        val obs = Single.create<Boolean> { e ->

            val builder = AlertDialog.Builder(context)
            val dialogClickListener = { dialog: DialogInterface, which: Int ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> e.onSuccess(true)

                    DialogInterface.BUTTON_NEGATIVE -> e.onSuccess(false)
                }
                dialog.dismiss()
            }
            builder.setMessage(question).setPositiveButton(yes, dialogClickListener)
            if (no != null) {
                builder.setNegativeButton(no, dialogClickListener)
                builder.setCancelable(false)
            }
            builder.show()
        }
        return obs.observeOn(AndroidSchedulers.mainThread())
    }

    private const val PERM_REQUEST_DELAY = 24 * 60 * 60 * 1000   // milliseconds between permission requests
}
