package com.controlj.backend

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.WindowManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.RelativeLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.controlj.Utilities.R
import com.controlj.graphics.CColor
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsFactory
import com.controlj.rx.DisposedSingleEmitter
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.view.ButtonEntry
import com.controlj.view.DialogEntry
import com.controlj.view.LabelledEntry
import com.controlj.view.TextDelegate
import com.controlj.view.ViewContext
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import kotlin.math.ceil

/**
 * Created by clyde on 18/6/17.
 */

class DialogBuilder : androidx.fragment.app.DialogFragment(), DialogData.DialogShower {

    private lateinit var alertDialog: AlertDialog
    private val dialogWidth = 500f
    private var entries = mutableListOf<DialogEntry>()
    private lateinit var dialogData: DialogData
    private lateinit var outerView: FrameLayout
    private var emitter: SingleEmitter<String> = DisposedSingleEmitter()
    private val metrics by lazy { requireActivity().resources.displayMetrics }
    private val width by lazy {
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dialogWidth, metrics)
            .coerceAtMost(GraphicsFactory().screenBounds.width.toFloat()).toInt()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        retainInstance = true

        outerView = FrameLayout(requireActivity())
        outerView.layoutParams = ViewGroup.LayoutParams(width, WRAP_CONTENT)
        dialogData = dataMap[tag] ?: throw IllegalStateException("tag $tag not found in dataMap")
        entries = dialogData.items.map { DialogEntry.create(requireActivity(), it, dialogData) }.toMutableList()

        val builder = AlertDialog.Builder(context)
        val title = TextView(context)
        title.text = dialogData.title
        title.gravity = Gravity.CENTER
        title.setTextColor(CColor.BLACK)
        title.textSize = 18f
        if (dialogData.closeButton) {
            val titleBar = RelativeLayout(context)
            titleBar.layoutParams = RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
            titleBar.addView(title, RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).also {
                it.addRule(RelativeLayout.CENTER_IN_PARENT)
            })
            val closeButton = ImageView(context).also {
                it.setImageResource(R.drawable.close_x)
            }
            titleBar.addView(closeButton, RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT).also {
                it.addRule(RelativeLayout.ALIGN_PARENT_END)
                it.marginEnd = 10
                it.bottomMargin = 10
                it.topMargin = 10
            })
            closeButton.setOnClickListener { dismiss() }
            builder.setCustomTitle(titleBar)
        } else
            builder.setCustomTitle(title)
        val view = LinearLayout(context)
        view.orientation = LinearLayout.VERTICAL
        val padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, PADDING.toFloat(), metrics).toInt()
        view.setPadding(padding, padding, padding, padding)
        view.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        val textWidth = ceil(
            padding * 4 + TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP, dialogData.labelWidth.toFloat(), metrics
            )
        ).toInt()
        val labels = entries.filterIsInstance<LabelledEntry>()
        labels.filter { it.item.style.labelled }.forEach { it.label.layoutParams.width = textWidth }
        entries.forEach { view.addView(it.view) }
        val buttonView = LinearLayout(context)
        buttonView.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        buttonView.orientation = LinearLayout.HORIZONTAL
        dialogData.buttonList.forEach {
            val button = ButtonEntry(requireActivity(), it, dialogData)
            button.view.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f)
            entries.add(button)
            buttonView.addView(button.view)
        }
        view.addView(buttonView)
        val scrollView = ScrollView(context)
        scrollView.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        scrollView.isFillViewport = true
        scrollView.addView(view)
        outerView.addView(scrollView)
        builder.setView(outerView)
        alertDialog = builder.create()
        alertDialog.setCanceledOnTouchOutside(dialogData.type.closeable)
        //alertDialog.setOnShowListener { alertDialog.window?.setLayout(width, WRAP_CONTENT) }
        return alertDialog
    }

    override fun onDestroy() {
        dialogData.shower = null
        entries.forEach { it.onDestroy() }
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(width, WRAP_CONTENT)
        refresh()
    }

    override fun dismiss() {
        dismissAllowingStateLoss()
    }

    override fun refresh(source: DialogItem?) {
        entries.forEach { it.refresh() }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        dialogData.dismiss()
        dataMap.remove(dialogData.title)
    }

    override fun showTextBox(
        title: String,
        value: String,
        style: DialogItem.InputStyle,
        source: CRect,
        timeout: Long
    ): Single<String> {
        return Single.create {
            emitter = it
            val layout = layoutInflater.inflate(R.layout.popup_text, outerView, false)
            val popupTitle = layout.findViewById<TextView>(R.id.popup_title)
            val popupValue = layout.findViewById<EditText>(R.id.popup_value)
            val watcher = TextDelegate(popupValue, style) { _: EditText, _: String -> }
            popupTitle.text = title
            watcher.setText(value)
            val popup = PopupWindow(layout, outerView.width, outerView.height, true)
            popupValue.setSelection(0, value.length)
            popupValue.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus)
                    requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
            }
            popup.setBackgroundDrawable(BitmapDrawable(requireActivity().resources, ""))
            popup.isOutsideTouchable = true
            popup.setOnDismissListener {
                emitter.onSuccess(popupValue.text.toString())
                requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

            }
            popup.showAtLocation(outerView, Gravity.CENTER, 0, 0)
            popup.isFocusable = true
            popup.update()
        }
    }

    companion object {
        const val PADDING = 6

        internal val dataMap = mutableMapOf<String, DialogData>()
        fun show(context: FragmentActivity, dialogData: DialogData) {
            dataMap[dialogData.title] = dialogData
            val fragment = DialogBuilder()
            fragment.show(context.supportFragmentManager, dialogData.title)
            dialogData.shower = fragment
        }

        fun dismiss(context: ViewContext, dialogData: DialogData) {
            context as AppCompatActivity
            val fragment =
                context.supportFragmentManager.findFragmentByTag(dialogData.title) as androidx.fragment.app.DialogFragment?
            fragment?.dismiss()
        }
    }
}
