package com.controlj.backend

import com.controlj.logging.CJLog.logException
import com.controlj.logging.Destination
import com.controlj.settings.DataStore
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.File

/**
 * Created by clyde on 5/12/17.
 */
class CrashlyticsLogger(val dataStore: DataStore) : Destination {
    override val previousLogfiles: List<File>?
        get() {
            return null
        }

    val disposable: Disposable

    init {
        storePrefs()
        disposable = dataStore.observeChanges
                .observeOn(Schedulers.computation())
                .subscribe({ _ -> storePrefs() },
                        { logException(it) })
    }

    private fun storePrefs() {
        val sb = StringBuilder()
        val entries = dataStore.allEntries
        for (key: String in entries.keys) {
            if (key.indexOf("password", 0) < 0) {
                sb.append(key)
                sb.append('=')
                sb.append(entries.get(key).toString())
                sb.append('\n')
            }
        }
        //Crashlytics.setString("SHARED_PREFS", sb.toString())
    }

    override fun sendMessage(deviceID: String, message: String) {
        //Crashlytics.log(message)
    }

    override fun retrieveMessages(limit: Int): String? {
        return null
    }

    override fun close() {
        disposable.dispose()
    }
}
