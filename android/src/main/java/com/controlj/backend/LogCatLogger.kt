package com.controlj.backend

import android.util.Log

import com.controlj.logging.Destination

/**
 * Created by clyde on 2/5/17.
 */

class LogCatLogger : Destination {
    override fun sendMessage(deviceID: String, message: String) {
        Log.d(deviceID, message)
    }
}
