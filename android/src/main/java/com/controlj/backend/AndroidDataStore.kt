package com.controlj.backend

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.controlj.logging.CJLog
import com.controlj.logging.CJLog.logException
import com.controlj.settings.DataStore
import com.controlj.settings.DataStore.Companion.DEBUG_FEATURES
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import java.lang.reflect.Type
import java.util.ArrayList
import java.util.HashMap

/**
 * Created by clyde on 14/5/17.
 */
class AndroidDataStore(
    val context: Context,
    filename: String = ""
) : DataStore {

    // if this is the default datastore, point the global instance to it.
    private val prefs = context.getSharedPreferences(
        if (filename.isBlank()) {
            init()
            context.applicationContext.packageName + "_preferences"
        } else
            filename,
        Context.MODE_PRIVATE
    )

    private val gson = Gson()

    override val allEntries: Map<String, *>
        get() = prefs.all

    override val allStringEntries: Map<String, String>
        get() {
            val entries = prefs.all
            val map = HashMap<String, String>()
            for (key in entries.keys) {
                val o = entries[key]
                if (o is String)
                    map[key] = o
            }
            return map
        }
    override val isDebugFeatures: Boolean
        get() = CJLog.isDebug || getBoolean(DEBUG_FEATURES)

    override fun getInt(key: String, dflt: Int): Int {
        return try {
            prefs.getInt(key, dflt)
        } catch (e: ClassCastException) {
            val s = prefs.getString(key, dflt.toString()) ?: dflt.toString()
            Integer.parseInt(s)
        }

    }

    override fun getFloat(key: String, dflt: Float): Float {
        return prefs.getFloat(key, dflt)
    }

    override fun getString(key: String, dflt: String): String {
        return prefs.getString(key, dflt) ?: dflt
    }

    override fun getLong(key: String, dflt: Long): Long {
        return try {
            val s = prefs.getString(key, dflt.toString())
            s?.toLong() ?: dflt
        } catch (e: ClassCastException) {
            prefs.getLong(key, 0)
        }
    }

    override fun putInt(key: String, value: Int) {
        val editor = prefs.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    override fun putFloat(key: String, value: Float) {
        val editor = prefs.edit()
        editor.putFloat(key, value)
        editor.apply()
    }

    override fun putString(key: String, value: String) {
        val editor = prefs.edit()
        editor.putString(key, value)
        editor.apply()
    }

    override fun putLong(key: String, value: Long) {
        val editor = prefs.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    override fun remove(key: String) {
        val editor = prefs.edit()
        editor.remove(key)
        editor.apply()
    }

    override fun getIntList(key: String): List<Int> {
        val s = getString(key)
        return gson.fromJson<List<Int>>(s, object : TypeToken<List<Int>>() {}.type)
            ?: return ArrayList()
    }

    override fun getStringList(key: String): List<String> {
        val s = getString(key)
        return gson.fromJson<List<String>>(s, object : TypeToken<List<String>>() {}.type)
            ?: return ArrayList()
    }

    override fun getBoolean(name: String): Boolean {
        return try {
            prefs.getBoolean(name, false)
        } catch (ex: ClassCastException) {
            val s = prefs.getString(name, "false")
            putBoolean(name, s != "false")
            s != "false"
        }

    }

    override fun putBoolean(key: String, value: Boolean) {
        val editor = prefs.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    override fun putIntList(key: String, values: List<Int>) {
        putString(key, gson.toJson(values))
    }

    override fun putStringList(key: String, values: List<String>) {
        putString(key, gson.toJson(values))
    }

    override fun contains(key: String): Boolean {
        return prefs.contains(key)
    }

    override fun <T> getObject(key: String, clazz: Class<T>): T? {
        val s = prefs.getString(key, null) ?: return null
        return try {
            gson.fromJson(s, clazz)
        } catch (ex: JsonSyntaxException) {
            null
        }
    }

    override fun <T> getObject(key: String, type: Type): T? {
        val s = prefs.getString(key, null) ?: return null
        return try {
            gson.fromJson<T>(s, type)
        } catch (ex: Exception) {
            logException(ex)
            null
        }
    }

    override val observeChanges: Observable<String> by lazy {
        Observable.create { e: ObservableEmitter<String> ->
            val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key -> e.onNext(key) }
            prefs.registerOnSharedPreferenceChangeListener(listener)
            e.setCancellable { prefs.unregisterOnSharedPreferenceChangeListener(listener) }
        }
            .share()
            .observeOn(AndroidSchedulers.mainThread())
    }

    override fun putObject(key: String, o: Any) {
        putString(key, gson.toJson(o))
    }

    @SuppressLint("ApplySharedPref")
    override fun commit() {
        Schedulers.io().scheduleDirect { prefs.edit().commit() }
    }
}
