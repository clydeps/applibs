package com.controlj.licensing;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 5/02/2016
 * Time: 10:51 AM
 */
public class Decoder {
	private static final String TAG = Decoder.class.getSimpleName();

	public static Map<String, String> decodeExtras(String extras) {
		Map<String, String> results = new HashMap<String, String>();
		try {
			String[] pairs = extras.split("&");
			for(String p : pairs) {
                String[] ps = p.split("=");
				if(ps.length == 2) {
					ps[0] = URLDecoder.decode(ps[0], "UTF-8");
					String name = ps[0];
					int i = 0;
					while(results.containsKey(name)) {
						name = ps[0] + ++i;
					}
					results.put(name, URLDecoder.decode(ps[1], "UTF-8"));
				}
			}
		} catch(UnsupportedEncodingException e) {
			Log.w(TAG, "Invalid syntax error while decoding extras data from server.");
		}
		return results;
	}
}
