package com.controlj

import android.content.Context
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.controlj.Utilities.R

/**
 * Convenience function for boolean visible flag on Android views.
 *
 */

var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.INVISIBLE
    }

// wrappers for findViewById

fun View.textView(id: Int): TextView = findViewById(id)
fun View.imageView(id: Int): ImageView = findViewById(id)
fun View.checkBox(id: Int): CheckBox = findViewById(id)

fun Context.getImageId(name: String): Int {
    val result = resources.getIdentifier(name.lowercase(), "drawable", packageName)
    if (result == 0) error("Did not find resource drawable \"$name\"")
    return result
}


/**
 * Change the background color by altering the greyness.
 */
fun View.setBackgroundTint(frac: Double) {
    val back = ContextCompat.getColor(context, R.color.field_normal)
    setBackgroundColor(back)
}

fun View.dpToPx(dp: Double): Int {
    val density = resources.getDisplayMetrics().density;
    return (dp * density + 0.5f).toInt()
}
