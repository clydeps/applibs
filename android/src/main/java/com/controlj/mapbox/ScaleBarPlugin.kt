package com.controlj.mapbox

import android.content.Context
import android.view.View
import androidx.annotation.UiThread
import androidx.annotation.VisibleForTesting
import com.mapbox.mapboxsdk.log.Logger
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.MapboxMap.OnCameraIdleListener
import com.mapbox.mapboxsdk.maps.MapboxMap.OnCameraMoveListener
import com.mapbox.mapboxsdk.maps.Projection

/**
 * Plugin class that shows a scale bar on MapView and changes the scale corresponding to the MapView's scale.
 */
class ScaleBarPlugin(private val mapView: MapView, private val mapboxMap: MapboxMap) {
    private val projection: Projection = mapboxMap.projection
    private var scaleBarWidget: ScaleBarWidget? = null

    @VisibleForTesting
    val cameraMoveListener = OnCameraMoveListener { invalidateScaleBar() }

    @VisibleForTesting
    val cameraIdleListener = OnCameraIdleListener { invalidateScaleBar() }

    /**
     * Create a scale bar widget on mapView.
     *
     * @param context The android context
     * @param config A block to configure the newly created widget
     * @return The created ScaleBarWidget instance.
     */
    @UiThread
    fun create(context: Context, config: ScaleBarWidget.() -> Unit): ScaleBarWidget {
        scaleBarWidget?.let { mapView.removeView(it) }
        ScaleBarWidget(context).apply(config).let { widget ->
            scaleBarWidget = widget
            widget.mapViewWidth = mapView.width
            mapView.addView(widget)
            enabled = true
            return widget
        }
    }

    /**
     * Toggles the scale plugin state.
     *
     *
     * If the scale plugin was enabled, a [MapboxMap.OnCameraMoveListener]
     * will be added to the [MapView] to listen to scale change events to update the state of this plugin. If the
     * plugin was disabled the [MapboxMap.OnCameraMoveListener]
     * will be removed from the map.
     *
     */
    var enabled: Boolean = false
        @UiThread
        set(value) {
            if (field == value)
                return
            scaleBarWidget?.let { widget ->
                field = value
                widget.visibility = if (value) View.VISIBLE else View.GONE
                if (value) {
                    mapboxMap.addOnCameraMoveListener(cameraMoveListener)
                    mapboxMap.addOnCameraIdleListener(cameraIdleListener)
                    invalidateScaleBar()
                } else {
                    mapboxMap.removeOnCameraMoveListener(cameraMoveListener)
                    mapboxMap.removeOnCameraIdleListener(cameraIdleListener)
                }
                return
            }
            Logger.w(TAG, "Create a widget before changing ScalebBarPlugin's state. Ignoring.")
        }

    private fun invalidateScaleBar() {
        scaleBarWidget?.metersPerPixel = (
            projection.getMetersPerPixelAtLatitude(mapboxMap.cameraPosition.target.latitude)
                    / mapView.pixelRatio
        )
    }

    companion object {
        private const val TAG = "Mbgl-ScaleBarPlugin"
    }

}
