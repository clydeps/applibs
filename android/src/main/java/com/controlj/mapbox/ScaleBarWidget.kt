package com.controlj.mapbox

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Build
import android.os.Handler
import android.os.Message
import android.util.TypedValue
import android.view.View
import com.controlj.mapbox.ScaleBarUnit.Companion.getLegends
import java.lang.ref.WeakReference
import kotlin.math.roundToInt

/**
 * The scale widget is a visual representation of the scale bar plugin.
 */
@Suppress("DEPRECATION")
class ScaleBarWidget internal constructor(context: Context) : View(context) {

    /**
     * plugin's minimum refresh interval, in millisecond.
     */
    var refreshInterval = REFRESH_INTERVAL_DEFAULT

    /**
     * textColor the text color on scale bar.
     */
    var textColor = 0
        set(textColor) {
            field = textColor
            textPaint.color = textColor
        }

    /**
     * the primary color of the scale bar.
     */
    var primaryColor: Int

    /**
     * the secondary color of the scale bar.
     */
    var secondaryColor: Int

    /**
     * the margin between scale bar and the top of mapView.
     */
    var marginTop: Double

    /**
     * the margin between text and blocks inside scale bar.
     */
    var textBarMargin = 0.0

    /**
     * Get the bar height for blocks.
     */
    var barHeight = 0.0

    /**
     * Get the border width in scale bar.
     */
    var borderWidth = 0.0

    /**
     * Set the ratio of scale bar max width compared with MapView width.
     */
    var widthRatio = 0.5

    /**
     * The choice of units
     */

    var unitSet: List<ScaleBarUnit> = ScaleBarUnit.metric

    /**
     * Get the width of current mapView.
     */
    var mapViewWidth = 0

    /**
     * the text size
     */

    var textSize: Double

    /**
     * The left margin
     */
    var marginLeft: Double

    /**
     * True if the widget should be centered
     */

    var isCentered: Boolean = true

    private val textPaint = Paint()
    private val barPaint = Paint()
    private val refreshHandler: RefreshHandler = RefreshHandler(this)

    init {
        textPaint.isAntiAlias = true
        textPaint.textAlign = Paint.Align.CENTER
        barPaint.isAntiAlias = true
        val metrics = context.resources.displayMetrics
        fun Double.dpToPixels(): Double =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), metrics).toDouble()

        barHeight = 4.0.dpToPixels()
        borderWidth = 1.0.dpToPixels()
        textSize = 8.0.dpToPixels()
        marginTop = 8.0.dpToPixels()
        marginLeft = 10.0.dpToPixels()
        textBarMargin = 2.0.dpToPixels()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            primaryColor = context.resources.getColor(android.R.color.black, null)
            secondaryColor = context.resources.getColor(android.R.color.white, null)
        } else {
            primaryColor = context.resources.getColor(android.R.color.black)
            secondaryColor = context.resources.getColor(android.R.color.white)
        }
    }


    override fun onDraw(canvas: Canvas) {
        if (metersPerPixel <= 0) {
            return
        }
        var lMargin: Double = if (isCentered)
            (mapViewWidth.toDouble() * (1 - widthRatio)) / 2
        else
            marginLeft
        val maxDistance = (mapViewWidth.toDouble() - lMargin) * metersPerPixel * widthRatio / 2
        val ticks = unitSet.getLegends(maxDistance)
        val unitBarWidth = (ticks.length / metersPerPixel).toFloat() / ticks.ticks
        lMargin = (mapViewWidth.toDouble() - unitBarWidth * ticks.ticks) / 2
        //Drawing the surrounding borders
        barPaint.style = Paint.Style.FILL_AND_STROKE
        barPaint.color = secondaryColor
        canvas.drawRect(
            (lMargin - borderWidth * 2).toFloat(),
            (textBarMargin + textSize + marginTop - borderWidth * 2).toFloat(),
            (lMargin + unitBarWidth * ticks.ticks + borderWidth * 2).toFloat(),
            (textBarMargin + textSize + marginTop + barHeight + borderWidth * 2).toFloat(),
            barPaint
        )
        barPaint.color = primaryColor
        canvas.drawRect(
            (lMargin - borderWidth).toFloat(),
            (textBarMargin + textSize + marginTop - borderWidth).toFloat(),
            (lMargin + unitBarWidth * ticks.ticks + borderWidth).toFloat(),
            (textBarMargin + textSize + marginTop + barHeight + borderWidth).toFloat(),
            barPaint
        )

        //Drawing the fill
        barPaint.style = Paint.Style.FILL
        textPaint.textSize = textSize.toFloat()
        for (i in 0 until ticks.ticks) {
            barPaint.color = if (i % 2 == 0) primaryColor else secondaryColor
            val text = if (i == 0)
                ticks.name
            else
                (ticks.tickLength * i).roundToInt().toString()
            canvas.drawText(
                text,
                (lMargin + unitBarWidth * i).toFloat(),
                (textSize + marginTop).toFloat(),
                textPaint
            )
            canvas.drawRect(
                (lMargin + unitBarWidth * i).toFloat(),
                (textBarMargin + textSize + marginTop).toFloat(),
                (lMargin + unitBarWidth * (1 + i)).toFloat(),
                (textBarMargin + textSize + marginTop + barHeight).toFloat(),
                barPaint
            )
        }
        canvas.drawText(
            (ticks.tickLength * ticks.ticks).roundToInt().toString(),
            (lMargin + unitBarWidth * ticks.ticks).toFloat(),
            (textSize + marginTop).toFloat(),
            textPaint
        )
    }

    /**
     * how many meters in each pixel.
     * Update the scale when mapView's scale has changed.
     */
    var metersPerPixel: Double = 0.0
        set(value) {
            field = value
            if (!refreshHandler.hasMessages(MSG_WHAT)) {
                refreshHandler.sendEmptyMessageDelayed(MSG_WHAT, refreshInterval.toLong())
            }
        }

    /**
     * Handler class to limit the refresh frequent.
     */
    private class RefreshHandler(scaleBarWidget: ScaleBarWidget) : Handler() {
        var scaleBarWidgetWeakReference: WeakReference<ScaleBarWidget> = WeakReference(scaleBarWidget)
        override fun handleMessage(msg: Message) {
            val scaleBarWidget = scaleBarWidgetWeakReference.get()
            if (msg.what == MSG_WHAT && scaleBarWidget != null) {
                scaleBarWidget.invalidate()
            }
        }

    }

    companion object {
        private const val MSG_WHAT = 0
        private const val REFRESH_INTERVAL_DEFAULT = 15
    }
}
