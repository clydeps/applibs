package com.controlj.mapbox

import kotlin.math.floor
import kotlin.math.log10
import kotlin.math.pow

/**
 * A unit for use on a scale bar.
 * @param The name of the unit
 * @param The number of meters/unit
 */
class ScaleBarUnit(val name: String, val scale: Double) {

    /**
     * Common predefined lists.
     */
    companion object {
        val metric = listOf(
            ScaleBarUnit("km", 1000.0),
            ScaleBarUnit("m", 1.0)
        )
        val imperial = listOf(
            ScaleBarUnit("mi", 1609.344),
            ScaleBarUnit("ft", 0.3048)
        )
        val nautical =
            listOf(
                ScaleBarUnit("nm", 1852.0),
                ScaleBarUnit("ft", 0.3048)
            )

        /**
         * Given a distance in meters, determine which unit, what span and how many divisions to show.
         */

        fun List<ScaleBarUnit>.getLegends(distance: Double): ScaleLegends {
            // select a unit so we will have at least 2 divisions. By default, choose the last
            val unit = firstOrNull { it.scale <= distance } ?: last()
            val maxval = niceNum(distance / unit.scale)
            val tickCount = if (maxval.first % 3 == 0) 3 else 2
            return ScaleLegends(unit.name, tickCount, maxval.second / tickCount, maxval.second * unit.scale)
        }

        /**
         * Returns a "nice" number equal to or greater than range
         *
         * @param range the data range
         * @return a "nice" number to be used for the data range, represented as a base number and multiplier
         */
        private fun niceNum(range: Double): Pair<Int, Double> {
            val multiplier = 10.0.pow(floor(log10(range)))
            val fraction = range / multiplier

            val base = when {
                fraction <= 1 -> 1
                fraction <= 2 -> 2
                fraction <= 3 -> 3
                fraction <= 6 -> 6
                else -> 10
            }
            return base to multiplier * base
        }
    }



    /**
     * A class to wrap the return value from getLegends()
     * @param name The unit name
     * @param ticks the number of ticks
     * @param tickLength The length of a tick in the chosen unit
     * @param length the total length in meters
     */


    class ScaleLegends(val name: String, val ticks: Int, val tickLength: Double, val length: Double)
}
