package com.controlj.androidutil;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 31/03/2015
 * Time: 4:34 PM
 */
public interface ProgressListener {

	// notify that progress has reached value units out of max
    void incProgress(int value);
}
