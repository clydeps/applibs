package com.controlj.androidutil;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.controlj.Utilities.R;
import com.controlj.utility.Parameter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 8/02/2016
 * Time: 12:44 PM
 */

public class History extends BaseAdapter {
    public static class Entry extends Parameter {

        public Entry(String name, String value) {
            super(name, value);
        }
    }

    static final int MAX_HISTORY = 20;
    HashMap<String, Entry> entryMap;
    LinkedList<Entry> values;
    final String pref_key;
    private Context context;
    boolean dirty;
    Gson gson = new Gson();

    public History(String pref_key, Context context) {
        this.pref_key = pref_key;
        this.context = context;
        entryMap = new HashMap<>();
        values = new LinkedList<>();
        String saved = context.getSharedPreferences(pref_key, Context.MODE_PRIVATE).getString(pref_key, "");
        if(!saved.isEmpty()) {
            ArrayList<Entry> newValues = gson.fromJson(saved, new TypeToken<ArrayList<Entry>>() {}.getType());
            for(Entry entry : newValues) {
                add(entry);
            }
        }
    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        dirty = true;
    }

    public void remove(Entry val) {
        remove(val.getName());
    }

    public void remove(String key)  {
        if(entryMap.containsKey(key)) {
            values.remove(entryMap.get(key));
            entryMap.remove(key);
            notifyDataSetChanged();
        }
    }

    public void add(Entry e) {
        if(e.getName() == null)
            return;
        remove(e.getName());
        if(values.size() == MAX_HISTORY)
            remove(values.getLast());
        values.addFirst(e);
        entryMap.put(e.getName(), e);
        notifyDataSetChanged();
    }
    public Entry add(String val, String data) {
        Entry p = new Entry(val, data);
        add(p);
        return p;
    }

    public void save() {
        if(!dirty)
            return;
        ArrayList<Entry> list = new ArrayList<>();
        list.addAll(values);
        String saveValue = gson.toJson(list);
        SharedPreferences sp = context.getSharedPreferences(pref_key, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putString(pref_key, saveValue);
        ed.apply();
        dirty = false;
    }

    public String getKey(int i) {
        if(i >= getCount())
            return null;
        return getParam(i).getName();
    }

    public String getValue(int i) {
        if(i >= getCount())
            return null;
        return getParam(i).getValue();
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int pos) {
        return getKey(pos);
    }

    public Entry getParam(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
            convertView = View.inflate(context, R.layout.historytext, null);
        TextView tv = convertView.findViewById(R.id.textViewItem);
        tv.setText(getKey(position));
        return convertView;
    }

    public List<Entry> getValues() {
        return new ArrayList<>(values);
    }
}
