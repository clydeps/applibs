package com.controlj.androidutil

import android.text.InputFilter
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * This class implements a filter for input text that represents a waypoint code. It converts
 * lowercase letters to uppercase, and filters out all except letters, digits and spaces.
 *
 *
 * User: clyde
 * Date: 19/09/2016
 * Time: 5:48 PM
 */
class CodeInputFilter : InputFilter {
    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
        if (start > end)
            return null
        var keepOriginal = true
        val sb = StringBuilder()
        for (i in start until end) {
            val c = source[i]
            if (c.code >= 128)
                continue
            if (!(c == ' ' || Character.isUpperCase(c) || Character.isDigit(c)))
                keepOriginal = false
            if (Character.isLowerCase(c))
                sb.append(Character.toUpperCase(c))
            else if (Character.isLetterOrDigit(c))
                sb.append(c)
            else if (c == ' ')
                sb.append(c)
        }
        if (keepOriginal)
            return null
        if (source is Spanned) {
            val sp = SpannableString(sb)
            TextUtils.copySpansFrom(source, start, end, null, sp, 0)
            return sp
        }
        return sb
    }
}
