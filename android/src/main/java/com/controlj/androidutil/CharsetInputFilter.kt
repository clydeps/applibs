package com.controlj.androidutil

import android.text.InputFilter
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * This class implements a filter for input text that contains only characters in the supplied string.
 * case conversion will be done if
 *
 *
 * User: clyde
 * Date: 19/09/2016
 * Time: 5:48 PM
 */
class CharsetInputFilter(val charset: Set<Char>) : InputFilter {
    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
        if (start > end)
            return null
        var keepOriginal = true
        val sb = StringBuilder()
        for (i in start until end) {
            val c = source[i]
            val other = when {
                c.isUpperCase() -> c.lowercase()
                c.isLowerCase() -> c.uppercase()
                else -> c
            }
            if(!charset.contains(c)) {
                keepOriginal = false
                if(charset.contains(other))
                    sb.append(other)
            } else
                sb.append(c)
        }
        if (keepOriginal)
            return null
        if (source is Spanned) {
            val sp = SpannableString(sb)
            TextUtils.copySpansFrom(source, start, end, null, sp, 0)
            return sp
        }
        return sb
    }
}
