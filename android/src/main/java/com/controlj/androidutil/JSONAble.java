package com.controlj.androidutil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 11/05/2016
 * Time: 6:51 AM
 */
public interface JSONAble {
	JSONObject toJSON() throws JSONException;
	void fromJSON(JSONObject jobj) throws JSONException;
}
