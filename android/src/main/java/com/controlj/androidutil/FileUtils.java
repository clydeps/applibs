package com.controlj.androidutil;

import java.io.*;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 19/05/15
 * Time: 6:02 PM
 */
public class FileUtils {

	static public String fileToString(File f) throws IOException {
		BufferedReader rd = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
		String line;
		StringBuilder response = new StringBuilder();
		while((line = rd.readLine()) != null) {
			response.append(line);
			response.append('\n');
		}
		rd.close();
		return response.toString();
	}
}
