package com.controlj.androidutil

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.controlj.Utilities.R
import com.controlj.data.Deletable
import com.controlj.settings.DataStore
import java.lang.reflect.Type
import java.util.LinkedList

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 8/02/2016
 * Time: 12:44 PM
 */

class PersistentAdapter<T : Deletable>(private val pref_key: String, val context: Activity, val token: Type) : BaseAdapter() {
    internal var values: LinkedList<T> = LinkedList()

    init {
        val newValues = DataStore().getObject<List<T>>(pref_key, token)
        if (newValues != null)
            for (entry in newValues) values.add(entry)
    }

    fun remove(item: T): Boolean {
        val index = values.indexOf(item)
        if (index != -1) {
            remove(index)
            save()
            return true
        }
        return false

    }

    fun remove(index: Int) {
        val item = values.get(index)
        values.removeAt(index)
        item.delete()
        save()
    }

    fun add(vararg items: T) {
        for (item in items) {
            if (values.size == MAX_HISTORY)
                remove(MAX_HISTORY - 1)
            values.addFirst(item)
        }
        save()
    }

    fun save() {
        notifyDataSetChanged()
        val list = ArrayList<T>()
        list.addAll(values)
        DataStore().putObject(pref_key, list)
    }

    override fun getItem(i: Int): T? {
        return if (i >= count) null else values[i]
    }

    override fun getCount(): Int {
        return values.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val newView = convertView ?: View.inflate(context, R.layout.historytext, null)
        val tv = newView.findViewById<TextView>(R.id.textViewItem)
        tv.text = getItem(position)!!.toString()
        return newView
    }

    fun getValues(): List<T> {
        return ArrayList(values)
    }

    companion object {
        internal val MAX_HISTORY = 20
    }
}
