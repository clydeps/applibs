package com.controlj.viewmodel

import com.google.gson.annotations.Expose

/**
 * Created by clyde on 2/9/18.
 */
open class PageViewModel : ViewModel {
    override fun onDestroy() {
    }

    @Expose
    val pageNumber = ViewModel.IntData()
}
