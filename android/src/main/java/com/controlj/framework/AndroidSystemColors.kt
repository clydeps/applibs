package com.controlj.framework

import com.controlj.graphics.CColor

object AndroidSystemColors: SystemColors {
    init {
        init()
    }

    override val label: Int
        get() = CColor.BLACK
    override val secondaryLabel: Int
        get() = CColor.grey(0.3)
    override val systemFill: Int
        get() = TODO("Not yet implemented")
    override val secondarySystemFill: Int
        get() = TODO("Not yet implemented")
    override val placeholderText: Int
        get() = CColor.grey(0.5)
    override val systemBackground: Int
        get() = CColor.WHITE
    override val secondarySystemBackground: Int
        get() = CColor.grey(0.8)
    override val systemGroupedBackground: Int
        get() = TODO("Not yet implemented")
    override val secondarySystemGroupedBackground: Int
        get() = TODO("Not yet implemented")
    override val separator: Int
        get() = TODO("Not yet implemented")
    override val opaqueSeparator: Int
        get() = TODO("Not yet implemented")
    override val link: Int
        get() = TODO("Not yet implemented")
    override val systemGray2: Int
        get() = TODO("Not yet implemented")
    override val systemGray3: Int
        get() = TODO("Not yet implemented")
    override val systemGray4: Int
        get() = TODO("Not yet implemented")
    override val systemGray5: Int
        get() = TODO("Not yet implemented")
    override val systemGray6: Int
        get() = TODO("Not yet implemented")
}
