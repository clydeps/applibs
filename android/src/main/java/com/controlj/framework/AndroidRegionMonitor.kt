package com.controlj.framework

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import androidx.core.app.NotificationCompat
import com.controlj.location.Region
import com.controlj.location.RegionMonitor
import com.controlj.logging.CJLog.logException
import io.reactivex.rxjava3.core.Single
import org.altbeacon.beacon.BeaconConsumer
import org.altbeacon.beacon.BeaconManager
import org.altbeacon.beacon.BeaconParser
import org.altbeacon.beacon.Identifier
import org.altbeacon.beacon.MonitorNotifier
import java.util.concurrent.ConcurrentSkipListMap

class AndroidRegionMonitor(private val context: Context) : RegionMonitor {

    private val callbackMap = ConcurrentSkipListMap<String, (Region.Event) -> Unit>(String.CASE_INSENSITIVE_ORDER)
    private val appName by lazy {
        context.packageManager.getApplicationLabel(context.applicationInfo).toString()
    }
    private val manager by lazy {
        BeaconManager.getInstanceForApplication(context).also {
            it.beaconParsers.add(BeaconParser("iBeacon").setBeaconLayout(
                    "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"
            ))
            it.enableForegroundServiceScanning(getNotification(), NOTIFICATION_ID)
            it.addMonitorNotifier(object : MonitorNotifier {
                override fun didDetermineStateForRegion(state: Int, region: org.altbeacon.beacon.Region) {
                    onEvent(region.fromAltRegion(), state == MonitorNotifier.INSIDE)
                }

                override fun didEnterRegion(p0: org.altbeacon.beacon.Region?) {
                }

                override fun didExitRegion(p0: org.altbeacon.beacon.Region?) {
                }
            })
        }
    }

    private val notificationManager by lazy {
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    private val CHANNEL_ID by lazy { "$appName.beacons" }

    private val beaconManager: Single<BeaconManager> by lazy {
        Single.create<BeaconManager> { emitter ->
            manager.bind(object : BeaconConsumer {
                override fun getApplicationContext(): Context {
                    return context.applicationContext
                }

                override fun unbindService(conn: ServiceConnection) {
                    context.unbindService(conn)
                }

                override fun bindService(intent: Intent, conn: ServiceConnection, flags: Int): Boolean {
                    return context.bindService(intent, conn, flags)
                }

                override fun onBeaconServiceConnect() {
                    emitter.onSuccess(manager)
                }

            })
        }.cache()
    }

    override fun startRegionMonitor(region: Region, callback: (Region.Event) -> Unit) {
        if (region is Region.Beacon) {
            callbackMap[region.identifier] = callback
            beaconManager.subscribe(
                    {
                        val altRegion = region.toAltRegion()
                        it.startMonitoringBeaconsInRegion(altRegion)
                        it.requestStateForRegion(altRegion)
                    },
                    ::logException
            )
        }
    }

    override fun stopRegionMonitor(region: Region) {
        if (region is Region.Beacon) {
            callbackMap.remove(region.identifier)
            beaconManager.subscribe(
                    {
                        it.stopMonitoringBeaconsInRegion(region.toAltRegion())
                    },
                    ::logException
            )
        }
    }

    override fun clearAllRegions() {
        manager.monitoredRegions.forEach { manager.stopMonitoringBeaconsInRegion(it) }
    }

    fun onEvent(region: Region, state: Boolean) {
        callbackMap[region.identifier]?.invoke(Region.Event(region, state))
    }

    private val channelId: String by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "$appName-Beacons"
            val descriptionText = "$appName beacon scanning"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.description = descriptionText
            //val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
            //mChannel.setSound(Uri.parse("android.resource://$packageName/raw/${Priority.ACTION.sound}"), attributes)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(mChannel)
        }
        CHANNEL_ID
    }

    fun getNotification(): Notification {
        val builder = NotificationCompat.Builder(context, channelId)
                .setContentTitle("$appName is scanning")
                .setOnlyAlertOnce(true)
        return builder.build()
    }

    init {
        RegionMonitor.instance = this
    }

    companion object {
        const val NOTIFICATION_ID = 0x5CA77E
    }
}

fun Region.Beacon.toAltRegion(): org.altbeacon.beacon.Region {
    val fields = mutableListOf(Identifier.parse(uuid))
    major?.let {
        fields.add(Identifier.fromInt(it))
        minor?.let {
            fields.add(Identifier.fromInt(it))
        }
    }
    return org.altbeacon.beacon.Region(identifier, fields)
}

fun org.altbeacon.beacon.Region.fromAltRegion(): Region {
    return Region.Beacon(id1.toString(), id2?.toInt(), id3?.toInt())
}
