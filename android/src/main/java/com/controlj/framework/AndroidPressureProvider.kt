package com.controlj.framework

import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import com.controlj.pressure.PressureProvider
import com.controlj.pressure.PressureSource.DEVICE_PRIORITY
import com.controlj.rx.DisposedEmitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter

class AndroidPressureProvider(contextVar: Context) : PressureProvider {

    private val sensorManager: SensorManager = contextVar.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private var emitter: ObservableEmitter<Double> = DisposedEmitter()
    private val hasFeature = contextVar.packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_BAROMETER)

    private val pressureListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        }

        override fun onSensorChanged(event: SensorEvent) {
            emitter.onNext(event.values[0].toDouble())
        }
    }

    override val pressureObserver: Observable<Double> =
        Observable.create { e: ObservableEmitter<Double> ->
            emitter = e
            if (!hasFeature)
                e.onComplete()
            else {
                val pressureSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE)
                emitter.setCancellable { sensorManager.unregisterListener(pressureListener) }
                sensorManager.registerListener(pressureListener, pressureSensor, PRESSURE_RATE * 1000)
            }
        }
            .share()

    override val priority: Int = DEVICE_PRIORITY

    companion object {
        const val PRESSURE_RATE = 5000         // pressure update rate in ms
    }
}
