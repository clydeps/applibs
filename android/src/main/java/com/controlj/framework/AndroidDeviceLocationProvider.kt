package com.controlj.framework

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.GnssStatus
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.annotation.RequiresApi
import androidx.core.content.PermissionChecker
import com.controlj.backend.PermissionRequester
import com.controlj.data.Location
import com.controlj.location.GpsLocation
import com.controlj.location.LocationProvider
import com.controlj.nmea.GpsFix
import com.controlj.nmea.GpsStatus
import com.controlj.pressure.PressureSource
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import com.controlj.settings.BooleanSetting
import com.controlj.ui.UiAction
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Single
import org.threeten.bp.Instant

/**
 * An Android specific location provider
 */
@SuppressLint("MissingPermission")
class AndroidDeviceLocationProvider(val context: Context) : LocationProvider {

    private val sensorManager: SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager

    private val status = GpsStatus.Builder()

    override var isConnected: Boolean = false
        private set
    override var priority = Location.DEVICE_PRIORITY
    override var authorisationStatus: LocationProvider.AuthorisationStatus =
        LocationProvider.AuthorisationStatus.Unknown


    private val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private val locationListener = object : LocationListener {
        override fun onLocationChanged(location: android.location.Location) {
            isConnected = true
            locationEmitter.onNext(location.asLocation())
        }

        override fun onProviderEnabled(provider: String) {
        }

        override fun onProviderDisabled(provider: String) {
        }
    }

    private val statusListener by lazy {
        @RequiresApi(24)
        object : GnssStatus.Callback() {
            override fun onSatelliteStatusChanged(gnssStatus: GnssStatus) {
                status.satellitesInView = gnssStatus.satelliteCount
                status.satellitesUsed = (0 until status.satellitesInView).count { gnssStatus.usedInFix(it) }
            }
        }
    }

    @Suppress("DEPRECATION")
    private val legacyStatusListener by lazy {
        android.location.GpsStatus.Listener {
            locationManager.getGpsStatus(null)?.let { s ->
                status.satellitesInView = s.maxSatellites
                status.satellitesUsed = s.satellites.count { it.usedInFix() }
            }
        }
    }

    private var locationEmitter: ObservableEmitter<Location> = DisposedEmitter()
    override val name: String = "Android"

    private val pressureListener = object : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        }

        override fun onSensorChanged(event: SensorEvent) {
            status.baroAltitude = PressureSource.toAltitude(event.values[0].toDouble())
        }
    }

    @Suppress("DEPRECATION")
    override val locationObserver: Observable<Location> = Observable.create { emitter ->
        locationEmitter = emitter
        emitter.setCancellable {
            locationManager.removeUpdates(locationListener)
            if (Build.VERSION.SDK_INT >= 24)
                locationManager.unregisterGnssStatusCallback(statusListener)
            else
                locationManager.removeGpsStatusListener(legacyStatusListener)
            sensorManager.unregisterListener(pressureListener)
            isConnected = false
        }

        val provider: String = when {
            locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) -> LocationManager.GPS_PROVIDER
            locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) -> LocationManager.NETWORK_PROVIDER
            else -> {
                emitter.onError(IllegalStateException("Location not enabled"))
                return@create
            }
        }
        locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER)?.let {
            emitter.onNext(it.asLocation())
        }
        locationManager.requestLocationUpdates(
            provider, 1000L, 0f,
            locationListener, Looper.getMainLooper()
        )
        if (Build.VERSION.SDK_INT >= 24) {
            locationManager.registerGnssStatusCallback(statusListener, Handler(Looper.getMainLooper()))
        } else {
            // must run on ui thread
            MainScheduler().scheduleDirect {
                locationManager.addGpsStatusListener(legacyStatusListener)
            }
        }
        if (context.packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_BAROMETER)) {
            val pressureSensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE)
            sensorManager.registerListener(pressureListener, pressureSensor, 5 * 1000000)
        }
    }

    private val permissionMap = listOf(
        LocationProvider.AuthorisationStatus.Unknown to null,
        LocationProvider.AuthorisationStatus.None to null,
        LocationProvider.AuthorisationStatus.WhileInUse to Manifest.permission.ACCESS_FINE_LOCATION,
        LocationProvider.AuthorisationStatus.Always to
            if (Build.VERSION.SDK_INT >= 29)
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            else
                Manifest.permission.ACCESS_FINE_LOCATION
    )

    class PermissionRequest(val rationale: String, val requiredStatus: LocationProvider.AuthorisationStatus) :
        UiAction.Result<LocationProvider.AuthorisationStatus>()

    override fun checkPermission(): Single<LocationProvider.AuthorisationStatus> {
        return Single.just(
            permissionMap.reversed().first { pair ->
                pair.second?.let {
                    PermissionChecker.checkSelfPermission(
                        context,
                        it
                    ) == PermissionChecker.PERMISSION_GRANTED
                } ?: true
            }.first.also { authorisationStatus = it }
        )
    }

    override val enabled = BooleanSetting("androidLocation", "Android", "Android device location", true)

    /**
     * Request device location permission.
     */

    override fun askPermission(
        rationale: String,
        requiredStatus: LocationProvider.AuthorisationStatus
    ): Single<LocationProvider.AuthorisationStatus> {
        return checkPermission().flatMap {
            if (it >= requiredStatus)
                Single.just(it)
            else
                Single.create(PermissionRequest(rationale, requiredStatus))
        }
    }


    init {
        LocationProvider.deviceProvider = this
    }

    companion object {
        fun requestLocationPermission(
            activity: Activity,
            rationale: String,
            requiredStatus: LocationProvider.AuthorisationStatus
        ): Single<LocationProvider.AuthorisationStatus> {
            val permissions = mutableListOf(Manifest.permission.ACCESS_FINE_LOCATION)
            when {
                requiredStatus < LocationProvider.AuthorisationStatus.WhileInUse -> {
                    return Single.just(LocationProvider.AuthorisationStatus.None)
                }

                requiredStatus == LocationProvider.AuthorisationStatus.Always &&
                    Build.VERSION.SDK_INT >= 29 -> {
                    permissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                }
            }
            return PermissionRequester.requestPermission(
                activity,
                rationale,
                *permissions.toTypedArray()
            )
                .map {
                    when {
                        it.contains(Manifest.permission.ACCESS_BACKGROUND_LOCATION) -> LocationProvider.AuthorisationStatus.Always
                        it.contains(Manifest.permission.ACCESS_FINE_LOCATION) -> LocationProvider.AuthorisationStatus.WhileInUse
                        else -> LocationProvider.AuthorisationStatus.None
                    }
                }
        }
    }

    private val androidCreator = Location.Creator("Android", Location.DEVICE_PRIORITY)

    fun android.location.Location.asLocation(): Location {
        if (hasAccuracy()) {
            val hasVAcc = if (Build.VERSION.SDK_INT >= 26) hasVerticalAccuracy() else true
            val loc = Location.of(
                latitude,
                longitude,
                altitude,
                speed.toDouble(),
                bearing.toDouble(),
                timestamp = Instant.ofEpochMilli(time),
                creator = androidCreator,
                accuracyH = accuracy.toDouble(),
                accuracyV = if (Build.VERSION.SDK_INT >= 26)
                    verticalAccuracyMeters.toDouble()
                else
                    accuracy.toDouble() * 2.0
            )
            status.geoAltitude = altitude
            status.gpsFix = when {
                hasAccuracy() && hasVAcc -> GpsFix.ThreeD
                hasAccuracy() -> GpsFix.TwoD
                else -> GpsFix.None
            }
            return GpsLocation.of(loc, status.copy)
        }
        return Location.invalid
    }
}
