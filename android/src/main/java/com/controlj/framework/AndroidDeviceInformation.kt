package com.controlj.framework

import android.content.Context
import android.os.Build
import com.controlj.settings.DataStore
import java.util.UUID


@Suppress("DEPRECATION")
class AndroidDeviceInformation(application: Context) : DeviceInformation {

    override val appBuild: Int
    override val appVersion: String
    override val appName = application.javaClass.simpleName

    init {
        init()
        val info = application.packageManager.getPackageInfo(application.packageName, 0)
        appBuild = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P)
            info.versionCode else info.longVersionCode.toInt()
        appVersion = info.versionName
    }

    override val deviceName: String by lazy {
        DataStore().run {
            if (!contains(DEVICE_NAME_KEY))
                putString(DEVICE_NAME_KEY, UUID.randomUUID().toString().takeLast(8))
            getString(DEVICE_NAME_KEY)
        }
    }

    companion object {
        private const val DEVICE_NAME_KEY = "DeviceInformation.deviceName"
    }
}
