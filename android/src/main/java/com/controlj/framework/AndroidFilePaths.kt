package com.controlj.framework

import android.content.Context
import com.controlj.framework.FilePaths.Companion.init
import com.controlj.logging.CJLog.logMsg
import java.io.File
import java.io.IOException
import java.io.InputStream

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 7/11/19
 * Time: 16:24
 */

class AndroidFilePaths(val context: Context, val identifier: String) : FilePaths {

    override val databaseDirectory: File = context.getDatabasePath("dummy").parentFile!!.also { it.mkdirs() }

    override val tempDirectory: File = context.cacheDir.also { it.mkdirs() }

    override val dataDirectory: File = File(context.filesDir, identifier).also { it.mkdirs() }

    override fun listAssets(path: String): List<String> {
        return context.assets.list(path)?.toList() ?: listOf()
    }

    override fun openAsset(name: String): InputStream? {
        try {
            return context.assets.open(name)
        } catch (ex: IOException) {
            logMsg(ex.toString())
            return null
        }
    }

    init {
        init(this)
    }
}
