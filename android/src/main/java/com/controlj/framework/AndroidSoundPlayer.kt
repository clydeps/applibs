package com.controlj.framework

import android.annotation.TargetApi
import android.content.Context
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.media.AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE
import android.media.AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK
import android.media.MediaPlayer
import android.os.Build
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.QUEUE_ADD
import android.speech.tts.UtteranceProgressListener
import androidx.core.content.ContextCompat.getSystemService
import com.controlj.logging.CJLog.logMsg

@Suppress("DEPRECATION")
@TargetApi(Build.VERSION_CODES.O)
object AndroidSoundPlayer : SoundPlayer {

    val focusRequest: AudioFocusRequest by lazy {
        AudioFocusRequest.Builder(AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE).run {
            setAudioAttributes(AudioAttributes.Builder().run {
                setUsage(AudioAttributes.USAGE_MEDIA)
                setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                build()
            })
            build()
        }
    }

    private val context by lazy {
        (FilePaths() as AndroidFilePaths).context
    }
    val audioManager by lazy {
        context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    }

    var prevVolume: Int? = null
    private val player = MediaPlayer()

    /**
     * Prepare the media player for use when required, cache the result.
     */

    override fun playSound(sound: String) {
        (FilePaths() as? AndroidFilePaths)?.run {
            player.apply {
                setAudioStreamType(AudioManager.STREAM_MUSIC)
                setOnPreparedListener {
                    start()
                }
                setOnCompletionListener {
                    reset()
                    SoundPlayer.endSound()
                }
                setOnErrorListener { _: MediaPlayer?, what: Int, extra: Int ->
                    logMsg("Player error: $what/$extra")
                    SoundPlayer.endSound()
                    false
                }
                reset()
                val afd = context.assets.openFd("$sound.mp3")
                setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
                prepareAsync()
                afd.close()
            }
        }
    }

    lateinit var tts: TextToSpeech

    private class Speaker(val text: String) : TextToSpeech.OnInitListener {

        override fun onInit(status: Int) {
            tts.voice = tts.voices.filterNot { it.isNetworkConnectionRequired }
                .firstOrNull { it.name.startsWith(SoundPlayer.language, true) }
                ?: tts.voices.first()
            tts.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onStart(utteranceId: String?) = Unit

                override fun onDone(utteranceId: String?) {
                    SoundPlayer.endSound()
                }

                override fun onError(utteranceId: String?) {
                    logMsg("Speech failed")
                    SoundPlayer.endSound()
                }
            })
            tts.speak(text, QUEUE_ADD, null, text)
        }
    }

    override fun speak(text: String) {
        if (this::tts.isInitialized)
            tts.speak(text, QUEUE_ADD, null, text)
        else
            tts = TextToSpeech(context, Speaker(text))
    }

    override fun startSession() {
        prevVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
        val callBack = { _: Int ->
            if (SoundPlayer.maxVolume)
                audioManager.setStreamVolume(
                    AudioManager.STREAM_MUSIC,
                    audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                    0
                )
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            audioManager.requestAudioFocus(focusRequest)
            callBack(0)
        } else {
            audioManager.requestAudioFocus(
                callBack,
                AudioManager.STREAM_MUSIC,
                AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK
            )
        }
    }

    override fun endSession() {
        prevVolume?.let {
            try {
                audioManager.setStreamVolume(
                    AudioManager.STREAM_MUSIC,
                    it,
                    0
                )
            } catch (ex: Exception) {
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            audioManager.abandonAudioFocusRequest(focusRequest)
        else
            audioManager.abandonAudioFocus { }
    }

    init {
        SoundPlayer.instance = this
    }
}

