package com.controlj.logging

import android.content.Context
import android.os.Build
import java.util.Date

/**
 * Created by clyde on 16/4/18.
 */
object AndroidCrashLogger : CrashLogger {
    fun init(context: Context) {
        initialise(mapOf(
                "USER_CRASH_DATE" to Date().toString(),
                "INITIAL_CONFIGURATION" to context.resources.configuration.toString(),
                "DEVICE" to Build.DEVICE,
                "BRAND" to Build.BRAND,
                "MODEL" to Build.MODEL,
                "SDK_INT" to Build.VERSION.SDK_INT.toString(),
        ))
    }
}
