package com.controlj.graphics

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import android.text.TextPaint
import com.controlj.Utilities.R
import com.controlj.logging.CJLog.logException
import java.io.FileNotFoundException
import java.util.Locale

@SuppressLint("StaticFieldLeak")
/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 27/1/17
 * Time: 8:23 AM
 */
class AndroidGraphicsFactory(val context: Context) : GraphicsFactory {

    override val screenBounds: CRect
        get() {
            val metrics = Resources.getSystem().displayMetrics
            return CRect(0.0, 0.0, metrics.widthPixels.toDouble(), metrics.heightPixels.toDouble())
        }

    private val density: Float
    private val packageName: String by lazy { context.packageName }
    private val res: Resources by lazy { context.resources }

    init {
        init()
        val dm = Resources.getSystem().displayMetrics
        density = if (dm.heightPixels < dm.widthPixels)
            dm.heightPixels / 768f
        else
            dm.widthPixels / 768f

    }

    override fun createPath(): Path {
        return AndroidPath()
    }

    override fun getImageBuffer(width: Int, height: Int): GraphicsContext {
        val canvasWrapper = CanvasWrapper()
        val bm = android.graphics.Bitmap.createBitmap(width, height, android.graphics.Bitmap.Config.ARGB_8888)
        canvasWrapper.setBitmap(bm)
        canvasWrapper.setCanvas(Canvas(bm))
        return canvasWrapper
    }

    override fun getTextSize(text: String, style: TextStyle): CSize {
        val textStyle = AndroidTextStyle.getTextStyle(style)
        val paint = TextPaint()
        paint.typeface = textStyle.getTypeFace()
        paint.textSize = textStyle.pixels.toFloat()
        return CSize(paint.measureText(text).toDouble(), (paint.descent() - paint.ascent()).toDouble())

    }
    override fun getTextBounds(text: String, style: TextStyle): CRect {
        return CRect(CPoint(), getTextSize(text, style))
    }

    override fun pointsToPixels(points: Double): Int {
        return Math.ceil((points * density)).toInt()
    }

    override val textFactor: Double = 0.7

    override fun loadImage(filename: String, isTemplate: Boolean): CImage {
        var resID = res.getIdentifier(filename.lowercase(), "drawable", packageName)
        if (resID <= 0) {
            logException(FileNotFoundException("No resource named $filename"))
            resID = R.drawable.dot_small
        }
        val bitmap = BitmapFactory.decodeResource(res, resID)
        if (bitmap != null)
            return BitmapWrapper(BitmapFactory.decodeResource(res, resID))
        val drawable = DrawableCompat.wrap(ContextCompat.getDrawable(context, resID)!!)
        val drawnBitmap = Bitmap.createBitmap(drawable.intrinsicWidth,
                drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(drawnBitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return BitmapWrapper(drawnBitmap)

    }
}
