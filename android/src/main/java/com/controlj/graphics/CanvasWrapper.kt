package com.controlj.graphics

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 26/1/17
 * Time: 12:34 PM
 */
class CanvasWrapper : GraphicsContext {
    private var canvas: Canvas? = null

    var androidBitmap: android.graphics.Bitmap? = null
        private set
    private val fillPaint = Paint()
    private val strokePaint = Paint()
    private val textPaint = Paint()
    private val rectF = RectF()
    private val fm = Paint.FontMetrics()

    fun setBitmap(bitmap: android.graphics.Bitmap) {
        this.androidBitmap = bitmap
    }

    init {
        fillPaint.style = Paint.Style.FILL
        fillPaint.color = CColor.WHITE
        //fillPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        strokePaint.style = Paint.Style.STROKE
        textPaint.style = Paint.Style.FILL
        strokePaint.isAntiAlias = true
        fillPaint.isAntiAlias = true
        textPaint.isAntiAlias = true
    }

    fun setCanvas(canvas: Canvas) {
        this.canvas = canvas
    }

    override fun drawImage(image: CImage, bounds: CRect, tintColor: Int?) {
        if (image is BitmapWrapper) {
            rectF.set(bounds.left.toFloat(), bounds.top.toFloat(), bounds.right.toFloat(), bounds.bottom.toFloat())
            val bm = image.bitmap
            fillPaint.color = CColor.BLACK        // without this, it just doesn't work. Dunno why.
            canvas?.drawBitmap(bm, null, rectF, fillPaint)
        }
    }

    override fun strokePath(path: Path) {
        canvas?.drawPath(path as android.graphics.Path, strokePaint)
    }

    override val bitmap: CImage
        get() = BitmapWrapper(androidBitmap)
    override var fillColor: Int
        get() = fillPaint.color
        set(value) {
            fillPaint.color = value
        }
    override var strokeColor: Int
        get() = strokePaint.color
        set(value) {
            strokePaint.color = value
        }
    override var lineWidth: Double
        get() = strokePaint.strokeWidth.toDouble()
        set(value) {
            strokePaint.strokeWidth = value.toFloat()
        }
    override var lineCap: GraphicsContext.LineCap = GraphicsContext.LineCap.butt
        set(value) {
            field = value
            when (value) {
                GraphicsContext.LineCap.round -> strokePaint.strokeCap = Paint.Cap.ROUND
                GraphicsContext.LineCap.square -> strokePaint.strokeCap = Paint.Cap.SQUARE
                else -> strokePaint.strokeCap = Paint.Cap.BUTT
            }
        }


    override fun fillRect(bounds: CRect) {
        canvas?.drawRect(bounds.left.toFloat(), bounds.top.toFloat(), bounds.right.toFloat(), bounds.bottom.toFloat(), fillPaint)
    }

    override fun strokeRect(bounds: CRect) {
        canvas?.drawRect(bounds.left.toFloat(), bounds.top.toFloat(), bounds.right.toFloat(), bounds.bottom.toFloat(), strokePaint)
    }

    override fun saveState() {
        canvas?.save()
    }

    override fun restoreState() {
        canvas?.restore()
    }

    override fun drawArc(x: Double, y: Double, radius: Double, startAngle: Double, endAngle: Double) {
        rectF.set((x - radius).toFloat(), (y - radius).toFloat(), (x + radius).toFloat(), (y + radius).toFloat())
        canvas?.drawArc(rectF, startAngle.toFloat(), (endAngle - startAngle).toFloat(), false, strokePaint)
    }

    override fun drawArc(center: Point, radius: Double, startAngle: Double, endAngle: Double) {
        drawArc(center.getX().toDouble(), center.getY().toDouble(), radius, startAngle, endAngle)
    }

    override fun rotateBy(degrees: Double) {
        canvas?.rotate(degrees.toFloat())
    }

    override fun drawLine(x1: Double, y1: Double, x2: Double, y2: Double) {
        canvas?.drawLine(x1.toFloat(), y1.toFloat(), x2.toFloat(), y2.toFloat(), strokePaint)
    }

    override fun drawText(text: String, bounds: CRect, style: TextStyle, color: Int) {
        val x: Double
        textPaint.color = color
        when (style.alignment) {
            TextStyle.Alignment.Center -> {
                textPaint.textAlign = Paint.Align.CENTER
                x = (bounds.left + bounds.right) / 2
            }
            TextStyle.Alignment.Right -> {
                textPaint.textAlign = Paint.Align.RIGHT
                x = bounds.right
            }
            else -> {
                textPaint.textAlign = Paint.Align.LEFT
                x = bounds.left
            }
        }
        textPaint.typeface = AndroidTextStyle.getTextStyle(style).getTypeFace()
        textPaint.textSize = style.size.toFloat()
        textPaint.getFontMetrics(fm)
        canvas?.drawText(text, x.toFloat(), (bounds.top + bounds.bottom).toFloat() / 2 - (fm.bottom + fm.top) / 2, textPaint)
    }

    override fun clip(bounds: CRect) {
        canvas?.clipRect(bounds.left.toFloat(), bounds.top.toFloat(), bounds.right.toFloat(), bounds.bottom.toFloat())
    }

    override fun fillEllipse(bounds: CRect) {
        rectF.set(bounds.left.toFloat(), bounds.top.toFloat(), bounds.right.toFloat(), bounds.bottom.toFloat())
        canvas?.drawOval(rectF, fillPaint)
    }

    override fun scaleBy(x: Double, y: Double) {
        canvas?.scale(x.toFloat(), y.toFloat())
    }

    override fun translateBy(x: Double, y: Double) {
        canvas?.translate(x.toFloat(), y.toFloat())
    }

    override fun fillPath(path: Path) {
        canvas?.drawPath(path as android.graphics.Path, fillPaint)
    }
}

