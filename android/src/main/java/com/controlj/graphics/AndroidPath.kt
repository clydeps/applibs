package com.controlj.graphics

import android.graphics.RectF

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 27/1/17
 * Time: 8:27 AM
 */
class AndroidPath : android.graphics.Path(), Path {
    override fun clear() {
        rewind()
    }

    override fun addRoundedRect(bounds: CRect, xRadius: Double, yRadius: Double) {
        val rectF = RectF(bounds.left.toFloat(), bounds.top.toFloat(), bounds.right.toFloat(), bounds.bottom.toFloat())
        addRoundRect(rectF, xRadius.toFloat(), yRadius.toFloat(), android.graphics.Path.Direction.CW)
    }

    override fun addArc(x: Double, y: Double, radius: Double, startAngle: Double, endAngle: Double) {
        arcTo((x - radius).toFloat(), (y - radius).toFloat(), (x + radius).toFloat(), (y + radius).toFloat(), startAngle.toFloat(), endAngle.toFloat(), false)
    }

    override fun moveTo(x: Double, y: Double) {
        moveTo(x.toFloat(), y.toFloat())
    }

    override fun lineTo(x: Double, y: Double) {
        lineTo(x.toFloat(), y.toFloat())
    }

    override fun closePath() {
        close()
    }
}
