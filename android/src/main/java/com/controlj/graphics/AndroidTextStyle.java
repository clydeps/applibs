package com.controlj.graphics;

import android.graphics.Typeface;
import android.util.SparseArray;

import java.util.HashMap;

/**
 * Created by clyde on 11/5/17.
 */

public class AndroidTextStyle {
    private final int pixels;
    protected Typeface typeFace;
    final protected TextStyle textStyle;
    final static private SparseArray<Typeface> typeFaces = new SparseArray<>();
    final static private HashMap<TextStyle, AndroidTextStyle> styleMap = new HashMap<>();

    protected AndroidTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
        pixels = (int)(textStyle.getSize() + 0.5f);
    }

    static public AndroidTextStyle getTextStyle(TextStyle style) {
        if(!styleMap.containsKey(style)) {
            styleMap.put(style, new AndroidTextStyle(style));
        }
        return styleMap.get(style);
    }

    /**
     * Get a typeface from the supplied style. Cache typefaces and return cached versions
     * where possible.
     *
     * @return a Typeface corresponding to the supplied style
     */
    public Typeface getTypeFace() {
        Typeface typeface = typeFaces.get(textStyle.getStyleBits());
        if(typeface != null)
            return typeface;
        switch(textStyle.getFontFamily()) {
            default:
                typeface = Typeface.DEFAULT;
                break;

            case Monospaced:
                typeface = Typeface.MONOSPACE;
                break;

            case SansSerif:
                typeface = Typeface.SANS_SERIF;
                break;

            case Serif:
                typeface = Typeface.SERIF;
                break;
        }
        int styleBits = 0;
        switch(textStyle.getFontStyle()) {
            case BoldItalic:
                styleBits |= Typeface.BOLD_ITALIC;
                break;
            case Italic:
                styleBits |= Typeface.ITALIC;
                break;
            case Bold:
                styleBits |= Typeface.BOLD;
                break;
        }
        typeface = Typeface.create(typeface, styleBits);
        typeFaces.put(textStyle.getStyleBits(), typeface);
        return typeface;
    }

    public int getPixels() {
        return pixels;
    }
}
