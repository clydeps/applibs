package com.controlj.graphics;


/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 * <p>
 * User: clyde
 * Date: 27/1/17
 * Time: 11:16 AM
 */
public class BitmapWrapper implements CImage {
	private android.graphics.Bitmap bitmap;

	public android.graphics.Bitmap getBitmap() {
		return bitmap;
	}

	public BitmapWrapper(android.graphics.Bitmap bitmap) {
		this.bitmap = bitmap;
	}
}
