package com.controlj.content

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.controlj.Utilities.R
import com.controlj.framework.DeviceInformation
import com.controlj.logging.CJLog.logException
import com.controlj.settings.DataStore
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

/**
 * Created with IntelliJ IDEA.
 * User: clyde
 * Date: 18/09/12
 * Time: 18:24
 * To change this template use File | Settings | File Templates.
 */
@Suppress("DEPRECATION")
class About : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var versionName = "1.0.0"
        setContentView(R.layout.about)
        val iv = findViewById<ImageView>(R.id.icon)
        var view = findViewById<TextView>(R.id.about_author)
        @Suppress("DEPRECATION")
        view.text = Html.fromHtml(resources.getString(R.string.about_author))
        view = findViewById(R.id.about_appname)
        view.text = getString(R.string.app_name)
        view = findViewById(R.id.about_version)
        val credits = findViewById<Button>(R.id.credits)
        try {
            val info = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                packageManager.getPackageInfo(packageName, PackageManager.PackageInfoFlags.of(0L))
            else
                packageManager.getPackageInfo(packageName, 0)
            versionName = info.versionName
            view.text = "Version $versionName"
            iv.setImageDrawable(packageManager.getApplicationIcon(applicationInfo))
            DataStore().putInt(
                getString(R.string.current_version_number),
                DeviceInformation().appBuild
            )
        } catch (ex: PackageManager.NameNotFoundException) {
            logException(ex)
        }
        try {
            credits.visibility = View.VISIBLE
            assets.open("relnotes/$versionName.html").use {
                findViewById<WebView>(R.id.about_relnotes).loadData(
                    convertStreamToString(it),
                    "text/html; charset=UTF-8",
                    null
                )
            }
        } catch (ex: IOException) {
            logException(ex)
        }
    }

    fun onOK(@Suppress("UNUSED_PARAMETER") ignored: View?) {
        onBackPressed()
    }

    fun onCredits(@Suppress("UNUSED_PARAMETER") v: View?) {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Credits")
        try {
            val wv = WebView(this)
            assets.open("credits.html").use {
                wv.loadData(convertStreamToString(it), "text/html", "UTF-8")
            }
            wv.webViewClient = object : WebViewClient() {
                @Deprecated("Deprecated in Java")
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    view.loadUrl(url)
                    return true
                }
                override fun shouldOverrideUrlLoading(view: WebView, url: WebResourceRequest): Boolean {
                    view.loadUrl(url.url.toString())
                    return true
                }
            }
            alert.setView(wv)
            alert.setNegativeButton("Close") { dialog, _ -> dialog.dismiss() }
            alert.show()
        } catch (ex: IOException) {
            logException(ex)
        }
    }

    companion object {
        operator fun invoke(context: Context) {
            context.startActivity(Intent(context, About::class.java))
        }

        @Throws(IOException::class)
        fun convertStreamToString(`is`: InputStream?): String {
            val reader = BufferedReader(InputStreamReader(`is`))
            val sb = StringBuilder()
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
            reader.close()
            return sb.toString()
        }
    }
}
