package com.controlj.content;

import java.util.List;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 26/05/15
 * Time: 10:52 AM
 */
public interface ImageGallery {
	String getAbstract();
	String getHeadline();
	List<GalleryImage> getImages();

}
