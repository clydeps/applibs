package com.controlj.content;

import android.content.Context;

import java.io.IOException;
import java.util.*;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 6/05/2015
 * Time: 8:43 AM
 */
public abstract class Publications {
	public static final String PUBLICATIONS = "publications";
	public static final String PUBLICATION_KEY = "publication.key";
	final Context context;
	public Publications(Context context) {
		this.context = context;
	}

	public abstract boolean getLatest() throws IOException;

	public abstract void cleanup();

}
