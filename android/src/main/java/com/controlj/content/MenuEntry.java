package com.controlj.content;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.List;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 17/04/2015
 * Time: 9:35 AM
 */
public class MenuEntry {
	private String text = null;
	private String code = null;
	protected Drawable image = null;
	protected List<MenuEntry> children;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public MenuEntry(String text) {
		this.text = text;
	}

	public List<MenuEntry> getChildren() {
		return children;
	}

	public void add(MenuEntry me) {
		children.add(me);
	}

	public MenuEntry get(int i) {
		return children.get(i);
	}

	public int size() {
		if(isEmpty())
			return 0;
		return children.size();
	}

	public boolean contains(String s) {
		if(isEmpty())
			return false;
		for(MenuEntry me : children) {
			if(me.getCode() != null && me.getCode().equals(s))
				return true;
		}
		return false;
	}

	public boolean isEmpty() {
		return children == null || children.isEmpty();
	}

	public Drawable getImage() {
		return image;
	}

	public String getText() {
		return text;
	}

	public MenuEntry(String text, Drawable image) {

		this.text = text;
		this.image = image;
	}

	// custom run for this menu entry. Return false if none
	public boolean invoke(Context context) {
		return false;
	}

	public void setChildren(List<MenuEntry> children) {
		this.children = children;
	}
}
