package com.controlj.content;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 26/05/15
 * Time: 10:48 AM
 */
public interface GalleryImage {
	String getUrl();
	String getCaption();
}
