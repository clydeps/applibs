package com.controlj.content;

import java.util.ArrayList;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 17/04/2015
 * Time: 9:40 AM
 */
public class DrawerContent extends ArrayList<MenuEntry> {
	private String header;

	public DrawerContent(String header) {
		this.header = header;
	}

	public String getHeader() {

		return header;
	}
}
