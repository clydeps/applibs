package com.controlj.appframework

import android.app.Activity
import android.content.Context
import android.net.http.HttpResponseCache
import android.os.Build
import android.os.Bundle
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.controlj.backend.AndroidDataStore
import com.controlj.backend.LogCatLogger
import com.controlj.framework.AndroidDeviceInformation
import com.controlj.framework.AndroidFilePaths
import com.controlj.framework.AndroidSystemColors
import com.controlj.framework.ApplicationState
import com.controlj.framework.DeviceInformation
import com.controlj.framework.FilePaths
import com.controlj.logging.AndroidCrashLogger
import com.controlj.logging.CJLog
import com.controlj.logging.FileLogger
import com.controlj.logging.HttpLogger
import com.controlj.ui.ViewModelStore
import java.io.File
import java.io.IOException

/**
 * Base application providing various facilities
 */

abstract class BaseApplication : MultiDexApplication() {

    /**
     * Should be implemented by concrete classes. The app may be a debug build but this library not.
     */
    abstract val isDebug: Boolean

    /**
     * A logger destination overrideable by subclasses. Used only in debug builds
     */

    protected open val loggerHostName = HttpLogger.SYSLOGGER

    /**
     * An identifier for the app, used to construct filenames etc.
     */

    abstract val identifier: String

    init {
        context = this
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    @Suppress("DEPRECATION")
    override fun onCreate() {
        super.onCreate()
        AndroidSystemColors.init()
        AndroidDataStore(this)
        AndroidDeviceInformation(this)
        AndroidFilePaths(this, identifier)
        CJLog.deviceId = "${DeviceInformation().appName}-${Build.DEVICE}"
        CJLog.isDebug = isDebug
        AndroidCrashLogger.init(this)
        Thread.setDefaultUncaughtExceptionHandler(AndroidCrashLogger)
        CJLog.versionString = DeviceInformation().appVersion
        CJLog.buildNumber = DeviceInformation().appBuild
        CJLog.packageName = DeviceInformation().appName
        ViewModelStore.setup(FilePaths().dataDirectory)

        val logfile = File(File(filesDir, "logs"), this.javaClass.simpleName + ".log")
        CJLog.add(FileLogger(logfile))
        if (isDebug) {
            CJLog.add(LogCatLogger())
        }
        AndroidBackgroundServiceProvider(this)

        if (isDebug)
            CJLog.add(HttpLogger(postUrl = loggerHostName))
        try {
            val httpCacheDir = File(cacheDir, "http")
            val httpCacheSize = (15 * 1024 * 1024).toLong() // 15 MiB
            HttpResponseCache.install(httpCacheDir, httpCacheSize)
        } catch (e: IOException) {
            CJLog.logException(e)
        }
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityPaused(activity: Activity) {
                ApplicationState.current.data = ApplicationState.Visible
            }

            override fun onActivityResumed(activity: Activity) {
                ApplicationState.current.data = ApplicationState.Active
            }

            override fun onActivityStarted(activity: Activity) {
                ApplicationState.current.data = ApplicationState.Visible
            }

            override fun onActivityStopped(activity: Activity) {
                ApplicationState.current.data = ApplicationState.Background
            }

            override fun onActivityDestroyed(activity: Activity) {
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
            }

            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            }
        })
        ApplicationState.current.data = ApplicationState.Background
    }

    companion object {

        lateinit var context: BaseApplication
            protected set
    }
}
