package com.controlj.appframework

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.TRANSPORT_CELLULAR
import android.net.NetworkCapabilities.TRANSPORT_ETHERNET
import android.net.NetworkCapabilities.TRANSPORT_WIFI
import android.os.BatteryManager
import android.os.Build
import androidx.work.Constraints
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.ListenableWorker
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.controlj.data.Progress
import com.controlj.event.EventBus
import com.controlj.framework.BackgroundServiceProvider
import com.controlj.framework.ProcessWorker
import com.controlj.framework.TaskStatus
import com.controlj.location.LocationProvider
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.observeBy
import com.controlj.utility.FormRequest
import com.controlj.view.AppPermission
import com.google.common.util.concurrent.ListenableFuture
import com.google.common.util.concurrent.SettableFuture
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Single
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.io.File
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class AndroidBackgroundServiceProvider(val context: Context) : BackgroundServiceProvider {

    companion object {
        const val URL_KEY = "url"
        const val HEADERS_KEY = "headers"
        const val METHOD_KEY = "method"
        const val FILE_KEY = "file"
        const val IDENTIFIER_KEY = "identifier"
        const val CONTENT_TYPE_KEY = "content-type"
        const val WORKER_CLASS_KEY = "workerClassKey"
        const val WORKER_ID_KEY = "workerIdKey"
    }

    private val executor: Executor by lazy { Executors.newCachedThreadPool() }

    init {
        init()
    }

    override fun isPermitted(appPermission: AppPermission): Single<Boolean> {
        return when (appPermission) {
            AppPermission.BLUETOOTH -> Single.just(true)
            AppPermission.LOCATION_IN_USE -> LocationProvider.deviceProvider.checkPermission()
                .map { it >= LocationProvider.AuthorisationStatus.WhileInUse }

            AppPermission.LOCATION_ALWAYS -> LocationProvider.deviceProvider.checkPermission()
                .map { it >= LocationProvider.AuthorisationStatus.Always }
        }
    }

    internal class UploadWorker(context: Context, val params: WorkerParameters) : Worker(context, params) {
        override fun doWork(): Result {
            try {
                val data = inputData
                val identifier = data.getString(IDENTIFIER_KEY) ?: "upload"
                logMsg("trying connection for $identifier")
                val url = URL(params.inputData.getString(URL_KEY))
                val headers = data.getStringArray(HEADERS_KEY)
                val connection = url.openConnection() as HttpURLConnection
                headers?.map { it.split('=', limit = 2) }?.forEach {
                    connection.addRequestProperty(it.first(), it.last())
                }
                val file = File(data.getString(FILE_KEY) ?: error("No value for $FILE_KEY"))
                connection.requestMethod = inputData.getString(METHOD_KEY) ?: "POST"
                connection.doInput = true
                connection.doOutput = true
                val stream = file.inputStream()
                stream.copyTo(connection.outputStream)
                stream.close()
                file.delete()
                val code = connection.responseCode
                when (code) {
                    in (200..299) -> {
                        val message = connection.inputStream.bufferedReader().readText()
                        connection.disconnect()
                        EventBus.postEvent(BackgroundServiceProvider.Response(identifier, code), message)
                        logMsg("success $code for $identifier ")
                        return Result.success()
                    }

                    else -> {
                        val message = connection.errorStream.bufferedReader().readText()
                        connection.disconnect()
                        EventBus.postEvent(BackgroundServiceProvider.Response(identifier, code), message)
                        logMsg("Connection($identifier) failed with $code: $message")
                        return Result.failure()
                    }
                }
            } catch (ex: Exception) {
                logMsg("Uploadworker for $inputData threw an exception")
                logException(ex)
                return Result.failure()
            }
        }
    }

    override fun postRequest(
        postData: FormRequest,
        identifier: String,
        emitter: ObservableEmitter<Progress>?
    ) {
        val data = Data.Builder()
            .putString(METHOD_KEY, postData.method)
            .putString(URL_KEY, postData.url.toString())
            .putString(IDENTIFIER_KEY, identifier)
            .putString(FILE_KEY, postData.fileData().path)
            .putString(CONTENT_TYPE_KEY, postData.data.contentType)
            .putStringArray(HEADERS_KEY, postData.headers.map { it.key + "=" + it.value }.toTypedArray())
            .build()
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val request = OneTimeWorkRequest.Builder(UploadWorker::class.java)
            .addTag(identifier)
            .setConstraints(constraints)
            .setInputData(data)
            .build()
        WorkManager.getInstance(context).enqueueUniqueWork(identifier, ExistingWorkPolicy.REPLACE, request)
        emitter?.onComplete()
    }

    override fun getStatus(identifier: String): Single<TaskStatus> {
        return Single.create { emitter ->
            val future = WorkManager.getInstance(context).getWorkInfosByTag(identifier)
            future.addListener({
                val result = future.get()
                emitter.onSuccess(
                    if (result.isEmpty())
                        TaskStatus.Unknown
                    else when (result.first().state) {
                        WorkInfo.State.CANCELLED, WorkInfo.State.FAILED -> TaskStatus.Failed
                        WorkInfo.State.ENQUEUED,
                        WorkInfo.State.BLOCKED -> TaskStatus.Scheduled

                        WorkInfo.State.RUNNING -> TaskStatus.Running
                        WorkInfo.State.SUCCEEDED -> TaskStatus.Completed
                    }
                )
            }, executor)
        }
    }

    class AndroidWorker(appContext: Context, workerParameters: WorkerParameters) :
        ListenableWorker(appContext, workerParameters) {

        override fun startWork(): ListenableFuture<Result> {
            val future = SettableFuture.create<Result>()
            try {
                val action = inputData.getString(WORKER_CLASS_KEY) ?: return future.error("No class name provided")
                logMsg("Processing task $action started")
                ProcessWorker.runProcess(action)
                    .observeBy(onError = {
                        logMsg("Processing task $action failed: $it")
                        future.setException(it)
                    }) {
                        logMsg("Processing task $action completed")
                        future.set(if (it) Result.success() else Result.failure())
                    }
            } catch (ex: Exception) {
                future.setException(ex)
            }
            return future
        }

        fun SettableFuture<Result>.error(value: String, type: String = "Error"): SettableFuture<Result> {
            set(Result.failure(workDataOf(type to value)))
            return this
        }
    }

    override fun scheduleProcessingTask(
        action: String,
        atTime: Instant,
        requiresNetwork: Boolean,
        longRunning: Boolean
    ): Boolean {
        val constraints = Constraints.Builder().run {
            if (requiresNetwork)
                setRequiredNetworkType(NetworkType.CONNECTED)
            setRequiresStorageNotLow(true)
            setRequiresBatteryNotLow(true)
            build()
        }

        val timeDiff = Duration.between(Instant.now(), atTime).toMillis().coerceAtLeast(0)

        val inputData = Data.Builder().apply {
            putString(WORKER_CLASS_KEY, action)
        }.build()

        val worker = OneTimeWorkRequestBuilder<AndroidWorker>().run {
            setConstraints(constraints)
            setInputData(inputData)
            setInitialDelay(timeDiff, TimeUnit.MILLISECONDS)
            build()
        }
        // Queue worker
        WorkManager.getInstance(context).enqueueUniqueWork(
            action,
            ExistingWorkPolicy.REPLACE,
            worker
        )
        return true
    }


    override fun cancelProcessingTask(action: String) {
        WorkManager.getInstance(context).cancelUniqueWork(action)
    }

    override val hasExternalStorage: Boolean = false

    val networkManager: ConnectivityManager? by lazy { context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager? }

    override val networkObserver: Observable<Boolean> = Observable.interval(0, 60, TimeUnit.SECONDS).map {
        networkManager?.let { manager ->
            manager.activeNetwork?.let { manager.getNetworkCapabilities(it) }?.let { nwc ->
                nwc.hasTransport(TRANSPORT_WIFI) ||
                    nwc.hasTransport(TRANSPORT_CELLULAR) ||
                    nwc.hasTransport(TRANSPORT_ETHERNET)
            }
        } ?: false
    }

    val batteryManager: BatteryManager by lazy { context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager }
    override val batteryLevel: Double?
        get() {
            val percent = batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
            if (percent > 0)
                return percent.toDouble() / 100.0
            return null
        }

    override val debuggerConnected: Boolean
        get() = System.getProperty("init.svc.adbd") == "running"
}
