/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlj.appframework

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import com.controlj.Utilities.R
import com.controlj.view.KeyboardActivity
import java.util.ArrayList

/**
 * @author clyde
 */
abstract class Wizard : KeyboardActivity() {
    protected var views: MutableList<View> = ArrayList()
    protected var current: Int = 0

    // expected to be overridden
    protected open val isNextEnabled: Boolean
        get() = true

    protected open val isPrevEnabled: Boolean
        get() = current != 0

    open var title: String = ""
    open val showTitle: Boolean = false
    abstract val layoutRes: Int
    private lateinit var nextButton: Button
    private lateinit var prevButton: Button
    private lateinit var cancelButton: Button

    /**
     * Called when the activity is first created.
     */
    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        if (!showTitle)
            requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.wizard)
        findViewById<TextView>(R.id.wizard_title).text = title
        prevButton = findViewById(R.id.prev_button)
        nextButton = findViewById(R.id.next_button)
        cancelButton = findViewById(R.id.cancel_button)
        nextButton.setOnClickListener { onNext() }
        prevButton.setOnClickListener { onPrev() }
        cancelButton.setOnClickListener { onCancel() }
        val wizardContent: ViewGroup = findViewById(R.id.wizard_content)
        val inflater = LayoutInflater.from(this)
        val frameLayout = inflater.inflate(layoutRes, null) as FrameLayout
        val cnt = frameLayout.childCount
        for (i in 0 until cnt)
            views.add(frameLayout.getChildAt(i))
        for (v in views) {
            frameLayout.removeView(v)
            wizardContent.addView(v)
        }
        current = 0
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState.containsKey(CURRENT_PAGE)) {
            current = savedInstanceState.getInt(CURRENT_PAGE)
            selectView()
        }
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(CURRENT_PAGE, current)
    }

    /**
     * Called when the Finish button is clicked. Default run is to finish the activity.
     * Override this to perform other actions before finishing.
     */
    protected fun onFinish() {
        finish()
    }

    /**
     * Called when the Next button is clicked. Returns true if the new page is valid, otherwise calls onFinish and returns false.
     *
     * @return true if the page was changed
     */
    protected open fun onNext(): Boolean {
        if (current == views.size - 1) {
            onFinish()
            return false
        }
        current++
        selectView()
        return true
    }

    protected fun onCancel() {
        finish()
    }

    /**
     * Called when the Prev button is clicked.
     *
     * @return true if the page changed
     */
    protected fun onPrev(): Boolean {
        if (current != 0) {
            current--
            selectView()
            return true
        }
        return false
    }

    protected fun selectView() {
        for (i in views.indices)
            if (i == current)
                views[i].visibility = View.VISIBLE
            else
                views[i].visibility = View.GONE
        if (current == views.size - 1)
            nextButton.setText(R.string.finish)
        else
            nextButton.setText(R.string.next)
        prevButton.isEnabled = isPrevEnabled
        nextButton.isEnabled = isNextEnabled
    }

    companion object {

        private const val CURRENT_PAGE = "current_page"
    }
}
