package com.controlj.license;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 20/04/2015
 * Time: 5:10 PM
 */
public class LicensingStateException extends Exception {
	public LicensingStateException(String s) {
		super(s);
	}
}
