package com.controlj.view

import android.Manifest
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.controlj.Utilities.R
import com.controlj.backend.PermissionRequester
import com.controlj.ble.BleScanResult

/**
 * A fragment representing a list of Items.
 *
 */
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
open class BluetoothFragment : ListFragment<BleScanResult, BluetoothListPresenter>() {

    override val layoutResourceId = R.layout.fragment_bluetooth_list
    override val rowResource: Int = R.layout.bluetooth_list_entry

    override val presenter = BluetoothListPresenter()


    private val icons = mapOf(
        Pair(BleScanResult.signalBarNames[0], R.drawable.ic_signal_cellular_0_bar),
        Pair(BleScanResult.signalBarNames[1], R.drawable.ic_signal_cellular_1_bar),
        Pair(BleScanResult.signalBarNames[2], R.drawable.ic_signal_cellular_2_bar),
        Pair(BleScanResult.signalBarNames[3], R.drawable.ic_signal_cellular_3_bar),
        Pair(BleScanResult.signalBarNames[4], R.drawable.ic_signal_cellular_4_bar)
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        swipeRefreshLayout.setOnRefreshListener(presenter::startRefresh)
        toolBar.setNavigationOnClickListener { activity?.onBackPressed() }
    }

    override fun onStart() {
        PermissionRequester.requestPermission(
            requireActivity(), "Location is needed for Bluetooth LE scanning",
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        super.onStart()
    }

    override fun bindItem(view: View, item: BleScanResult, textValues: List<String>) {
        val deviceName = view.findViewById<TextView>(R.id.device_name)
        val deviceAddress = view.findViewById<TextView>(R.id.device_address)
        val signalText = view.findViewById<TextView>(R.id.signal_text)
        val signalIcon = view.findViewById<ImageView>(R.id.signal_icon)

        deviceName.text = textValues[BluetoothListPresenter.DEV_NAME]
        deviceAddress.text = textValues[BluetoothListPresenter.DEV_ADDRESS]
        signalText.text = textValues[BluetoothListPresenter.DEV_RSSI]
        val signal = icons[textValues[BluetoothListPresenter.DEV_BARS]]
        if (signal != null)
            signalIcon.setImageResource(signal)
        view.setOnClickListener { presenter.onItemSelected(item) }
    }

}


