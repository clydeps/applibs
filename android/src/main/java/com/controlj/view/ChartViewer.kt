package com.controlj.view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.controlj.Utilities.R
import com.controlj.logging.CJLog.logMsg
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 */
class ChartViewer : AppCompatActivity() {
    private val mHideHandler = Handler()
    private val mHidePart2Runnable = Runnable { this.hideSystemBar() }
    private var rotation = SubsamplingScaleImageView.ORIENTATION_0
    private var bitmap: Bitmap? = null

    private val mShowPart2Runnable = Runnable { fullscreenContentControls.visibility = View.VISIBLE }
    private var mVisible: Boolean = false
    private val mHideRunnable = Runnable { this.hide() }
    private lateinit var chartImage: SubsamplingScaleImageView
    private lateinit var fullscreenContentControls: View
    private lateinit var closeButton: View
    private lateinit var rotateLeft: View
    private lateinit var rotateRight: View

    private fun hideSystemBar() {
        findViewById<View>(R.id.chart_image).systemUiVisibility = (View.SYSTEM_UI_FLAG_LOW_PROFILE
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart_viewer)
        chartImage = findViewById<SubsamplingScaleImageView>(R.id.chart_image)
        fullscreenContentControls = findViewById(R.id.fullscreen_content_controls)
        rotateLeft = findViewById(R.id.rotate_left)
        rotateRight = findViewById(R.id.rotate_right)
        closeButton = findViewById(R.id.close_button)
        mVisible = true

        // Set up the user interaction to manually show or hide the system UI.
        chartImage.setOnClickListener { toggle() }

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        closeButton.setOnClickListener { finish() }
        rotateLeft.setOnClickListener {
            rotation -= 90
            doRotate()
        }
        rotateRight.setOnClickListener {
            rotation += 90
            doRotate()
        }
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
        //lockOrientation();
    }

    private fun doRotate() {
        rotation += 360
        rotation %= 360
        chartImage.orientation = rotation
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        hideSystemBar()
        show()
        Completable.timer(INITIAL_HIDE_DELAY_MILLIS.toLong(), TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe { this.hide() }
    }

    override fun onStart() {
        super.onStart()
        val intent = intent
        chartImage.maxScale = 4f
        if (intent.hasExtra(FILEPATH)) {
            val orientation = resources.configuration.orientation
            val filename = intent.getStringExtra(FILEPATH) ?: ""
            logMsg("Showing image $filename")
            Single.create { e: SingleEmitter<Int> ->
                val options = BitmapFactory.Options()
                options.inJustDecodeBounds = !filename.endsWith("gif")
                bitmap = BitmapFactory.decodeFile(filename, options)
                if (options.outHeight == 0)
                    e.onError(Throwable("Invalid image"))
                else {
                    val isLand = options.outWidth > options.outHeight
                    if (isLand != (orientation == Configuration.ORIENTATION_LANDSCAPE))
                        e.onSuccess(90)
                    else
                        e.onSuccess(0)
                }
            }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { rot ->
                                rotation = rot
                                doRotate()
                                if(bitmap != null)
                                    chartImage.setImage(ImageSource.bitmap(bitmap!!))
                                else
                                    chartImage.setImage(ImageSource.uri(filename))
                            },
                            { throwable -> Toast.makeText(this, "Failed to load image: " + throwable.message, Toast.LENGTH_LONG).show() })
        }
    }

    override fun onStop() {
        logMsg("onStop")
        super.onStop()
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        mHideHandler.removeCallbacks(mHideRunnable)
        mHideHandler.postDelayed(mHideRunnable, delayMillis.toLong())
    }

    private fun toggle() {
        if (mVisible) {
            hide()
        } else {
            show()
        }
    }

    private fun hide() {
        // Hide UI first
        fullscreenContentControls.visibility = View.GONE
        mVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable)
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    @SuppressLint("InlinedApi")
    private fun show() {
        // Show the system bar
        chartImage.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        mVisible = true

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable)
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [.AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private val AUTO_HIDE = true

        /**
         * If [.AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private val AUTO_HIDE_DELAY_MILLIS = 2000
        private val INITIAL_HIDE_DELAY_MILLIS = 1000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private val UI_ANIMATION_DELAY = 300
        private val FILEPATH = "filepath"
        private val MAX_IMG_DIM = 2000.0

        operator fun invoke(context: Context?, filename: String) {
            if(context == null)
                return
            val intent = Intent(context, ChartViewer::class.java)
            intent.putExtra(FILEPATH, filename)
            context.startActivity(intent)
        }
    }
}
