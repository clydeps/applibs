package com.controlj.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.inputmethodservice.Keyboard
import android.inputmethodservice.KeyboardView
import android.text.InputType
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.controlj.Utilities.R

/**
 * Created by clyde on 15/06/2016.
 */
@Suppress("Deprecation")
class CJKeyboardView : KeyboardView {
    private var caps = false
    private var allCaps = false
    private var currentEditText: EditText? = null

    constructor(context: Context, attrs: AttributeSet) : super(ContextWrapperInner(context), attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(ContextWrapperInner(context), attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    private fun init(attrs: AttributeSet, defStyleAttr: Int) {

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.CJKeyboardView, defStyleAttr, 0)
        val layout: Int
        try {
            layout = a.getResourceId(R.styleable.CJKeyboardView_layoutResource, R.xml.qwerty)
        } finally {
            a.recycle()
        }
        isPreviewEnabled = true
        onKeyboardActionListener = object : KeyboardView.OnKeyboardActionListener {
            override fun onPress(primaryCode: Int) {

            }

            override fun onRelease(primaryCode: Int) {

            }

            override fun onKey(primaryCode: Int, keyCodes: IntArray) {
                if (currentEditText != null) {
                    val event: KeyEvent
                    if (primaryCode == KeyEvent.KEYCODE_SHIFT_LEFT) {
                        caps = !caps || allCaps
                        isShifted = caps
                    } else {
                        if (primaryCode == COLON)
                            event = KeyEvent(System.currentTimeMillis(), System.currentTimeMillis(), KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_SEMICOLON, 0, KeyEvent.META_SHIFT_LEFT_ON)
                        else
                            event = KeyEvent(System.currentTimeMillis(), System.currentTimeMillis(), KeyEvent.ACTION_DOWN, primaryCode, 0, if (caps || allCaps) KeyEvent.META_CAPS_LOCK_ON else 0)
                        currentEditText!!.dispatchKeyEvent(event)
                        updateCaps()
                    }
                }
            }

            override fun onText(text: CharSequence) {

            }

            override fun swipeLeft() {

            }

            override fun swipeRight() {

            }

            override fun swipeDown() {

            }

            override fun swipeUp() {

            }
        }
        val keyboard = Keyboard(context, layout)
        setKeyboard(keyboard)
    }

    /**
     * Attach an this view to any focusable EditText elements in the view.
     * Return the desired visibility of this view when the enclosing view is on screen.
     */
    @SuppressLint("ClickableViewAccessibility")
    fun attach(activity: Activity, view: View) : Boolean {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        val focusListener = { v: View, hasFocus: Boolean ->
            if (hasFocus && v is EditText) {
                currentEditText = v
                updateCaps()
            } else
                currentEditText = null
        }
        val listener = { v: View, e: MotionEvent ->
            v.onTouchEvent(e)
            if (visibility == View.VISIBLE) {
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            } else {
                v.requestFocus()
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
                activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
            true
        }
        //imm.hideSoftInputFromInputMethod(windowToken, 0)
        activity.window.setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM,
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        val focusables = view.getFocusables(View.FOCUS_FORWARD)
        var vis = false
        for (v in focusables) {
            if (v is EditText) {
                vis = true
                val type = v.inputType
                v.inputType = 0
                v.setRawInputType(type)
                v.setTextIsSelectable(true)
                v.setOnFocusChangeListener(focusListener)
                v.setOnTouchListener(listener)
            }
        }
        updateCaps()
        return vis
    }

    private fun updateCaps() {
        if (currentEditText == null)
            caps = false
        else {
            val type = currentEditText!!.inputType
            allCaps = false
            caps = allCaps
            if (type and InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS != 0) {
                allCaps = true
                caps = allCaps
            } else if (type and InputType.TYPE_TEXT_FLAG_CAP_WORDS != 0) {
                val text = currentEditText!!.text
                if (text.length == 0)
                    caps = true
                else {
                    val c = text[text.length - 1]
                    caps = !Character.isJavaIdentifierPart(c)
                }
            }
        }
        isShifted = caps
    }

    class ContextWrapperInner(base: Context) : ContextWrapper(base) {

        internal var v: View
        fun setV(v: View) {
            this.v = v
        }

        init {
            v = View(base)
        }

        override fun getSystemService(name: String): Any? {
            return if (v.isInEditMode && Context.AUDIO_SERVICE == name) null else super.getSystemService(name)
        }
    }

    companion object {

        val COLON = -1
    }
}
