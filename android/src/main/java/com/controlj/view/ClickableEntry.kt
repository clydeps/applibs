package com.controlj.view

import android.content.Context
import com.controlj.graphics.CColor
import com.controlj.ui.ClickableDialogItem
import com.controlj.ui.DialogData

class ClickableEntry(
        context: Context,
        item: ClickableDialogItem,
        dialogData: DialogData
) : LabelledEntry(context, item, dialogData) {
    init {
        label.setTextColor(CColor.argb(255, 128, 255, 128))
        label.setOnClickListener { item.onChanged() }
    }


}
