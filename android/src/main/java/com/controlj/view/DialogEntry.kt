package com.controlj.view

import android.content.Context
import androidx.annotation.CallSuper
import android.view.LayoutInflater
import android.view.View
import com.controlj.ui.ButtonDialogItem
import com.controlj.ui.CViewItem
import com.controlj.ui.CheckboxItem
import com.controlj.ui.ClickableDialogItem
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.ui.GroupDialogItem
import com.controlj.ui.NumericDialogItem
import com.controlj.ui.ProgressDialogItem
import com.controlj.ui.RangeUnitDialogItem
import com.controlj.ui.SeparatorDialogItem
import com.controlj.ui.SpinnerDialogItem
import com.controlj.ui.StatusDialogItem
import com.controlj.ui.TextValueItem
import com.controlj.ui.ValueDialogItem

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-07-09
 * Time: 05:14
 */
abstract class DialogEntry(
        val context: Context,
        open val item: DialogItem,
        val dialogData: DialogData
) {

    abstract val view: View

    @CallSuper
    open fun refresh(): Boolean {
        if(view.isEnabled != item.isEnabled) {
            view.isEnabled = item.isEnabled
            view.invalidate()
            return true
        }
        return false
    }

    open fun onDestroy() {

    }

    val inflater: LayoutInflater by lazy { LayoutInflater.from(context) }

    companion object {
        var marginSize = 5
        fun create(context: Context, item: DialogItem, dialogData: DialogData): DialogEntry {
            when (item) {
                is RangeUnitDialogItem -> return RangeUnitEntry(context, item, dialogData)
                is CViewItem -> return CviewEntry(context, item, dialogData)
                is ProgressDialogItem -> return ProgressEntry(context, item, dialogData)
                is SeparatorDialogItem -> return SeparatorEntry(context, item, dialogData)
                is ClickableDialogItem -> return ClickableEntry(context, item, dialogData)
                is GroupDialogItem -> return GroupEntry(context, item, dialogData)
                is StatusDialogItem -> return StatusEntry(context, item, dialogData)
                is NumericDialogItem -> return NumericEntry(context, item, dialogData)
                is TextValueItem -> return TextValueEntry(context, item, dialogData)
                is ButtonDialogItem -> return ButtonEntry(context, item, dialogData)
                is SpinnerDialogItem<*> -> return SpinnerEntry(context, item, dialogData)
                is CheckboxItem -> return CheckboxEntry(context, item, dialogData)
                is ValueDialogItem -> return ValueEntry(context, item, dialogData)
                else -> {
                    if (item.style != DialogItem.Style.PLAIN)
                        error("Creating DialogEntry from unhandled style ${item.style}, class is ${item.javaClass.name}")
                    return LabelledEntry(context, item, dialogData)
                }
            }
        }
    }
}

