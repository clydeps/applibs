package com.controlj.view

import android.view.Choreographer
import com.controlj.animation.AnimationController
import com.controlj.framework.AndroidDeviceLocationProvider
import com.controlj.framework.ApplicationState
import com.controlj.logging.CJLog
import com.controlj.rx.observeBy
import com.controlj.ui.UiAction
import io.reactivex.rxjava3.disposables.CompositeDisposable

open class ForegroundActivity : KeyboardActivity() {

    protected open val useAnimation = false      // subclasses should override this if animation is desired
    protected val disposables = CompositeDisposable()
    protected lateinit var fgsp: AndroidForegroundServiceProvider

    override fun onStop() {
        fgsp.onDestroy()
        disposables.clear()
        super.onStop()
    }

    /**
     * Start animations. They will stop automagically when the activity is no longer visible
     */

    override fun onStart() {
        super.onStart()
        fgsp = AndroidForegroundServiceProvider(this)
        disposables.add(UiAction.observer.subscribe { action ->
            if (!fgsp.processAction(action))
                processAction(action)
        })
        if (useAnimation)
            Choreographer.getInstance().postFrameCallback(this)
    }

    override fun doFrame(l: Long) {
        if (ApplicationState.current.data.visible) {
            AnimationController.onFrame(l.toDouble() / 1000000000.0)
            Choreographer.getInstance().postFrameCallback(this)
        }
    }

    open fun processAction(action: UiAction) {
        when (action) {
            is AndroidDeviceLocationProvider.PermissionRequest -> {
                AndroidDeviceLocationProvider.requestLocationPermission(this, action.rationale, action.requiredStatus)
                    .observeBy(
                        { throwable -> action.fail(throwable) }
                    ) { status ->
                        CJLog.logMsg("Location permission $status")
                        action.succeed(status)
                    }
            }
            else -> CJLog.logMsg("Can't handle action $action")
        }
    }

}
