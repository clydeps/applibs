package com.controlj.view

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.View.TEXT_ALIGNMENT_VIEW_END
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.TextView
import com.controlj.ui.DialogData
import com.controlj.ui.SpinnerDialogItem

class SpinnerEntry(
        context: Context,
        override val item: SpinnerDialogItem<*>,
        dialogData: DialogData
) : LabelledEntry(context, item, dialogData) {

    var ddSelectedBackgroundColor = Color.rgb(204, 255, 255)
    val spinner = FilteredSpinner(context)
    val adapter = object : ArrayAdapter<Any>(
            context,
            android.R.layout.simple_spinner_item,
            item.choices.toMutableList()
    ) {
        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = super.getDropDownView(position, convertView, parent)
            (view as? TextView)?.textAlignment = TEXT_ALIGNMENT_VIEW_END
            if (position == spinner.selectedItemPosition)
                view.setBackgroundColor(ddSelectedBackgroundColor)
            return view
        }
    }

    init {
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f)
        spinner.adapter = adapter
        spinner.gravity = Gravity.END
        spinner.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                if (item.selected != position) {
                    item.selected = position
                    dialogData.refresh()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
        contentView.addView(spinner)
    }

    override fun refresh(): Boolean {
        var changed = adapter.count != item.choices.size
        if (!changed)
            for (i in 0 until item.choices.size) {
                if (adapter.getItem(i) != item.choices[i]) {
                    changed = true
                    break
                }
            }
        if (changed) {
            adapter.clear()
            adapter.addAll(*item.choices.toTypedArray())
        }
        if (spinner.selectedItemPosition != item.selected) {
            spinner.pgmSetSelection(item.selected)
            changed = true
        }
        return super.refresh() || changed
    }
}
