package com.controlj.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.controlj.Utilities.R
import io.reactivex.rxjava3.disposables.Disposable

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-07-09
 * Time: 11:25
 */
abstract class ListFragment<V : Comparable<V>, T : ListPresenter<V>> : KeyboardFragment() {
    open val headerResource = R.layout.list_header      // resource to load header view from
    open val rowResource = R.layout.list_entry


    protected var disposable: Disposable = Disposable.disposed()
    protected abstract val presenter: T
    protected val adapter: RecyclerView.Adapter<out RecyclerView.ViewHolder> = Adapter()
    protected lateinit var inflater: LayoutInflater
    protected open val layoutResourceId: Int = R.layout.fragment_list
    protected lateinit var recyclerView: RecyclerView
    protected lateinit var swipeRefreshLayout: SwipeRefreshLayout
    protected lateinit var toolBar: Toolbar
    protected lateinit var layoutManager: LinearLayoutManager

    protected inner class Adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return object : RecyclerView.ViewHolder(getRowLayout(parent, viewType)) {}
        }

        override fun getItemViewType(position: Int): Int {
            return presenter.viewType(presenter.fromGlobal(position)).ordinal
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val rowIndex = presenter.fromGlobal(position)
            val item = presenter.itemAt(rowIndex)
            if (item == null)
                bindHeader(holder.itemView, rowIndex.section, presenter.textValues(rowIndex.section))
            else
                bindItem(holder.itemView, item, presenter.textValues(item))
        }

        override fun getItemCount(): Int {
            return presenter.totalRows
        }
    }

    /**
     * Override this function to generate views for rows.
     */
    open fun getRowLayout(parent: ViewGroup, viewType: Int): View {
        return inflater.inflate(
            when (viewType) {
                ListPresenter.ViewType.HEADER.ordinal -> headerResource
                else -> rowResource
            }, parent, false
        )
    }

    /**
     * Bind an item to a view
     */
    open fun bindItem(view: View, item: V, textValues: List<String>) {
        if (textValues.isNotEmpty()) {
            view.findViewById<TextView>(R.id.text1)?.apply { text = textValues[0] }
            if (textValues.size > 1)
                view.findViewById<TextView>(R.id.text2)?.apply { text = textValues[1] }
        }
    }

    /**
     * Bind a header to a view
     */

    open fun bindHeader(view: View, section: Int, textValues: List<String>) {
        textValues.firstOrNull()?.let {
            view.findViewById<TextView>(R.id.headertext)?.text = it
            if (section == 0)
                toolBar.title = it
        }
    }

    /**
     * Handle custom events. Override to implement functionality.
     */
    open fun customEvent(event: ListPresenter.ChangeRange) {
        TODO()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.inflater = inflater
        val view = inflater.inflate(layoutResourceId, container, false)
        recyclerView = view.findViewById(R.id.list)
        view.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.default_background))
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        toolBar = view.findViewById(R.id.toolbar)
        recyclerView.adapter = adapter
        layoutManager = object : LinearLayoutManager(context) {
            override fun supportsPredictiveItemAnimations(): Boolean {
                return false
            }
        }
        recyclerView.layoutManager = layoutManager
        swipeRefreshLayout.setOnRefreshListener(presenter::startRefresh)
        toolBar.setNavigationOnClickListener { activity?.onBackPressed() }
        return view
    }

    protected open fun connectPresenter() {
        disposable.dispose()
        disposable = presenter.changeObserver
            .subscribe(this::onListEvent)
        adapter.notifyDataSetChanged()
        updateButtons()
        view?.invalidate()
    }

    override fun onStart() {
        super.onStart()
        connectPresenter()
    }

    @Suppress("UNCHECKED_CAST")
    protected open fun onListEvent(event: ListPresenter.ViewEvent?) {
        when (event) {
            is ListPresenter<*>.MenuEvent -> showMenu(event as ListPresenter<V>.MenuEvent)
            is ListPresenter.ChangeRange -> {
                when (event.reason) {
                    ListPresenter.ChangeReason.CHANGED -> rangeChanged(event.start, event.length)
                    ListPresenter.ChangeReason.ADDED -> rangeInserted(event.start, event.length)
                    ListPresenter.ChangeReason.REMOVED -> rangeRemoved(event.start, event.length)
                    ListPresenter.ChangeReason.BUSY -> onRefreshStart()
                    ListPresenter.ChangeReason.IDLE -> {
                        onRefreshComplete()
                        updateButtons()
                    }
                    ListPresenter.ChangeReason.STARTED -> Unit
                    ListPresenter.ChangeReason.ENDED -> updateButtons()
                    //ListViewModel.ChangeReason.MOVED -> rangeMoved(it.start, it.length)
                    ListPresenter.ChangeReason.REFRESH -> {
                        adapter.notifyDataSetChanged()
                        updateButtons()
                    }
                    ListPresenter.ChangeReason.CUSTOM -> customEvent(event)
                }
            }

        }
    }

    override fun onStop() {
        disposable.dispose()
        super.onStop()
    }

    private fun rangeInserted(start: Int, count: Int) {
        adapter.notifyItemRangeInserted(start, count)
    }

    private fun rangeRemoved(start: Int, count: Int) {
        adapter.notifyItemRangeRemoved(start, count)
    }

    private fun rangeChanged(start: Int, count: Int) {
        adapter.notifyItemRangeChanged(start, count)
        updateButtons()
    }

    private fun rangeMoved(from: Int, to: Int) {
        adapter.notifyItemMoved(from, to)
    }

    protected fun viewFor(item: V?): View? {
        return presenter.indexOf(item)?.let { index ->
            return recyclerView.findViewHolderForLayoutPosition(presenter.toGlobal(index))?.itemView
        }
    }

    override fun onResume() {
        super.onResume()
        updateButtons()
    }

    /**
     * Called when a refresh cycle is complee
     */
    open fun onRefreshComplete() {
        swipeRefreshLayout.isRefreshing = false
    }

    /**
     * Called when a refresh cycle is starting
     */

    open fun onRefreshStart() {
        swipeRefreshLayout.isRefreshing = true
    }

    /**
     * After values have changed, use this to update other state in the view
     */
    open fun updateButtons() {

    }

    open fun showMenu(menu: ListPresenter<V>.MenuEvent) {
        val itemView = viewFor(menu.item)
        if (itemView != null)
            activity?.let { activity ->
                AndroidMenu.showMenu(activity, menu.title, menu.choices, itemView)
            }
    }

}
