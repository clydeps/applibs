package com.controlj.view

import android.os.Bundle
import android.view.View
import com.controlj.Utilities.R
import com.controlj.logging.CJLog.logMsg
import com.controlj.settings.UserInterfaceSettings

/**
 * Created by clyde on 17/06/2016.
 */
open class KeyboardFragment : TextFragment() {

    protected var lastFocus: View? = null
    protected var keyboardView: CJKeyboardView? = null

    enum class KeyboardState {
        NONE,
        NATIVE,
        EMBEDDED
    }

    var keyboardState = KeyboardState.NONE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    protected fun showHideKeyboard() {
        val kbdActivity = activity
        if (kbdActivity is KeyboardActivity)
            when (keyboardState) {
                KeyboardState.NONE -> {
                    keyboardView?.visibility = View.GONE
                    kbdActivity.showKeyboard(false)
                }
                KeyboardState.NATIVE -> {
                    keyboardView?.visibility = View.GONE
                    kbdActivity.showKeyboard(true)

                }
                KeyboardState.EMBEDDED -> {
                    keyboardView?.visibility = View.VISIBLE
                    kbdActivity.showKeyboard(false)

                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity is KeyboardActivity) {
            keyboardView = view.findViewById(R.id.keyboard)
            if (keyboardView == null)
                keyboardView = activity!!.findViewById(R.id.keyboard)
            when {
                keyboardView == null -> keyboardState = KeyboardState.NONE
                UserInterfaceSettings.useNativeKeyboard.value -> KeyboardState.NATIVE
                keyboardView!!.attach(activity!!, view) -> keyboardState = KeyboardState.EMBEDDED
                else -> keyboardState = KeyboardState.NONE
            }
        }
    }

    open fun onBackPressed(): Boolean {
        return true
    }

    override fun onDeselected() {
        super.onDeselected()
        lastFocus = view?.findFocus()
    }

    override fun onSelected() {
        super.onSelected()
        logMsg("%s onSelected()", this.javaClass.simpleName)
        showHideKeyboard()
        lastFocus?.requestFocus()
    }

    override fun onReselected() {
        super.onReselected()
        logMsg("%s onReselected()", this.javaClass.simpleName)
        showHideKeyboard()
        lastFocus?.requestFocus()
    }

    override fun onStop() {
        logMsg("${this.javaClass.simpleName} onStop()")
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        logMsg("${this.javaClass.simpleName} onResume()")
        if (isSelected)
            showHideKeyboard()
    }
}
