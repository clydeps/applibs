package com.controlj.view

import android.content.Context
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.controlj.graphics.CColor
import com.controlj.setBackgroundTint
import com.controlj.settings.RangeSet
import com.controlj.settings.RangeState
import com.controlj.ui.DialogData
import com.controlj.ui.RangeUnitDialogItem
import com.controlj.utility.Units

class RangeUnitEntry(
        context: Context,
        override val item: RangeUnitDialogItem,
        dialogData: DialogData
) : LabelledEntry(context, item, dialogData) {

    inner class RangeGroup(first: Boolean = false) : LinearLayout(context) {
        private val startField = TextDelegate(EditText(context)).also {
            if (!first)
                it.view.isFocusable = false
        }
        private val endField = TextDelegate(EditText(context))

        private val colorSpinner: ListSpinner<RangeState> = object : ListSpinner<RangeState>(context) {
            override fun getBackgroundColor(position: Int): Int {
                return items[position].color
            }
        }.also { spinner ->
            spinner.items = RangeState.values().toList()
            spinner.listener = {
                if (it != item.selected)
                    update()
            }
        }

        init {
            startField.view.setPadding(padding, padding/2, padding, padding/2)
            startField.view.textSize = 18f
            startField.view.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED or
                    InputType.TYPE_NUMBER_FLAG_DECIMAL
            startField.view.setBackgroundTint(0.9)
            startField.view.textAlignment = TEXT_ALIGNMENT_TEXT_END
            startField.view.layoutParams = LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f)

            endField.view.setPadding(padding, padding/2, padding, padding/2)
            endField.view.textSize = 18f
            endField.view.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED or
                    InputType.TYPE_NUMBER_FLAG_DECIMAL
            endField.view.setBackgroundTint(0.9)
            endField.view.textAlignment = TEXT_ALIGNMENT_TEXT_START
            endField.view.layoutParams = LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f)
            colorSpinner.layoutParams = LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.5f)
            addView(startField.view)
            addView(TextView(context).also { it.text = " - " })
            addView(endField.view)
            addView(colorSpinner)

            @Suppress("CheckResult")
            startField.observeDebouncedText().mergeWith(endField.observeDebouncedText())
                    .subscribe {
                        update()
                    }
        }

        var currentData: RangeSet.RangeInfo
            get() = RangeSet.RangeInfo(
                    startField.view.text.toString(),
                    endField.view.text.toString(),
                    colorSpinner.selectedItem as RangeState
            )
            set(info) {
                if (startField.view.text.toString().toDoubleOrNull() != info.start.toDoubleOrNull())
                    startField.setText(info.start)
                if (endField.view.text.toString().toDoubleOrNull() != info.end.toDoubleOrNull())
                    endField.setText(info.end)
                colorSpinner.setSelection(info.state.ordinal)
            }

    }

    private val rangeGroups = item.rangeSet().ranges.mapIndexed { index, _ -> RangeGroup(index == 0) }

    val spinner = ListSpinner<Units.Unit>(context).also { spinner ->
        spinner.items = item.choices
        spinner.listener = {
            if (it != item.selected) {
                item.selected = it
                refresh()
            }
        }
        spinner.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT).also {
            it.gravity = Gravity.END
        }
        spinner.gravity = Gravity.END
        spinner.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
    }

    init {
        val rangeVert = LinearLayout(context)
        rangeVert.orientation = LinearLayout.VERTICAL
        rangeVert.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
        rangeVert.addView(spinner)
        rangeGroups.forEach { rangeVert.addView(it) }
        contentView.addView(rangeVert)
    }

    override fun refresh(): Boolean {
        var changed = false
        if (spinner.selectedItemPosition != item.selected) {
            spinner.setSelection(item.selected)
            changed = true
        }
        item.rangeSet().rangeInfos.forEachIndexed { index, rangeInfo ->
            val group = rangeGroups[index]
            group.currentData = rangeInfo
        }
        return super.refresh() || changed
    }

    fun update() {
        item.rangeSet().rangeInfos = rangeGroups.map { it.currentData }
        item.onChanged()
        refresh()
    }
}
