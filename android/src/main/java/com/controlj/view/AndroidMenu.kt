package com.controlj.view

import android.app.Activity
import android.view.Gravity
import android.view.View
import android.widget.PopupMenu
import com.controlj.rx.MainScheduler
import io.reactivex.rxjava3.core.Completable
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-07-03
 * Time: 11:23
 */
object AndroidMenu {

    fun showMenu(
            activity: Activity,
            @Suppress("unused_parameter")
            title: String,
            items: List<MenuEntry>,
            source: View?,
            gravity: Int = Gravity.CENTER_HORIZONTAL
    ) {
        val popup = PopupMenu(activity, source, gravity)
        val disposable = Completable.timer(10L, TimeUnit.SECONDS, MainScheduler.instance)
                .subscribe {
                    popup.dismiss()
                }
        popup.setOnDismissListener { disposable.dispose() }
        val menu = popup.menu
        items.forEach { entry ->
            val item = menu.add(if (entry.selected) entry.title + " \u2713" else entry.title)
            item.isEnabled = entry.enabled
            item.setOnMenuItemClickListener {
                if (entry.enabled) entry.action()
                true
            }
        }
        popup.show()
    }
}
