package com.controlj.view

import android.content.Context
import android.text.InputType
import android.view.Gravity
import android.view.View.TEXT_ALIGNMENT_TEXT_END
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import com.controlj.setBackgroundTint
import com.controlj.ui.DialogData
import com.controlj.ui.ValueDialogItem
import com.controlj.utility.Units

class ValueEntry(
        context: Context,
        override val item: ValueDialogItem,
        dialogData: DialogData
) : LabelledEntry(context, item, dialogData) {
    private val valueField = TextDelegate(EditText(context).also { field ->
        field.textSize = 18f
        field.setPadding(8, 4, 4, 4)
        field.layoutParams = LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f)
        field.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED or
                InputType.TYPE_NUMBER_FLAG_DECIMAL
        field.setBackgroundTint(0.9)
        field.textAlignment = TEXT_ALIGNMENT_TEXT_END
    }) { _: EditText, text: String ->
        item.convertedValue = text
        item.onChanged()
        dialogData.refresh(item)
    }
    private val spinner = ListSpinner<Units.Unit>(context).also { spinner ->
        spinner.items = item.unitList
        spinner.listener = {
            if (it != item.selected) {
                item.selected = it
                refresh()
            }
        }
        spinner.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        spinner.gravity = Gravity.END
        spinner.textAlignment = TEXT_ALIGNMENT_TEXT_END
    }

    init {
        contentView.addView(valueField.view)
        contentView.addView(spinner)
        @Suppress("CheckResult")
        valueField.observeDebouncedText()
                .subscribe { item.convertedValue = it }
    }

    fun update() {
    }

    override fun refresh(): Boolean {
        var changed = super.refresh()
        valueField.view.isEnabled = item.isEnabled
        if (spinner.selectedItemPosition != item.selected) {
            spinner.setSelection(item.selected)
            changed = true
        }
        if (valueField.view.text.toString() != item.convertedValue) {
            valueField.view.setText(item.convertedValue)
            changed = true
        }
        return changed
    }
}
