package com.controlj.view

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.text.Editable
import android.text.InputFilter
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.SpinnerAdapter
import android.widget.TextView
import com.controlj.Utilities.R
import com.controlj.settings.Setting
import com.controlj.settings.UnitSetting
import com.controlj.utility.Units
import java.util.Arrays

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 22/02/2016
 * Time: 9:25 AM
 */
class UnitValueView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : LinearLayout(context, attrs, defStyle), AdapterView.OnItemSelectedListener, TextWatcher {

    private var backingValue: Double = 0.0
    var value
        get() = backingValue
        set(value) {
            if (value != backingValue) {
                backingValue = value
                setValueText()
                if (valueText is EditText && selectAllOnFocus)
                    valueText.selectAll()
            }
        }
    private var updating = 0
    private val valueText: TextView
    private val selector: Spinner
    var currentUnit: Units.Unit
        get() = _units[selected]
        set(value) {
            selector.setSelection(units.indexOf(value))
        }

    private val _units = mutableListOf(Units.Unit.IDENTITY)
    var units: Iterable<Units.Unit>
        get() = _units
        set(value) {
            _units.clear()
            _units.addAll(value)
            adapter.notifyDataSetChanged()

        }
    var selected: Int
        get() = selector.selectedItemPosition
        set(value) {
            selector.setSelection(value)
        }
    var listener: ((Units.Unit, Double) -> Unit)? = null
    var selectAllOnFocus: Boolean = false
        set(value) {
            field = value
            valueText.setSelectAllOnFocus(value)
        }

    private val inflater by lazy { LayoutInflater.from(context) }

    inner class Adapter : BaseAdapter(), SpinnerAdapter {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val view = (convertView
                    ?: inflater.inflate(R.layout.unitview_spinner_view, parent, false)) as TextView
            view.text = _units[position].unitName
            return view
        }

        override fun getItem(position: Int): Any {
            return _units[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return _units.size
        }
    }

    private val adapter by lazy { Adapter() }

    fun setUnits(unitSetting: UnitSetting) {
        units = unitSetting.choices
        currentUnit = unitSetting.value
    }

    var decimalPlaces: Int = 0
        set(value) {
            field = value
            setValueText()
        }

    init {
        @Suppress("CustomViewStyleable")
        val a = context.obtainStyledAttributes(attrs, R.styleable.TextUnitValueView)
        val readonly: Boolean
        val category: String
        val places: Int
        try {
            readonly = a.getBoolean(R.styleable.TextUnitValueView_readOnly, false)
            category = a.getString(R.styleable.TextUnitValueView_category) ?: ""
            places = a.getInteger(R.styleable.TextUnitValueView_decimalPlaces, 0)
        } finally {
            a.recycle()
        }
        val v = View.inflate(context, if (readonly) R.layout.unitvalueview_readonly else R.layout.unitvalueview_editable, this) as ViewGroup
        valueText = v.getChildAt(0) as TextView
        selector = v.getChildAt(1) as Spinner
        selector.adapter = adapter
        if (category.isNotBlank()) {
            val unitSetting = Setting.get<Setting<*>>(category)
            if (unitSetting is UnitSetting) setUnits(unitSetting)
        }
        decimalPlaces = places
        val filters = valueText.filters
        val nfilters = Arrays.copyOf(filters, filters.size + 1)
        nfilters[filters.size] = InputFilter { source, start, end, _, _, _ ->
            if (start > end)
                return@InputFilter null
            var keepOriginal = true
            val sb = StringBuilder()
            (start until end).forEach { i ->
                val c = source.get(i)
                if (c == '-' || c == '.' || Character.isDigit(c))
                    sb.append(c)
                else
                    keepOriginal = false
            }
            if (keepOriginal)
                return@InputFilter null
            if (source is Spanned) {
                val sp = SpannableString(sb)
                TextUtils.copySpansFrom(source, start, end, null, sp, 0)
                return@InputFilter sp
            }
            sb
        }
        valueText.filters = nfilters
        selector.onItemSelectedListener = this
        valueText.addTextChangedListener(this)
        value = 0.0
    }

    override fun toString(): String {
        return currentUnit.toString(value, decimalPlaces) + " " + currentUnit.unitName
    }

    private fun setValueText() {
        updating++
        val newText = currentUnit.toString(backingValue, decimalPlaces)
        if (newText != valueText.text.toString()) {
            val end = valueText.selectionEnd.coerceIn(0..newText.length)
            val start = valueText.selectionStart.coerceIn(0..newText.length)
            valueText.text = newText
            if (valueText is EditText)
                valueText.setSelection(start, end)
        }
        listener?.invoke(currentUnit, value)
        updating--
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
        setValueText()
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    public override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable(SUPER_STATE, super.onSaveInstanceState())
        bundle.putInt(CURRENT_UNIT, selected)
        bundle.putDouble(CURRENT_VALUE, value)
        return bundle
    }

    public override fun onRestoreInstanceState(savedState: Parcelable?) {
        (savedState as? Bundle)?.let {
            savedState.getParcelable<Parcelable>(SUPER_STATE)?.let {
                super.onRestoreInstanceState(it)
            }
            selected = savedState.getInt(CURRENT_UNIT)
            value = savedState.getDouble(CURRENT_VALUE)
            setValueText()
        }
    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if (updating != 0)
            return
        val text = s.toString()
        if (text.isBlank())
            return
        val f = currentUnit.fromString(text)
        if (value != f) {
            value = f
        }
    }

    companion object {

        val CURRENT_VALUE = "current_value"
        val SUPER_STATE = "superState"
        val CURRENT_UNIT = "current_unit"
    }
}
