package com.controlj.view

import android.content.Context
import android.util.TypedValue
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.controlj.graphics.CColor
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem

open class BasicEntry(
        context: Context,
        item: DialogItem,
        dialogData: DialogData
) : DialogEntry(context, item, dialogData) {

    val metrics = context.resources.displayMetrics
    val padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, marginSize.toFloat(), metrics).toInt()

    override val view: LinearLayout = LinearLayout(context).also {
        it.orientation = LinearLayout.VERTICAL
        it.setPadding(padding, padding, padding, padding)
        it.layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }
    open val contentView: LinearLayout = LinearLayout(context).also {
        it.orientation = LinearLayout.HORIZONTAL
        it.layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )
        view.addView(it)
    }

    open val descriptionView = TextView(context).also {
        it.layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )
        it.text = item.description
        view.addView(it)
    }

    override fun refresh(): Boolean {
        if(item.description != descriptionView.text.toString()) {
            if (item.description.isBlank())
                descriptionView.layoutParams.height = 0
            else {
                descriptionView.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                descriptionView.text = item.description
            }
            super.refresh()
            return true
        }
        if (item.description.isBlank())
            descriptionView.layoutParams.height = 0
        return super.refresh()
    }
}
