package com.controlj.view

import android.os.Bundle
import android.util.TypedValue
import androidx.fragment.app.Fragment
import io.reactivex.rxjava3.disposables.Disposable

/**
 * Created by clyde on 4/9/18.
 */
abstract class DisposingFragment: Fragment() {
    var destroyed = true
    protected val disposables: MutableList<Disposable> = mutableListOf()
    protected val displayMetrics by lazy {
        requireContext().resources.displayMetrics
    }

    /**
     * convert a value in device-independent pixels to actual pixels
     */
    protected fun dpToPixels(dip: Double): Double =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip.toFloat(), displayMetrics).toDouble()

    override fun onStop() {
        disposables.forEach { it.dispose() }
        disposables.clear()
        super.onStop()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        destroyed = false
        super.onCreate(savedInstanceState)
    }

    override fun onDestroyView() {
        destroyed = true
        super.onDestroyView()
    }
}
