package com.controlj.view

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.widget.AppCompatSpinner

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-08-08
 * Time: 12:33
 */
@Suppress("UNUSED")
open class ListSpinner<T : Any>(
        context: Context,
        @Suppress("unused_parameter")attributeSet: AttributeSet? = null
) : AppCompatSpinner(context), AdapterView.OnItemSelectedListener {
    private val listAdapter = ListArrayAdapter()
    var listener: (Int) -> Unit = {}
    var items: List<T> = listOf()
        set(value) {
            field = value
            listAdapter.clear()
            listAdapter.addAll(value)
        }
    var ddSelectedBackgroundColor = Color.rgb(204, 255, 255)

    override fun onNothingSelected(parent: AdapterView<*>?) {
        listener(-1)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        listener(position)
    }

    open fun getBackgroundColor(position: Int): Int {
        return Color.TRANSPARENT
    }

    init {
        onItemSelectedListener = this
        adapter = listAdapter
    }

    inner class ListArrayAdapter : ArrayAdapter<T>(
            context,
            android.R.layout.simple_spinner_item
    ) {
        init {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = super.getDropDownView(position, convertView, parent)
            (view as? TextView)?.textAlignment = TEXT_ALIGNMENT_VIEW_END
            val color = getBackgroundColor(position)
            if (color != Color.TRANSPARENT)
                view.setBackgroundColor(color)
            else if (position == selectedItemPosition)
                view.setBackgroundColor(ddSelectedBackgroundColor)
            return view
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = super.getView(position, convertView, parent)
            val color = getBackgroundColor(position)
            if (color != Color.TRANSPARENT)
                view.setBackgroundColor(color)
            return view
        }
    }
}
