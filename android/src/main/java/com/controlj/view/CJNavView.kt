package com.controlj.view

import android.content.Context
import android.util.AttributeSet
import android.view.MenuItem
import com.google.android.material.navigation.NavigationView

class CJNavView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    NavigationView(context, attrs, defStyleAttr) {

    private val actionMap = mutableMapOf<MenuItem, () -> Unit>()

    init {
        setNavigationItemSelectedListener {
            actionMap[it]?.invoke()
            true
        }
    }

    fun add(title: String, icon: Int = 0, action: () -> Unit): MenuItem {
        menu.add(title).apply {
            actionMap[this] = action
            if (icon > 0)
                setIcon(icon)
            return this
        }
    }
}
