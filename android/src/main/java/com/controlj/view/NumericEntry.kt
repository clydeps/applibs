package com.controlj.view

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.controlj.Utilities.R
import com.controlj.graphics.CColor
import com.controlj.ui.DialogData
import com.controlj.ui.NumericDialogItem

class NumericEntry(
        context: Context,
        override val item: NumericDialogItem,
        dialogData: DialogData
) : LabelledEntry(context, item, dialogData) {
    val textView = TextView(context).also { textView ->
        textView.gravity = Gravity.END
        textView.setBackgroundColor(ContextCompat.getColor(context, R.color.field_normal))
        textView.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
        textView.setTextSize(18f)
        textView.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f)
        contentView.addView(textView)
    }

    override fun refresh(): Boolean {
        if (textView.text.toString() != item.value) {
            textView.text = item.value
            super.refresh()
            return true
        }
        return super.refresh()
    }
}
