package com.controlj.view

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import com.controlj.Utilities.R
import com.controlj.backend.DialogBuilder
import com.controlj.location.LocationProvider
import com.controlj.logging.CJLog
import com.controlj.ui.DialogData
import com.controlj.ui.UiAction
import io.reactivex.rxjava3.core.Single
import java.io.File

/**
 * Implement foreground services for Android
 */
open class AndroidForegroundServiceProvider(val activity: FragmentActivity) : ForegroundServiceProvider() {
    override fun showDialog(dialogData: DialogData) {
        DialogBuilder.show(activity, dialogData)
    }

    override var keepScreenOn: Boolean
        get() = (activity.window.attributes.flags and WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) != 0
        set(value) {
            if (value)
                activity.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            else
                activity.window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }

    override fun toast(message: String, duration: UiAction.ToastDuration) {
        Toast.makeText(
            activity,
            message,
            if (duration == UiAction.ToastDuration.LONG) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
        ).show()
    }

    override fun openAppSettings() {
        Intent(ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:${activity.packageName}")).apply {
            addCategory(Intent.CATEGORY_DEFAULT)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            activity.startActivity(this)
        }
    }



    override fun openUrl(url: String): Boolean {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        CJLog.logMsg("Opening url $url")
        if (browserIntent.resolveActivity(activity.packageManager) == null) {
            Toast.makeText(
                activity,
                "No application for to open URL",
                Toast.LENGTH_LONG
            ).show()
            return false
        }
        activity.startActivity(browserIntent)
        return true
    }

    override fun openFile(file: File, mimeType: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        val uri = FileProvider.getUriForFile(activity, activity.getString(R.string.fileprovider), file)
        intent.setDataAndType(uri, mimeType)
        intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_GRANT_READ_URI_PERMISSION
        val chooserIntent = Intent.createChooser(intent, "View PDF file")
        try {
            activity.startActivity(chooserIntent)
        } catch (ex: ActivityNotFoundException) {
            Toast.makeText(
                activity,
                "No application installed handles $mimeType",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    init {
        init()
    }
}
