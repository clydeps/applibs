package com.controlj.view

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.text.*
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.controlj.Utilities.R
import com.controlj.settings.Setting
import com.controlj.settings.UnitSetting
import com.controlj.utility.Units
import java.text.NumberFormat
import java.util.*

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 22/02/2016
 * Time: 9:25 AM
 */
class UnitView @JvmOverloads constructor(context: Context, attrs: AttributeSet, defStyle: Int = 0) : LinearLayout(context, attrs, defStyle), AdapterView.OnItemSelectedListener, TextWatcher {

    var value = 0.0
        set(value) {
            if (value != field) {
                field = value
                setValueText()
                if (selectAllOnFocus && valueText is EditText)
                    valueText.selectAll()
            }
        }
    private var updating = 0
    private val valueText: TextView
    private val selector: Spinner
    private val formatter: NumberFormat
    private var currentUnit: String? = null
    private var adapter: ArrayAdapter<String>? = null
    private var onValueChangeListener: OnValueChangeListener? = null
    private var selectAllOnFocus: Boolean = false
    var category: String? = null
        set(category) {

            field = category
            if (category != null && !category.isEmpty()) {
                val res = context.resources
                val values: Array<String>
                val dflt: String
                if (isInEditMode) {
                    values = testValues
                    dflt = testValues[0]
                } else {
                    val setting = Setting.map.values.filterIsInstance<UnitSetting>().filter { it.key.equals(category, true) }.firstOrNull()
                    if (setting != null) {
                        dflt = setting.value.label
                        values = setting.choices.map { it.label }.toTypedArray()
                    } else {
                        val id = res.getIdentifier(category, "array", context.packageName)
                        values = res.getStringArray(id)
                        dflt = values[0]
                    }
                }
                adapter = ArrayAdapter(context, R.layout.unitview_spinner_view, values)
                adapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                selector.adapter = adapter
                selector.setSelection(adapter!!.getPosition(dflt))
            }
        }
    private var decimalPlaces: Int = 0

    interface OnValueChangeListener {
        fun onValueChanged(uv: UnitView)
    }

    init {
        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.TextUnitValueView, defStyle, 0)
        val v: ViewGroup
        if (a.getBoolean(R.styleable.TextUnitValueView_readOnly, false)) {
            v = View.inflate(context, R.layout.unitvalueview_readonly, this) as ViewGroup
        } else {
            v = View.inflate(context, R.layout.unitvalueview_editable, this) as ViewGroup
        }
        valueText = v.getChildAt(0) as TextView
        selector = getChildAt(1) as Spinner
        formatter = NumberFormat.getNumberInstance()
        formatter.isGroupingUsed = false
        val label = a.getString(R.styleable.TextUnitValueView_label)
        if (label != null) {
            val tv = TextView(context, attrs, defStyle)
            tv.text = label
            tv.gravity = Gravity.RIGHT
            addView(tv, 0)
        }
        try {
            val selectAllOnFocus = a.getBoolean(R.styleable.TextUnitValueView_selectAllOnFocus, false)
            setSelectAllOnFocus(selectAllOnFocus)
            if (a.getBoolean(R.styleable.TextUnitValueView_flagNoFullscreen, false))
                valueText.imeOptions = valueText.imeOptions or EditorInfo.IME_FLAG_NO_FULLSCREEN
            setDecimalPlaces(a.getInt(R.styleable.TextUnitValueView_decimalPlaces, 0))
            category = a.getString(R.styleable.TextUnitValueView_category)
        } finally {
            a.recycle()
        }
        val filters = valueText.filters
        val nfilters = Arrays.copyOf(filters, filters.size + 1)
        nfilters[filters.size] = InputFilter { source, start, end, _, _, _ ->
            if (start > end)
                return@InputFilter null
            var keepOriginal = true
            val sb = StringBuilder()
            for (i in start until end) {
                val c = source.get(i)
                if (c == '-' || c == '.' || Character.isDigit(c))
                    sb.append(c)
                else
                    keepOriginal = false
            }
            if (keepOriginal)
                return@InputFilter null
            if (source is Spanned) {
                val sp = SpannableString(sb)
                TextUtils.copySpansFrom(source, start, end, null, sp, 0)
                return@InputFilter sp
            }
            sb
        }
        valueText.filters = nfilters
        selector.onItemSelectedListener = this
        valueText.addTextChangedListener(this)
        currentUnit = selector.selectedItem.toString()
        value = 0.0
    }

    fun setDecimalPlaces(decimalPlaces: Int) {
        this.decimalPlaces = decimalPlaces
        setFormatter()
    }

    private fun setFormatter() {
        if (currentUnit != null) {
            val unit = Units.getUnit(currentUnit!!)
            var places = decimalPlaces + Math.round(Math.log10(unit.ratio)).toInt()
            if (places < 0)
                places = 0
            formatter.maximumFractionDigits = places
            formatter.minimumFractionDigits = places
        }
    }

    override fun toString(): String {
        if (currentUnit != null && !currentUnit!!.isEmpty()) {
            val v = Units.convertTo(value, currentUnit!!)
            return formatter.format(v) + " " + currentUnit
        }
        return formatter.format(value)
    }

    fun setSelectAllOnFocus(value: Boolean) {
        valueText.setSelectAllOnFocus(value)
    }

    fun setOnValueChangeListener(onValueChangeListener: OnValueChangeListener) {
        this.onValueChangeListener = onValueChangeListener
    }


    private fun setValueText() {
        val fvalue = if (currentUnit != null) Units.convertTo(value, currentUnit!!) else value
        updating++
        val newText = formatter.format(fvalue)
        if (newText != valueText.text.toString()) {
            val end = valueText.selectionEnd//.coerceIn(0..newText.length)
            val start = valueText.selectionStart//.coerceIn(0..newText.length)
            valueText.text = newText
            if (valueText is EditText)
                valueText.setSelection(start, end)
        }
        updating--
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
        currentUnit = parent.getItemAtPosition(position).toString()
        setFormatter()
        setValueText()
    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    public override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable(SUPER_STATE, super.onSaveInstanceState())
        bundle.putString(CURRENT_UNIT, currentUnit)
        bundle.putDouble(CURRENT_VALUE, value)
        return bundle
    }

    public override fun onRestoreInstanceState(savedState: Parcelable?) {
        if (savedState is Bundle) {
            val state = savedState.getParcelable<Parcelable>(SUPER_STATE)
            currentUnit = savedState.getString(CURRENT_UNIT)
            selector.setSelection(adapter!!.getPosition(currentUnit))
            value = savedState.getDouble(CURRENT_VALUE)
            setValueText()
            super.onRestoreInstanceState(state)
        } else
            super.onRestoreInstanceState(savedState)

    }

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

    }

    override fun afterTextChanged(s: Editable) {
        if (updating != 0)
            return
        val text = s.toString()
        if (text.isEmpty())
            return
        try {
            var f = valueText.text.toString().toDouble()
            if (currentUnit != null)
                f = Units.convertFrom(f, currentUnit!!)
            if (value != f) {
                value = f
                onValueChangeListener?.onValueChanged(this)
            }
        } catch (ignored: NumberFormatException) {
        }

    }

    companion object {

        private val testValues = arrayOf("ft", "m")
        val CURRENT_VALUE = "current_value"
        val SUPER_STATE = "superState"
        val CURRENT_UNIT = "current_unit"
    }
}
