package com.controlj.view

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.controlj.Utilities.R
import com.controlj.ui.DialogData
import com.controlj.ui.SeparatorDialogItem

class SeparatorEntry(
        context: Context,
        override val item: SeparatorDialogItem,
        dialogData: DialogData
) : DialogEntry(context, item, dialogData) {
    override val view = View(context).also {
        it.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, context.resources.getDimensionPixelSize(R.dimen.separatorHeight))
        it.setBackgroundColor(0x808080)
    }
}