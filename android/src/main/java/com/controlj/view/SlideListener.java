package com.controlj.view;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 29/04/2015
 * Time: 12:06 PM
 */
public interface SlideListener {
	void onSlide(int displacement);
}
