package com.controlj.view

import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.controlj.Utilities.R
import com.controlj.viewmodel.PageViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 6/05/2016
 * Time: 2:36 PM
 */
abstract class PagedFragment(private val resid: Int) : KeyboardFragment(), RegisteredFragmentAdapter.OnSelectionListener {

    lateinit var pagerAdapter: RegisteredFragmentAdapter<out KeyboardFragment>
    abstract val viewModel: PageViewModel
    protected lateinit var pager: ViewPager

    var pagerPosition: Int
        get() = pager.currentItem
        set(value) {
            if (pager.currentItem != value)
                pager.currentItem = value
            viewModel.pageNumber.data = value
        }

    val currentFragment: KeyboardFragment
        get() = pagerAdapter.instantiateItem(pager, pagerPosition) as KeyboardFragment

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(resid, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pager = view.findViewById(R.id.pager)
        pager.adapter = pagerAdapter
        pagerAdapter.setListener(this)
    }


    override fun onStart() {
        super.onStart()
        pagerPosition = viewModel.pageNumber.data
    }
    override fun onSelected(fragment: SelectableFragment) {
        viewModel.pageNumber.data = pager.currentItem
        Completable.timer(2, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (currentFragment === fragment)
                        fragment.onReselected()
                    else
                        fragment.onSelected()
                }
    }

}
