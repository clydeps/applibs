package com.controlj.view

import android.content.Context
import android.view.Choreographer
import androidx.appcompat.app.AppCompatActivity
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity
import com.controlj.animation.AnimationController
import com.controlj.framework.ApplicationState
import io.reactivex.rxjava3.disposables.CompositeDisposable

/**
 * Created by clyde on 13/8/18.
 *
 * This class
 */
abstract class KeyboardActivity : FragmentActivity(), ViewContext, Choreographer.FrameCallback {

    fun showKeyboard(show: Boolean) {
        val v = currentFocus
        window.setSoftInputMode(
            if (show)
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
            else
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
        if (v != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (show)
                imm.showSoftInput(v, 0)
            else {
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                v.clearFocus()
            }
        }
    }

}
