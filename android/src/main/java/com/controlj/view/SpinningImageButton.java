package com.controlj.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.controlj.Utilities.R;

/**
 * Created by clyde on 13/11/17.
 */

public class SpinningImageButton extends androidx.appcompat.widget.AppCompatImageButton {
    private int busyness;
    private Animation animation;

    public SpinningImageButton(Context context) {
        super(context);
    }

    public SpinningImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinningImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void spin() {
        if(animation == null)
            animation = AnimationUtils.loadAnimation(getContext(), R.anim.spin_button);
        if(busyness == 0)
            animation.setRepeatCount(0);    // this will stop it at the start of the next cycle
        else {
            animation.setRepeatCount(10000);
            startAnimation(animation);
        }
    }

    public int getBusyness() {
        return busyness;
    }

    public void setBusy() {
        setBusyness(busyness + 1);
    }

    public void clearBusy() {
        setBusyness(busyness - 1);
    }

    public void setBusyness(int busyness) {
        if(busyness < 0)
            busyness = 0;
        this.busyness = busyness;
        spin();
    }
}
