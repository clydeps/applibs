package com.controlj.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import com.controlj.graphics.CPoint
import com.controlj.graphics.CRect
import com.controlj.graphics.CanvasWrapper
import com.controlj.graphics.GraphicsFactory
import com.controlj.widget.CView
import kotlin.math.ceil
import kotlin.math.min

/**
 * Created by Clyde Stubbs on 6/07/2017.
 */

open class CViewWrapper @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr), ViewController {
    private var gestureDetector: GestureDetector? = null
    private var zoomDetector: ScaleGestureDetector? = null
    private var gestureListener = object : GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, ScaleGestureDetector.OnScaleGestureListener {
        override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
            return true
        }

        override fun onScaleEnd(detector: ScaleGestureDetector) {
        }

        override fun onScale(detector: ScaleGestureDetector): Boolean {
            cView.apply {
                val hippo = detector.currentSpan
                val dy = detector.currentSpanY / hippo
                val dx = detector.currentSpanX / hippo
                val scale = detector.scaleFactor - 1.0
                onZoom(CPoint(detector.focusX.toDouble(), detector.focusY.toDouble()), 1 + scale * dx, 1 + scale * dy)
                return true
            }
        }

        override fun onDoubleTap(e: MotionEvent): Boolean {
            return cView.onDoubleClick(e.toCPoint())
        }

        override fun onDoubleTapEvent(e: MotionEvent): Boolean {
            return false
        }

        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            return false
        }

        override fun onShowPress(e: MotionEvent) {
            cView.onTouch(e.toCPoint(), true)
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            return cView.onClick(e.toCPoint())
        }

        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            return cView.onFling(e2.toCPoint(), velocityX.toDouble(), velocityY.toDouble())
        }

        override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
            if (e2.pointerCount != 1) return false
            cView.let {
                if (it is CView.PanListener)
                    return it.onPan(e2.toCPoint(), -distanceX.toDouble(), -distanceY.toDouble())
            }
            return false
        }

        override fun onLongPress(e: MotionEvent) {
            cView.onLongPress(e.toCPoint(), false)
        }
    }

    var cView: CView = CView("dummy")
        set(value) {
            if (field != value) {
                paint.style = Paint.Style.FILL
                paint.color = Color.BLACK
                paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC)
                field.parent = null
                field = value
                value.let { v: CView ->
                    check(v.parent == null) { "CView already has a parent" }
                    v.parent = this
                    gestureDetector = GestureDetector(context, gestureListener)
                    zoomDetector = ScaleGestureDetector(context, gestureListener)
                }
            }
        }

    override var bounds: CRect
        get() = CRect(0.0, 0.0, width.toDouble(), height.toDouble())
        set(value) {
            cView.bounds = value
        }
    private var canvasWrapper = CanvasWrapper()
    private var paint = Paint()
    internal var background: Bitmap? = null

    override fun onDraw(canvas: Canvas) {
        if (background == null) {
            cView.let {
                val cRect = it.bounds
                if (!cRect.empty) {
                    val gc = GraphicsFactory().getImageBuffer(
                            cRect.width.toInt(),
                            cRect.height.toInt()
                    ) as CanvasWrapper
                    it.drawBackground(gc)
                    background = gc.androidBitmap
                }
            }
        }
        background?.let {
            canvasWrapper.setCanvas(canvas)
            canvas.drawBitmap(it, 0f, 0f, paint)
            cView.draw(canvasWrapper)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        bounds = CRect(0.0, 0.0, w.toDouble(), h.toDouble())
        background = null
    }

    @SuppressLint("SwitchIntDef")
    private fun getDimension(spec: Int, preferred: Float): Int {
        val specSize = MeasureSpec.getSize(spec)
        val pref = ceil(preferred.toDouble()).toInt()

        return when (MeasureSpec.getMode(spec)) {
            MeasureSpec.EXACTLY -> specSize

            MeasureSpec.AT_MOST -> min(specSize, pref)

            else -> pref
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        cView.let {
            setMeasuredDimension(getDimension(widthMeasureSpec, it.preferredWidth.toFloat()),
                    getDimension(heightMeasureSpec, it.preferredHeight.toFloat()))
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_UP -> {
                cView.onLongPress(event.toCPoint(), true)
            }
        }
        val result = zoomDetector?.onTouchEvent(event) ?: false
        if (gestureDetector?.onTouchEvent(event) == true || result)
            return true
        return super.onTouchEvent(event)
    }

    override fun requestRedraw(rect: CRect) {
        background = null
        super.invalidate()
    }

    override fun redrawForeground(rect: CRect) {
        //setVisibility(cView.isVisible() ? VISIBLE : INVISIBLE);
        super.invalidate()
    }
}

fun MotionEvent.toCPoint(): CPoint {
    return CPoint(x.toDouble(), y.toDouble())
}
