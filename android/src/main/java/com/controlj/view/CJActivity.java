/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlj.view;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.controlj.Utilities.R;
import com.controlj.content.DrawerContent;
import com.controlj.content.MenuEntry;

/**
 * @author clyde
 */

class DrawerAdapter extends BaseExpandableListAdapter {
    private Activity activity;
    private static final int[] EMPTY_STATE_SET = {};
    private static final int[] GROUP_EXPANDED_STATE_SET = {android.R.attr.state_expanded};
    private static final int[][] GROUP_STATE_SETS = {EMPTY_STATE_SET, // 0
        GROUP_EXPANDED_STATE_SET // 1
    };
    private DrawerContent content;

    public DrawerAdapter(Activity activity, DrawerContent content) {
        super();
        this.activity = activity;
        this.content = content;
    }

	/*
    @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null) {
			convertView = activity.getLayoutInflater().inflate(R.layout.drawer_entry, null);
		}
		ImageView iv = (ImageView)convertView.findViewById(R.id.drawer_entry_image);
		TextView tv = (TextView)convertView.findViewById(R.id.drawer_entry_text);
		MenuEntry entry = getItem(position);
		Drawable d = null;//entry.getImage();		// not all logos are available so don't use them.
		if(d != null) {
			tv.setVisibility(View.GONE);
			iv.setVisibility(View.VISIBLE);
			iv.setImageDrawable(d);
		} else {
			iv.setVisibility(View.GONE);
			tv.setVisibility(View.VISIBLE);
			tv.setText(entry.getText());
		}
		return convertView;
	} */

    @Override
    public int getGroupCount() {
        return content.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return content.get(i).size();
    }

    @Override
    public Object getGroup(int i) {
        return content.get(i);
    }

    @Override
    public Object getChild(int groupPos, int childPos) {
        MenuEntry g = (MenuEntry)getGroup(groupPos);
        return g.get(childPos);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean isExpanded, View view, ViewGroup viewGroup) {

        if(view == null)
            view = activity.getLayoutInflater().inflate(R.layout.groupview, null);
        MenuEntry me = content.get(i);
        //view = activity.getLayoutInflater().inflate(android.R.layout.simple_expandable_list_item_2, null);
        TextView tv = view.findViewById(R.id.grouptext);
        tv.setText(me.getText());
        ImageView indView = view.findViewById(R.id.groupindicator);
        if(me.isEmpty()) {
            indView.setVisibility(View.GONE);
            tv.setBackgroundColor(0xFFDDDDFF);
        } else {
            indView.setVisibility(View.VISIBLE);
            int stateSetIndex = (isExpanded ? 1 : 0);
            Drawable drawable = indView.getDrawable();
            drawable.setState(GROUP_STATE_SETS[stateSetIndex]);
            tv.setBackgroundColor(0xFFFFFFFF);
        }
        return view;
    }

    @Override
    public View getChildView(int groupPos, int childPos, boolean b, View convertView, ViewGroup viewGroup) {
        if(convertView == null) {
            convertView = activity.getLayoutInflater().inflate(R.layout.drawer_entry, null);
        }
        ImageView iv = convertView.findViewById(R.id.drawer_entry_image);
        TextView tv = convertView.findViewById(R.id.drawer_entry_text);
        MenuEntry entry = (MenuEntry)getChild(groupPos, childPos);
        Drawable d = null;//entry.getImage();		// not all logos are available so don't use them.
        if(d != null) {
            tv.setVisibility(View.GONE);
            iv.setVisibility(View.VISIBLE);
            iv.setImageDrawable(d);
        } else {
            iv.setVisibility(View.GONE);
            tv.setVisibility(View.VISIBLE);
            tv.setText(entry.getText());
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    public DrawerContent getContent() {
        return content;
    }
}

