package com.controlj.view

import android.animation.LayoutTransition
import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.View.MeasureSpec.EXACTLY
import android.view.ViewGroup
import androidx.core.view.GestureDetectorCompat
import com.controlj.Utilities.R

/**
 * Created by clyde on 27/4/18.
 */
class SlideViewLayout @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defAttr: Int = 0) : ViewGroup(context, attrs, defAttr), SlideViewPresenter.View {
    val presenter: SlideViewPresenter

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.SlideViewLayout)
        try {
            val edges = SlideViewPresenter.Edges.values()[a.getInteger(R.styleable.SlideViewLayout_edges, 0)]
            val leftFrac = a.getFloat(R.styleable.SlideViewLayout_leftFrac, 0.1f).toDouble()
            val rightFrac = a.getFloat(R.styleable.SlideViewLayout_rightFrac, 0.1f).toDouble()
            presenter = SlideViewPresenter(this, edges, leftFrac, rightFrac)
        } finally {
            a.recycle()
        }
        //debug("leftFrac = $leftFrac, rightFrac = $rightFrac")
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
    }

    val gestureDetectors: Array<GestureDetectorCompat> by lazy {
        arrayOf(
                GestureDetectorCompat(context, SwipeDetector(presenter.swipeListeners[0])),
                GestureDetectorCompat(context, SwipeDetector(presenter.swipeListeners[1]))
        )
    }

    var inited = false
    override var wantFullScreen: Boolean = false
        set(value) {
            if (field != value) {
                field = value
                doLayout()
            }
        }

    override fun doLayout() {
        handler.post {
            //CJLog.debug("requestLayout");
            requestLayout()
        }
    }

    inner class TouchListener(val index: Int) : OnTouchListener {

        init {
            getChildAt(index + 1).setOnTouchListener(this)
        }

        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(v: View, event: MotionEvent): Boolean {
            return gestureDetectors[index].onTouchEvent(event)
        }

    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        //CJLog.debug("onLayout, $l, $t, $r, $b")
        val height = b - t
        val width = r - l
        if (childCount >= 3 && height > 0 && width > 0) {
            presenter.onMeasure(width, height)
            if (!inited) {
                TouchListener(0)
                TouchListener(1)
                inited = true
            }
            presenter.childRects.forEachIndexed { index, cRect ->
                val child = getChildAt(index)
                measureChild(child, MeasureSpec.makeMeasureSpec(cRect.width.toInt(), EXACTLY),
                        MeasureSpec.makeMeasureSpec(cRect.height.toInt(), EXACTLY))
                child.layout(cRect.left.toInt(), cRect.top.toInt(), cRect.right.toInt(), cRect.bottom.toInt())
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        //CJLog.debug("onMeasure, childcount=$childCount, measuredWidth=$measuredWidth, measuredHeight=$measuredHeight")
        // we always fill the available space
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec))
        presenter.onMeasure(measuredWidth, measuredHeight)
        presenter.childRects.forEachIndexed { index, cRect ->
            val child = getChildAt(index)
            measureChild(child, MeasureSpec.makeMeasureSpec(cRect.width.toInt(), EXACTLY),
                    MeasureSpec.makeMeasureSpec(cRect.height.toInt(), EXACTLY))
        }
    }
}
