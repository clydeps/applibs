package com.controlj.view

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.widget.EditText
import com.controlj.rx.DisposedEmitter
import com.controlj.ui.DialogItem
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-07-15
 * Time: 14:52
 */
class TextDelegate(
        val view: EditText,
        style: DialogItem.InputStyle = DialogItem.InputStyle.PLAIN,
        val callback: (EditText, String) -> Unit = { _, _ -> }
) : TextWatcher {

    private var suspend: Boolean = false
    private var emitter: ObservableEmitter<Data> = DisposedEmitter()
    private var lastText = view.text.toString()
    enum class Event {
        BEFORE, // event thrown before text is changed
        ON, // thrown when text is changed
        AFTER          // thrown after change is complete.
    }

    class Data(val event: Event, val text: CharSequence, val start: Int, val before: Int, val after: Int)

    override fun beforeTextChanged(charSequence: CharSequence, start: Int, before: Int, after: Int) {
        if (!suspend)
            emitter.onNext(Data(Event.BEFORE, charSequence, start, before, after))
    }

    override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, after: Int) {
        if (!suspend)
            emitter.onNext(Data(Event.ON, charSequence, start, before, after))
    }

    override fun afterTextChanged(s: Editable) {
        val text = s.toString()
        if (!suspend) {
            emitter.onNext(Data(Event.AFTER, s.toString(), 0, 0, 0))
            if (text != lastText)
                callback(view, s.toString())
        }
    }
    fun setText(text: String): Boolean {
        lastText = text
        if (text != view.text.toString()) {
            suspend = true
            val selectionStart = view.selectionStart.coerceIn(0, text.length)
            val selectionEnd = view.selectionEnd.coerceIn(selectionStart, text.length)
            view.setText(text)
            view.setSelection(selectionStart, selectionEnd)
            suspend = false
            return true
        }
        return false
    }

    fun setPasswordStyle(visible: Boolean) {
        view.inputType = if (visible) (InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD or
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS)
        else
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD or
                    InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
    }

    init {
        view.addTextChangedListener(this)
        when (style) {
            DialogItem.InputStyle.PASSWORD, DialogItem.InputStyle.VISIBLE_PASSWORD -> {
                setPasswordStyle(style == DialogItem.InputStyle.VISIBLE_PASSWORD)
            }
            else -> view.inputType = when (style) {
                DialogItem.InputStyle.PHONE -> InputType.TYPE_CLASS_PHONE
                DialogItem.InputStyle.EMAIL -> InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                DialogItem.InputStyle.DECIMAL -> {
                    view.gravity = Gravity.END
                    view.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED or InputType.TYPE_NUMBER_FLAG_DECIMAL
                }
                DialogItem.InputStyle.SIGNEDDECIMAL -> {
                    view.gravity = Gravity.END
                    view.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED or InputType.TYPE_NUMBER_FLAG_DECIMAL
                }
                DialogItem.InputStyle.INTEGER -> {
                    view.gravity = Gravity.END
                    view.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                    InputType.TYPE_CLASS_NUMBER
                }
                DialogItem.InputStyle.SIGNEDINT -> {
                    view.gravity = Gravity.END
                    view.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED
                }
                DialogItem.InputStyle.MULTI_LINE -> {
                    InputType.TYPE_TEXT_FLAG_MULTI_LINE or InputType.TYPE_CLASS_TEXT
                }
                else -> {
                    view.gravity = Gravity.START
                    InputType.TYPE_CLASS_TEXT
                }
            }
        }
    }
    fun observe(): Observable<Data> {
        return Observable.create<Data> {
            emitter = it
        }
    }

    fun observeDebouncedText(): Observable<String> {
        return observe()
                .filter { data -> data.event == Event.AFTER }
                .debounce(500, TimeUnit.MILLISECONDS)
                .map { view.text.toString() }
                .observeOn(AndroidSchedulers.mainThread())
    }
}
