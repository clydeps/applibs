package com.controlj.view

import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 14/04/2015
 * Time: 1:20 PM
 */

@Suppress("UNCHECKED_CAST")
abstract class RegisteredFragmentAdapter<T : SelectableFragment>(fm: FragmentManager) :
        FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var listener: OnSelectionListener? = null
    var currentItem: T? = null
        private set

    fun setListener(listener: OnSelectionListener) {
        this.listener = listener
    }

    interface OnSelectionListener {
        fun onSelected(fragment: SelectableFragment)
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, obj: Any) {
        super.setPrimaryItem(container, position, obj)
        if (obj != currentItem) {
            currentItem?.onDeselected()
            obj as T
            currentItem = obj
            if (obj.activity != null)
                obj.onSelected()
            listener?.onSelected(obj)
        }
    }

    fun tabReselected() {
        currentItem?.let {
            listener?.onSelected(it)
        }
    }
}
