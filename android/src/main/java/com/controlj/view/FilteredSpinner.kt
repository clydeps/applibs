package com.controlj.view

import android.content.Context
import androidx.appcompat.widget.AppCompatSpinner
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter

/**
 * Created by clyde on 22/11/17.
 */
class FilteredSpinner : AppCompatSpinner {
    private var lastSelection = AdapterView.INVALID_POSITION

    constructor(context: Context) : super(context)
    constructor(context: Context, mode: Int) : super(context, mode)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, mode: Int) : super(
            context,
            attrs,
            defStyleAttr,
            mode
    )

    override fun toString(): String {
        return super.toString()
    }

    fun pgmSetSelection(i: Int) {
        lastSelection = i
        setSelection(i)
    }

    /**
     * Observe item selections within this spinner. Events will not be delivered if they were triggered
     * by a call to setSelection(). Selection of nothing will return an event equal to INVALID_POSITION
     *
     * @return an Observable delivering selection events
     */
    fun observeSelections(): Observable<Int?> {
        return Observable.create { emitter: ObservableEmitter<Int?> ->
            onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                    if (i != lastSelection) {
                        lastSelection = i
                        emitter.onNext(i)
                    }
                }

                override fun onNothingSelected(adapterView: AdapterView<*>?) {
                    onItemSelected(adapterView, null, AdapterView.INVALID_POSITION, 0)
                }
            }
        }
    }
}
