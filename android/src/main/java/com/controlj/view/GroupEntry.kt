package com.controlj.view

import android.content.Context
import androidx.core.content.ContextCompat
import com.controlj.Utilities.R
import com.controlj.graphics.CColor
import com.controlj.ui.DialogData
import com.controlj.ui.GroupDialogItem

class GroupEntry(
        context: Context,
        item: GroupDialogItem,
        dialogData: DialogData
) : LabelledEntry(context, item, dialogData) {
    init {
        label.setBackgroundColor(ContextCompat.getColor(context, R.color.group_background))
        label.setTextColor(DialogData.TINTCOLOR)
    }
}
