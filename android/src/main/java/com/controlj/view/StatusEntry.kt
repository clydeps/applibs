package com.controlj.view

import android.content.Context
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.controlj.ui.DialogData
import com.controlj.ui.StatusDialogItem

class StatusEntry(
        context: Context,
        override val item: StatusDialogItem,
        dialogData: DialogData
) : LabelledEntry(context, item, dialogData) {

    val progressBar = ProgressBar(context, null, android.R.attr.progressBarStyleSmall).also { progressBar ->
        progressBar.layoutParams = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
        contentView.addView(progressBar)
    }

    override fun refresh(): Boolean {
        if(item.busy != (progressBar.visibility == View.VISIBLE)) {
            progressBar.visibility = if (item.busy) View.VISIBLE else View.INVISIBLE
            super.refresh()
            return true
        }
        return super.refresh()
    }
}
