package com.controlj.view

import android.content.Context
import android.content.res.Configuration
import com.controlj.settings.DataStore

/**
 * Created by clyde on 13/8/18.
 */
open class TextFragment : SelectableFragment() {
    private val TEXT_INCREMENT = 1.1f
    private val TEXT_MAX_SCALE = 4.0f
    private val DEFAULT_SCALE = 1.0f
    private val textScalePrefPortrait = this.javaClass.simpleName + "_text_scale_portrait"
    private val textScalePrefLandscape = this.javaClass.simpleName + "_text_scale_landscape"
    private var textScalePref = textScalePrefPortrait

    protected var textScale = DEFAULT_SCALE

    open fun onTextScaleChanged() {
        DataStore().putFloat(textScalePref, textScale)
    }

    fun textZoomIn(): Boolean {
        if (textScale < TEXT_MAX_SCALE) {
            textScale *= TEXT_INCREMENT
            onTextScaleChanged()
        }
        return textScale < TEXT_MAX_SCALE
    }

    fun textZoomReset() {
        textScale = 1.0f
        onTextScaleChanged()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        updateTextPref()
        onTextScaleChanged()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        updateTextPref()
        onTextScaleChanged()
    }

    fun textZoomOut(): Boolean {
        if (textScale > 1.0 / TEXT_MAX_SCALE) {
            textScale /= TEXT_INCREMENT
            onTextScaleChanged()
        }
        return textScale > 1.0 / TEXT_MAX_SCALE
    }

    protected fun updateTextPref(defaultScale: Float = DEFAULT_SCALE) {
        val orientation = activity!!.resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE)
            textScalePref = textScalePrefLandscape
        else
            textScalePref = textScalePrefPortrait
        val newScale = DataStore().getFloat(textScalePref, defaultScale)
        if (textScale != newScale) {
            textScale = newScale
            onTextScaleChanged()
        }
    }
}
