package com.controlj.view

import android.content.Context
import com.controlj.ui.CViewItem
import com.controlj.ui.DialogData

class CviewEntry(
        context: Context,
        override val item: CViewItem,
        dialogData: DialogData
) : DialogEntry(context, item, dialogData) {
    override val view: CViewWrapper = CViewWrapper(context).also { it.cView = item.cView }
}
