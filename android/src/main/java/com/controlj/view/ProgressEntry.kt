package com.controlj.view

import android.annotation.SuppressLint
import android.content.Context
import android.widget.ProgressBar
import com.controlj.Utilities.R
import com.controlj.ui.DialogData
import com.controlj.ui.ProgressDialogItem

class ProgressEntry(
        context: Context,
        override val item: ProgressDialogItem,
        dialogData: DialogData
) : DialogEntry(context, item, dialogData) {
    @SuppressLint("InflateParams")
    override val view: ProgressBar = (inflater.inflate(R.layout.progressbar, null) as ProgressBar).also {
        it.isIndeterminate = false
    }

    override fun refresh(): Boolean {
        if (view.progress != item.progress) {
            view.progress = item.progress
            super.refresh()
            return true
        }
        return super.refresh()
    }
}
