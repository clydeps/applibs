package com.controlj.view

/**
 * Created by clyde on 30/4/18.
 */

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.controlj.Utilities.R
import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.Position
import com.controlj.getImageId
import com.controlj.logging.CJLog
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.mapbox.ScaleBarPlugin
import com.controlj.mapbox.ScaleBarUnit
import com.controlj.mapping.Skysight
import com.controlj.route.TrackSource
import com.controlj.settings.LayerSettings
import com.controlj.traffic.TrafficTarget
import com.controlj.ui.ViewModelStore
import com.mapbox.android.gestures.ShoveGestureDetector
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdate
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.location.LocationComponent
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.LocationUpdate
import com.mapbox.mapboxsdk.location.OnCameraTrackingChangedListener
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.MapboxMap.OnCameraMoveStartedListener.REASON_API_GESTURE
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.module.http.HttpRequestUtil
import com.mapbox.mapboxsdk.plugins.annotation.Line
import com.mapbox.mapboxsdk.plugins.annotation.LineManager
import com.mapbox.mapboxsdk.plugins.annotation.LineOptions
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import com.mapbox.mapboxsdk.style.expressions.Expression
import com.mapbox.mapboxsdk.style.layers.FillLayer
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillColor
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillOpacity
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.fillOutlineColor
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.rasterOpacity
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.textField
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility
import com.mapbox.mapboxsdk.style.layers.PropertyValue
import com.mapbox.mapboxsdk.style.layers.RasterLayer
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.layers.TransitionOptions
import com.mapbox.mapboxsdk.style.sources.RasterSource
import com.mapbox.mapboxsdk.style.sources.TileSet
import com.mapbox.mapboxsdk.style.sources.VectorSource
import com.mapbox.mapboxsdk.utils.ColorUtils
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.disposables.Disposable
import org.threeten.bp.Duration

@SuppressLint("MissingPermission")
/**
 * Support Fragment wrapper around a map view.
 *
 */
@Suppress("CheckResult")
open class RouteMapFragment : DisposingFragment(), OnCameraTrackingChangedListener {

    companion object {
        fun createInstance(): Fragment = RouteMapFragment()

    }

    override fun onCameraTrackingChanged(currentMode: Int) {
        //debug("mode =$currentMode")
    }

    override fun onCameraTrackingDismissed() {
        //debug("dismissed")
        locationLayer.cameraMode = CameraMode.NONE
        //presenter.onViewChanged()
    }

    private lateinit var trafficManager: SymbolManager
    private val trafficMap = mutableMapOf<TrafficTarget.EmitterAddress, Symbol>()
    private var disposable: Disposable = Disposable.disposed()
    open val presenter: RouteMapViewModel = ViewModelStore.loadViewModel(RadarMapViewModel::class.java)
    private lateinit var locationLayer: LocationComponent

    // need multiple line managers to arrange layering.
    private val trackLineManagers = mutableMapOf<TrackSource.TrackKey, LineManager>()

    // but also need to keep track of old lines to be able to delete them (deleteAll() causes flickering.)
    private val lineMap = mutableMapOf<TrackSource.TrackKey, Line>()
    private lateinit var routeLineManager: LineManager
    private lateinit var mapStyle: Style

    fun hideDebugLayers() {
        removeLayer("debug_label")
        removeLayer("debug_bounds")
    }

    fun showDebugLayers(source: String) {
        mapboxMap.subscribe(
            { map ->
                val outline = LineLayer("debug_bounds", source)
                outline.sourceLayer = "debug_bounds"
                map.style?.addLayer(outline)
                outline.setProperties(
                    visibility(Property.VISIBLE),
                    PropertyFactory.lineColor(Expression.color(Color.BLACK)),
                    PropertyFactory.lineWidth(2.0f),
                    PropertyFactory.lineOpacity(1.0f)
                )
                val label = SymbolLayer("debug_label", source)
                label.sourceLayer = "debug_label"
                map.style?.addLayer(label)
                label.setProperties(
                    textField(Expression.get("tile")),
                    visibility(Property.VISIBLE),
                    PropertyFactory.textColor(Expression.color(0xFF000000.toInt())),
                    PropertyFactory.textOpacity(1.0f),
                    PropertyFactory.textSize(40.0f)
                )
            },
            ::logException
        )
    }

    private fun removeLayer(name: String) {
        mapboxMap.subscribe(
            { map ->
                val layer = map.style?.getLayer(name)
                if (layer != null) {
                    //debug("Removing layer ${layer.id}")
                    map.style?.removeLayer(layer)
                }
            },
            ::logException
        )
    }

    private lateinit var mapView: MapView
    private lateinit var mapboxMap: Single<MapboxMap>

    /**
     * Flag to say the user is moving the camera
     */
    private var cameraMoving: Boolean = false

    private fun cameraMoved(camera: CameraPosition) {
        if (cameraMoving)
            presenter.onViewChanged(
                camera.target.toPosition(),
                camera.zoom,
                camera.tilt
            )
    }

    private fun getMapboxSingle(): Single<MapboxMap> {
        return Single.create { e: SingleEmitter<MapboxMap> ->
            mapView.getMapAsync { mapbox ->
                mapbox.setMaxZoomPreference(presenter.maximumZoomLevel)
                mapbox.setMinZoomPreference(presenter.minimumZoomLevel)

                mapbox.setStyle(Style.Builder().fromUri(presenter.style)) { style ->
                    mapStyle = style
                    requireActivity().apply {
                        RouteMapViewModel.TrafficIcon.values().forEach {
                            style.addImage(it.name, ContextCompat.getDrawable(this, getImageId(it.imageFile))!!)
                        }
                    }
                    routeLineManager = LineManager(mapView, mapbox, style, LayerSettings.routeBelow)
                    trafficManager = SymbolManager(mapView, mapbox, style).apply {
                        textKeepUpright = true
                        iconIgnorePlacement = true
                        textAllowOverlap = true
                        textVariableAnchor = arrayOf("left", "right", "top", "bottom")
                    }
                    TrackSource.TrackKey.values().forEach {
                        trackLineManagers[it] = LineManager(mapView, mapbox, style, LayerSettings.routeBelow)
                    }
                    mapbox.addOnCameraMoveStartedListener {
                        cameraMoving = it == REASON_API_GESTURE
                        cameraMoved(mapbox.cameraPosition)
                    }
                    mapbox.addOnCameraMoveListener {
                        cameraMoved(mapbox.cameraPosition)
                    }
                    mapbox.addOnCameraIdleListener {
                        cameraMoving = false
                    }
                    mapbox.addOnFlingListener {
                        cameraMoved(mapbox.cameraPosition)
                    }
                    mapbox.addOnShoveListener(object : MapboxMap.OnShoveListener {
                        override fun onShove(detector: ShoveGestureDetector) {
                        }

                        override fun onShoveBegin(detector: ShoveGestureDetector) {
                        }

                        override fun onShoveEnd(detector: ShoveGestureDetector) {
                            cameraMoved(mapbox.cameraPosition)
                        }
                    })
                    locationLayer = mapbox.locationComponent
                    val options = LocationComponentOptions.builder(requireActivity())
                        .foregroundDrawable(R.drawable.flight_marker)
                        .gpsDrawable(R.drawable.flight_marker)
                        .backgroundDrawable(R.drawable.dot)
                        .accuracyAlpha(1.0f)
                        .trackingInitialMoveThreshold((mapView.width / 4).toFloat())
                        .build()
                    with(locationLayer) {
                        activateLocationComponent(
                            LocationComponentActivationOptions.builder(requireActivity(), mapStyle)
                                .useDefaultLocationEngine(false)
                                .build()
                        )
                        isLocationComponentEnabled = true
                        applyStyle(options)
                        renderMode = RenderMode.GPS
                        cameraMode = CameraMode.NONE
                    }
                    with(mapbox.uiSettings) {
                        setCompassFadeFacingNorth(false)
                        isRotateGesturesEnabled = true
                        isDisableRotateWhenScaling = true
                        isTiltGesturesEnabled = true
                    }
                    ScaleBarPlugin(mapView, mapbox).create(requireContext()) {
                        isCentered = true
                        textSize = dpToPixels(12.0)
                        textBarMargin = dpToPixels(4.0)
                        barHeight = dpToPixels(4.0)
                        borderWidth = dpToPixels(1.0)
                        marginTop = dpToPixels(12.0)
                        widthRatio = 0.7
                        unitSet = ScaleBarUnit.nautical
                        refreshInterval = 15
                    }
                    e.onSuccess(mapbox)
                }
            }
        }
            .cache()
            .takeUntil { destroyed }
            .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Show a line on the map. Remove any line with the same key first.
     */
    private fun showLine(track: RouteMapViewModel.ShowLineEvent) {
        trackLineManagers[track.key]?.let { manager ->
            if (track.points.isEmpty()) {
                manager.deleteAll()
                return
            }
            val options = LineOptions()
                .withLatLngs(track.points.map { it.asLatLng() })
                .withLineWidth(track.width.toFloat())
                .withLineColor(ColorUtils.colorToRgbaString(track.color))
            lineMap.put(track.key, manager.create(options))?.let {
                manager.delete(it)
            }
        }
    }

    private fun processRouteEvent(map: MapboxMap, event: RouteMapViewModel.RouteMapViewEvent) {
        when (event) {
            is RouteMapViewModel.ShowLineEvent -> showLine(event)
            is RouteMapViewModel.ShowRouteEvent -> {
                val route = event.route
                logMsg("new route = $route")
                routeLineManager.deleteAll()
                val list = ArrayList<LineOptions>()
                route.points.forEachIndexed { index, routePoint ->
                    if (index != 0) {
                        val line = LineOptions()
                            .withLatLngs(listOf(route.points[index - 1].asLatLng(), routePoint.asLatLng()))
                            .withLineWidth(4f)
                            .withLineColor(
                                ColorUtils.colorToRgbaString(
                                    if (index == route.active) Color.MAGENTA else Color.WHITE
                                )
                            )
                        list.add(line)
                    }
                }
                routeLineManager.create(list)
            }

            is RouteMapViewModel.RemoveSourceEvent -> {
                map.style?.getSource(event.name)?.let {
                    map.style?.removeSource(it)
                }
            }

            is RouteMapViewModel.RemoveLayerEvent -> {
                map.style?.getLayer(event.name)?.let { layer ->
                    map.style?.removeLayer(layer)
                }
            }

            is RouteMapViewModel.SetLayerOpacityEvent -> {
                when (val layer = map.style?.getLayer(event.name)) {
                    is RasterLayer -> layer.setProperties(rasterOpacity(event.opacity.toFloat()))
                    is FillLayer -> layer.setProperties(fillOpacity(event.opacity.toFloat()))
                }
            }

            is RouteMapViewModel.AddFillLayerEvent -> {
                map.style?.let { style ->
                    if (style.getLayer(event.name) == null) {
                        val layer = FillLayer(event.name, event.source)
                        layer.sourceLayer = event.sourceLayer
                        val properties = mutableListOf<PropertyValue<*>>()
                        if (event.parameter.isNotBlank() && event.fillColors.isNotEmpty()) {
                            properties.add(
                                fillColor(
                                    Expression.interpolate(
                                        Expression.linear(), Expression.get(event.parameter),
                                        *event.fillColors.map {
                                            Expression.stop(
                                                it.key,
                                                Expression.color(it.value or 0xF0000000.toInt())
                                            )
                                        }.toTypedArray()
                                    )
                                )
                            )
                            properties.add(
                                fillOutlineColor(
                                    Expression.interpolate(
                                        Expression.linear(), Expression.get(event.parameter),
                                        *event.fillColors.map {
                                            Expression.stop(
                                                it.key,
                                                Expression.color(it.value or 0x80000000.toInt())
                                            )
                                        }.toTypedArray()
                                    )
                                )
                            )
                        }
                        layer.setProperties(visibility(Property.VISIBLE), *properties.toTypedArray())
                        layer.fillOpacityTransition = TransitionOptions(0, 0)
                        layer.setProperties(fillOpacity(1f))
                        if (event.below.isNotBlank())
                            style.addLayerBelow(layer, event.below)
                        else
                            style.addLayer(layer)
                    }
                }
            }

            is RouteMapViewModel.AddRadarSourceEvent -> {
                map.style?.let { style ->
                    val tsource = event.source
                    val tileSet = TileSet("2.2.0", *tsource.tiles.toTypedArray())
                    tileSet.maxZoom = tsource.maxZoom.toFloat()
                    tileSet.minZoom = tsource.minZoom.toFloat()
                    val source = RasterSource(tsource.name, tileSet)
                    style.addSource(source)
                }
            }

            is RouteMapViewModel.AddSourceEvent -> {
                map.style?.let {
                    if (it.getSource(event.name) == null)
                        if (event.vector)
                            it.addSource(VectorSource(event.name, event.url))
                        else {
                            it.addSource(RasterSource(event.name, event.url))
                        }
                }
            }

            is RouteMapViewModel.AddRasterLayerEvent -> {
                map.style?.let { style ->
                    if (style.getLayer(event.name) == null) {
                        val layer = RasterLayer(event.name, event.source)
                        if (event.below.isNotBlank())
                            style.addLayerBelow(layer, event.below)
                        else
                            style.addLayer(layer)
                        layer.setProperties(visibility(Property.VISIBLE), rasterOpacity(1.0f))
                    }
                }
            }

            is RouteMapViewModel.AddTilesEvent -> addTiles(map, event)

            is RouteMapViewModel.ShowLayerEvent -> {
                map.style?.getLayer(event.layer)?.setProperties(
                    visibility(if (event.visible) Property.VISIBLE else Property.NONE)
                )

            }

            else -> logMsg("Unknown route map event $event")
        }
    }

    private fun addTiles(map: MapboxMap, event: RouteMapViewModel.AddTilesEvent) {
        map.style?.let { style ->
            if (style.getSource(event.name) == null) {
                val tsource = event.source
                val tileSet = TileSet("2.2.0", *tsource.tiles.toTypedArray())
                tileSet.maxZoom = tsource.maxZoom.toFloat()
                tileSet.minZoom = tsource.minZoom.toFloat()
                val source = VectorSource(event.name, tileSet)
                style.addSource(source)
            }
        }
    }

    private fun MapboxMap.moveCamera(position: CameraUpdate, duration: Duration) {
        if (duration.isZero)
            moveCamera(position)
        else
            easeCamera(position, duration.toMillis().toInt(), false)
    }

    private fun processMapEvent(mapbox: MapboxMap, event: MapViewModel.MapViewEvent) {

        when (event) {
            is MapViewModel.SetNoteTextEvent -> view?.findViewById<TextView>(R.id.mapViewNote)?.text = event.text
            is MapViewModel.BoundsEvent -> {
                val bounds = LatLngBounds.Builder()
                    .include(LatLng(event.bounds.south, event.bounds.west))
                    .include(LatLng(event.bounds.north, event.bounds.east))
                    .build()
                mapbox.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20), event.animation)
            }

            is MapViewModel.OffsetEvent -> {
                val builder = CameraPosition.Builder(mapbox.cameraPosition)
                    .padding((mapView.width * event.left), (mapView.height * event.top), 0.0, 0.0)
                mapbox.moveCamera(CameraUpdateFactory.newCameraPosition(builder.build()))
            }

            is MapViewModel.CenterEvent -> {

                val builder = CameraPosition.Builder(mapbox.cameraPosition)
                if (event.direction != INVALID_DATA)
                    builder.bearing(event.direction)
                if (event.center.valid)
                    builder.target(event.center.asLatLng())
                if (event.zoom != INVALID_DATA)
                    builder.zoom(event.zoom)
                if (event.pitch != INVALID_DATA)
                    builder.tilt(event.pitch)
                val newPos = builder.build()
                mapbox.moveCamera(CameraUpdateFactory.newCameraPosition(newPos), event.animation)
            }

            is MapViewModel.MarkerPositionEvent -> {
                val androidLocation = event.location.toAndroid()
                androidLocation.altitude = event.location.altitude
                androidLocation.bearing = if (event.location.heading >= -180.0)
                    event.location.heading.toFloat()
                else
                    locationLayer.lastKnownLocation?.bearing ?: 0f
                locationLayer.forceLocationUpdate(LocationUpdate.Builder().location(androidLocation).build())
            }

            is MapViewModel.MenuEvent -> {
                activity?.let {
                    AndroidMenu.showMenu(it, event.title, event.entries, event.where as View)
                }
            }

            else -> logMsg("Unknown map event $event")
        }
    }

    /**
     * Creates the fragment view hierarchy.
     *
     * @param inflater           Inflater used to inflate content.
     * @param container          The parent layout for the map fragment.
     * @param savedInstanceState The saved instance state for the map fragment.
     * @return The view created
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        HttpRequestUtil.setOkHttpClient(Skysight.httpClient)
        return inflater.inflate(R.layout.fragment_routemap, container, false)
    }

    /**
     * Called when the fragment view hierarchy is created.
     *
     * @param view               The content view of the fragment
     * @param savedInstanceState THe saved instance state of the fragment
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapView = view.findViewById(R.id.mapbox)
        val layerButton: ImageButton = view.findViewById(R.id.layerButton)
        val mapViewButton: ImageButton = view.findViewById(R.id.mapviewButton)
        mapViewButton.setOnClickListener { presenter.onLocationClick(mapViewButton) }
        layerButton.setOnClickListener { presenter.onLayerClick() }
        mapboxMap = getMapboxSingle()
        mapView.onCreate(savedInstanceState)
    }

    /**
     * Called when the fragment is visible for the users.
     */
    override fun onStart() {
        super.onStart()
        mapView.onStart()
        disposable = mapboxMap
            .flatMapObservable { mapbox: MapboxMap ->
                presenter.observable.map { Pair(mapbox, it) }
            }
            .subscribe(
                { (mapbox, event) ->
                    when (event) {
                        is RouteMapViewModel.TrafficEvent -> processTraffic(mapbox, event)
                        is RouteMapViewModel.RouteMapViewEvent -> processRouteEvent(mapbox, event)
                        else -> processMapEvent(mapbox, event)
                    }
                }, CJLog::logException
            )
    }

    private fun processTraffic(mapbox: MapboxMap, event: RouteMapViewModel.TrafficEvent) {
        // first remove any traffic no longer there
        val newTrafficSet = event.targets.associateBy { it.address }
        val delList = trafficMap.filterNot { newTrafficSet.contains(it.key) }
        trafficManager.delete(delList.values.toList())
        delList.forEach { trafficMap.remove(it.key) }
        // add or update traffic
        newTrafficSet.values.forEach {
            trafficMap.getOrPut(it.address) {
                trafficManager.create(
                    SymbolOptions()
                        .withGeometry(Point.fromLngLat(it.longitude, it.latitude))
                        .withIconImage(RouteMapViewModel.TrafficIcon.Normal.name)
                        .withTextJustify("auto")
                        .withTextOffset(arrayOf(1.0f, 0.0f))
                        .withTextColor("black")
                )
            }.apply {
                iconRotate = (it.track - mapbox.cameraPosition.bearing).toFloat()
                iconImage = when {
                    it.isHazard -> RouteMapViewModel.TrafficIcon.Alert
                    it.isProximate -> RouteMapViewModel.TrafficIcon.Proximate
                    else -> RouteMapViewModel.TrafficIcon.Normal
                }.name
                textField = it.shortString
                latLng = it.asLatLng()
            }
            trafficManager.update(trafficMap.values.toList())
        }
    }

    /**
     * Called when the fragment is ready to be interacted with.
     */
    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    /**
     * Called when the fragment is pausing.
     */
    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    /**
     * Called when the fragment state needs to be saved.
     *
     * @param outState The saved state
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    /**
     * Called when the fragment is no longer visible for the user.
     */
    override fun onStop() {
        presenter.onStop()
        disposable.dispose()
        if (::locationLayer.isInitialized)
            locationLayer.onStop()
        mapView.onStop()
        super.onStop()
    }

    /**
     * Called when the fragment receives onLowMemory call from the hosting Activity.
     */
    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    /**
     * Called when the fragment is view hierarchy is being destroyed.
     */
    override fun onDestroyView() {
        mapView.onDestroy()
        super.onDestroyView()
    }

}

fun Position.toAndroid(): android.location.Location {
    val location = android.location.Location("position")
    location.latitude = latitude
    location.longitude = longitude
    return location
}

fun Position.asLatLng(): LatLng {
    return LatLng(latitude, longitude)
}

fun LatLng?.toPosition(): Position {
    if (this == null)
        return Position.invalid
    return Position.of(latitude, longitude)
}
