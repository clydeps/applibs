package com.controlj.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.controlj.Utilities.R
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.utility.HttpConnector
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * A Fragment that can display an image which can be panned and zoomed by the user.
 */
class ImageZoomFragment : Fragment() {

    companion object {
        val REFRESH_INTERVAL = 5L * 60 * 1000
        val BUNDLE_KEY = "zoomkey"
    }

    var uri: String? = null
    var disposable = Disposable.disposed()

    private fun constructFileName(): File? {
        if (uri == null)
            return null
        val url = uri?.toHttpUrlOrNull()
        if (url == null)
            return null
        val path = url.encodedPath.replace("/", "_")
        return File(activity?.cacheDir, path)
    }

    private fun updateImage() {
        if (uri == null)
            return
        val file = constructFileName() ?: return
        Single.create<ImageSource> { e ->
            if (!(file.exists() && file.lastModified() > System.currentTimeMillis() + REFRESH_INTERVAL)) {
                val connector = HttpConnector()
                connector.setUrl(uri)
                connector.getRequest()
                val mime = connector.getHeaderField("Content-Type")
                if (!mime.startsWith("image")) {
                    connector.disconnect()
                    logMsg("Unexpected mime-type for image")
                    e.onError(Throwable("Not an image file"))
                }
                connector.copyToFile(file)
                connector.disconnect()
            }
            e.onSuccess(ImageSource.uri(file.path).tilingDisabled())
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { source -> view?.findViewById<SubsamplingScaleImageView>(R.id.image_view)?.setImage(source) },
                { t -> logException(t) })
    }

    override fun onStart() {
        super.onStart()
        //image_view.setDebug(true)
        updateImage()
        disposable = Observable.interval(REFRESH_INTERVAL, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .subscribe { updateImage() }
    }

    override fun onStop() {
        disposable.dispose()
        super.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (uri != null)
            outState.putString(BUNDLE_KEY, uri)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_image_zoom, container, false)
        if (uri == null)
            uri = savedInstanceState?.getString(BUNDLE_KEY)
        return view
    }
}
