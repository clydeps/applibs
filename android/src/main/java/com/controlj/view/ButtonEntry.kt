package com.controlj.view

import android.content.Context
import android.view.ViewGroup
import android.widget.Button
import com.controlj.ui.ButtonDialogItem
import com.controlj.ui.DialogData

class ButtonEntry(context: Context, item: ButtonDialogItem, dialogData: DialogData) : DialogEntry(context, item, dialogData) {

    override val view: Button = Button(context).also {
        it.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )
        it.setOnClickListener {
            dialogData.onItemPressed(item)
        }
    }

    override fun refresh(): Boolean {
        if(view.text.toString() != item.text) {
            view.text = item.text
            super.refresh()
            return true
        }
        return super.refresh()
    }
}
