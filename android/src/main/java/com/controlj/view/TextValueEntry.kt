package com.controlj.view

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import com.controlj.Utilities.R
import com.controlj.setBackgroundTint
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.ui.TextValueItem


class TextValueEntry(
        context: Context,
        override val item: TextValueItem,
        dialogData: DialogData
) : LabelledEntry(context, item, dialogData) {

    var passwordVisible = false
    private val editText: EditText = EditText(context).apply {
        gravity = Gravity.END
        setBackgroundTint(0.9)
        setPadding(padding, padding/2, padding, padding/2)
        textAlignment = if (item.inputStyle.rightAlign)
            View.TEXT_ALIGNMENT_TEXT_END
        else
            View.TEXT_ALIGNMENT_TEXT_START
        layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f)
        hint = item.hint
        contentView.addView(this)
        if (item.inputStyle == DialogItem.InputStyle.PASSWORD) {
            val eye = LayoutInflater.from(context).inflate(R.layout.password_eye, contentView, false)
            contentView.addView(eye)
            eye.setOnClickListener {
                passwordVisible = !passwordVisible
                watcher.setPasswordStyle(passwordVisible)
            }
        } else if(item.lines > 1) {
            minLines = item.lines
            maxLines = item.lines
        }
    }

    val watcher = TextDelegate(editText, item.inputStyle) { _, text ->
        item.value = text
        item.onChanged()
        dialogData.refresh(item)
    }

    override fun refresh(): Boolean {
        if (watcher.setText(item.value)) {
            super.refresh()
            return true
        }
        return super.refresh()
    }
}
