package com.controlj.view

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.controlj.Utilities.R
import com.controlj.settings.Setting
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.viewmodel.DialogListModel

/**
 * Created by clyde on 21/8/18.
 */
abstract class DialogDataFragment<T : DialogListModel> : ListFragment<DialogItem, T>(), DialogData.DialogShower {
    override lateinit var presenter: T
    private var labelWidth = 0

    override fun getRowLayout(parent: ViewGroup, viewType: Int): View {
        return inflater.inflate(
            when (viewType) {
                ListPresenter.ViewType.HEADER.ordinal -> R.layout.list_header
                else -> R.layout.list_entry_container
            }, parent, false
        )
    }

    override fun bindItem(view: View, item: DialogItem, textValues: List<String>) {
        val entry = DialogEntry.create(requireActivity(), item, presenter.dialogData)
        if (labelWidth != 0 && entry is LabelledEntry) {
            entry.label.layoutParams.width = labelWidth
        }
        view.tag = entry
        entry.view.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        view.findViewById<ViewGroup>(R.id.container)?.apply {
            removeAllViews()
            addView(entry.view)
        }
        entry.refresh()
    }

    override fun bindHeader(view: View, section: Int, textValues: List<String>) {
        view.findViewById<TextView>(R.id.headertext).text = textValues.first()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.resources?.displayMetrics?.let { metrics ->
            labelWidth = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, presenter.dialogData.labelWidth.toFloat(), metrics
            ).toInt()
        }
        toolBar.setOnClickListener {
            Setting.shakeSauceBottle()
        }
        // hide the keyboard when a non-editable item gains focus
        view.setOnTouchListener { v, _ ->
            if (!(v is EditText)) {
                val token = activity?.currentFocus?.windowToken
                if (token != null)
                    (activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager)
                        ?.hideSoftInputFromWindow(token, 0)
            }
            false
        }
    }

    override fun connectPresenter() {
        super.connectPresenter()
        presenter.dialogData.shower = this
        presenter.dialogData.refresh()
    }

    override fun onStop() {
        super.onStop()
        presenter.dialogData.shower = null
    }

    override fun refresh(source: DialogItem?) {
        toolBar.title = presenter.dialogData.title
        val first = layoutManager.findFirstVisibleItemPosition()
        val last = layoutManager.findLastVisibleItemPosition()
        (first..last).filter {
            val entry = (recyclerView.findViewHolderForAdapterPosition(it)?.itemView?.tag as? DialogEntry?)
            source != entry?.item && entry?.refresh() == true
        }.forEach {
            adapter.notifyItemChanged(it)
        }
    }

    override fun dismiss() {
        activity?.onBackPressed()
    }
}
