package com.controlj.view

import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.controlj.graphics.TextStyle
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem

open class LabelledEntry(
        context: Context,
        item: DialogItem,
        dialogData: DialogData
) : BasicEntry(context, item, dialogData) {

    open val label = TextView(context).also {
        it.layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )

        val textStyle = item.textStyle
        it.textSize = textStyle.size.toFloat()
        it.setTypeface(it.typeface,
                when (textStyle.fontStyle) {
                    TextStyle.FontStyle.Bold -> Typeface.BOLD
                    TextStyle.FontStyle.Italic -> Typeface.ITALIC
                    TextStyle.FontStyle.BoldItalic -> Typeface.BOLD_ITALIC
                    else -> Typeface.NORMAL
                })
        it.setPadding(padding, 0, padding, 0)
        contentView.addView(it)
        // add a spacer with small but non-zero weight to soak up any spare space, so WRAP_CONTENT width widgets
        // will float to the end
        val spacer = View(context)
        spacer.layoutParams = LinearLayout.LayoutParams(0, 0, .01f)
        contentView.addView(spacer)
    }


    override fun refresh(): Boolean {
        if (label.text.toString() != item.text) {
            label.text = item.text
            super.refresh()
            return true
        }
        return super.refresh()
    }
}
