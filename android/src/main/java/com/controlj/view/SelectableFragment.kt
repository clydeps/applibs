package com.controlj.view

import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Maybe

/**
 * Created by clyde on 13/8/18.
 */
abstract class SelectableFragment : DisposingFragment() {

    protected var optionsMenu: Menu? = null
    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        optionsMenu = menu
    }

    var isSelected: Boolean = false
    open fun onReselected() {
        isSelected = true
    }

    open fun onSelected() {
        isSelected = true
    }

    open fun onDeselected() {
        isSelected = false
    }

    val supportActionBar: ActionBar?
        get() {
            if (activity is AppCompatActivity)
                return (activity as AppCompatActivity).supportActionBar
            return null
        }

    fun setEnabled(item: MenuItem?, enabled: Boolean) {
        item?.let {
            it.isEnabled = enabled
            it.icon?.alpha = if (enabled) 255 else 80
        }
    }
    protected fun setEnabled(id: Int, enable: Boolean) {
        setEnabled(optionsMenu?.findItem(id), enable)
    }


    fun setEnabled(v: View, enabled: Boolean) {
        v.isEnabled = enabled
        v.alpha = if (enabled) 1f else 0.6f
    }

    fun ask(question: String, yes: String, no: String = "", maybe: String = ""): Maybe<Boolean> {
        val obs = Maybe.create<Boolean> { e ->

            val builder = AlertDialog.Builder(context!!)
            builder.setMessage(question).setPositiveButton(yes) { dialog, _ ->
                e.onSuccess(true)
                dialog.dismiss()
            }
            if (!maybe.isBlank()) {
                builder.setNeutralButton(maybe) { dialog, _ ->
                    dialog.dismiss()
                    e.onComplete()
                }
                builder.setCancelable(false)
            }
            if (!no.isBlank()) {
                builder.setNegativeButton(no) { dialog, _ ->
                    e.onSuccess(false)
                    dialog.dismiss()
                }
                builder.setCancelable(false)
            }
            builder.show()
        }
        return obs.observeOn(AndroidSchedulers.mainThread())
    }

    fun popFragment() {
        activity?.supportFragmentManager?.popBackStack()
    }
}
