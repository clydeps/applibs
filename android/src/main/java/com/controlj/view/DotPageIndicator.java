package com.controlj.view;

/**
 * Created by clyde on 20/06/2016.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.controlj.Utilities.R;

public class DotPageIndicator extends View implements ViewPager.OnPageChangeListener {

	static final int DEFAULT_DOTS = 40;
	int dotSpacing = 12;
	Drawable bigDot, smallDot;
	ViewPager pager;
	private int width;

	public void setPager(ViewPager pager) {
		if(this.pager != null)
			this.pager.removeOnPageChangeListener(this);
		this.pager = pager;
		pager.addOnPageChangeListener(this);
		invalidate();
	}

	public DotPageIndicator(Context context) {
		super(context);
		init();
	}

	private void init() {
		bigDot =  ResourcesCompat.getDrawable(getResources(), R.drawable.dot_big, null);
		smallDot =  ResourcesCompat.getDrawable(getResources(), R.drawable.dot_small, null);
	}

	public DotPageIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public DotPageIndicator(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int activeDot;
		int totalNoOfDots;
		if(pager != null) {
			totalNoOfDots = pager.getAdapter().getCount();
			activeDot = pager.getCurrentItem();
			int start = (width - dotSpacing * totalNoOfDots) / 2;
			for(int i = 0; i != totalNoOfDots; i++) {
				Drawable sd = i == activeDot ? bigDot : smallDot;
				sd.setBounds(start, 0, start + sd.getIntrinsicWidth(), sd.getIntrinsicHeight());
				start += dotSpacing;
				sd.draw(canvas);
			}
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		width = w;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = DEFAULT_DOTS * (bigDot.getIntrinsicWidth() + dotSpacing);
		width = resolveSize(width, widthMeasureSpec);
		int height = bigDot.getIntrinsicHeight();
		height = resolveSize(height, heightMeasureSpec);
		setMeasuredDimension(width, height);
	}

	public void setDotSpacing(int dotSpacing) {
		this.dotSpacing = dotSpacing;
		invalidate();
	}
	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
	}

	@Override
	public void onPageSelected(int position) {
		invalidate();
	}

	@Override
	public void onPageScrollStateChanged(int state) {

	}

}


