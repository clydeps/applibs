package com.controlj.view

import android.content.Context
import android.view.Gravity
import android.widget.CheckBox
import android.widget.LinearLayout
import com.controlj.ui.CheckboxItem
import com.controlj.ui.DialogData

class CheckboxEntry(
        context: Context,
        override val item: CheckboxItem,
        dialogData: DialogData
) : LabelledEntry(context, item, dialogData) {
    val checkbox = CheckBox(context).also {
        it.gravity = Gravity.END
        it.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ).also {
            it.gravity = Gravity.END
        }
        it.setOnCheckedChangeListener { _, checked ->
            if(item.isChecked != checked) {
                item.isChecked = checked
                dialogData.refresh()
            }
        }
        contentView.addView(it)
    }

    override fun refresh(): Boolean {
        checkbox.isEnabled = item.isEnabled
        if(checkbox.isChecked != item.isChecked) {
            checkbox.isChecked = item.isChecked
            checkbox.jumpDrawablesToCurrentState()
            super.refresh()
            return true
        }
        return super.refresh()
    }
}
