/* Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.controlj.billing;

import java.util.*;

/**
 * Represents a block of information about in-app items.
 * An Inventory is returned by such methods as {@link IabHelper#queryInventory}.
 */
public class Inventory {
    Map<String,SkuDetails> skus = new HashMap<String,SkuDetails>();
    Map<String,Purchase> purchases = new HashMap<String,Purchase>();

    public Inventory() { }

    /** Returns the listing details for an in-app product. */
    public SkuDetails getSkuDetails(String sku) {
        return skus.get(sku);
    }

    /** Returns purchase information for a given product, or null if there is no purchase. */
    public Purchase getPurchase(String sku) {
        return purchases.get(sku);
    }

    /** Returns whether or not there exists a purchase of the given product. */
    public boolean hasPurchase(String sku) {
        return purchases.containsKey(sku);
    }

    /** Return whether or not details about the given product are available. */
    public boolean hasDetails(String sku) {
        return skus.containsKey(sku);
    }

    /**
     * Erase a purchase (locally) from the inventory, given its product ID. This just
     * modifies the Inventory object locally and has no effect on the server! This is
     * useful when you have an existing Inventory object which you know to be up to date,
     * and you have just consumed an item successfully, which means that erasing its
     * purchase data from the Inventory you already have is quicker than querying for
     * a new Inventory.
     */
    public void erasePurchase(String sku) {
        purchases.remove(sku);
    }

    /** Returns a list of all owned product IDs. */
    public List<String> getAllOwnedSkus() {
        return new ArrayList<String>(purchases.keySet());
    }

    /** Returns a list of all owned product IDs of a given type */
    public List<String> getAllOwnedSkus(String itemType) {
        List<String> result = new ArrayList<String>();
        for (Purchase p : purchases.values()) {
            if (p.getItemType().equals(itemType)) result.add(p.getProductId());
        }
        return result;
    }

	public Collection<SkuDetails> getSkus() {
		return skus.values();
	}

	public Collection<Purchase> getPurchases() {
		return purchases.values();
	}

	public void setSkus(List<SkuDetails> details) {
		for(SkuDetails skuDetails : details)
			addSkuDetails(skuDetails);
	}

	public void setPurchases(List<Purchase> purches) {
		for(Purchase p : purches)
			addPurchase(p);
	}

    /** Returns a list of all purchases. */
    public List<Purchase> getAllPurchases() {
        return new ArrayList<Purchase>(purchases.values());
    }

    void addSkuDetails(SkuDetails d) {
        skus.put(d.getProductId(), d);
    }

    void addPurchase(Purchase p) {
        purchases.put(p.getProductId(), p);
    }
}
