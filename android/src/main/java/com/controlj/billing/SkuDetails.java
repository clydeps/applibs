/* Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.controlj.billing;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents an in-app product's listing details.
 */
public class SkuDetails {
	String itemType;
    String productId;
    String type;
    String price;
    String title;
    String description;

	String currency;
	String mJson;

	public String getCurrency() {
		return currency;
	}

	public String getItemType() {
		return itemType;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public SkuDetails() {
	}

	public SkuDetails(String jsonSkuDetails) throws JSONException {
        this(IabHelper.ITEM_TYPE_INAPP, jsonSkuDetails);
    }

    public SkuDetails(String itemType, String jsonSkuDetails) throws JSONException {
        this.itemType = itemType;
        mJson = jsonSkuDetails;
        JSONObject o = new JSONObject(mJson);
        productId = o.optString("productId");
        type = o.optString("type");
        price = o.optString("price");
        title = o.optString("title");
        description = o.optString("description");
		currency = o.optString("price_currency_code");
    }

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProductId() { return productId; }
    public String getType() { return type; }
    public String getPrice() { return price; }
    public String getTitle() { return title; }
    public String getDescription() { return description; }

    @Override
    public String toString() {
        return "SkuDetails:" + mJson;
    }
}
