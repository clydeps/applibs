package com.controlj.ui

import android.annotation.TargetApi
import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_MUTABLE
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.controlj.data.Message
import com.controlj.data.Priority
import com.controlj.framework.ApplicationState

class AndroidNotificationPresenter(
    private val context: Context,
    private val icon: Int,
    private val activityClass: Class<out Activity>
) : NotificationPresenter {
    private val notificationManager = NotificationManagerCompat.from(context)

    // map our priorities to Android's
    private val priorities = Priority.values().map {
        it to when (it) {
            Priority.ALARM, Priority.ERROR, Priority.ACTION, Priority.CAUTION -> NotificationManagerCompat.IMPORTANCE_MAX
            else -> NotificationManagerCompat.IMPORTANCE_DEFAULT
        }
    }.toMap()

    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    @TargetApi(Build.VERSION_CODES.O)
    private val channels: Map<Priority, String> = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val attributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
        Priority.values().filter { it.description.isNotBlank() }.map {
            val channel = NotificationChannel(
                it.name,
                it.description + " messages",
                priorities.getValue(it)
            )
            if (it.sound.isNotBlank())
                channel.setSound(Uri.parse("android.resource://${context.packageName}/raw/${it.sound}"), attributes)
            notificationManager.createNotificationChannel(channel)
            it to channel.id
        }.toMap()
    } else mapOf()

    init {
        NotificationPresenter.instance = this
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun sendNotifications(priority: Priority, msgs: List<Message>) {
        if (msgs.isNotEmpty()) {
            val flags = if(Build.VERSION.SDK_INT >= 26) FLAG_MUTABLE else 0
            val intent = Intent(context, activityClass)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, flags)
            val first = msgs.first()
            val channel = channels[priority] ?: ""
            val builder = NotificationCompat.Builder(context, channel)
                .setContentTitle(first.toString())
                .setContentText(msgs.drop(1).takeLast(5).joinToString("\n") { it.toString() })
                .setSmallIcon(icon)
                .setContentIntent(pendingIntent)
                .setOnlyAlertOnce(true)
                .setAutoCancel(true)
            if (first.priority.sound.isNotBlank())
                builder.setSound(Uri.parse("android.resource://${context.packageName}/raw/${first.priority.sound}"))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                builder.priority = priorities.getValue(priority)
            notificationManager.notify(priority.ordinal, builder.build())
        } else
            notificationManager.cancel(priority.ordinal)

    }

    /**
     * Update the notifications, grouped by priority. Cancel all notifications if the app is active
     */
    override fun updateNotifications() {
        Priority.values().forEach { priority ->
            sendNotifications(priority, NotificationPresenter.messageList.filter {
                !ApplicationState.current.data.active && it.priority == priority
            })
        }
    }
}
