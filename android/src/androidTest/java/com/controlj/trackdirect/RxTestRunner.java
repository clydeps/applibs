package com.controlj.trackdirect;

import androidx.test.runner.AndroidJUnitRunner;

import com.squareup.rx2.idler.Rx2Idler;

import io.reactivex.plugins.RxJavaPlugins;

/**
 * Created by clyde on 20/11/17.
 */

public class RxTestRunner extends AndroidJUnitRunner {
    @Override
    public void onStart() {
        RxJavaPlugins.setInitComputationSchedulerHandler(
            Rx2Idler.create("RxJava 2.x Computation Scheduler"));
        RxJavaPlugins.setInitSingleSchedulerHandler(
            Rx2Idler.create("RxJava 2.x Single Scheduler"));
        RxJavaPlugins.setInitIoSchedulerHandler(
            Rx2Idler.create("RxJava 2.x IO Scheduler"));

        super.onStart();
    }
}
