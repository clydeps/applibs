import org.jetbrains.kotlin.gradle.internal.Kapt3GradleSubplugin.Companion.findKaptConfiguration

plugins {
    id("com.android.library")
    kotlin("android")
    id("org.jetbrains.dokka")
}

val buildNumber:Int = project.buildNumber
val appVersion:String = project.appVersion
android {
    defaultConfig {
        compileSdk = AndroidApp.targetSdkVersion
        targetSdk = AndroidApp.targetSdkVersion
        minSdk = AndroidApp.minSdkVersion

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        multiDexEnabled = true
    }

    packagingOptions {
        resources.excludes.add("META-INF/DEPENDENCIES")
        resources.excludes.add("META-INF/LICENSE")
        resources.excludes.add("META-INF/LICENSE.txt")
        resources.excludes.add("META-INF/license.txt")
        resources.excludes.add("META-INF/NOTICE")
        resources.excludes.add("META-INF/NOTICE.txt")
        resources.excludes.add("META-INF/notice.txt")
        resources.excludes.add("META-INF/ASL2.0")
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    sourceSets {
        getByName("main").java.srcDirs("src/main/java")
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("test").java.srcDirs("src/test/java")
    }
    testOptions {
        unitTests.isIncludeAndroidResources = true
    }
    ndkVersion = "22.0.7026061"

    publishing {
        singleVariant("release") {
            withSourcesJar()
            //withJavadocJar()
        }
    }
}


dependencies {
    implementation(project(":common"))

    listOf(
        ThreetenAbp,
        AndroidXAnnotation,
        AndroidXAppcompat,
        AndroidXMultidex,
        Rxjava3,
        Rxjava3Android,
        Rxjava3Kotlin,
        AndroidMapboxSdk,
        AndroidMapboxBuildings,
        AndroidMapboxAnnotations,
        AndroidMapboxScaleBar,
        Material,
        MaterialDrawer,
        ConstraintLayout,
        RecyclerView,
        SqlDroid,
        Kotlin,
        KotlinReflect,
        OkHttp,
        KotlinStdlib,
        Retrofit,
        RetrofitRxJava3,
        AndroidXWork,
        TbruyelleRxpermissions2,
        AltBeacon,
        ScaleImageView,
        SwipeRefreshLayout,
        Guava,
        CJLog,
        WorkManager,
        WorkManager.kotlin

    ).forEach { implementation(it()) }

    testImplementation(Junit())
    testImplementation(Mockk())
    listOf(
        AndroidxTestCore,
        AndroidxTestCoreKtx,
        AndroidxTestExtJunit,
        AndroidxTestExtTruth,
        EspressoCore,
        EspressoWeb,
        EspressoContrib,
        Roboelectric,
        Junit,
        TestRules,
        Mockk
    )
        .forEach { testImplementation(it()) }
    listOf(
        EspressoCore,
        EspressoWeb,
        EspressoContrib,
        TestRunner,
        AndroidxTestCore,
        AndroidxTestCoreKtx,
        TestRules
    ).forEach { androidTestImplementation(it()) }
    androidTestImplementation("androidx.test.ext:junit:1.1.4")
    androidTestUtil("androidx.test:orchestrator:1.4.2")
    androidTestImplementation("com.squareup.rx.idler:rx2-idler:0.11.0")
}


val dokkaJar by tasks.creating(Jar::class) {
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
    dependsOn(tasks.dokkaHtml)
}

afterEvaluate {
    publishing {
        publications {
            create<MavenPublication>("maven") {
                from(components["release"])
                artifact(dokkaJar)

                groupId = rootProject.group as String
                version = rootProject.version as String
                artifactId = "android"

                pom {
                    name.set("Applibs Android")
                    description.set("Android specific application libraries")
                    licenses {
                        license {
                            name.set("Copyright (C) 2021 Control-J Pty Ltd. All rights reserved")
                        }
                    }
                    developers {
                        developer {
                            id.set("clyde")
                            name.set("Clyde Stubbs")
                        }
                    }
                    scm {
                        connection.set("scm:git:git@gitlab.com:clydeps/applibs.git")
                    }
                }

                versionMapping {
                    usage("java-api") {
                        fromResolutionOf("runtimeClasspath")
                    }
                    usage("java-runtime") {
                        fromResolutionResult()
                    }
                }
            }
        }
    }
}
task<Exec>("asciidoc") {
    commandLine("asciidoc", "${rootProject.projectDir}/asciidoc/about.adoc")
}
