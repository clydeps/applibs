<?
	function db_die($err) {
		global $fp;
		global $dbh;
		fwrite($fp, $err[2] . "\n");
		fclose($fp);
		$dbh->rollBack();
		header("Status: 405 DB error " . $err[2]);
		exit;
	}
	define(KEY, "ababa14e-f843-11e4-ba33-8b1f48b3f879");
	require_once("database.php");
	$fp = fopen("/usr/home/sr22/logs/crashreport.tmp", "w");
	fwrite($fp, print_r($_REQUEST, true));
	fwrite($fp, print_r($_POST, true));
	$fields = array();
	$custom = $_POST['CUSTOM_DATA'];
	$cf = explode("\n", $custom);
	fwrite($fp, print_r($cf, true));
	foreach($cf as $c) {
		$parts = explode('=', $c);
		if(count($parts) == 2)
			$fields[trim($parts[0])] = trim($parts[1]);
	}
	fwrite($fp, print_r($fields, true));
	if(!array_key_exists('stamp', $fields) || !array_key_exists('key', $fields)) {
		header("Status: 403 Forbidden");
		exit;
	}
	$kk = $fields['stamp'] . KEY;
	$hash = hash("sha256", $kk);
	fwrite($fp, "input = $kk, hash = $hash, key = " . $fields['key'] . "\n");
	if(strtolower($hash) != strtolower($fields['key'])) {
		header("Status: 403 Forbidden");
		fwrite($fp, "hash did not match\n");
		fclose($fp);
		exit;
	}
	$data = $dbh->quote($_SERVER["REMOTE_ADDR"]);
	fwrite($fp, "Remote IP = $data\n");
	$res = $dbh->exec("delete from blocked_ips where insert_date < subdate(now(), interval 1 month)");
	$res = $dbh->query("select * from blocked_ips where ipaddr=$data");
	if(!$res || $res->rowCount() != 0) {
		header("Status: 403 Forbidden");
		fwrite($fp, "IP blocked\n");
		fclose($fp);
		exit;
	}
	$dbh->beginTransaction();
	fwrite($fp, "Transaction bgun\n");
	$device = $_POST['PHONE_MODEL'];
	$device = $dbh->quote(trim($device));
	$res = $dbh->exec("insert into crash_reports (id, device) values(null, $device)");
	if(!$res)
		db_die($dbh->errorInfo());
	$id = $dbh->lastInsertId();
	fwrite($fp, "insert id = $id\n");
	foreach($_POST as $key => $data) {
		$key = $dbh->quote(trim($key));
		$data = $dbh->quote(trim($data));
		$res = $dbh->exec("insert into crash_data (report_id, field, data) values($id, $key, $data)");
		if(!$res)
			db_die($dbh->errorInfo());
	}
	$data = $dbh->quote($_SERVER["REMOTE_ADDR"]);
	$key = $dbh->quote("IPADDR");
	$res = $dbh->exec("insert into crash_data (report_id, field, data) values($id, $key, $data)");
	$dbh->commit();
	fwrite($fp, "Done!\n");
?>
