import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */

plugins {
    jacoco
    kotlin("jvm")
    id("org.jetbrains.dokka")
}

dependencies {

    implementation(kotlin("stdlib-jdk8"))

    listOf(
        Rxjava3,
        Rxjava3Kotlin,
        Gson,
        Kotlin,
        KotlinReflect,
        KotlinStdlib,
        OkHttp,
        Retrofit,
        RetrofitRxJava3,
        RetrofitGson,
        Ktorm,
        KtormSqlite,
        Jsoup,
        CJLog,

    ).forEach { implementation(it()) }

    compileOnly(ThreetenBp())

    listOf(
        ThreetenBp,
        Junit,
        Mockk,
        MockServer,
        MockWebserver,
        MockOkHttp,
        Xerial
    ).forEach { testImplementation(it()) }
}

java {
    withSourcesJar()
}


val dokkaJar by tasks.creating(Jar::class) {
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
    dependsOn(tasks.dokkaHtml)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
            artifact(dokkaJar)
            groupId = rootProject.group as String
            version = rootProject.version as String
            artifactId = "common"

            pom {
                name.set("Applibs common")
                description.set("platform independent application libraries")
                licenses {
                    license {
                        name.set("Copyright (C) 2021 Control-J Pty Ltd. All rights reserved")
                    }
                }
                developers {
                    developer {
                        id.set("clyde")
                        name.set("Clyde Stubbs")
                    }
                }
                scm {
                    connection.set("scm:git:git@gitlab.com:clydeps/applibs.git")
                }
            }

            versionMapping {
                usage("java-api") {
                    fromResolutionOf("runtimeClasspath")
                }
                usage("java-runtime") {
                    fromResolutionResult()
                }
            }
        }
    }
}

tasks.test {
    useJUnitPlatform()
}
jacoco {
    toolVersion = "0.8.4"
}

tasks {
    jacocoTestCoverageVerification {
        violationRules {
            rule { limit { minimum = BigDecimal.valueOf(0.2) } }
        }
    }
    check {
        dependsOn(jacocoTestCoverageVerification)
    }
}


//jacocoTestCoverageVerification {
//    violationRules {
//        rule {
//            limit {
//                minimum = 0.25
//            }
//        }
//    }
//}

//check.dependsOn jacocoTestCoverageVerification
//jacocoTestReport.dependsOn test
//jacocoTestCoverageVerification.dependsOn jacocoTestReport

//jacoco {
//    toolVersion = "0.8.4"
//}

//jacocoTestReport {
//    reports {
//        xml.enabled = true
//        csv.enabled = false
//        html.destination file("${buildDir}/jacocoHtml")
//    }
//}

//test {
//    // show standard out and standard error of the test JVM(s) on the console
//    testLogging.showStandardStreams = true
//
//    // set heap size for the test JVM(s)
//    minHeapSize = "128m"
//    maxHeapSize = "512m"
//    forkEvery = 1
//
//    // set JVM arguments for the test JVM(s)
//
//    // listen to events in the test execution lifecycle
//    beforeTest { descriptor ->
//        logger.lifecycle("Running test: " + descriptor)
//    }
//
//    // Fail the "test" task on the first test failure
//
//    workingDir = new File("..")
//    // listen to standard out and standard error of the test JVM(s)
//}

repositories {
    mavenCentral()
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
