package com.controlj.traffic

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.Location
import com.controlj.location.LocationSource
import com.controlj.utility.Units
import com.controlj.utility.instantNow
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import kotlin.math.roundToInt

/**
 * A generic traffic report
 */
interface TrafficTarget : Location, Comparable<TrafficTarget> {
    override fun compareTo(other: TrafficTarget): Int {
        return when {
            distance < other.distance -> -1
            distance > other.distance -> 1
            vDistance < other.vDistance -> -1
            vDistance > other.vDistance -> 1
            else -> 0
        }
    }

    /**
     * The address type.
     */
    enum class AddressType {
        AdsbIcao,
        AdsbSelf,
        TisbIcao,
        TisbFile,
        Surface,
        Beacon,
        Flarm,
        None
    }

    data class EmitterAddress(val type: AddressType, val address: Int) {
        override fun toString(): String {
            return "$type(${address.toString(16).uppercase()})"
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as EmitterAddress

            if (type != other.type) return false
            if (address != other.address) return false

            return true
        }

        override fun hashCode(): Int {
            var result = type.ordinal
            result = 31 * result + address
            return result
        }
    }

    /**
     * What kind of object this represents
     */
    enum class EmitterCategory(val flarmType: Int = 0) {
        None,
        Light(8),
        Small(8),
        Large(9),
        HighVortex(9),
        Heavy(9),
        Fighter(9),
        Rotorcraft(3),
        Unassigned8,
        Glider(1),
        Balloon(0xB),
        Parachute(4),
        HangGlider(6),
        Unassigned13,
        UAV(0xD),
        Spaceship,
        Unassigned16,
        SurfaceEmergency,
        SurfaceService,
        Obstacle(0xF),
        ObstacleCluster(0xF),
        ObstacleLine(0xF)
    }

    // the address - typically represented as 6 hex digits
    val address: EmitterAddress

    // what kind of thing this represents
    val emitterCategory: EmitterCategory

    // climb rate, in m/s
    val verticalSpeed: Double

    // is this an extrapolated position?
    val isExtrapolated: Boolean

    // The callsign of the thing
    val callSign: String

    // rate of turn in degrees/sec
    val turnRate: Double
        get() = 0.0

    // how far away is it in m?
    val distance: Double
        get() = distanceTo(LocationSource.lastLocation)


    // what's the altitude difference in m?
    val vDistance: Double
        get() = LocationSource.lastLocation.altitude - altitude

    // is this considered a proximate target?

    val isProximate: Boolean
        get() = distance < TrafficSettings.proximateDistance.value &&
            vDistance < TrafficSettings.proximateVertical.value

    // is this target a harzard?
    val isHazard: Boolean
        get() = false       // TODO implement some logic here


    // get a compact string representation suitable for display
    val shortString: String
        get() {
            val hundreds: Int = (Units.Unit.FOOT.convertTo(vDistance) / 100).roundToInt()
            val vDirn: Char = when {
                verticalSpeed == INVALID_DATA -> ' '
                verticalSpeed < -0.5 -> '\u25BC'
                verticalSpeed > 0.5 -> '\u25B2'
                else -> ' '
            }
            if (hundreds == 0)
                return "000$vDirn\n$callSign"
            return "%+04d%c\n%s".format(hundreds, vDirn, callSign)
        }

    data class of(
        override val latitude: Double,
        override val longitude: Double,
        override val creator: Location.Creator,
        override val speed: Double,
        override val track: Double,
        override val altitude: Double,
        override val timestamp: Instant,
        override val accuracyH: Double,
        override val accuracyV: Double,
        override val address: EmitterAddress,
        override val emitterCategory: EmitterCategory,
        override val callSign: String,
        override val verticalSpeed: Double,
        override val turnRate: Double = 0.0,
        override val isExtrapolated: Boolean = false
    ) : TrafficTarget {

        override val distance: Double by lazy { distanceTo(LocationSource.lastLocation) }

        override val vDistance: Double by lazy { altitude - LocationSource.lastLocation.altitude }

        override fun equals(other: Any?): Boolean {
            return (other as? TrafficTarget)?.address == address
        }

        override fun hashCode(): Int {
            return address.hashCode()
        }

        override fun toString(): String {
            return "TrafficTarget{${callSign.trimEnd(0.toChar())}:distance=${distance}m, src=${creator.name}}"
        }
    }

    fun from(
        latitude: Double = this.latitude,
        longitude: Double = this.longitude,
        altitude: Double = this.altitude,
        speed: Double = this.speed,
        track: Double = this.track,
        creator: Location.Creator = this.creator,
        timestamp: Instant = this.timestamp,
        accuracyH: Double = this.accuracyH,
        accuracyV: Double = this.accuracyV,
        address: EmitterAddress = this.address,
        emitterCategory: EmitterCategory = this.emitterCategory,
        verticalSpeed: Double = this.verticalSpeed,
        isExtrapolated: Boolean = this.isExtrapolated,
        isInFlight: Boolean = this.isInFlight,
        isHazard: Boolean = this.isHazard,
        callSign: String = this.callSign,
    ): TrafficTarget {
        return of(
            latitude,
            longitude,
            creator,
            speed,
            track,
            altitude,
            timestamp,
            accuracyH,
            accuracyV,
            address,
            emitterCategory,
            callSign,
            verticalSpeed,
            isExtrapolated = isExtrapolated,
        )
    }

    /**
     * Return an extrapolated version of the given target
     */

    fun getExtrapolated(at: Instant = instantNow): TrafficTarget {
        val duration = (at.toEpochMilli() - timestamp.toEpochMilli()) / 1000.0
        val newPosition = translate(track, duration * speed)
        val newAltitude = if (verticalSpeed > -1000.0 && verticalSpeed < 1000.0)
            altitude + verticalSpeed * duration
        else
            altitude
        return from(
            latitude = newPosition.latitude,
            longitude = newPosition.longitude,
            altitude = newAltitude,
            timestamp = at,
            isExtrapolated = true,
        )
    }

    companion object {
        val invalid = of(
            INVALID_DATA,
            INVALID_DATA,
            Location.Creator("Invalid", Location.LOW_PRIORITY),
            INVALID_DATA,
            0.0,
            INVALID_DATA,
            Instant.EPOCH,
            Double.NaN,
            Double.NaN,
            EmitterAddress(AddressType.None, 0),
            EmitterCategory.None,
            "",
            INVALID_DATA,
            isExtrapolated = false,
        )

        /**
         * Our position etc.
         */
        var ownship: TrafficTarget = invalid
            set(value) {
                field = of(
                    value.latitude,
                    value.longitude,
                    value.creator,
                    value.speed,
                    value.track,
                    value.altitude,
                    value.timestamp,
                    value.accuracyH,
                    value.accuracyV,
                    value.address,
                    value.emitterCategory,
                    value.callSign,
                    value.verticalSpeed,
                    (value.track - field.track) /
                        Duration.between(field.timestamp, value.timestamp).toMillis() / 1000.0,
                    isExtrapolated = value.isExtrapolated,
                )
            }
    }
}