package com.controlj.traffic

import com.controlj.data.Location
import java.lang.Math.abs

enum class TrafficAlert {
    None,       // no immediate concern
    Alert,      // be aware of this
    Warning,    // proximity within 30 seconds
    Alarm;      // imminent collision risk

    fun TrafficTarget.getAlertStatus(location: Location, ourCategory: TrafficTarget.EmitterCategory): TrafficAlert {

        // no concern about targets outside our wider sphere of interest
        if (distanceTo(location) > TrafficSettings.distanceFilter.value ||
            abs(altitude - location.altitude) > TrafficSettings.altitudeFilter.value
        )
            return TrafficAlert.None
        // special handling when we are a glider.

        //val easting =

        //if(ourCategory == TrafficTarget.EmitterCategory.Glider) {

        //}

        return None
    }
}
