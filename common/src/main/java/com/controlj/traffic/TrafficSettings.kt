package com.controlj.traffic

import com.controlj.settings.IntSetting
import com.controlj.settings.Setting
import com.controlj.settings.ValueSetting
import com.controlj.utility.Units

object TrafficSettings {
    val distanceFilter = ValueSetting(
        "distanceFilter", "Distance filter", "Ignore targets further away than this",
        listOf(Units.Unit.METER, Units.Unit.NM, Units.Unit.KM), 8000.0
    )
    val altitudeFilter = ValueSetting(
        "altitudeFilter", "Altitude filter", "Ignore targets more than this above or below",
        listOf(Units.Unit.FOOT, Units.Unit.METER), Units.Unit.FOOT.convertFrom(4500.0)
    )

    val proximateDistance = ValueSetting(
        "proximateDistance", "Proximity distance", "Distance to consider a target close",
        listOf(Units.Unit.METER, Units.Unit.NM, Units.Unit.KM), 2000.0
    )

    val proximateVertical = ValueSetting(
        "proximateAltitude", "Proximity altitude delta", "Altitude difference to consider a target close ",
        listOf(Units.Unit.FOOT, Units.Unit.METER), Units.Unit.FOOT.convertFrom(1000.0)
    )


    val maxTargets = IntSetting("maxTargets", "Target display count", "Show only this many targets", 10)
    val coastTime = IntSetting("targetCoastTime", "Coast time", "Show targets this many seconds after last report", 20)

    val group =
        Setting.Group(
            "Traffic",
            distanceFilter,
            altitudeFilter,
            proximateDistance,
            proximateVertical,
            maxTargets,
            coastTime
        )
}
