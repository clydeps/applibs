package com.controlj.traffic

import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.observeBy
import com.controlj.utility.instantNow
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.internal.schedulers.SingleScheduler
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import kotlin.math.abs

/**
 * The source of all traffic information
 */
object TrafficSource {
    // the emitter for subscriptions
    internal var emitter: ObservableEmitter<List<TrafficTarget>> = DisposedEmitter()

    private val scheduler = SingleScheduler()

    // the set of providers

    private val sources: ConcurrentHashMap<TrafficProvider, Disposable> = ConcurrentHashMap()

    /**
     * Wrap a target with some metadata
     */

    private class Wrapper(val target: TrafficTarget, wasActive: Boolean, val source: TrafficProvider) {
        val active: Boolean

        init {
            val hThresh = TrafficSettings.distanceFilter.value.let {
                if (wasActive)
                    it * 1.25
                else
                    it
            }
            val vThresh = TrafficSettings.altitudeFilter.value.let {
                if (wasActive)
                    it * 1.25
                else
                    it
            }
            active = abs(target.vDistance) <= vThresh && abs(target.distance) <= hThresh
        }
    }
    // the currently tracked traffic, mapped to their distance, for convenient filtering

    private val targets = mutableMapOf<TrafficTarget, Wrapper>()

    /**
     * Add a target to the current set.
     */
    private fun addTarget(target: TrafficTarget, provider: TrafficProvider) {
        val wasActive = targets[target]?.active == true
        targets[target] = Wrapper(target, wasActive, provider)
    }

    /**
     * Expire any stale targets
     */

    private fun expireStale() {
        val cutoff = instantNow.minusSeconds(TrafficSettings.coastTime.value.toLong())
        targets.values.filter { it.target.timestamp < cutoff }.toList().forEach {        // create new list!
            targets.remove(it.target)
        }
    }

    /**
     * get all targets currently tracked.
     */

    val allTargets: List<TrafficTarget> get() = targets.values.map { it.target }

    /**
     * Get the current targets. Omit any outside the distance or altitude envelope, order the rest by distance
     * and return only the N closest, where N is the configured number of targets to show
     * The list will be sorted by the closest first.
     */
    val currentTargets: List<TrafficTarget>
        get() = targets.values.asSequence()
            .distinctBy { it.target.address }
            .filter { it.active }
            .map { it.target }
            .sortedWith(compareBy({ !it.isHazard }, { it.distance }))
            .take(TrafficSettings.maxTargets.value)
            .map { it.getExtrapolated() }.toList()

    /**
     * An observer to deliver traffic updates
     */
    val observer: Observable<List<TrafficTarget>> = Observable.interval(1, TimeUnit.SECONDS, scheduler)
        .map {
            expireStale()
            currentTargets
        }
        .share()


    fun add(provider: TrafficProvider) {
        sources[provider]?.dispose()
        sources[provider] = provider.trafficObserver
            .observeOn(scheduler)
            .observeBy(
                onError = { logMsg(it.toString()) },
                onNext = { addTarget(it, provider) }
            )
    }

    fun remove(provider: TrafficProvider) {
        sources.remove(provider)?.dispose()
    }

    fun targetsFor(provider: TrafficProvider): List<TrafficTarget> {
        return targets.values.filter { it.source == provider }.map { it.target }
    }
    /**
     * Resend traffic data to any listeners
     */

    fun refresh() {
        emitter.onNext(currentTargets)
    }
}
