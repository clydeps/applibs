package com.controlj.traffic

import com.controlj.viewmodel.ListenableValue
import io.reactivex.rxjava3.core.Observable

interface TrafficProvider {
    val name: String
    val trafficObserver: Observable<TrafficTarget>

    /**
     * An enable flag
     */

    val enabled: ListenableValue<Boolean>

    /**
     * Is it connected?
     */

    val isConnected: Boolean

}
