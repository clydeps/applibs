package com.controlj.mapping


import com.google.gson.annotations.SerializedName

data class RainViewerMaps(
    @SerializedName("generated")
    val generated: Int, // 1613625650
    @SerializedName("host")
    val host: String, // https://tilecache.rainviewer.com
    @SerializedName("radar")
    val radar: Radar,
    @SerializedName("satellite")
    val satellite: Satellite,
    @SerializedName("version")
    val version: String // 2.0
) {
    data class Radar(
        @SerializedName("nowcast")
        val nowcast: List<Snapshot>,
        @SerializedName("past")
        val past: List<Snapshot>
    )

    data class Satellite(
        @SerializedName("infrared")
        val infrared: List<Snapshot>
    )

    data class Snapshot(
        @SerializedName("path")
        val path: String, // /v2/satellite/f2ba5d64592d
        @SerializedName("time")
        val time: Int // 1613618400
    )
}
