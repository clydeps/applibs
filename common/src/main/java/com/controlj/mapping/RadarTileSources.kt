package com.controlj.mapping

import com.controlj.comms.OKClient
import com.controlj.settings.Properties
import com.controlj.utility.Hash
import com.controlj.utility.instantNow
import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 9/11/18
 * Time: 19:55
 */
object RadarTileSources {

    private val LATENCY = TimeUnit.SECONDS.toMillis(60)     // allow this long for a tileset to be valid
    private val RETRY_DELAY = TimeUnit.SECONDS.toMillis(60)     // Retry a fetch after this delay
    private val RADAR_SECRET = Properties.getProperty("radarSecret")
    private val META_URL = Properties.getProperty("radarUrl")
    val EMPTY_TILESET = TileSet()

    interface TileRest {
        @GET("meta.json")
        fun getTileFills(@Header("Authorization") authorization: String): Observable<TileSet>
    }

    data class TileSet(
            @Expose
            val timestamp: Int,
            @Expose
            val layerName: String,
            @Expose
            val url: String,
            @Expose
            val heatmap: String,
            @Expose
            val parameterName: String,
            @Expose
            val colors: Map<Int, Int>,      // maps intensity values to colors
            @Expose
            val history: Map<String, Long>,
            @Expose
            val updateInterval: Long
    ) {
        internal constructor() : this(0, "radar", "", "", "intensity", mapOf(), mapOf(), Long.MAX_VALUE)

        val historyList by lazy { history.keys.sortedDescending() }

        // get the age in milliseconds of the oldest image in the specified tileset
        fun getAge(uuid: String): Long {
            return instantNow.toEpochMilli() - (history[uuid] ?: 0)
        }

        fun isValid(): Boolean {
            return colors.isNotEmpty()
        }

        /**
         *  Get the URL for the tile source with the requested history
         */
        fun getUrl(uuid: String, heatmap: Boolean): String {
            return (if (heatmap) this.heatmap else url).replace("{UUID}", uuid)
        }
    }

    private val gson = GsonBuilder()
            .setLenient()
            .create()

    private val retrofit by lazy {
        Retrofit.Builder()
                .client(OKClient.builder.build())
                .baseUrl(META_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build()
    }

    fun getMeta(): Observable<TileSet> {
        val metaApi = retrofit.create(TileRest::class.java)
        return metaApi.getTileFills(Hash.createHashString(RADAR_SECRET)).subscribeOn(Schedulers.io())
                .take(1).flatMap { tileset ->
                    Observable.interval(tileset.updateInterval, TimeUnit.MILLISECONDS, Schedulers.io())
                            .flatMap {
                                metaApi.getTileFills(Hash.createHashString(RADAR_SECRET))
                                        .take(1)
                                        .retryWhen { Observable.timer(RETRY_DELAY, TimeUnit.MILLISECONDS) }
                            }
                            .startWithItem(tileset)
                }
    }
}
