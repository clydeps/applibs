package com.controlj.mapping

import org.threeten.bp.Instant

class RasterTileSource(
    val name: String,
    val tiles: List<String>,
    val description: String,
    val age: Long = 0,
    val size: Int = 256,
    val minZoom: Int = 0,
    val maxZoom: Int = 18,
    val bounds: List<Double> = listOf(-180.0, -80.0, 180.0, 80.0)
)
