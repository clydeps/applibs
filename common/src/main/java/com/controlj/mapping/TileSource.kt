package com.controlj.mapping

data class TileSource(
    val name: String,
    val tiles: List<String>,
    val minZoom: Int = 0,
    val maxZoom: Int = 18,
    val layer: String,
    val colors: Map<Int, Int> = mutableMapOf(),
    val bounds: List<Double> = listOf(-180.0, -80.0, 180.0, 80.0)
)
