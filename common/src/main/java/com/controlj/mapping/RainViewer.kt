package com.controlj.mapping

import com.controlj.comms.OKClient
import com.google.gson.GsonBuilder
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

/**
 * Access precipitation tiles from Rainviewer.com
 */
object RainViewer {

    private const val baseUrl = "https://api.rainviewer.com/public/"
    private const val RETRY_TIME = 60L
    private const val REFRESH_TIME = 300L


    private interface TileRest {
        @GET("weather-maps.json")
        fun getMeta(): Single<RainViewerMaps>
    }

    private val gson = GsonBuilder()
        .setLenient()
        .create()

    private val retrofit by lazy {
        Retrofit.Builder()
            .client(OKClient.builder.build())
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
    }

    private val metaApi = retrofit.create(TileRest::class.java)

    private val metaSource = Observable.interval(0L, REFRESH_TIME, TimeUnit.SECONDS, Schedulers.io())
        .flatMapSingle { metaApi.getMeta() }
        .retryWhen { Observable.timer(RETRY_TIME, TimeUnit.SECONDS) }
        .subscribeOn(Schedulers.io())
        .share()

    private fun getMaps(
        selector: RainViewerMaps.() -> List<RainViewerMaps.Snapshot>,
        count: Int,
    ): Observable<List<Pair<String, Long>>> = metaSource
        .map { it to it.selector() }
        .filter { it.second.isNotEmpty() }
        .map { map ->
            map.second
                .sortedByDescending { it.time }
                .take(count)
                .map {
                val path = "${map.first.host}${it.path}"
                val age = TimeUnit.MINUTES.convert(
                    System.currentTimeMillis() / 1000 - it.time,
                    TimeUnit.SECONDS
                )    // convert to minutes
                path to age
            }
        }

    object Radar : WeatherSource {

        private const val pngSuffix = "/256/{z}/{x}/{y}/6/1_1.png"

        override fun toString(): String {
            return "Precipitation (Weather radar)"
        }

        override fun getHistory(count: Int): Observable<List<RasterTileSource>> {
            return getMaps({ radar.past }, count)
                .map {
                    it.mapIndexed { index, pair ->
                        val description = "Radar age > ${pair.second} minutes"
                        RasterTileSource(
                            "radar_$index",
                            listOf(pair.first + pngSuffix),
                            description,
                            pair.second,
                            minZoom = 3,
                            maxZoom = 9
                        )
                    }
                }
        }
    }

    object Satellite : WeatherSource {

        private const val pngSuffix = "/256/{z}/{x}/{y}/0/0_0.png"
        override fun getHistory(count: Int): Observable<List<RasterTileSource>> {
            return getMaps({ satellite.infrared }, count)
                .map {
                    it.mapIndexed { index, pair ->
                        val description = "Image age ${pair.second} minutes"
                        RasterTileSource(
                            "satellite_$index",
                            listOf(pair.first + pngSuffix),
                            description,
                            pair.second,
                            minZoom = 3,
                            maxZoom = 9
                        )
                    }
                }
        }

        override fun toString(): String {
            return "Clouds (Weather satellite)"
        }
    }
}
