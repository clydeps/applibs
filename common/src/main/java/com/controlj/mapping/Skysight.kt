package com.controlj.mapping

import com.controlj.comms.OKClient
import com.controlj.graphics.CColor
import org.threeten.bp.LocalDateTime

object Skysight {

    private const val host = "edge.skysight.io"
    private const val origin = "https://skysight.io"
    private const val defaultLayer = "wstar_bsratio"
    private const val defaultRegion = "EAST_AUS"
    private val indexColors = listOf(
        CColor.rgb(96, 96, 178),
        CColor.rgb(98, 165, 243),
        CColor.rgb(102, 228, 193),
        CColor.rgb(143, 213, 107),
        CColor.rgb(246, 235, 106),
        CColor.rgb(245, 180, 102),
        CColor.rgb(240, 99, 104),
        CColor.rgb(150, 97, 171)
    )

    fun getCurrentTiles(
        time: LocalDateTime = LocalDateTime.now().plusMinutes(30),
        layer: String = defaultLayer,
        region: String = defaultRegion
    ): TileSource {
        val dateString =
            "${time.year}${time.monthValue.toString().padStart(2, '0')}${time.dayOfMonth.toString().padStart(2, '0')}"
        val hr = time.hour.toString().padStart(2, '0')
        val min = if (time.minute > 30) "30" else "00"
        val hrMin = "$hr$min"
        return TileSource(
            "thermals",
            listOf("https://$host/$region/$dateString/$hr$min/$layer/{z}/{x}/{y}"),
            layer = hrMin,
            colors = indexColors.mapIndexed { index, i -> index to i }.toMap(),
            minZoom = 3,
            maxZoom = 5

        )
    }

    val headers = mapOf(
        "User-agent" to
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
        "Origin" to origin,
        "DNT" to "1"
    )

    val httpClient by lazy {
        OKClient.builder.addInterceptor { chain ->
            val request = chain.call().request()
            val newRequest = request.newBuilder()
            if (request.url.host.contains(host)) {
                newRequest.removeHeader("User-agent")
                headers.forEach {
                    newRequest.addHeader(it.key, it.value)
                }
            }
            val response = chain.proceed(newRequest.build())
            response
        }.build()
    }
}
