package com.controlj.mapping

import io.reactivex.rxjava3.core.Observable

/**
 * A provider of raster tiles for weather information
 */
interface WeatherSource {
    fun getHistory(count: Int): Observable<List<RasterTileSource>>

    companion object {
        val empty = object: WeatherSource {
            override fun getHistory(count: Int): Observable<List<RasterTileSource>> {
                return Observable.just(listOf())
            }

            override fun toString(): String {
                return "None"
            }
        }
    }
}
