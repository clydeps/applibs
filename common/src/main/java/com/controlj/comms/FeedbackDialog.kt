package com.controlj.comms

import com.controlj.data.DiskFileEntry
import com.controlj.logging.CJLog
import com.controlj.settings.DataStore
import com.controlj.settings.Properties
import com.controlj.ui.*
import com.controlj.utility.Units
import io.reactivex.rxjava3.core.Single
import java.io.File

/**
 * Construct a feedback dialog
 */
class FeedbackDialog(
    title: String = "Debug report",
    includeLogs: Boolean = true,
    choices: List<String> = listOf("Bug report", "Suggestion", "Question"),
    val files: List<File> = listOf()
) :
    DialogData(
        Type.MODAL,
        title,
        ButtonDialogItem("Send report"),
        CANCEL
    ) {
    private val organisation = Properties.getProperty("organisation", "the developer")
    private val privacyUrl = Properties.getProperty("privacyUrl", "")
    private val emailItem: TextValueItem = TextValueItem("Your email", DialogItem.InputStyle.EMAIL).also {
        it.key = "FEEDBACK_EMAIL"
    }
    private val nameItem: TextValueItem = TextValueItem("Your name", DialogItem.InputStyle.NOCORRECT).also {
        it.key = "FEEDBACK_NAME"
    }
    private val messageItem = TextValueItem("Message", inputStyle = DialogItem.InputStyle.MULTI_LINE, lines = 5).also {
        it.key = "FEEDBACK_MESSAGE"
    }
    private val typeItem = SpinnerDialogItem<String>("Feedback type").also {
        it.key = "FEEDBACK_TYPE"
    }
    private val logsItem = CheckboxItem("Send logfiles")

    init {
        if (choices.isNotEmpty()) {
            typeItem.choices = choices
            addItem(typeItem)
        }
        addItem(
            DialogItem("This data is used by $organisation to troubleshoot app problems.")
        )
        if (privacyUrl.isNotBlank()) {
            addItem(
                DialogItem(
                    "The report may contain personal information - " +
                            "$organisation's privacy policy may be viewed at:"
                )
            )
            addItem(ClickableDialogItem(text = privacyUrl, action = { UiAction.openUrl(privacyUrl) }))
            addItem(DialogItem("You may provide an email address if you would like feedback, otherwise the report will be anonymous."))
            emailItem.value = DataStore().getString(EMAIL_KEY)
            nameItem.value = DataStore().getString(NAME_KEY)
            addItem(nameItem)
            addItem(emailItem)
        }
        addItem(messageItem)
        if (includeLogs) {
            logsItem.isChecked = true
            addItem(logsItem)
        }
    }

    override val singleObserver: Single<ButtonDialogItem>
        get() {
            return super.singleObserver
                .filter { it != CANCEL }
                .flatMapSingle {
                    val email = emailItem.value
                    val name = nameItem.value
                    if (name.isNotBlank())
                        DataStore().putString(NAME_KEY, name)
                    val fileList = if (logsItem.isChecked) files + CJLog.logFiles else files
                    val s = if (fileList.isEmpty())
                        "Uploading $title"
                    else
                        Units.getQuantity("Uploading $title with %n file%s", fileList.size)
                    val extras = items.map { it.key to it.value }.filterNot { it.first.isBlank() }.toMap()
                    val progDialog = ProgressDialog(s, true)
                    progDialog.run(
                        Uploader(CJLog.packageName, "${CJLog.versionString}/${CJLog.buildNumber}", extras = extras)
                            .upload(
                                messageItem.value,
                                files = fileList.map { DiskFileEntry(it) },
                                email = email,
                            )
                    )
                }
                .toSingle()
                .onErrorReturnItem(CANCEL)
        }

    companion object {
        const val EMAIL_KEY = "key_debug_email_address"
        const val NAME_KEY = "key_debug_name"
    }
}