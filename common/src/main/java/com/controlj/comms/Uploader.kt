package com.controlj.comms


import com.controlj.data.DiskFileEntry
import com.controlj.data.FileEntry
import com.controlj.data.Progress
import com.controlj.framework.ApplicationState
import com.controlj.framework.BackgroundServiceProvider
import com.controlj.framework.FilePaths
import com.controlj.framework.size
import com.controlj.logging.CJLog
import com.controlj.logging.CrashLogger
import com.controlj.settings.Setting
import com.controlj.utility.FileParameter
import com.controlj.utility.FormData
import com.controlj.utility.FormRequest
import com.controlj.utility.Hash
import com.controlj.utility.Parameter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import java.lang.Long.toHexString

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 28/1/17
 * Time: 9:48 AM
 */
open class Uploader(val app: String, val version: String, val extras: Map<String, String> = mapOf()) {

    open fun upload(message: String = "", files: Collection<FileEntry>, email: String = ""): Observable<Progress> {
        return Observable.create { emitter: ObservableEmitter<Progress> ->
            if (files.sumOf { it.length } > MAX_UPLOAD) {
                emitter.onError(IllegalArgumentException("Combined upload size exceeds limit"))
            } else {
                ApplicationState.setBusy("Uploader", true)
                emitter.onNext(Progress("Copying files", 0, false))
                val diskFiles = files.asSequence()
                    .takeWhile { !emitter.isDisposed }
                    .map { DiskFileEntry.from(it) }
                    .toList()
                val formList = (CrashLogger.deviceInfo.map { it.name to it.value } +
                    listOf(
                        "STACK_TRACE" to "File upload",
                        "CUSTOM_DATA" to customData(),
                        "SETTINGS" to Setting.asString()
                    )).toMap().toMutableMap()

                if (message.isNotBlank())
                    formList["FEEDBACK_MESSAGE"] = message
                if (email.isNotBlank())
                    formList["FEEDBACK_EMAIL"] = email
                val log = CJLog.log
                if (log != null)
                    formList["LOG"] = log
                val dataDir = FilePaths().dataDirectory.apply {
                    if (name == "BlueMAX") parentFile.parentFile else parentFile
                }
                formList["DATAFILES"] = dataDir.walkTopDown().filter { !it.isDirectory }.joinToString("\n") {
                    "${it.toRelativeString(dataDir)}: ${it.size}"
                }
                val formData = FormData((formList + extras).map { Parameter(it.key, it.value) })
                formData.addAll(diskFiles.map { FileParameter(it.name, it) })
                ApplicationState.setBusy("Uploader", false)
                if (!emitter.isDisposed)
                    try {
                        BackgroundServiceProvider().postRequest(
                            FormRequest(
                                CrashLogger.url,
                                formData,
                                headers = mapOf("Accept" to "*/*")
                            ),
                            "uploader",
                            emitter
                        )
                    } catch (e: Exception) {
                        CJLog.logException(e)
                        emitter.onError(e)
                    }
            }
        }
            .subscribeOn(Schedulers.io())
    }

    data class Response(val status: String, val files: Int)

    companion object {
        const val MAX_UPLOAD = 8 * 1024 * 1024L
        private fun customData(): String {
            val now = toHexString(System.currentTimeMillis())
            return "stamp=" + now + "\n" + "key=" + Hash.sha256(now + CrashLogger.ACRAKey)
        }
    }
}


