package com.controlj.comms

import com.controlj.logging.CJLog
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-24
 * Time: 11:03
 *
 * This class provides a Builder for OKHTTP that is configured correctly for accessing Cloudflare SSL
 */
object OKClient {
    val builder: OkHttpClient.Builder
        get() {
            val specs = mutableListOf(
                ConnectionSpec.MODERN_TLS,
                ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
                    .allEnabledCipherSuites()
                    .allEnabledTlsVersions()
                    .build()
            )
            if (CJLog.isDebug)
                specs.add(ConnectionSpec.CLEARTEXT)
            return OkHttpClient.Builder().connectionSpecs(specs).applyDebug()
        }

    private val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
        @Suppress("TrustAllX509TrustManager")
        override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
        }

        @Suppress("TrustAllX509TrustManager")
        override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
        }

        override fun getAcceptedIssuers() = arrayOf<X509Certificate>()
    })

    // Install the all-trusting trust manager
    private val sslContext =
        SSLContext.getInstance("SSL").also { it.init(null, trustAllCerts, java.security.SecureRandom()) }

    fun OkHttpClient.Builder.applyDebug(): OkHttpClient.Builder {
        if (CJLog.isDebug) {
            sslSocketFactory(
                sslContext.socketFactory,
                trustAllCerts[0] as X509TrustManager
            ).hostnameVerifier { _, _ -> true }
        }
        return this
    }
}
