package com.controlj.comms;

/**
 * Created by clyde on 3/6/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Locale;

public class TestData {

    @SerializedName("bootloader")
    @Expose
    private String bootloader;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("configuration")
    @Expose
    private String configuration;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("modelnumber")
    @Expose
    private String modelnumber;
    @SerializedName("testdate")
    @Expose
    private String testdate;
    @SerializedName("macaddress")
    @Expose
    private String macaddress;
    @SerializedName("id")
    @Expose
    private Integer id;

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModelnumber() {
        return modelnumber;
    }

    public void setModelnumber(String modelnumber) {
        this.modelnumber = modelnumber;
    }

    public String getTestdate() {
        return testdate;
    }

    public void setTestdate(String testdate) {
        this.testdate = testdate;
    }

    public String getMacaddress() {
        return macaddress;
    }

    public void setMacaddress(String macaddress) {
        this.macaddress = macaddress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "%s:%s:%s:%s", getSerialNumber(), macaddress, modelnumber, testdate);
    }

    public String getSerialNumber() {
        return String.format(Locale.US, "%05d", id);
    }

    public String getConfiguration() {
        return configuration;
    }

    public void setConfiguration(String configuration) {
        this.configuration = configuration;
    }

    public String getVersion() {
        return version;
    }

    public String getBootloader() {
        return bootloader;
    }

    public void setBootloader(String bootloader) {
        this.bootloader = bootloader;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
