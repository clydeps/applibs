package com.controlj.graphics

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 26/1/17
 * Time: 9:45 PM
 */
data class CRect(var left: Double = 0.0, var top: Double = 0.0, var right: Double = 0.0, var bottom: Double = 0.0) {
    constructor(origin: CPoint, size: CSize) : this(origin.x, origin.y, origin.x + size.width, origin.y + size.height)

    val width: Double
        get() = right - left
    val height: Double
        get() = bottom - top
    val middleX: Double
        get() = (left + right) / 2.0
    val middleY: Double
        get() = (top + bottom) / 2.0

    /**
     * Create a new Crect, inset by the supplied values (can be negative, to enlarge a boundary)
     * @param paddingLeft
     * @param paddingTop
     * @param paddingRight
     * @param paddingBottom
     * @return
     */
    fun inset(paddingLeft: Double, paddingTop: Double, paddingRight: Double, paddingBottom: Double): CRect {
        return CRect(left + paddingLeft, top + paddingTop, right - paddingRight, bottom - paddingBottom)
    }

    /**
     * Create a new CRect, translated from this by the supplied values.
     * @param horiz
     * @param vert
     * @return
     */
    fun translate(horiz: Double, vert: Double): CRect {
        return CRect(left + horiz, top + vert, right + horiz, bottom + vert)
    }

    val aspectRatio: Double
        get() {
            if (empty)
                return 1.0
            return width / height
        }
    /**
     * True if this CRect is empty - i.e. width or height is zero
     */
    val empty: Boolean
        get() {
            return width == 0.0 || height == 0.0
        }

    /**
     * Does this rect contain the given point?
     * @param position The point to check
     * @return true if the point lies within this CRect
     */
    fun contains(position: CPoint): Boolean {
        return position.x >= left && position.x < right && position.y >= top && position.y < bottom
    }

    /**
     * Check for intersection
     * @param other The other CRect to check for intersection with
     * @return true if the two rectangles intersect
     */
    fun intersects(other: CRect): Boolean {
        return other.left < right && left < other.right && other.top < bottom && top < other.bottom
    }
}



