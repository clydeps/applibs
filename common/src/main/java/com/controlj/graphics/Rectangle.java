package com.controlj.graphics;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 * <p>
 * User: clyde
 * Date: 25/1/17
 * Time: 12:17 PM
 */
public class Rectangle {
	float left;
	float top;
	float width;
	float height;


	public Rectangle(float left, float top, float width, float height) {
		this.left = left;
		this.top = top;
		this.width = width;
		this.height = height;
	}

	public float getMidX() {
		return left+width/2;
	}

	public float getMidY() {
		return top+height/2;
	}

	public float getBottom() {
		return top + height;
	}

	public float getRight() {
		return left + width;
	}

	public float getLeft() {
		return left;
	}

	public void setLeft(float left) {
		this.left = left;
	}

	public float getTop() {
		return top;
	}

	public void setTop(float top) {
		this.top = top;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}
}
