package com.controlj.graphics

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 25/1/17
 * Time: 9:22 PM
 */
open class TextStyle(
    var fontFamily: FontFamily = FontFamily.SystemDefault,
    var fontStyle: FontStyle = FontStyle.Normal,
    var alignment: Alignment = Alignment.Center,
    var size: Double,
) {

    var pixels: Int = 0
        protected set

    val styleBits: Int
        get() = fontFamily.ordinal or (fontStyle.ordinal shl 8)

    enum class Alignment {
        Left,
        Right,
        Center
    }

    enum class FontFamily {
        SystemDefault,
        Serif,
        SansSerif,
        Monospaced
    }

    enum class FontStyle {
        Normal,
        Bold,
        Italic,
        BoldItalic
    }

    init {
        this.pixels = (size + 0.5f).toInt()
    }

    fun zoom(increment: Double): TextStyle {
        return TextStyle(fontFamily, fontStyle, alignment, size + increment)
    }

    override fun hashCode(): Int {
        return size.toInt() + (fontStyle.ordinal shl 8) + (fontFamily.ordinal shl 16)
    }

    override fun equals(other: Any?): Boolean {
        if (other !is TextStyle)
            return false
        return other.fontFamily == fontFamily && other.fontStyle == fontStyle && other.size == size && other.alignment == alignment
    }

    override fun toString(): String {
        return "TextStyle[$fontFamily, $fontStyle, size: $size]"
    }

    fun from(
        fontFamily: FontFamily = this.fontFamily,
        fontStyle: FontStyle = this.fontStyle,
        alignment: Alignment = this.alignment,
        size: Double = this.size
    ): TextStyle {
        return TextStyle(fontFamily, fontStyle, alignment, size)
    }

    companion object {
        val DEFAULT = TextStyle(FontFamily.SystemDefault, FontStyle.Normal, Alignment.Center, 16.0)
        val LEFT_ALIGNED = TextStyle(FontFamily.SystemDefault, FontStyle.Normal, Alignment.Left, 16.0)
        val HEADING = TextStyle(FontFamily.SystemDefault, FontStyle.Bold, Alignment.Left, 20.0)
        val SUMMARY = TextStyle(FontFamily.SystemDefault, FontStyle.Normal, Alignment.Right, 12.0)
    }

}
