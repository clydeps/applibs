package com.controlj.graphics

import kotlin.math.sqrt

/**
 * Created by clyde on 17/5/17.
 */
object CColor {
    fun rgb(r: Int, g: Int, b: Int): Int {
        return 0xFF000000.toInt() or (r and 0xFF shl 16) or (g and 0xFF shl 8) or (b and 0xFF)
    }

    fun argb(a: Double, r: Double, g: Double, b: Double): Int {
        return argb((a * 255).toInt(), (r * 255).toInt(), (g * 255).toInt(), (b * 255).toInt())
    }

    fun argb(a: Int, r: Int, g: Int, b: Int): Int {
        return a and 0xFF shl 24 or (r and 0xFF shl 16) or (g and 0xFF shl 8) or (b and 0xFF)
    }

    fun grey(whiteFrac: Double): Int {
        return fromWhiteAlpha(whiteFrac, 1.0)
    }

    fun fromWhiteAlpha(white: Double, alpha: Double): Int {
        return argb(alpha, white, white, white)
    }

    fun invert(orig: Int): Int {
        return orig xor 0xFFFFFF
    }

    val RED = rgb(0xFF, 0, 0)
    val GREEN = rgb(0, 0xFF, 0)
    val BLUE = rgb(0, 0, 0xFF)
    val BLACK = rgb(0, 0, 0)
    val NORMAL: Int = rgb(0, 0xFF, 0)
    val NORMAL_DARK: Int = rgb(0x01, 0x72, 0x20)
    val CAUTION: Int = rgb(0xEF, 0xEF, 0x20)
    val CAUTION_DARK: Int = rgb(0xCF, 0xCF, 0x10)
    val ALARM: Int = rgb(0xFF, 0x34, 0x3F)
    val WHITE = rgb(0xFF, 0xFF, 0xFF)
    val UNDER_RANGE = WHITE
    val UNDER_RANGE_DARK = BLACK
    val NEUTRAL = WHITE
    val CYAN = rgb(0, 0xFF, 0xFF)
    val CLEAR = 0x00000000  // transparent
    val OUT_OF_RANGE = CLEAR

}

val Int.rgbString: String
    get() {
        return "argb(%02x,%02x,%02x,%02x)".format(alpha, red, green, blue)
    }

val Int.red: Int
    get() {
        return this shr 16 and 0xFF
    }

val Int.green: Int
    get() {
        return this shr 8 and 0xFF
    }

val Int.blue: Int
    get() {
        return this and 0xFF
    }

val Int.alpha: Int
    get() {
        return this shr 24 and 0xFF
    }

val Int.brightness: Double
    get() {
        val r = red
        val g = green
        val b = blue
        return sqrt(r * r * .299 + g * g * .587 + b * b * .114) / 255.0
    }

val Int.isDark: Boolean
    get() {
        return brightness < 0.5
    }

val Int.contrastColor: Int
    get() {
        return if (isDark) CColor.WHITE else CColor.BLACK
    }

