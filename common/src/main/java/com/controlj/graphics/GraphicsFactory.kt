package com.controlj.graphics

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 26/1/17
 * Time: 10:22 PM
 */
interface GraphicsFactory {

    /**
     * Get the bounds of the screeen. Will always be zero based.
     * @return
     */
    val screenBounds: CRect

    /**
     * Create a mutable path that can later be used to draw things.
     * @return  A new Path
     */
    fun createPath(): Path

    /**
     * Get an image buffer that we can write a bitmap to.
     * @param width The width of the buffer
     * @param height The height of the buffer
     * @return  A new buffer
     */
    fun getImageBuffer(width: Int, height: Int): GraphicsContext

    /**
     * Get the bounds of a text string
     * @param text  The input string
     * @param style The style to be applied to the string for measurement purposes
     * @return  A CRect with 0,0 origin and the width and height of the text in points
     */
    fun getTextBounds(text: String, style: TextStyle): CRect

    /**
     * Get the size of a text string
     * @param text  The input string
     * @param style The style to be applied to the string for measurement purposes
     * @return  A CSize with the width and height of the text in pixels
     */
    fun getTextSize(text: String, style: TextStyle): CSize

    /**
     * Convert points to pixels
     * @param points
     * @return  pixels
     */
    fun pointsToPixels(points: Double): Int

    val textFactor: Double

    /*
    Load an image from a filename.
     */
    fun loadImage(filename: String, isTemplate: Boolean = false): CImage

    fun init() {
        instance = this
    }

    companion object {
        private lateinit var instance: GraphicsFactory

        operator fun invoke() = instance
    }
}
