package com.controlj.graphics


interface GraphicsContext {

    val bitmap: CImage

    enum class LineCap {
        butt,
        round,
        square
    }

    var fillColor: Int
    var strokeColor: Int
    var lineWidth: Double
    var lineCap: LineCap
    fun fillRect(bounds: CRect)
    fun strokeRect(bounds: CRect)
    fun saveState()
    fun restoreState()
    fun drawArc(center: Point, radius: Double, startAngle: Double, endAngle: Double)
    fun drawArc(x: Double, y: Double, radius: Double, startAngle: Double, endAngle: Double)
    fun rotateBy(degrees: Double)
    fun drawLine(x1: Double, y1: Double, x2: Double, y2: Double)

    /**
     * Draw text
     * @param text  The text string
     * @param bounds    The bounds within which to draw
     * @param style The style to be applied
     * @param color
     */
    fun drawText(text: String, bounds: CRect, style: TextStyle, color: Int)

    fun clip(bounds: CRect)
    fun fillEllipse(bounds: CRect)
    fun scaleBy(x: Double, y: Double)
    fun translateBy(x: Double, y: Double)
    fun fillPath(path: Path)
    fun drawImage(image: CImage, bounds: CRect, tintColor: Int? = null)
    fun strokePath(path: Path)

}
