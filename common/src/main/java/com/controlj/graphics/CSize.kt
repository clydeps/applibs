package com.controlj.graphics

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-07-14
 * Time: 12:50
 *
 * A size
 */
data class CSize(val width: Double, val height: Double)