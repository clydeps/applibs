package com.controlj.graphics;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 * <p>
 * User: clyde
 * Date: 26/1/17
 * Time: 2:26 PM
 */
public interface Path {
	void moveTo(double x, double y);
	void lineTo(double x, double y);
	void addRoundedRect(CRect bounds, double xRadius, double yRadius);
	void addArc(double x, double y, double radius, double startAngle, double sweepAngle);
	void closePath();
	void clear();
	boolean isEmpty();
}
