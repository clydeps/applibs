package com.controlj.graphics;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 * <p>
 * User: clyde
 * Date: 26/1/17
 * Time: 9:02 AM
 */
public class Vector {
	public double	x1, y1;
	public double	x2, y2;

	public Vector(double x1, double y1, double x2, double y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
}
