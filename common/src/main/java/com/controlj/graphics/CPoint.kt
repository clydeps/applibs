package com.controlj.graphics

import java.util.*

/**
 * Created by clyde on 2/7/17.
 */

data class CPoint(val x: Double = 0.0, val y: Double = 0.0) {

    override fun toString(): String {
        return String.format(Locale.US, "[%.0f,%.0f]", x, y)
    }
}
