package com.controlj.animation

import java.util.concurrent.ConcurrentSkipListSet

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 29/1/17
 * Time: 4:46 PM
 */
object AnimationController {
    private val animations: ConcurrentSkipListSet<Animation> = ConcurrentSkipListSet()
    private val removals: ConcurrentSkipListSet<Animation> = ConcurrentSkipListSet()
    var minInterval = 0.05      // minimum frame interval in s
    private var lastTime: Double = 0.toDouble()

    /**
     * Add an animation to the list and start it
     *
     * @param animation The animation to be added
     */
    fun add(animation: Animation) {
        animations.add(animation)
    }

    /**
     * This method must be called each animation frame in order for the animations to run
     *
     * @param time the frame time in seconds
     */

    fun onFrame(time: Double) {
        if (time < lastTime + minInterval)
            return
        lastTime = time
        if (animations.isEmpty())
            return
        animations.forEach { animation -> animation.update(time) }
        removals.forEach { animation -> animations.remove(animation) }
        removals.clear()
    }

    /**
     * Schedule the animation for removal. Should only be called via onFrame.
     *
     * @param animation
     */
    fun remove(animation: Animation) {
        removals.add(animation)
    }
}
