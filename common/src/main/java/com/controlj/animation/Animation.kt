package com.controlj.animation

/**
 * Create a new animation
 *
 * @param delay    The delay in seconds from the call to start() before the animation begins
 * @param duration The animation duration in seconds
 * @param listener The onProgress method of this object will be called each frame
 */
class Animation(var delay: Double = 0.0, var duration: Double, var listener: (Double) -> Unit) : Comparable<Animation> {

    var callback: Callback? = null
    private var startTime: Double = 0.0
    private var currentTime = 0.0
    private var state = State.IDLE
        set(value) {
            field = value
            value.update(this, currentTime)
        }

    /**
     * IS the animation running?
     *
     * @return true unless the animation is in the IDLE or COMPLETED state
     */
    val isRunning: Boolean
        get() = state.runState

    /**
     * Check if the animation start delay has expired
     *
     * @return true if the animation is running and the start delay has expired
     */
    val isStarted: Boolean
        get() = state.startState

    /**
     * Check if the animation has completed. If start() has never been called this will be false.
     *
     * @return true if the animation has completed.
     */
    val isCompleted: Boolean
        get() = state === State.COMPLETED

    override fun compareTo(other: Animation): Int {
        return hashCode() - other.hashCode()
    }

    interface Callback {
        /**
         * Called when the animation is starting.
         */
        fun onStart()

        /**
         * Called when the amination completes.
         */
        fun onStop()
    }

    /**
     * A state machine for animation control
     */
    internal enum class State(var runState: Boolean, var startState: Boolean) {
        /**
         * The animation begins in an idle state.
         */
        IDLE(false, false) {
            override fun update(animation: Animation, currentTime: Double) {
                animation.startTime = currentTime + animation.delay
                animation.state = PRESTART
            }
        },
        /**
         * This is the state during the delay before the animation proper commences
         */
        PRESTART(true, false) {
            override fun update(animation: Animation, currentTime: Double) {
                if (currentTime < animation.startTime)
                // still delaying?
                    return
                animation.callback?.onStart()
                animation.state = STARTED
            }
        },
        /**
         * The animation is started and onProgress will be called each frame
         */
        STARTED(true, true) {
            override fun update(animation: Animation, currentTime: Double) {
                val progress = if(animation.duration <= 0.0) 1.0 else ((currentTime - animation.startTime) / animation.duration)
                animation.listener(progress.coerceAtMost(1.0))
                if (progress >= 1.0) {
                    animation.callback?.onStop()
                    AnimationController.remove(animation)
                    animation.state = COMPLETED
                }
            }
        },
        /**
         * The animation has been completed.
         */
        COMPLETED(false, true) {
            override fun update(animation: Animation, currentTime: Double) {
            }
        };

        internal abstract fun update(animation: Animation, currentTime: Double)
    }

    /**
     * Update the animation.
     *
     * @param currentTime
     * @return false if this animation is now complete
     */
    fun update(currentTime: Double): Boolean {
        if(!isRunning)
            return false
        this.currentTime = currentTime
        state.update(this, currentTime)
        return true
    }

    /**
     * Stop the animation. No further calls to onProgress will be made
     */
    fun stop() {
        if (isStarted)
            state = State.COMPLETED
    }

    /**
     * Start the animation, after the delay if non-zero
     *
     * @return true if the animation was started, false if it was already running
     */
    fun start(): Boolean {
        if (isRunning)
            return false
        state = State.IDLE
        AnimationController.add(this)
        return true
    }

    /**
     * Restart the animation from the beginning
     */
    fun restart() {
        stop()
        start()
    }
}
