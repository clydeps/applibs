package com.controlj.data

/**
 * Created by clyde on 30/6/17.
 */

class DropboxAuthToken(
        var access_token: String,
        var token_type: String,
        var account_id: String,
        var uid: String
)
