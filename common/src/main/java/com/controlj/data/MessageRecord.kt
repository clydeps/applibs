package com.controlj.data

import com.controlj.database.CJDatabase
import com.controlj.database.timestamp
import com.controlj.utility.instantNow
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import org.ktorm.dsl.QueryRowSet
import org.ktorm.dsl.and
import org.ktorm.dsl.deleteAll
import org.ktorm.dsl.desc
import org.ktorm.dsl.eq
import org.ktorm.dsl.from
import org.ktorm.dsl.greaterEq
import org.ktorm.dsl.insert
import org.ktorm.dsl.lessEq
import org.ktorm.dsl.limit
import org.ktorm.dsl.map
import org.ktorm.dsl.orderBy
import org.ktorm.dsl.select
import org.ktorm.dsl.update
import org.ktorm.dsl.where
import org.ktorm.schema.BaseTable
import org.ktorm.schema.boolean
import org.ktorm.schema.int
import org.ktorm.schema.varchar
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter

interface MessageRecord : Comparable<MessageRecord> {

    val message: String
    val priority: Priority
    var subtext: String
    val started: Instant
    var ended: Instant
    var active: Boolean
    var id: Int

    fun save(): Single<MessageRecord> {

        return database.single {
            when (id) {
                0 -> {
                    val id = database.db.insert(MessageRecords) {
                        set(it.message, message)
                        set(it.subtext, subtext)
                        set(it.priority, priority.name)
                        set(it.ended, ended)
                        set(it.started, started)
                        set(it.active, active)
                    }
                    this@MessageRecord.id = id
                    this@MessageRecord
                }
                else -> {
                    database.db.update(MessageRecords) {
                        set(it.message, message)
                        set(it.subtext, subtext)
                        set(it.priority, priority.name)
                        set(it.ended, ended)
                        set(it.started, started)
                        set(it.active, active)
                        where { it.id eq id }
                    }
                    this@MessageRecord
                }
            }
        }.subscribeOn(database.scheduler)
    }

    /**
     * Update the end time to now, and save.
     * Optionally update the active state
     * @param newState The new active state to apply
     */
    fun update(newState: Boolean = active): Single<MessageRecord> {
        active = newState
        ended = instantNow
        return save()
    }

    /**
     * Get the duration of an event
     */
    val duration: Duration
        get() = Duration.between(started, ended)

    /**
     * Default sort order is by started time descending, then message.
     */
    override fun compareTo(other: MessageRecord): Int {
        val i = other.started.compareTo(started)
        if (i != 0)
            return i
        return message.compareTo(other.message)
    }

    fun zuluTime(): String {
        return timeFormat.format(started) + " Z"
    }

    private data class Impl(
        override val message: String,
        override val priority: Priority,
        override var subtext: String,
        override val started: Instant,
        override var ended: Instant,
        override var active: Boolean,
        override var id: Int
    ) : MessageRecord {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Impl

            if (message != other.message) return false
            if (priority != other.priority) return false
            if (active != other.active) return false

            return true
        }

        override fun hashCode(): Int {
            var result = message.hashCode()
            result = 31 * result + priority.hashCode()
            return result
        }
    }

    companion object {
        private val zone = ZoneId.of("UTC")
        private val timeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(zone)
        val maxInstant = Instant.parse("9999-01-01T00:00:00Z")
        val minInstant = Instant.EPOCH

        internal const val DB_NAME = "MessageRecords.db"
        private const val DB_TABLE_NAME = "MessageRecords"

        internal object database : CJDatabase(DB_NAME) {
            override val updateStatements: List<List<String>> = listOf(
                listOf(
                    """
                                create table $DB_TABLE_NAME (
                                    _id integer primary key autoincrement not null,
                                    message varchar not null,
                                    subtext varchar not null,
                                    priority varchar not null,
                                    started varchar not null,
                                    ended varchar not null,
                                    active integer)
                            """,
                    """
                                create index started_index on $DB_TABLE_NAME (started)
                            """
                )
            )
        }

        fun of(
            message: String,
            priority: Priority,
            subtext: String = "",
            started: Instant = instantNow,
            ended: Instant = started,
            active: Boolean = true,
            id: Int = 0
        ): MessageRecord = Impl(message, priority, subtext, started, ended, active, id)

        fun from(other: MessageRecord, newid: Int = other.id): MessageRecord =
            other.run { of(message, priority, subtext, started, ended, active, newid) }

        /**
         * Reset the active state on any old messages
         * @return A single delivering the number of active alerts that were cleared.
         */

        fun clearActive(): Single<Int> {
            return database.single {
                database.db.update(MessageRecords) {
                    set(MessageRecords.active, false)
                    where { MessageRecords.active }
                }
            }
        }

        /**
         * Get all message records within the given range , ordered by time started, descending
         * @param lowerBound The lower time bound
         * @param upperBound The upper time  bound
         * @param start The starting index
         */
        fun getEntries(
            lowerBound: Instant = minInstant,
            upperBound: Instant = maxInstant,
            start: Int = 0,
            count: Int = Int.MAX_VALUE
        ): Observable<MessageRecord> {
            return database.observe {
                from(MessageRecords).select()
                    .where {
                        (MessageRecords.started greaterEq lowerBound) and
                                (MessageRecords.ended lessEq upperBound)
                    }
                    .orderBy(MessageRecords.active.desc(), MessageRecords.started.desc())
                    .limit(start, count)
                    .map { MessageRecords.createEntity(it) }
            }
        }

        fun count(): Single<Int> {
            return database.single { sequenceOf(MessageRecords).count() }
                .subscribeOn(database.scheduler)
        }

        fun deleteAll(): Single<Int> {
            return database.single { this.deleteAll(MessageRecords) }
        }

        private object MessageRecords : BaseTable<MessageRecord>(DB_TABLE_NAME) {
            val id = int("_id").primaryKey()
            val message = varchar("message")
            val priority = varchar("priority")
            val subtext = varchar("subtext")
            val started = timestamp("started")
            val ended = timestamp("ended")
            val active = boolean("active")

            override fun doCreateEntity(row: QueryRowSet, withReferences: Boolean): MessageRecord {
                return of(
                    row[message] ?: "",
                    Priority.valueOf(
                        row[priority]
                            ?: Priority.NONE.name
                    ),
                    row[subtext] ?: "",
                    row[started] ?: instantNow,
                    row[ended] ?: instantNow,
                    row[active] ?: false,
                    row[id] ?: 0
                )
            }
        }
    }
}
