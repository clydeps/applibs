package com.controlj.data

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 7/5/18
 * Time: 16:44
 */

object Constants {
    val INVALID_DATA = -65536.0   // chosen so lower 16 bits are zero
    val INVALID_DATA_INT = 32768   // used only in comms channels 16 bits wide
    val INVALID_POS = 0x40000
}


