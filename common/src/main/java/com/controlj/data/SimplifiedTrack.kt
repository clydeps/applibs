package com.controlj.data

import org.threeten.bp.Duration
import kotlin.math.abs

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-08-16
 * Time: 09:17
 *
 * Store a simplified track. As points are added, if the new point represents a straight line extension of
 * the track between the last two points, replace the last point with the new one.
 */
class SimplifiedTrack(points: Iterable<TrackPoint> = listOf()) {

    // the list of points defining this track
    private var points: MutableList<TrackPoint> = mutableListOf()

    init {
        points.forEach { add(it) }
    }
    // retrieve or set the current list of points
    var pointList: List<TrackPoint>
        get() = points.toList()
        set(value) {
            points.clear()
            points.addAll(value)
        }


    /**
     * The number of points in the track
     */

    val size: Int
        get() = points.size

    /**
     * The number of points in the track prior to trimming.
     */

    var residual: Int = 0
        private set

    /**
     * Clear the point list
     */
    fun clear() {
        points.clear()
    }

    /**
     * Clear current list and add new points
     */

    /**
     * Trim the list to the minimum number of points.
     */

    fun trim() {
        points = points.takeLast(MIN_POINTS).toMutableList()
        residual = points.size
    }

    /**
     * Add a new point to the list.
     */
    fun add(p: TrackPoint) {

        if (!p.valid || p.latitude == 0.0 && p.longitude == 0.0)
            return
        /**
         * If the track is empty, use what we have.
         */
        if (points.isEmpty()) {
            points.add(TrackPoint.from(p))
            return
        }
        val first = points.first()
        /**
         * If the track is quite small, and the speed between this point and the origin exceeds the speed of sound,
         * drop the track so far. This will typically occur when a spurious location has started the track.
         */
        if (points.size < 50 && p.distanceTo(first) / Duration.between(first.timestamp, p.timestamp).seconds > 350.0) {
            points.clear()
            points.add(TrackPoint.from(p))
            return
        }
        // include the first few points always.
        if (points.size < MIN_POINTS) {
            points.add(TrackPoint.from(p))
            return
        }
        val last = points.last()
        val prev = points[points.size - 2]
        val pprev = points[points.size - 3]
        val duration = Duration.between(prev.timestamp, last.timestamp).seconds
        if (duration > 60L) {
            points.add(TrackPoint.from(p))
            return
        }
        val trackDiff = abs(pprev.bearingTo(prev) - prev.bearingTo(p)) * duration.toInt()
        if (trackDiff > TRACK_DIFF) {
            points.add(TrackPoint.from(p))
            return
        }
        // get direction of climb, previous segment
        // if vertical direction is not the same, add the new point
        val prevVdir = (last.altitude.toInt() / ALT_DIFF) >= (prev.altitude.toInt() / ALT_DIFF)
        val newVdir = (p.altitude.toInt() / ALT_DIFF) >= (last.altitude.toInt() / ALT_DIFF)
        if (prevVdir != newVdir) {
            points.add(TrackPoint.from(p))
            //debug("trackDif = $trackDiff, preDir = $prevVdir, newDir=$newVdir, points = ${points.size}")
            return
        }
        // replace the last point with the new one.
        points[points.size - 1] = TrackPoint.from(p)
        residual = (residual - 1).coerceAtLeast(0)        // last point is now new.
    }

    companion object {

        const val TRACK_DIFF = 5.0       // heading changes less than this are disregarded.
        const val MIN_POINTS = 5               // include the first few points unconditionally.
        const val ALT_DIFF = 7                 // ignore altitude differences less than this
    }

}
