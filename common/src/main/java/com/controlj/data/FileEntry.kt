package com.controlj.data

import com.controlj.utility.asUInt
import okio.Buffer
import okio.Source
import okio.Timeout
import org.threeten.bp.Instant
import java.io.InputStream
import kotlin.math.round

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-05-27
 * Time: 17:31
 *
 * An interface describing a "file" which may not exist on disk as such.
 *
 *
 */
interface FileEntry: Comparable<FileEntry> {
    val name: String    // the friendly name of the file
    val fileName: String
        get() = name
    val folder: String
        get() = ""
    val length: Long     // length - may be approximate - in bytes
    fun getSource(): Source // get a source of data
    fun delete()        // delete this file
    fun save()          // save to the database
    val token: String   // a token that can be used to recreate this object
    val timeStamp: Instant
    val exact: Boolean  // true if the length is exact
        get() = false

    override fun compareTo(other: FileEntry): Int {
        return timeStamp.compareTo(other.timeStamp)
    }

    // get a stream to read this
    fun getInputStream(): InputStream {
        return object : InputStream() {
            var count = 0L
            val buffer = Buffer()
            val source = getSource()

            override fun read(): Int {
                if (exact && count == length)
                    return -1
                if (buffer.size == 0L)
                    if (source.read(buffer, 8192) <= 0)
                        return -1
                count++
                return buffer.readByte().asUInt()
            }

            override fun close() {
                source.close()
            }
        }
    }

    val sizeString: String
        get() = when {
            length < 1500 ->
                "${length}B"
            length < 1500000 -> "${round((length) / 1000.0).toInt()}K"
            else -> "${round((length) / 1000000.0).toInt()}M"
        }

    companion object {

        /**
         * Get a buffered source of data from this entry that can be used for HTTP requests etc.
         */
        fun getSource(byteStream: CloseableIterator<ByteArray>): Source {
            return object : Source {

                private var total: Long = 0L
                private val timeout = Timeout()
                private var data: ByteArray? = null

                override fun timeout(): Timeout {
                    return timeout
                }

                override fun close() {
                    byteStream.close()
                }

                override fun read(sink: Buffer, byteCount: Long): Long {
                    if (byteCount == 0L)
                        return 0
                    var buf = data
                    data = null
                    if (buf == null) {
                        if (!byteStream.hasNext()) {
                            return -1
                        }
                        buf = byteStream.next()
                    }
                    if (byteCount >= buf.size) {
                        sink.write(buf, 0, buf.size)
                        total += buf.size
                        return buf.size.toLong()
                    }
                    sink.write(buf, 0, byteCount.toInt())
                    data = buf.copyOfRange(byteCount.toInt(), buf.size)
                    total += byteCount
                    return byteCount
                }
            }
        }
    }
}
