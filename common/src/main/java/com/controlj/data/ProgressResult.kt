package com.controlj.data

/**
 * Created by clyde on 10/11/17.
 */

open class ProgressResult(val message:String, val percent: Int = -1)
