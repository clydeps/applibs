package com.controlj.data

import org.threeten.bp.Instant

/**
 * A position with an altitude and a timestamp
 */

interface TrackPoint : Position {
    val altitude: Double            // altitude in meters
    val timestamp: Instant

    /**
     * Convert to serialised byte array
     */
    override fun bytes(): ByteArray {
        with(Buffers.allocate(BYTE_SIZE)) {
            putFloat(latitude.toFloat())
            putFloat(longitude.toFloat())
            putFloat(altitude.toFloat())
            instant = timestamp
            return array()
        }
    }

    companion object {

        const val BYTE_SIZE = 20
        /**
         * Construct a 3D position from a Location. Always returns a new object
         */
        fun from(location: TrackPoint): TrackPoint {
            return of(location.latitude, location.longitude, location.altitude, location.timestamp)
        }

        fun from(bytes: ByteArray): TrackPoint {
            with(Buffers.bufferOf(bytes)) {
                return of(float.toDouble(), float.toDouble(), float.toDouble(), instant)
            }
        }

        fun of(latitude: Double, longitude: Double, altitude: Double, timestamp: Instant): TrackPoint {
            return object : TrackPoint {
                override val latitude = latitude
                override val longitude = longitude
                override val altitude: Double = altitude
                override val timestamp: Instant = timestamp

                override fun equals(other: Any?): Boolean {
                    return other is TrackPoint &&
                            other.latitude == latitude &&
                            other.longitude == longitude &&
                            other.altitude == altitude &&
                            other.timestamp == timestamp
                }

                override fun hashCode(): Int {
                    return (latitude * longitude).toInt()
                }

                override fun toString(): String {
                    return "TrackPoint{lat=$latitude, lon=$longitude, alt=$altitude, time=$timestamp}"
                }
            }
        }
    }
}
