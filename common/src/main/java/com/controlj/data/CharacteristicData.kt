package com.controlj.data

import com.controlj.ble.Characteristic
import java.util.*

/**
 * Created by clyde on 10/5/17.
 */

data class CharacteristicData(val characteristic: Characteristic, val data: ByteArray) {
    override fun toString(): String {
        return "CharacteristicData(characteristic=$characteristic, data=${Arrays.toString(data)})"
    }

    fun asString(): String {
        return String(data)
    }
}
