package com.controlj.data

import com.controlj.GIS.riemann.Magvar
import com.controlj.data.Constants.INVALID_DATA
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import java.lang.Math.toDegrees
import java.lang.Math.toRadians
import java.lang.reflect.Type
import kotlin.math.PI
import kotlin.math.abs
import kotlin.math.asin
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt
import kotlin.math.tan

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-01
 * Time: 20:58
 *
 * Represents a position on the Earth's surface
 * @property latitude The latitude in degrees
 * @property longitude The longitude in degrees
 */
interface Position {
    val latitude: Double
    val longitude: Double

    // used for returning values from DistanceBearing
    data class Result(val distance: Double, val initialBearing: Double, val finalBearing: Double)

    /**
     * Calculate the distance to another [PositionA
     * @param other The other point
     * @return The distance between the points in meters
     */
    fun distanceTo(other: Position?): Double {
        if (other != null && valid && other.valid)
            return distanceTo(other.latitude, other.longitude)
        return INVALID_DATA
    }

    /**
     * Is this [Position] valid?
     */
    val valid: Boolean
        get() = latitude >= -90 && latitude <= 90 &&
            longitude >= -180 && longitude <= 180

    /**
     * Interpolate between this position and another
     * @param next The other position
     * @param frac The fraction of the distance
     * @return A new Position
     */
    fun interpolate(next: Position, frac: Double): Position {
        val invFrac = 1.0 - frac
        val lat = latitude * invFrac + next.latitude * frac
        val long = longitude * invFrac + next.longitude * frac

        return of(lat, long)
    }

    /**
     * Calculate the distance between this point and the given coordinates
     * @param lat The latitude of the other point
     * @param lon The longitude of the other point
     * @return The distance in meters
     */

    fun distanceTo(lat: Double, lon: Double): Double {
        if (!valid || lat == INVALID_DATA || lon == INVALID_DATA)
            return INVALID_DATA
        return getDistanceBearing(this.latitude, this.longitude, lat, lon).distance
    }

    /**
     * Calculate the bearing to another point. Note that this is an average - on a great circle
     * the bearing will change over distance
     * @param lat The latitude of the other point
     * @param lon The longitude of the other point
     * @return The bearing to the other point from this point (start of a great circle)
     *
     */
    fun bearingTo(lat: Double, lon: Double): Double {
        return getDistanceBearing(latitude, longitude, lat, lon).initialBearing
    }

    /**
     * Calculate the bearing to another point. Note that this is an average - on a great circle
     * the bearing will change over distance
     * @param other The latitude of the other point
     * @return The average of the bearing to the other point and from the other point to this one.
     */
    fun bearingTo(other: Position?): Double {
        if (other != null && valid && other.valid)
            return bearingTo(other.latitude, other.longitude)
        return INVALID_DATA
    }

    /**
     * Given a speed, calculate the time required to travel to the other point
     * @param other The other point
     * @param speed The speed in m/s
     * @return The time in seconds
     */
    fun timeTo(other: Position?, speed: Double): Double {
        val dist = distanceTo(other)
        if (dist == INVALID_DATA || speed <= 0)
            return INVALID_DATA
        return dist / speed
    }

    /**
     * return a new location translated from this location by the given bearing and distance
     * @param bearing The bearing in degrees
     * @param distance The distance in meters
     */
    fun translate(bearing: Double, distance: Double): Position {
        /*
         o2 = asin( sin o1 * cos d + cos o1 * sin d * cos t )
         l2 = l1 + atan2( sin t * sin d * cos o1, cos d - sin o1 * sin o2 )
         where
         o is latitude,
         l is longitude,
         t is the bearing (clockwise from north),
         d is the angular distance d/R;
         d being the distance travelled, R the earth's radius
         */

        if (bearing == INVALID_DATA || distance == INVALID_DATA)
            return from(this)
        val o1 = toRadians(latitude)
        val l1 = toRadians(longitude)
        val t = toRadians(bearing)
        val d = distance / EARTH_RADIUS // normalize linear distance to radian angle

        val o2 = asin(sin(o1) * cos(d) + cos(o1) * sin(d) * cos(t))
        val l2 = l1 + atan2(sin(t) * sin(d) * cos(o1), cos(d) - sin(o1) * sin(o2))

        val l2_harmonised = (l2 + 3 * PI) % (2 * PI) - PI // normalise to -180..+180

        return of(toDegrees(o2), toDegrees(l2_harmonised))
    }

    /**
     * Return a new Position translated from this Position by an arc.
     * This is an approximation valid for short distances only.
     *
     * @param bearing The initial bearing in degrees
     * @param distance The distance travelled around the arc
     * @param angle The angle subtended by the arc
     */


    //fun translate(bearing: Double, distance: Double, angle: Double): Position {
        // calculate arc radius



    //}

    private fun normalBearing(to: Position): Double {
        val result = bearingTo(to)
        if (result < 180.0)
            return result
        return result - 360.0
    }

    fun easting(other: Position): Double {
        val lat = (abs(latitude) + abs(other.latitude)) / 2     // average the latitudes
        return sin(toRadians(other.longitude - longitude)) * cos(toRadians(lat)) * EARTH_RADIUS
    }

    /**
     * Calculate how far north the other point is from this point
     * @param other The other point
     * @return The distance in meters north. Negative values indicate it is south of here
     */
    fun northing(other: Position): Double {
        return sin(toRadians(other.latitude - latitude)) * EARTH_RADIUS
    }

    /**
     * Given an active, from and to waypoint, are we past this point?
     * If there is no active, or no from and no to we can't be past it.
     * Otherwise we draw a line bisecting the in and out tracks.
     */
    fun past(active: Position?, from: Position?, to: Position?): Boolean {
        if (active == null || !valid)
            return false
        val passageLine = when {
            from == null -> if (to == null) return false else active.normalBearing(to)
            to == null -> from.normalBearing(active)
            else -> ((active.normalBearing(to) + from.normalBearing(active)) / 2.0 + 90).let {
                when {
                    it > 180 -> it - 180
                    it < -180 -> it + 180
                    else -> it
                }
            }
        }
        val brgToWpt = normalBearing(active)
        //println("passageLine = $passageLine, brg=$brgToWpt")
        return when {
            passageLine > 0 -> when {
                brgToWpt > 0 -> brgToWpt < passageLine
                else -> brgToWpt < -passageLine
            }

            else -> when {
                brgToWpt < 0 -> brgToWpt > passageLine
                else -> brgToWpt > -passageLine
            }
        }
    }

    /**
     * Has the location moved from this previous one?
     */
    fun movedFrom(other: Position): Boolean {
        return latitude != other.latitude || longitude != other.longitude
    }

    /**
     * Magnetic variation for this location. East is positive.
     */

    class Serializer : JsonSerializer<Position>, JsonDeserializer<Position> {
        override fun serialize(src: Position, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
            val jsonObject = JsonObject()
            jsonObject.addProperty(LAT_NAME, src.latitude)
            jsonObject.addProperty(LON_NAME, src.longitude)
            return jsonObject
        }

        override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Position {
            val jsonobject = json.asJsonObject
            if (jsonobject.has(LAT_NAME) && jsonobject.has(LON_NAME))
                return of(jsonobject.get(LAT_NAME).asDouble, jsonobject.get(LON_NAME).asDouble)
            return invalid
        }
    }

    /**
     * Create a concrete implementation of Position
     *
     */

    data class of(override val latitude: Double, override val longitude: Double) : Position

    /**
     * serialise to a ByteArray
     */

    fun bytes(): ByteArray {
        with(Buffers.allocate(BYTE_SIZE)) {
            putFloat(latitude.toFloat())
            putFloat(longitude.toFloat())
            return array()
        }
    }

    companion object {
        const val LAT_NAME = "lat"
        const val LON_NAME = "lon"

        private const val BYTE_SIZE = 8     // bytes used to serialise

        fun from(oldPos: Position): Position {
            return of(oldPos.latitude, oldPos.longitude)
        }

        fun from(bytes: ByteArray): Position {
            with(Buffers.bufferOf(bytes)) {
                return of(float.toDouble(), float.toDouble())
            }
        }

        const val EARTH_RADIUS = 6371.01 * 1000 //meters

        /**
         * Get an invalid Position.
         */
        val invalid: Position = of(INVALID_DATA, INVALID_DATA)

        /**
         * Normalise a bearing to the range -360 to 360 degrees
         * @param bearing The bearing in degrees
         * @return The normalised value
         */
        fun normaliseBearing(bearing: Double): Double {
            var brg = bearing
            while (brg < 0)
                brg += 360.0
            return brg % 360
        }

        /**
         * Calculate the distance between two points, and the initial and final bearing of a great circle between the points.
         * @param latitude Latitude of first point
         * @param longitude of first point
         * @param lat2 Latitude of second point
         * @param lon2 Longitude of second point
         * @return An object with the distance in meters, initial and final bearings in degrees
         */
        fun getDistanceBearing(latitude: Double, longitude: Double, lat2: Double, lon2: Double): Result {
            val l1 = toRadians(longitude)
            val l2 = toRadians(lon2)

            val o1 = toRadians(latitude)
            val o2 = toRadians(lat2)

            val a = 6378137.0
            val b = 6_356_752.314245
            val f = 1 / 298.257223563

            val L = l2 - l1
            val tanU1 = (1 - f) * tan(o1)
            val cosU1 = 1 / sqrt(1 + tanU1 * tanU1)
            val sinU1 = tanU1 * cosU1
            val tanU2 = (1 - f) * tan(o2)
            val cosU2 = 1 / sqrt(1 + tanU2 * tanU2)
            val sinU2 = tanU2 * cosU2

            var l = L
            var lprev: Double
            var iterationLimit = 100.0
            var cosSqalpha: Double
            var rho: Double
            var cos2AlphaM: Double
            var cosRho: Double
            var sinRho: Double
            var sinL: Double
            var cosL: Double
            do {
                sinL = sin(l)
                cosL = cos(l)
                val sinSqRho =
                    cosU2 * sinL * (cosU2 * sinL) + (cosU1 * sinU2 - sinU1 * cosU2 * cosL) * (cosU1 * sinU2 - sinU1 * cosU2 * cosL)
                sinRho = sqrt(sinSqRho)
                if (sinRho == 0.0)
                    return Result(0.0, 0.0, 0.0)
                cosRho = sinU1 * sinU2 + cosU1 * cosU2 * cosL
                rho = atan2(sinRho, cosRho)
                val sinAlpha = cosU1 * cosU2 * sinL / sinRho
                cosSqalpha = 1 - sinAlpha * sinAlpha
                cos2AlphaM = cosRho - 2.0 * sinU1 * sinU2 / cosSqalpha

                if (java.lang.Double.isNaN(cos2AlphaM))
                    cos2AlphaM = 0.0  // equatorial line: cosSqalpha=0
                val C = f / 16 * cosSqalpha * (4 + f * (4 - 3 * cosSqalpha))
                lprev = l
                l =
                    L + (1 - C) * f * sinAlpha * (rho + C * sinRho * (cos2AlphaM + C * cosRho * (-1 + 2.0 * cos2AlphaM * cos2AlphaM)))
            } while (Math.abs(l - lprev) > 1e-12 && --iterationLimit > 0)

            if (iterationLimit == 0.0)
                throw IllegalStateException("Formula failed to converge")

            val uSq = cosSqalpha * (a * a - b * b) / (b * b)
            val A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)))
            val B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)))
            val deltaA =
                B * sinRho * (cos2AlphaM + B / 4 * (cosRho * (-1 + 2.0 * cos2AlphaM * cos2AlphaM) - B / 6 * cos2AlphaM * (-3 + 4.0 * sinRho * sinRho) * (-3 + 4.0 * cos2AlphaM * cos2AlphaM)))

            val distance = b * A * (rho - deltaA)

            var initialBearing = atan2(cosU2 * sinL, cosU1 * sinU2 - sinU1 * cosU2 * cosL)
            initialBearing = (initialBearing + 2 * PI) % (2 * PI) //turning value to trigonometric direction

            var finalBearing = atan2(cosU1 * sinL, -sinU1 * cosU2 + cosU1 * sinU2 * cosL)
            finalBearing = (finalBearing + 2 * PI) % (2 * PI)  //turning value to trigonometric direction
            return Result(distance, toDegrees(initialBearing), toDegrees(finalBearing))
        }

        // redux, using Positions.

        fun getDistanceBearing(start: Position, end: Position): Result {
            return getDistanceBearing(start.latitude, start.longitude, end.latitude, end.longitude)
        }

        /**
         * Calculate the cross-track error.
         *  @param start The start point of the course leg
         *  @param end The end point of the leg
         *  @param current The current position
         *  @return The distance from the current position to the nearest point on the leg course, in meters. A negative
         *  value means the error is to the left
         */

        fun crossTrackError(start: Position, end: Position, current: Position): Double {
            val leg = getDistanceBearing(start, end)
            val xtrack = getDistanceBearing(start, current)
            return asin(
                sin(
                    xtrack.distance / EARTH_RADIUS *
                        sin(toRadians(xtrack.initialBearing) - toRadians(leg.initialBearing))
                )
            ) * EARTH_RADIUS
        }
    }

    /**
     * calculate distance east from one point to another
     * Valid only for points close to each other
     * @param other The other point
     * @return The approximate distance in meters that [other] is east of this point
     */

}

/**
 * Intermediate points on a great circle
In previous sections we have found intermediate points on a great circle given either the crossing latitude or longitude. Here we find points (lat,lon) a given fraction of the distance (d) between them. Suppose the starting point is (lat1,lon1) and the final point (lat2,lon2) and we want the point a fraction f along the great circle route. f=0 is point 1. f=1 is point 2. The two points cannot be antipodal ( i.e. lat1+lat2=0 and abs(lon1-lon2)=pi) because then the route is undefined. The intermediate latitude and longitude is then given by:
A=sin((1-f)*d)/sin(d)
B=sin(f*d)/sin(d)
x = A*cos(lat1)*cos(lon1) +  B*cos(lat2)*cos(lon2)
y = A*cos(lat1)*sin(lon1) +  B*cos(lat2)*sin(lon2)
z = A*sin(lat1)           +  B*sin(lat2)
lat=atan2(z,sqrt(x^2+y^2))
lon=atan2(y,x)

 */

val Position.magneticVariation: Double get() = Magvar.current.variationDegreesCached(latitude, longitude)
val Position.latitudeString: String get() = "%.5f".format(latitude)
val Position.longitudeString: String get() = "%.5f".format(longitude)
val Position.description: String
    get() = "%c%02d\u00b0%02d %c%03d\u00b0%02d".format(
        if (latitude < 0) 'S' else 'N',
        abs(latitude.toInt()),
        (abs(latitude * 60.0).toInt() % 60),
        if (longitude < 0) 'W' else 'E',
        abs(longitude.toInt()),
        (abs(longitude * 60.0).toInt() % 60)
    )
