package com.controlj.data

import com.controlj.logging.CJLog.debug
import okio.Buffer
import java.io.OutputStream

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-13
 * Time: 16:20
 */
class MeasuredFileEntry(child: FileEntry) : FileEntry by child {
    override val length: Long by lazy {
        if (child.exact)
            child.length
        else {
            var len = 0L
            val source = child.getSource()
            val buffer = Buffer()
            val nullStream = object: OutputStream() {
                override fun write(b: Int) {
                    len++
                }
            }
            while(source.read(buffer, 1000) != -1L)
                buffer.writeTo(nullStream)
            source.close()
            debug("calculated length $len for ${child.name}")
            len
        }
    }
    override val exact: Boolean = true
}