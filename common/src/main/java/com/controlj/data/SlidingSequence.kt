package com.controlj.data

import com.controlj.data.SlidingSequence.Indexable
import com.controlj.logging.CJLog.debug
import com.controlj.rx.DisposedEmitter
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

/**
 * This class provides a way of managing a sliding window into a sequence. Intended particularly for managing a
 * pannable, zoomable view into a sequence of data items on an x-axis (e.g. time)
 *
 * The items to be managed need only implement the [Indexable] interface, where each item has a unique index.
 * The indices of the underlying set need not be contiguous, but must be monotonically increasing.
 *
 * @param dataSource The datasource to be used.
 * @param targetCount The desired number of points within the window.
 * @param msDelay The desired debounce delay in ms
 */
class SlidingSequence<T : Indexable, DS : SlidingSequence.DataSource<T>>(
    val dataSource: DS,
    val targetCount: Int = 256,
    val msDelay: Long = 400
) {

    /**
     * The base type of the items managed by a SlidingSequence.
     * @property index The index of this item in its set.
     */
    interface Indexable {
        val index: Int
    }

    /**
     * A datasource for items of type [T]. The datasource can be asynchronously queried for a given range,
     * with a specified maximum number of items to be returned, evenly spaced across the range
     */

    interface DataSource<FT : Indexable> {

        /**
         * Fetch a series of data items
         * @param first The index of the first desired item in the range
         * @param last The index of the last item
         * @param count The number of items desired. The generated sequence may contain more or less
         * than this number - it is regarded as a target, not a requirement
         */
        fun fetch(first: Int, last: Int, count: Int): Flowable<FT>

        /**
         * The first available index in the source
         */
        val firstIndex: Int

        /**
         * The last available index in the source
         */

        val lastIndex: Int

    }

    // the total range of indices in the dataset
    private val totalPoints = dataSource.lastIndex - dataSource.firstIndex + 1

    // the minimum scale (max zoom) - will result in 1:1 point mapping
    private val minScale = targetCount / totalPoints.toDouble()

    /**
     * The scale of the window. A value of 1.0 (the maximum) means the entire window, 0.5 means half the window.
     * Writing to this may trigger the generation of a data subset
     */

    private var backingScale: Double = 1.0      // a private backing value

    var scale: Double
        get() = backingScale
        set(value) {
            val limited = value.coerceIn(minScale..1.0)
            if (limited != backingScale) {
                backingScale = limited
                emitter.onNext(limited)
            }
        }

    /**
     * The offset of the center of window from the center of the range.
     * This may be set to values in the range (-0.5 .. 0.5)
     * Writing to this may trigger the generation of a data subset
     */

    private var backingOffset = 0.0

    var offset: Double
        get() = backingOffset
        set(value) {
            val limited = value.coerceIn(-0.5..0.5)
            if (backingOffset != limited) {
                backingOffset = limited
                emitter.onNext(limited)
            }
        }

    /**
     * calculate the position relative to the window given a position relative to the dataset.
     */

    fun windowPos(dataPos: Double): Double {
        return (dataPos - 0.5 - backingOffset) / backingScale + 0.5
    }

    /**
     * Calculate the position relative to the dataset, given a window position
     */

    fun dataPos(windowPos: Double): Double {
        return (windowPos - 0.5) * backingScale + 0.5 + backingOffset
    }

    /**
     * Pan the window by the specified factor, as a fraction of the window size.
     *
     * @param delta The amount to pan. Posiive values move the window down (i.e. towards smaller numbers)
     */

    fun onPan(delta: Double) {
        offset = backingOffset - delta * scale
    }

    /**
     * zoom the window about a given point. The zoom fraction, and the point, are expressed in window units.
     * The offset is adjusted so that the center location remains in the same location relative to the window.
     * @param center The center point of the zoom gesture, window coordinates
     * @param delta The fraction to zoom by
     */

    fun onZoom(center: Double, delta: Double) {
        val oldPos = dataPos(center)        // capture the current data position at the center pos
        backingScale = (scale / delta).coerceIn(minScale..1.0)       // update the scale
        val newPos = dataPos(center)
        offset += newPos - oldPos       // translate as required, trigger event
    }

    // The emitter for the observable sequence of zoom and pan events
    /**
     * Cache the base set of items when first required.
     */
    private var emitter: ObservableEmitter<Any> = DisposedEmitter()

    /**
     * A consumer may listen for changes in the window dataset through this property
     **/
    val observable =
        dataSource.fetch(dataSource.firstIndex, dataSource.lastIndex, targetCount).toList()
            .flatMapObservable { baseItems ->
                Observable.create { e: ObservableEmitter<Any> -> emitter = e }.startWithItem(Unit)
                    .map { getRange() }
                    .doOnNext { debug("$it") }
                    .debounce(msDelay, TimeUnit.MILLISECONDS)
                    .distinctUntilChanged()
                    .doOnNext { debug("$it") }
                    .flatMapSingle { dataSource.fetch(it.first, it.second, targetCount).toList() }
                    .observeOn(Schedulers.computation())
                    .map { combine(baseItems, it) }
            }
            .share()


    /**
     * Get a range of indices representing the current visible window.
     */

    private fun getRange(): Pair<Int, Int> {
        val first: Int = (dataPos(0.0) * totalPoints).roundToInt()
        val last: Int = (dataPos(1.0) * totalPoints).roundToInt()
        return Pair(first.coerceAtLeast(dataSource.firstIndex), last.coerceAtMost(dataSource.lastIndex))
    }

    /**
     * Combine the base list and a sublist into a single
     */
    private fun combine(baseList: List<T>, subList: List<T>): List<T> {
        if (subList.isEmpty())
            return baseList
        val subFirst = subList.first().index
        val subLast = subList.last().index
        return baseList.takeWhile { it.index < subFirst } +
            subList +
            baseList.takeLastWhile { it.index > subLast }

    }
}
