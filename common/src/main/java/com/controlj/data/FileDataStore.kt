package com.controlj.data

import com.controlj.settings.StringStore
import java.io.File
import java.io.IOException

/**
 * Created by clyde on 6/9/18.
 */
class FileDataStore(val dir: File): StringStore {
    init {
        if(!(dir.isDirectory || dir.mkdirs()))
            throw IOException("unable to create directory ${dir.path}")
    }
    override fun putString(key: String, value: String) {
        File(dir, key).writeText(value)
    }

    override fun getString(key: String, dflt: String): String {
        val file = File(dir, key)
        if(file.exists())
            return file.readText()
        return dflt
    }
}
