package com.controlj.data

import com.controlj.data.Buffers.Companion.ORDER
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.nio.ByteBuffer
import java.nio.ByteOrder


interface Buffers {

    companion object {
        val ORDER = ByteOrder.LITTLE_ENDIAN

        /**
         * Utility function to get a byteBuffer of a given size with the default endianness
         */

        fun allocate(len: Int): ByteBuffer {
            return ByteBuffer.allocate(len).order(ORDER)
        }

        fun bufferOf(bytes: ByteArray) : ByteBuffer {
            return ByteBuffer.wrap(bytes).order(ORDER)
        }
    }
}

var ByteBuffer.latLon: Position
    get() {
        val latitude = float.toDouble()
        val longitude = float.toDouble()
        return Position.of(latitude, longitude)
    }
    set(value) {
        putFloat(value.latitude.toFloat())
        putFloat(value.longitude.toFloat())
    }

/**
 * Retrieve a list of floats and return as a list of doubles
 */

var ByteBuffer.floatArray: List<Double>
    get() {
        val count = get()
        return (1..count).map { float.toDouble() }.toMutableList()
    }
    set(values) {
        put(values.size.toByte())
        values.forEach { putFloat(it.toFloat()) }
    }

var ByteBuffer.duration: Duration
    get() = Duration.ofMillis(getLong())
    set(value) {
        putLong(value.toMillis())
    }

var ByteBuffer.instant: Instant
    get() = Instant.ofEpochMilli(getLong())
    set(value) {
        putLong(value.toEpochMilli())
    }

