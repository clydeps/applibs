package com.controlj.data

import kotlin.math.max
import kotlin.math.min

/**
 * Created by clyde on 26/4/18.
 */
data class LocationBounds(val south: Double = 0.0, val west: Double = 0.0, val north: Double = 0.0, val east: Double = 0.0) {

    fun paddedBy(latitudeSpan: Double, longitudeSpan: Double): LocationBounds {
        return LocationBounds(south - latitudeSpan / 2, west - longitudeSpan / 2, north + latitudeSpan / 2, east + longitudeSpan / 2)
    }

    val width: Double
        get() = east - west

    val height: Double
        get() = north - south

    val isEmpty: Boolean
        get() = south >= north && west >= east

    fun contains(position: Position): Boolean {
        return position.longitude >= west && position.longitude <= east &&
                position.latitude >= south && position.latitude <= north
    }

    fun center(): Position {
        return Position.of(south + height / 2.0, west + width / 2.0)
    }

    companion object {
        fun containing(points: Iterable<Position>): LocationBounds {

            var north = -1000.0
            var south = 1000.0
            var west = 1000.0
            var east = -1000.0
            points.forEach {
                if (it.valid) {
                    north = max(north, it.latitude)
                    south = min(south, it.latitude)
                    east = max(east, it.longitude)
                    west = min(west, it.longitude)
                }
            }
            if (north < -90 || north > 90 || east < -180 || east > 180)
                return LocationBounds()
            return LocationBounds(south, west, north, east)
        }

        fun centeredOn(location: Position, latExtent: Double = 1.0, longExtent: Double = 1.0): LocationBounds {
            return LocationBounds(location.latitude - latExtent / 2, location.longitude - longExtent / 2,
                    location.latitude + latExtent / 2, location.longitude + longExtent / 2)
        }
    }
}
