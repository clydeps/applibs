package com.controlj.data

/**
 * Created by clyde on 18/6/17.
 */

open class Progress(val data: String, val progress: Int= -1, val isComplete: Boolean = false): Result {

    override fun toString(): String {
        return when {
            isComplete -> "$data: Completed"
            progress >= 0 -> "$data: $progress%"
            else -> data
        }
    }
}
