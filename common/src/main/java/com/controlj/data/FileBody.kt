package com.controlj.data

import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okio.Buffer
import okio.BufferedSink
import okio.Source
import okio.Timeout

/**
 * Created by clyde on 19/6/17.
 */

class FileBody(val fileEntry: FileEntry, val listener: ((Long) -> Unit)? = null) : RequestBody(), Source {

    val source = fileEntry.getSource()
    override fun timeout(): Timeout {
        return source.timeout()
    }

    override fun close() {
        source.close()
    }

    override fun read(sink: Buffer, byteCount: Long): Long {
        val result = source.read(sink, byteCount)
        listener?.invoke(result)
        return result
    }

    override fun contentType(): MediaType? {
        return "application/octet-stream".toMediaType()
    }

    override fun contentLength(): Long {
        return if (fileEntry.exact) fileEntry.length else -1
    }

    override fun writeTo(sink: BufferedSink) {
        sink.writeAll(this)
        close()
    }
}
