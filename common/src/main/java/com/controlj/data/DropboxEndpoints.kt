package com.controlj.data

import io.reactivex.rxjava3.core.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Created by clyde on 30/6/17.
 */

interface DropboxEndpoints {

    @FormUrlEncoded
    @POST("/oauth2/token")
    fun getToken(
            @Field("code") code: String,
            @Field("grant_type") grantType: String,
            @Field("client_id") appKey: String,
            @Field("client_secret") appSecret: String,
            @Field("redirect_uri") redirectUri: String): Single<DropboxAuthToken>

    companion object {
        val API_URL = "https://api.dropboxapi.com"
    }
}
