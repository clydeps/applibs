package com.controlj.data

import java.io.Closeable


/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-05-30
 * Time: 11:02
 *
 * An interface exposing an iterator with a close method, used to release resources before the
 * iterator is exhausted.
 */
interface CloseableIterator<T: Any>:Iterator<T>, Closeable