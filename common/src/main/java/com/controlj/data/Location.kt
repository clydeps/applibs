package com.controlj.data

import com.controlj.GIS.riemann.applyVariation
import com.controlj.utility.Units
import com.controlj.utility.formatted
import com.controlj.utility.instantNow
import org.threeten.bp.Duration
import org.threeten.bp.Instant

/**
 * Created by clyde on 9/5/17.
 */

interface Location : TrackPoint {

    data class Creator(
        val name: String,        // the name of the creator
        val priority: Int       // measure of accuracy/reliability. Higher is better
    )

    val creator: Creator        // Where this location came from
    val accuracyH: Double      // the horizontal accuracy in meters. negative or NaN means invalid
    val accuracyV: Double      // the vertical accuracy in meters. negative or NaN means invalid

    @Deprecated("Migrated to accuracyH", ReplaceWith("accuracyH"))
    val accuracy: Double get() = accuracyH

    val speed: Double                   // horizontal speed. Negative means invalid
    val track: Double                   // current track in degrees true

    /*
    ) : this(creator, latitude, longitude, altitude, horizontalAccuracy, verticalAccuracy) {
        speed = sqrt((northVel * northVel + eastVel * eastVel + vertVel * vertVel))
        track = (toDegrees(atan2(eastVel, northVel)) + 360.0) % 360.0
    } */

    /**
     * When this location was created, in milliseconds since the epoch
     */


    val heading: Double
        get() = track

    val magneticHeading: Double
        get() = heading.applyVariation(magneticVariation)

    /**
     * track in degrees magnetic
     */
    val magneticTrack: Double
        get() = track.applyVariation(magneticVariation)

    /**
     * Are we in flight?
     */

    val isInFlight: Boolean
        get() = speed > 25      // about 50 knots

    /**
     * Time since this location was created
     */
    val age: Duration
        get() = Duration.between(timestamp, instantNow)

    /**
     * Is this location valid? True if it has valid vertical and horizontal accuracies
     */
    override val valid: Boolean
        get() = !accuracyH.isNaN() && super.valid

    /**
     * return a new location translated from this location by the given bearing and distance
     */
    fun translate(bearing: Double, distance: Double, creator: Creator): Location {
        val npos = super.translate(bearing, distance)
        return of(npos.latitude, npos.longitude, altitude, speed, track, creator, timestamp, accuracyH)
    }


    override fun translate(bearing: Double, distance: Double): Position {
        return translate(bearing, distance, creator)
    }

    /**
     * Linear interpolate between this data and the next.
     * @param next  The next datapoint
     * @param frac The fraction along the way to interpolate to, ranges from 0.0(just this) to 1.0 (just next)
     */
    fun interpolate(next: Location, frac: Double): Location {
        val invFrac = 1.0 - frac
        val lat = latitude * invFrac + next.latitude * frac
        val long = longitude * invFrac + next.longitude * frac
        val alt = altitude * invFrac + next.altitude * frac

        val track = track * invFrac + next.track * frac
        val speed = speed * invFrac + next.speed * frac
        val newtimestamp = Instant.ofEpochMilli(
            (timestamp.toEpochMilli() * invFrac + next.timestamp.toEpochMilli() * frac).toLong()
        )
        return of(lat, long, alt, speed, track, creator, newtimestamp, accuracyH)
    }

    /**
     * Extrapolate a location using original track, and a given time
     */

    fun extrapolate(timeSecs: Double): Location {
        val newPosition = translate(track, speed * timeSecs)
        return from(
            latitude = newPosition.latitude,
            longitude = newPosition.longitude,
            timestamp = timestamp.plusMillis((1000 * timeSecs).toLong())
        )
    }

    /**
     * Create a concrete location from the given parameters.
     */
    data class of(
        override val latitude: Double,
        override val longitude: Double,
        override val altitude: Double,
        override val speed: Double,
        override val track: Double,
        override val creator: Creator,
        override val timestamp: Instant = instantNow,
        override val accuracyH: Double = 200.0,
        override val accuracyV: Double = 200.0
    ) : Location {
    }

    fun from(
        latitude: Double = this.latitude,
        longitude: Double = this.longitude,
        altitude: Double = this.altitude,
        speed: Double = this.speed,
        track: Double = this.track,
        creator: Creator = this.creator,
        timestamp: Instant = this.timestamp,
        accuracyH: Double = this.accuracyH,
        accuracyV: Double = this.accuracyV
    ): Location {
        return of(
            latitude,
            longitude,
            altitude,
            speed,
            track,
            creator,
            timestamp,
            accuracyH,
            accuracyV
        )
    }

    companion object {

        const val LOW_PRIORITY = 10
        const val DEFAULT_PRIORITY = 30        // The priority you have when you're not having a priority
        const val DEVICE_PRIORITY = 40      // for locations derived from internal GPS
        const val DEMO_PRIORITY = 50        // demo data replayed from log files
        const val GDL_PRIORITY = 60      // for locations derived from GDL-90 over wifi
        const val AVIATION_PRIORITY = 70      // for locations derived from BlueMAX data
        const val IFD_PRIORITY = 80      // for locations derived from IFD over WiFi
        const val HIGH_PRIORITY = 100       // Proper GPS navigators

        fun of(position: Position): Location {
            return of(position.latitude, position.longitude, 0.0, 0.0, 0.0, Creator("Position", LOW_PRIORITY))
        }

        fun equals(first: Location, second: Location): Boolean {
            if (first === second) return true
            with(first) {
                if (latitude != second.latitude) return false
                if (longitude != second.longitude) return false
                if (altitude != second.altitude) return false
                if (speed != second.speed) return false
                if (track != second.track) return false
                if (creator != second.creator) return false
                if (timestamp != second.timestamp) return false
                if (accuracyH != second.accuracyH) return false
                if (accuracyV != second.accuracyV) return false
            }
            return true
        }

        /**
         * Utility function to create new location from another, updating specific fields.
         */


        fun invalid(creator: Creator) = of(
            -100.0,
            -200.0,
            0.0,
            -2.0,
            -10.0,
            creator,
            accuracyH = Double.NaN,
            accuracyV = Double.NaN
        )

        val invalid = invalid(Creator("Invalid", 0))

        fun toString(loc: Location): String {
            loc.apply {
                return "${javaClass.simpleName}{${creator.name} lat=${latitude.formatted(3)} lon=${longitude.formatted(3)} " +
                    "alt=${altitude.formatted(unit = Units.Unit.FOOT)}ft " +
                    "spd=${speed.formatted(unit = Units.Unit.KNOT)}kt " +
                    "trk=${track.formatted(1)} ${timestamp} accH=${accuracyH.toInt()}}"
            }
        }
    }
}

