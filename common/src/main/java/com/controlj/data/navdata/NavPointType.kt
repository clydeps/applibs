package com.controlj.data.navdata

import java.util.Locale

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 7/11/19
 * Time: 16:34
 */
enum class NavPointType(val airport: Boolean = false, vararg val names: String) {
    AIRPORT(true, "AD", "AIRPORT"),
    AIRFIELD(true, "ALA"),
    VFR,
    IFR,
    NDB,
    VOR,
    DME,
    GBAS,
    GP,
    BALLOONBASE(true, "HAB"),
    HELIPAD(true, "HLS"),
    ILS,
    LOC,
    MM,
    OM,
    TAC,
    SEAPLANEBASE(true, "WATER"),
    UNKNOWN;

    companion object {

        private val nameMap = values().map { type -> type.names.map { it to type } + (type.name to type) }.flatten().toMap()
        /**
         * Find a type matching the given name.
         * @param name The name to map
         * @return The matching type, or UNKNOWN if no match
         */
        fun find(name: String?): NavPointType {
            if(name == null)
                return UNKNOWN
            return nameMap.get(name.uppercase()) ?: UNKNOWN
        }
    }
}
