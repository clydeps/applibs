package com.controlj.data.navdata

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.FlightPoint
import com.controlj.data.Position
import com.controlj.database.CJDatabase
import com.controlj.framework.FileUpdater
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import org.ktorm.dsl.QueryRowSet
import org.ktorm.dsl.and
import org.ktorm.dsl.eq
import org.ktorm.dsl.greaterEq
import org.ktorm.dsl.insert
import org.ktorm.dsl.lessEq
import org.ktorm.dsl.like
import org.ktorm.dsl.update
import org.ktorm.entity.filter
import org.ktorm.entity.find
import org.ktorm.entity.sequenceOf
import org.ktorm.entity.toList
import org.ktorm.schema.BaseTable
import org.ktorm.schema.double
import org.ktorm.schema.int
import org.ktorm.schema.varchar
import java.io.File

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-04
 * Time: 20:50

 */
interface NavPoint : FlightPoint {
    val id: Int                         // the database ID
    val elevation: Double               // elevation in feet
    val type: NavPointType              // type.

    /**
     * Save or update this to the database.
     */
    fun save(): Single<NavPoint> {
        return Single.create<NavPoint> { emitter ->
            emitter.onSuccess(
                when (id) {
                    -1 -> {
                        val id = database.db.insert(NavPoints) {
                            set(it.ident, ident)
                            set(it.description, description)
                            set(it.elevation, elevation)
                            set(it.latitude, latitude)
                            set(it.longitude, longitude)
                            set(it.type, type.name)
                        }
                        from(this@NavPoint, id)
                    }
                    else -> {
                        database.db.update(NavPoints) {
                            set(it.ident, ident)
                            set(it.description, description)
                            set(it.elevation, elevation)
                            set(it.latitude, latitude)
                            set(it.longitude, longitude)
                            set(it.type, type.name)
                            where { it.id eq id }
                        }
                        this@NavPoint
                    }
            })
        }.subscribeOn(database.scheduler)
    }

    companion object {

        /**
         * Create a NavPoint from constituents
         */
        fun of(
            ident: String,
            description: String,
            elevation: Double,
            latitude: Double,
            longitude: Double,
            type: NavPointType = NavPointType.UNKNOWN,
            id: Int = -1
        ): NavPoint {
            return object : NavPoint {
                override val id = id
                override val ident = ident
                override val description = description
                override val elevation = elevation
                override val latitude = latitude
                override val longitude = longitude
                override val type = type

                override fun toString(): String {
                    return ident
                }
            }
        }

        /**
         * Copy with optional update of id
         */
        fun from(other: NavPoint, newId: Int = other.id): NavPoint {
            return with(other) {
                of(ident, description, elevation, latitude, longitude, type, newId)
            }
        }


        const val DB_FILE_NAME = "waypoints.sqlite"
        const val META_FILE_NAME = "waypoints.meta"
        const val DB_TABLE_NAME = "wpts"

        // the database. Before opening, it is copied or updated from assets.
        val database by lazy {
            CJDatabase(DB_FILE_NAME) {
                FileUpdater.updateFileFromAssets(File(CJDatabase.directory, META_FILE_NAME))
            }
        }

        /**
         * The schema.
         */
        private object NavPoints : BaseTable<NavPoint>(DB_TABLE_NAME) {
            val id = int("_id").primaryKey()
            val ident = varchar("code")
            val description = varchar("name")
            val state = varchar("state")
            val elevation = double("elevation")
            val latitude = double("latitude")
            val longitude = double("longitude")
            val type = varchar("type")
            val priority = int("priority")

            override fun doCreateEntity(row: QueryRowSet, withReferences: Boolean): NavPoint {
                return of(
                    row[ident] ?: "",
                    listOf(row[description] ?: "", row[state] ?: "").joinToString(" "),
                    row[elevation] ?: INVALID_DATA,
                    row[latitude] ?: INVALID_DATA,
                    row[longitude] ?: INVALID_DATA,
                    NavPointType.find(row[type]),
                    row[id] ?: 0
                )

            }
        }


        /**
         * Return a list of points with the given ident.
         */
        fun find(ident: String): Observable<NavPoint> {
            return database.observe { sequenceOf(NavPoints).filter { it.ident eq ident }.toList() }
        }

        fun startsWith(ident: String): Observable<NavPoint> {
            return database.observe { sequenceOf(NavPoints).filter { it.ident like "$ident%" }.toList() }
        }

        /**
         * Get a point by ID
         */

        fun get(id: Int): Maybe<NavPoint> {
            return database.observe { listOf(sequenceOf(NavPoints).find { it.id eq id }).filterNotNull() }
                .firstElement()
        }

        /**
         * Find a list of NavPoints in the given bounding box
         * @param latMin Southernmost boundary of the box
         * @param lonMin Westernmost boundary
         * @param latMax Northernmost boundary
         * @param lonMax Easternmost boundary
         * @return An observable delivering all points found within the box
         */
        fun find(latMin: Double, lonMin: Double, latMax: Double, lonMax: Double): Observable<NavPoint> {
            return database.observe {
                sequenceOf(NavPoints).filter {
                    (it.latitude greaterEq latMin) and
                            (it.latitude lessEq latMax) and
                            (it.longitude greaterEq lonMin) and
                            (it.longitude lessEq lonMax)
                }.toList()
            }
        }

        /**
         * find the nearest point to the given location, within the specified distance
         * @param position The center point to use
         * @param distance Distance in meters from the point to consider
         * @param filter An optional filter, e.g. for waypoint type
         * @return The closest point to the center, within the bounds as given, if it exists
         */

        fun nearest(position: Position, distance: Double, filter: (NavPoint) -> Boolean = { true }): Maybe<NavPoint> {
            val latDiff = position.translate(0.0, distance).latitude - position.latitude
            val lonDiff = position.translate(90.0, distance).longitude - position.longitude
            return find(
                position.latitude - latDiff, position.longitude - lonDiff,
                position.latitude + latDiff, position.longitude + lonDiff
            )
                .filter(filter)
                .toList()
                .flatMapMaybe {
                    val result = it.minByOrNull { position.distanceTo(it.latitude, it.longitude) }
                    if (result == null)
                        Maybe.empty()
                    else
                        Maybe.just(result)
                }
        }
    }
}
