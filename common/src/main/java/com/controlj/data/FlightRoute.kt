package com.controlj.data

import com.controlj.GIS.riemann.applyVariation
import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.navdata.NavPoint
import com.google.gson.annotations.Expose
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import java.util.regex.Pattern
import kotlin.math.max
import kotlin.math.min

/**
 * Created by clyde on 22/3/18.
 */
open class FlightRoute<T : FlightPoint>(
    @Expose val creator: String = "Invalid",
    @Expose var priority: Int = Location.LOW_PRIORITY,
    points: Iterable<T> = listOf()
) {

    @Expose
    open val points: MutableList<T> = points.toMutableList()

    /*
    The index of the active waypoint
     */
    @Expose
    var active = -1

    val destination: FlightPoint?
        get() {
            return points.lastOrNull()
        }

    var activeWpt: FlightPoint?
        get() {
            return points.getOrNull(active)
        }
        set(value) {
            active = points.indexOf(value)
        }

    val isEmpty: Boolean
        get() = points.isEmpty()

    /**
     * Add a waypoint, optionally making it active
     */

    fun add(flightPoint: T, isActive: Boolean = false) {
        if (isActive)
            active = points.size
        points.add(flightPoint)
    }

    /**
     * Remove the waypoint at the specified index
     *
     */
    fun remove(index: Int) {
        points.removeAt(index)
        if (active >= points.size)
            active--
    }

    /**
     * Sequence the route so that the next waypoint after the active one becomes active.
     * If the current active wayppoint is the last one in the route it will remain active
     * If no waypoint is active the first waypoint wil be made active
     * @return true if the active waypoint has changed
     */
    fun sequenceToNext(): Boolean {
        if (active < points.size - 1) {
            active++
            return true
        }
        return false
    }

    /**
     * Get the bounding box of the current active leg, or the first leg if no leg active
     */

    fun activeBounds(): LocationBounds {
        if (points.size == 0)
            return LocationBounds(0.0, 0.0, 0.0, 0.0)
        if (points.size == 1 || active <= 0)
            return LocationBounds.centeredOn(points[0])
        val p0 = points[active]
        val p1 = points[active - 1]

        return LocationBounds(
            min(p0.latitude, p1.latitude),
            min(p0.longitude, p1.longitude),
            max(p0.latitude, p1.latitude),
            max(p0.longitude, p1.longitude)
        )
    }

    /**
     * Get the bounds of the entire route
     */

    fun bounds(): LocationBounds {
        if (points.size == 0)
            return LocationBounds()
        val north = points.map { it.latitude }.maxOrNull() ?: 0.0
        val south = points.map { it.latitude }.minOrNull() ?: 0.0
        val east = points.map { it.longitude }.maxOrNull() ?: 0.0
        val west = points.map { it.longitude }.minOrNull() ?: 0.0
        return LocationBounds(south, west, north, east)
    }

    /**
     * Are we past the current waypoint?
     * Calculate the average of the course to the wpt and from it (to the next wpt)
     * and subtract from the bearing of the current location (supplied) to the the current
     * active waypoint. If the absoluue difference is less than 90, we are past it.
     */

    open fun fromActive(current: Position): Boolean {
        return current.past(activeWpt, points.getOrNull(active - 1), points.getOrNull(active + 1))
    }

    /**
     * Get the desired track - i.e. the track to the current active waypoint from the previous waypoint.
     * If the current waypoint is the origin this is meaningless
     */
    open val desiredTrack: Double
        get() {
            if (points.size < 2 || active <= 0)
                return INVALID_DATA
            return points[active - 1].bearingTo(points[active])
        }

    open val desiredTrackMagnetic: Double
        get() {
            return desiredTrack.let {
                if (it == INVALID_DATA)
                    INVALID_DATA
                else
                    it.applyVariation(points[active].magneticVariation)
            }
        }

    /**
     * The total length of this route
     */
    val totalLength: Double
        get() = (0 until points.size - 1).sumOf { points[it].distanceTo(points[it + 1]) }

    /**
     * The distance from the current position to the destination. This is the sum of the distance from the current position
     * to the active waypoint, plus the length of the remaining legs.
     */
    fun destDist(position: Position): Double {
        if (points.isEmpty() || points.any { !it.valid })
            return INVALID_DATA
        val p = active.coerceIn(0 until points.size)
        return position.distanceTo(points[p]) +
            (p until points.size - 1).sumOf { points[it].distanceTo(points[it + 1]) }
    }

    /**
     * Get the time in seconds from the given position and speed to the destination
     * @param point The current position
     * @param speed The speed to use for the calculation. Will be derived from the position if it includes speed data.
     */

    fun destTime(point: Position, speed: Double = (point as? Location)?.speed ?: INVALID_DATA): Double {
        if (speed == INVALID_DATA)
            return INVALID_DATA
        val dist = destDist(point)
        if (dist == INVALID_DATA)
            return INVALID_DATA
        return dist / speed
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as FlightRoute<*>
        if (active != other.active) return false
        if (points.size != other.points.size)
            return false
        points.forEachIndexed { index, point ->
            if (point != other.points[index])
                return false
        }
        return true
    }

    override fun hashCode(): Int {
        return points.sumOf { it.hashCode() }
    }

    fun copy(): FlightRoute<FlightPoint> {
        val newPoints = ArrayList<FlightPoint>()
        points.forEach { newPoints.add(it.copy()) }
        val newRoute = FlightRoute<FlightPoint>(creator, priority, newPoints)
        newRoute.active = active
        return newRoute
    }

    override fun toString(): String {
        return "$creator: " + (points.mapIndexed { index, point ->
            point.ident + (if (index == active) "*" else "")
        }.joinToString(separator = "-"))
    }

    companion object {

        /**
         * Create a Flightroute from a list of waypoint names.
         * @param string The
         */
        fun fromString(string: String): Single<FlightRoute<NavPoint>> {
            val points = string.split(Pattern.compile("(\\s|[,;-])+"))
            return Observable.concat(points.map { NavPoint.find(it).take(1) })
                .toList()
                .map { FlightRoute("FromString", Location.DEFAULT_PRIORITY, it) }


        }
    }
}
