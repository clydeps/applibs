package com.controlj.data

/**
 * Created by clyde on 21/6/17.
 */

class UpdateInfo {
    var id: Int = 0

    var appkey: String = ""

    var version: Int = 0

    var changes: String = ""

    var filename: String = ""

    var url: String = ""

    override fun toString(): String {
        return "App: $appkey; Version: $version; Filename: $filename"

    }

}
