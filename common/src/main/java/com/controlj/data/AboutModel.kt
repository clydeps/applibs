package com.controlj.data

import com.controlj.framework.DeviceInformation
import com.controlj.framework.FilePaths
import com.controlj.graphics.TextStyle
import com.controlj.rx.MainScheduler
import com.controlj.settings.DataStore
import com.controlj.ui.ButtonDialogItem
import com.controlj.ui.DialogData
import com.controlj.ui.DialogData.Companion.OK
import com.controlj.ui.DialogData.Companion.dialog
import com.controlj.ui.DialogItem
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

object AboutModel {
    private var relnotesPath: String = "relnotes.txt"
    private var disclaimerPath: String = "disclaimer.txt"

    private fun genDialog(title: String, path: String): Single<DialogData> {
        return Single.fromCallable {
            val reader = FilePaths().openAsset(path)?.bufferedReader()
            val text = reader?.let {
                val result = it.readText()
                it.close()
                result
            } ?: ""
            dialog(DialogData.Type.MODAL, title) {
                addItem(DialogItem(text, textStyle = TextStyle.LEFT_ALIGNED))
            }
        }
                .subscribeOn(Schedulers.io())
                .observeOn(MainScheduler())
    }

    var relnotesShown: Boolean
        get() = DataStore().getInt(LATEST_VERSION_SEEN,
                DeviceInformation().appBuild) == DeviceInformation().appBuild
        set(value) {
            if (value)
                DataStore().putInt(LATEST_VERSION_SEEN, DeviceInformation().appBuild)
        }

    fun showRelnotes(always: Boolean) {
        if ((always || !relnotesShown) && relnotesPath.isNotBlank()) {
            genDialog("Release Notes", relnotesPath).flatMap { dialogData ->
                dialogData.positive = OK
                relnotesShown = true
                dialogData.singleObserver
            }.subscribe()
        }
    }

    fun showDisclaimer(delay: Long): Single<Boolean> {
        if (DataStore().getBoolean(DISCLAIMER_ACCEPTED))
            return Single.just(true)
        if (disclaimerPath.isBlank())
            return Single.just(true)

        return Single.timer(delay, TimeUnit.SECONDS).flatMap {
            genDialog("Accept terms and conditions", disclaimerPath).flatMap { dialogData ->
                dialogData.negative = ButtonDialogItem("Decline")
                dialogData.positive = ButtonDialogItem("Accept")
                dialogData.singleObserver
                        .map {
                            if (it == dialogData.positive) {
                                DataStore().putBoolean(DISCLAIMER_ACCEPTED, true)
                                true
                            } else
                                false
                        }
            }
        }
    }

    const val DISCLAIMER_ACCEPTED = "disclaimerAccepted"
    const val LATEST_VERSION_SEEN = "latestVersion"
}
