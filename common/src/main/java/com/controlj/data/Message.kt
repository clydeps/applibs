package com.controlj.data

import com.controlj.data.Message.Companion.ALARM_SOUND
import com.controlj.data.Message.Companion.CHIME_SOUND
import com.controlj.graphics.CColor

/**
 * Created by clyde on 15/4/18.
 *
 * Base class for messages ordered by priority
 */

data class Message(var text: String, val priority: Priority) : Comparable<Any> {
    override fun compareTo(other: Any): Int {
        if (other is Message) {
            if (other.priority != priority)
                return priority.compareTo(other.priority)
            return text.compareTo(other.text)
        }
        return -1
    }

    var subText: String = ""
    /**
     * If set, this message will be cancelled when dismissed.
     */
    @Transient
    var autoCancel = false

    override fun equals(other: Any?): Boolean {
        if(this === other) return true
        return other is Message && priority == other.priority && text == other.text
    }

    override fun hashCode(): Int {
        var result = text.hashCode()
        result = 31 * result + priority.hashCode()
        return result
    }

    override fun toString(): String {
        return "$text $subText".trim()
    }

    val isImportant: Boolean
        get() = priority != Priority.ADVISORY

    val color: Int
        get() = priority.color
    val backgroundColor: Int
        get() = priority.backgroundColor

    companion object {
        const val CHIME_SOUND = "chime"
        const val ALARM_SOUND = "alarm"
    }
}

enum class Priority(val backgroundColor: Int, val color: Int, val description: String, val sound: String = "") {
    NONE(CColor.rgb(200, 200, 200), CColor.CLEAR, ""),
    NORMAL(CColor.NORMAL, CColor.BLACK, "Normal", ""), // nothing to see here
    ADVISORY(CColor.CYAN, CColor.BLACK, "Advisory", CHIME_SOUND),                // something changed
    ACTION(CColor.CYAN, CColor.BLACK, "Action required", CHIME_SOUND),     // something needs attention
    CAUTION(CColor.CAUTION, CColor.BLACK, "Caution", CHIME_SOUND), // something needs watching
    ERROR(CColor.ALARM, CColor.WHITE, "Error", ALARM_SOUND),     // something has failed
    ALARM(CColor.ALARM, CColor.WHITE, "Alarm", ALARM_SOUND)     // something is wrong
}

