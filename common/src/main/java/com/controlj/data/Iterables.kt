package com.controlj.data

/**
 * Created by clyde on 26/6/18.
 */
inline fun <T> Iterable<T>.sumByLong(selector: (T) -> Long): Long {
    return map { selector(it) }.sum()
}
