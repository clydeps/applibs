package com.controlj.data

/**
 * Created by clyde on 21/8/18.
 */
interface Deletable {
    fun delete()
}
