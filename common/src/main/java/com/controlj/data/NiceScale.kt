package com.controlj.data

import kotlin.math.floor
import kotlin.math.log10
import kotlin.math.pow
import kotlin.math.roundToInt

/**
 * Created by clyde on 14/6/18.
 */
class NiceScale
/**
 * Instantiates a new instance of the NiceScale class.
 *
 * @param min the minimum data point on the axis
 * @param max the maximum data point on the axis
 */
    (var minPoint: Double = Double.MAX_VALUE, var maxPoint: Double = Double.MIN_VALUE, var maxTicks: Double = 10.0) {
    var tickSpacing: Double = 0.0
        private set
    val niceRange: Double
        get() = niceMax - niceMin
    val range: Double
        get() = maxPoint - minPoint
    var niceMin: Double = 0.0
        private set
    var niceMax: Double = 0.0
        private set
    val tickCount: Int
        get() = (niceRange / tickSpacing).roundToInt()

    /**
     * Calculate and update values for tick spacing and nice
     * minimum and maximum data points on the axis.
     */
    fun calculate() {
        this.tickSpacing = niceNum((maxPoint - minPoint) / (maxTicks - 1))
        this.niceMin = Math.floor(minPoint / tickSpacing) * tickSpacing
        this.niceMax = Math.ceil(maxPoint / tickSpacing) * tickSpacing
    }

    /**
     * Returns a "nice" number approximately equal to range
     *
     * @param range the data range
     * @return a "nice" number to be used for the data range
     */
    private fun niceNum(range: Double): Double {
        val multiplier = 10.0.pow(floor(log10(range)))
        val fraction = range / multiplier

        return when {
            fraction <= 1 -> 1.0
            fraction <= 2 -> 2.0
            fraction <= 3 -> 3.0
            fraction <= 6 -> 6.0
            else -> 10.0
        } * multiplier
    }

    /**
     * Given a value in the range, convert it to a fraction of the range
     */

    fun toFraction(value: Double): Double {
        return (value - minPoint) / range
    }

    fun fromFraction(frac: Double): Double {
        return frac * range + minPoint
    }
}

