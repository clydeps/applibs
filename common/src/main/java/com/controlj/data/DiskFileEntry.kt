package com.controlj.data

import com.controlj.framework.FilePaths
import okio.Buffer
import okio.Source
import okio.Timeout
import org.threeten.bp.Instant
import java.io.File
import java.io.FileInputStream
import java.io.OutputStream
import java.util.UUID

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-03
 * Time: 17:27
 */
open class DiskFileEntry(val file: File, override val folder: String = ".") : FileEntry {

    override val token: String get() = "file:$folder:${file.canonicalPath}"
    override val name: String = file.name
    override val exact: Boolean = true
    override val length = file.length()
    override val timeStamp: Instant = Instant.ofEpochSecond(file.lastModified())

    private val timeout = Timeout()
    override fun getSource(): Source {
        return object : Source {
            val inputStream = FileInputStream(file)
            var total = 0L
            override fun timeout(): Timeout {
                return timeout
            }

            override fun close() {
                inputStream.close()
            }

            override fun read(sink: Buffer, byteCount: Long): Long {
                val available = (length - total).coerceAtMost(byteCount)
                if (available == 0L)
                    return -1
                sink.readFrom(inputStream, available)
                total += available
                return available
            }
        }
    }

    override fun delete() {
        file.delete()
    }

    override fun save() {
        // ignore
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DiskFileEntry

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return file.hashCode()
    }

    companion object {
        fun from(entry: FileEntry): DiskFileEntry {
            val file = File(FilePaths().tempDirectory, UUID.randomUUID().toString())
            val stream: OutputStream = file.outputStream()
            entry.getInputStream().copyTo(stream)
            stream.close()
            return object : DiskFileEntry(file, entry.folder) {
                override val name: String = entry.name
                override val timeStamp: Instant = entry.timeStamp
            }
        }
    }
}
