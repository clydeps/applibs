package com.controlj.data;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import static com.controlj.logging.CJLog.logMsg;

/**
 * Created by clyde on 13/6/17.
 */

public class BmDatabase {
    static private final String[] createStrings = {
        "create table if not exists LOGFILES (ID int auto_increment primary key not null, FILENAME varchar(255) unique , UPLOADED boolean)"
    };
    public static final String SQLITE_JDBCDRIVER = "SQLite.JDBCDriver";
    public static final String BLUEMAX_DB = "bluemax.db";
    public static final String JDBC_SQLITE = "jdbc:sqlite:";
    private final File f;
    private Connection connection;

    public BmDatabase(File f) {
        this.f = f;
        try {
            Class.forName(SQLITE_JDBCDRIVER);
        } catch(ClassNotFoundException e) {
            logMsg("Could not find class " + SQLITE_JDBCDRIVER);

            return;
        }
        f = new File(f, BLUEMAX_DB);
        try {
            connection = DriverManager.getConnection(JDBC_SQLITE + f.getPath());
            createTables();
        } catch(SQLException e) {
            logSql(e);
        }
    }

    private void createTables() throws SQLException {
        Statement stmt = connection.createStatement();
        for(String s : createStrings) {
            logMsg("update: %s", s);
            stmt.executeUpdate(s);
        }
    }

    public void close() {
        try {
            connection.close();
        } catch(SQLException e) {
            logSql(e);
        }
    }

    public Completable update(String sql) {
        Completable observable = Completable.create(e -> {
            try {
                connection.createStatement().executeUpdate(sql);
            } catch(SQLException ex) {
                logSql(ex);
                e.onError(ex);
            }
            e.onComplete();
        });
        return observable
            .subscribeOn(Schedulers.io());
    }

    public Single<ResultSet> query(String sql) {
        Single<ResultSet> observable = Single.create(e -> {
            try {
                ResultSet set = connection.createStatement().executeQuery(sql);
                e.onSuccess(set);
            } catch(SQLException ex) {
                logSql(ex);
                e.onError(ex);
            }
        });
        return observable
            .subscribeOn(Schedulers.io());
    }

    private void logSql(SQLException e) {
        logMsg("Sql exception %s", e);
    }
}
