package com.controlj.data

/**
 * Created by clyde on 8/8/17.
 */

interface FlightPoint : Position, Comparable<FlightPoint> {

    val ident: String       // the code for this point
    val description: String // a description

    companion object {

        /**
         * Factory method
         */

        fun of(ident: String, latitude: Double, longitude: Double, description: String = ident): FlightPoint {
            return object : FlightPoint {
                override val ident = ident
                override val description = description
                override val latitude = latitude
                override val longitude = longitude

                override fun hashCode(): Int {
                    return ident.hashCode() + latitude.toInt() + longitude.toInt()
                }

                override fun equals(other: Any?): Boolean {
                    return (other as? FlightPoint)?.let {
                        ident == it.ident && latitude == it.latitude && longitude == it.longitude
                    } ?: false
                }

                override fun toString(): String {
                    return ident
                }
            }
        }

        fun of(ident: String, origin: Position, bearing: Double, distance: Double): FlightPoint {
            val point = origin.translate(bearing, distance)
            return of(ident, point.latitude, point.longitude)
        }
    }


    fun copy(): FlightPoint {
        return of(ident, latitude, longitude)
    }

    override fun compareTo(other: FlightPoint): Int = ident.compareTo(other.ident)
}
