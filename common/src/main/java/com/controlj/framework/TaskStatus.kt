package com.controlj.framework

/**
 * Possible statuses of a scheduled task
 */
enum class TaskStatus {
    Unknown,
    Scheduled,
    Running,
    Completed,
    Failed
}
