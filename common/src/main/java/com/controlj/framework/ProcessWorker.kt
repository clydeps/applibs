package com.controlj.framework

import io.reactivex.rxjava3.core.Single

/**
 * A base class for processing tasks.
 * @param identifier The task identifier
 * @param data A map of name/value pairs for the job
 */
abstract class ProcessWorker {

    /**
     * Do the actual work.
     * Should throw an exception on error, return on completion
     */
    abstract fun doWork(): Single<Boolean>

    /**
     * called when the job is stopped externally. Should try to cancel any work going on.
     */
    open fun onStopped() = Unit

    companion object {

        /**
         * run a process given the class name
         * @param action The name of the ProcessWorker concrete class to instantiate
         */
        fun runProcess(action: String): Single<Boolean> {
            val clazz = Class.forName(action)        // throws exception if not found
            return (clazz.getConstructor().newInstance() as ProcessWorker).doWork()
        }
    }
}
