package com.controlj.framework

import com.controlj.graphics.CColor

interface SystemColors {
    val label: Int
        get() = CColor.BLACK
    val secondaryLabel: Int
        get() = CColor.grey(0.3)
    val systemFill: Int
        get() = TODO("Not yet implemented")
    val secondarySystemFill: Int
        get() = TODO("Not yet implemented")
    val placeholderText: Int
        get() = CColor.grey(0.5)
    val systemBackground: Int
        get() = CColor.WHITE
    val secondarySystemBackground: Int
        get() = CColor.grey(0.8)
    val systemGroupedBackground: Int
        get() = TODO("Not yet implemented")
    val secondarySystemGroupedBackground: Int
        get() = TODO("Not yet implemented")
    val separator: Int
        get() = TODO("Not yet implemented")
    val opaqueSeparator: Int
        get() = TODO("Not yet implemented")
    val link: Int
        get() = TODO("Not yet implemented")
    val systemGray2: Int
        get() = TODO("Not yet implemented")
    val systemGray3: Int
        get() = TODO("Not yet implemented")
    val systemGray4: Int
        get() = TODO("Not yet implemented")
    val systemGray5: Int
        get() = TODO("Not yet implemented")
    val systemGray6: Int
        get() = TODO("Not yet implemented")

    fun init() {
        instance = this
    }

    companion object {
        private lateinit var instance: SystemColors

        operator fun invoke() = instance
    }
}

