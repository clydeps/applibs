package com.controlj.framework

import com.controlj.data.Progress
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.MainScheduler
import com.controlj.utility.FormRequest
import com.controlj.view.AppPermission
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.net.InetAddress
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 15/4/18.
 *
 * Classes implementing this interface provide services based on OS features.
 * Features exposed here generally are available even when the app is in background.
 */

interface BackgroundServiceProvider {

    /**
     * Check if a given permission is set
     */
    abstract fun isPermitted(appPermission: AppPermission): Single<Boolean>
    interface TaskToken

    /**
     * Start a background task immediately, asking the iOS for a time window.
     * @param identifier An identifier for the task.
     * @return a token identifying the task, null if it could not be started
     */

    fun startBackgroundTask(identifier: String): TaskToken? = object : TaskToken {}

    /**
     * Terminate a background task window
     * @param token The token returned from startBackgroundTask
     */

    fun endBackgroundTask(token: TaskToken) = Unit


    /**
     * Schedule a background processing task at a given time.
     *
     * A failure to schedule will be signalled with an exception.
     *
     * @param identifier An identifier for the task
     * @param action The fully qualified class name of the action to be performed
     * @param data Data for the job
     * @param atTime When to start the task. The default is now.
     * @param requiresNetwork If the task should be delayed until a network connection is available. Defaults to true
     * @param longRunning A hint that this task may take some time.
     * @return false if the task could not be scheduled
     */

    fun scheduleProcessingTask(
        action: String,
        atTime: Instant = Instant.now(),
        requiresNetwork: Boolean = true,
        longRunning: Boolean = true
    ): Boolean

    /**
     * Cancel a scheduled processing task.
     * @param action An identifier for the task to be cancelled.
     */

    fun cancelProcessingTask(action: String)

    /**
     * Get status of a background task request
     */
    fun getStatus(identifier: String): Single<TaskStatus>

    /**
     * Post an HTTP request.
     * @param postData The data for the POST request
     * @param identifier The identifier for the request.
     * @param emitter If non-null, progress reports will be posted here
     *
     */
    fun postRequest(
        postData: FormRequest,
        identifier: String,
        emitter: ObservableEmitter<Progress>? = null,
    )

    /**
     * Broadcast response to completed post request.
     */
    data class Response(val identifier: String, val response: Int)

    /**
     * Send a result
     */

    /**
     * GEt the battery charge level, in the range 0.0-1.0.
     * null is returned if the information is not available
     */

    val batteryLevel: Double?

    /**
     * Static Test for there being a network connection. Does not guarantee reachability
     */

    /**
     * Listen for changes in network status.
     */
    val networkObserver: Observable<Boolean>
        get() = Observable.interval(0, 60, TimeUnit.SECONDS).map { true }


    /** Check network availability for the given target. This will emit on initial call
     * or when there is a change in network status.
     *
     */
    fun isNetworkAvailable(target: String = NETWORK_TARGET): Observable<Boolean> {
        return networkObserver
            .map {
                it && try {
                    InetAddress.getByName(target)
                    true
                } catch (ex: Exception) {
                    false
                }
            }
            .subscribeOn(Schedulers.io())
            .share()
    }


    // true if the device has a separate external storage location (e.g. SD card.)
    // should return false on android if the SD card is merely emulated
    val hasExternalStorage: Boolean
        get() = false

    val debuggerConnected: Boolean get() = false

    fun init() {
        instance = this
    }

    companion object {
        const val NETWORK_TARGET = "google.com"

        lateinit var instance: BackgroundServiceProvider
        private val backgroundTasks = ConcurrentSkipListSet<String>()    // tracks background tasks

        fun isNetworkAvailable(target: String = NETWORK_TARGET): Observable<Boolean> =
            instance.isNetworkAvailable(target)

        /**
         * Get the current instance
         */

        operator fun invoke() = instance

        /**
         * Execute the supplied lambda on the main thread after the given time.
         * @param secs The time delay in seconds
         * @param lambda The block to execute
         */
        @Suppress("CheckResult")
        fun afterSecs(secs: Long, lambda: () -> Unit) {
            MainScheduler().scheduleDirect(lambda, secs, TimeUnit.SECONDS)
        }

        /**
         *  Run a background task, if not already running
         *  @param identifier An identifier for the task
         *  @param action The code to run
         *  @param onceOnly If true, don't run this task more than once.
         *  @param scheduler A scheduler to run the task on, default is Schedulers.io()
         *  @return true if the task was scheduled. It will not be scheduled if a task with the same
         *  identifier is already running
         */

        fun runBackground(
            identifier: String,
            action: () -> Unit,
            onceOnly: Boolean = false,
            scheduler: Scheduler = Schedulers.io()
        ): Boolean {
            if (backgroundTasks.contains(identifier))
                return false
            instance.startBackgroundTask(identifier)?.let { token ->
                logMsg("Running background task $identifier with token $token")
                backgroundTasks.add(identifier)
                scheduler.scheduleDirect {
                    try {
                        action()
                        instance.endBackgroundTask(token)
                        if (!onceOnly)
                            backgroundTasks.remove(identifier)
                        logMsg("Ended background task $identifier")
                    } catch (ex: Exception) {
                        logMsg("$identifier: $ex")
                    }
                }
                return true
            }
            return false
        }
    }
}
