package com.controlj.framework

interface DeviceInformation {
    val appBuild: Int
    val appVersion:  String
    val appName: String
    val deviceName: String

    fun init() {
        instance = this
    }

    companion object {
        lateinit var instance: DeviceInformation
        operator fun invoke() = instance
    }
}
