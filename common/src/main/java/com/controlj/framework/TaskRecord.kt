package com.controlj.framework

import com.controlj.database.CJDatabase
import com.controlj.database.timestamp
import com.controlj.utility.instantNow
import io.reactivex.rxjava3.core.Single
import org.ktorm.dsl.QueryRowSet
import org.ktorm.dsl.eq
import org.ktorm.dsl.insert
import org.ktorm.dsl.update
import org.ktorm.schema.BaseTable
import org.ktorm.schema.int
import org.ktorm.schema.varchar
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId

interface TaskRecord : Comparable<TaskRecord> {

    val identifier: String
    var status: TaskStatus
    val started: Instant
    var ended: Instant
    var id: Int

    fun save(): Single<TaskRecord> {

        return database.single {
            when (id) {
                0 -> {
                    val id = database.db.insert(TaskRecords) {
                        set(it.identifier, identifier)
                        set(it.ended, ended)
                        set(it.started, started)
                        set(it.status, status.name)
                    }
                    this@TaskRecord.id = id
                    this@TaskRecord
                }
                else -> {
                    database.db.update(TaskRecords) {
                        set(it.identifier, identifier)
                        set(it.ended, ended)
                        set(it.started, started)
                        set(it.status, status.name)
                        where { TaskRecords.id eq id }
                    }
                    this@TaskRecord
                }
            }
        }.subscribeOn(database.scheduler)
    }

    /**
     * Update the end time to now, and save.
     * Optionally update the active state
     * @param newState The new active state to apply
     */
    fun update(newState: TaskStatus): Single<TaskRecord> {
        status = newState
        ended = instantNow
        return save()
    }

    /**
     * Get the duration of an event
     */
    val duration: Duration
        get() = Duration.between(started, ended)

    /**
     * Default sort order is by started time descending, then message.
     */
    override fun compareTo(other: TaskRecord): Int {
        val i = other.started.compareTo(started)
        if (i != 0)
            return i
        return identifier.compareTo(other.identifier)
    }

    private data class Impl(
        override val identifier: String,
        override var status: TaskStatus,
        override val started: Instant,
        override var ended: Instant,
        override var id: Int
    ) : TaskRecord {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Impl

            if (identifier != other.identifier) return false
            if (status != other.status) return false

            return true
        }

        override fun hashCode(): Int {
            return identifier.hashCode()
        }
    }

    companion object {
        private val zone = ZoneId.of("UTC")

        internal const val DB_NAME = "TaskRecords.db"
        private const val DB_TABLE_NAME = "TaskRecords"

        internal object database : CJDatabase(DB_NAME) {
            override val updateStatements: List<List<String>> = listOf(
                listOf(
                    """
                                create table $DB_TABLE_NAME (
                                    _id integer primary key autoincrement not null,
                                    identifier varchar not null,
                                    started varchar not null,
                                    ended varchar not null,
                                    status varchar not null)
                            """,
                    """
                                create index identifier on $DB_TABLE_NAME (identifier)
                            """
                )
            )
        }

        fun of(
            identifier: String,
            status: TaskStatus = TaskStatus.Unknown,
            started: Instant = instantNow,
            ended: Instant = started,
            id: Int = 0
        ): TaskRecord = Impl(identifier, status, started, ended, id)


        private object TaskRecords : BaseTable<TaskRecord>(DB_TABLE_NAME) {
            val id = int("_id").primaryKey()
            val identifier = varchar("identifier")
            val started = timestamp("started")
            val ended = timestamp("ended")
            val status = varchar("status")

            override fun doCreateEntity(row: QueryRowSet, withReferences: Boolean): TaskRecord {
                return of(
                    row[identifier] ?: "",
                    TaskStatus.valueOf(row[status] as String),
                    row[started] ?: instantNow,
                    row[ended] ?: instantNow,
                    row[id] ?: 0
                )
            }
        }
    }
}
