package com.controlj.framework

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import java.lang.IllegalArgumentException

class  WorkQueue<T: Any> : Observable<T>() {
    private lateinit var observer: Observer<in T>
    private var disposed: Boolean = true
    override fun subscribeActual(observer: Observer<in T>) {
        this.observer = observer
        disposed = false
        observer.onSubscribe(object: Disposable {
            override fun isDisposed(): Boolean {
                return disposed
            }

            override fun dispose() {
                disposed = true
            }
        })
    }

    fun queue(work: T) {
        if(disposed)
            throw IllegalArgumentException("Queue is disposed")
        observer.onNext(work)
    }
}
