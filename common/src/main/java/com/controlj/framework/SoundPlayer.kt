package com.controlj.framework

import com.controlj.logging.CJLog.logMsg
import io.reactivex.rxjava3.internal.schedulers.SingleScheduler
import java.util.LinkedList
import java.util.Locale


interface SoundPlayer {

    sealed class Item(val name: String) {
        class Sound(name: String) : Item(name)
        class Speech(name: String) : Item(name)
    }

    /**
     * Play a sound asynchronously. Don't call this directly, use the companion function. This ensures
     * that it is only called on the main thread.
     * @param sound The name of the sound file, located in assets
     *
     */

    fun playSound(sound: String)

    /**
     * Convert text to speech.
     *
     * @param text The text to convert to speech
     */

    fun speak(text: String)

    /**
     * Start an audio session
     */

    fun startSession()

    /**
     * End an audio session
     */

    fun endSession()


    companion object {
        val scheduler = SingleScheduler()
        lateinit var instance: SoundPlayer

        operator fun invoke() = instance

        val enLocales = mapOf(
            "au" to "AU",
            "ie" to "GB",
            "nz" to "AU",
            "za" to "ZA",
            "gb" to "GB",
            "us" to "US",
        )

        val soundQueue = LinkedList<Item>()
        var maxVolume: Boolean = false
        var language: String = "en-" + Locale.getDefault().country.lowercase().let {
            enLocales[it.lowercase()] ?: "US"
        }

        private fun start(item: Item) {
            when (item) {
                is Item.Sound -> instance.playSound(item.name)
                is Item.Speech -> instance.speak(item.name)
            }
        }

        /**
         * Call back for completion of a sound or speech item
         */
        fun endSound() {
            scheduler.scheduleDirect {
                soundQueue.pop()
                soundQueue.peek()?.let {
                    start(it)
                } ?: instance.endSession()
            }
        }

        fun add(item: Item) {
            if (soundQueue.isEmpty()) {
                instance.startSession()
                soundQueue.add(item)
                start(item)
            } else
                soundQueue.add(item)
        }

        /**
         * Play a sound. The player will be dispatched asynchronously on the main thread.
         * @param sound The base name of the sound file.
         */
        fun playSound(sound: String) {
            logMsg("Playing sound $sound")
            scheduler.scheduleDirect {
                try {
                    add(Item.Sound(sound))
                } catch (e: Exception) {
                    logMsg("Sound play failed for $sound: $e")
                }
            }
        }

        fun speak(text: String) {

            logMsg("Speaking text $text with language $language")
            scheduler.scheduleDirect {
                try {
                    add(Item.Speech(text))
                } catch (e: Exception) {
                    logMsg("Speak text failed for $text: $e")
                }
            }
        }
    }
}
