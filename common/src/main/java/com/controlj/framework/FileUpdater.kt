package com.controlj.framework

import com.google.gson.GsonBuilder
import java.io.File

/**
 * This object manages copying of files from assets to working directory.
 * Each file is accompanied by a metadata file that is used to determine if it requires updating
 */
object FileUpdater {

    /**
     * Storage for metadata
     */
    data class Meta(
            val filename: String,        // the source filename
            val version: Int,           // the version of the file
            val description: String     // a description
    ) {
        private constructor() : this("", 0, "")      // for Gson's benefit
    }

    private val gson = GsonBuilder().create()

    /**
     * Check if there is a newer version of a file available and copy it and its metadata if so.
     * @param file The target metadata file. May not exist. It is expected that a corresponding asset file will
     * exist, e.g. if the old meta file is databases/xx.meta then a file with pathname databases/xx.meta will be looked
     * for in the assets folder.
     */
    fun updateFileFromAssets(oldMetaFile: File) {
        val targetDir = oldMetaFile.parentFile
        val newMetaFile =  oldMetaFile.name
        val newMeta = FilePaths().openAsset(newMetaFile)?.use {
            gson.fromJson<Meta>(it.reader(), Meta::class.java)
        } ?: return
        if (oldMetaFile.exists()) {
            val oldMeta = gson.fromJson<Meta>(oldMetaFile.readText(), Meta::class.java)
            if (newMeta.version <= oldMeta.version)
                return
        }
        // new metadata is newer than old. Copy the file first, then the metadata
        FilePaths.copyAssetUnzipping(newMeta.filename, targetDir)
        FilePaths.copyAssetUnzipping(newMetaFile, targetDir)
    }
}
