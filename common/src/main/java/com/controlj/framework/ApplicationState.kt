package com.controlj.framework

import com.controlj.logging.CJLog.debug
import com.controlj.rx.MainScheduler
import com.controlj.rx.observeBy
import com.controlj.viewmodel.ViewModel
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import java.util.concurrent.ConcurrentSkipListSet
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 23/4/18.
 *
 * @param visible If the app is visible to the user in this state
 * @param active If the user can interact with the app
 */
enum class ApplicationState(val visible: Boolean = false, val active: Boolean = false) {
    Stopped(),        // the app is completely stopped
    Background(),     // the app is running in background mode. No windows are visible
    Visible(true),        // the app is visible, but non-interactive
    Active(true, true);          // the app is visible and interacting with the user

    companion object {
        // The current application state
        val current = object : ViewModel.Data<ApplicationState>(Stopped) {
            override var data: ApplicationState
                get() = super.data
                set(value) {
                    super.data = value
                }
        }

        /**
         * Convenience properties
         */
        val isVisible: Boolean get() = current().visible
        val isActive: Boolean get() = current().active
        val isBusy: Boolean get() = isVisible && ApplicationBusy()


        /**
         * Set the busy state for a given object.
         * The global busy flag is true if any individual item is busy.
         */
        fun setBusy(key: String, state: Boolean) {
            ApplicationBusy[key] = state
        }
    }
}

object ApplicationBusy : ViewModel.BooleanData(false) {
    private val busyKeys = ConcurrentSkipListSet<String>()
    private var disposable = Disposable.disposed()

    /**
     * Reset busy, clear keys
     */
    fun clear() {
        busyKeys.clear()
        data = false
    }

    operator fun set(key: String, state: Boolean) {
        if (state) {
            busyKeys.add(key)
        } else
            busyKeys.remove(key)
        // debounce showing the busy indicator
        MainScheduler().scheduleDirect {
            if (busyKeys.isNotEmpty()) {
                if (disposable.isDisposed && !data) {
                    disposable = Single.timer(100L, TimeUnit.MILLISECONDS, MainScheduler())
                        .filter { busyKeys.isNotEmpty() }
                        .observeBy { data = true }
                }
            } else {
                disposable.dispose()
                data = false
            }
        }
    }

    operator fun get(key: String): Boolean {
        return busyKeys.contains(key)
    }
}

