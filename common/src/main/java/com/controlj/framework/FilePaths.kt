package com.controlj.framework

import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.util.zip.ZipInputStream
import kotlin.math.round

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 7/11/19
 * Time: 16:15
 *
 * Centralise the the locations for directories holding various things.
 */
interface FilePaths {

    /**
     * Get the directory in which to store databases.
     */

    val databaseDirectory: File

    /**
     * The directory in which to store general data
     */
    val dataDirectory: File

    /**
     * get a directory suitable for temporary files
     */

    val tempDirectory: File

    /**
     * Return a list of files in an asset directory
     * @param path  The folder path, relative to the assets folder
     * @return A list of files within that folder. Will be empty if the directory does not exist, or is empty
     */

    fun listAssets(path: String): List<String>

    /**
     * Open a file in the asset directory.
     * @param name The name of the file.
     * @return an InputStream, or null if the file was not found
     */

    fun openAsset(name: String): InputStream?

    companion object {
        internal lateinit var instance: FilePaths

        operator fun invoke() = instance

        private const val DB_VERSION_PREF = "database_version"

        /**
         * Return a list of files in a given subdirectory of the dataDirectory
         */

        fun listDataFiles(folder: String): List<File> {
            return (File(instance.dataDirectory, folder).listFiles() ?: arrayOf()).toList()
        }

        fun init(instance: FilePaths) {
            this.instance = instance
        }

        /**
         *  Copy a file, or unzip an archive, from the source in the assets folder to the target directory.
         *  @param source The name of the source file, relative to the assets folder
         *  @param targetDir The target directory.
         */
        fun copyAssetUnzipping(source: String, targetDir: File) {
            var isZipped = source.endsWith(".zip")
            val inputStream = instance.openAsset(source) ?: source.let {
                if (isZipped)
                    null
                else instance.openAsset("$source.zip")?.also { isZipped = true }
            } ?: return
            if (isZipped) {
                ZipInputStream(inputStream).use { input ->
                    var next = input.nextEntry
                    while (next != null) {
                        logMsg("Copying ${next.name} to ${targetDir.path}")
                        try {
                            val targetFile = File(targetDir, next.name)
                            require(targetFile.canonicalPath.startsWith(targetDir.canonicalPath))
                            targetFile.outputStream().use { output -> input.copyTo(output, 4096) }
                        } catch (ex: Exception) {
                            logException(ex)
                        } finally {
                            input.closeEntry()
                            next = input.nextEntry
                        }
                    }
                }
            } else {
                try {
                    val targetFile = File(source).name
                    inputStream.use { input ->
                        File(targetDir, targetFile).outputStream().use { output ->
                            input.copyTo(output)
                        }
                    }
                } catch (ex: FileNotFoundException) {

                }
            }
        }

        /*
        private fun databaseSetup() {
            BackgroundServiceProvider.runBackground(FilePaths::class.jvmName, {
                with(instance) {
                    if (!databaseDirectory.exists())
                        databaseDirectory.mkdirs()
                    val versionNo = Properties.getProperty(DB_VERSION_PREF).toIntOrNull() ?: -1
                    val databaseNames = listAssets("").filter { it.endsWith(".db") || it.endsWith(".db.zip") }
                    val lastDBVersion = DataStore.getInt(DB_VERSION_PREF)
                    databaseNames.map { it.trim() }.forEach { name ->
                        if (name.endsWith(".zip")) {
                            val input = ZipInputStream(openAsset(name))
                            var next = input.nextEntry
                            while (next != null) {
                                logMsg("Copying ${next.name}")
                                try {
                                    val outFile = File(databaseDirectory, next.name)
                                    if (lastDBVersion < versionNo || !outFile.exists() || outFile.length() < 1000) {
                                        val output = outFile.outputStream()
                                        input.copyTo(output, 4096)
                                        output.close()
                                    }
                                } catch (ex: IOException) {
                                    logException(ex)
                                } finally {
                                    input.closeEntry()
                                    next = input.nextEntry
                                }
                            }
                        } else {
                            val db = File(databaseDirectory, name)

                            if (lastDBVersion < versionNo || !db.exists() || db.length() < 1000) {
                                try {
                                    copyDataBase(name)
                                    logMsg("Copied database file %s", name)
                                    // any journal file must be deleted since it will not apply to the new one.
                                    File(databaseDirectory, "$name-journal").apply {
                                        if (exists())
                                            delete()
                                    }
                                } catch (e: IOException) {
                                    logException(e)
                                }

                            } else
                                logMsg("Skipped database file %s, length %d", db.name, db.length())
                        }
                    }
                    DataStore().putInt(DB_VERSION_PREF, versionNo)
                }
            })
        } */
    }
}


val File.size: String
    get() {
        val len = length()
        return when {
            len < 1500 ->
                "${len}B"

            len < 1500000 -> "${round((len) / 1000.0).toInt()}K"
            else -> "${round((len) / 1000000.0).toInt()}M"
        }
    }
