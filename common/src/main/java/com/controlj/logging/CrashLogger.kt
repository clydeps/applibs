package com.controlj.logging

import com.controlj.data.FileEntry
import com.controlj.data.MeasuredFileEntry
import com.controlj.framework.BackgroundServiceProvider
import com.controlj.framework.DeviceInformation
import com.controlj.logging.CJLog.isDebug
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedEmitter
import com.controlj.settings.Properties
import com.controlj.settings.Setting
import com.controlj.utility.FileParameter
import com.controlj.utility.FormData
import com.controlj.utility.FormRequest
import com.controlj.utility.Hash
import com.controlj.utility.Parameter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.File
import java.io.PrintWriter
import java.io.StringWriter
import java.net.URL
import java.util.ArrayList
import java.util.Locale

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 28/1/17
 * Time: 9:48 AM
 *
 * Installing an instance of this class as the JVM uncaught exception handler will log crashes to a remote
 * location. The destination and secret key must be supplied as arguments to the constructor.
 */
interface CrashLogger : Thread.UncaughtExceptionHandler {

    override fun uncaughtException(t: Thread, e: Throwable) {
        remoteLog(e)
    }

    fun initialise(data: Map<String, String>) {
        instance = this
        data.forEach { deviceInfo.add(Parameter(it.key, it.value)) }
        deviceInfo.add(Parameter("PACKAGE_NAME", DeviceInformation().appName))
        deviceInfo.add(Parameter("APP_VERSION_CODE", DeviceInformation().appVersion))
        deviceInfo.add(Parameter("BUILD", DeviceInformation().appBuild.toString()))
        deviceInfo.add(Parameter("DEVICE_NAME", DeviceInformation().deviceName))
    }

    companion object {
        operator fun invoke() = this
        private val disposable = Observable.create<FormData> { emitter ->
            this.emitter = emitter
        }
                .subscribeOn(Schedulers.io())
                .subscribe({ parameters ->
                    parameters.addAll(deviceInfo)
                    val log = CJLog.log
                    if (log != null)
                        parameters.add(Parameter("APPLICATION_LOG", log))
                    sendMessage(parameters)
                },
                        CJLog::logException
                )

        val ACRAKey by lazy {
            Properties.getProperty("crashlogger.secret", "ababa14e-f843-11e4-ba33-8b1f48b3f879")
        }
        val LOGHOST: String by lazy {
            Properties.getProperty("crashlogger.url", "https://us-central1-cjlogbackend.cloudfunctions.net/")
        }
        private lateinit var instance: CrashLogger


        val url = URL(URL(LOGHOST), "crashlog")
        private var emitter: ObservableEmitter<FormData> = DisposedEmitter()
        val deviceInfo = ArrayList<Parameter>()

        private fun sendMessage(parameters: FormData) {
            val pair = Hash.createHash(ACRAKey)
            parameters.add(Parameter("CUSTOM_DATA", "stamp=${pair.token}\nkey=${pair.hash}"))
            try {
                BackgroundServiceProvider().postRequest(
                    FormRequest(
                            url,
                            parameters,
                            headers = mapOf("Accept" to "*/*")
                    ),
                    "crashlogger"
                )
            } catch (e: Exception) {
                CJLog.logException(e)
            }
        }

        internal fun remoteLogMessage(message: String, vararg args: Any) {
            logMsg(message, *args)
            val parameters = FormData()
            parameters.add(Parameter("MESSAGE", String.format(Locale.US, message, *args)))
            parameters.add(Parameter("STACK_TRACE", "Message log"))
            sendMessage(parameters)
        }

        protected fun queueMessage(parameters: FormData) {
            parameters.add(Parameter("SETTINGS", Setting.asString()))
            emitter.onNext(parameters)
        }

        fun close() {
            disposable.dispose()
        }

        fun remoteLog(parameters: FormData, message: String) {
            if (!isDebug)
                instance.apply {
                    parameters.add(Parameter("MESSAGE", message))
                    parameters.add(Parameter("STACK_TRACE", "Remote message log"))
                    queueMessage(parameters)
                }
        }

        fun sendReport(message: String, email: String, fileEntries: Collection<FileEntry>) {
            instance.apply {
                val parameters = FormData(fileEntries.map { FileParameter(it.fileName, MeasuredFileEntry(it)) })
                parameters.add(Parameter("MESSAGE", message))
                parameters.add(Parameter("EMAIL", email))
                parameters.add(Parameter("STACK_TRACE", "Debug Report"))
                queueMessage(parameters)
            }
        }

        fun remoteLog(message: String, files: List<File> = listOf()) {
            if (!isDebug)
                instance.apply {
                    val parameters = FormData(files.filter { it.exists() }.map { FileParameter(it.name, it) })
                    parameters.add(Parameter("MESSAGE", message))
                    parameters.add(Parameter("STACK_TRACE", "Remote message log"))
                    queueMessage(parameters)
                }
        }

        fun remoteLog(throwable: Throwable, message: String = "") {
            CJLog.logException(throwable)
            if (!isDebug)
                instance.apply {
                    val parameters = FormData()
                    if (message.isNotBlank()) {
                        parameters.add(Parameter("MESSAGE", message))
                        logMsg(message)
                    }
                    val sw = StringWriter()
                    throwable.printStackTrace(PrintWriter(sw))
                    parameters.add(Parameter("STACK_TRACE", sw.toString()))
                    queueMessage(parameters)
                }
        }

        fun logException(throwable: Throwable) {
            remoteLog(throwable)
        }
    }
}
