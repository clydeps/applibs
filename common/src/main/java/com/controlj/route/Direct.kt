package com.controlj.route

import com.controlj.route.Procedure.Companion.packFix
import java.nio.ByteBuffer

/**
 * Created by clyde on 29/3/18.
 */
class Direct(val fix: Procedure.Fix, val constraint: AltitudeConstraint) : Procedure {
    override val kind = Procedure.Kind.Direct
    companion object {
        // bytebuffer is pointing past the Procedure type.
        fun unpack(buffer: ByteBuffer): Direct {
            val fix = Procedure.unpackFix(buffer)
            val constraint = AltitudeConstraint.unpack(buffer)
            return Direct(fix, constraint)
        }
    }

    override fun pack(buffer: ByteBuffer) {
        // buffer has the procedure type already
        packFix(fix, buffer)
        constraint.pack(buffer)
    }

    override fun toString(): String {
        return "DCT ${fix.ident} $constraint"
    }
}
