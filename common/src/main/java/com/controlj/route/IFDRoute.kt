package com.controlj.route

import com.controlj.logging.CJLog.logMsg
import com.controlj.utility.getString
import com.controlj.utility.toUInt
import java.nio.ByteBuffer

/**
 * Created by clyde on 29/3/18.
 */
class IFDRoute(val name: String) {

    companion object {
        const val MaxRecords = 128
        const val RouteNameLen = 16

        @JvmStatic
        fun decode(buffer: ByteBuffer): IFDRoute {
            val name = buffer.getString(RouteNameLen)
            val route = IFDRoute(name)
            val cnt = buffer.get().toUInt()
            for (i in 0 until cnt) {
                val type = buffer.get()
                when (type.toUInt()) {
                    Procedure.Kind.Origin.ordinal -> route.add(Origin.unpack(buffer))
                    Procedure.Kind.VfrDest.ordinal -> route.add(VfrDestination.unpack(buffer))
                    Procedure.Kind.IfrDest.ordinal -> route.add(IfrDestination.unpack(buffer))
                    Procedure.Kind.Direct.ordinal -> route.add(Direct.unpack(buffer))
                    Procedure.Kind.Airway.ordinal -> route.add(Airway.unpack(buffer))
                    Procedure.Kind.Sid.ordinal -> route.add(TransitionProcedure.unpack(Procedure.Kind.Sid, buffer))
                    Procedure.Kind.Star.ordinal -> route.add(TransitionProcedure.unpack(Procedure.Kind.Star, buffer))
                    Procedure.Kind.Approach.ordinal -> route.add(TransitionProcedure.unpack(Procedure.Kind.Approach, buffer))
                    Procedure.Kind.Hold.ordinal -> route.add(Hold.unpack(buffer))
                    else -> {
                        logMsg("Route entry %s", Procedure.Kind.values()[type.toUInt()])
                        buffer.position(buffer.position() + Procedure.RecordLen - 1)
                    }
                }
            }
            return route
        }
    }

    val procedures: MutableList<Procedure> = ArrayList()

    fun add(procedure: Procedure) {
        procedures.add(procedure)
    }

    override fun toString(): String {
        val sb = StringBuilder(name)
        sb.append(":")
        for (proc in procedures) {
            sb.append(' ')
            sb.append(proc.toString())
        }
        return sb.toString()


    }
}
