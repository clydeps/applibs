package com.controlj.route

import java.util.Locale

/**
 * Created by clyde on 29/3/18.
 */
open class Runway(val bearing: Int, val type: Type) {
    companion object {
        fun fromString(s: String): Runway {
            if (s.isBlank())
                return UNSPECIFIED
            if (s.length < 4 || s.length > 5 || !s.startsWith("RW"))
                throw IllegalArgumentException("Invalid runway syntax")
            val brg = s.substring(2, 4).toInt()
            val type = if (s.length == 4) Type.Plain else typeMap.get(s.elementAt(4)) ?: Type.Plain
            return Runway(brg, type)
        }

        val typeMap = Type.values().associateBy { it.indicator }
        val UNSPECIFIED = object : Runway(0, Type.Plain) {
            override fun toString(): String {
                return ""
            }
        }
    }

    init {
        if (bearing < 0 || bearing > 36)
            throw IllegalArgumentException("Invalid runway bearing $bearing")
    }

    override fun toString(): String {
        when (type) {
            Type.Plain -> return String.format(Locale.US, "RW%02d", bearing)
            else -> return String.format(Locale.US, "RW%02d%c", bearing, type.indicator)
        }
    }

    enum class Type(val indicator: Char) {
        Plain(0.toChar()),
        Left('L'),
        Right('R'),
        Center('C'),
        True('T'),
    }
}
