package com.controlj.route

import java.nio.ByteBuffer
import java.nio.ByteOrder

/**
 * Created by clyde on 29/3/18.
 */
data class AltitudeConstraint(val type: ConstraintType, val altitude: Double, val offset: Double) {

    enum class ConstraintType {
        None,
        At,
        AtOrAbove,
        AtOrBelow
    }

    companion object {
        @JvmStatic
        fun unpack(buffer: ByteBuffer): AltitudeConstraint {
            buffer.order(ByteOrder.BIG_ENDIAN)
            val c = buffer.get().toInt().toChar()
            val offset = buffer.short.toDouble() / 10.0
            val altitude = buffer.short.toDouble()
            buffer.get()    // unused
            return when (c) {
                'T' -> AltitudeConstraint(ConstraintType.At, altitude, offset)
                'A' -> AltitudeConstraint(ConstraintType.AtOrAbove, altitude, offset)
                'B' -> AltitudeConstraint(ConstraintType.AtOrBelow, altitude, offset)
                else -> AltitudeConstraint(ConstraintType.None, altitude, offset)
            }

        }
    }

    fun pack(buffer: ByteBuffer) {
        buffer.order(ByteOrder.BIG_ENDIAN)
        when (type) {
            ConstraintType.At -> buffer.put('T'.code.toByte())
            ConstraintType.AtOrBelow -> buffer.put('B'.code.toByte())
            ConstraintType.AtOrAbove -> buffer.put('A'.code.toByte())
            else -> buffer.put(0)
        }
        buffer.putShort((offset * 10).toInt().toShort())
        buffer.putShort(altitude.toInt().toShort())
        buffer.put(0)       // padding
    }

    override fun toString(): String {
        return when (type) {
            ConstraintType.At -> "/$altitude/"
            ConstraintType.AtOrBelow -> "/$altitude"
            ConstraintType.AtOrAbove -> "$altitude/"
            else -> ""
        }
    }
}
