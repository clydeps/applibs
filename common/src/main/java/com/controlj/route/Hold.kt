package com.controlj.route

import com.controlj.route.Procedure.Companion.NameLen
import com.controlj.route.Procedure.Companion.Ref1Len
import com.controlj.route.Procedure.Companion.Ref2Len
import com.controlj.route.Procedure.Companion.Ref3Len
import com.controlj.utility.addDouble
import com.controlj.utility.addString
import com.controlj.utility.advance
import com.controlj.utility.fill
import com.controlj.utility.getString
import java.nio.ByteBuffer

/**
 * Created by clyde on 29/3/18.
 */
class Hold(val fix: String, val useTime: Boolean, val legLength: Double, val inboundCourse: Double, val rightTurns: Boolean, val published: Boolean ) : Procedure {
    override val kind = Procedure.Kind.Hold

    companion object {
        // bytebuffer is pointing past the Procedure type.
        fun unpack(buffer: ByteBuffer): Hold {
            buffer.get()
            val fix = buffer.getString(NameLen)
            val course = buffer.getString(Ref1Len).toDouble()
            val length = buffer.getString(Ref2Len).toDouble()
            val rightTurns = buffer.getAscii() == 'R'
            val useTime = buffer.getAscii() == 'T'
            val published = buffer.getAscii() == 'P'
            buffer.advance(Ref3Len - 3)
            return Hold(fix, useTime, length, course, rightTurns, published)
        }
    }

    override fun pack(buffer: ByteBuffer) {
        // buffer has the procedure type already
        buffer.put(0)
        buffer.addString(fix, NameLen)
        buffer.addDouble(inboundCourse, Ref1Len)
        buffer.addDouble(legLength, Ref2Len)
        buffer.putAscii(if(rightTurns) 'R' else 'L')
        buffer.putAscii(if(useTime) 'T' else 'D')
        buffer.putAscii(if(published) 'P' else 'U')
        buffer.fill(Ref3Len - 3)
    }

    override fun toString(): String {
        return "HOLD at $fix $inboundCourse\u00B0 ${if(rightTurns) ":Right turns" else ""}"
    }
}

fun ByteBuffer.putAscii(value: Char) {
    put(value.code.toByte())
}

fun ByteBuffer.getAscii(): Char = get().toInt().toChar()
