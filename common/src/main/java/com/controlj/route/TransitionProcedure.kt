package com.controlj.route

import com.controlj.route.Procedure.Companion.NameLen
import com.controlj.route.Procedure.Companion.Ref1Len
import com.controlj.route.Procedure.Companion.Ref2Len
import com.controlj.route.Procedure.Companion.Ref3Len
import com.controlj.utility.addString
import com.controlj.utility.advance
import com.controlj.utility.fill
import com.controlj.utility.getString
import java.nio.ByteBuffer

/**
 * Created by clyde on 29/3/18.
 */
class TransitionProcedure(override val kind: Procedure.Kind, val name: String, val runwayTransition: String, val enrouteTransition: String) : Procedure {
    companion object {
        // bytebuffer is pointing past the Procedure type.
        fun unpack(kind: Procedure.Kind, buffer: ByteBuffer): TransitionProcedure {
            buffer.get()    // skip fix type
            val name = buffer.getString(NameLen)
            val rwy = buffer.getString(Ref1Len)
            val enroute = buffer.getString(Ref2Len)
            buffer.advance(Ref3Len)
            return TransitionProcedure(kind, name, rwy, enroute)
        }
    }

    override fun pack(buffer: ByteBuffer) {
        // buffer has the procedure type already
        buffer.put(0)
        buffer.addString(name, NameLen)
        buffer.addString(runwayTransition, Ref1Len)
        buffer.addString(enrouteTransition, Ref2Len)
        buffer.fill(Ref3Len)
    }

    override fun toString(): String {
        return "$kind $name $runwayTransition -> $enrouteTransition"
    }
}
