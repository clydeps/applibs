package com.controlj.route

import com.controlj.route.Procedure.Companion.NameLen
import com.controlj.route.Procedure.Companion.Ref1Len
import com.controlj.route.Procedure.Companion.Ref2Len
import com.controlj.route.Procedure.Companion.Ref3Len
import com.controlj.utility.addString
import com.controlj.utility.advance
import com.controlj.utility.fill
import com.controlj.utility.getString
import java.nio.ByteBuffer

/**
 * Created by clyde on 29/3/18.
 */
class Airway(val name: String, val exitFix: String) : Procedure {
    override val kind = Procedure.Kind.Airway

    companion object {
        // bytebuffer is pointing past the Procedure type.
        fun unpack(buffer: ByteBuffer): Airway {
            buffer.advance(NameLen+1)
            val name = buffer.getString(Ref1Len)
            val exitFix = buffer.getString(Ref2Len)
            buffer.advance(Ref3Len)
            return Airway(name, exitFix)
        }
    }

    override fun pack(buffer: ByteBuffer) {
        // buffer has the procedure type already
        buffer.fill(NameLen)
        buffer.addString(name, Ref1Len)
        buffer.addString(exitFix, Ref2Len)
        buffer.fill(Ref3Len)
    }

    override fun toString(): String {
        return "Airway: $name -> $exitFix"
    }
}
