package com.controlj.route

import com.controlj.data.FlightPoint
import com.controlj.data.FlightRoute
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-05
 * Time: 13:28
 *
 * A global object to distribute updated routes
 */
object RouteSource {

    internal var emitter: ObservableEmitter<FlightRoute<FlightPoint>> = DisposedEmitter()
    var lastRoute: FlightRoute<FlightPoint> = FlightRoute()
        internal set

    private val obs: Observable<FlightRoute<FlightPoint>> = Observable.create<FlightRoute<FlightPoint>> { emitter = it }
            .observeOn(MainScheduler.instance)
            .share()

    val routeObserver: Observable<FlightRoute<FlightPoint>>
        get() = obs.startWithItem(lastRoute)

    fun send(route: FlightRoute<FlightPoint>) {
        logMsg("Updating route $route")
        lastRoute = route
        emitter.onNext(route)
    }

    /**
     * Send current data to any listeners
     */
    fun refresh() {
        emitter.onNext(lastRoute)
    }
}
