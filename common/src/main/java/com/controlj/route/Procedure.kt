package com.controlj.route

import com.controlj.utility.addDouble
import com.controlj.utility.addString
import com.controlj.utility.getString
import java.nio.ByteBuffer

/**
 * Created by clyde on 29/3/18.
 */
interface Procedure {

    data class Fix(val type: FixType, val ident: String, val latitude: Double, val longitude: Double)
    companion object {

        // read len bytes form the buffer, convert to a string with trailing nulls removed.
        const val RecordLen = 39
        const val NameLen = 7
        const val Ref1Len = 12
        const val Ref2Len = 12
        const val Ref3Len = 6


        fun unpackFix(buffer: ByteBuffer): Fix {
            val type = buffer.get()
            when(type.toInt()) {
                FixType.NoFix.ordinal,
                FixType.VhfNavaid.ordinal,
                FixType.NdbNavaid.ordinal,
                FixType.Airport.ordinal,
                FixType.Waypoint.ordinal,
                FixType.UserWaypoint.ordinal,
                FixType.Fix.ordinal -> {}

                else -> throw IllegalArgumentException("Unknown fix type $type")
            }
            val fixType = FixType.values()[type.toInt()]
            val idar = buffer.getString(NameLen)
            val latar = buffer.getString(Ref1Len)
            val lonar = buffer.getString(Ref2Len)
            return Fix(fixType, idar, latar.toDouble(), lonar.toDouble())
        }

        fun packFix(fix: Fix, buffer: ByteBuffer) {
            buffer.put(fix.type.ordinal.toByte())
            buffer.addString(fix.ident, NameLen)
            buffer.addDouble(fix.latitude, Ref1Len)
            buffer.addDouble(fix.longitude, Ref2Len)
        }
    }

    enum class Kind {
        None,
        Origin,
        VfrDest,
        Direct,
        Airway,
        Sid,
        Star,
        Approach,
        Hold,
        Reserved,
        IfrDest,
        Orbit,
    }

    enum class FixType {
        NoFix,
        VhfNavaid,
        NdbNavaid,
        Airport,
        Waypoint,
        UserWaypoint,
        Fix
    }

    /**
     * Pack the procedure into a buffer. The buffer position is advanced by the number of bytes in the record
     */
    fun pack(buffer: ByteBuffer)

    /**
     * Get the procedure type
     *
     */

    val kind: Procedure.Kind
}
