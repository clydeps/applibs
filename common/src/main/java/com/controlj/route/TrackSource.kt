package com.controlj.route

import com.controlj.data.LocationBounds
import com.controlj.data.SimplifiedTrack
import com.controlj.data.TrackPoint
import com.controlj.graphics.CColor
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter

/**
 * A place to get the latest track, or an historical track of interest.
 */
object TrackSource {
    val TRACK_COLOR = CColor.argb(1.0, 0.0, 0.0, 0.7)
    val PREVIOUS_TRACK_COLOR = CColor.argb(1.0, 0.7, 0.7, 1.0)

    enum class TrackKey(val color: Int) {
        PREVIOUS_TRACK(PREVIOUS_TRACK_COLOR),
        STATIC_TRACK(TRACK_COLOR),
        CURRENT_TRACK(TRACK_COLOR)
    }

    data class TrackEvent(val key: TrackKey, val points: List<TrackPoint>, val bounds: LocationBounds = LocationBounds.containing(points))
    private var trackEmitter: ObservableEmitter<TrackEvent> = DisposedEmitter()
    private val currentTrack = SimplifiedTrack()

    /**
     * Restart the source. The track will be recreated from the supplied list, then updated as required.
     */
    fun restart(points: List<TrackPoint> = listOf()) {
        currentTrack.clear()
        points.forEach { currentTrack.add(it) }
        send(currentTrack.pointList)
    }

    fun update(point: TrackPoint) {
        currentTrack.add(point)
        send(currentTrack.pointList)
    }
    fun send(trackEvent: TrackEvent) {
        trackEmitter.onNext(trackEvent)
    }

    fun send(
            track: List<TrackPoint>,
            key: TrackKey = TrackKey.CURRENT_TRACK,
            bounds: LocationBounds = LocationBounds.containing(track)
    ) {
        send(TrackEvent(key, track, bounds))
    }

    /**
     * Resend the current track.
     */
    fun refresh() {
        send(listOf())      // resets listeners
        send(currentTrack.pointList)
    }

    val observer: Observable<TrackEvent> by lazy {
        Observable.create<TrackEvent> {
            trackEmitter = it
        }
                .observeOn(MainScheduler.instance)
                .share()
    }

}
