package com.controlj.route

import com.controlj.route.Procedure.Companion.Ref3Len
import com.controlj.route.Procedure.Companion.packFix
import com.controlj.utility.addString
import com.controlj.utility.getString
import java.nio.ByteBuffer

/**
 * Created by clyde on 29/3/18.
 */
class VfrDestination(val fix: Procedure.Fix, val runway: Runway) : Procedure {
    override val kind = Procedure.Kind.VfrDest

    companion object {
        // bytebuffer is pointing past the Procedure type.
        fun unpack(buffer: ByteBuffer): VfrDestination {
            val fix = Procedure.unpackFix(buffer)
            val rwy = Runway.fromString(buffer.getString(Procedure.Ref3Len))
            return VfrDestination(fix, rwy)
        }
    }

    override fun pack(buffer: ByteBuffer) {
        // buffer has the procedure type already
        packFix(fix, buffer)
        buffer.addString(runway.toString(), Ref3Len)
    }

    override fun toString(): String {
        return "Destination: ${fix.ident} $runway"
    }
}
