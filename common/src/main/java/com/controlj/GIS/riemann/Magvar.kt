/**
 * Calculate magnetic declination (aka variation) using the WMM 2005 model.
 */

package com.controlj.GIS.riemann

import com.controlj.data.Constants
import kotlin.math.abs


class Magvar {
/* construct a new Magvar for a specific time */


    private var lastLatitude = 1000.0
    private var lastLongitude = 1000.0
    private var lastVar = 0.0

    /**
     * Return a magnetic variation for a position in degrees. Use a cached version if the location is
     * not too far from the last one calculated.
     */
    fun variationDegreesCached(latitude: Double, longitude: Double): Double {
        if(latitude <-90.0 || latitude > 90.0 || longitude < -180.0 || longitude > 180.0)
            return 0.0
        if(abs(latitude - lastLatitude) > 0.25 || abs(longitude - lastLongitude) > 0.25) {
            lastLatitude = latitude
            lastLongitude = longitude
            lastVar = GeomagneticField(latitude.toFloat(), longitude.toFloat(), 0f).declination.toDouble()
        }
        return lastVar
    }

    companion object {
        val current: Magvar by lazy { Magvar() }

    }
}

/**
 * Convert magnetic to true
 * Values are in degrees, positive variations are easterly.
 * Returns a value in the range 0-360
 */
fun Double.removeVariation(variation: Double): Double {
    if(this == Constants.INVALID_DATA)
        return this
    return (this + variation + 360.0) % 360.0

}
/**
 * Apply a magnetic variation to a bearing
 * Values are in degrees, positive variations are easterly.
 * Returns a value in the range 0-360
 */
fun Double.applyVariation(variation: Double): Double {
    if(this == Constants.INVALID_DATA)
        return this
    return (this - variation + 360.0) % 360.0

}
