package com.controlj.GIS.riemann


import com.controlj.data.Position
import java.util.Locale

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Utilities for assisting in the parsing of latitude and longitude strings into Decimals.
 */
class CoordinateParser private constructor() {

    init {
        throw UnsupportedOperationException("Can't initialize class")
    }

    companion object {
        private val DMS = ("\\s*(\\d{1,3})\\s*[°d ]"
                + "\\s*([0-6]?\\d)\\s*['m ]"
                + "\\s*(?:"
                + "([0-6]?\\d(?:[,.]\\d+)?)"
                + "\\s*(?:\"|''|s)?"
                + ")?\\s*")
        private val DMS_SINGLE = Pattern.compile("^$DMS$", Pattern.CASE_INSENSITIVE)
        private val DMS_COORD = Pattern.compile("^$DMS([NSEOW])[ ,;/]?$DMS([NSEOW])$", Pattern.CASE_INSENSITIVE)
        private val POSITIVE = "NEO"
        private val delims = arrayOf(",", ";", "/", "	", " ")
        private val TAG = CoordinateParser::class.java.simpleName

        /**
         * This parses string representations of latitude and longitude values. It tries its best to interpret the values and
         * throws an exception if the values are invalid.
         *
         *
         * Coordinate precision will be 5 decimals at most, any more precise values will be rounded.
         *
         *
         * Supported standard formats are the following, with dots or optionally a comma as the decimal marker:
         *
         *  * 43.63871944444445
         *  * N43°38'19.39"
         *  * 43°38'19.39"N
         *  * 43d 38m 19.39s N
         *  * 43 38 19.39
         *
         *
         * @param latitude  The decimal latitude
         * @param longitude The decimal longitude
         * @return The parse result
         */
        @Throws(IllegalArgumentException::class)
        fun parseLatLng(latitude: String?, longitude: String?): Position {
            if (latitude == null || longitude == null || longitude.isBlank() or latitude.isBlank())
                throw IllegalArgumentException("Empty string")
            var lat: Float
            var lng: Float
            try {
                lat = java.lang.Float.parseFloat(latitude)
                lng = java.lang.Float.parseFloat(longitude)
            } catch (e: NumberFormatException) {
                // try degree minute seconds
                lat = parseDMS(latitude, true)
                lng = parseDMS(longitude, false)
            }

            return validateAndRound(lat, lng)
        }

        private fun inRange(lat: Float, lon: Float): Boolean {
            return lat <= 90f && lat >= -90f && lon <= 180f && lon >= -180f
        }

        private fun isLat(direction: String): Boolean {
            when(direction.uppercase()) {
                "N", "S" -> return true
                else -> return false
            }
        }

        private fun coordSign(direction: String): Int {
            return if (POSITIVE.contains(direction.uppercase())) 1 else -1
        }

        // 02° 49' 52" N	131° 47' 03" E
        @Throws(IllegalArgumentException::class)
        fun parseVerbatimCoordinates(coordinates: String?): Position {
            if (coordinates == null || coordinates.isBlank())
                throw IllegalArgumentException("Empty string")
            val m = DMS_COORD.matcher(coordinates)
            if (m.find()) {
                val dir1 = m.group(4)
                val dir2 = m.group(8)
                // first parse coords regardless whether they are lat or lon
                val c1 = coordFromMatcher(m, 1, 2, 3, dir1)
                val c2 = coordFromMatcher(m, 5, 6, 7, dir2)
                // now see what order the coords are in:
                return if (isLat(dir1) && !isLat(dir2)) {
                    validateAndRound(c1, c2)
                } else if (!isLat(dir1) && isLat(dir2)) {
                    validateAndRound(c2, c1)
                } else {
                    throw IllegalArgumentException("Invalid values")
                }
            } else if (coordinates.length > 4) {
                // try to split and then use lat/lon parsing
                for (delim in delims) {
                    val latlon = coordinates.split(delim.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    if (latlon.size == 2)
                        return parseLatLng(latlon[0], latlon[1])
                }
            }
            throw IllegalArgumentException("Invalid format")
        }

        private fun validateAndRound(lat: Float, lon: Float): Position {
            // collecting issues for result
            // round to 5 decimals
            // 0,0 is too suspicious
            // if everything falls in range
            if (inRange(lat, lon)) {
                return Position.of(lat.toDouble(), lon.toDouble())
            }

            // if lat is out of range, but in range of the lng,
            // assume swapped coordinates.
            // note that should we desire to trust the following records, we would need to clear the flag for the records to
            // appear in
            // search results and maps etc. however, this is logic decision, that goes above the capabilities of this method
            if ((lat > 90f || lat < -90f) && lon >= -90f && lon <= 90f) {
                return Position.of(lat.toDouble(), lon.toDouble())
            }

            // then something is out of range
            throw IllegalArgumentException("Values out of range")
        }

        /**
         * Parses a single DMS coordinate
         *
         * @param coord
         * @param lat
         * @return the converted decimal up to 5 decimals accuracy
         */
        protected fun parseDMS(coord: String, lat: Boolean): Float {
            var ncoord = coord
            val DIRS = if (lat) "NS" else "EOW"
            ncoord = ncoord.trim { it <= ' ' }.uppercase()

            if (ncoord.length > 3) {
                // preparse the direction and remove it from the string to avoid a very complex regex
                var dir = 'n'
                if (DIRS.contains(ncoord[0].toString())) {
                    dir = ncoord[0]
                    ncoord = ncoord.substring(1)
                } else if (DIRS.contains(ncoord[ncoord.length - 1].toString())) {
                    dir = ncoord[ncoord.length - 1]
                    ncoord = ncoord.substring(0, ncoord.length - 1)
                }
                // without the direction chuck it at the regex
                val m = DMS_SINGLE.matcher(ncoord)
                if (m.find()) {
                    return coordFromMatcher(m, 1, 2, 3, dir.toString())
                }
            }
            throw IllegalArgumentException()
        }

        private fun coordFromMatcher(m: Matcher, idx1: Int, idx2: Int, idx3: Int, sign: String): Float {
            return coordSign(sign) * dmsToDecimal(java.lang.Float.parseFloat(m.group(idx1)), java.lang.Float.parseFloat(m.group(idx2)), java.lang.Float.parseFloat(m.group(idx3)))
        }

        private fun dmsToDecimal(degree: Float, minutes: Float, seconds: Float): Float {
            return degree + minutes / 60f + seconds / 3600f
        }
    }
}
