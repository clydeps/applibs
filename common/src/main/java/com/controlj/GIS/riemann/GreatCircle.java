package com.controlj.GIS.riemann;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 12/10/13
 * Time: 1:32 PM
 */
public class GreatCircle {

	static final double LEPS = 1E-10;
	// WGS84 parameters

	static public final double axis = 6378137.0;    // meters
	static public final double flattening = 0.00335281066474;
	public static final int BEARING = 1;
	public static final int DISTANCE = 0;

	// get the bearing and distance in tp2 having travelled from tp1. Distance is returned in the first array element
	public static double[] getDistanceBearing(LatLong tp1, LatLong tp2) {
		return getDistanceBearing(tp1.getLatitude(), tp1.getLongtitude(), tp2.getLatitude(), tp2.getLongtitude());
	}

	public static double[] getDistanceBearingDegrees(double lat1, double lon1, double lat2, double lon2) {
        double[] result = getDistanceBearing(Math.toRadians(lat1), Math.toRadians(lon1), Math.toRadians(lat2), Math.toRadians(lon2));
        result[BEARING] = Math.toDegrees(result[BEARING]);
        return result;
	}
	public static double[] getDistanceBearing(double lat1, double lon1, double lat2, double lon2) {

		double[] result = new double[2];
		result[DISTANCE] = 0;
		result[BEARING] = 0;
		if(Math.abs(lon1 - lon2) <= LEPS && Math.abs(lat1 - lat2) <= LEPS)
			return result;
		if(Math.abs(lat1) <= LEPS && Math.abs(lat2) <= LEPS) {
			result[DISTANCE] = (Math.abs(lon1 - lon2) * axis); // Points are on the equator.
			result[BEARING] = 90;
			if(Math.abs(lon1 - lon2) > Math.PI) {
				if(lon1 < lon2)
					result[BEARING] = 270;
			} else if(lon1 > lon2)
				result[BEARING] = 270;
			return result;
		}
		final int MAX_ITERATIONS = 100;
		final double EPS = 0.5E-13;
		final double F = flattening;
		final double R = 1 - F;

		double tu1 = R * Math.sin(lat1) / Math.cos(lat1);
		double tu2 = R * Math.sin(lat2) / Math.cos(lat2);
		double cu1 = 1 / Math.sqrt(tu1 * tu1 + 1);
		double cu2 = 1 / Math.sqrt(tu2 * tu2 + 1);
		double su1 = cu1 * tu1;
		double s = cu1 * cu2;
		double baz = s * tu2;
		double faz = baz * tu1;
		double x = lon2 - lon1;
		for(int i = 0; i < MAX_ITERATIONS; i++) {
			final double sx = Math.sin(x);
			final double cx = Math.cos(x);
			tu1 = cu2 * sx;
			tu2 = baz - su1 * cu2 * cx;
			final double sy = Math.sqrt(tu1 * tu1 + tu2 * tu2);
			final double cy = s * cx + faz;
			final double y = Math.atan2(sy, cy);
			final double SA = s * sx / sy;
			final double c2a = 1 - SA * SA;
			double cz = faz + faz;
			if(c2a > 0) {
				cz = -cz / c2a + cy;
			}
			double e = cz * cz * 2 - 1;
			double c = ((-3 * c2a + 4) * F + 4) * c2a * F / 16;
			double d = x;
			x = ((e * cy * c + cz) * sy * c + y) * SA;
			x = (1 - c) * x * F + lon2 - lon1;

			if(Math.abs(d - x) <= EPS) {
				x = Math.sqrt((1 / (R * R) - 1) * c2a + 1) + 1;
				x = (x - 2) / x;
				c = 1 - x;
				c = (x * x / 4 + 1) / c;
				d = (0.375 * x * x - 1) * x;
				x = e * cy;
				s = 1 - 2 * e;
				result[DISTANCE] = (((((sy * sy * 4 - 3) * s * cz * d / 6 - x) * d / 4 + cz) * sy * d + y) * c * R * axis);
				// compute the average of the forward bearing and back bearing. This
				// should be very close to a rhumb line.

				result[BEARING] = ((Math.atan2(tu1, tu2) + Math.atan2(cu1 * sx, baz * cx - su1 * cu2)) / 2);
                if(result[BEARING] < 0)
					result[BEARING] += Math.PI * 2;
				return result;
			}
		}
		throw new ArithmeticException("Great Circle not computable");
	}
}
