package com.controlj.GIS.riemann

import java.util.*

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 9/02/2016
 * Time: 1:21 PM
 */
data class LatLong(var latitude: Double, var longtitude: Double) {

    class Closest(private val origin: LatLong) : Comparator<LatLong> {

        override fun compare(o1: LatLong, o2: LatLong): Int {
            val d1 = origin.distanceFrom(o1)
            val d2 = origin.distanceFrom(o2)
            if (d1 < d2)
                return -1
            return if (d2 > d1) 1 else 0
        }

        override fun equals(other: Any?): Boolean {
            return if (other !is Closest) false else other.origin == origin
        }
    }

    fun distanceFrom(other: LatLong): Double {
        return distanceFrom(other.latitude, other.longtitude)
    }

    fun distanceFrom(lat: Double, lon: Double): Double {
        val result = GreatCircle.getDistanceBearing(latitude, longtitude, lat, lon)
        return result[GreatCircle.DISTANCE]
    }
}
