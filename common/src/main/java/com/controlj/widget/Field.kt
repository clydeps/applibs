package com.controlj.widget

import com.controlj.graphics.*

/**
 * Created by clyde on 20/5/17.
 */

open class Field(var label: String, open var unitName: String) {
    var bounds: CRect = CRect()
    var valueBounds: CRect = CRect()
    var labelBounds: CRect = CRect()
    open val currentColor = CColor.WHITE

    open fun draw(gc: GraphicsContext, valueTextStyle: TextStyle) {}

    open fun drawBackground(gc: GraphicsContext, path: Path, labelTextStyle: TextStyle, labelColor: Int) {
        gc.drawText(label, labelBounds, labelTextStyle, labelColor)
    }

}
