package com.controlj.widget

import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.TextStyle

/**
 * Created by clyde on 17/5/17.
 */
open class TextBoxGroup(name: String, val textStyle: TextStyle, private val labelTextStyle: TextStyle, private val textWidth: Double, vararg fields: Field) : CView(name) {
    var borderWidth = BORDER_WIDTH
    var cornerRadius = CORNER_RADIUS
    var paddingLeft = PADDING
    var paddingRight = PADDING
    var paddingTop = PADDING
    var paddingBottom = PADDING
    var borderColor: Int = BORDER_COLOR
    var labelColor: Int = LABEL_COLOR
    val fields = ArrayList<Field>()
    private var labelWidth = 0.0
    private var height = 0.0

    override var preferredHeight: Double
        get() = fields.size * height
        @Suppress("UNUSED_PARAMETER")
        set(value) {
        }

    init {
        borderWidth = factory.pointsToPixels(BORDER_WIDTH).toDouble()
        cornerRadius = factory.pointsToPixels(CORNER_RADIUS).toDouble()
        if (fields.size != 0)
            addFields(*fields)
    }

    fun addFields(vararg fields: Field) {
        this.fields.addAll(fields)
        paddingTop = factory.pointsToPixels(PADDING).toDouble()
        paddingRight = paddingTop
        paddingLeft = paddingRight
        paddingBottom = paddingLeft
        var w = 0.0
        for (value in fields) {
            val b = factory.getTextBounds(value.label, labelTextStyle)
            if (b.width > w)
                w = b.width
        }
        labelWidth = w
        height = textStyle.size + paddingTop + paddingBottom + borderWidth * 2 + cornerRadius * 2
    }

    override val preferredWidth: Double
        get() = labelWidth + textWidth + paddingLeft + paddingRight + borderWidth * 4 + cornerRadius * 2

    override fun draw(gc: GraphicsContext, rect: CRect) {
        super.draw(gc, rect)
        gc.saveState()
        for (field in fields) {
            field.draw(gc, textStyle)
            gc.translateBy(0.0, height)
        }
        gc.restoreState()
    }

    override fun drawBackground(gc: GraphicsContext, rect: CRect) {
        if (bounds.empty)
            return
        gc.saveState()
        // this is the frame for the label.
        // frame for the topmost value
        val outer = CRect(bounds.left, bounds.top, bounds.left + preferredWidth, bounds.top + height)
        val labelRect = outer.inset(paddingLeft + borderWidth, paddingTop + borderWidth,
                preferredWidth - labelWidth - paddingLeft - borderWidth * 2, paddingBottom + borderWidth)

        /*
                xcenters[0] = xcenters[1] = labelRect.getRight() + paddingLeft + borderWidth + cornerRadius;
        xcenters[2] = xcenters[3] = width - paddingRight - cornerRadius - borderWidth;
        ycenters[2] = ycenters[1] = paddingTop + borderWidth + cornerRadius;
        ycenters[0] = ycenters[3] = height - paddingBottom - borderWidth - cornerRadius;
        xpoints[0] = xpoints[1] = xcenters[0] - cornerRadius;
        xpoints[2] = xpoints[7] = xcenters[0];
        xpoints[3] = xpoints[6] = xcenters[2];
        xpoints[4] = xpoints[5] = xcenters[2] + cornerRadius;
        ypoints[0] = ypoints[5] = ycenters[0];
        ypoints[1] = ypoints[4] = ycenters[1];
        ypoints[2] = ypoints[3] = ycenters[1] - cornerRadius;
        ypoints[6] = ypoints[7] = ycenters[0] + cornerRadius;
        CRect valueRect = factory.getRect(xcenters[0], ycenters[1], xcenters[2], ycenters[3]);

         */

        val valueRect = outer.inset(
                labelRect.width + paddingLeft + borderWidth + cornerRadius,
                paddingTop + borderWidth + cornerRadius,
                paddingRight + cornerRadius + borderWidth,
                paddingBottom + borderWidth + cornerRadius)
        val roundRect = factory.createPath()
        roundRect.addRoundedRect(valueRect, cornerRadius, cornerRadius)
        for (field in fields) {
            field.bounds = outer
            field.valueBounds = valueRect
            field.labelBounds = labelRect
            gc.strokeColor = borderColor
            gc.lineWidth = borderWidth
            field.drawBackground(gc, roundRect, labelTextStyle, labelColor)
            gc.translateBy(0.0, height)
        }
        gc.restoreState()
    }

    companion object {
        internal val BORDER_COLOR = 0xFFFFFFFF.toInt()
        internal val LABEL_COLOR = 0xFFFFFFFF.toInt()
        internal val BORDER_WIDTH = 1.0
        internal val CORNER_RADIUS = 4.0
        internal val PADDING = 4.0
    }
}
