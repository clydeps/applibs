package com.controlj.widget

import com.controlj.graphics.CImage
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.GraphicsFactory

/**
 * A  CView that just displays an image.
 */
class CImageView(var image: CImage, name: String = "") : CView(name) {

    constructor(imageName: String, name: String = "") : this(GraphicsFactory().loadImage(imageName), name)

    override fun drawBackground(gc: GraphicsContext, rect: CRect) {
        super.drawBackground(gc, rect)
        gc.drawImage(image, rect)
    }
}
