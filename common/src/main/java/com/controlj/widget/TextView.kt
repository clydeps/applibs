package com.controlj.widget

import com.controlj.framework.SystemColors
import com.controlj.graphics.CColor
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.GraphicsFactory
import com.controlj.graphics.TextStyle
import com.controlj.view.ForegroundServiceProvider

/**
 * Created by clydestubbs on 7/07/2017.
 */

open class TextView(open val textStyle: TextStyle, open var text: String, name: String = "") : CView(name) {
    var textColor = SystemColors().label
    var padding: Int = 0
    protected lateinit var textBounds: CRect

    override var bounds: CRect
        get() = super.bounds
        set(bounds) {
            super.bounds = bounds
            var hdiff = (bounds.width - textRectWidth) / 2
            if (hdiff < 0)
                hdiff = 0.0
            var vDiff = (bounds.height - textRectHeight) / 2
            if (vDiff < 0)
                vDiff = 0.0
            textBounds = bounds.inset(hdiff, vDiff, hdiff, vDiff)
        }

    protected val textRectHeight: Double
        get() = textStyle.size + padding * 2

    protected open val textRectWidth: Double
        get() {
            val rect = GraphicsFactory().getTextSize(text, textStyle)
            return Math.ceil(rect.width + padding * 2)
        }

    override val preferredWidth: Double
        get() = textRectWidth + padding * 2

    override val preferredHeight: Double
        get() = textRectHeight + padding * 2

    init {
        padding = factory.pointsToPixels(PADDING.toDouble())
    }

    override fun draw(gc: GraphicsContext, rect: CRect) {
        gc.drawText(text, textBounds, textStyle, textColor)
    }

    companion object {
        private const val PADDING = 4
    }
}
