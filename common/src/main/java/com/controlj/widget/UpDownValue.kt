package com.controlj.widget

import com.controlj.framework.SystemColors
import com.controlj.graphics.CColor
import com.controlj.graphics.CPoint
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.Path
import com.controlj.graphics.TextStyle
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import com.controlj.view.ForegroundServiceProvider
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.disposables.Disposable
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 11/7/17.
 */

class UpDownValue(textStyle: TextStyle, digits: Int, precision: Int, name: String = "") : ValueView(textStyle, digits, precision, name) {
    private lateinit var upArrow: Path
    private lateinit var downArrow: Path
    private var upColor = CColor.argb(0xA0, 0, 0x40, 0xFF)
    private var upPressedColor = CColor.argb(0xFF, 0, 0x40, 0xFF)
    private var downColor = CColor.argb(0xA0, 0xFF, 0x40, 0)
    private var downPressedColor = CColor.argb(0xFF, 0xFF, 0x40, 0)
    private var downPressed: Boolean = false
    private var upPressed: Boolean = false
    private var emitter: ObservableEmitter<Event> = DisposedEmitter()

    enum class Event {
        CHANGED,
        TAPPED
    }

    /**
     * Listen for taps on the numeric area and changes in value. The current [value] is supplied on each tap.
     */
    val observable: Observable<Event> = Observable.create { emitter = it }
    private var counterDisposable: Disposable = Disposable.disposed()
    var maxValue = 1e10
        set(maxValue) {
            field = maxValue
            if (value > maxValue)
                value = maxValue
        }
    var minValue = -1e10
        set(minValue) {
            field = minValue
            if (value < minValue)
                value = minValue
        }
    private var count: Int = 0

    init {
        textColor = SystemColors().label
    }

    override var text: String
        get() = super.text
        set(textVal) {
            value = (textVal.toDoubleOrNull() ?: 0.0).coerceIn(minValue, maxValue)
        }

    override var bounds: CRect
        get() = super.bounds
        set(value) {
            super.bounds = value
            //logMsg("setBounds height %f, preferredHeight %f", bounds.getHeight(), getPreferredHeight());
            val height = (value.height - padding * 2) / 3
            upArrow = factory.createPath()
            val center = (value.left + value.right) / 2
            upArrow.moveTo((center - height), (value.top + height))
            upArrow.lineTo((center + height), (value.top + height))
            upArrow.lineTo(center, (value.top + padding))
            upArrow.closePath()

            downArrow = factory.createPath()
            downArrow.moveTo((center - height), (value.bottom - height))
            downArrow.lineTo((center + height), (value.bottom - height))
            downArrow.lineTo(center, (value.bottom - padding))
            downArrow.closePath()
        }

    override fun onTouch(position: CPoint, down: Boolean) {
        if (!down) {
            counterDisposable.dispose()
            upPressed = false
            downPressed = upPressed
        } else if (position.y < textBounds.top) {
            upPressed = true
            downPressed = false
        } else if (position.y > textBounds.bottom) {
            downPressed = true
            upPressed = false
        } else {
            upPressed = false
            downPressed = upPressed
        }
        redrawForeground()
    }

    override var preferredHeight: Double
        get() = super.preferredHeight * 3
        @Suppress("UNUSED_PARAMETER")
        set(value) {
        }

    override fun draw(gc: GraphicsContext, rect: CRect) {
        super.draw(gc, rect)
        gc.fillColor = if (downPressed) downPressedColor else downColor
        gc.fillPath(downArrow)
        gc.fillColor = if (upPressed) upPressedColor else upColor
        gc.fillPath(upArrow)
    }

    override fun onClick(position: CPoint): Boolean {
        when {
            position.y < textBounds.top -> click(1)
            position.y > textBounds.bottom -> click(-1)
            else -> emitter.onNext(Event.TAPPED)
        }
        return true
    }

    private fun longPress(increment: Int) {
        count = 0
        counterDisposable.dispose()
        counterDisposable = Observable.interval(
                50,
                300,
                TimeUnit.MILLISECONDS,
                MainScheduler.instance
        ).subscribe(
                { click(if (++count >= 10) increment * 5 else increment) },
                {},
                { downPressed = false; upPressed = false })
    }

    override fun onLongPress(position: CPoint, ended: Boolean) {
        when {
            ended -> counterDisposable.dispose()
            position.y < textBounds.top -> longPress(10)
            position.y > textBounds.bottom -> longPress(-10)
        }
    }

    private fun click(increment: Int) {
        value += (increment * Math.pow(10.0, (-precision).toDouble()))
        if (value > this.maxValue)
            value = this.maxValue
        else if (value < this.minValue)
            value = this.minValue
        emitter.onNext(Event.CHANGED)
        redrawForeground()
    }

    override fun drawBackground(gc: GraphicsContext, rect: CRect) {
        gc.strokeColor = textColor
        gc.strokePath(roundedRect)
    }
}
