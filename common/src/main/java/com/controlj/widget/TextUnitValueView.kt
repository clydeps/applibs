package com.controlj.widget

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.graphics.CColor
import com.controlj.utility.Units

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 7/5/18
 * Time: 20:32
 *
 * Display a value with a unit
 */
open class TextUnitValueView(
        label: String,
        open val unit: Units.Unit = Units.Unit.IDENTITY,
        override var precision: Int = 0,
        backgroundColor: Int = CColor.BLACK,
        hasUnit: Boolean = true
) : TextValueCview(
        label,
        backgroundColor = backgroundColor,
        hasUnit = hasUnit
), SingleValued<Double> {

    override val unitText: String
        get() = unit.label

    override var targetValue: Double = 0.0                // value animating to
        set(value) {
            field = value
            if (value == INVALID_DATA) {
                valueText = " ---"
            } else {
                valueText = unit.toString(value, precision)
            }
        }
}
