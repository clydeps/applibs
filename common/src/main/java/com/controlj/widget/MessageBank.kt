package com.controlj.widget

import com.controlj.data.Message
import com.controlj.data.MessageRecord
import com.controlj.data.Priority
import com.controlj.framework.SoundPlayer
import com.controlj.logging.CJLog.logMsg
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-04-10
 * Time: 15:40
 */
class MessageBank {

    internal val displayQueue = mutableSetOf<Message>()
    private var emitter: ObservableEmitter<MessageBank>? = null
    private var activeMessages = HashMap<Message, MessageRecord>()

    val observable: Observable<MessageBank> = Observable.create { e: ObservableEmitter<MessageBank> ->
        emitter = e
    }.share()

    // get a list of active messages, highest priority first
    val messageList: List<Message>
        get() = displayQueue.toList().sortedDescending()

    val highestPriority: Priority
        get() = activeMessages.keys.map {it.priority}.sorted().lastOrNull() ?: Priority.NONE
    val hasUnAcked: Boolean
        get() = displayQueue.isEmpty()

    /**
     * Post a message. Returns true if the message was not already active.
     * @param message The message
     * @param playSound If a sound should be played when the message is first posted
     * @return true if the message was not already active.
     */
    fun postMessage(message: Message, playSound: Boolean): Boolean {
        var existing = false
        val record = activeMessages.get(message)?.let {
            it.subtext = message.subText
            existing = true
            it
        } ?: MessageRecord.of(message.text, message.priority, message.subText).also {
            logMsg("Posting message: $message")
            activeMessages.put(message, it)
            if (playSound && it.priority.sound.isNotBlank())
                SoundPlayer.playSound(it.priority.sound)
        }
        record.update().subscribe()
        displayQueue.add(message)
        emitter?.onNext(this)
        return !existing
    }

    fun cancelMessage(message: Message): Boolean {
        activeMessages.remove(message)?.let {
            it.update(false).subscribe()
            displayQueue.remove(message)
            logMsg("cancelled message $message")
            emitter?.onNext(this)
            //debug(MessageRecord.getEntries().joinToString(", "))
            return true
        }
        return false
    }

    /**
     * Is the given message currentl in the active set?
     */
    fun isActive(message: Message): Boolean {
        return activeMessages.contains(message)
    }

    /**
     * Is the given message currently active and unacked?
     */
    fun isShowing(message: Message): Boolean {
        return displayQueue.contains(message)
    }

    fun ackMessage() {
        if (displayQueue.isNotEmpty()) {
            val msg = messageList.first()
            logMsg("Acknowledged message ${msg}")
            displayQueue.remove(msg)
            if (msg.autoCancel)
                cancelMessage(msg)
        }
    }

    fun clearMessages() {
        displayQueue.clear()
        activeMessages.keys.toList().forEach { cancelMessage(it) }
        emitter?.onNext(this)
    }
}
