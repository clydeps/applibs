package com.controlj.widget

import com.controlj.animation.Animation
import com.controlj.graphics.CColor
import com.controlj.graphics.CPoint
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.ui.UiAction
import kotlin.math.min

/**
 *  A hamburger icon for opening a menu
 *  @param name A name for the widget - chiefly for debugging purposes
 *  @property isOpen True when the widget is in the "opened" state
 *  @property margin The margin in pixels to use
 *  @property foregroundColor The color in which to draw the bars
 *  @property horizontalAlignment Where to align the widget contents, horizontally
 *  @property verticalAlignment Where to align the widget content, vertically
 */
class Hamburger(name: String = "") : CView(name) {

    private val animation = Animation(0.0, 0.4, ::updateState)
    private var percentage: Double = 0.0

    var margin: Double = 3.0        // margin in points
    var foregroundColor = CColor.BLACK
    var horizontalAlignment = CViewContainer.Alignment.LEFT
    var verticalAlignment = CViewContainer.VAlignment.MIDDLE

    var isOpen = false
        set(value) {
            if (field != value) {
                field = value
                animation.restart()
            }
        }


    override fun onClick(position: CPoint): Boolean {
        UiAction.sendAction(ToggleMenu)
        return true
    }

    private fun updateState(percentage: Double) {
        if (isOpen)
            this.percentage = percentage
        else
            this.percentage = 1.0 - percentage
        requestRedraw()
    }


    override fun draw(gc: GraphicsContext, rect: CRect) {
        gc.saveState()
        val dimen = min(rect.width, rect.height) - margin * 2    // size of image
        val lineHeight = dimen / LINES
        val left = rect.left +
            when (horizontalAlignment) {
                CViewContainer.Alignment.LEFT -> margin
                CViewContainer.Alignment.RIGHT -> rect.width - margin - dimen
                else -> (rect.width - dimen) / 2
            }
        val top = rect.top + lineHeight / 2 +
            when (verticalAlignment) {
                CViewContainer.VAlignment.TOP -> margin
                CViewContainer.VAlignment.BOTTOM -> rect.height - margin - dimen
                else -> (rect.height - dimen) / 2
            }
        gc.strokeColor = foregroundColor
        gc.lineWidth = lineHeight / 2
        gc.drawLine(left, top, left + dimen / 2, top + percentage * lineHeight)
        gc.drawLine(left + dimen, top, left + dimen / 2, top + percentage * lineHeight)
        gc.drawLine(left, top + lineHeight * 2, left + dimen / 2, top + lineHeight * 2 - percentage * lineHeight)
        gc.drawLine(
            left + dimen,
            top + lineHeight * 2,
            left + dimen / 2,
            top + lineHeight * 2 - percentage * lineHeight
        )
        gc.drawLine(
            left + percentage * dimen / 2,
            top + lineHeight,
            left + dimen - percentage * dimen / 2,
            top + lineHeight
        )
        gc.restoreState()
    }

    companion object {
        val CloseMenu = UiAction.MenuAction("Hamburger")
        val OpenMenu = UiAction.MenuAction("Hamburger")
        val ToggleMenu = UiAction.MenuAction("Hamburger")

        const val LINES = 3
    }
}
