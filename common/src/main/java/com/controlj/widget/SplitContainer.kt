package com.controlj.widget

import com.controlj.graphics.CColor
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext

/**
 * Created by clyde on 17/5/17.
 */

open class SplitContainer(
        var ratio: Double = 0.5,
        var direction: Direction = Direction.AUTOMATIC,
        var showDividers: Boolean = false
) : CViewContainer() {
    private var currentDir: Direction = direction
    private var dividerWidth = 1.0
    private var boundary: Double = 0.toDouble()
    private val dividerColor: Int = CColor.WHITE

    override fun doLayout() {
        currentDir = direction
        if (direction === Direction.AUTOMATIC)
            currentDir = if (bounds.width > bounds.height)
                Direction.HORIZONTAL
            else
                Direction.VERTICAL
        when (subviews.size) {
            0 -> {
            }

            1 -> {
                subviews[0].bounds = bounds
                boundary = 0.0
            }

            2 -> if (currentDir === Direction.VERTICAL) {
                boundary = bounds.height * ratio
                subviews[0].bounds = bounds.inset(0.0, 0.0, 0.0, bounds.height - boundary)
                subviews[1].bounds = bounds.inset(0.0, boundary, 0.0, 0.0)
            } else {
                boundary = bounds.width * ratio
                subviews[0].bounds = bounds.inset(0.0, 0.0, bounds.width - boundary, 0.0)
                subviews[1].bounds = bounds.inset(boundary, 0.0, 0.0, 0.0)
            }

            else -> throw IllegalArgumentException("Too many children of " + this.javaClass.simpleName)
        }
    }

    override fun drawBackground(gc: GraphicsContext, rect: CRect) {
        super.drawBackground(gc, rect)
        if (showDividers && boundary != 0.0) {
            gc.lineWidth = dividerWidth.toFloat().toDouble()
            gc.strokeColor = dividerColor
            if (currentDir === Direction.VERTICAL)
                gc.drawLine(bounds.left.toFloat().toDouble(), (bounds.top.toFloat() + boundary.toFloat()).toDouble(), bounds.right.toFloat().toDouble(), (bounds.top.toFloat() + boundary.toFloat()).toDouble())
            else
                gc.drawLine((bounds.left + boundary).toFloat().toDouble(), bounds.top.toFloat().toDouble(), (bounds.left + boundary).toFloat().toDouble(), bounds.bottom.toFloat().toDouble())
        }
    }
}
