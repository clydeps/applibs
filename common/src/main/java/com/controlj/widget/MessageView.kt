package com.controlj.widget

import com.controlj.graphics.CColor
import com.controlj.graphics.CPoint
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.GraphicsFactory
import com.controlj.graphics.Path
import com.controlj.graphics.TextStyle

/**
 * Created by clyde on 2/7/17.
 */

open class MessageView(val messageBank: MessageBank) : CView() {
    private var textSize = 16
    private var textStyle: TextStyle = TextStyle(TextStyle.FontFamily.SansSerif, TextStyle.FontStyle.Normal, TextStyle.Alignment.Center, textSize.toDouble())
    private var inner: CRect = CRect()
    private var roundRect: Path = factory.createPath()
    private var lineWidth = 0.0
    private var padding = 0.0

    init {
        val factory = GraphicsFactory()
        setTextSize(TEXTSIZE)
        lineWidth = factory.pointsToPixels(2.0).toDouble()
        padding = factory.pointsToPixels(PADDING).toDouble()
    }

    override var isVisible: Boolean
        get() = !messageBank.hasUnAcked
        @Suppress("UNUSED_PARAMETER")
        set(value) {
        }

    override var preferredWidth: Double
        get() {
            return (textSize * 12 + padding * (2 + OFFSET))
        }
        @Suppress("UNUSED_PARAMETER")
        set(value) {
        }

    override var preferredHeight: Double
        get() {
            return (textSize * 2 + padding * (2 + OFFSET))
        }
        @Suppress("UNUSED_PARAMETER")
        set(value) {
        }

    override var bounds: CRect
        get() = super.bounds
        set(value) {
            super.bounds = value
            inner = value.inset(padding, padding, (padding * (OFFSET + 1)), (padding * (OFFSET + 1)))
            roundRect.clear()
            roundRect.addRoundedRect(inner, padding, padding)
        }

    fun setTextSize(pointSize: Double) {
        textSize = factory.pointsToPixels(pointSize)
        textStyle = TextStyle(TextStyle.FontFamily.SansSerif, TextStyle.FontStyle.Normal, TextStyle.Alignment.Center, textSize.toDouble())
    }

    override fun draw(gc: GraphicsContext, rect: CRect) {
        if (!isVisible)
            return
        gc.saveState()
        val queue = messageBank.messageList
        var i = queue.count()
        if (i > OFFSET)
            i = OFFSET
        gc.strokeColor = OUTLINE_COLOR
        gc.translateBy(padding * i, padding * i)
        queue.take(i).reversed().forEach { message ->
            gc.fillColor = message.backgroundColor
            gc.fillPath(roundRect)
            gc.lineWidth = lineWidth
            gc.strokePath(roundRect)
            gc.translateBy(-padding, -padding)
        }
        val message = queue.first()
        gc.drawText(message.text, inner, textStyle, message.color)
        gc.restoreState()
    }

    // clicking on the message will remove the currently showing message. It remains in the active
    // messages set so that re-showing the message will not cause it to reappear unless it has since
    // been cancelled.
    override fun onClick(position: CPoint): Boolean {
        messageBank.ackMessage()
        requestRedraw()
        return true
    }

    companion object {
        internal const val TEXTSIZE = 30.0
        internal const val OFFSET = 4        // offset this many paddings to show stack
        const val MARGIN = 12.0                // bottom and right margin to use
        private val OUTLINE_COLOR = CColor.BLACK
    }
}
