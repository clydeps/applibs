package com.controlj.widget

import com.controlj.graphics.CColor
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import org.threeten.bp.LocalTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.TemporalAccessor

/**
 * Created by clyde on 17/5/17.
 */

class TimeValue(backgroundColor: Int = CColor.WHITE) : TextValueCview(backgroundColor = backgroundColor) {

    override fun draw(gc: GraphicsContext, rect: CRect) {
        valueText = formatZulu(LocalTime.now(zone))
        super.draw(gc, rect)
    }

    companion object {
        val zone = ZoneId.of("UTC")
        val timeFormat = DateTimeFormatter.ofPattern("HH:mm:ss").withZone(zone)

        fun formatZulu(time: TemporalAccessor): String {
            return timeFormat.format(time) + " Z"
        }
    }
}
