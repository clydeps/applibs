package com.controlj.widget

import com.controlj.data.Constants.INVALID_DATA

interface Updater<T> {
    fun update(data: T)
    fun invalidate()
}
class SingleUpdater<T>(private val b: SingleValued<Double>, private val getter: (data: T) -> Double) : Updater<T> {
    override fun invalidate() {
        b.targetValue = INVALID_DATA
    }

    override fun update(data: T) {
        b.targetValue = getter(data)
    }

}
