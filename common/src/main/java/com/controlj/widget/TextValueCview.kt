package com.controlj.widget

import com.controlj.graphics.CColor
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.TextStyle
import com.controlj.graphics.contrastColor
import com.controlj.graphics.rgbString

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-20
 * Time: 17:15
 *
 * A basic class to display up to three text values in a line. The label and unit fields may be suppressed.
 */
open class TextValueCview(
        var label: String = "",
        final override var backgroundColor: Int,
        val hasUnit: Boolean = false
) : CView(label) {
    companion object {

        const val CANVAS_WIDTH = 100.0
        const val PADDING = CANVAS_WIDTH * .03
        const val ASPECT_RATIO = 0.2       // ratio of height to width
        const val CANVAS_HEIGHT = CANVAS_WIDTH * ASPECT_RATIO
        const val LABEL_WIDTH = 35.0         // weights for width
        const val UNIT_WIDTH = 20.0
        const val TEXT_SIZE = CANVAS_WIDTH * 0.12
    }

    open protected val labelRect by lazy {
        if (label.isNotBlank())
            CRect(PADDING, PADDING, LABEL_WIDTH - PADDING * 2, CANVAS_HEIGHT - PADDING)
        else
            CRect(0.0, PADDING, 0.0, CANVAS_HEIGHT - PADDING)
    }
    open protected val unitRect by lazy {
        if (hasUnit)
            CRect(CANVAS_WIDTH - UNIT_WIDTH - PADDING * 2, PADDING, CANVAS_WIDTH - PADDING, CANVAS_HEIGHT - PADDING)
        else
            CRect(CANVAS_WIDTH, PADDING, CANVAS_WIDTH, CANVAS_HEIGHT - PADDING)
    }
    open protected val valueRect by lazy {
        CRect(
                labelRect.right + PADDING,
                PADDING,
                unitRect.left - PADDING,
                CANVAS_HEIGHT - PADDING
        )
    }
    protected val unitTextStyle by lazy {
        TextStyle(
                TextStyle.FontFamily.SystemDefault,
                TextStyle.FontStyle.Normal,
                TextStyle.Alignment.Left,
                TEXT_SIZE * 0.5
        )
    }
    protected val labelTextStyle by lazy {
        TextStyle(
                TextStyle.FontFamily.SystemDefault,
                TextStyle.FontStyle.Normal,
                TextStyle.Alignment.Right,
                TEXT_SIZE * 0.6
        )
    }
    protected val valueTextStyle by lazy {
        TextStyle(
                TextStyle.FontFamily.SystemDefault,
                TextStyle.FontStyle.Bold,
                if(hasUnit) TextStyle.Alignment.Right else TextStyle.Alignment.Center,
                TEXT_SIZE
        )
    }

    protected var topOffset: Double = 0.0
    protected var leftOffset: Double = 0.0
    protected var scale: Double = 1.0
    open var valueText: String = ""
    open val unitText = ""

    open val currentColor: Int = backgroundColor.contrastColor
    open val labelTextColor: Int = CColor.invert(backgroundColor)

    override var bounds: CRect
        get() = super.bounds
        set(b) {
            super.bounds = b
            if (!b.empty) {
                when {
                    // wider than high
                    b.width * ASPECT_RATIO > b.height -> {
                        scale = b.height / (CANVAS_HEIGHT)
                        topOffset = 0.0
                        leftOffset = (b.width - CANVAS_WIDTH * scale) / 2.0
                    }
                    else -> {
                        b.width / CANVAS_WIDTH
                        scale = b.width / (CANVAS_WIDTH)
                        topOffset = (b.height - CANVAS_HEIGHT * scale) / 2.0
                        leftOffset = 0.0
                    }
                }
            }
        }

    override fun drawBackground(gc: GraphicsContext, rect: CRect) {
        val b = bounds
        if (b.empty || rect.empty)
            return
        super.drawBackground(gc, rect)
        gc.saveState()
        gc.translateBy(b.left + leftOffset, b.top + topOffset)
        gc.scaleBy(scale, scale)
        if(label.isNotBlank())
            gc.drawText(label, labelRect, labelTextStyle, labelTextColor)
        if (hasUnit)
            gc.drawText(unitText, unitRect, unitTextStyle, labelTextColor)
        gc.restoreState()
    }

    override fun draw(gc: GraphicsContext, rect: CRect) {
        val b = bounds
        if (b.empty)
            return
        super.draw(gc, rect)
        gc.saveState()
        gc.translateBy(b.left + leftOffset, b.top + topOffset)
        gc.scaleBy(scale, scale)
        gc.drawText(valueText, valueRect, valueTextStyle, currentColor)
        gc.restoreState()
    }

    override fun toString(): String {
        return "${this::class.simpleName}[$label \"$valueText\" ${unitText}-${currentColor.rgbString}]"
    }
}
