package com.controlj.widget

import com.controlj.graphics.CColor
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext

/**
 * Created by clyde on 17/5/17.
 */

open class LinearContainer @JvmOverloads constructor(direction: Direction, var showDividers: Boolean = false) : CViewContainer() {
    var alignment: Alignment = Alignment.CENTER
    var vertAlignment: VAlignment = VAlignment.MIDDLE
    var direction: Direction = Direction.VERTICAL
        set(value) {
            field = value
            computedDirection = value
        }
    var computedDirection: Direction = Direction.VERTICAL
        protected set
    var dividerWidth = 1.0

    var dividerColor: Int = 0
    private var divPos = ArrayList<Double>()

    override var preferredWidth: Double
        get() {
            var width = 0.0
            when (direction) {
                Direction.VERTICAL -> {
                    for (view in subviews)
                        if (view.preferredWidth > width)
                            width = view.preferredWidth
                    return width
                }

                Direction.HORIZONTAL -> {
                    for (view in subviews)
                        if (view.preferredWidth == 0.0)
                            return 0.0
                        else
                            width += view.preferredWidth
                    return width
                }

                else -> return 0.0
            }
        }
        @Suppress("UNUSED_PARAMETER")
        set(value) {
        }

    override var preferredHeight: Double
        get() {
            var height = 0.0
            when (direction) {
                Direction.HORIZONTAL -> {
                    for (view in subviews)
                        if (view.preferredHeight > height)
                            height = view.preferredHeight
                    return height
                }

                Direction.VERTICAL -> {
                    for (view in subviews)
                        if (view.preferredHeight == 0.0)
                            return 0.0
                        else
                            height += view.preferredHeight
                    return height
                }

                else -> return 0.0
            }
        }
        @Suppress("UNUSED_PARAMETER")
        set(value) {
        }

    init {
        this.direction = direction
        computedDirection = this.direction
    }

    override fun doLayout() {
        if (subviews.size == 0)
            return
        divPos.clear()
        val width = bounds.width
        val height = bounds.height
        if (direction == Direction.AUTOMATIC)
            computedDirection = if (width > height) Direction.HORIZONTAL else Direction.VERTICAL

        var used = 0.0
        var cnt = 0
        if (computedDirection == Direction.VERTICAL) {
            for (view in subviews) {
                val h = view.preferredHeight
                if (h == 0.0)
                    cnt++
                else
                    used += h
            }
            var gap = 0.0
            var delta = height - used
            var y = 0.0
            if (cnt == 0) {
                when (vertAlignment) {
                    VAlignment.TOP -> {
                    }

                    VAlignment.BOTTOM -> y += delta

                    VAlignment.MIDDLE -> y += delta / 2

                    VAlignment.FILL -> gap = delta / (subviews.size + 1)
                }
                delta = 0.0
            } else
                delta /= cnt.toDouble()
            for (view in subviews) {
                var childHeight = view.preferredHeight
                if (childHeight == 0.0)
                    childHeight = delta
                var childWidth = view.preferredWidth
                if (childWidth == 0.0 || childWidth > width)
                    childWidth = width
                var linset = width - childWidth
                if (linset < 0)
                    linset = 0.0
                var rinset = 0.0
                when (alignment) {
                    Alignment.RIGHT -> {
                    }
                    Alignment.LEFT -> {
                        rinset = linset
                        linset = 0.0
                    }
                    Alignment.CENTER -> {
                        rinset = linset / 2
                        linset = rinset
                    }
                    Alignment.FILL -> {
                        rinset = 0.0
                        linset = rinset
                    }
                }
                view.bounds = bounds.inset(linset, y + gap / 2, rinset, height - y - childHeight - gap / 2)
                divPos.add(y + bounds.top)
                y += childHeight + gap
            }
        } else {
            var x = 0.0
            for (view in subviews) {
                val h = view.preferredWidth
                if (h == 0.0)
                    cnt++
                else
                    used += h
            }
            if (cnt == 0)
                cnt = subviews.size
            val delta = (width - used) / cnt
            for (view in subviews) {
                var childHeight = view.preferredHeight
                if (childHeight == 0.0 || childHeight > height)
                    childHeight = height
                var childWidth = view.preferredWidth
                if (childWidth == 0.0)
                    childWidth = delta
                if (childWidth < 0)
                    childWidth = 0.0
                var tinset = height - childHeight
                if (tinset < 0)
                    tinset = 0.0
                var binset = 0.0
                when (vertAlignment) {
                    VAlignment.BOTTOM -> {
                    }
                    VAlignment.TOP -> {
                        binset = tinset
                        tinset = 0.0
                    }
                    VAlignment.MIDDLE -> {
                        binset = tinset / 2
                        tinset = binset
                    }
                    VAlignment.FILL -> {
                        binset = 0.0
                        tinset = binset
                    }
                }
                view.bounds = bounds.inset(x, tinset, width - x - childWidth, binset)
                divPos.add(x + bounds.left)
                x += childWidth
            }
        }
    }

    override fun drawBackground(gc: GraphicsContext, rect: CRect) {
        super.drawBackground(gc, rect)
        gc.lineWidth = dividerWidth
        if (showDividers) {
            if (dividerColor == 0)
                gc.strokeColor = CColor.invert(backgroundColor)
            else
                gc.strokeColor = dividerColor
            divPos.forEach {
                if (computedDirection == Direction.VERTICAL)
                    gc.drawLine(bounds.left, it, bounds.right, it)
                else
                    gc.drawLine(it, bounds.top, it, bounds.bottom)
            }
        }
    }
}
