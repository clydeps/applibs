package com.controlj.widget

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.Path
import com.controlj.graphics.TextStyle
import com.controlj.utility.Units
import com.controlj.utility.formatted

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-11
 * Time: 20:21
 */
open class UnitValue(
        label: String,
        open val unit: Units.Unit,
        override var precision: Int = 0,
        var showUnit: Boolean = false
) : Field(label, unit.unitName), SingleValued<Double> {
    var currentValue: Double = 0.0

    @Suppress("UNUSED_PARAMETER")
    override var unitName: String
        get() = unit.label
        set(value) = Unit

    override var targetValue: Double
        get() = currentValue
        set(value) {
            currentValue = when {
                value != INVALID_DATA -> unit.convertTo(value)
                else -> INVALID_DATA
            }
        }
    open val valueString: String
        get() = currentValue.formatted(precision.coerceAtLeast(unit.precision), valid, if (showUnit) unitName else "")

    override fun draw(gc: GraphicsContext, valueTextStyle: TextStyle) {
        super.draw(gc, valueTextStyle)
        gc.drawText(valueString, valueBounds, valueTextStyle, currentColor)
    }

    override fun drawBackground(gc: GraphicsContext, path: Path, labelTextStyle: TextStyle, labelColor: Int) {
        super.drawBackground(gc, path, labelTextStyle, labelColor)
        gc.strokePath(path)
    }
}
