package com.controlj.widget

import com.controlj.settings.UnitSetting
import com.controlj.utility.Units

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-11
 * Time: 20:09
 */
open class SettingValue(label: String, val setting: UnitSetting, precision: Int = 0, showUnit: Boolean = false) : UnitValue(label, setting.value, precision, showUnit) {
    override val unit: Units.Unit
        get() = setting.value

}