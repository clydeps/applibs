package com.controlj.widget

import com.controlj.graphics.CPoint
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import java.util.ArrayList

/**
 * Created by clyde on 11/5/17.
 */

abstract class CViewContainer : CView() {

    var subviews = ArrayList<CView>()
    protected var padding: Int = PADDING
        set(value) {
            field = factory.pointsToPixels(value.toDouble())

        }
    private var touchView: CView? = null

    // if we want a line drawn on an edge
    var edges = mutableSetOf<Edge>()

    override var bounds: CRect
        set(value) {
            super.bounds = value
            touchView = null
            doLayout()
        }
        get() = super.bounds

    override fun drawBackground(gc: GraphicsContext, rect: CRect) {
        super.drawBackground(gc, rect)
        if(rect.empty)
            return
        gc.saveState()
        gc.clip(rect)
        subviews.filter { it.isVisible && it.bounds.intersects(rect) }.forEach {
            gc.saveState()
            gc.clip(it.bounds)
            it.drawBackground(gc)
            gc.restoreState()
        }
        edges.forEach {
            when (it) {
                Edge.LEFT -> gc.drawLine(bounds.left, bounds.top, bounds.left, bounds.bottom)
                Edge.TOP -> gc.drawLine(bounds.left, bounds.top, bounds.right, bounds.top)
                Edge.BOTTOM -> gc.drawLine(bounds.left, bounds.bottom, bounds.right, bounds.bottom)
                Edge.RIGHT -> gc.drawLine(bounds.right, bounds.top, bounds.right, bounds.bottom)
            }
        }
        gc.restoreState()
    }

    override fun draw(gc: GraphicsContext, rect: CRect) {
        super.draw(gc, rect)
        gc.saveState()
        gc.clip(rect)
        subviews.filter { it.isVisible && it.bounds.intersects(rect) }.forEach {
            gc.saveState()
            gc.clip(it.bounds)
            it.draw(gc)
            gc.restoreState()
        }
        gc.restoreState()
    }

    fun addViews(vararg views: CView) {
        for (view in views)
            addView(view)
    }

    fun getViewContaining(position: CPoint): CView? {
        var view: CView
        var i = subviews.size
        while (i-- != 0) {
            view = subviews[i]
            if (view.isVisible && view.bounds.contains(position))
                return view
        }
        return null
    }

    override fun onClick(position: CPoint): Boolean {
        val view = getViewContaining(position)
        return view != null && view.onClick(position)
    }

    override fun onTouch(position: CPoint, down: Boolean) {
        val view = getViewContaining(position)
        if (touchView !== view)
            touchView?.onTouch(position, false)
        touchView = if (down) view else null
        if (view !== this)
            view?.onTouch(position, down)
    }

    override fun onLongPress(position: CPoint, ended: Boolean) {
        getViewContaining(position)?.onLongPress(position, ended)
    }

    fun addView(subView: CView) {
        subView.parent = this
        subviews.add(subView)
    }

    fun removeView(subView: CView) {
        subView.parent = null
        subviews.remove(subView)
    }

    fun removeAllViews() {
        for (view in subviews)
            view.parent = null
        subviews.clear()
    }

    abstract fun doLayout()

    enum class Direction {
        HORIZONTAL,
        VERTICAL,
        AUTOMATIC
    }

    enum class Alignment {
        LEFT,
        CENTER,
        RIGHT,
        FILL
    }

    enum class VAlignment {
        TOP,
        MIDDLE,
        BOTTOM,
        FILL
    }

    enum class Edge {
        LEFT,
        TOP,
        BOTTOM,
        RIGHT
    }

    companion object {
        private val PADDING = 6
    }
}
