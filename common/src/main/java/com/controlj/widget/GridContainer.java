package com.controlj.widget;

import com.controlj.graphics.CRect;

import java.util.ArrayList;

/**
 * Created by clydestubbs on 7/07/2017.
 */

public class GridContainer extends CViewContainer {
    final private int columns;
    private ArrayList<Double> rowHeights;

    public GridContainer(int columns) {
        super();
        this.columns = columns;
    }

    private CView getViewAt(int col, int row) {
        col += row * columns;
        if(col < getSubviews().size())
            return getSubviews().get(col);
        return null;
    }

    @Override
    public double getPreferredWidth() {
        rowHeights = new ArrayList<>();
        int count = getSubviews().size();
        int rows = (count + columns - 1) / columns;
        double width = 0;
        for(int i = 0; i != rows; i++) {
            double rowHeight = 0;
            double rowWidth = 0;
            for(int j = 0; j != columns; j++) {
                CView v = getViewAt(j, i);
                if(v != null) {
                    rowWidth += v.getPreferredWidth();
                    if(v.getPreferredHeight() > rowHeight)
                        rowHeight = v.getPreferredHeight();
                }
            }
            rowHeights.add(rowHeight);
            if(rowWidth > width)
                width = rowWidth;
        }
        return width + getPadding() * (columns + 1);
    }

    @Override
    public double getPreferredHeight() {
        int count = getSubviews().size();
        int rows = (count + columns - 1) / columns;
        float height = 0;
        for(int i = 0; i != columns; i++) {
            float colHeight = 0;
            for(int j = 0; j != rows; j++) {
                CView v = getViewAt(i, j);
                if(v != null)
                    colHeight += v.getPreferredHeight();
            }
            if(colHeight > height)
                height = colHeight;
        }
        return height + getPadding() * (rows + 1);
    }

    @Override
    public double getAspectRatio() {
        double f = getPreferredHeight();
        if(f == 0)
            return 0;
        return getPreferredWidth() / f;
    }

    public int getColumns() {
        return columns;
    }

    @Override
    public void doLayout() {
        int count = getSubviews().size();
        int rows = (count + columns - 1) / columns;
        double totalHeight = getBounds().getHeight() - getPadding();
        double colWidth = (float)Math.floor((getBounds().getWidth() - getPadding()) / columns);
        float rowHeight = (float)Math.floor(totalHeight / rows);
        if(rowHeights == null)
            getPreferredWidth();
        float y = getPadding();
        for(int row = 0; row != (count+columns-1)/columns; row++) {
            double height = rowHeights.get(row);
            if(height == 0)
                height = rowHeight - getPadding() * 2;
            for(int col = 0; col != columns; col++) {
                CView view = getSubviews().get(row * columns + col);
                CRect rect = getBounds().inset(getPadding() + col * colWidth, y,
                    (columns - 1 - col) * colWidth + getPadding(), totalHeight - y - height);
                view.setBounds(rect);
            }
            y += height + getPadding();
        }
    }
}
