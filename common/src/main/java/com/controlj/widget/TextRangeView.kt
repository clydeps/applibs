package com.controlj.widget

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.graphics.CColor
import com.controlj.settings.RangeSet
import com.controlj.utility.Units

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 7/5/18
 * Time: 20:32
 *
 * A display of a value with unit, coloured according to a supplied range
 */
open class TextRangeView(
        label: String = "",
        private var rangeSet: () -> RangeSet = { RangeSet.empty},
        precision: Int = 0,
        backgroundColor: Int
) : SettingValueView(
        label = label,
        setting = rangeSet().setting,
        precision = precision,
        backgroundColor = backgroundColor
) {
    override val currentColor: Int
        get() = rangeSet().colorForValue(targetValue, valid, backgroundColor == CColor.WHITE)
}

/**
 * A view of a time duration, coloured according to a range
 */
open class TimeRangeView(
        label: String,
        val rangeSet: () -> RangeSet,
        backgroundColor: Int
) : TextValueCview(label, backgroundColor = backgroundColor), SingleValued<Double> {

    override val currentColor: Int
        get() = rangeSet().colorForValue(targetValue, valid, backgroundColor == CColor.WHITE)

    override var targetValue: Double = INVALID_DATA
        set(value) {
            field = value
            valueText = Units.Unit.HMS.toString(value)
        }

}
