package com.controlj.widget

import com.controlj.settings.RangeSet

/**
 * Created by clyde on 19/5/17.
 */

open class RangedValue(
        label: String,
        var rangeSet: () -> RangeSet,
        precision: Int = 0,
        showUnit: Boolean = false
) : SettingValue(label, rangeSet().setting, precision, showUnit) {
    override var targetValue: Double
        get() = super.targetValue
        set(value) {
            super.targetValue = value
            currentColor = rangeSet().colorForValue(value, valid)
        }
    override var currentColor: Int = super.currentColor
}
