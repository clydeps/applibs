package com.controlj.widget

import com.controlj.data.Constants.INVALID_DATA

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 10/5/18
 * Time: 21:39
 */
interface SingleValued<T> {
    /**
     * Set the target value - in basic units
     */
    var targetValue: T
    val valid: Boolean
        get() = targetValue != INVALID_DATA
    var precision: Int      // number of decimal places to print
        get() = 0
        set(_) {}
}

