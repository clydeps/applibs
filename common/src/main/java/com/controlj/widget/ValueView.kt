package com.controlj.widget

import com.controlj.graphics.CColor
import com.controlj.graphics.TextStyle
import java.util.*

/**
 * Created by clydestubbs on 7/07/2017.
 */

open class ValueView(
        textStyle: TextStyle,
        digits: Int,
        precision: Int,
        name: String = ""
) : RoundedTextView(textStyle, "", name) {
    var precision = 1
    var digits = 5
    var value: Double = 0.0

    override val textRectWidth: Double
        get() = Math.ceil(digits * textStyle.size / 2) + padding * 2

    override val preferredWidth: Double
        get() = Math.ceil(digits * textStyle.size / 2 + padding * 4)

    init {
        this.precision = precision
        this.digits = digits
        backgroundColor = CColor.argb(0xff, 0xE0, 0xE0, 0xE0)
        textColor = CColor.argb(0xFF, 0x10, 0x10, 0x10)
    }

    override var text: String = ""
        get() {
            return if (precision <= 0)
                String.format(Locale.US, "%d", Math.round(value * Math.pow(10.0, precision.toDouble())))
            else
                String.format(Locale.US, "%." + precision + "f", value)
        }
}
