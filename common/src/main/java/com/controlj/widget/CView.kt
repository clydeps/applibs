package com.controlj.widget

import com.controlj.graphics.CColor
import com.controlj.graphics.CPoint
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.GraphicsFactory
import com.controlj.view.ViewController

/**
 * Created by clyde on 11/5/17.
 * A lightweight view with a background and not much more.
 *
 */

open class CView(var name: String = "") : ViewController {

    interface PanListener {
        /**
         * Deliver a scroll (pan) event to the view.
         *
         * @param position The current position of the gesture
         * @param deltaX The distance in pixels moved since the last call to onScroll()
         * @param deltaY The distance in pixels moved since the last call to onScroll()
         * @return true if the gesture was consumed
         */
        fun onPan(position: CPoint, deltaX: Double, deltaY: Double): Boolean {
            return false
        }
    }

    val factory: GraphicsFactory by lazy { GraphicsFactory() }
    override var bounds: CRect = CRect()
    open var isVisible = true
    open var parent: ViewController? = null
    open var backgroundColor = CColor.CLEAR

    open val preferredWidth = 0.0
    open val preferredHeight = 0.0

    open val aspectRatio: Double
        get() = bounds.aspectRatio

    /**
     * Click listeners
     */

    private val clickListeners = mutableSetOf<(CView, CPoint) -> Boolean>()

    fun addClickListener(listener: (CView, CPoint) -> Boolean) {
        clickListeners.add(listener)
    }

    fun removeClickListener(listener: (CView, CPoint) -> Boolean) {
        clickListeners.remove(listener)
    }

    /**
     * Draw the background using the supplied context. The default behaviour is to fill the
     * background with the background color

     */
    open fun drawBackground(gc: GraphicsContext, rect: CRect = bounds) {
        if (isVisible && !rect.empty) {
            gc.fillColor = backgroundColor
            gc.fillRect(rect)
        }
    }

    /**
     * Draw the foreground using the supplied context
     */
    open fun draw(gc: GraphicsContext, rect: CRect = bounds) {}

    /**
     * Post a click on this view. Returns true if the view handled the click, otherwise it will
     * be passed up to its parent
     *
     * @param position
     * @return true if click has been consumed
     */
    open fun onClick(position: CPoint): Boolean {
        return clickListeners.any { it(this, position) }
    }

    /**
     * Post a double click on this view. Returns true if the view handled the click, otherwise it will
     * be passed up to its parent. Note that onClick will already have been called before this one
     *
     * @param position
     * @return true if click has been consumed
     */
    open fun onDoubleClick(position: CPoint): Boolean {
        //logMsg("Clicked %s at %s", this.getClass().getSimpleName(), position.toString());
        return false
    }

    /**
     * Deliver a long press event to the view. This will be called twice - once
     * when the gesture is first recognised, once when it ends
     *
     * @param position The position of the press
     * @param ended True if the gesture has ended
     */
    open fun onLongPress(position: CPoint, ended: Boolean) {
    }

    /**
     * Deliver a touch event to the view. This is intended to allow data to be highlighted
     * e.g. to show what is about to be dragged etc.
     *
     * @param position The position of the touch
     * @param down True if the finger is down
     */
    open fun onTouch(position: CPoint, down: Boolean) {

    }

    /**
     * Deliver a fling event to the view.
     *
     * @param position The starting position of the fling gesture
     * @param velocityX The x velocity in pixels/second
     * @param velocityY The y velocity in pixels/second
     * @return true if the gesture was consumed
     */
    open fun onFling(position: CPoint, velocityX: Double, velocityY: Double): Boolean {
        return false
    }


    /**
     * Deliver a zoom (pinch) gesture. Values delivered are cumulative
     *
     * @param position The center position of the gesture
     * @param deltaX The x scale of the zoom since the last call to this function
     * @param deltaY The y scale of the zoom since the last call to this function
     */
    open fun onZoom(position: CPoint, deltaX: Double, deltaY: Double) {

    }

    /**
     * Tell the view controller that it should redraw itself, including the background.
     */
    override fun requestRedraw(rect: CRect) {
        parent?.requestRedraw(rect)
    }

    /**
     * Tell the view controller that it should redraw the foreground
     */
    override fun redrawForeground(rect: CRect) {
        parent?.redrawForeground(rect)
    }

    companion object {
        @JvmStatic
        val PADDING = 5.0
    }
}
