package com.controlj.widget

import com.controlj.graphics.CColor
import com.controlj.settings.UnitSetting
import com.controlj.utility.Units

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 7/5/18
 * Time: 20:32
 *
 * A value view that gets its unit from a setting. It will update as the setting updates.
 */
open class SettingValueView(
        label: String,
        var setting: UnitSetting,
        precision: Int = 0,
        backgroundColor: Int = CColor.BLACK
) : TextUnitValueView(
        label = label,
        precision = precision,
        backgroundColor = backgroundColor
) {
    override val unit: Units.Unit
        get() = setting.value
}

