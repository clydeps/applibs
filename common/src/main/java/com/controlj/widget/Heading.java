package com.controlj.widget;

import com.controlj.graphics.GraphicsContext;
import com.controlj.graphics.Path;
import com.controlj.graphics.TextStyle;

/**
 * Created by clyde on 17/5/17.
 */

public class Heading extends Field {

	public Heading(String label) {
		super(label, "");
	}

	@Override
	public void drawBackground(GraphicsContext gc, Path path, TextStyle labelTextStyle, int labelColor) {
		// don't draw the frame
	}

	@Override
	public void draw(GraphicsContext gc, TextStyle valueTextStyle) {
        if(getLabel() != null)
            gc.drawText(getLabel(), getBounds(), valueTextStyle, getCurrentColor());
	}
}
