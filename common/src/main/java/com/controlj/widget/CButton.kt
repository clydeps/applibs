package com.controlj.widget

import com.controlj.framework.SystemColors
import com.controlj.graphics.CColor
import com.controlj.graphics.CPoint
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.TextStyle
import com.controlj.view.ForegroundServiceProvider
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter

/**
 * Created by clyde on 11/7/17.
 */

class CButton(textStyle: TextStyle, text: String) : RoundedTextView(textStyle, text) {

    var observable: Observable<Event>
        internal set
    private var touched: Boolean = false
    private var emitter: ObservableEmitter<Event>? = null
    private var downColor = CColor.argb(0x20, 0, 0, 0)

    override val textRectWidth: Double
        get() = bounds.width - padding * 2

    enum class Event {
        CLICK,
        LONGPRESS,
        ENDLONGPRESS
    }

    init {
        backgroundColor = SystemColors().secondarySystemBackground
        observable = Observable.create { e: ObservableEmitter<Event> -> emitter = e }.share()
        padding *= 2
    }

    override fun draw(gc: GraphicsContext, rect: CRect) {
        super.draw(gc, rect)
        if (touched) {
            gc.fillColor = downColor
            gc.fillPath(roundedRect)
        }
    }

    override fun onTouch(position: CPoint, down: Boolean) {
        if (down != touched) {
            touched = down
            redrawForeground()
        }
    }

    override fun onLongPress(position: CPoint, ended: Boolean) {
        emitter?.onNext(Event.LONGPRESS)
    }

    override fun onClick(position: CPoint): Boolean {
        emitter?.onNext(Event.CLICK)
        return true
    }
}
