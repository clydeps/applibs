package com.controlj.widget

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 8/5/18
 * Time: 14:45
 *
 * Objects implementing this interface will be periodically passed data to update their visible presentation.
 * Only valid data will be passed via update(). If a data timeout occurs invalidate() will be called, and
 * the object should remove any data being displayed from the previous update
 */
interface Updateable<T> {
    fun invalidateData()
    fun update(data: T)
}
