package com.controlj.widget

import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.Path
import com.controlj.graphics.TextStyle

/**
 * Created by clyde on 11/7/17.
 */

open class RoundedTextView(textStyle: TextStyle, text: String, name: String = "") : TextView(textStyle, text, name) {
    protected lateinit var roundedRect: Path

    override var bounds: CRect
        get() = super.bounds
        set(value) {
            super.bounds = value
            roundedRect = factory.createPath()
            roundedRect.addRoundedRect(textBounds, padding.toDouble(), padding.toDouble())
        }

    override val preferredHeight: Double
        get() = textStyle.size + padding * 4

    override fun drawBackground(gc: GraphicsContext, rect: CRect) {
        gc.fillColor = backgroundColor
        gc.fillPath(roundedRect)
    }
}
