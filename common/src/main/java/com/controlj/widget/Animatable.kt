package com.controlj.widget

/**
 * Created by clyde on 17/5/17.
 */

interface Animatable  {
    val valid: Boolean
    fun animate(progress: Double)
}
