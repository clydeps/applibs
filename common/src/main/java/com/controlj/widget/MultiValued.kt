package com.controlj.widget

import com.controlj.view.ViewController

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 10/5/18
 * Time: 21:39
 */
interface MultiValued: Animatable, ViewController {
    fun setTargetValues(values: List<Double>)
}
