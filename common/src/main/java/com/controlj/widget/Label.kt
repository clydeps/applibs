package com.controlj.widget

import com.controlj.graphics.GraphicsContext
import com.controlj.graphics.Path
import com.controlj.graphics.TextStyle

/**
 * Created by clyde on 25/5/17.
 */

open class Label(label: String, unit: String) : Field(label, unit) {

    override fun drawBackground(gc: GraphicsContext, path: Path, labelTextStyle: TextStyle, labelColor: Int) {
        super.drawBackground(gc, path, labelTextStyle, labelColor)
        gc.drawText(unitName, valueBounds, labelTextStyle, labelColor)
    }
}
