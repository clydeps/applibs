package com.controlj.ifd

import com.controlj.data.FlightPoint
import com.controlj.data.FlightRoute
import com.controlj.data.Location
import com.controlj.data.Location.Companion.IFD_PRIORITY
import com.controlj.location.LocationProvider
import com.controlj.logging.CJLog.debug
import com.controlj.logging.CJLog.log
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.network.NetworkFactory
import com.controlj.route.IFDRoute
import com.controlj.settings.BooleanSetting
import com.controlj.stratux.Stratux
import com.controlj.stratux.gdl90.GDL90
import com.controlj.traffic.TrafficProvider
import com.controlj.traffic.TrafficTarget
import com.controlj.utility.toUInt
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.IOException
import java.lang.Math.toDegrees
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.channels.SocketChannel
import java.util.concurrent.TimeUnit
import kotlin.math.atan2
import kotlin.math.sqrt

/**
 * This class decodes data from an Avidyne IFD over WiFi
 *
 * Created by clyde on 20/3/18.
 */
object IfdGps : LocationProvider {
    const val GPS_PORT = 5681
    const val AVIATION_PORT = 5677
    const val GDL90_PORT = 5678
    const val FMS_PORT = 5666
    const val GPS_BUFSIZE = 77
    const val MAX_DATALEN = 32768
    const val MAX_ERROR = 3             // max number of retries

    const val STATE_OFFS = 2
    const val TIME_OFFS = 4
    const val LAT_OFFS = 8
    const val LON_OFFS = 16
    const val ALT_OFFS = 24
    const val NVEL_OFFS = 32
    const val EVEL_OFFS = 36
    const val VVEL_OFFS = 40
    const val HPL_OFFS = 44
    const val VPL_OFFS = 48
    const val HFOM_OFFS = 52
    const val VFOM_OFFS = 56

    // Download ready buffer offsets
    const val STATUS_OFFSET = 0
    const val ID_OFFSET = 1
    const val UID_OFFSET = 5
    const val LEN_OFFSET = 6
    const val PACKETID_OFFSET = 6
    const val DATA_OFFSET = 7

    const val GPS_UPDATE = 500L  // milliseconds
    const val ROUTE_UPDATE = 2000L       // route update delay in ms

    var msgId: Byte = 0
    override var priority = IFD_PRIORITY
    override val name: String = "IFD"
    override val enabled = BooleanSetting("ifdLocation", name, "IFD location via Wi-Fi", true)
    override var isConnected: Boolean = false
        internal set

    internal enum class GPSState {
        eFault,         ///< Fault - Invalid
        eSelfTest,          ///< self test / initial state value
        eInit,              ///< Initializing
        eSearch,            ///< Acquisition
        eNav,               ///< Navigation Without Integrity
        eNavFde,            ///< Nav with FDE Integrity
        eNavSbas,           ///< Nav with SBAS Integrity *
    }

    internal enum class Dataset {
        eNoDataset,
        eRouteDataset,
        eUserWaypointDataset,
        eTestDataset,
        eTestFileErrorDataset,
        eNumberOfDatasets
    }

    internal enum class FmsCommand(val value: Byte, val len: Int) {
        eRequestUploadCmd(0, 11),
        eRequestDownloadCmd(1, 8),
        eStartDownloadCmd(4, 8),
        eResetSessionCmd(0x3F, 6),
        eUploadDataCmd(0x40, 8),
        eDownloadDataCmd(0x41, 8),
        eReady(0x80.toByte(), 8),
        eUnable(0x81.toByte(), 8),
        eFail(0x82.toByte(), 8),
        eDone(0x83.toByte(), 8),
        ePacketAck(0x84.toByte(), 8),
        ePacketNak(0x85.toByte(), 8)
    }

    internal enum class ResponseSubCode {
        eSuccessSubCode,
        eBusy,
        eNotSupported,
        eInvalidUploadId,
        eInvalidDownloadId,
        eTimedOut,
        eChecksumError,
        eFileCrcError,
        eInvalidMessageLength,
        eCannotCreateFile,
        eCannotOpenFile,
        eNoUploadActive,
        eNoDownloadActive,
        ePacketOutOfSequence,
        eUnexpectedCommand;

        val byte: Byte
            get() = ordinal.toByte()

    }

    internal fun getHeader(command: FmsCommand, len: Int = 0): ByteBuffer {
        val bufLen = command.len + len
        val buffer = ByteBuffer.allocate(bufLen)
        buffer.put(command.value)
        buffer.put(++msgId)
        buffer.put(bufLen.toByte())
        buffer.put(0)
        buffer.put(0)
        return buffer
    }

    internal fun logBuffer(msg: String, buffer: ByteBuffer) {
        val builder = StringBuilder(msg)
        builder.append(buffer.toString())
        for (i in 0..16) {
            if (i >= buffer.limit())
                break
            builder.append(String.format("%02X, ", buffer.get(i).toUInt()))
        }
        logMsg("%s", builder.toString())
    }

    internal fun writeBuffer(channel: SocketChannel, buffer: ByteBuffer) {
        buffer.put(checkSum(buffer))
        buffer.flip()
        //logBuffer("Writing: ", buffer)
        channel.write(buffer)
    }

    internal fun resetChannel(channel: SocketChannel) {
        writeBuffer(channel, getHeader(FmsCommand.eResetSessionCmd))
        val buffer = ByteBuffer.allocate(8)
        channel.read(buffer)
        logBuffer("Got in response to reset: ", buffer)
    }

    internal fun downloadFile(hostName: String, dataset: Dataset): ByteBuffer {
        // request download of the route dataset
        var buffer: ByteBuffer
        val channel = NetworkFactory.getTcpChannel(hostName, FMS_PORT)
        // write a route download request. Format of message:
        //     <cmd> <msgId> <len> 0 0 <dataset> 0 <checksum>
        //
        var errors = MAX_ERROR
        var readBuffer = ByteBuffer.allocate(11).order(ByteOrder.BIG_ENDIAN)
        do {
            buffer = getHeader(FmsCommand.eRequestDownloadCmd)
            val id = msgId
            buffer.put(dataset.ordinal.toByte())
            buffer.put(0)   // ???
            writeBuffer(channel, buffer)
            readBuffer.clear()
            val cnt = channel.read(readBuffer)
            //logBuffer("Read:", readBuffer)
            if (cnt != 11 ||
                readBuffer.get(STATUS_OFFSET) != FmsCommand.eReady.value ||
                readBuffer.get(ID_OFFSET) != id
            ) {
                logMsg(
                    "Read %d bytes from socket, status = %02X, msgid was %02X, expected %02X",
                    cnt, readBuffer.get(STATUS_OFFSET).toUInt(), readBuffer.get(ID_OFFSET).toUInt(), id.toUInt()
                )
                if (--errors == 0) {
                    throw IOException("could not start download")
                }
                resetChannel(channel)
                continue
            }
            break
        } while (true)
        // get uid and length of data

        val uid = readBuffer.get(UID_OFFSET)
        var nextPacketId: Byte = 0
        val dataLen = readBuffer.getInt(LEN_OFFSET)
        logMsg("Got datalen of %d", dataLen)
        if (dataLen > MAX_DATALEN) {
            resetChannel(channel)
            channel.close()
            throw IllegalArgumentException("datalen exceeds limit")
        }
        val dataBuffer = ByteBuffer.allocate(dataLen)
        buffer = getHeader(FmsCommand.eStartDownloadCmd)
        buffer.put(ID_OFFSET, uid)
        buffer.put(uid)
        buffer.put(0)
        writeBuffer(channel, buffer)
        readBuffer = ByteBuffer.allocate(256).order(ByteOrder.BIG_ENDIAN)
        do {
            var ok = false
            readBuffer.clear()
            buffer = getHeader(FmsCommand.ePacketNak)
            val cnt = channel.read(readBuffer)
            //logBuffer("Read:", readBuffer)
            if (cnt < 8 || cnt - 8 > dataBuffer.remaining())
                buffer.put(ResponseSubCode.eInvalidMessageLength.byte)
            else if (checkSum(readBuffer, cnt - 1) != readBuffer.get(cnt - 1))
                buffer.put(ResponseSubCode.eChecksumError.byte)
            else if (readBuffer.get(UID_OFFSET) != uid) {
                buffer.put(ResponseSubCode.eInvalidDownloadId.byte)
            } else if (readBuffer.get(PACKETID_OFFSET) != nextPacketId)
                buffer.put(ResponseSubCode.ePacketOutOfSequence.byte)
            else {
                dataBuffer.put(readBuffer.array(), DATA_OFFSET, cnt - 8)
                buffer.put(0, FmsCommand.ePacketAck.value)
                buffer.put(1, uid)
                buffer.put(ResponseSubCode.eSuccessSubCode.byte)
                buffer.put(nextPacketId++)
                ok = true
            }
            while (buffer.remaining() != 1)
                buffer.put(0)
            writeBuffer(channel, buffer)
            if (!ok) {
                resetChannel(channel)
                channel.close()
                throw IOException("Error")
            }
        } while (dataBuffer.hasRemaining())
        channel.close()
        logBuffer("Rx data: ", dataBuffer)
        dataBuffer.order(ByteOrder.BIG_ENDIAN)
        dataBuffer.flip()
        val isRle = dataBuffer.get()
        val outlen = dataBuffer.int
        if (outlen > MAX_DATALEN)
            throw IOException("Excessive uncompressed length $outlen")
        val outBuffer = ByteBuffer.allocate(outlen)
        if (isRle == 1.toByte()) {
            val checkPos = dataBuffer.limit() - 2
            val checksum = dataBuffer.getShort(checkPos).toUInt()
            val wrapBuffer = ByteBuffer.wrap(dataBuffer.array(), 5, dataBuffer.limit() - 7)
            val fletcher = fletcher16(wrapBuffer)
            if (fletcher != checksum)
                throw IOException("Checksum mismatch - expected $checksum, got $fletcher")
            // rewind the buffer
            wrapBuffer.position(5)
            try {
                rleDecompress(wrapBuffer, outBuffer)
            } catch (e: Exception) {
                throw IOException("Error decompressing buffer: $e")
            }
        } else {
            outBuffer.put(dataBuffer.array(), 5, dataBuffer.remaining())
        }
        logBuffer("Decoded buffer: ", outBuffer)
        outBuffer.flip()
        return outBuffer
    }

    val udpObserver = NetworkFactory.observeUdpBroadcast(GPS_PORT)
        .subscribeOn(Schedulers.io())
        .share()

    override val authorisationStatus = LocationProvider.AuthorisationStatus.Always

    override val locationObserver = udpObserver
        .throttleFirst(GPS_UPDATE, TimeUnit.MILLISECONDS)
        .map { data ->
            val loc = decodeGps(data.data)
            //debug("IFD GPS data $loc")
            loc
        }.subscribeOn(Schedulers.io())
        .retryWhen {
            it.flatMap {
                Observable.timer(10, TimeUnit.SECONDS)
            }
        }
        .share()

    val fmsObserver = udpObserver
        .throttleFirst(ROUTE_UPDATE, TimeUnit.MILLISECONDS)
        .map { data ->
            try {
                val buffer = downloadFile(data.address.hostName, Dataset.eRouteDataset)
                IFDRoute.decode(buffer).log()
            } catch (e: Exception) {
                logException(e)
            }
        }.subscribeOn(Schedulers.io())
        .share()

    val aviationObserver = udpObserver
        .take(1)
        .flatMap { data ->
            Observable.create { emitter: ObservableEmitter<FlightRoute<FlightPoint>> ->
                try {
                    Thread {
                        debug { "Starting thread for TCP read port $AVIATION_PORT, hostname=${data.address.hostAddress}" }
                        try {
                            val channel = NetworkFactory.getTcpChannel(data.address.hostAddress, AVIATION_PORT)
                            emitter.setCancellable {
                                debug("closing TCP socket")
                                channel.close()
                            }
                            var avData = AviationData(IFD_PRIORITY)
                            val readBuffer = ByteBuffer.allocate(1024)
                            channel.socket().soTimeout = 3000
                            do {
                                readBuffer.clear()
                                channel.read(readBuffer)
                                if (readBuffer.remaining() == 0) {
                                    emitter.onError(IOException("EOF on TCP socket"))
                                    break
                                }
                                isConnected = true
                                //debug("read ${readBuffer.position()} bytes from channel")
                                readBuffer.flip()
                                while (readBuffer.hasRemaining() && avData.pump(readBuffer)) {
                                    //debug("Got Avdata $avData")
                                    emitter.onNext(avData.route)
                                    avData = AviationData(IFD_PRIORITY)
                                }
                            } while (!emitter.isDisposed)
                            debug("closing channel - isdisposed = ${emitter.isDisposed}, channel.isconnected=${channel.isConnected}")
                            channel.close()
                            isConnected = false
                        } catch (ex: Exception) {
                            logMsg(ex.toString())
                            emitter.tryOnError(ex)
                        }
                    }.start()
                } catch (ex: Exception) {
                    logMsg(ex.toString())
                    emitter.tryOnError(ex)
                }
            }
        }
        .startWithItem(FlightRoute())
        .subscribeOn(Schedulers.io())
        .retryWhen {
            it.flatMap {
                Observable.timer(10, TimeUnit.SECONDS)
            }
        }
        .share()



    private fun decodeGps(byteArray: ByteArray): Location {
        if (byteArray.size < GPS_BUFSIZE)
            return Location.invalid
        val buffer = ByteBuffer.wrap(byteArray)
            .order(ByteOrder.BIG_ENDIAN)
        when (GPSState.values()[buffer.get(STATE_OFFS).toInt()]) {
            GPSState.eNav, GPSState.eNavFde, GPSState.eNavSbas -> {
                val lat = toDegrees(buffer.getDouble(LAT_OFFS))
                val lon = toDegrees(buffer.getDouble(LON_OFFS))
                val alt = buffer.getDouble(ALT_OFFS)
                val nVel = buffer.getFloat(NVEL_OFFS)
                val eVel = buffer.getFloat(EVEL_OFFS)
                val vVel = buffer.getFloat(VVEL_OFFS)
                val speed = sqrt((nVel * nVel + eVel * eVel + vVel * vVel)).toDouble()
                val track = (toDegrees(atan2(eVel.toDouble(), nVel.toDouble())) + 360.0) % 360.0
                val location =
                    Location.of(
                        lat,
                        lon,
                        alt,
                        speed,
                        track,
                        creator,
                        accuracyH = buffer.getFloat(HFOM_OFFS).toDouble(),
                        accuracyV = buffer.getFloat(VFOM_OFFS).toDouble(),
                    )
                return location
                // TODO set timestamp
            }
            else -> return Location.invalid
        }
    }

    private fun rleDecompress(inbuf: ByteBuffer, outbuf: ByteBuffer) {
        while (inbuf.hasRemaining()) {
            val b = inbuf.get().toUInt()
            if ((b >= 0x80)) {
                for (i in 0 until (b - 0x80))
                    outbuf.put(inbuf.get())
            } else {
                val d = inbuf.get()
                for (i in 0 until b)
                    outbuf.put(d)
            }
        }
    }

    fun fletcher16(buffer: ByteBuffer): Int {
        var s1 = 0
        var s2 = 0
        while (buffer.hasRemaining()) {
            val b = buffer.get().toUInt()
            s1 = (s1 + b) % 255
            s2 = (s2 + s1) % 255
        }
        return s1 + s2 * 256
    }

    fun checkSum(buffer: ByteBuffer, len: Int = buffer.position()): Byte {
        var value = 0
        for (i in 0 until len)
            value += 1 + i + buffer.get(i)
        return value.toByte()
    }
}


object IfdTraffic : TrafficProvider {
    private class Pump {
        val buffer: ByteBuffer = ByteBuffer.allocate(1024)

        /**
         * Add a byte to the buffer, return true when the buffer is ready for reading
         */
        fun pump(byte: Byte): Boolean {
            when {
                // if there is data in the buffer and this is an end byte, return it
                byte == Stratux.FLAG_BYTE -> {
                    if (buffer.position() <= 1) {
                        buffer.clear()
                        buffer.put(byte)
                        return false
                    } else {
                        buffer.put(byte)
                        buffer.flip()
                        return true
                    }
                }
                buffer.position() != 0 -> {
                    buffer.put(byte)
                    return false
                }
                else -> return false
            }
        }

        fun getBytes(): ByteArray {
            return buffer.array().copyOf(buffer.limit())
        }
    }
    override var isConnected: Boolean = false
        private set

    val gdl90Observer: Observable<GDL90> = IfdGps.udpObserver.take(1).flatMap { data ->
        Observable.create<GDL90> { emitter ->
            Thread {
                debug("Starting thread for TCP read, hostname=${data.address.hostAddress}")
                try {
                    val readBuffer = ByteBuffer.allocate(1024)
                    val buffer = Pump()
                    NetworkFactory.getTcpChannel(data.address.hostAddress, IfdGps.GDL90_PORT).use { channel ->
                        channel.socket().soTimeout = 3000
                        do {
                            readBuffer.clear()
                            val count = channel.read(readBuffer)
                            if (count <= 0) {
                                emitter.onError(IOException("EOF on TCP socket"))
                                break
                            }
                            //debug("read ${readBuffer.position()} bytes from channel")
                            isConnected = true
                            readBuffer.flip()
                            while (readBuffer.hasRemaining()) {
                                if (buffer.pump(readBuffer.get())) {
                                    Stratux.decode(buffer.getBytes()).map { bytes ->
                                        GDL90.toGdl90(bytes, data.address).debug()
                                    }
                                        .forEach { emitter.onNext(it) }
                                }
                            }
                        } while (!emitter.isDisposed)
                        channel.close()
                        debug("closing channel - isdisposed = ${emitter.isDisposed}, channel.isconnected=${channel.isConnected}")
                    }
                } catch (ex: Exception) {
                    logMsg(ex.toString())
                    emitter.tryOnError(ex)
                }
                isConnected = false
            }.start()
        }
    }
        .retryWhen {
            it.flatMap {
                Observable.timer(10, TimeUnit.SECONDS)
            }
        }
        .share()

    override val trafficObserver: Observable<TrafficTarget> =
        gdl90Observer.ofType(TrafficTarget::class.java).share()
    override val name: String = "IFD"
    override val enabled = BooleanSetting("ifdTraffic", name, "IFD traffic via Wi-Fi", true)
}
