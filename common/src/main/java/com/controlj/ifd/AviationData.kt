package com.controlj.ifd

import com.controlj.GIS.riemann.removeVariation
import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.FlightPoint
import com.controlj.data.FlightRoute
import com.controlj.data.Location
import com.controlj.data.magneticVariation
import com.controlj.utility.Units
import org.threeten.bp.Instant
import java.nio.ByteBuffer
import java.util.ArrayList

/**
 * Created by clyde on 20/3/18.
 */
class AviationData(val priority: Int) : Location {
    private val buffer = ByteBuffer.allocate(32)
    var route = FlightRoute<FlightPoint>("Aviation", priority)
    var destid: String? = null
    var wptDist = INVALID_DATA
    var destDist = INVALID_DATA
    var xtrack = INVALID_DATA
    var dtk = INVALID_DATA
    var dstBrg = INVALID_DATA
    var magVar = INVALID_DATA
    override var altitude = INVALID_DATA
        private set
    override val accuracyH: Double = ACCURACY
    override val accuracyV: Double = ACCURACY
    override val creator: Location.Creator = aviationCreator
    override var latitude = INVALID_DATA
        private set
    override var longitude = INVALID_DATA
        private set
    override var speed: Double = INVALID_DATA
        private set
    override var timestamp: Instant = Instant.now()
        private set
    override var magneticTrack: Double = INVALID_DATA
        private set
    override val track: Double get() = magneticTrack.removeVariation(magneticVariation)
    private var state = 0

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append(Location.toString(this))
        sb.append(": WPTdist ")
        sb.append(Units.Unit.NM.convertTo(wptDist))
        sb.append("nm TRK ")
        sb.append(track)
        sb.append(" DTK ")
        sb.append(dtk)
        sb.append(" DEST ")
        sb.append(destid)
        sb.append(" ")
        sb.append(Units.Unit.NM.convertTo(destDist))
        sb.append("nm BRG ")
        sb.append(dstBrg)
        sb.append("; Route ")
        sb.append(route.toString())
        return sb.toString()
    }

    val points: List<FlightPoint>
        get() = route.points

    enum class GpsAction {
        ACCEPT, CONTINUE, FAIL, COMPLETE
    }

    private fun reset() {
        route = FlightRoute("Aviation", priority, ArrayList())
        altitude = INVALID_DATA
        latitude = INVALID_DATA
        longitude = INVALID_DATA
        speed = INVALID_DATA
        destDist = INVALID_DATA
        wptDist = INVALID_DATA
    }

    /**
     * Pump data into the lexical analyser. Returns true when a complete packet has been read.
     *
     * @param data
     * @return
     */
    fun pump(data: ByteBuffer): Boolean { //System.out.format("Pumping %d bytes\n", data.remaining());
        while (data.hasRemaining()) {
            val b = data.get()
            if (buffer.hasRemaining()) buffer.put(b)
            when (gpsLex(b.toInt().toChar())) {
                GpsAction.CONTINUE -> {
                }
                GpsAction.ACCEPT ->  //System.out.format("Accept at state %d\n", state);
                    state = VALID_STATE
                GpsAction.FAIL ->  //System.out.format("Fail at state %d\n", state);
                    state = 0
                GpsAction.COMPLETE -> return true
            }
        }
        return false
    }

    fun gpsLex(token: Char): GpsAction { //System.out.format("lex byte %02X in state %d\n", token, state);
        return when (state) {
            VALID_STATE -> {
                when (token) {
                    'A' -> {
                        buffer.clear()
                        state = 1
                        return GpsAction.CONTINUE
                    }
                    'B' -> {
                        buffer.clear()
                        state = 12
                        return GpsAction.CONTINUE
                    }
                    0x03.toChar() -> return GpsAction.COMPLETE
                    'C' -> {
                        buffer.clear()
                        state = 25
                        return GpsAction.CONTINUE
                    }
                    'D' -> {
                        buffer.clear()
                        state = 30
                        return GpsAction.CONTINUE
                    }
                    'E' -> {
                        buffer.clear()
                        state = 35
                        return GpsAction.CONTINUE
                    }
                    'G' -> {
                        buffer.clear()
                        state = 42
                        return GpsAction.CONTINUE
                    }
                    'I' -> {
                        buffer.clear()
                        state = 49
                        return GpsAction.CONTINUE
                    }
                    'K' -> {
                        buffer.clear()
                        state = 55
                        return GpsAction.CONTINUE
                    }
                    'L' -> {
                        buffer.clear()
                        state = 62
                        return GpsAction.CONTINUE
                    }
                    'l' -> {
                        buffer.clear()
                        state = 68
                        return GpsAction.CONTINUE
                    }
                    'Q' -> {
                        buffer.clear()
                        state = 76
                        return GpsAction.CONTINUE
                    }
                    'S' -> {
                        buffer.clear()
                        state = 82
                        return GpsAction.CONTINUE
                    }
                    'T' -> {
                        buffer.clear()
                        state = 89
                        return GpsAction.CONTINUE
                    }
                    'w' -> {
                        buffer.clear()
                        state = 100
                        return GpsAction.CONTINUE
                    }
                    'z' -> {
                        buffer.clear()
                        state = 120
                        return GpsAction.CONTINUE
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            1 -> {
                when (token) {
                    'N', 'S', '-' -> {
                        ++state // = 2;
                        return GpsAction.CONTINUE
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            2, 5, 17, 13 -> {
                when (token) {
                    ' ' -> {
                        ++state // = 3;
                        return GpsAction.CONTINUE
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            3, 4, 120, 121, 122, 123, 124, 6, 7, 8, 9, 62, 63, 64, 65, 100, 101, 77, 78, 79, 68, 69, 70, 71, 72, 73, 49, 50, 51, 52, 43, 44, 45, 46, 35, 36, 37, 38, 39, 30, 31, 32, 25, 26, 27, 18, 19, 20, 21, 14, 15, 16 -> {
                if (token == '-' || token >= '0' && token <= '9') {
                    ++state // = 5;
                    return GpsAction.CONTINUE
                }
                GpsAction.FAIL
            }
            10, 125, 118, 60, 98, 87, 80, 74, 66, 53, 47, 40, 33, 28, 22 -> {
                when (token) {
                    '\r' -> {
                        ++state // = 11;
                        return GpsAction.CONTINUE
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            11 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        if (buffer[2] == '-'.code.toByte()) latitude = INVALID_DATA else {
                            val sn = buffer.get()
                            buffer.get()
                            val dd = parseInt(buffer, 2, 1.0)
                            buffer.get()
                            val mmhh = parseInt(buffer, 4, 6000.0)
                            if (sn == '-'.code.toByte() || dd == INVALID_DATA) latitude = INVALID_DATA else {
                                latitude = dd + mmhh
                                if (sn == 'S'.code.toByte()) latitude = -latitude
                            }
                        }
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            12, 76 -> {
                when (token) {
                    'E', 'W', '-' -> {
                        ++state // = 13;
                        return GpsAction.CONTINUE
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            23 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        if (buffer[2] == '-'.code.toByte()) longitude = -10000.0 else {
                            val sn = buffer.get()
                            buffer.get()
                            val dd = parseInt(buffer, 3, 1.0)
                            buffer.get()
                            val mmhh = parseInt(buffer, 4, 6000.0)
                            if (sn == '-'.code.toByte() || dd == INVALID_DATA) longitude = INVALID_DATA else {
                                longitude = dd + mmhh
                                if (sn == 'W'.code.toByte()) longitude = -longitude
                            }
                        }
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            29 -> {
                when (token) {
                    '\n' -> {
                        // track
                        buffer.flip()
                        magneticTrack = parseInt(buffer, 3, 1.0)
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            34 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        speed = parseInt(buffer, 3, 1.0, Units.Unit.KNOT)
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            41 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        wptDist = parseInt(buffer, 5, 10.0, Units.Unit.NM)
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            42 -> {
                when (token) {
                    'L', 'R', '-' -> {
                        ++state // = 43;
                        return GpsAction.CONTINUE
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            48 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        val dir = buffer.get()
                        xtrack = parseInt(buffer, 4, 100.0, Units.Unit.NM)
                        if (dir == 'L'.code.toByte()) xtrack = -xtrack
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            54 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        dtk = parseInt(buffer, 4, 10.0)
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            55, 56, 57, 58, 59, 105, 106, 107, 108 -> {
                if (token == '-' || token in '0'..'9' || token in 'A'..'Z' || token in 'a'..'z' || token == ' ') {
                    ++state // = 60;
                    return GpsAction.CONTINUE
                }
                GpsAction.FAIL
            }
            61 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        val data = ByteArray(5)
                        buffer[data]
                        destid = String(data).trim { it <= ' ' }
                        if (destid == "GARMN" || destid == "AVIDN") reset()
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            67 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        dstBrg = parseInt(buffer, 4, 10.0)
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            75 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        destDist = parseInt(buffer, 6, 10.0, Units.Unit.NM)
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            81 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        val ew = buffer.get().toInt().toChar()
                        magVar = parseInt(buffer, 3, 10.0)
                        if (ew == 'E') magVar = -magVar
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            82, 83, 84, 85, 86, 89, 90, 91, 92, 93, 94, 95, 96, 97 -> {
                when (token) {
                    '-', 'N' -> {
                        ++state // = 87;
                        return GpsAction.CONTINUE
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            88 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        if (buffer[4] == 'N'.code.toByte())   // nav is flagged
                            reset()
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            99 -> {
                when (token) {
                    '\n' -> {
                        run {}
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            102 -> {
                ++state // = 103;
                GpsAction.CONTINUE
            }
            103 -> {
                if (token == '-' || token in '0'..'9' || token in 'A'..'Z' || token in 'a'..'z') {
                    state = 105
                    return GpsAction.CONTINUE
                }
                when (token) {
                    '\r' -> {
                        ++state // = 104;
                        return GpsAction.CONTINUE
                    }
                    ' ' -> {
                        state = 105
                        return GpsAction.CONTINUE
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            104 -> {
                when (token) {
                    '\n' ->  // empty route
                        return GpsAction.ACCEPT
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            109, 110, 111, 112, 113, 114, 115, 116, 117 -> {
                ++state // = 118;
                GpsAction.CONTINUE
            }
            119 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        // decode route entry
                        buffer.get() // skip sequence number in ascii
                        buffer.get()
                        val seq: Int = buffer.get().toInt() and 0x7F
                        val b = ByteArray(5)
                        buffer[b]
                        val id = String(b).trim { it <= ' ' }
                        var deg: Int = buffer.get().toInt() and 0xFF
                        var min: Int = buffer.get().toInt() and 0x3F
                        var hmin: Int = buffer.get().toInt() and 0x7F
                        var latitude = (deg and 0x7F) + min / 60.0 + hmin / 6000.0
                        if (deg and 0x80 != 0) latitude = -latitude
                        val ns: Int = buffer.get().toInt() and 0x80
                        deg = buffer.get().toInt() and 0xFF
                        min = buffer.get().toInt() and 0x3F
                        hmin = buffer.get().toInt() and 0x7F
                        var longitude = deg + min / 60.0 + hmin / 6000.0
                        if (ns != 0) longitude = -longitude
                        route.add(FlightPoint.of(id, latitude, longitude), seq and 0x20 != 0)
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            126 -> {
                when (token) {
                    '\n' -> {
                        buffer.flip()
                        altitude = parseInt(buffer, 5, 1.0, Units.Unit.FOOT)
                        return GpsAction.ACCEPT
                    }
                    else -> {
                    }
                }
                GpsAction.FAIL
            }
            else -> {
                if (token.code == 0x02) {
                    state = VALID_STATE
                    reset()
                    return GpsAction.ACCEPT
                }
                GpsAction.FAIL
            }
        }
    }

    companion object {
        const val ACCURACY = 10.0 // use a poor accuracy since we can't actually tell
        private const val VALID_STATE = 1000 // state after STX seen
        private val aviationCreator = Location.Creator("BlueMAX", 50)
        fun parseInt(buffer: ByteBuffer, len: Int, precision: Double, fromUnit: Units.Unit = Units.Unit.IDENTITY): Double {
            val bytes = ByteArray(len)
            buffer.get(bytes)
            return if (bytes[0].toInt().toChar() == '-')
                INVALID_DATA
            else
                fromUnit.convertFrom(String(bytes).toInt() / precision)
        }
    }

    init {
        reset()
    }
}
