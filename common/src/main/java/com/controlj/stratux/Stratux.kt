package com.controlj.stratux

import com.controlj.data.Location
import com.controlj.location.GpsLocation
import com.controlj.location.LocationProvider
import com.controlj.logging.CJLog.debug
import com.controlj.logging.CJLog.logMsg
import com.controlj.network.NetworkFactory
import com.controlj.network.addCrc16
import com.controlj.network.crc16
import com.controlj.nmea.GpsStatus
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.observeOnMainBy
import com.controlj.settings.BooleanSetting
import com.controlj.settings.IntSetting
import com.controlj.stratux.gdl90.ForeFlight
import com.controlj.stratux.gdl90.GDL90
import com.controlj.stratux.gdl90.GDL90TrafficReport
import com.controlj.stratux.gdl90.OwnshipAltitude
import com.controlj.stratux.gdl90.OwnshipReport
import com.controlj.stratux.gdl90.StratuxStatus
import com.controlj.traffic.TrafficProvider
import com.controlj.traffic.TrafficTarget
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import java.util.concurrent.TimeUnit
import kotlin.experimental.xor


/**
 * Receive and decode GDL90 data from a Stratux or compatible UDP source
 */

object Stratux : LocationProvider {

    private var skyDisposable = Disposable.disposed()


    private val status = GpsStatus.Builder()

    val portSetting = IntSetting(
        "listenPort", "GDL90 listen port",
        "UDP port on which to listen for GDL90 data",
        4000
    )
    const val FLAG_BYTE = 0x7E.toByte()
    private const val CONTROL_BYTE = 0x7D.toByte()
    private const val maxErrors = 20
    private var errorCount = 0
    private var blockCount = 0

    override var isConnected: Boolean = false
        private set

    private var emitter: ObservableEmitter<GDL90> = DisposedEmitter()
    private val disposables = CompositeDisposable()

    /**
     * Start the flow. Watch for changes in the port number and restart if it changes (with debounce)
     */
    private fun startFlow() {
        disposables.clear()
        disposables.add(getGenerator().observeOnMainBy {
            when (it) {
                is StratuxStatus -> {
                    status.gpsFix = it.gpsFix
                    status.satellitesUsed = it.satellitesLocked
                    status.satellitesInView = it.satellitesTracked
                }

                is OwnshipReport -> {
                    status.baroAltitude = it.altitude
                }

                is OwnshipAltitude -> {
                    status.geoAltitude = it.geoAltitude.toDouble()
                }
            }
            emitter.onNext(it)
        })
        disposables.add(portSetting.observable
            .debounce(10, TimeUnit.SECONDS)
            .distinct()
            .observeOnMainBy {
                startFlow()
            })
    }

    val observer: Observable<GDL90> = Observable.create {
        emitter = it
        startFlow()
    }
        .doOnDispose { disposables.clear() }
        .share()

    fun getGenerator(): Observable<GDL90> = NetworkFactory.observeUdpBroadcast(portSetting.value)
        .doOnSubscribe {
            blockCount = 0
            errorCount = 0
        }
        .doFinally {
            isConnected = false
        }
        .flatMap { udpData ->
            Observable.fromIterable(
                try {
                    decode(udpData.data)
                        .filter { it.isNotEmpty() }
                        .map {
                            isConnected = true
                            GDL90.toGdl90(it, udpData.address)
                        }
                } catch (ex: Exception) {
                    if (errorCount != maxErrors) {
                        logMsg("$ex from ${ex.stackTraceToString().lines().first()}")
                        errorCount++
                    }
                    listOf<GDL90>()
                })
        }

    /**
     * Given a block of bytes, decode into valid GDL90 packets.
     * The packet will be surrounded by flag bytes, and within the packet
     * and flag bytes or control bytes are represented by a two byte sequence
     */
    fun decode(data: ByteArray): List<ByteArray> {
        val blocks = mutableListOf<List<Byte>>()
        data.fold(mutableListOf<Byte>()) { list, c ->
            if (c == FLAG_BYTE) {
                if (list.isNotEmpty()) {
                    blocks.add(list.drop(1))
                    list.clear()
                } else {
                    list.add(c)
                }
            } else {    // not a flag byte
                if (list.isEmpty()) {
                    if (blockCount != maxErrors) {
                        logMsg("Missing start byte - block is ${
                            data.joinToString(" ") { "%02X".format(it.toInt() and 0xFF) }
                        }}")
                        blockCount++
                    }
                } else
                    list.add(c)
            }
            list
        }
        //if (result.isNotEmpty())
        //logMsg("Missing end byte")
        return blocks.map {
            val output = mutableListOf<Byte>()
            if (it.fold(false) { flag, b ->
                    when {
                        flag -> {
                            output.add(b xor 0x20)
                            false
                        }

                        b == CONTROL_BYTE -> true
                        else -> {
                            output.add(b)
                            false
                        }
                    }
                })
                error("Control byte at end of buffer")
            val arra = output.dropLast(2).toByteArray()
            val calced = arra.crc16
            val crcBytes = output.takeLast(2)
            val crc = (crcBytes[0].toInt() and 0xFF) + ((crcBytes[1].toInt() and 0xFF) shl 8)
            if (crc != calced) {
                logMsg("Bad CRC for block type ${arra[0]}")
                debug {
                    val bytes = output.joinToString(" ") { "%X".format(it.toInt() and 0xFF) }
                    "Calced=%X, expected=%X, bytes=$bytes".format(calced, crc)
                }
                byteArrayOf()
            } else
                arra
        }
    }

    /**
     * Encode a GDL90 packet into a properly formatted transmittable block.
     */

    fun GDL90.encode(): ByteArray {
        return sequence {
            yield(FLAG_BYTE)
            this@encode.databytes.addCrc16.forEach {
                when (it) {
                    FLAG_BYTE, CONTROL_BYTE -> {
                        yield(CONTROL_BYTE)
                        yield(it xor 0x20)
                    }

                    else -> yield(it)
                }
            }
            yield(FLAG_BYTE)
        }.toList().toByteArray()
    }

    override val name: String = "GDL-90"
    override val enabled = BooleanSetting("gdl90Location", name, "GDL-90 via Wi-Fi", true)
    override val locationObserver: Observable<Location>
        get() = observer
            .map {
                if (skyDisposable.isDisposed && it is ForeFlight.IDMsg && it.deviceName.startsWith("SkyEcho"))
                    it.sourceAddress?.let { address ->
                        skyDisposable = SkyEcho(address.hostAddress).observable.observeOnMainBy {
                            status.apply {
                                baroAltitude = it.baroAltitude
                                geoAltitude = it.geoAltitude
                                gpsFix = it.gpsFix
                                satellitesInView = it.satellitesInView
                                satellitesUsed = it.satellitesUsed
                            }
                        }
                    }
                it
            }
            .filter { it is OwnshipReport }
            .map {
                GpsLocation.of(it as Location, status.copy)
            }

    override var priority: Int = Location.GDL_PRIORITY
    override val authorisationStatus: LocationProvider.AuthorisationStatus =
        LocationProvider.AuthorisationStatus.Always
}

object StratuxTraffic : TrafficProvider {

    override var isConnected: Boolean = false
        private set
    override val trafficObserver: Observable<TrafficTarget>
        get() = Stratux.observer
            .doFinally { isConnected = false }
            .filter { it is GDL90TrafficReport }.map {
                isConnected = true
                it as TrafficTarget
            }

    override val name: String = "GDL-90"
    override val enabled = BooleanSetting("gdl90Traffic", name, "GDL-90 via Wi-Fi", true)
}
