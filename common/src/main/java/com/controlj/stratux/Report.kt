package com.controlj.stratux

/**
 * A catch-all top level interface for GDL90 reports and the like.
 */
interface Report
