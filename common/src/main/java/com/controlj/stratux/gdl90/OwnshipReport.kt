package com.controlj.stratux.gdl90

import com.controlj.data.magneticVariation
import com.controlj.utility.Units
import org.threeten.bp.ZoneOffset
import org.threeten.bp.temporal.ChronoField
import java.lang.Math.abs
import java.net.InetAddress
import kotlin.math.roundToInt

@ExperimentalUnsignedTypes
class OwnshipReport(address: InetAddress? = null) : PositionReport(ID, address) {

    override fun fromBytes(bytes: ByteArray): OwnshipReport {
        super.fromBytes(bytes)
        return this
    }
    companion object {
        const val ID = 10
    }
}
