package com.controlj.stratux.gdl90

import com.controlj.utility.asUInt
import com.controlj.utility.bit
import com.controlj.utility.putString
import java.net.InetAddress
import java.nio.ByteBuffer
import java.nio.ByteOrder

abstract class ForeFlight(val subType: Int, address: InetAddress? = null) : GDL90(ID, address) {

    override fun check(bytes: ByteArray) {
        super.check(bytes)
        require(bytes[1].asUInt() == subType)
    }

    class IDMsg(address: InetAddress? = null) : ForeFlight(SUB_ID, address) {

        var version: Int = 1
        var serialNumber: ULong = ULong.MAX_VALUE
        var deviceName: String = ""
        var longDeviceName: String = ""
        var usesMSL: Boolean = false

        override fun fromBytes(bytes: ByteArray): IDMsg {
            check(bytes)
            version = bytes[2].asUInt()
            serialNumber = bytes.getULong(3)
            deviceName = String(bytes, 11, 8).trim()
            longDeviceName = String(bytes, 19, 16).trim()
            usesMSL = bytes[35].bit(0)
            return this
        }

        override fun toString(): String {
            return "IDMsg(version=$version, serialNumber=$serialNumber, deviceName='$deviceName', longDeviceName='$longDeviceName', usesMSL=$usesMSL)"
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other !is IDMsg) return false

            if (version != other.version) return false
            if (serialNumber != other.serialNumber) return false
            if (deviceName != other.deviceName) return false
            if (longDeviceName != other.longDeviceName) return false
            if (usesMSL != other.usesMSL) return false

            return true
        }

        override fun hashCode(): Int {
            var result = serialNumber.hashCode()
            result = 31 * result + deviceName.hashCode()
            return result
        }

        override val messageLength: Int = 39

        override val databytes: ByteArray
            get() {
                val buffer = ByteBuffer.allocate(messageLength)
                buffer.order(ByteOrder.BIG_ENDIAN)
                buffer.put(type.toByte())
                buffer.put(subType.toByte())
                buffer.put(version.toByte())
                buffer.putLong(serialNumber.toLong())
                buffer.put(deviceName.take(8).padEnd(8, ' ').toByteArray())
                buffer.put(longDeviceName.take(16).padEnd(16, ' ').toByteArray())
                buffer.put(if (usesMSL) 1 else 0)
                return buffer.array()
            }

        companion object {
            const val SUB_ID = 0
        }
    }

    class AHRS(address: InetAddress? = null) : ForeFlight(SUB_ID, address) {

        var roll: Double = 0x7FFF / 10.0
        var pitch: Double = 0x7FFF / 10.0
        var heading: Double = 0x7FFF / 10.0
        var ias: Double = 65535.0
        var tas: Double = 65535.0

        override val databytes: ByteArray
            get() {
                val buffer = ByteBuffer.allocate(messageLength)
                buffer.order(ByteOrder.BIG_ENDIAN)
                buffer.put(type.toByte())
                buffer.put(subType.toByte())
                buffer.putShort((roll * 10.0).toInt().toShort())
                buffer.putShort((pitch * 10.0).toInt().toShort())
                buffer.putShort((heading * 10.0).toInt().toShort())
                buffer.putShort(ias.toInt().toShort())
                buffer.putShort(tas.toInt().toShort())
                return buffer.array()
            }

        override fun fromBytes(bytes: ByteArray): AHRS {
            check(bytes)
            roll = bytes.get16BitSigned(2) / 10.0
            pitch = bytes.get16BitSigned(4) / 10.0
            heading = bytes.get16BitSigned(6) / 10.0
            ias = bytes.get16BitSigned(8).toDouble()
            tas = bytes.get16BitSigned(10).toDouble()
            return this
        }

        override fun toString(): String {
            return "AHRS(roll=$roll, pitch=$pitch, heading=$heading, ias=$ias, tas=$tas)"
        }

        override val messageLength: Int = 12

        companion object {
            const val SUB_ID = 1
        }
    }

    class Unknown(bytes: ByteArray, address: InetAddress? = null) : ForeFlight(bytes[1].asUInt(), address) {
        override var databytes: ByteArray = bytes
        override val messageLength: Int = databytes.size

        override fun fromBytes(bytes: ByteArray): GDL90 {
            databytes = bytes
            return this
        }


        override fun toString(): String {
            return "GDL90.Unknown($type, size=$messageLength)"
        }
    }

    companion object {
        const val ID = 101
        fun fromBytes(bytes: ByteArray, address: InetAddress? = null): ForeFlight {
            return when (bytes[1].toInt()) {
                AHRS.SUB_ID -> AHRS(address).fromBytes(bytes)
                IDMsg.SUB_ID -> IDMsg(address).fromBytes(bytes)
                else -> Unknown(bytes, address)

            }
        }
    }
}
