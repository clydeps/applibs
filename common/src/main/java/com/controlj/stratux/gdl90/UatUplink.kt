package com.controlj.stratux.gdl90

import java.net.InetAddress

class UatUplink(address: InetAddress? = null) : GDL90(ID, address) {

    override val messageLength: Int = 436
    override fun fromBytes(bytes: ByteArray): GDL90 {
        check(bytes)
        return this
    }

    override val databytes: ByteArray
        get() = TODO("Not yet implemented")

    companion object {
        const val ID = 7
    }
}
