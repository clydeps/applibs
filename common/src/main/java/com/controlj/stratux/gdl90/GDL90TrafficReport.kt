package com.controlj.stratux.gdl90

import com.controlj.traffic.TrafficTarget
import com.controlj.utility.nibble
import com.controlj.utility.nibblesToByte
import java.net.InetAddress

@ExperimentalUnsignedTypes
class GDL90TrafficReport(address: InetAddress? = null) : PositionReport(ID, address), TrafficTarget {

    override var isHazard: Boolean = false

    override fun fromBytes(bytes: ByteArray): GDL90TrafficReport {
        super.fromBytes(bytes)
        isHazard = bytes[1].nibble(1).toInt() == 1
        return this
    }

    override val databytes: ByteArray
        get() = super.databytes.also {
            it[1] = nibblesToByte(address.type.ordinal, if (isHazard) 1 else 0)
        }

    companion object {
        const val ID = 20

        fun TrafficTarget.asGDL90Report(): GDL90TrafficReport {
            val result = GDL90TrafficReport(null).also {
                it.address = address
                it.verticalSpeed = verticalSpeed
                it.emitterCategory = emitterCategory
                it.callSign = callSign
                it.isHeading = false                     // if false, "track" is heading
            }
            result.apply(this)
            return result
        }
    }
}