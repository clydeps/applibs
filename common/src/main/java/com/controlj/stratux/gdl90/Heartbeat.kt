package com.controlj.stratux.gdl90

import com.controlj.utility.asUInt
import com.controlj.utility.bit
import com.controlj.utility.bitfield
import com.controlj.utility.bitsToByte
import com.controlj.utility.byte
import java.net.InetAddress

class Heartbeat(address: InetAddress? = null) : GDL90(ID, address) {

    companion object {
        const val ID = 0x00
        const val LENGTH = 7
    }

    override val messageLength: Int = LENGTH

    var gpsPosValid: Boolean = false        // GPS position is valid
    var maintReqd: Boolean = false          // device requires maintenance
    var ident: Boolean = false              // IDENT talkback?
    var addrType: Boolean = false
    var gpsBattLow: Boolean = false         // battery is low
    var rAtcS: Boolean = false
    var uatInitialized: Boolean = false
    var csaRequested: Boolean = false
    var csaNotAvailable: Boolean = false
    var utcOk: Boolean = false              // UTC time is ok
    var timeStamp: Int = 0                  // time in seconds since 00:00Z
    var uplinkRecvd: Int = 0                // count of uplink messages received
    var totalRecvd: Int = 0                 // count of total messages received

    override fun fromBytes(bytes: ByteArray): Heartbeat {
        check(bytes)
        gpsPosValid = bytes[1].bit(7)
        maintReqd = bytes[1].bit(6)
        ident = bytes[1].bit(5)
        addrType = bytes[1].bit(4)
        gpsBattLow = bytes[1].bit(3)
        rAtcS = bytes[1].bit(2)
        uatInitialized = bytes[1].bit(0)

        csaRequested = bytes[2].bit(6)
        csaNotAvailable = bytes[2].bit(5)
        utcOk = bytes[2].bit(0)
        timeStamp = bytes[3].asUInt() + (bytes[4].asUInt() shl 8) + if (bytes[2].bit(7)) 0x10000 else 0
        uplinkRecvd = bytes[5].bitfield(3, 5)
        totalRecvd = bytes[6].asUInt() + (bytes[5].bitfield(0, 2) shl 8)
        return this
    }


    @ExperimentalUnsignedTypes
    override val databytes: ByteArray
        get() {
            return byteArrayOf(
                ID.toByte(),
                bitsToByte(uatInitialized, false, rAtcS, gpsBattLow, addrType, ident, maintReqd, gpsPosValid),
                bitsToByte(utcOk, false, false, false, false, csaNotAvailable, csaRequested, timeStamp.bit(16)),
                timeStamp.byte(0),
                timeStamp.byte(1),
                ((uplinkRecvd shl 3) + ((totalRecvd ushr 8) and 3)).toByte(),
                totalRecvd.byte(0)
            )
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Heartbeat

        if (gpsPosValid != other.gpsPosValid) return false
        if (maintReqd != other.maintReqd) return false
        if (ident != other.ident) return false
        if (addrType != other.addrType) return false
        if (gpsBattLow != other.gpsBattLow) return false
        if (rAtcS != other.rAtcS) return false
        if (uatInitialized != other.uatInitialized) return false
        if (csaRequested != other.csaRequested) return false
        if (csaNotAvailable != other.csaNotAvailable) return false
        if (utcOk != other.utcOk) return false
        if (timeStamp != other.timeStamp) return false
        if (uplinkRecvd != other.uplinkRecvd) return false
        if (totalRecvd != other.totalRecvd) return false

        return true
    }

    override fun hashCode(): Int {
        var result = gpsPosValid.hashCode()
        result = 31 * result + utcOk.hashCode()
        result = 31 * result + timeStamp
        result = 31 * result + uplinkRecvd
        result = 31 * result + totalRecvd
        return result
    }

    override fun toString(): String {
        return "Heartbeat(gpsPosValid=$gpsPosValid, uatInitialized=$uatInitialized, utcOk=$utcOk, timeStamp=$timeStamp, uplinkRecvd=$uplinkRecvd, totalRecvd=$totalRecvd)"
    }
}
