package com.controlj.stratux.gdl90

import com.controlj.utility.bit
import com.controlj.utility.bitfield
import java.net.InetAddress

class StratuxHeartbeat(address: InetAddress? = null) : GDL90(ID, address) {

    override val messageLength: Int = 2
    var gpsEnabled: Boolean = false
    var ahrsEnabled: Boolean = false
    var protocolVersion: Int = 0

    override val databytes: ByteArray
        get() {
            var byte1 = protocolVersion shl 2
            if (gpsEnabled)
                byte1 = byte1 or 2
            if (ahrsEnabled)
                byte1 = byte1 or 1
            return byteArrayOf(
                ID.toByte(),
                byte1.toByte()
            )
        }

    @ExperimentalUnsignedTypes
    override fun fromBytes(bytes: ByteArray): GDL90 {
        check(bytes)
        //Byte1: p p p p p p GPS AHRS
        protocolVersion = bytes[1].bitfield(2, 6)
        gpsEnabled = bytes[1].bit(1)
        ahrsEnabled = bytes[1].bit(0)
        return this
    }

    override fun toString(): String {
        return "StratuxHeartbeat(gpsEnabled=$gpsEnabled, ahrsEnabled=$ahrsEnabled, protocolVersion=$protocolVersion)"
    }

    companion object {
        const val ID = 204
    }

}
