package com.controlj.stratux.gdl90

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.Location
import com.controlj.data.Position
import com.controlj.traffic.TrafficTarget
import com.controlj.utility.Units
import com.controlj.utility.bit
import com.controlj.utility.bitsToByte
import com.controlj.utility.byte
import com.controlj.utility.instantNow
import com.controlj.utility.nibble
import com.controlj.utility.nibblesToByte
import com.controlj.utility.signExtend
import org.threeten.bp.Instant
import java.net.InetAddress
import kotlin.math.abs
import kotlin.math.roundToInt

@ExperimentalUnsignedTypes
open class PositionReport(id: Int, address: InetAddress? = null) : GDL90(id, address), Location {

    override val messageLength: Int = 28

    override var latitude: Double = INVALID_DATA    // latitude in degrees
    override var longitude: Double = INVALID_DATA   // longitude in degrees
    override var altitude: Double = INVALID_DATA             // altitude in meters
    override var speed: Double = INVALID_DATA                // speed in m/s
    override var track: Double = INVALID_DATA                // track or heading in degrees true
    override var valid: Boolean = false
    override var timestamp: Instant = instantNow
    override var creator: Location.Creator = GDL90.creator
    override var isInFlight: Boolean = false

    override val accuracyH: Double
        get() = accuracyTable.getOrNull(nacP) ?: INVALID_DATA
    override val accuracyV: Double
        get() = accuracyH * 1.5

    var address = TrafficTarget.EmitterAddress(TrafficTarget.AddressType.None, -1)
    var verticalSpeed: Double = INVALID_DATA        // vertical speed in m/s
    var emitterCategory = TrafficTarget.EmitterCategory.None
    var callSign: String = ""
    var priorityCode: Int = 0
    var isHeading: Boolean = true                     // if false, "track" is heading
    var isExtrapolated: Boolean = false
    var nic: Int = 0                            // navigation integrity category
    var nacP: Int = 0                           // navigation accuracy


    fun apply(position: Position) {
        latitude = position.latitude
        longitude = position.longitude
        valid = position.valid
        if (position is Location) {
            altitude = position.altitude
            speed = position.speed
            track = position.track
            nacP = accuracyToNacP(position.accuracyH)
            nic = nacP
        }
    }

    override fun fromBytes(bytes: ByteArray): PositionReport {
        check(bytes)
        address = TrafficTarget.EmitterAddress(
            TrafficTarget.AddressType.values().getOrNull(bytes[1].nibble(0).toInt()) ?: TrafficTarget.AddressType.None,
            bytes.get24Bit(2)
        )
        latitude = bytes.get24BitSigned(5) * 360.0 / 0x1000000
        longitude = bytes.get24BitSigned(8) * 360.0 / 0x1000000
        altitude = Units.Unit.FOOT.convertFrom(
            ((bytes[11].toUByte().toInt() shl 4) + bytes[12].nibble(1)).toDouble() * 25.0 - 1000.0
        )
        val m = bytes[12].nibble(0)
        isInFlight = m.bit(3)
        isExtrapolated = m.bit(2)
        isHeading = m.bit(1)
        val ia = bytes[13]
        nic = ia.nibble(1).toInt()
        nacP = ia.nibble(0).toInt()
        speed = Units.Unit.KNOT.convertFrom(((bytes[14].toUByte().toInt() shl 4) + bytes[15].nibble(1)).toDouble())
        val vs = (bytes[15].nibble(0).toInt() shl 8) + bytes[16].toUByte().toInt()
        verticalSpeed = when {
            vs in (0x1FF..0xE01) -> INVALID_DATA
            else -> Units.Unit.FPM.convertFrom(vs.signExtend(11) * 64.0)
        }
        track = bytes[17].toUByte().toDouble() * 360.0 / 256.0
        emitterCategory = TrafficTarget.EmitterCategory.values().getOrNull(bytes[18].nibble(0).toInt())
            ?: TrafficTarget.EmitterCategory.None
        callSign = String(bytes.copyOfRange(19, 27).filter { it in validCallChars }.toByteArray())
        priorityCode = bytes[27].nibble(1).toInt()
        valid = super.valid
        return this
    }

    override fun toString(): String {
        val vs = if (verticalSpeed == INVALID_DATA) "---" else
            Units.Unit.FPM.convertTo(verticalSpeed).roundToInt().toString()

        return Location.toString(this) +
            ", (vs=${vs} " +
            "addr=${address} " +
            "callsign='${callSign.trim()}' " +
            "category=${emitterCategory.name} " +
            "inFlight=$isInFlight)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PositionReport

        return address == other.address
    }

    override fun hashCode(): Int {
        return address.hashCode()
    }

    override val databytes: ByteArray
        get() {
            val latInt = (latitude / 360.0 * 0x1000000).toInt()
            val lonInt = (longitude / 360.0 * 0x1000000).toInt()
            val altInt: Int = ((Units.Unit.FOOT.convertTo(altitude) + 1000.0) / 25).roundToInt()
            val m = bitsToByte(track != INVALID_DATA, isHeading, isExtrapolated, isInFlight).toInt()
            val speedInt = Units.Unit.KNOT.convertTo(speed).roundToInt()
            val vsInt = (Units.Unit.FPM.convertTo(verticalSpeed) / 64).toInt()
            return byteArrayOf(
                type.toByte(),
                nibblesToByte(address.type.ordinal, 0),
                address.address.byte(2), address.address.byte(1), address.address.byte(0),
                latInt.byte(2), latInt.byte(1), latInt.byte(0),
                lonInt.byte(2), lonInt.byte(1), lonInt.byte(0),
                (altInt shr 4).toByte(), nibblesToByte(m, altInt and 0xF),
                nibblesToByte(nacP, nic),
                (speedInt shr 4).toByte(),
                nibblesToByte(vsInt.nibble(2), speedInt.nibble(0)),
                vsInt.toByte(),
                (track / 360 * 256).toInt().toByte(),
                emitterCategory.ordinal.toByte(),
                *callSign.take(8).padEnd(8).toByteArray(),
                nibblesToByte(priorityCode, 0)
            )
        }


    companion object {
        val validCallChars = (('A'..'Z') + ('0'..'9') + '-').map { it.code.toByte() }.toSet()
        private val accuracyTable = listOf(
            INVALID_DATA,   // 0
            *listOf(
                10.0, 4.0, 2.0, 1.0, 0.5, 0.3, 0.1, 0.05
            ).map { Units.Unit.NM.convertFrom(it) }.toTypedArray(),
            30.0, 10.0, 3.0
        )

        fun accuracyToNacP(accuracy: Double): Int {
            val x = accuracyTable.minOf { abs(accuracy - it) }
            return accuracyTable.indexOf(x)
        }

        val MAX_VS = Units.Unit.FPM.convertFrom(32576.0)
    }
}
