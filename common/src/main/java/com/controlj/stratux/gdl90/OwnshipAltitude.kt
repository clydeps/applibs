package com.controlj.stratux.gdl90

import com.controlj.utility.asUInt
import com.controlj.utility.bit
import com.controlj.utility.bitfield
import com.controlj.utility.byte
import java.net.InetAddress
import kotlin.experimental.or

class OwnshipAltitude(address: InetAddress? = null) : GDL90(ID, address) {

    companion object {
        const val ID = 11
        const val NOVFOM = 0x7FFF
        const val HIVFOM = 0x7FFE
    }

    override val messageLength: Int = 5
    var geoAltitude: Int = 0
    var warning: Boolean = false
    var vFom: Int = NOVFOM

    override fun fromBytes(bytes: ByteArray) : OwnshipAltitude {
        check(bytes)
        geoAltitude = (bytes[2].asUInt() + (bytes[1].asUInt() shl 8)).toShort().toInt() * 5
        warning = bytes[3].bit(7)
        vFom = (bytes[3].bitfield(0, 7) shl 8) + bytes[4].asUInt()
        return this
    }

    @ExperimentalUnsignedTypes
    override val databytes: ByteArray
        get() {
            val ft5 = geoAltitude / 5
            return byteArrayOf(
                    ft5.byte(0),
                    ft5.byte(1),
                    vFom.byte(1) or (if (warning) 0x80 else 0).toByte(),
                    vFom.byte(0)
            )
        }

    override fun toString(): String {
        return "OwnshipAltitude(geoAltitude=$geoAltitude, warning=$warning, vFom=$vFom)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OwnshipAltitude

        if (geoAltitude != other.geoAltitude) return false
        if (warning != other.warning) return false
        if (vFom != other.vFom) return false

        return true
    }

    override fun hashCode(): Int {
        return geoAltitude
    }
}
