package com.controlj.stratux.gdl90

import com.controlj.data.Location
import com.controlj.data.Location.Companion.GDL_PRIORITY
import com.controlj.stratux.Report
import com.controlj.utility.asUInt
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import org.threeten.bp.temporal.ChronoField
import java.net.InetAddress

abstract class GDL90(
    val type: Int,
    val sourceAddress: InetAddress? = null// the source IP address, if known.
) : Report {

    abstract val databytes: ByteArray

    abstract fun fromBytes(bytes: ByteArray): GDL90

    abstract val messageLength: Int      // number of bytes in the message

    open fun check(bytes: ByteArray) {
        require(bytes.size == messageLength) { "Required $messageLength bytes, but got ${bytes.size}"}
        require(bytes[0].asUInt() == type) { "Expected type $type, got ${bytes[0]}"}
    }

    class Unknown(bytes: ByteArray, address: InetAddress? = null) : GDL90(bytes[0].asUInt(), address) {
        override var databytes: ByteArray = bytes
        override val messageLength: Int = databytes.size

        override fun fromBytes(bytes: ByteArray): GDL90 {
            databytes = bytes
            return this
        }


        override fun toString(): String {
            return "GDL90.Unknown($type, size=$messageLength)"
        }
    }

    companion object {

        var creator = Location.Creator("GDL-90", GDL_PRIORITY)
            private set
        var gpsValid: Boolean = false       // GPS data is valid
            private set
        var utcSeconds: Int = -1             // seconds since 00:00Z
            private set
        var utcValid: Boolean = false       // utc time is valid
            private set
            get() = field && utcSeconds in (0 until 86400)

        var lastPosition: OwnshipReport? = null

        private fun process(ownshipReport: OwnshipReport): OwnshipReport {
            ownshipReport.valid = gpsValid
            if (gpsValid && utcValid)
                ownshipReport.timestamp =
                    LocalDateTime.now().with(ChronoField.SECOND_OF_DAY, utcSeconds.toLong())
                        .toInstant(ZoneOffset.UTC)
            if (gpsValid)
                lastPosition = ownshipReport
            return ownshipReport
        }

        private fun process(report: Heartbeat): Heartbeat {
            gpsValid = report.gpsPosValid
            utcSeconds = report.timeStamp
            utcValid = report.utcOk
            return report
        }

        private fun process(report: ForeFlight): ForeFlight {
            when (report) {
                is ForeFlight.IDMsg -> {
                    creator = Location.Creator(report.deviceName.trim(), 50)
                }
            }
            return report
        }

        /**
         * Convert a byte array into a GDL90 object
         */

        @ExperimentalUnsignedTypes
        fun toGdl90(bytes: ByteArray, address: InetAddress?): GDL90 {
            require(bytes.isNotEmpty())
            return when (bytes[0].asUInt()) {
                Heartbeat.ID -> process(Heartbeat(address).fromBytes(bytes))
                OwnshipAltitude.ID -> OwnshipAltitude(address).fromBytes(bytes)
                GDL90TrafficReport.ID -> GDL90TrafficReport(address).fromBytes(bytes)
                OwnshipReport.ID -> process(OwnshipReport(address).fromBytes(bytes))
                StratuxStatus.ID -> StratuxStatus(address).fromBytes(bytes)
                StratuxHeartbeat.ID -> StratuxHeartbeat(address).fromBytes(bytes)
                UatUplink.ID -> UatUplink(address).fromBytes(bytes)
                ForeFlight.ID -> process(ForeFlight.fromBytes(bytes, address))
                else -> Unknown(bytes)
            }
        }
    }
}

@ExperimentalUnsignedTypes
fun ByteArray.get24Bit(start: Int): Int {
    return ((get(start).asUInt() shl 16) +
        (get(start + 1).asUInt() shl 8) +
        get(start + 2).asUInt())
}

@ExperimentalUnsignedTypes
fun ByteArray.get24BitSigned(start: Int): Int {
    return get24Bit(start).shl(8).shr(8)
}

fun ByteArray.get16Bit(start: Int): Int {
    return ((get(start).asUInt() shl 8) +
        get(start + 1).asUInt())
}

fun ByteArray.get16BitSigned(start: Int): Int {
    return get16Bit(start).shl(16).shr(16)
}

/**
 * Get a 64 bit number from the byte array. Assumed to be big-endian
 * @param start - the offset in bytes to start at
 */
fun ByteArray.getULong(start: Int): ULong {
    return asUByteArray().drop(start).take(8).fold(0UL) { acc, byte ->
        (acc shl 8) + byte.toULong()
    }
}
