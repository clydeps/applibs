package com.controlj.stratux.gdl90

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.nmea.GpsFix
import com.controlj.utility.bit
import com.controlj.utility.byte
import com.controlj.utility.bytesToInt
import com.controlj.utility.nibble
import com.controlj.utility.setBit
import com.controlj.utility.signExtend
import java.net.InetAddress
import kotlin.math.roundToInt

/**
 * represents a Status message from Stratux.
 */
class StratuxStatus(address: InetAddress?) : GDL90(ID, address) {

    override val messageLength: Int = 29
    var gpsFix: GpsFix = GpsFix.None
    var ahrsValid: Boolean = false
    var pressureValid: Boolean = false
    var cpuTempValid: Boolean = false
    var enable978: Boolean = false
    var enable1090: Boolean = false
    var enableGps: Boolean = false
    var enableAhrs: Boolean = false
    var radioCount: Int = 0
    var rY835AI: Boolean = false
    var satellitesLocked: Int = 0
    var satellitesTracked: Int = 0
    var num978Targets: Int = 0
    var num1090Targets: Int = 0
    var rate978Targets: Int = 0
    var rate1090Targets: Int = 0
    var cpuTemp: Double = INVALID_DATA
    var numTowers: Int = 0
    var version = VERSION
    var revCode = 0

    private val byte13Bits = listOf(
        { gpsFix.ordinal.bit(0) },
        { gpsFix.ordinal.bit(1) },
        { ahrsValid },
        { pressureValid },
        { cpuTempValid },
        { enable978 },
        { enable1090 },
        { enableGps },
    )

    override val databytes: ByteArray
        get() {
            val cpuTempInt = (cpuTemp * 10.0).roundToInt()
            return byteArrayOf(
                ID.toByte(),
                'X'.code.toByte(),
                1.toByte(),
                VERSION.toByte(),
                version.toByte(),
                (version shr 8).toByte(),
                (version shr 16).toByte(),
                (version shr 24).toByte(),
                revCode.toByte(),
                (revCode shr 8).toByte(),
                (revCode shr 16).toByte(),
                (revCode shr 24).toByte(),
                if (enableAhrs) 1 else 0,
                byte13Bits.mapIndexed { n, c -> Pair(1 shl n, c) }.filter { it.second() }.sumOf { it.first }.toByte(),
                0, // byte 14
                ((radioCount and 3) + if (rY835AI) 4 else 0).toByte(),
                satellitesLocked.toByte(),
                satellitesTracked.toByte(),
                (num978Targets shr 8).toByte(),
                num978Targets.toByte(),
                (num1090Targets shr 8).toByte(),
                num1090Targets.toByte(),
                (rate978Targets shr 8).toByte(),
                rate978Targets.toByte(),
                (rate1090Targets shr 8).toByte(),
                rate1090Targets.toByte(),
                (cpuTempInt shr 8).toByte(),
                cpuTempInt.toByte(),
                numTowers.toByte(),
            )
        }

    override fun fromBytes(bytes: ByteArray): StratuxStatus {
        require(bytes.size >= messageLength)
        require(bytes[0].toInt() == ID)
        require(bytes[1].toInt() == 'X'.code)
        require(bytes[2] == 1.toByte())
        require(bytes[3] <= VERSION)
        version = bytesToInt(bytes[4], bytes[5], bytes[6], bytes[7])
        revCode = bytesToInt(bytes[8], bytes[9], bytes[10], bytes[11])
        val flags = bytes[13]
        gpsFix = GpsFix.values()[flags.nibble(0, 2).toInt()]
        ahrsValid = flags.bit(2)
        pressureValid = flags.bit(3)
        cpuTempValid = flags.bit(4)
        enable978 = flags.bit(5)
        enable1090 = flags.bit(6)
        enableGps = flags.bit(7)
        enableAhrs = bytes[12].bit(0)
        cpuTemp = if (cpuTempValid)
            bytesToInt(bytes[27], bytes[26]).signExtend(15) / 10.0
        else
            INVALID_DATA
        radioCount = bytes[15].nibble(0, 2).toInt()
        rY835AI = bytes[15].bit(2)
        satellitesLocked = bytes[16].toUByte().toInt()
        satellitesTracked = bytes[17].toUByte().toInt()
        num978Targets = bytesToInt(bytes[19], bytes[18])
        num1090Targets = bytesToInt(bytes[21], bytes[20])
        rate978Targets = bytesToInt(bytes[23], bytes[22])
        rate1090Targets = bytesToInt(bytes[25], bytes[24])
        cpuTemp = bytesToInt(bytes[27], bytes[26]).toFloat() / 10.0
        numTowers = bytes[28].toUByte().toInt()
        return this
    }


    override fun toString(): String {
        return "StratuxStatus(gpsFix=$gpsFix, ahrsValid=$ahrsValid, pressureValid=$pressureValid, enable978=$enable978, enable1090=$enable1090, enableGps=$enableGps, enableAhrs=$enableAhrs, radioCount=$radioCount, rY835AI=$rY835AI, satellitesLocked=$satellitesLocked, satellitesConnected=$satellitesTracked, num978Targets=$num978Targets, num1090Targets=$num1090Targets, rate978Targets=$rate978Targets, rate1090Targets=$rate1090Targets, cpuTemp=$cpuTemp, numTowers=$numTowers, version=$version, revCode=$revCode)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is StratuxStatus) return false

        if (gpsFix != other.gpsFix) return false
        if (ahrsValid != other.ahrsValid) return false
        if (pressureValid != other.pressureValid) return false
        if (cpuTempValid != other.cpuTempValid) return false
        if (enable978 != other.enable978) return false
        if (enable1090 != other.enable1090) return false
        if (enableGps != other.enableGps) return false
        if (enableAhrs != other.enableAhrs) return false
        if (radioCount != other.radioCount) return false
        if (rY835AI != other.rY835AI) return false
        if (satellitesLocked != other.satellitesLocked) return false
        if (satellitesTracked != other.satellitesTracked) return false
        if (num978Targets != other.num978Targets) return false
        if (num1090Targets != other.num1090Targets) return false
        if (rate978Targets != other.rate978Targets) return false
        if (rate1090Targets != other.rate1090Targets) return false
        if (cpuTemp != other.cpuTemp) return false
        if (numTowers != other.numTowers) return false
        if (version != other.version) return false
        if (revCode != other.revCode) return false

        return true
    }

    override fun hashCode(): Int {
        var result = radioCount
        result = 31 * result + satellitesLocked
        result = 31 * result + satellitesTracked
        result = 31 * result + num978Targets
        result = 31 * result + num1090Targets
        result = 31 * result + cpuTemp.hashCode()
        result = 31 * result + version
        result = 31 * result + revCode
        return result
    }

    val decodedVersion: String
        get() {
            val dot = when (version.byte(2)) {
                0.toByte() -> "."
                1.toByte() -> "b"
                2.toByte() -> "r"
                3.toByte() -> "rc"
                else -> error("Invalid version format")
            }
            return "${version.byte(0)}.${version.byte(1)}$dot${version.byte(3)}"
        }

    companion object {
        const val ID = 83
        const val VERSION = 1
    }
}
