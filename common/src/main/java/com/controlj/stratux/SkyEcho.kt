package com.controlj.stratux

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener

/**
 * A class to get data from Skyecho over a websocket
 * @param path The host and path string. Should not include ws://
 */
class SkyEcho(val path: String) {

    /**
     * Observe status reports from the SkyEcho.
     */
    val observable: Observable<SkyEchoStatus> =
        Observable.create { emitter: ObservableEmitter<SkyEchoStatus> ->
            OkHttpClient().newWebSocket(
                Request.Builder().url("ws://$path").build(),
                object: WebSocketListener() {
                    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                        emitter.onComplete()
                    }

                    override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
                        emitter.onError(t)
                    }

                    override fun onMessage(webSocket: WebSocket, text: String) {
                        emitter.onNext(SkyEchoStatus(text))
                    }
                })
        }.share()
}

