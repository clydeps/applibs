package com.controlj.stratux

import com.controlj.nmea.GpsFix
import com.controlj.nmea.GpsStatus
import com.controlj.utility.fromJson
import com.controlj.utility.instantNow
import org.threeten.bp.Instant

class SkyEchoStatus(json: String) : Report, GpsStatus {

    override val gpsFix: GpsFix
    override val baroAltitude: Double
    override val geoAltitude: Double
    override val satellitesUsed: Int
    override val satellitesInView: Int
        get() = satellitesUsed
    val NACp: Int
    val NIC: Int
    val timestamp: Instant = instantNow
    val latitude: Double
    val longitude: Double
    val callsign: String
    val icaoAddress: Int
    val clientCount: Int

    private data class SkyEcho(
        val clientCount: Int,
        val status: Status
    )

    data class Status(
        val icaoAddress: Int,
        val callsign: String,
        val latitude: Int,       // degrees * 1000
        val longitude: Int,       // degrees * 1000
        val baroAltitude: Int,   // millimeters
        val gnssAltitude: Int,
        val gpsFix: Int,
        val gpsSatellites: Int,
        val NACp: Int,
        val NIC: Int
    )

    init {

        /**
         * Create a status message from a SkyEcho websocket message
         */

        // {"clientCount":3,"status":{"icaoAddress":8158997,"callsign":"VHZDZ","latitude":-314209792,"longitude":1527573248,"baroAltitude":9521,"gnssAltitude":36750,"gpsFix":3,"gpsSatellites":6,"NACp":8,"NIC":6}}
        val map: SkyEcho = json.fromJson()
        gpsFix = when (map.status.gpsFix) {
            2 -> GpsFix.TwoD
            3 -> GpsFix.ThreeD
            4 -> GpsFix.DGPS
            5 -> GpsFix.RTK
            else -> GpsFix.None
        }
        clientCount = map.clientCount
        callsign = map.status.callsign
        icaoAddress = map.status.icaoAddress
        latitude = map.status.latitude / 10000000.0
        longitude = map.status.longitude / 10000000.0
        baroAltitude = map.status.baroAltitude / 1000.0
        geoAltitude = map.status.gnssAltitude / 1000.0
        NACp = map.status.NACp
        NIC = map.status.NIC
        satellitesUsed = map.status.gpsSatellites

    }

    override fun toString(): String {
        return "SkyEchoStatus(gpsFix=$gpsFix, baroAltitude=$baroAltitude, altitude=$geoAltitude, satellites=$satellitesUsed, NACp=$NACp, NIC=$NIC, timestamp=$timestamp, latitude=$latitude, longitude=$longitude, callsign='$callsign', icaoAddress=$icaoAddress, clientCount=$clientCount)"
    }
}
