package com.controlj.rx

import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Cancellable


/**
 * Created by clyde on 28/3/18.
 */
class DisposedEmitter<T:Any>: ObservableEmitter<T> {
    override fun isDisposed(): Boolean {
        return true
    }

    override fun tryOnError(t: Throwable): Boolean {
        return false
    }

    override fun onComplete() {
    }

    override fun setCancellable(c: Cancellable?) {
    }

    override fun setDisposable(d: Disposable?) {
    }

    override fun serialize(): ObservableEmitter<T> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onNext(value: T) {
    }

    override fun onError(error: Throwable) {
    }
}
