package com.controlj.rx

/**
 * Created by clyde on 30/6/18.
 */
/*
 * Copyright 2016 Jake Wharton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.FlowableTransformer
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableTransformer
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Consumer
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

/**
 * A transformer which combines the `replay(1)`, `publish()`, and `refCount()`
 * operators.
 *
 *
 * Unlike traditional combinations of these operators, `ReplayingShare` caches the last emitted
 * value from the upstream observable or flowable *only* when one or more downstream subscribers
 * are connected. This allows expensive upstream sources to be shut down when no one is listening
 * while also replaying the last value seen by *any* subscriber to new ones.
 */
class ReplayingShare<T : Any> private constructor() : ObservableTransformer<T, T>, FlowableTransformer<T, T> {

    override fun apply(upstream: Observable<T>): Observable<T> {
        val lastSeen = LastSeen<T>()
        return LastSeenObservable(upstream.doOnNext(lastSeen).share(), lastSeen)
    }

    override fun apply(upstream: Flowable<T>): Flowable<T> {
        val lastSeen = LastSeen<T>()
        return LastSeenFlowable(upstream.doOnNext(lastSeen).share(), lastSeen)
    }

    internal class LastSeen<T : Any> : Consumer<T> {
        @Volatile
        var value: T? = null

        override fun accept(latest: T) {
            value = latest
        }
    }

    internal class LastSeenObservable<T : Any>(private val upstream: Observable<T>, private val lastSeen: LastSeen<T>) :
        Observable<T>() {

        override fun subscribeActual(observer: Observer<in T>) {
            upstream.subscribe(LastSeenObserver(observer, lastSeen))
        }
    }

    internal class LastSeenObserver<T : Any>(
        private val downstream: Observer<in T>,
        private val lastSeen: LastSeen<T>
    ) : Observer<T> {

        override fun onSubscribe(d: Disposable) {
            downstream.onSubscribe(d)

            val value = lastSeen.value
            if (value != null) {
                downstream.onNext(value)
            }
        }

        override fun onNext(value: T) {
            downstream.onNext(value)
        }

        override fun onComplete() {
            downstream.onComplete()
        }

        override fun onError(e: Throwable) {
            downstream.onError(e)
        }
    }

    internal class LastSeenFlowable<T : Any>(private val upstream: Flowable<T>, private val lastSeen: LastSeen<T>) :
        Flowable<T>() {

        override fun subscribeActual(subscriber: Subscriber<in T>) {
            upstream.subscribe(LastSeenSubscriber(subscriber, lastSeen))
        }
    }

    @Suppress("ReactiveStreamsSubscriberImplementation")
    internal class LastSeenSubscriber<T : Any>(
        private val downstream: Subscriber<in T>,
        private val lastSeen: LastSeen<T>
    ) : Subscriber<T>, Subscription {

        private var subscription: Subscription? = null
        private var first = true

        override fun onSubscribe(subscription: Subscription) {
            this.subscription = subscription
            downstream.onSubscribe(this)
        }

        override fun request(amount: Long) {
            if (amount == 0L)
                return

            var count = amount
            if (first) {
                first = false

                val value = lastSeen.value
                if (value != null) {
                    downstream.onNext(value)

                    if (count != java.lang.Long.MAX_VALUE && --count == 0L) {
                        return
                    }
                }
            }
            subscription!!.request(count)
        }

        override fun cancel() {
            subscription!!.cancel()
        }

        override fun onNext(value: T) {
            downstream.onNext(value)
        }

        override fun onComplete() {
            downstream.onComplete()
        }

        override fun onError(t: Throwable) {
            downstream.onError(t)
        }
    }

    companion object {
        private val INSTANCE = ReplayingShare<Any>()

        /**
         * The singleton instance of this transformer.
         */
        // Safe because of erasure.
        @Suppress("UNCHECKED_CAST")
        fun <T : Any> instance(): ReplayingShare<T> {
            return INSTANCE as ReplayingShare<T>
        }
    }
}

fun <T : Any> Observable<T>.replayingShare(): Observable<T> = compose(ReplayingShare.instance<T>())
fun <T : Any> Flowable<T>.replayingShare(): Flowable<T> = compose(ReplayingShare.instance<T>())


