package com.controlj.rx

import io.reactivex.rxjava3.core.MaybeEmitter
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Cancellable


/**
 * Created by clyde on 28/3/18.
 */
class DisposedMaybeEmitter<T:Any>: MaybeEmitter<T> {

    override fun isDisposed(): Boolean {
        return true
    }

    override fun tryOnError(t: Throwable): Boolean {
        return false
    }

    override fun onComplete() {
    }

    override fun setCancellable(c: Cancellable?) {
    }

    override fun setDisposable(d: Disposable?) {
    }


    override fun onSuccess(value: T) {
    }

    override fun onError(error: Throwable) {
    }
}
