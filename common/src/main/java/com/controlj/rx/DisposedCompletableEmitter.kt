package com.controlj.rx

import io.reactivex.rxjava3.core.CompletableEmitter
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Cancellable


/**
 * Created by clyde on 15/4/18.
 */
object DisposedCompletableEmitter: CompletableEmitter {
    override fun onComplete() {
    }

    override fun isDisposed(): Boolean {
        return true
    }

    override fun tryOnError(t: Throwable): Boolean {
        return false
    }

    override fun setCancellable(c: Cancellable?) {
    }

    override fun setDisposable(d: Disposable?) {
    }

    override fun onError(error: Throwable) {
    }
}
