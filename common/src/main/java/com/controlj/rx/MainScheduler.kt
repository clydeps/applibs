package com.controlj.rx

import com.controlj.logging.CJLog
import io.reactivex.rxjava3.annotations.CheckReturnValue
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable

/**
 * Created by clyde on 2/9/18.
 */
object MainScheduler {
    lateinit var instance: Scheduler

    operator fun invoke(runnable: () -> Unit) {
        instance.scheduleDirect(runnable)
    }

    operator fun invoke(): Scheduler {
        return instance
    }
}

private val onErrorStub: (Throwable) -> Unit = CJLog::logException
private val onCompleteStub: () -> Unit = {}

/**
 * Utility extension functions for doing stuff on various threads.
 */

/**
 * Observe a [Single] with the given onError and onSuccess functions. No scheduler is used by default
 * @receiver A Single
 * @param onError The onError function or lambda called on error with a [Throwable]
 * @param onSuccess Called on success with the emitted item
 */

@CheckReturnValue
fun <T : Any> Single<T>.observeBy(
        onError: (Throwable) -> Unit = onErrorStub,
        onSuccess: (T) -> Unit
): Disposable {
    return subscribe(onSuccess, onError)
}

/**
 * Observe a [Single] with the given onError and onSuccess functions on the main thread
 * @receiver A Single
 * @param onError The onError function or lambda called on error with a [Throwable]
 * @param onSuccess Called on success with the emitted item
 */

@CheckReturnValue
fun <T : Any> Single<T>.observeOnMainBy(
        onError: (Throwable) -> Unit = onErrorStub,
        onSuccess: (T) -> Unit
): Disposable {
    return observeOn(MainScheduler()).subscribe(onSuccess, onError)
}

/**
 * Observe an [Observable] with the given functions. No scheduler is used by default
 * @receiver An Observable
 * @param onError The onError function or lambda called on error with a [Throwable]
 * @param onComplete Called if the observable completes
 * @param onNext Called for each emitted item
 */


@CheckReturnValue
fun <T : Any> Observable<T>.observeBy(
        onError: (Throwable) -> Unit = onErrorStub,
        onComplete: () -> Unit = onCompleteStub,
        onNext: (T) -> Unit
): Disposable {
    return subscribe(onNext, onError, onComplete)
}

/**
 * Observe an [Observable] with the given onError onNext and onComplete functions on the main thread
 * @receiver An Observable
 * @param onError The onError function or lambda called on error with a [Throwable]
 * @param onComplete Called if the observable completes
 * @param onNext Called for each emitted item
 */

@CheckReturnValue
fun <T : Any> Observable<T>.observeOnMainBy(
        onError: (Throwable) -> Unit = onErrorStub,
        onComplete: () -> Unit = onCompleteStub,
        onNext: (T) -> Unit
): Disposable {
    return observeOn(MainScheduler()).subscribe(onNext, onError, onComplete)
}

/**
 * Observe a [Maybe] with the given functions with no specific scheduler
 * @receiver A Maybe
 * @param onError The onError function or lambda called on error with a [Throwable]
 * @param onComplete Called if the Maybe completes
 * @param onSuccess Called on success with the emitted item
 */

@CheckReturnValue
fun <T : Any> Maybe<T>.observeBy(
        onError: (Throwable) -> Unit = onErrorStub,
        onComplete: () -> Unit = onCompleteStub,
        onSuccess: (T) -> Unit
): Disposable {
    return subscribe(onSuccess, onError, onComplete)
}

/**
 * Observe a [Maybe] with the given functions on the main thread
 * @receiver A Maybe
 * @param onError The onError function or lambda called on error with a [Throwable]
 * @param onComplete Called if the Maybe completes
 * @param onSuccess Called on success with the emitted item
 */

@CheckReturnValue
fun <T : Any> Maybe<T>.observeOnMainBy(
        onError: (Throwable) -> Unit = onErrorStub,
        onComplete: () -> Unit = onCompleteStub,
        onSuccess: (T) -> Unit
): Disposable {
    return observeOn(MainScheduler()).subscribe(onSuccess, onError, onComplete)
}

/**
 * Typically used to update UI when an observable is subscribed and finished.
 * @receiver an Observable
 * @param onSubscribe A function called on the main thread when the Observable is subscribed to
 * @param onFinished A function called on the main thread when the Observable finishes, for any reason
 */
fun <T : Any> Observable<T>.updateUI(
        onSubscribe: () -> Unit,
        onFinished: () -> Unit
): Observable<T> {
    return initUI(onSubscribe).finaliseUI(onFinished)
}

/**
 * Typically used to update UI when an observable finished.
 * @receiver an Observable
 * @param onFinished A function called on the main thread when the Observable finishes, for any reason
 */
fun <T : Any> Observable<T>.finaliseUI(
        onFinished: () -> Unit
): Observable<T> {
    return doFinally { MainScheduler(onFinished) }
}

/**
 * Typically used to update UI when an observable is subscribed to
 * @receiver an Observable
 * @param onSubscribe A function called on the main thread when the Observable is subscribed to
 */
fun <T : Any> Observable<T>.initUI(
        onSubscribe: () -> Unit
): Observable<T> {
    return doOnSubscribe { MainScheduler(onSubscribe) }
}

/**
 * Typically used to update UI when a completable is subscribed to
 * @receiver an Completable
 * @param onSubscribe A function called on the main thread when the Completable is subscribed to
 */
fun Completable.initUI(
        onSubscribe: () -> Unit
): Completable {
    return doOnSubscribe { MainScheduler(onSubscribe) }
}

/**
 * Typically used to update UI when a completable is finished.
 * @receiver an Completable
 * @param onFinished A function called on the main thread when the Completable finishes, for any reason
 */
fun Completable.finaliseUI(
        onFinished: () -> Unit
): Completable {
    return doFinally { MainScheduler(onFinished) }
}

/**
 * Typically used to update UI when a completable is subscribed and finished.
 * @receiver an Completable
 * @param onSubscribe A function called on the main thread when the Completable is subscribed to
 * @param onFinished A function called on the main thread when the Completable finishes, for any reason
 */
fun Completable.updateUI(
        onSubscribe: () -> Unit,
        onFinished: () -> Unit
): Completable {
    return initUI(onSubscribe).finaliseUI(onFinished)
}

/**
 * Typically used to update UI when a Single is subscribed to and finished.
 * @receiver a Single
 * @param onSubscribe A function called on the main thread when the Single is subscribed to
 * @param onFinished A function called on the main thread when the Single finishes, for any reason
 */
fun <T : Any> Single<T>.updateUI(
        onSubscribe: () -> Unit,
        onFinished: () -> Unit
): Single<T> {
    return initUI(onSubscribe).finaliseUI(onFinished)
}

/**
 * Typically used to update UI when a Single is subscribed to
 * @receiver a Single
 * @param onSubscribe A function called on the main thread when the Single is subscribed to
 */
fun <T : Any> Single<T>.initUI(
        onSubscribe: () -> Unit
): Single<T> {
    return doOnSubscribe { MainScheduler(onSubscribe) }
}

/**
 * Typically used to update UI when a Single is finished.
 * @receiver a Single
 * @param onFinished A function called on the main thread when the Single finishes, for any reason
 */
fun <T : Any> Single<T>.finaliseUI(
        onFinished: () -> Unit
): Single<T> {
    return doFinally { MainScheduler(onFinished) }
}
