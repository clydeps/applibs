package com.controlj.rx

import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.Cancellable


/**
 * Created by clyde on 15/4/18.
 */
class DisposedSingleEmitter<T:Any>: SingleEmitter<T> {
    override fun isDisposed(): Boolean {
        return true
    }

    override fun tryOnError(t: Throwable): Boolean {
        return false
    }

    override fun setCancellable(c: Cancellable?) {
    }

    override fun setDisposable(d: Disposable?) {
    }

    override fun onSuccess(value: T) {
    }

    override fun onError(error: Throwable) {
    }

    override fun toString(): String {
        return "DisposedSingleEmitter"
    }
}
