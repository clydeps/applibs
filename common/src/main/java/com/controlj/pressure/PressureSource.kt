package com.controlj.pressure

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.logging.CJLog
import com.controlj.rx.DisposedEmitter
import com.controlj.utility.Units
import com.controlj.utility.from
import com.controlj.utility.instantNow
import com.controlj.utility.to
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import org.threeten.bp.Instant
import java.util.concurrent.ConcurrentHashMap
import kotlin.math.pow

object PressureSource {
    /**
     * The last pressure we have seen, in hPa
     */
    var lastPressure: Double = INVALID_DATA
        private set(value) {
            field = value
            if (value != INVALID_DATA)
                emitter.onNext(value)
        }

    val lastAltitude: Double
        get() = toAltitude(lastPressure)

    // the emitter for subscriptions
    internal var emitter: ObservableEmitter<Double> = DisposedEmitter()

    /**
     * An observer to deliver pressure updates
     */
    val pressureObserver: Observable<Double> = Observable.create<Double> { emitter = it }.share()

    /**
     * Observe pressure altitude
     */

    val altitudeObserver: Observable<Double>
        get() = pressureObserver.map {
            toAltitude(it)
        }

    /**
     * Convert pressure in hPa to altitude in m
     */
    fun toAltitude(pressure: Double): Double {
        return pressure from Units.Unit.HPA to Units.Unit.PALT
    }

    /**
     * Convert pressure altitude in m to pressure in hPa
     */
    fun toPressure(altitude: Double): Double {
        return altitude from Units.Unit.PALT to Units.Unit.HPA
    }

    /**
     * Resend pressure to any listeners
     */

    fun refresh() {
        emitter.onNext(lastPressure)
    }

    /** here we collect locations from sources of different priorities and assess their worth.
     * A source is considered valid if it has delivered at least m updates in the last T seconds.
     */

    private const val TIME_WINDOW = 60L      // seconds over which to assess
    private const val MIN_ITEMS = 5         // minimum number of items received in that time

    /**
     * The sources to be used, mapped to a history of valid data
     */
    private val sources: ConcurrentHashMap<PressureProvider, Provider> = ConcurrentHashMap()

    private class Provider(val source: PressureProvider) {
        val instants = mutableListOf<Instant>()
        val disposable = source.requestPermission()
                .observeOn(Schedulers.io())
                .filter { it }
                .flatMapObservable { source.pressureObserver }
                .filter { it != INVALID_DATA }
                .subscribe(
                        {
                            val since = instantNow.minusSeconds(TIME_WINDOW)
                            synchronized(instants) {
                                instants.add(instantNow)
                                instants.removeAll { it.isBefore(since) }
                            }
                            if (source == best(since)) {
                                lastPressure = it
                            }
                        },
                        {
                            CJLog.logException(it)
                            sources.remove(source)
                        }
                )

        fun valid(since: Instant): Boolean {
            synchronized(instants) {
                return instants.count { it.isAfter(since) } >= MIN_ITEMS
            }
        }
    }

    /**
     * Add a source
     */

    fun add(source: PressureProvider) {
        sources.getOrPut(source, { Provider(source) })
    }

    /**
     * Remove a source
     */

    fun remove(source: PressureProvider) {
        sources.remove(source)?.disposable?.dispose()
    }

    /**
     * Get the highest priority valid source.
     */

    private fun best(since: Instant): PressureProvider? {
        sources.filter { it.value.valid(since) }.maxByOrNull { it.key.priority }?.key?.let { return it }
        return sources.maxByOrNull { it.value.instants.size }?.key
    }

    const val DEVICE_PRIORITY = 100

}
