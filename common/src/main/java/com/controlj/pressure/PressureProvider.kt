package com.controlj.pressure

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single


/**
 * An interface to be implemented by sources of atmospheric pressure.
 *
 */
interface PressureProvider {
    /**
     * An Observable that will deliver a location stream.
     */
    val pressureObserver: Observable<Double>

    /**
     * The priority of the source. Higher is better
     */
    val priority: Int

    /**
     * Request permission to use this source.
     * Default implementation returns true
     */
    fun requestPermission(rationale: String = RATIONALE): Single<Boolean> {
        return Single.just(true)
    }

    companion object {
        const val RATIONALE = "Pressure measurement is used to determine pressure altitude"
    }
}
