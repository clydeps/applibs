package com.controlj.view

import com.controlj.logging.CJLog.logMsg

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-04-11
 * Time: 07:31
 */
open class MenuEntry(val title: String, val block: () -> Unit) {
    open val enabled: Boolean = true
    open var selected: Boolean = false

    fun action() {
        logMsg("Running menu action $title")
        block()
    }
}
