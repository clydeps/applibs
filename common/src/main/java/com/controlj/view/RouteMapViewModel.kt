package com.controlj.view

import com.controlj.data.FlightPoint
import com.controlj.data.FlightRoute
import com.controlj.data.Position
import com.controlj.data.TrackPoint
import com.controlj.mapping.RasterTileSource
import com.controlj.mapping.Skysight
import com.controlj.mapping.TileSource
import com.controlj.route.RouteSource
import com.controlj.route.TrackSource
import com.controlj.rx.MainScheduler
import com.controlj.rx.observeBy
import com.controlj.settings.BooleanSetting
import com.controlj.settings.ChoiceSetting
import com.controlj.settings.LayerSettings
import com.controlj.settings.Properties
import com.controlj.settings.Setting
import com.controlj.traffic.TrafficSource
import com.controlj.traffic.TrafficTarget
import com.controlj.utility.Units
import com.google.gson.annotations.Expose
import io.reactivex.rxjava3.internal.util.ArrayListSupplier
import org.threeten.bp.Duration
import java.lang.Math.pow
import java.util.concurrent.TimeUnit
import kotlin.math.abs
import kotlin.math.sqrt

/**
 * Created by clyde on 2/5/18.
 *
 * This class builds on MapViewModel adding route display
 */
open class RouteMapViewModel : MapViewModel() {

    @Expose
    var route: FlightRoute<FlightPoint> = FlightRoute()

    var previousTrack: List<TrackPoint> = listOf()

    val show3D: Boolean     // if 3D airspace should shown
        get() = LayerSettings.threeD.value && pitch >= 10.0

    // The mapbox style to use
    override val style: String = Properties.getProperty("mapStyle")
    private val token: String = Properties.getProperty("mapboxToken")
    private val satelliteSource: String = Properties.getProperty("satelliteSource")

    // the size of the last static track list sent
    private var staticTrackSize = 0

    protected val inFlight: Boolean
        get() = location.valid && location.speed > MIN_FLIGHT_SPEED

    /**************************************************************
     * Messages to deliver
     **************************************************************/

    interface RouteMapViewEvent : MapViewEvent

    /**
     * Show the given [route]
     *
     */
    data class ShowRouteEvent(val route: FlightRoute<FlightPoint>) : RouteMapViewEvent

    /**
     * Show the given track points. Use an empty list to remove.
     */
    data class ShowLineEvent(
        val key: TrackSource.TrackKey,
        val points: List<TrackPoint> = listOf(),
        val width: Double = TRACK_WIDTH,
        val color: Int = key.color
    ) : RouteMapViewEvent

    /**
     * Show or hide a given layer
     */

    data class ShowLayerEvent(val layer: String, val visible: Boolean) : RouteMapViewEvent

    /**
     * Add the vector source given by [name] to the map using the [url]
     * The source type is vector(default) or raster according to the [vector] value.
     */

    open class AddSourceEvent(val name: String, val url: String, val vector: Boolean = true) : RouteMapViewEvent

    // add a tileset
    open class AddTilesEvent(
        val name: String,
        val source: TileSource,
        val opacity: Double = 0.5,
        val vector: Boolean = true
    ) : RouteMapViewEvent

    open class AddRadarSourceEvent(
        val source: RasterTileSource
    ) : RouteMapViewEvent

    /**
     * Remove the vector source given by [name] from the map
     */

    data class RemoveSourceEvent(val name: String, val vector: Boolean = true) : RouteMapViewEvent

    /**
     * Remove the layer given by [name] from the map
     */

    data class RemoveLayerEvent(val name: String) : RouteMapViewEvent

    /**
     * Set the opacity of the given layer
     * @param name The layer name
     * @param opacity The target opacity
     * @param duration The duration over which to animate it.
     */

    data class SetLayerOpacityEvent(val name: String, val opacity: Double, val duration: Double = 0.0) :
        RouteMapViewEvent

    /**
     * Add a fill layer to the map
     * @param name The name of the layer
     * @param source The name of the vector source to use
     * @param sourceLayer The layer from the source to use
     * @param parameter The interpolation parameter name
     * @param fillColors The set of fill colours to interpolate through
     * @param below The layer below which to place this layer
     */

    data class AddFillLayerEvent(
        val name: String,
        val source: String,
        val sourceLayer: String,
        val parameter: String = "",
        val fillColors: Map<Int, Int> = mapOf(),
        val below: String = ""
    ) : RouteMapViewEvent

    /**
     * Add a raster layer to the map
     * @param name The name of the layer
     * @param source The name of the raster source to use
     * @param below The layer below which to place this layer
     */

    data class AddRasterLayerEvent(
        val name: String,
        val source: String,
        val below: String = ""
    ) : RouteMapViewEvent

    /**
     * A list of possible icons and their filenames
     */
    enum class TrafficIcon(val imageFile: String) {
        Normal("traffic"),
        Proximate("traffic_proximate"),
        Alert("traffic_alert")
    }

    /**
     * Traffic data
     */

    data class TrafficEvent(val targets: List<TrafficTarget>) : MapViewEvent

    /**
     * When the layers button is tapped, show a dialog to select active layers
     */
    @Suppress("CheckResult")
    fun onLayerClick() {
        LayerSettings.dialogData.singleObserver.subscribe { _ -> refreshLayers() }
    }

    // utility fun

    private var lastThermalTiles: TileSource? = null

    private fun String.is2D(): Boolean = !contains("3d", true)
    override fun refreshLayers() {
        //debug("refresh: 3d = ${LayerSettings.threeD.value}, pitch = $pitch")
        LayerSettings.layerMap.forEach { layerList ->
            val setting = Setting.get<BooleanSetting>(layerList.key)
            setting?.let {
                layerList.value.forEach { name ->
                    emitter.onNext(ShowLayerEvent(name, setting.value && (show3D || name.is2D())))
                }
            }
        }
        if (LayerSettings.satellite.value) {
            emitter.onNext(AddSourceEvent("satellite", "$satelliteSource?secure&access_token=$token", vector = false))
            emitter.onNext(AddRasterLayerEvent("satellite", "satellite", LayerSettings.satelliteBelow))
            emitter.onNext(SetLayerOpacityEvent("satellite", 1.0))
        } else {
            emitter.onNext(RemoveLayerEvent("satellite"))
            emitter.onNext(RemoveSourceEvent("satellite"))
        }
        if (LayerSettings.thermals.value) {
            lastThermalTiles = Skysight.getCurrentTiles().also {
                emitter.onNext(AddTilesEvent("thermals", it))
                emitter.onNext(
                    AddFillLayerEvent(
                        "thermals",
                        "thermals",
                        sourceLayer = it.layer,
                        parameter = "idx",
                        fillColors = it.colors,
                        below = LayerSettings.satelliteBelow
                    )
                )
                emitter.onNext(SetLayerOpacityEvent("thermals", .9))
            }
        } else {
            emitter.onNext(RemoveLayerEvent("thermals"))
            emitter.onNext(RemoveSourceEvent("thermals"))
        }

        TrackSource.refresh()
        RouteSource.refresh()
    }

    private fun clearTrack() {
        emitter.onNext(ShowLineEvent(TrackSource.TrackKey.CURRENT_TRACK))
        emitter.onNext(ShowLineEvent(TrackSource.TrackKey.STATIC_TRACK))
        staticTrackSize = 0
    }

    private fun updateTrack(track: TrackSource.TrackEvent) {
        when (track.key) {
            TrackSource.TrackKey.CURRENT_TRACK -> {
                if (LayerSettings.trackHistory.value) {
                    if (track.points.size < staticTrackSize)
                        clearTrack()
                    if (track.points.size > staticTrackSize + TRACK_INCREMENT + 3) {
                        emitter.onNext(ShowLineEvent(TrackSource.TrackKey.STATIC_TRACK, track.points.dropLast(1)))
                        staticTrackSize = track.points.size - 3
                    }
                    emitter.onNext(ShowLineEvent(track.key, track.points.drop(staticTrackSize)))
                } else
                    clearTrack()
            }

            TrackSource.TrackKey.PREVIOUS_TRACK -> {
                previousTrack = track.points
                if (!track.bounds.isEmpty) {
                    val trackCenter = track.bounds.center()
                    suspendTracking()
                    emitter.onNext(
                        BoundsEvent(
                            track.bounds,
                            if (moveDistance(center, trackCenter) > 4.0) Duration.ZERO else Duration.ofMillis(500L)
                        )
                    )
                }
            }

            else -> Unit
        }
        refreshTracks()
    }

    private fun refreshTracks() {
        // remove existing lines
        emitter.onNext(
            ShowLineEvent(
                TrackSource.TrackKey.PREVIOUS_TRACK,
                if (LayerSettings.previousTrack.value) previousTrack else listOf()
            )
        )
        if (!LayerSettings.trackHistory.value)
            clearTrack()
    }

    override fun onStart() {
        super.onStart()
        addDisposable(RouteSource.routeObserver
            .observeOn(MainScheduler())
            .observeBy {
                route = it
                emitter.onNext(ShowRouteEvent(it))
            }
        )
        addDisposable(TrackSource.observer
            .observeOn(MainScheduler())
            .observeBy {
                updateTrack(it)
            }
        )
        addDisposable(
            TrafficSource.observer
                .observeOn(MainScheduler())
                .filter { LayerSettings.traffic.value }
                .buffer(
                    5,
                    TimeUnit.SECONDS,
                    MainScheduler(),
                    1,
                    ArrayListSupplier.asSupplier<List<TrafficTarget>?>(),   // type argument required for disambiguation
                    true
                )
                .observeBy {
                    emitter.onNext(TrafficEvent(if (it.isEmpty()) listOf() else it.first()))
                }
        )
        refreshLayers()
    }

    /**
     * When recentering, adjust the zoom to include the selected point in the view
     * TODO - this doesn't work quite right
     */
    override fun resumeTracking(force: Boolean, setHeading: Boolean) {
        if (force || trackPosition) {
            val point = when (zoomRoute.value) {
                RouteZoom.ACTIVE -> route.activeWpt
                RouteZoom.DESTINATION -> route.destination
                RouteZoom.NONE -> null
            }
            if (point != null) {
                zoom = zoomFor(point)
            }
        }
        super.resumeTracking(force, setHeading)
    }

    /**
     * Calculate the zoom required to display the given point on the same map as the center point
     */

    fun zoomFor(pos1: Position): Double {
        val latDiff = pos1.latitude - location.latitude
        val lonDiff = pos1.longitude - location.longitude
        val diff = when (trackingMode.headingMode) {
            HeadingMode.NORTHUP -> abs(latDiff)
            else -> sqrt(latDiff * latDiff + lonDiff + lonDiff)
        }
        if (diff < 360.0 / pow(2.0, maximumZoomLevel))
            return maximumZoomLevel
        return ((Math.log(180.0) - Math.log(diff)) / Math.log(2.0) * if (trackingMode.offset) 1.2 else 1.1)
            .coerceIn(minimumZoomLevel, maximumZoomLevel)
    }

    enum class RouteZoom(val description: String) {
        NONE("None"),
        DESTINATION("Destination"),
        ACTIVE("Active waypoint")
    }

    companion object {

        /**
         * Any speed above this will be treated as inflight
         */
        val MIN_FLIGHT_SPEED = Units.Unit.KNOT.convertFrom(30.0)
        const val ZOOM_SETTING_LANDING = "zoom_on_landing"
        const val ZOOM_SETTING_ROUTE = "zoom_route"

        val zoomRoute = ChoiceSetting(
            ZOOM_SETTING_ROUTE,
            "Zoom to waypoint",
            "Automatically zoom to show waypoint",
            RouteZoom.values().toList()
        )
        val zoomOnLanding = BooleanSetting(
            ZOOM_SETTING_LANDING,
            "Zoom on landing",
            "Zoom in to taxiway level after landing",
            true
        )

        const val TRACK_INCREMENT = 20      // Keep dynamically updated track points to less than this.
        const val TRACK_WIDTH = 4.0
    }
}

val TrafficTarget.icon: RouteMapViewModel.TrafficIcon
    get() = when {
        isHazard -> RouteMapViewModel.TrafficIcon.Alert
        isProximate -> RouteMapViewModel.TrafficIcon.Proximate
        else -> RouteMapViewModel.TrafficIcon.Normal
    }

