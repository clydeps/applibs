package com.controlj.view

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.Location
import com.controlj.data.LocationBounds
import com.controlj.data.Position
import com.controlj.location.LocationSource
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import com.controlj.utility.instantNow
import com.controlj.viewmodel.ViewModel
import com.google.gson.annotations.Expose
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 26/4/18.
 */

abstract class MapViewModel : ViewModel {

    /**
     * The zoom level of the view
     */
    @Expose
    var zoom: Double = 6.0

    /**
     * The pitch of the view. Zero means looking straight down
     */
    @Expose
    var pitch: Double = 0.0

    /**
     * The current map view center
     */
    @Expose
    var center: Position = Position.invalid

    /**
     * The tracking mode, persisted
     */
    @Expose
    var trackingMode: TrackingMode = TrackingMode.TrackUp

    /**
     * The name of the image to use as a flight marker
     */
    @Expose
    var flightMarkerFilename: String = "Flight_marker"

    open var paused = false       // set externally when map is completely covered
        set(value) {
            field = value
            if (!value)
                refreshLayers()
        }

    companion object {
        val MaxAnimationDuration: Duration = Duration.ofSeconds(2)
        const val CENTER_OFFSET = 0.5      // offset of center from current position as fraction of height
        /**
         * The default delay after a pan operation before the map is re-centered, in seconds
         */
        var panDelay = 10       // delay after panning before resetting position
        const val REFRESH_DELAY = 1000      // debounce move events by this for refresh, in ms
    }

    /**
     * Possible heading modes
     */
    enum class HeadingMode {
        NONE,                   // no map rotation done
        NORTHUP,                // map is oriented north up
        TRACKUP                 // map is oriented track up
    }

    /**
     * Tracking modes
     * @param headingMode   The heading mode to be used
     * @param trackLocation True if the map should be moved as the location changes
     * @param offset        True if the location should be offset towards the bottom of the map
     */
    enum class TrackingMode(val headingMode: HeadingMode, val trackLocation: Boolean = true, val offset: Boolean = false) {
        None(headingMode = HeadingMode.NONE, trackLocation = false, offset = false),    // no tracking
        NorthUp(headingMode = HeadingMode.NORTHUP, offset = false),                      // Northup, position center
        TrackUp(headingMode = HeadingMode.TRACKUP, offset = true),                      // heading up, position bottom
        Center(headingMode = HeadingMode.TRACKUP, offset = false)                       // heading up, position center
    }

    /**
     * Events that are delivered by this model
     */

    interface MapViewEvent

    /**
     * optionally Rotate and center the map
     */
    data class CenterEvent(
            val direction: Double,    // the map heading (direction of up)
            val center: Position,   // the new map center position
            val zoom: Double,       // the zoom level
            val pitch: Double,      // the viewing pitch
            val animation: Duration     // the time allowed to change, in ms
    ) : MapViewEvent

    /**
     * An event to command the map to zoom to the specified bounds
     */
    data class BoundsEvent(
            val bounds: LocationBounds,
            val animation: Duration = Duration.ZERO     // the time allowed to change, in ms
    ) : MapViewEvent

    /**
     * Set the position of the ownship marker
     */
    data class MarkerPositionEvent(val location: Location, val animationTime: Duration) : MapViewEvent

    /**
     * Set the map display offsets.
     */
    data class OffsetEvent(val left: Double, val top: Double) : MapViewEvent

    /**
     * Set the map display offsets.
     */
    data class MenuEvent(val title: String, val entries: List<MenuEntry>, val where: Any?) : MapViewEvent

    /**
     * Set the map note text to the supplied value
     */

    data class SetNoteTextEvent(val text: String) : MapViewEvent

    /**
     * The emitter for map events
     */
    protected var emitter: ObservableEmitter<MapViewEvent> = DisposedEmitter()

    /**
     * Observe events from this model
     */
    val observable: Observable<MapViewEvent> = Observable.create<MapViewEvent> {
        emitter = it
        onStart()
    }
            .filter { !paused }
            .doFinally { onStop() }
            .observeOn(MainScheduler.instance)
            .share()

    /**
     * Return true if location updates should move the map position.
     * By default tracking is suspended for a period of time after pan operations
     */
    open val trackPosition
        get() = trackingMode.trackLocation && pannedDisposable.isDisposed
    /**
     * The style to use for this map
     */
    abstract val style: String

    /**
     * Things to be disposed on when this model is stopped.
     */
    protected val disposables = CompositeDisposable()

    /**
     * Maximum map zoom level
     */
    open val maximumZoomLevel = 17.0
    /**
     * Minimum zoom level
     */
    open val minimumZoomLevel = 4.0

    /**
     * Disposable for pan timer
     */
    private var pannedDisposable = Disposable.disposed()       // for a timer that resets the view after user panning
        set(value) {
            removeDisposable(field)
            field = value
            addDisposable(value)
        }

    /**
     * Disposable for tracking location.
     */
    private var locationDisposable = Disposable.disposed()
        set(value) {
            removeDisposable(field)
            field = value
            addDisposable(value)
        }
    private var cameraMoveDisposable = Disposable.disposed()
        set(value) {
            removeDisposable(field)
            field = value
            addDisposable(value)
        }
    private var cameraMoveEmitter: ObservableEmitter<Double> = DisposedEmitter()

    /**
     * When the last update was done
     */
    private var lastUpdateTime: Instant = Instant.EPOCH
    /**
     * The interval between updates, used to estimate required animation time
     */
    private var lastUpdateInterval: Duration = Duration.ofMillis(500)

    /**
     * The current location - will update the marker
     */
    var location: Location = Location.invalid
    //debug("location $value")

    /**
     * GEt the move distance in degrees squared, roughly
     */
    protected fun moveDistance(from: Position, to: Position): Double {
        val dy = from.latitude - to.latitude
        val dx = from.longitude - to.longitude
        return dx * dx + dy * dy
    }

    /**
     * Recenter the map
     */
    open fun resumeTracking(force: Boolean = false, setHeading: Boolean = true) {
        if ((force || trackPosition) && location.valid) {
            val direction = if (setHeading) when (trackingMode.headingMode) {
                HeadingMode.TRACKUP -> location.track
                else -> 0.0
            } else INVALID_DATA
            val animationTime = if (moveDistance(location, center) > 4) Duration.ZERO else lastUpdateInterval
            center = Position.from(location)
            emitter.onNext(OffsetEvent(0.0, if (trackingMode.offset) CENTER_OFFSET else 0.0))
            emitter.onNext(CenterEvent(direction, center, zoom, pitch, animationTime))
        }
    }

    open fun refreshLayers() {
        LocationSource.refresh()
    }

    /**
     * Move the location marker
     */

    open fun updateMarker() {
        emitter.onNext(MarkerPositionEvent(location, lastUpdateInterval))
    }

    /**
     * Cancel panning, recenter
     */

    open fun cancelPan() {
        removeDisposable(pannedDisposable)
        resumeTracking()
    }

    /**
     * Called by clients when the "Location" button is clicked. Triggers recentering or cycling through
     * tracking modes.
     */
    open fun onLocationClick(source: Any?) {
        if (!pannedDisposable.isDisposed) {
            cancelPan()
        } else {
            resumeTracking(true)
            val menu = TrackingMode.values().map {
                val entry = MenuEntry(it.name) { trackingMode = it; resumeTracking(true) }
                if (it == trackingMode)
                    entry.selected = true
                entry
            }
            emitter.onNext(MenuEvent("Tracking mode", menu, source))
        }
    }

    /**
     * Start operations.
     */
    open fun onStart() {
        if (disposables.size() != 0)
            disposables.clear()
        if (center.valid) {
            location = Location.of(center)
            resumeTracking(force = true, setHeading = false)
        }
        locationDisposable = LocationSource.observer
                .subscribe(
                        { value ->
                            onLocation(value)
                        },
                        {}
                )

        // debounce camera moves, refresh layers if pitch has changed.
        cameraMoveDisposable = Observable.create<Double> { emitter -> cameraMoveEmitter = emitter }
                .filter { it != pitch }
                .debounce(REFRESH_DELAY.toLong(), TimeUnit.MILLISECONDS, MainScheduler.instance)
                .subscribe { refreshLayers() }
        refreshLayers()
    }

    open fun onStop() {
        disposables.clear()
    }

    open fun onLocation(value: Location) {
        val now = instantNow
        lastUpdateInterval = Duration.between(lastUpdateTime, now)
                .coerceAtMost(MaxAnimationDuration)
        // force recenter if this is the first valid location.
        val force = !location.valid
        if (value.valid) {
            if (value.movedFrom(location)) {
                location = value
                lastUpdateTime = now
                resumeTracking(force)
                updateMarker()
            }
        }
    }

    /**
     * Add a disposable to the pool to dispose when stopped
     */
    protected fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    /**
     * Remove a disposable from the pool and dispose it.
     */
    protected fun removeDisposable(disposable: Disposable) {
        disposables.remove(disposable)
    }

    /**
     * Called by the client when the view has been changed by the user, e.g. a pan event
     */
    open fun onViewChanged(newCenter: Position, newZoom: Double, newPitch: Double = 0.0) {
        zoom = newZoom
        center = newCenter
        val oldPitch = pitch
        pitch = newPitch
        //debug("View changed to $center, $zoom, $newPitch")
        removeDisposable(pannedDisposable)
        suspendTracking()
        cameraMoveEmitter.onNext(oldPitch)
    }

    fun suspendTracking() {
        if (trackingMode != TrackingMode.None)
            pannedDisposable = Completable.timer(panDelay.toLong(), TimeUnit.SECONDS, MainScheduler.instance)
                    .subscribe(this::cancelPan)
    }
}
