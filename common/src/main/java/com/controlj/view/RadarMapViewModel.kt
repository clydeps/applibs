package com.controlj.view

import com.controlj.mapping.RasterTileSource
import com.controlj.rx.MainScheduler
import com.controlj.rx.observeBy
import com.controlj.settings.LayerSettings
import com.controlj.ui.UiAction
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 2/5/18.
 *
 * This class builds on RouteMapViewModel adding radar display
 */
open class RadarMapViewModel : RouteMapViewModel() {

    private var rasterDisposable = Disposable.disposed()        // disposable for animating raster display
    private var tileSourceDisposable = Disposable.disposed()   // for receiving raster data
    private var rasterLayers = listOf<String>()                  // the list of raster layers

    override val trackPosition: Boolean
        get() = inFlight && super.trackPosition

    private fun hideRaster() {
        removeDisposable(rasterDisposable)
        rasterLayers.forEach {
            emitter.onNext(RemoveLayerEvent(it))
            emitter.onNext(RemoveSourceEvent(it))
        }
        rasterLayers = listOf()
    }

    /**
     * Start, or restart, the raster layer display
     */

    private fun startRaster() {
        hideRaster()
        emitter.onNext(SetNoteTextEvent(""))
        removeDisposable(tileSourceDisposable)
        val history =
            (if (LayerSettings.looping.value) MAX_RADAR_LAYERS else 1)
        tileSourceDisposable = LayerSettings.weather.getTypedValue().getHistory(history)
            .observeOn(MainScheduler())
            .observeBy(
                {
                    UiAction.Toast(it.message.toString(), UiAction.ToastDuration.LONG)
                    hideRaster()
                }
            ) {
                showRaster(it)
            }
        addDisposable(tileSourceDisposable)
    }

    override fun refreshLayers() {
        super.refreshLayers()
        startRaster()
    }

    /**
     * Show the radar display, now we have some data
     */
    private fun showRaster(layers: List<RasterTileSource>) {
        rasterLayers = layers.map { it.name }
        layers.forEachIndexed { index, path ->
            emitter.onNext(AddRadarSourceEvent(path))
            emitter.onNext(AddRasterLayerEvent(path.name, path.name, below = LayerSettings.radarBelow))
            emitter.onNext(SetLayerOpacityEvent(path.name, if (index == 0) 1.0 else 0.0))
        }
        if (layers.size > 1) {
            val list = layers.reversed()        // oldest first
            rasterDisposable =
                Observable.interval(ANIMATE_INTERVAL / 10, TimeUnit.MILLISECONDS)
                    .observeBy {
                        val index = ((it / 10) % (layers.size + ANIMATE_PAUSE)).toInt()
                        if (index < list.size) {
                            val previous: Int
                            val fade: Double
                            if (index == 0) {
                                fade = 1.0
                                previous = list.size - 1
                            } else {
                                fade = if (index == 0) 1.0 else (it % 10 + 1) / 10.0
                                previous = index - 1
                            }
                            emitter.onNext(SetNoteTextEvent(list[index].description))
                            emitter.onNext(SetLayerOpacityEvent(list[index].name, (fade * 2.0).coerceAtMost(1.0)))
                            emitter.onNext(SetLayerOpacityEvent(list[previous].name, 1.0 - fade))
                        }
                    }
            addDisposable(rasterDisposable)
        }
    }

    companion object {

        const val ANIMATE_INTERVAL = 600L    // milliseconds
        const val ANIMATE_PAUSE = 4         // number of intervals to pause on last
        const val MAX_RADAR_LAYERS = 8
    }
}
