package com.controlj.view

import com.controlj.framework.ApplicationState
import com.controlj.logging.CJLog.logException
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import com.controlj.rx.updateUI
import com.controlj.utility.DiffUtil
import com.controlj.utility.ListUpdateCallback
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.ArrayList

/**
 * This class implements a presenter for a listview. The list may contain multiple headings and is organized as a list of groups, each with its own heading.
 *
 * Created by clyde on 27/11/17.
 */

/**
 * The list view presenter.
 */
abstract class ListPresenter<T : Comparable<T>> : ListUpdateCallback {

    /**
     * A model that should be overridden by subclasses
     */

    open val model: ListModel by lazy { ListModel() }

    private val naturalOrder = Comparator<T> { o1, o2 -> o1.compareTo(o2) }
    private val reverseOrder = Comparator<T> { o1, o2 -> o2.compareTo(o1) }

    /**
     * Get a comparator to sort the list within each section
     */

    open val comparator: Comparator<T>
        get() = when {
            model.sortDescending.data -> reverseOrder
            else -> naturalOrder
        }

    private val itemMap = mutableMapOf<String, T>()
    private var depth: Int = 0          // depth of changes

    open val hideHeaders: Boolean =
        false   // set to true to hide section headers. Typically used only with a single section

    abstract val keySelector: (T) -> String

    /**
     * reasons to change the dataset
     */
    enum class ChangeReason {
        CHANGED,        // specifies rows that have been changed
        ADDED,          // rows have been added
        REMOVED,        //rows have been deleted

        //MOVED,          // one row moved
        REFRESH,        // refresh the entire data set
        STARTED,        // a changeset has been started
        ENDED,          // a change has completed
        BUSY,           // flag that we are busy
        IDLE,            // flag no longer busy
        CUSTOM          // custom event for subclasses to use
    }

    interface ViewEvent

    open inner class MenuEvent(val title: String, val choices: List<MenuEntry>, val item: T?) : ViewEvent

    /**
     * Capture a reason and range of changes
     */
    open class ChangeRange(val reason: ChangeReason, val start: Int = 0, val length: Int = 0) : ViewEvent {
        override fun toString(): String {
            return "ChangeRange(reason=$reason, start=$start, length=$length)"
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as ChangeRange

            if (reason != other.reason) return false
            if (start != other.start) return false
            if (length != other.length) return false

            return true
        }

        override fun hashCode(): Int {
            var result = reason.hashCode()
            result = 31 * result + start
            result = 31 * result + length
            return result
        }
    }

    private var refreshDisposable = Disposable.disposed()
    protected val disposables = CompositeDisposable()
    private var changeEmitter: ObservableEmitter<ViewEvent> = DisposedEmitter()
    private var selectionEmitter: ObservableEmitter<T> = DisposedEmitter()

    /**
     * Set the busy state of this presenter
     */

    protected fun setBusy(busy: Boolean) {
        ApplicationState.setBusy(this::class.java.simpleName, busy)
    }

    /**
     * Start a refresh operation.
     * By default starts the standard refresh, can be overridden for custom behaviour.
     */
    open fun startRefresh() {
        refresh()
    }

    /**
     * Signal the view that the refresh has completed
     */
    open fun endRefresh() {
        disposables.remove(refreshDisposable)
    }

    /**
     * Notify that a block of updates is beginning
     */
    open fun blockStart() {
        if (depth == 0)
            rangeChanged(ChangeReason.STARTED)
        depth++
    }

    /**
     * Notify that a block of updates has ended
     */
    open fun blockEnd() {
        depth--
        if (depth == 0)
            rangeChanged(ChangeReason.ENDED, length = totalRows)
    }

    /**
     * Listen for changes to the data
     */
    val changeObserver: Observable<ViewEvent> by lazy {
        Observable.create { emitter: ObservableEmitter<ViewEvent> ->
            changeEmitter = emitter
            onStart()
        }
            .subscribeOn(MainScheduler())
            .observeOn(MainScheduler())
            .doFinally { onStop() }
            .share()
    }

    open fun onStop() {
        disposables.clear()
    }

    open fun onStart() {
        val collapsedSet = model.collapsedList.toSet()
        sections.forEach { it.collapsed = collapsedSet.contains(it.name) }
        startRefresh()
    }

    /**
     * Listen for selection changes
     */

    val selectionObserver: Observable<T> by lazy {
        Observable.create { emitter: ObservableEmitter<T> ->
            selectionEmitter = emitter
            selected?.let { emitter.onNext(it) }
        }.share()

    }

    interface Checkable {
        var isChecked: Boolean
    }

    interface Selectable {
        var isSelected: Boolean
    }

    /**
     * Are the list items checkable? Override in subclasses as required
     */
    open val checkable = false
    open val selectable = false

    /**
     * A list of the checked items.
     * The equals() and hash() methods of the contained type should be suitable
     * for checking this
     */

    val checkedItems: Set<T>
        get() {
            return model.checkedSet.mapNotNull { itemMap[it] }.toSet()
        }

    /**
     * The currently selected section or item
     */

    open var selected: T? = null
        set(value) {
            if (indexOf(value) == indexOf(field)) return
            val previous = field
            field = value
            previous?.let {
                if (it is Selectable)
                    it.isSelected = false
                val pos = indexOf(it)
                if (pos != null)
                    rangeChanged(ChangeReason.CHANGED, toGlobal(pos), 1)
            }
            if (value != null) {
                if (value is Selectable) {
                    value.isSelected = true
                    selectionEmitter.onNext(value)
                }
                indexOf(value)?.let {
                    rangeChanged(ChangeReason.CHANGED, toGlobal(it), 1)
                }
                model.selectedKey.data = keySelector(value)
            }
        }
        get() {
            return field.let {
                when {
                    it == null || indexOf(it) == null -> null
                    else -> it
                }
            }
        }

    /**
     * This class defines a row.
     * @param section the section number, zero based
     * @param row The row within the section, where row 0 is the header.
     */

    data class RowIndex(val section: Int, val row: Int)

    fun isHeader(index: RowIndex): Boolean {
        return !hideHeaders && index.row == 0
    }

    protected fun showMenu(title: String, choices: List<MenuEntry>, item: T) {
        changeEmitter.onNext(MenuEvent(title, choices, item))
    }

    protected fun rangeChanged(reason: ChangeReason, start: Int = 0, length: Int = 0) {
        //debug("rangechanged start=$start, totalRows=$totalRows")
        changeEmitter.onNext(ChangeRange(reason, start, length))
    }

    protected fun onEvent(event: ViewEvent) {
        changeEmitter.onNext(event)
    }

    fun toGlobal(index: RowIndex): Int {
        return toGlobal(index.section, index.row)
    }

    fun toGlobal(section: Int, row: Int): Int {
        var globalRow = row
        for (i in 0 until section)
            globalRow += sections[i].rowCount
        return globalRow
    }

    private fun toRow(from: Int): Int {
        if (hideHeaders)
            return from
        return from - 1
    }

    private fun toPosition(index: Int, row: Int): RowIndex {
        if (hideHeaders)
            return RowIndex(index, row)
        return RowIndex(index, row + 1)
    }

    /**
     * Each row in the list may be either a header or a data element
     */
    enum class ViewType {
        HEADER,
        DATA
    }

    /**
     * Data structure to represent sections in the list.
     * Each section has a header. A RowIndex identifies a section
     * and the row within that section. Row 0 is the header
     * @param name The section name
     * @param index The index of the section.
     * @param refresh An [Observable] that will issue the data items in the section, used by [refresh]
     */
    inner class Section(
        var name: String,
        val index: Int,
        internal val refresh: Observable<out T>
    ) : Comparable<Section> {
        override fun compareTo(other: Section): Int {
            return index - other.index
        }

        /**
         * The data withing the section.
         */
        internal var data: MutableList<T> = mutableListOf()

        // cache the all checked state
        internal var allChecked: Boolean
            set(value) {
                if (value)
                    model.checkedSet.addAll(data.map { keySelector(it) })
                else
                    model.checkedSet.removeAll(data.map { keySelector(it) })
                allItems.filterIsInstance<Checkable>().forEach { it.isChecked = value }
                rangeChanged(ChangeReason.CHANGED, toGlobal(index, 0), rowCount)
            }
            get() = data.isNotEmpty() && model.checkedSet.containsAll(data.map { keySelector(it) })
        var collapsed: Boolean = false      // set this to collapse the section to just the header
        val allItems: List<T>
            get() = data.toList()

        /**
         * The number of rows in the section, including the header
         */
        val rowCount: Int
            get() =
                when {
                    hideHeaders -> data.size
                    collapsed -> 1
                    else -> data.size + 1
                }

        fun contains(item: T): Boolean {
            return data.contains(item)
        }

        fun remove(row: Int) {
            val item = data.removeAt(toRow(row))
            itemMap.remove(keySelector(item))
        }

        fun add(item: T): RowIndex {
            val result = toPosition(index, data.size)
            data.add(item)
            itemMap[keySelector(item)] = item
            return result
        }

        fun getItem(row: Int): T? {
            val pos = toRow(row)
            return data.getOrNull(pos)
        }

        fun indexOf(item: T): RowIndex? {
            if (collapsed)
                return null
            val p = data.indexOf(item)
            if (p >= 0)
                return toPosition(index, p)
            return null
        }

        /**
         * Perform a replacement of data using the changelist. Must be called on main thread.
         */

        fun replace(newData: List<T>) {
            data = newData.toMutableList()
            data.forEach { itemMap[keySelector(it)] = it }
        }
    }

    /**
     * The sections representing the dataset
     */
    val sections: MutableList<Section> = ArrayList()

    /**
     * The total number of rows in the list. Includes header rows if not hidden.
     */
    val totalRows: Int
        get() = sections.sumOf { it.rowCount }

    /**
     * True if the dataset is empty except for headers.
     */
    val isEmpty: Boolean
        get() = sections.sumOf { it.data.size } == 0

    /**
     * Get the RowIndex for a given linear row number
     */
    fun fromGlobal(position: Int): RowIndex {
        if (position < 0)
            throw IndexOutOfBoundsException("row index < 0")
        var result = position
        sections.forEach { section ->
            if (result < section.rowCount) return RowIndex(section.index, result)
            result -= section.rowCount
        }
        throw IndexOutOfBoundsException("row index > total")
    }

    /**
     * Get the Rowindex of a specific entry in the list
     */

    fun indexOf(item: T?): RowIndex? {
        if (item == null)
            return null
        sections.forEach { section ->
            val result = section.indexOf(item)
            if (result != null)
                return result
        }
        return null
    }

    /**
     * Get the number of sections in the dataset.
     */

    val sectionCount: Int
        get() = sections.size

    /**
     * Return the count of rows in the section.
     * @param section The section index
     * @return The number of rows, including the header, if shown.
     */
    fun rowsInSection(section: Int): Int {
        return sections[section].rowCount
    }

    private fun sectionName(idx: Int): String {
        return sections[idx].name
    }

    /**
     * Get a list of all items in the dataset.
     */
    val allItems: List<T>
        get() = sections.map { it.allItems }.flatten()

    open fun itemAt(index: RowIndex): T? {
        return sections[index.section].getItem(index.row)
    }

    protected open fun addSection(name: String, refresh: Observable<out T>): Section {
        return Section(name, sections.size, refresh).also { sections.add(it) }
    }

    open fun viewType(index: RowIndex): ViewType {
        if (isHeader(index))
            return ViewType.HEADER
        return ViewType.DATA
    }

    fun isCollapsed(section: Int): Boolean {
        return sections[section].collapsed
    }

    /**
     * Return a list of text values to populate an item with
     * The default is to return the section name. This should be overridden
     */
    @Deprecated("", ReplaceWith("textValues(index.section) or textValues(item)"))
    open fun textValues(index: RowIndex): List<String> {
        return textValues(index.section)
    }

    open fun textValues(section: Int): List<String> {
        return listOf(sectionName(section))
    }

    open fun textValues(item: T): List<String> {
        return listOf()
    }

    /**
     * Called by the view when an item is selected from the list
     */
    @Deprecated("Select by data instead of row", ReplaceWith("onItemSelected(item)"))
    open fun onItemSelected(index: RowIndex) {
        onItemSelected(itemAt(index))
    }

    open fun onItemSelected(item: T?) {
        selected = item
    }

    open fun onHeaderClicked(section: Int) {

    }

    /**
     * Is the header for this section checked.
     * @param section The section index
     * @return true if the section is checked.
     */
    open fun isSectionChecked(section: Int): Boolean {
        return sections[section].allChecked
    }

    /**
     * Is the item in the checked set?
     * @param item The item to be examined.
     * @return true if the item is checked.
     */

    open fun isChecked(item: T?): Boolean {
        if (item == null) return false
        return model.checkedSet.contains(keySelector(item))
    }

    /**
     * Mark a section header as checked - this has the effect of checking all the items in that section.
     * @param section The section number
     * @param value The desired state
     */
    open fun setCheckedHeader(section: Int, value: Boolean): Boolean {
        val wasChecked = sections[section].allChecked
        sections[section].allChecked = value
        return value != wasChecked
    }

    @Deprecated("Unsafe to use", ReplaceWith("setChecked(itemAt(index), value)"))
    open fun setChecked(index: RowIndex, value: Boolean): Boolean {
        return setChecked(itemAt(index), value)
    }

    /**
     * Set an item as being checked. If the item implements the [Checkable] interface then
     * its [Checkable.isChecked] state will be set accordingly as well.
     *
     * @param item The item to be checked.
     * @param value True if the item should be shown as checked.
     */
    open fun setChecked(item: T?, value: Boolean): Boolean {
        if (item != null) {
            val wasChecked = isChecked(item)
            if (value)
                model.checkedSet.add(keySelector(item))
            else
                model.checkedSet.remove(keySelector(item))
            if (item is Checkable)
                item.isChecked = value
            indexOf(item)?.let {
                rangeChanged(ChangeReason.CHANGED, toGlobal(it), 1)
                rangeChanged(ChangeReason.CHANGED, toGlobal(it.section, 0), 1)
            }
            return wasChecked != value
        }
        return false
    }

    /**
     * Set the collapsed state of the specified section. When collapsed only the header is visible.
     * @param secIdx The index of the section
     * @param value True to collapse the section.
     */
    open fun setCollapsed(secIdx: Int, value: Boolean) {
        if (hideHeaders)
            throw IllegalArgumentException("Can't collapse sections with hidden headers")
        val section = sections[secIdx]
        if (section.collapsed != value) {
            selected?.let {
                if (section.indexOf(it) != null)
                    selected = null
            }
            section.collapsed = value
            if (value)
                rangeChanged(ChangeReason.REMOVED, toGlobal(secIdx, 1), section.data.size)
            else
                rangeChanged(ChangeReason.ADDED, toGlobal(secIdx, 1), section.data.size)
            rangeChanged(ChangeReason.CHANGED, toGlobal(secIdx, 0), 1)
            model.collapsedList = sections.filter { it.collapsed }.map { it.name }
        }
    }

    override fun onInserted(position: Int, count: Int) {
        rangeChanged(ChangeReason.ADDED, position, count)
    }

    override fun onRemoved(position: Int, count: Int) {
        rangeChanged(ChangeReason.REMOVED, position, count)
    }

    override fun onMoved(fromPosition: Int, toPosition: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onChanged(position: Int, count: Int, payload: Any?) {
        rangeChanged(ChangeReason.CHANGED, position, count)
    }

    protected open fun compareContents(item1: T?, item2: T?): Boolean {
        return item1 == item2
    }

    /**
     * Calculate the changes needed to replace the current data with new.
     */
    private fun calcDiffs(newList: List<List<T>>): Pair<List<List<T>>, DiffUtil.DiffResult?> {

        /**
         * If the old list is empty except for headers, just refresh the whole lot
         */
        if (totalRows == sectionCount)
            return Pair(newList, null)
        val newMap = mutableListOf<RowIndex>()
        val oldMap = mutableListOf<RowIndex>()
        sections.forEachIndexed { index, section ->
            when {
                section.collapsed -> {
                    newMap.add(RowIndex(index, 0))
                    oldMap.add(RowIndex(index, 0))
                }
                else -> {
                    var pos = 0
                    if (!hideHeaders) {
                        newMap.add(RowIndex(index, 0))
                        oldMap.add(RowIndex(index, 0))
                        pos++
                    }
                    repeat(newList[index].size) {
                        newMap.add(RowIndex(index, it + pos))
                    }
                    repeat(sections[index].data.size) {
                        oldMap.add(RowIndex(index, it + pos))
                    }
                }
            }
        }
        val callback = object : DiffUtil.Callback() {
            override fun getOldListSize(): Int = totalRows
            override fun getNewListSize(): Int = newMap.size

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldRow = oldMap[oldItemPosition]
                val newRow = newMap[newItemPosition]
                //println("Comparing $oldItemPosition with $newItemPosition returns $result")
                return when {
                    isHeader(oldRow) != isHeader(newRow) -> false
                    isHeader(newRow) -> oldRow.section == newRow.section
                    else -> keySelector(newList[newRow.section][toRow(newRow.row)]) == keySelector(itemAt(oldRow)!!)
                }
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldRow = oldMap[oldItemPosition]
                val newRow = newMap[newItemPosition]
                if (isHeader(oldRow) != isHeader(newRow))
                    return false
                if (isHeader(oldRow) && isHeader(newRow))
                    return true
                return compareContents(itemAt(oldRow), newList[newRow.section][toRow(newRow.row)])
            }
        }
        // don't use moves
        return Pair(newList, DiffUtil.calculateDiff(callback, false))
    }

    /**
     * provide an asynchronous operation to redraw the displayed data.
     */
    protected open val refresher: Completable
        get() = Single.concat(sections.map { it.refresh.toSortedList(comparator) }).toList()
            .subscribeOn(MainScheduler())
            .flatMap {
                Single.fromCallable { calcDiffs(it) }
                    .subscribeOn(Schedulers.computation())
            }
            .observeOn(MainScheduler.instance)
            .map { pair ->
                val changes = pair.second
                if (changes == null) {
                    itemMap.clear()
                    sections.forEach {
                        it.replace(pair.first[it.index])
                    }
                    rangeChanged(ChangeReason.REFRESH)
                } else {
                    blockStart()
                    changes.dispatchUpdatesTo(this)
                    itemMap.clear()
                    sections.forEach {
                        it.replace(pair.first[it.index])
                    }
                    selected = itemMap[model.selectedKey.data]
                    model.checkedSet = model.checkedSet.mapNotNull { itemMap[it] }
                        .map { keySelector(it) }.toMutableSet()
                    blockEnd()
                }
            }
            .ignoreElement()

    /**
     * Refresh the display. This is intended to simply update the display from data already gathered.
     * Long-running operations to update the data should be started in [startRefresh] and stopped in
     * [endRefresh]
     */
    @Suppress("CheckResult")
    open fun refresh() {
        if (refreshDisposable.isDisposed) {
            refreshDisposable = refresher
                .updateUI({ setBusy(true) }, { setBusy(false) })
                .subscribe({ }, ::logException)
            disposables.add(refreshDisposable)
        }
    }
}
