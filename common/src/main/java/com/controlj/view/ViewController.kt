package com.controlj.view

import com.controlj.graphics.CRect

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 30/1/17
 * Time: 11:26 AM
 */
interface ViewController {


    /**
     * The bounds of the viewcontroller
     */
    var bounds: CRect
    /**
     * Tell the view controller that it should redraw itself, including the background.
     * @param rect The rectangle that needs redrawing - the default is the whole area
     */
    fun requestRedraw(rect: CRect = bounds)

    /**
     * Tell the view to redraw itself. This won't necessarily redraw the background.
     * @param rect The rectangle that needs redrawing - the default is the whole area
     */

    fun redrawForeground(rect: CRect = bounds)

}
