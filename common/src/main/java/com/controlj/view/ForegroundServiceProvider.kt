package com.controlj.view

import com.controlj.data.Message
import com.controlj.data.Priority
import com.controlj.framework.ApplicationState
import com.controlj.framework.SoundPlayer
import com.controlj.framework.SystemColors
import com.controlj.logging.CJLog.debug
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.MainScheduler
import com.controlj.ui.DialogData
import com.controlj.ui.NotificationPresenter
import com.controlj.ui.UiAction
import com.controlj.viewmodel.ViewModel
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import java.io.File
import java.util.concurrent.LinkedBlockingQueue

abstract class ForegroundServiceProvider {
    protected abstract var keepScreenOn: Boolean

    protected abstract fun showDialog(dialogData: DialogData)
    protected abstract fun openUrl(url: String): Boolean
    protected abstract fun openFile(file: File, mimeType: String)
    protected abstract fun toast(message: String, duration: UiAction.ToastDuration)

    open val systemColors: SystemColors = object : SystemColors {}

    /**
     * Process the given action, if possible
     * @param action The action
     * @return true if the action was processed here
     */
    open fun processAction(action: UiAction): Boolean {
        when (action) {
            is UiAction.Screen -> keepScreenOn = action.state
            is UiAction.Toast -> toast(action.message, action.duration)
            is UiAction.OpenUrl -> openUrl(action.url)
            is UiAction.OpenFile -> openFile(action.file, action.mimeType)
            is DialogData -> showDialog(action)
            else -> return false
        }
        return true
    }


    @Suppress("CheckResult")
    protected fun init() {
        instance = this
        Observable.merge(ApplicationState.current.listener, workAvailable.listener.filter { it })
            .subscribeOn(MainScheduler.instance)
            .subscribe(
                { checkWork() },
                ::logException
            )
    }

    fun onDestroy() {
        if (instance == this)
            instance = null
    }

    /**
     * Open the settings for this app
     */

    abstract fun openAppSettings()


    companion object {
        var instance: ForegroundServiceProvider? = null
            private set

        operator fun invoke() = instance
        operator fun <T : Any?> invoke(block: ForegroundServiceProvider.() -> T): T? = instance?.run(block)

        private val dialogQueue = LinkedBlockingQueue<DialogData>(10)
        private val workAvailable = ViewModel.BooleanData()

        /**
         * The currently showing dialog
         */
        private var showingDialog: DialogData? = null

        /**
         * Check for work and do it if possible. Should only be run on the main thread.
         */
        private fun checkWork() {
            debug("Checkwork: showing $showingDialog, AppState=${ApplicationState.current.data}, queue=${dialogQueue.joinToString { it.toString() }}}")
            if (showingDialog == null && ApplicationState.current.data.active && dialogQueue.isNotEmpty()) {
                @Suppress("CheckResult")
                dialogQueue.take().let {
                    showingDialog = it
                    cancelNotification(it)
                    it.singleObserver.subscribe(
                        {
                            showingDialog = null
                            checkWork()
                        },
                        {
                            logException(it)
                            showingDialog = null
                            checkWork()
                        })
                }
            }
        }

        /**
         * Add a dialog to the queue. If the dialog cannot be shown immediately, and it has notifyWhenInBackground set,
         * then a notification will be shown.
         * @param dialogData The dialog data
         */
        fun queue(dialogData: DialogData) {
            if (showingDialog == dialogData || dialogQueue.contains(dialogData))
                return
            if (!dialogQueue.offer(dialogData))
                logMsg("Failed to queue ${dialogData.title}, queue size = ${dialogQueue.size}")
            else {
                if (dialogData.notifyWhenInBackground && !ApplicationState.current.data.active) {
                    NotificationPresenter.showNotification(dialogData.message)
                    if (dialogData.speakTitle) {
                        SoundPlayer.speak(dialogData.title)
                    }
                }
                workAvailable.data = true
            }
        }

        /**
         * Cancel the queued dialog, or dismiss it if already showing
         * @param dialogData The dialog data to cancel
         */
        fun cancel(dialogData: DialogData) {
            if (dialogData.isShowing)
                dialogData.dismiss()
            else
                dialogQueue.remove(dialogData)
            cancelNotification(dialogData)
            workAvailable.data = dialogQueue.isNotEmpty()
        }

        private fun cancelNotification(dialogData: DialogData) {
            if (dialogData.notifyWhenInBackground)
                NotificationPresenter.cancelNotification(dialogData.message)
        }

        /**
         * Cancel all queued dialogs
         */

        fun cancelAll() {
            dialogQueue.toList().forEach { cancel(it) }
            showingDialog?.dismiss()
        }
    }

}

val DialogData.message: Message
    get() = Message(title, Priority.ACTION)
