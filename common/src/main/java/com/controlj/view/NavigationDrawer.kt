package com.controlj.view

import com.controlj.ui.DialogData

/**
 * Created by clyde on 1/11/17.
 */

interface NavigationDrawer {

    enum class Position {
        LEFT, RIGHT
    }

    enum class State {
        UNDEFINED, OPEN, CLOSED, OPENING, CLOSING
    }

    companion object {
        const val containerViewMaxAlpha = 0.4
        const val drawerAnimationDuration = 0.25
    }

    // the menu items to show.
    var menu: DialogData
    fun dismiss()
}
