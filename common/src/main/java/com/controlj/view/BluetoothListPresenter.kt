package com.controlj.view

import com.controlj.ble.BleDevice
import com.controlj.ble.BleScanResult
import com.controlj.ble.BleScanner
import com.controlj.location.LocationProvider
import com.controlj.rx.observeOnMainBy
import com.controlj.rx.updateUI
import com.controlj.ui.UiAction
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.internal.disposables.EmptyDisposable
import java.util.TreeMap
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 7/2/18.
 */

/**
 * @param typeName A name for the type of adapters we handle
 */
open class BluetoothListPresenter(typeName: String = "Bluetooth") : ListPresenter<BleScanResult>() {
    companion object {
        private const val ACTIVE_SECTION = 0
        private const val NEW_SECTION = 1
        private const val OTA_SECTION = 2
        private const val SCAN_DURATION = 30L
        private const val BUSY_TAG = "Bluetooth Presenter Scanning"

        // indices into textvalues
        const val DEV_NAME = 0
        const val DEV_ADDRESS = 1
        const val DEV_BARS = 2
        const val DEV_RSSI = 3

    }

    open val requiredPermissions = listOf(AppPermission.LOCATION_IN_USE, AppPermission.BLUETOOTH)

    // required location status
    open val locationStatus = LocationProvider.AuthorisationStatus.WhileInUse

    open val scanDuration = SCAN_DURATION

    open val knownDevices: MutableMap<String, BleScanResult> = TreeMap(String.CASE_INSENSITIVE_ORDER)

    open val updatable: MutableMap<String, BleScanResult> = TreeMap(String.CASE_INSENSITIVE_ORDER)
    open val newDevices: MutableMap<String, BleScanResult> = TreeMap(String.CASE_INSENSITIVE_ORDER)

    open fun isOneOfOurs(scanResult: BleScanResult): Boolean = false

    protected var scanDisposable: Disposable = EmptyDisposable.INSTANCE


    protected open val activeSection = addSection("Active ${typeName} adapters", Observable.defer {
        Observable.fromIterable(knownDevices.values)
    })
    protected open val newSection = addSection("New ${typeName} adapters", Observable.defer {
        Observable.fromIterable(newDevices.values)
    })
    private val otaSection = addSection("Firmware upgrade devices", Observable.defer {
        Observable.fromIterable(updatable.values)
    })


    /**
     * Start a Bluetooth scan and update as we go
     */
    override fun startRefresh() {
        disposables.remove(scanDisposable)
        updatable.clear()
        newDevices.clear()
        scanDisposable =
            LocationProvider.deviceProvider
                .askPermission(
                    "Location permission is required to scan for Bluetooth devices",
                    locationStatus
                )
                .flatMap {
                    AppPermission.requestPermissions(requiredPermissions)
                }
                .filter {
                    val enabled = it.size == requiredPermissions.size
                    if (!enabled)
                        UiAction.toast("Bluetooth scanning requires Bluetooth and Location permissions")
                    enabled
                }
                .flatMapObservable { BleScanner().bleScan.take(scanDuration, TimeUnit.SECONDS) }
                .updateUI({
                    refresh()
                    rangeChanged(ChangeReason.BUSY)
                }) {
                    rangeChanged(ChangeReason.IDLE)
                    refresh()
                }
                .observeOnMainBy(
                    onError =
                    {
                        UiAction.toast(it)
                    }, onNext = ::addDevice
                )
        disposables.add(scanDisposable)
    }

    override fun endRefresh() {
        scanDisposable.dispose()
        super.endRefresh()
    }

    protected open fun addDevice(result: BleScanResult) {
        when {
            knownDevices.containsKey(result.address) -> {
                newDevices.remove(result.address)
                updatable.remove(result.address)
                knownDevices.put(result.address, result)
            }
            isOneOfOurs(result) -> {
                newDevices.put(result.address, result)
                updatable.remove(result.address)
            }
            result.type == BleDevice.Type.OTA_DFU -> {
                newDevices.remove(result.address)
                knownDevices.remove(result.address)
                updatable.put(result.address, result)
            }
            else -> return
        }
        refresh()
    }

    override fun textValues(item: BleScanResult): List<String> {
        return listOf(
            item.name,
            item.address,
            BleScanResult.signalBarNames[item.signal],
            item.signalText
        )
    }


    override fun compareContents(item1: BleScanResult?, item2: BleScanResult?): Boolean {
        if (item1 == null && item2 == null)
            return true
        if (item1 == null || item2 == null)
            return false
        return item1 == item2
    }

    override val keySelector: (BleScanResult) -> String
        get() = { it.address }
}

