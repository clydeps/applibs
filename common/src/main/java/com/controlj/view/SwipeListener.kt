package com.controlj.view

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 16/5/18
 * Time: 09:39
 */
interface SwipeListener {
    enum class Direction {
        Up,
        Down,
        Left,
        Right;

        companion object {

            /**
             * Returns a direction given an angle.
             * Directions are defined as follows:
             *
             * Up: 45-135
             * Right: 0-45 and 315-360]
             * Down: 225-315
             * Left: 135-225
             *
             * @param angle an angle from 0 to 360 - e
             * @return the direction of an angle
             */
            operator fun get(angle: Double): Direction {
                return if (inRange(angle, 45f, 135f)) {
                    Up
                } else if (inRange(angle, 0f, 45f) || inRange(angle, 315f, 360f)) {
                    Right
                } else if (inRange(angle, 225f, 315f)) {
                    Down
                } else {
                    Left
                }

            }

            /**
             * @param angle an angle
             * @param init the initial bound
             * @param end the final bound
             * @return returns true if the given angle is in the interval [init, end).
             */
            private fun inRange(angle: Double, init: Float, end: Float): Boolean {
                return angle >= init && angle < end
            }
        }
    }

    /**
     * Handle a swipe. Return true if it was handled
     */
    fun onSwipe(direction: Direction): Boolean
}
