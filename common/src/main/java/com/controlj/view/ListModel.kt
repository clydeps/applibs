package com.controlj.view

import com.controlj.viewmodel.ViewModel
import com.google.gson.annotations.Expose

/**
 * A class to store state for a view.
 */
open class ListModel : ViewModel {
    /**
     *
     * Identify the sections that are currently collapsed
     */
    @Expose
    var collapsedList = listOf<String>()
    /**
     * The sort order.
     */
    @Expose
    open val sortOrder = ViewModel.IntData()
    /**
     * Should the sort be descending?
     */
    @Expose
    open val sortDescending = ViewModel.BooleanData(false)
    /**
     * The key for the currently selected data item
     */
    @Expose
    val selectedKey = ViewModel.StringData()
    /**
     * The set of currently selected items, by key
     */
    @Expose
    var checkedSet = mutableSetOf<String>()
}