package com.controlj.view

import com.controlj.framework.ApplicationState
import com.controlj.framework.BackgroundServiceProvider
import com.controlj.graphics.TextStyle
import com.controlj.settings.DataStore
import com.controlj.ui.ButtonDialogItem
import com.controlj.ui.CheckboxItem
import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.utility.secsNow
import io.reactivex.rxjava3.core.Single

/**
 * Provide a means of checking the presence of a permission, and a dialog to navigate
 * system settings to change that permission.
 */
enum class AppPermission(val description: String) {

    LOCATION_IN_USE("Location access when using app"),
    LOCATION_ALWAYS("Location access always"),
    BLUETOOTH("Access Bluetooth devices");

    companion object {
        /**
         * Create and show dialog to ask for permissions
         * Don't show if the user has said not to bug them
         */

        private const val DONT_SHOW = "noAskPermission"
        private const val NEXT_ASK = "lastAskedPermission"
        private const val INTERVAL_SECS = 60

        private val dismissButton = ButtonDialogItem("Dismiss")
        private val dontShowAgain = CheckboxItem("Don't show this dialog again").apply {
            action = { dismissButton.text = if (isChecked) "Dismiss" else "Not now"; false }
        }
        private val style = TextStyle.LEFT_ALIGNED.from(fontStyle = TextStyle.FontStyle.Bold)
        private val openSettingsButton = ButtonDialogItem("Settings")
        private val infoItem =
            DialogItem("These permissions must be be granted in System Settings. Without these permissions some app features will not function correctly. Click the Settings button to open System Settings now.")


        /**
         * Query what permissions are granted from the given list. Return the ones that are granted.
         */
        private fun BackgroundServiceProvider.queryPermissions(
            requested: List<AppPermission>
        ): Single<List<AppPermission>> {
            return Single.concat(requested.map { permission ->
                isPermitted(permission).map { permission to it }
            })
                .filter { it.second }
                .map { it.first }
                .toList()
        }

        /**
         * Check for the required permissions. First check if all are already granted.
         * If not, ask the user to grant the given list of permissions.
         * Allows the user to request no further prompts if denied.
         * Will only ask again once per INTERVAL
         */
        fun requestPermissions(requested: List<AppPermission>): Single<List<AppPermission>> {

            return DataStore {
                BackgroundServiceProvider().queryPermissions(requested).flatMap { granted ->
                    val required = requested - granted
                    when {
                        // if all permissions are granted, reset the "don't ask" flag and delay
                        required.isEmpty() -> {
                            putBoolean(DONT_SHOW, false)
                            putLong(NEXT_ASK, 0)
                            Single.just(granted)
                        }
                        !ApplicationState.current.value.visible ||              // can't ask in background
                            getBoolean(DONT_SHOW) -> Single.just(granted)       // The user doesn't want to be asked again.
                        getLong(NEXT_ASK) >= secsNow -> Single.just(granted)    // don't be a pest
                        else -> {                                               // beg permissions
                            dontShowAgain.isChecked = false
                            dismissButton.text = "Not now"
                            DialogData(
                                DialogData.Type.MODAL,
                                "Required App Permissions",
                                openSettingsButton,
                                dismissButton,
                            )
                                .apply {
                                    addItem(infoItem)
                                    required.forEach { permission ->
                                        addItem(
                                            DialogItem(
                                                "\u2022 ${permission.description}",
                                                textStyle = style
                                            )
                                        )
                                    }
                                    addItem(dontShowAgain)
                                }
                                .singleObserver
                                .doOnSuccess {
                                    putLong(NEXT_ASK, secsNow + INTERVAL_SECS)
                                    if (it == dismissButton)
                                        putBoolean(DONT_SHOW, dontShowAgain.isChecked)
                                    else
                                        ForegroundServiceProvider.instance?.openAppSettings()
                                }
                        }
                    }
                }
                    .flatMap { BackgroundServiceProvider().queryPermissions(requested) }
            }
        }
    }
}
