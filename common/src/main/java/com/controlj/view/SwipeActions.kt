package com.controlj.view

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 16/5/18
 * Time: 09:50
 */
class SwipeActions(var vertical: Boolean, private val action1: () -> Unit, private val action2: () -> Unit) : SwipeListener {
    override fun onSwipe(direction: SwipeListener.Direction): Boolean {
        when {
            vertical -> when (direction) {
                SwipeListener.Direction.Up -> {
                    action2()
                    return true
                }
                SwipeListener.Direction.Down -> {
                    action1()
                    return true
                }
                else -> return false
            }
            else -> when (direction) {
                SwipeListener.Direction.Left -> {
                    action2()
                    return true
                }
                SwipeListener.Direction.Right -> {
                    action1()
                    return true
                }
                else -> return false
            }
        }
    }
}
