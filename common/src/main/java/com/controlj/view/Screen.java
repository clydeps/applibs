package com.controlj.view;


import io.reactivex.rxjava3.core.Observable;

/**
 * Created by clyde on 6/11/17.
 *
 * This interface will typically be implemented by a UIViewController on iOS and an Activity or Fragment on Android.
 * It has a lifecycle and a view
 */

public interface Screen extends ViewController {

    enum State {
        INITIAL,        // Screen has not been created/loaded
        CREATED,        // Android - after onCreate - iOS  - after ViewDidLoad
        FOREGROUND,     // running in foreground, i.e. accepting input
        BACKGROUND,     // possibly visible, but not accepting input
        STOPPED,        // in background, not visible
        DESTROYED       // destroyed/unloaded
    }

    Observable<State> getStates();
}
