package com.controlj.view

import com.controlj.graphics.CRect
import com.controlj.logging.CJLog.debug
import com.controlj.settings.DataStore
import java.util.Stack
import kotlin.math.roundToInt

/**
 * Created by clyde on 15/4/18.
 *
 * A way of displaying 3 views in a container. The main view occupies the middle section, the secondary views occupy
 * positions on opposite sides. The secondary views can be swiped across/up/down to cover the main view and swiped away
 */
class SlideViewPresenter(val view: View, var edges: Edges, val leftFrac: Double, val rightFrac: Double) {

    var skinnyRatio = 0.3           // if the aspect ratio is less than this, show only the main window.
    // choose which edges the secondary views are displayed on
    enum class Edges {
        None,
        LeftRight,
        TopBottom,
        LongEdges,
        ShortEdges
    }

    enum class State(val mainVisible: Boolean = false) {
        Normal(true),         // main view and both side views showing
        FirstOpen,      // First view expanded, second view showing
        SecondOpen,     // second view expanded, first view showing
        FirstFull,      // first view expanded to full view, others hidden
        SecondFull;      // second view expanded to full view, others hidden
    }

    /**
     * The view we are presenting must have this interface to allow it to be controlled
     */
    interface View {
        fun doLayout()
        val wantFullScreen: Boolean
    }

    /**
     * An interface to communicate state changes
     */
    interface Listener {
        /**
         * The controller should call interested parties when state or visibility of
         * the view changes.
         */
        fun onStateChange(state: State, stackSize: Int)
    }

    var listener: Listener? = null

    var state: State = State.values()[DataStore().getInt(SLIDEVIEW_STATE, State.Normal.ordinal)]
        set(value) {
            if (field != value) {
                field = value
                view.doLayout()
                DataStore().putInt(SLIDEVIEW_STATE, value.ordinal)
            }
            listener?.onStateChange(state, stateStack.size)
        }
    val stateStack = Stack<State>()
    var childRects = Array(3) { CRect() }
    var actualEdges: Edges = edges

    // the listeners for swipe actions
    val swipeListeners: Array<SwipeListener> = arrayOf(
            SwipeActions(true,
                    {
                        when (state) {
                            State.FirstOpen -> state = State.FirstFull
                            State.Normal, State.SecondOpen -> state = State.FirstOpen
                            else -> {
                            }
                        }
                    },
                    {
                        when (state) {
                            State.FirstFull -> state = State.FirstOpen
                            State.FirstOpen -> state = State.Normal
                            else -> {
                            }
                        }
                    }),
            SwipeActions(true,
                    {
                        //debug("action 1")
                        when (state) {
                            State.SecondFull -> state = State.SecondOpen
                            State.SecondOpen -> state = State.Normal
                            else -> {
                            }
                        }
                    },
                    {
                        //debug("action 2")
                        when (state) {
                            State.SecondOpen -> state = State.SecondFull
                            State.Normal, State.FirstOpen -> state = State.SecondOpen
                            else -> {
                            }
                        }
                    }
            )

    )

    fun pushState() {
        stateStack.push(state)
        debug("statstack size now ${stateStack.size}")
    }

    fun popState() {
        if (!stateStack.empty())
            state = stateStack.pop()
    }

    fun onMeasure(measuredWidth: Int, measuredHeight: Int) {
        when (edges) {
            Edges.LeftRight -> actualEdges = Edges.LeftRight
            Edges.TopBottom -> actualEdges = Edges.TopBottom

            Edges.LongEdges -> actualEdges = if (measuredHeight > measuredWidth) Edges.LeftRight else Edges.TopBottom
            Edges.ShortEdges, Edges.None -> actualEdges = if (measuredHeight < measuredWidth) Edges.LeftRight else Edges.TopBottom
        }
        val aspectRatio = measuredWidth.toDouble() / measuredHeight.toDouble()
        val leftActual: Double
        val rightActual: Double
        if (view.wantFullScreen) {
            leftActual = 0.0
            rightActual = 0.0
        } else {
            // check if we are in a skinny state. If so, show only the largest window
            val singleView = aspectRatio < skinnyRatio
            when (state) {
                State.Normal -> {
                    leftActual = if (singleView) 0.0 else leftFrac
                    rightActual = if (singleView) 0.0 else rightFrac
                }
                State.FirstOpen -> {
                    leftActual = if(singleView) 1.0 else 1.0 - rightFrac
                    rightActual = if(singleView) 0.0 else rightFrac
                }
                State.SecondOpen -> {
                    leftActual = if(singleView) 0.0 else leftFrac
                    rightActual = if(singleView) 1.0 else 1.0 - leftFrac
                }
                State.FirstFull -> {
                    leftActual = 1.0
                    rightActual = 0.0
                }
                State.SecondFull -> {
                    leftActual = 0.0
                    rightActual = 1.0
                }
            }
        }
        //debug("state=$state, leftActual=$leftActual, rightActual=$rightActual")
        when (actualEdges) {
            Edges.LeftRight -> {
                swipeListeners.forEach { (it as SwipeActions).vertical = false }
                childRects.forEach {
                    it.bottom = measuredHeight.toDouble()
                    it.top = 0.0
                }
                val leftWidth = (measuredWidth * leftActual).roundToInt().toDouble()
                val rightWidth = measuredWidth - (measuredWidth * rightActual).roundToInt().toDouble()
                childRects[0].left = leftWidth
                childRects[0].right = rightWidth
                childRects[1].left = 0.0
                childRects[1].right = leftWidth
                childRects[2].left = rightWidth
                childRects[2].right = measuredWidth.toDouble()
            }
            else -> {
                swipeListeners.forEach { (it as SwipeActions).vertical = true }
                childRects.forEach {
                    it.right = measuredWidth.toDouble()
                    it.left = 0.0
                }
                val topHeight = (measuredHeight * leftActual).roundToInt().toDouble()
                val bottomHeight = measuredHeight - (measuredHeight * rightActual).roundToInt().toDouble()
                childRects[0].top = topHeight
                childRects[0].bottom = bottomHeight
                childRects[1].top = 0.0
                childRects[1].bottom = topHeight
                childRects[2].top = bottomHeight
                childRects[2].bottom = measuredHeight.toDouble()
            }
        }
    }

    companion object {
        const val ANIMATION_DURATION = 0.5
        const val SLIDEVIEW_STATE = "slideview_state"
    }
}
