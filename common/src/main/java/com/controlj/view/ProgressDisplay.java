package com.controlj.view;

import com.controlj.data.Progress;

/**
 * Created by clyde on 1/7/17.
 */

public interface ProgressDisplay {
    void onProgress(Progress progress);
    void onComplete();
}
