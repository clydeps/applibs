package com.controlj.nmea

import com.controlj.data.Constants.INVALID_DATA

/**
 * @property gpsFix The type of fix available
 * @property baroAltitude Latest barometric altitude in meters
 * @property geoAltitude Latest height above the ellipsoid (GPS altitude)
 * @property satellitesUsed The number of satellites used to generate the fix
 * @property satellitesInView The number of satellites in view
 */
interface GpsStatus {
    val gpsFix: GpsFix
    val baroAltitude: Double
    val geoAltitude: Double
    val satellitesUsed: Int
    val satellitesInView: Int

    /**
     * Convenience builder
     */
    data class of(
        override val gpsFix: GpsFix,
        override val baroAltitude: Double,
        override val geoAltitude: Double,
        override val satellitesUsed: Int,
        override val satellitesInView: Int
    ) : GpsStatus

    /**
     * Mutable class for accumulating data from multiple sources. Do not send this into the wild, use the copy.
     */
    data class Builder(
        override var gpsFix: GpsFix = GpsFix.None,
        override var baroAltitude: Double = INVALID_DATA,
        override var geoAltitude: Double = INVALID_DATA,
        override var satellitesUsed: Int = 0,
        override var satellitesInView: Int = 0
    ) : GpsStatus {
        val copy: GpsStatus get() = of(gpsFix, baroAltitude, geoAltitude, satellitesUsed, satellitesInView)
    }

    companion object {
        val invalid = of(
            GpsFix.None,
            INVALID_DATA,
            INVALID_DATA,
            0,
            0
        )
    }
}
