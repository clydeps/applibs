package com.controlj.nmea

import com.controlj.logging.CJLog.logException
import java.io.IOException
import java.net.DatagramPacket
import java.net.InetAddress
import java.nio.channels.DatagramChannel

class UdpBroadcaster(val port: Int = 4353, address: String = "127.255.255.255") {

    val channel = DatagramChannel.open()
    val socket = channel.socket().also {
        it.reuseAddress = true
        it.broadcast = true
    }
    val inetAddress = InetAddress.getByName(address)

    fun broadcast(data: ByteArray, address: InetAddress = inetAddress, p: Int = port) {
        if(!socket.isClosed) try {
            val datagram = DatagramPacket(data, 0, data.size, address, p)
            socket.send(datagram)
        } catch(ex: IOException) {
            logException(ex)
        }
    }

    fun close() {
        socket.close()
        channel.close()
    }
}
