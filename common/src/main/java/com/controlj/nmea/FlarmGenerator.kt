package com.controlj.nmea

import com.controlj.data.Constants
import com.controlj.data.Location
import com.controlj.data.Position
import com.controlj.data.magneticVariation
import com.controlj.location.FlightStatus
import com.controlj.location.GpsLocation
import com.controlj.location.LocationSource
import com.controlj.location.gpsFix
import com.controlj.location.satellitesUsed
import com.controlj.rx.observeBy
import com.controlj.traffic.TrafficSource
import com.controlj.traffic.TrafficTarget
import com.controlj.utility.Units
import com.controlj.utility.to
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.internal.schedulers.SingleScheduler
import org.threeten.bp.ZoneOffset
import org.threeten.bp.temporal.ChronoField
import kotlin.math.abs
import kotlin.math.roundToInt

/**
 * Generate FLARM sentences by listening to the LocationSource and TrafficSource objects
 */
@ExperimentalUnsignedTypes
object FlarmGenerator {

    internal class LocStamp(location: Location) {
        val timeStamp: String
        val date: String
        val variation: String
        val latitude: String
        val longitude: String

        init {

            val absLat = abs(location.latitude)
            val latDeg = absLat.toInt()
            val absLon = abs(location.longitude)
            val lonDeg = absLon.toInt()
            val magvar = location.magneticVariation
            val absVar = abs(magvar)

            val stamp = location.timestamp.atOffset(ZoneOffset.UTC)
            timeStamp = "%02d%02d%02d".format(
                stamp.get(ChronoField.HOUR_OF_DAY),
                stamp.get(ChronoField.MINUTE_OF_HOUR),
                stamp.get(ChronoField.SECOND_OF_MINUTE),
            )
            date = "%02d%02d%02d".format(
                stamp.get(ChronoField.DAY_OF_MONTH),
                stamp.get(ChronoField.MONTH_OF_YEAR),
                stamp.get(ChronoField.YEAR) % 100,
            )
            variation = "%05.1f,%c".format(absVar, if (magvar < 0) 'E' else 'W')
            latitude = "%02d%05.2f,%c".format(
                latDeg, (absLat - latDeg) * 60, if (location.latitude < 0) 'S' else 'N'
            )
            longitude = "%02d%05.2f,%c".format(
                lonDeg, (absLon - lonDeg) * 60, if (location.longitude > 0) 'E' else 'W',
            )
        }
    }

    private val scheduler = SingleScheduler()

    private fun relativeDistance(position: Position, other: Position?): String {
        if (other == null)
            return ""
        return position.distanceTo(other).roundToInt().toString()
    }

    private fun relativeBearing(location: Location, other: Position?): String {
        if (other == null)
            return ""
        var relB = location.bearingTo(other) - location.track
        if (relB > 180)
            relB -= 360
        else if (relB < -180)
            relB += 360
        return relB.roundToInt().toString()
    }

    private fun relativeAltitude(location: Location, other: Location?): String {
        if (other == null)
            return ""
        return (other.altitude - location.altitude).roundToInt().toString()
    }

    internal fun gpgga(location: Location, locStamp: LocStamp = LocStamp(location)): String {
        /*
       * $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh
       *
       * Field Number:
       *  1) Universal Time Coordinated (UTC)   --Ignoring milliseconds
       *  2) Latitude
       *  3) N or S (North or South)
       *  4) Longitude
       *  5) E or W (East or West)
       *  6) GPS Quality Indicator,
       *     0 - fix not available,
       *     1 - GPS fix,
       *     2 - Differential GPS fix
       *     (values above 2 are 2.3 features)
       *     3 = PPS fix
       *     4 = Real Time Kinematic
       *     5 = Float RTK
       *     6 = estimated (dead reckoning)
       *     7 = Manual input mode
       *     8 = Simulation mode
       *  7) Number of satellites in view, 00 - 12
       *  8) Horizontal Dilution of precision (meters)
       *  9) Antenna Altitude above/below mean-sea-level (geoid) (in meters)
       * 10) Units of antenna altitude, meters
       * 11) Geoidal separation, the difference between the WGS-84 earth
       *     ellipsoid and mean-sea-level (geoid), "-" means mean-sea-level
       *     below ellipsoid
       * 12) Units of geoidal separation, meters
       * 13) Age of differential GPS data, time in seconds since last SC104
       *     type 1 or 9 update, null field when DGPS is not used
       * 14) Differential reference station ID, 0000-1023
       * 15) Checksum
       */
        return "GPGGA,%s,%s,%s,%d,%02d,%.1f,%.1f,M,,M,,".format(
            locStamp.timeStamp,
            locStamp.latitude,
            locStamp.longitude,
            if (location.gpsFix >= GpsFix.ThreeD) 1 else 0,
            location.satellitesUsed,
            location.accuracyH / 5.0,       // estimate HDOP
            location.altitude,
        ).checkSummed()
    }

    fun pflau(location: Location, alert: TrafficTarget?, count: Int): String {
        return "PFLAU,%d,1,%d,%d,%d,%s,%d,%s,%s,%s".format(
            count,      // number of targets
            when {                  // inflight/gps status
                !location.valid -> 0
                location.isInFlight -> 2
                else -> 1
            },
            if (FlightStatus.isBatteryLow) 0 else 1,  // battery status
            if (alert?.isHazard == true) 1 else 0,
            relativeBearing(location, alert),
            if (alert == null) 0 else 2,         // alarm type (aircraft == 2)
            relativeAltitude(location, alert),
            relativeDistance(location, alert),
            alert?.address?.address?.toString(16)?.uppercase()?.padStart(6, '0') ?: ""

        ).checkSummed()
        //PFLAU,<RX>,<TX>,<GPS>,<Power>,<AlarmLevel>,<RelativeBearing>, <AlarmType>,<RelativeVertical>,<RelativeDistance>,<ID>
    }


    private fun alarmLevel(target: TrafficTarget): Int {
        if (target.isHazard)     // TODO - calculate time to impact
            return 1
        return 0
    }

    private fun pflaa(location: Location, target: TrafficTarget): String {
        //PFLAA,<AlarmLevel>,<RelativeNorth>,<RelativeEast>, <RelativeVertical>,<IDType>,<ID>,<Track>,<TurnRate>,<GroundSpeed>, <ClimbRate>,<AcftType>
        // can't return data if we have no position
        //println("processing $target")
        return "PFLAA,%d,%d,%d,%d,1,%06X,%.1f,,%.1f,%s,%X".format(
            alarmLevel(target),
            location.northing(target).roundToInt(),
            location.easting(target).roundToInt(),
            (target.altitude - location.altitude).roundToInt(),
            target.address.address,
            target.track,
            target.speed,
            if (target.verticalSpeed == Constants.INVALID_DATA) "" else "%.1f".format(target.verticalSpeed),
            target.emitterCategory.flarmType,
        ).checkSummed()
    }

    private fun gprmc(location: Location, locStamp: LocStamp = LocStamp(location)): String {
        return "GPRMC,%s,%c,%s,%s,%05.1f,%05.1f,%s,%s".format(
            locStamp.timeStamp,
            if (location.valid) 'A' else 'V',
            locStamp.latitude,
            locStamp.longitude,
            (location.speed to Units.Unit.KNOT),
            location.track,
            locStamp.date,
            locStamp.variation
        ).checkSummed()
    }

    private fun pgrmz(status: GpsStatus): String {
        return "PGRMZ,%d,F,2".format(
            (status.baroAltitude to Units.Unit.FOOT).roundToInt()
        ).checkSummed()
    }

    val observer: Observable<String> = Observable.create { emitter: ObservableEmitter<String> ->
        val disp1 = LocationSource.observer
            .subscribeOn(scheduler)
            .observeBy {
                emitter.onNext(gpgga(it))
                emitter.onNext(gprmc(it))
                if (it is GpsLocation)
                    emitter.onNext(pgrmz(it.status))
            }
        val disp2 = TrafficSource.observer
            .subscribeOn(scheduler)
            .observeBy { targets ->
                val location = LocationSource.lastLocation
                targets.forEach { emitter.onNext(pflaa(location, it)) }
                targets.firstOrNull()?.let {
                    emitter.onNext(pflau(location, it, targets.size))
                }
            }
        emitter.setCancellable {
            disp1.dispose()
            disp2.dispose()
        }
    }
        .subscribeOn(scheduler)
        .share()
}
