package com.controlj.nmea;

import java.util.List;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 * User: clyde
 * Date: 24/10/2014
 * Time: 10:05 AM
 */
public class Nmea {

	String verb;
	String[] fields;

	public Nmea(String verb, List<String> fields) {
		this.verb = verb;
		this.fields = (String[])fields.toArray();
	}

	public Nmea(String buffer) throws IllegalArgumentException {

		buffer = buffer.trim();
		if(buffer.length() < 9 || !buffer.startsWith("$"))
			throw new IllegalArgumentException("Invalid NMEA string");
		int len = buffer.length();
		if(buffer.charAt(len - 3) != '*')
			throw new IllegalArgumentException("Missing '*'");
		int check = getCheckSum(buffer.substring(1, len - 4));
		int cval = Integer.parseInt(buffer.substring(len - 2, len), 16);
		if(check != cval)
			throw new IllegalArgumentException("Bad checksum");
		verb = buffer.substring(1, 6);
		fields = buffer.substring(6, len - 4).split(",");
	}

	private int getCheckSum(String buffer) {
		int sum = 0;
		int len = buffer.length();
		while(len-- != 0)
			sum ^= buffer.charAt(len) & 0xFF;
		return sum;
	}

}
