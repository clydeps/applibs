package com.controlj.nmea

enum class GpsFix {
    None,
    TwoD {
        override val string = "2D"
    },
    ThreeD {
        override val string: String = "3D"
    },
    Waas,
    DGPS,
    RTK
    ;

    open val string: String = name
}

