package com.controlj.nmea


/**
 * Given an nmea sentence, checksum it and add the initial $ and trailing * with checksum
 */

fun String.checkSummed(): String {
    val checksum = fold(0) { acc, chr -> acc xor chr.code }
    return "\$%s*%02X\r\n".format(this, checksum and 0xFF)
}
