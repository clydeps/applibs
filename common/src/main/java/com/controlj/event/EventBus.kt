package com.controlj.event


import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import java.util.concurrent.TimeUnit

/**
 * Created by clydestubbs on 28/05/2017.
 *
 * Allows app-wide events to be posted to and received.
 */

object EventBus {
    data class Event(val action: Any, val data: Any? = null, val silent: Boolean = false)

    private var eventEmitter: ObservableEmitter<Event> = DisposedEmitter()
    val eventObservable: Observable<Event> by lazy {
        Observable.create { emitter: ObservableEmitter<Event> ->
            eventEmitter = emitter
        }.observeOn(MainScheduler()).share()
    }

    operator fun invoke() = eventObservable

    /**
     * post an event to the queue
     *
     * @param event
     */
    fun postEvent(event: Event) {
        if (!event.silent)
            logMsg("Posted event ${event.action}")
        eventEmitter.onNext(event)
    }

    /**
     * post an event to the queue with a delay in ms.
     *
     * @param event
     * @param delay delay time in ms
     */
    @Suppress("CheckResult")
    fun postEvent(event: Event, delay: Long) {
        Observable.timer(delay, TimeUnit.MILLISECONDS).subscribe { postEvent(event) }
    }

    fun postEvent(action: Any) {
        postEvent(Event(action))
    }

    fun postEvent(action: Any, delay: Long) {
        postEvent(Event(action), delay)
    }

    fun postEvent(action: Any, argument: Any?) {
        postEvent(Event(action, argument))
    }
}
