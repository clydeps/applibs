package com.controlj.database

import com.controlj.framework.BackgroundServiceProvider
import com.controlj.framework.FilePaths
import com.controlj.logging.CJLog.debug
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.settings.Properties
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import org.ktorm.database.Database
import org.ktorm.database.SqlDialect
import org.ktorm.schema.BaseTable
import org.ktorm.schema.Column
import org.ktorm.schema.SqlType
import org.ktorm.support.sqlite.SQLiteDialect
import org.threeten.bp.Instant
import java.io.Closeable
import java.io.File
import java.sql.Connection
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Types
import java.util.concurrent.Executors

/**
 * PRovides the interface to a SQLite database via JDBC
 * @param dbName The name of the database file, relative to the database directory.
 * @param init An initialisation block to be run before the file is opened. This can be used
 * e.g. to copy an initial file from the assets folder, or update the file.
 */
open class CJDatabase(private val dbName: String, init: (CJDatabase) -> Unit = {}) : Closeable {
    // the name of the driver class
    open val driverClass: String get() = Properties.getProperty(DRIVER_CLASS_PROPERTY)

    // the url prefix to use for the JDBC connection
    open val jdbcUrl: String
        get() {
            val result = Properties.getProperty(DRIVER_URL_PROPERTY)
            if (result.isBlank())
                return "jdbc:sqlite"
            return result
        }

    open val dialect: SqlDialect = SQLiteDialect()

    // the database file itself
    private val dbFile: File get() = File(FilePaths().databaseDirectory, dbName)

    lateinit var conn: Connection
        private set
    lateinit var db: Database
        private set

    val isReady: Boolean get() = ::db.isInitialized

    /**
     * Metadata to create and update the schema
     */

    /**
     * Create and update statements to move from one version to another. The index of each list + 1 is the version number that will
     * result from the update.
     */

    open val updateStatements: List<List<String>> = listOf()

    /**
     * The latest version of the database.
     * Version 0 indicates the database is created externally.
     * Version 1 is the base version - there are create statements but no update statements
     * Higher versions indicate there are multiple steps from the base version to the latest version
     */
    val latestVersion: Int get() = updateStatements.size

    /**
     * A single threaded scheduler to run database operations on.
     */
    val scheduler = Schedulers.from(Executors.newSingleThreadExecutor { Thread(it, dbName) })

    /**
     * Initialisation - run as a background task on our private scheduler.
     */
    init {
        BackgroundServiceProvider.runBackground(dbName, {
            init(this)
            setup()
        }, scheduler = scheduler, onceOnly = true)
    }

    override fun close() {
        conn.close()
    }

    /**
     * Run a query on the database and return the results as an observable stream
     */

    fun <T : Any> observe(selector: Database.() -> List<T>): Observable<T> {
        return Observable.defer {
            Observable.fromIterable(db.selector())
        }.subscribeOn(scheduler)
    }

    /**
     * Run a query on the database and return the single result
     */

    fun <T : Any> single(selector: Database.() -> T): Single<T> {
        return Single.fromCallable { db.selector() }.subscribeOn(scheduler)
    }

    /**
     * Setup and/or upgrade the database
     */

    private fun setup() {
        try {
            if (driverClass.isNotBlank())
                Class.forName(driverClass)
            val connUrl = "$jdbcUrl:${dbFile.path}"
            debug("Getting jdbc connection: $connUrl")
            conn = DriverManager.getConnection(connUrl)
            debug("calling Database.connect")
            db = Database.connect(logger = KtormLogger, dialect = dialect) {
                object : Connection by conn {
                    override fun close() {
                        // leave open
                    }
                }
            }
            createDatabase()
        } catch (ex: Exception) {
            logException(ex)
        }
    }

    private fun getDatabaseVersion(): Int {
        try {
            db.useConnection { conn ->
                conn.createStatement().use { statement ->
                    val resultSet =
                        statement.executeQuery("select $META_VERSION from $META_TABLE order by $META_VERSION desc limit 1")
                    resultSet.next()
                    return resultSet.getInt(META_VERSION).coerceAtLeast(1).also {
                        debug("Database version currently $it")
                    }
                }
            }
        } catch (ex: Exception) {
            debug(ex.toString())
            return 0
        }
    }

    private fun createDatabase() {
        val targetVersion = latestVersion
        val currentVersion = getDatabaseVersion()
        debug("Create database $dbName version $latestVersion, currentVersion is $currentVersion")
        if (currentVersion < targetVersion) {
            db.useConnection { conn ->
                conn.createStatement().use { statement ->
                    updateStatements.takeLast(targetVersion - currentVersion).forEach { list ->
                        list.forEach { sql ->
                            try {
                                statement.executeUpdate(sql)
                            } catch (ex: Exception) {
                                logMsg("Was trying $sql")
                                logException(ex)
                            }
                        }
                    }
                    if (currentVersion == 0)
                        statement.executeUpdate(META_CREATE)
                    statement.executeUpdate("insert into $META_TABLE ($META_VERSION) values($targetVersion)")
                }
                logMsg("Updated database ${conn.metaData.url} from version $currentVersion to $targetVersion")
            }
        } else logMsg("Database already at version $currentVersion")
    }

    companion object {
        const val DRIVER_CLASS_PROPERTY = "jdbc.driver.classname"
        const val DRIVER_URL_PROPERTY = "jdbc.driver.url"
        const val META_TABLE = "metadata"       // the metadata table name
        const val META_VERSION = "version"
        const val META_CREATE =
            "create table if not exists $META_TABLE ($META_VERSION integer primary key, Timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)"
        val directory: File get() = FilePaths().databaseDirectory.also { it.mkdirs() }
    }
}

/**
 * Define a column typed of [InstantSqlType].
 */
fun BaseTable<*>.timestamp(name: String): Column<Instant> {
    return registerColumn(name, InstantSqlType)
}

/**
 * [SqlType] implementation represents `timestamp` SQL type.
 */
object InstantSqlType : SqlType<Instant>(Types.VARCHAR, "timestamp") {
    override fun doSetParameter(ps: PreparedStatement, index: Int, parameter: Instant) {
        ps.setString(index, parameter.toString())
    }

    override fun doGetResult(rs: ResultSet, index: Int): Instant? {
        return Instant.parse(rs.getString(index))
    }
}

