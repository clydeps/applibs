package com.controlj.database

import com.controlj.logging.CJLog
import com.controlj.logging.CJLog.logException
import org.ktorm.logging.Logger

object KtormLogger : Logger {
    override fun debug(msg: String, e: Throwable?) {
        CJLog.debug(msg)
        e?.let { logException(it) }
    }

    override fun error(msg: String, e: Throwable?) {
        CJLog.logMsg(msg)
        e?.let { logException(it) }
    }

    override fun info(msg: String, e: Throwable?) = error(msg, e)

    override fun isDebugEnabled(): Boolean = false

    override fun isErrorEnabled(): Boolean = true

    override fun isInfoEnabled(): Boolean = true

    override fun isTraceEnabled(): Boolean = false

    override fun isWarnEnabled(): Boolean = false

    override fun trace(msg: String, e: Throwable?) = error(msg, e)

    override fun warn(msg: String, e: Throwable?) = error(msg, e)
}
