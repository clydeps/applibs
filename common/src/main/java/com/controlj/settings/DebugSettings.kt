package com.controlj.settings

/**
 * Created by clyde on 23/8/18.
 */
object DebugSettings {
    var debugClickCount = 0
    val showDebugFeatures = BooleanSetting("show_debug", "Show debug features", defaultValue = false)
    val crashReports = BooleanSetting("crash_reports", "Send crash reports", defaultValue = true,
            description = "Crash reports help the developer fix bugs")
    val debugFeatures = object : BooleanSetting("debugfeatures", "Enable debug features", defaultValue = false,
            description = "For diagnostic purposes only") {
        override var isEnabled: Boolean
            get() = showDebugFeatures.value
            set(value) {
                showDebugFeatures.value = value
            }
    }

    init {
        crashReports.observable.subscribe {
            if (++debugClickCount == 4) {
                showDebugFeatures.value = true
                showDebugFeatures.saveValue()
            }
        }
    }
}
