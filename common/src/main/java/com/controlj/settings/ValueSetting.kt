package com.controlj.settings

import com.controlj.utility.Units

/**
 * Created by clyde on 6/7/18.
 */
open class ValueSetting(
    key: String,
    label: String,
    description: String,
    val units: List<Units.Unit>,
    override val defaultValue: Double = 0.0
) : Setting<Double>(key, label, description) {
    override var value: Double = 0.0
        set(value) {
            if (field != value) {
                field = value
                changed = true
            }
        }
    var currentUnit: Units.Unit = units.first()
    val unitKey by lazy { storeKey + "_unit" }

    override fun load(store: DataStore) {
        currentUnit = if (datastore.contains(unitKey)) Units.getUnit(datastore.getString(unitKey)) else units.first()
        value = if (datastore.contains(storeKey)) datastore.getDouble(storeKey) else defaultValue
    }

    override fun save(store: DataStore) {
        datastore.putString(unitKey, currentUnit.unitName)
        datastore.putDouble(storeKey, value)
    }

    init {
        loadValue()
    }
}
