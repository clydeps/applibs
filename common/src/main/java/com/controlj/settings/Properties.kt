package com.controlj.settings

import com.controlj.framework.FilePaths
import com.controlj.logging.CJLog.logMsg
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-07
 * Time: 10:19
 */
object Properties {

    internal const val PROPERTIES_FILE = "properties.json"
    internal const val LOCAL_FILE = "local_properties.json"

    private val reload get() = PROPERTIES_FILE.readProperties() + LOCAL_FILE.readProperties()

    internal fun reset() {
        map = reload
    }

    var map: Map<String, String> = reload
        private set(value) {
            field = value
        }

    /**
     * Get the named property
     */

    fun getProperty(name: String, default: String = ""): String {
        return map[name] ?: default
    }

    private fun String.readProperties(): Map<String, String> {
        return try {
            FilePaths().openAsset(this)?.use { stream ->
                Gson().fromJson(
                        stream.reader(),
                        object : TypeToken<Map<String, String>>() {}.type
                )
            } ?: mapOf()
        } catch (ex: Exception) {
            return mapOf()
        }
    }
}
