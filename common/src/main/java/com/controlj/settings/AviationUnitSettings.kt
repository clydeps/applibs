package com.controlj.settings

import com.controlj.utility.Units

/**
 * Created by clyde on 21/8/18.
 */
object AviationUnitSettings {
    val distanceUnitSetting = UnitSetting("distance", "Distance", "Default distance unit",
            listOf(Units.Unit.NM, Units.Unit.SM, Units.Unit.KM))
    val vertSpeedUnitSetting = UnitSetting("vspeed", "Vertical speed", "Default vertical speed unit",
            listOf(Units.Unit.FPM, Units.Unit.KNOT, Units.Unit.MPS))
    val speedUnitSetting = UnitSetting("speed", "Speed", "Default speed unit",
            listOf(Units.Unit.KNOT, Units.Unit.MPH, Units.Unit.KMH))
    val altitudeUnitSetting = UnitSetting("altitude", "Altitude", "Default altitude unit",
            listOf(Units.Unit.FOOT, Units.Unit.METER))
    val weightUnitSetting = UnitSetting("weight", "Weight", "Default weight unit",
            listOf(Units.Unit.KG, Units.Unit.LB))
    val timeUnitSetting = UnitSetting("time", "Time", "Default time unit",
            listOf(Units.Unit.MINS, Units.Unit.HRS))
    val latLonSetting = UnitSetting("latlonformat", "Coordinates",
            "Format to display latitude and longitude",
            listOf(Units.Unit.DEGMIN, Units.Unit.DEGREE_WORD, Units.Unit.DEGMINSEC))
    val oatUnitSetting = UnitSetting("oat", "OAT", "Outside Air Temperature unit",
            listOf(Units.Unit.CELSIUS, Units.Unit.FARENHEIT))
    val fuelTypeSetting = UnitSetting("fueltype", "Fuel type", "Used for W&B calculations",
            listOf(Units.Unit.AVGAS, Units.Unit.ULP, Units.Unit.AVTUR))
    val fuelUnitSetting = UnitSetting("fuelQuantity", "Fuel quantity", "Fuel quantity unit",
            listOf(Units.Unit.USGAL, Units.Unit.LITER, Units.Unit.IMPGAL))
    val transitionLevelSetting = IntSetting("transLevel", "Transition Level", "Lower boundary of flight levels in hundreds of feet", defaultValue = 180)
    val transitionAltitudeSetting = ValueSetting("transAltitude", "Transition Altitude", "Upper boundary of non-flight level airspace",
            listOf(Units.Unit.FOOT, Units.Unit.METER), Units.Unit.FOOT.convertFrom(18000.0))

    val group = Setting.Group("Aviation Units",
            distanceUnitSetting, speedUnitSetting, altitudeUnitSetting, vertSpeedUnitSetting,
            weightUnitSetting, timeUnitSetting,
            oatUnitSetting, latLonSetting,
            fuelTypeSetting, fuelUnitSetting,
            transitionAltitudeSetting, transitionLevelSetting
    )
    fun fuelFormat(): String {
        return when {
            fuelUnitSetting.value.ratio > 1 -> "%.1f"
            else -> "%.0f"
        }
    }
}
