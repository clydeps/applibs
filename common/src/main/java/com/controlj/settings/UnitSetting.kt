package com.controlj.settings

import com.controlj.utility.Units

/**
 * Created by clyde on 1/9/18.
 *
 * A setting for a unit
 * @param key The storage key to persist this setting's value
 * @param label The label for this setting
 * @param description A longer description
 * @param choices The set of choices for this unit
 */
open class UnitSetting(key: String, label: String, description: String?, choices: List<Units.Unit>) :
    ChoiceSetting<Units.Unit>(key, label, description, choices) {
    companion object {

        /**
         * Create a single-valued non-persistent setting
         */
        fun single(label: String, unit: Units.Unit): UnitSetting {
            return object : UnitSetting("", label, "", listOf(unit)) {
                override fun save(store: DataStore) = Unit
                override fun load(store: DataStore) = Unit
            }
        }

        val identity = single("", Units.Unit.IDENTITY)
    }
}
