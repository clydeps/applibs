package com.controlj.settings

import io.reactivex.rxjava3.core.Observable
import org.ktorm.schema.typeOf
import java.lang.reflect.Type

/**
 * Created by clyde on 9/5/17.
 */

interface DataStore : StringStore {

    companion object {
        private lateinit var instance: DataStore
        operator fun invoke() = instance

        operator fun <T : Any?> invoke(block: DataStore.() -> T): T = instance.run(block)

        val UNITS_DEFAULT_ = "units_default_"
        val UNITS_DEFAULT_VOLUME = UNITS_DEFAULT_ + "volume"
        val DEBUG_FEATURES = "pref_debugfeatures"
    }

    fun init() {
        instance = this
    }

    val allStringEntries: Map<String, String>
    val allEntries: Map<String, *>

    val isDebugFeatures: Boolean

    /**
     * Get an integer value
     *
     * @param key The value's key
     * @return the value, or 0 if key not found
     */
    fun getInt(key: String, dflt: Int = 0): Int

    override fun getString(key: String, dflt: String): String

    fun getLong(key: String, dflt: Long = 0L): Long

    fun putInt(key: String, value: Int)

    fun putFloat(key: String, value: Float)

    override fun putString(key: String, value: String)

    fun putLong(key: String, value: Long)

    fun remove(key: String)

    fun getIntList(key: String): List<Int>

    fun getStringList(key: String): List<String>

    fun getBoolean(name: String): Boolean

    fun putBoolean(key: String, value: Boolean)

    fun putStringList(key: String, values: List<String>)

    fun putIntList(key: String, values: List<Int>)

    operator fun contains(key: String): Boolean

    fun putObject(key: String, o: Any)

    fun <T> getObject(key: String, type: Type): T?

    fun <T> getObject(key: String, clazz: Class<T>): T?

    val observeChanges: Observable<String>

    fun getFloat(key: String, dflt: Float = 0f): Float

    fun putDouble(key: String, value: Double) {
        putLong(key, value.toBits())
    }

    fun getDouble(key: String): Double {
        try {
            return Double.fromBits(getLong(key))
        } catch (cce: ClassCastException) {
            return getFloat(key).toDouble()
        }
    }

    /**
     * Commit the database to disk
     */
    fun commit()
}

inline fun <reified T> DataStore.getObject(key: String): T? {
    return getObject<T>(key, typeOf<T>())
}
