package com.controlj.settings

class EnumSetting<T: Enum<T>>(key: String, label: String, description: String?, choices: List<T>):
        ChoiceSetting<T>(key, label, description, choices) {
}

