package com.controlj.settings

import com.controlj.ui.DialogItem

/**
 * Created by clyde on 6/7/18.
 */
open class DoubleSetting(
    key: String,
    label: String,
    description: String,
    final override val defaultValue: Double = 0.0,
    var decimalDigits: Int = 1
) : Setting<Double>(key, label, description) {
    override var value: Double = defaultValue
        set(value) {
            if (field != value) {
                field = value
                changed = true
            }
        }

    open var style = DialogItem.InputStyle.SIGNEDDECIMAL
    override fun load(store: DataStore) {
        value = if (datastore.contains(storeKey)) datastore.getDouble(storeKey) else defaultValue
    }

    override fun save(store: DataStore) {
        datastore.putDouble(storeKey, value)
    }

    init {
        loadValue()
    }
}
