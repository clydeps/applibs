package com.controlj.settings

import com.controlj.data.Priority

/**
 * A state for a value in a range
 * @param color The color corresponding to the state
 * @param level The level. Higher means more serious
 * @param message A string to be used to construct a message
 */
enum class RangeState(val priority: Priority, val message: String) {
    AlarmLow(Priority.ALARM, "Low"),
    CautionLow(Priority.CAUTION, "Check"),
    Normal(Priority.NORMAL, ""),
    Neutral(Priority.NONE, ""),
    CautionHigh(Priority.CAUTION, "Check"),
    AlarmHigh(Priority.ALARM, "High");

    val color: Int = priority.backgroundColor
}
