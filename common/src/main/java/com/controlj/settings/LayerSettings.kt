package com.controlj.settings

import com.controlj.framework.FilePaths
import com.controlj.logging.CJLog
import com.controlj.mapping.RainViewer
import com.controlj.mapping.RasterTileSource
import com.controlj.mapping.WeatherSource
import com.controlj.ui.DialogData.Companion.OK
import com.controlj.ui.SettingsDialogData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2018-11-27
 * Time: 07:26
 */
object LayerSettings {
    val weatherSources = mutableListOf(
        WeatherSource.empty,
        RainViewer.Radar,
        RainViewer.Satellite
    )

    val trackHistory = BooleanSetting("track_history", "Active track")
    val previousTrack = BooleanSetting("track_previous", "Previous track")
    val weather = ChoiceSetting("weather_layer", "Weather Overlay", "Choose a weather overlay", weatherSources)
    val looping = object: BooleanSetting("weather_looping", "Loop weather overlay", defaultValue = true) {
        override val isEnabled: Boolean
            get() = weather.getTypedValue() != WeatherSource.empty
    }
    val traffic = BooleanSetting("layer_traffic", "Traffic", defaultValue = true)
    val satellite = BooleanSetting("layer_satellite", "Satellite basemap")
    val thermals = BooleanSetting("layer_thermals", "Thermal strength")
    val lightning = BooleanSetting("layer_lightning", "Lightning")
    val threeD = BooleanSetting("threeDAirspace", "3D Airspace")
    val airspaceB = BooleanSetting("layer_airspace_b", "Class B")
    val airspaceC = BooleanSetting("layer_airspace_c", "Class C")
    val airspaceD = BooleanSetting("layer_airspace_d", "Class D")
    val airspaceE = BooleanSetting("layer_airspace_e", "Class E")
    val restricted = BooleanSetting("layer_airspace_r", "Restricted")
    val briefingAreas = BooleanSetting("layer_forecast_areas", "Forecast areas", defaultValue = true)
    val areaFrequencies = BooleanSetting("layer_frequency_areas", "Area frequencies", defaultValue = true)

    val airspaceGroup = Setting.Group("Airspace", threeD, airspaceB, airspaceC, airspaceD, airspaceE, restricted, areaFrequencies, briefingAreas)
    val vors = BooleanSetting("layer_vors", "VORs")
    val ndbs = BooleanSetting("layer_ndbs", "NDBs")
    val ifrWaypoints = BooleanSetting("layer_waypoints_ifr", "IFR waypoints")
    val vfrWaypoints = BooleanSetting("layer_waypoints_vfr", "VFR waypoints")
    val airports = BooleanSetting("layer_airports", "Airports", defaultValue = true)

    val radarGroup = Setting.Group("Layers", trackHistory, previousTrack, weather, looping, satellite, traffic).also {
        if(CJLog.isDebug || Setting.secretSauce.value)
            it.add(thermals)
    }
    val featureGroup = Setting.Group("Points", airports, vors, ndbs, ifrWaypoints, vfrWaypoints)

    val groupList = listOf(radarGroup, airspaceGroup, featureGroup)

    val dialogData: SettingsDialogData
        get() {
            return SettingsDialogData("Map Layers", groupList, false, OK)
        }

    val layerMap by lazy {
        val stream = FilePaths().openAsset("maplayers.json")
        if (stream == null)
            mapOf()
        else
            Gson().fromJson<Map<String, List<String>>>(stream.reader(), object : TypeToken<Map<String, List<String>>>() {}.type)
    }

    /**
     * The layer below which to load the satellite view. Roads, airports etc are on top of this.
     */
    val satelliteBelow: String
        get() = layerMap.get("layer_below")?.first() ?: ""

    val radarBelow = "barrier_line-land-line"
    val cloudsBelow = "barrier_line-land-line"
    val thermalsBelow = "barrier_line-land-line"

    /**
     * Place route and tracks below this layer.
     */
    val routeBelow: String
        get() = layerMap.get("layer_frequency_areas")?.first() ?: ""

}
