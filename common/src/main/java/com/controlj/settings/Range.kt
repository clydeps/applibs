package com.controlj.settings


/**
 * A value range with its associated color. This range corresponds to values from the top of the previous
 * range to value, in the base units of the enclosing [RangeSet].
 */
open class Range(var value: Double, var state: RangeState) {
    open val color = state.color
    override fun toString(): String {
        return "Range(value=$value, state=$state)"

    }
}
