package com.controlj.settings

/**
 * Created by clyde on 6/7/18.
 */
open class ActionSetting(label: String, description: String, val isButton: Boolean = false, val action: () -> Unit) : Setting<Any>("", label, description) {
    override val defaultValue: Any = 0
    override var value: Any = 0
    override val filtered = true

    override fun load(store: DataStore) {}

    override fun save(store: DataStore) {}
}
