package com.controlj.settings

/**
 * Created by clyde on 6/7/18.
 */
open class BooleanSetting(
    key: String,
    label: String,
    description: String = "",
    override val defaultValue: Boolean = false
) : Setting<Boolean>(key, label, description) {


    override var value: Boolean = false
        set(value) {
            if (field != value) {
                field = value
                changed = true
            }
        }

    override fun load(store: DataStore) {
        value = if (datastore.contains(storeKey)) datastore.getBoolean(storeKey) else defaultValue
    }

    override fun save(store: DataStore) {
        datastore.putBoolean(storeKey, value)
    }

    init {
        loadValue()
    }
}
