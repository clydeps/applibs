package com.controlj.settings

/**
 * Created by clyde on 6/7/18.
 */
open class ChoiceSetting<T : Any>(key: String, label: String, description: String?, val choices: List<T>) :
    Setting<T>(key, label, description) {

    private var stringVal: String = datastore.getString(storeKey, choices.first().toString())

    override var value: T
        set(value) {
            val str = value.toString()
            if (stringVal != str) {
                stringVal = str
                changed = true
            }
        }
        get() = choices.firstOrNull { it.toString() == stringVal } ?: choices.first()

    override val defaultValue: T
        get() = choices.first()

    override fun load(store: DataStore) {
        stringVal = store.getString(storeKey, "")
    }

    @Suppress("UNCHECKED_CAST")
    fun getTypedValue(): T {
        return value
    }

    override fun save(store: DataStore) {
        datastore.putString(storeKey, stringVal)
    }

    init {
        loadValue()
    }
}
