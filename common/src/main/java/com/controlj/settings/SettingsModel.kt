package com.controlj.settings

import com.controlj.ui.SettingsDialogData
import com.controlj.viewmodel.DialogListModel

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-04-19
 * Time: 07:51
 *
 * A class to manage settings for a view
 */
class SettingsModel(settingsData: SettingsDialogData) : DialogListModel(settingsData, settingsData.groups)