package com.controlj.settings

import com.controlj.ui.DialogItem

/**
 * Created by clyde on 6/7/18.
 */
open class StringSetting(
    key: String,
    label: String,
    description: String = "",
    override val defaultValue: String = "",
    val charset: Set<Char>? = null
) : Setting<String>(key, label, description) {
    override var value: String = ""
        set(value) {
            if (field != value) {
                field = value
                changed = true
            }
        }
    open var style: DialogItem.InputStyle = DialogItem.InputStyle.NOCORRECT

    override fun load(store: DataStore) {
        value = if (datastore.contains(storeKey)) datastore.getString(storeKey) else defaultValue
    }

    override fun save(store: DataStore) {
        datastore.putString(storeKey, value)
    }

    init {
        loadValue()
    }
}
