package com.controlj.settings

import com.controlj.framework.ApplicationState
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import com.controlj.rx.observeBy
import com.controlj.rx.observeOnMainBy
import com.controlj.ui.UiAction
import com.controlj.viewmodel.ListenableValue
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Single
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 6/7/18.
 * Represents a user setting.
 *
 * @param key The key used to identify the setting in external storage. Should be globally unique
 * @param label The short label for the setting in the UI
 * @param description A longer description for use in the UI
 * @property  filtered If set, this setting will not be included in crash reports etc. Use for passwords and
 * other sensitive information
 * @property value The setting's value
 * @property defaultValue The default value of the setting
 * @property groupIndex Determines the order within a group.
 * @property validator An optional validator to allow checking values
 * @property changed Will be true if this value has been changed. Setting this triggers notification of the change
 * to any listeners.
 * @property valid Is the value valid?
 * @property observable Delivers events when the value is changed.
 * @property isEnabled Is this setting enabled?
 * @property storeKey The setting key prefixed with a standard string, for use in external datastores.
 *
 */
abstract class Setting<T : Any>(
    val key: String,
    open val label: String,
    open val description: String? = null
) :
    Comparable<Any>, ListenableValue<T> {

    open val filtered = false      // set to true to exclude from crash reports
    abstract override var value: T
    abstract val defaultValue: T
    open var groupIndex = 0     // used for sorting - preserves order in group
    var validator: SettingValidator<T>? = null
    var changed = false
        set(value) {
            field = value
            if (value) {
                emitter.onNext(this)
                Setting.emitter.onNext(this)
            }
        }
    open var valid = true
    open val isEnabled: Boolean
        get() = group?.enabled ?: true
    val storeKey = KEY_PREFIX + key
    var group: Group? = null

    private var emitter: ObservableEmitter<in Setting<T>> = DisposedEmitter<Setting<T>>()

    val observable: Observable<out Setting<T>> by lazy {
        Observable.create { e: ObservableEmitter<in Setting<T>> ->
            emitter = e
        }.share()
    }

    /**
     * Implements listener from ListenableValue
     */

    override val listener: Observable<T> = observable.map { it.value }

    /**
     * Allow a setting to read or write with syntax settingObject()
     */
    operator fun invoke(newValue: T? = null): T {
        newValue?.let { value = it }
        return value
    }

    init {
        add(this)
    }

    /**
     * Reset the value to its default
     */
    open fun reset() {
        value = defaultValue
    }

    /**
     * Load the value from the datastore
     */
    fun loadValue() {
        load(datastore)
        changed = false
    }

    /**
     * Save the value, if valid, to the datastore and resets changed to false.
     */

    fun saveValue() {
        if (valid) {
            save(datastore)
            changed = false
        }
    }

    /**
     * Implements the load
     * @param store The datastore to load from. The default is the standard datastore
     */
    protected open fun load(store: DataStore) {
    }

    /**
     * Implements the save
     * @param store The dataStore to save to. The default is the standard datastore
     */
    protected open fun save(store: DataStore) {
    }

    override fun toString(): String {
        return value.toString()
    }

    /**
     * Run the validator on the value
     * @param value The proposed new value
     * @return true if there is no validator, or the value is acceptable
     */
    fun validate(value: T): Boolean {
        return validator?.changePermitted(this, value) ?: true
    }

    override fun compareTo(other: Any): Int {
        if (other is Setting<*>) {
            if (group == other.group)
                return groupIndex - other.groupIndex
            if (other.group == null)
                return 1
            group?.let {
                return it.compareTo(other.group)
            }
        }
        return -1
    }

    /**
     * A group of settings
     * @param name The group name
     * @param settings The group members
     */
    class Group(val name: String, vararg settings: Setting<*>) : Comparable<Group?> {
        val members = mutableListOf<Setting<*>>()
        var enabled = true

        init {
            groups.add(this)
            add(settings.asIterable())
        }

        fun add(settings: Iterable<Setting<*>>) {
            settings.forEach { add(it) }
        }

        fun add(setting: Setting<*>) {
            setting.groupIndex = members.size
            setting.group = this
            members.add(setting)
        }

        val groupObservable: Observable<Setting<*>>
            get() = Observable.merge(members.map { it.observable })

        override fun compareTo(other: Group?): Int {
            if (other == null)
                return 1
            return name.compareTo(other.name)
        }

        override fun toString(): String {
            return "$name:\n" + members.join("    ")
        }
    }

    companion object {
        const val KEY_PREFIX = "pref_"

        fun Iterable<Setting<*>>.join(prefix: String = ""): String {
            return joinToString("\n") { "${prefix}${it.key}=${it.value}" }
        }

        /**
         * The standard datastore
         * May be changed by the application (should do so before first use of any settings.)
         */
        var datastore: DataStore = DataStore()

        /**
         * A map of keys to settings
         */
        val map = HashMap<String, Setting<*>>()
        val filteredValues: Iterable<Setting<*>>
            get() = map.values.filter { !it.filtered }
        val groups = ArrayList<Group>()

        inline fun <reified T : Setting<*>> get(key: String): T? {
            return (map.get(key)) as T?
        }

        internal var emitter: ObservableEmitter<in Setting<*>> = DisposedEmitter()

        /**
         * Receive notifications when ANY settings are changed
         */
        val observableAll: Observable<out Setting<*>> by lazy {
            Observable.create { e: ObservableEmitter<in Setting<*>> ->
                emitter = e
            }.share()
        }

        /**
         * Add a setting to the map
         */
        private fun add(setting: Setting<*>) {
            if (setting.key.isNotBlank()) {
                if (map.containsKey(setting.key))
                    throw IllegalArgumentException("Duplicate settings key ${setting.key}")
                map[setting.key] = setting
            }
        }

        fun asString(): String {
            return filteredValues.groupBy { it.group }.map {
                "${it.key?.name ?: "Ungrouped"}:\n" + it.value.join("    ")
            }.joinToString("\n")
        }

        init {
            // when application moves from foreground, save any changed values.
            ApplicationState.current.changeObserver
                .filter { it.first.active && !it.second.active }
                .observeOnMainBy {
                    map.values.filter { it.changed }.forEach { it.saveValue() }
                }
        }

        private var sauceCounter = 0

        val secretSauce: BooleanSetting = BooleanSetting("secretSauce", "Enable hidden features")

        fun shakeSauceBottle() {
            if (!secretSauce()) {
                when (++sauceCounter) {
                    6 -> {
                        secretSauce.value = true
                        UiAction.toast("Hidden features enabled")
                    }
                    1 -> Single.timer(30, TimeUnit.SECONDS, MainScheduler())
                        .observeBy { sauceCounter = 0 }
                }
            }
        }
    }
}
