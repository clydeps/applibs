package com.controlj.settings

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.graphics.CColor
import com.controlj.utility.Units
import java.util.ArrayList
import kotlin.math.floor
import kotlin.math.max

/**
 * Created by clyde on 15/4/18.
 *
 * A set of ranges associated with caution and alarm limits.
 *
 *
 * @param key The key used to persist the values, and to associate with a unit setting
 * @param baseScaleStart The start of the first range. Each subsequent range starts from the end of the previous.
 * @param ranges The ranges. These values are in standard units
 *
 * Default values are required to ensure the presence of a no-args constructor for Gson use.
 */

open class RangeSet(
        val key: String = "",
        var baseScaleStart: Double = 0.0,
        ranges: List<Range> = listOf()
) {
    var ranges: MutableList<Range> = ranges.toMutableList()
    /**
     * The unit setting associated with the key.
     */
    @delegate:Transient
    val setting: UnitSetting by lazy { Setting.get(key) ?: UnitSetting.identity }

    /**
     * The nominal value. Used to adjust the ranges for a different nominal value
     */
    val nominal: Double
        get() {
            return ranges.filter {
                it.state.color == CColor.NORMAL || it.state.color == CColor.UNDER_RANGE
            }.lastOrNull()?.value ?: baseScaleStart
        }

    /**
     * The default number of ticks to draw on a scale
     */
    @Transient
    var ticks = 5

    /**
     * The start of the scale, in current units.
     */
    val scaleStart: Double
        get() {
            return toCurrent(baseScaleStart)
        }

    /**
     * Get the end of the last range, in current units.
     */
    val scaleEnd: Double
        get() = toCurrent(ranges[ranges.size - 1].value)

    /**
     * The span of the scale
     */
    val scaleSpan: Double
        get() = scaleEnd - scaleStart

    /**
     * Get the values for the scale ticks, in current units
     */
    val tickValues: List<Double>
        get() {
            val step = calculateStepSize()
            var lb = step * floor(scaleStart / step)
            val list = ArrayList<Double>()
            do {
                if (lb >= scaleStart)
                    list.add(lb)
                lb += step
            } while (lb <= scaleEnd)
            return list
        }

    /**
     * Is the given value (in basic units) valid? This is defined as outside the range by a factor of the range span
     */
    fun valid(value: Double): Boolean {
        if (value == INVALID_DATA) return false
        val converted = toCurrent(value)
        val distance = scaleSpan * 2
        return converted > scaleStart - distance && converted < scaleEnd + distance
    }

    /**
     * Update the ranges to a different nominal base, in basic units.
     */

    fun updateBase(newNominal: Double) {
        val factor = newNominal / nominal
        baseScaleStart *= factor
        ranges.forEach { it.value *= factor }
    }

    /**
     * Convert a value in base units to a value in current units
     */
    fun toCurrent(v: Double): Double {
        if (v == INVALID_DATA) return INVALID_DATA
        return setting.value.convertTo(v)
    }

    fun fromCurrent(v: Double): Double {
        if (v == INVALID_DATA) return INVALID_DATA
        return setting.value.convertFrom(v)
    }

    /**
     * Convert a value in basic units to a string in current units
     */
    fun toString(value: Double): String {
        return setting.value.toString(value, setting.value.precision)
    }

    /**
     * Convert a string representing a value in current units to base units
     */
    private fun fromString(string: String): Double {
        return fromCurrent(string.toDoubleOrNull() ?: 0.0)
    }

    /**
     * Get the current state for the given value
     * @receiver A RangeSet to examine
     * @param value The current value
     * @return The RangeState corresponding to the value
     */
    fun stateFor(value: Double): RangeState {
        if (value == INVALID_DATA)
            return RangeState.Neutral
        if (value < minVal)
            return ranges.first().state
        return (ranges.find { value <= it.value } ?: ranges.last()).state
    }

    /**
     * Get the color for a given value. Should be in basic units
     */
    open fun colorForValue(value: Double, dataValid: Boolean = true, lightBackground: Boolean = false): Int {
        if (!dataValid || value == INVALID_DATA)
            return CColor.OUT_OF_RANGE
        val state = stateFor(value)
        if (lightBackground) {
            when (state.color) {
                CColor.CAUTION -> return CColor.CAUTION_DARK
                CColor.NORMAL -> return CColor.NORMAL_DARK
                CColor.WHITE -> return CColor.BLACK
            }
        }
        return state.color
    }

    /**
     * Get the minimum value for the given color
     */

    fun minValForColor(color: Int): Double {
        val index = ranges.indexOfFirst { it.state.color == color }
        return when (index) {
            -1 -> baseScaleStart
            0 -> baseScaleStart
            else -> ranges[index - 1].value
        }
    }

    /**
     * Get the maximum value for any color, in standard units
     */

    val maxVal: Double
        get() = ranges.last().value

    /**
     * The minimum value for any color
     */

    val minVal: Double
        get() = baseScaleStart

    /**
     * The difference between the start and end of the scale, in standard units
     */

    val range: Double
        get() = maxVal - minVal

    /**
     * Get the maximum value for the given color
     */

    fun maxValForColor(color: Int): Double {
        return ranges.findLast { it.state.color == color }?.value ?: maxVal
    }

    fun size(): Int {
        return ranges.size
    }

    fun getColor(i: Int): Int {
        return ranges[i].state.color
    }

    /**
     * Get the ending value for a given range, in currentUnits
     */
    fun getValue(i: Int): Double {
        return toCurrent(ranges[i].value)
    }

    val multiplier: Double
        get() {
            val rangeMag = Math.floor(Math.log10(scaleEnd - 1))
            return Math.pow(10.0, max(0.0, rangeMag - 3))
        }

    fun calculateStepSize(): Double {
        // calculate an initial guess at step size
        val tempStep = (scaleEnd - scaleStart) / ticks / multiplier

        // get the magnitude of the step size
        val mag = Math.floor(Math.log10(tempStep))
        val magPow = Math.pow(10.0, mag)

        // calculate most significant digit of the new step size
        var magMsd = (tempStep / magPow + 0.5).toInt().toDouble()

        // promote the MSD to either 1, 2, or 5
        when {
            magMsd > 5.0 -> magMsd = 10.0
            magMsd > 2.0 -> magMsd = 5.0
            magMsd > 1.0 -> magMsd = 2.0
        }

        return magMsd * magPow * multiplier
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RangeSet

        if (key != other.key) return false
        if (baseScaleStart != other.baseScaleStart) return false
        if (ranges != other.ranges) return false
        if (nominal != other.nominal) return false
        if (ticks != other.ticks) return false

        return true
    }

    override fun hashCode(): Int {
        var result = key.hashCode()
        result = 31 * result + baseScaleStart.hashCode()
        return result
    }

    override fun toString(): String {
        return "RangeSet(key='$key', baseScaleStart=$baseScaleStart, ranges=$ranges)"
    }

    /**
     * Get/set a list of info about each range, in currentUnits
     */
    var rangeInfos: List<RangeInfo>
        get() {
            var start: Double = baseScaleStart
            return ranges.map {
                val next = it.value
                val res = RangeInfo(toString(start), toString(next), it.state)
                start = next
                res
            }
        }
        set(value) {
            baseScaleStart = fromString(value.first().start)
            value.forEachIndexed { index, info ->
                val end = fromString(info.end)
                if (index == ranges.size)
                    ranges.add(Range(end, info.state))
                else ranges[index].let {
                    it.value = end
                    it.state = info.state
                }
            }
        }

    data class RangeInfo(val start: String, val end: String, val state: RangeState) {
        override fun toString(): String {
            return "RangeInfo(start='$start', end='$end', state=${state.name}"
        }

        val colorIndex: Int get() = state.ordinal
    }

    companion object {
        fun from(
                baseUnit: Units.Unit,
                key: String,
                baseScaleStart: Double = 0.0,
                vararg ranges: Range,
                config: (RangeSet) -> Unit = {}

        ): RangeSet {
            return RangeSet(
                    key,
                    baseUnit.convertFrom(baseScaleStart),
                    ranges.map { Range(baseUnit.convertFrom(it.value), it.state) }
            ).also {
                config(it)
            }
        }

        val empty = from(Units.Unit.IDENTITY, "empty", 0.0, Range(Integer.MAX_VALUE.toDouble(), RangeState.Neutral))
        val emptyBlack = object : RangeSet(
                "empty",
                0.0,
                listOf(Range(0.0, RangeState.Neutral))
        ) {
            override fun colorForValue(value: Double, dataValid: Boolean, lightBackground: Boolean): Int {
                return CColor.BLACK
            }
        }
    }
}
