package com.controlj.settings

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2018-11-27
 * Time: 07:32
 */
interface  SettingValidator<T: Any> {

    /**
     * Return true if the new value is acceptable to the filter
     */
    fun changePermitted(setting: Setting<T>, value: T): Boolean
}
