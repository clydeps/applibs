package com.controlj.settings

/**
 * Created by clyde on 6/7/18.
 */
class IntSetting(key: String, label: String, description: String = "", override val defaultValue: Int = 0) :
    Setting<Int>(key, label, description) {
    override var value: Int = defaultValue
        set(value) {
            if (field != value) {
                field = value
                changed = true
            }
        }

    override fun load(store: DataStore) {
        value = if (datastore.contains(storeKey)) datastore.getInt(storeKey) else defaultValue
    }

    override fun save(store: DataStore) {
        datastore.putInt(storeKey, value)
    }
}
