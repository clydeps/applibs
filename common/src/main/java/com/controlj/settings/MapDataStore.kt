package com.controlj.settings

import com.controlj.rx.DisposedEmitter
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import java.lang.reflect.Type
import java.util.concurrent.ConcurrentHashMap

/**
 * Created by clyde on 2/7/18.
 *
 * A datastore that saves in a in memory map
 * @param default if this should be the default DataStore
 */
class MapDataStore(default: Boolean = false) : DataStore {

    private var emitter: ObservableEmitter<String> = DisposedEmitter()

    private val map = object : ConcurrentHashMap<String, Any>() {
        override fun put(key: String, value: Any): Any? {
            val result = super.put(key, value)
            emitter.onNext(key)
            return result
        }
    }
    private val gson = Gson()

    override val allStringEntries: Map<String, String>
        get() = map.filter { it.value is String }.mapValues { it.value as String }
    override val allEntries: Map<String, *>
        get() = map
    override val isDebugFeatures: Boolean
        get() = DataStore().isDebugFeatures

    init {
        if (default)
            init()
    }

    override fun getInt(key: String, dflt: Int): Int {
        return (map[key] as? Int)?: dflt
    }

    override fun getString(key: String, dflt: String): String {
        return map[key]?.toString() ?: dflt
    }

    override fun getLong(key: String, dflt: Long): Long {
        return (map[key] as? Long)?: dflt
    }

    override fun putInt(key: String, value: Int) {
        map[key] = value
    }

    override fun putFloat(key: String, value: Float) {
        map[key] = value
    }

    override fun putString(key: String, value: String) {
        map[key] = value
    }

    override fun putLong(key: String, value: Long) {
        map[key] = value
    }

    override fun remove(key: String) {
        map.remove(key)
    }

    override fun getIntList(key: String): List<Int> {
        val s = getString(key)
        return gson.fromJson<List<Int>>(s, object : TypeToken<List<Int>>() {}.type)
            ?: return ArrayList()
    }

    @Suppress("UNCHECKED_CAST")
    override fun getStringList(key: String): List<String> {
        val list = map[key]
        if (list is List<*>)
            return list as List<String>
        return listOf()
    }

    override fun getBoolean(name: String): Boolean {
        return map[name] as Boolean
    }

    override fun putBoolean(key: String, value: Boolean) {
        map[key] = value
    }

    override fun putStringList(key: String, values: List<String>) {
        map[key] = values
    }

    override fun putIntList(key: String, values: List<Int>) {
        map[key] = values
    }

    override fun contains(key: String): Boolean {
        return map.contains(key)
    }

    override fun putObject(key: String, o: Any) {
        putString(key, gson.toJson(o))
    }

    override fun <T> getObject(key: String, clazz: Class<T>): T? {
        val s = getString(key)
        return try {
            gson.fromJson(s, clazz)
        } catch (ex: JsonSyntaxException) {
            null
        }
    }

    override fun <T> getObject(key: String, type: Type): T? {
        val s = getString(key)
        return try {
            gson.fromJson<T>(s, type)
        } catch (ex: JsonSyntaxException) {
            null
        }
    }

    override val observeChanges: Observable<String> by lazy {
        Observable.create { e: ObservableEmitter<String> ->
            emitter = e
        }
            .share()
    }

    override fun getFloat(key: String, dflt: Float): Float {
        return (map[key] as? Float)?: dflt
    }

    override fun commit() = Unit
}
