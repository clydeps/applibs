package com.controlj.settings

/**
 * Created by clyde on 6/9/18.
 */
interface StringStore {
    fun putString(key: String, value: String)
    fun getString(key: String, dflt: String = ""): String
}
