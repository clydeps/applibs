package com.controlj.settings

/**
 * Created by clyde on 2/9/18.
 */
object UserInterfaceSettings {

    val useNativeKeyboard = BooleanSetting("use_native_keyboard", "Use standard keyboard",
            defaultValue = false, description = "Select to use pop-up OS keyboard instead of custom embedded keyboard")

}
