package com.controlj.network

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import java.net.DatagramPacket
import java.net.InetAddress
import java.net.InetSocketAddress
import java.nio.channels.DatagramChannel
import java.nio.channels.SocketChannel

/**
 * Created by clyde on 18/10/17.
 */

data class UdpData(val address: InetAddress, val data: ByteArray)

object NetworkFactory {

    fun observeUdpBroadcast(port: Int): Observable<UdpData> {
        return Observable.create { emitter: ObservableEmitter<UdpData> ->
            val channel: DatagramChannel = DatagramChannel.open()
            Thread {
                try {
                    channel.use {
                        val socket = it.socket()
                        //emitter.setCancellable { logException(IOException("udp listener cancelled"));debug("closing channel"); channel.close() }
                        socket.reuseAddress = true
                        socket.bind(InetSocketAddress("0.0.0.0", port))
                        val buffer = ByteArray(1024)
                        //debug("udp onSubscribe")
                        while (!emitter.isDisposed && !socket.isClosed) {
                            val packet = DatagramPacket(buffer, buffer.size)
                            socket.receive(packet)
                            val length = packet.length
                            //debug("Got udp buffer length $length, address ${packet.address}")
                            if (length == 0) {
                                break
                            }
                            val data = ByteArray(length)
                            System.arraycopy(buffer, 0, data, 0, length)
                            emitter.onNext(UdpData(packet.address, data))
                        }
                    }
                } catch (ex: Exception) {
                    if (!emitter.isDisposed)
                        if (ex is InterruptedException)
                            emitter.onComplete()
                        else
                            emitter.tryOnError(ex)
                }
            }.also { thread ->
                emitter.setCancellable {
                    thread.interrupt()
                    channel.close()
                }
            }.start()
        }
    }

    /**
     * Open a TCP Channel. Return an object which allows writes, and observes reads
     *
     * @param host
     * @param port
     * @return A TcpChannel object
     */
    @JvmStatic
    fun getTcpChannel(host: String, port: Int): SocketChannel {
        val address = InetSocketAddress(host, port)
        return SocketChannel.open(address)
    }
}
