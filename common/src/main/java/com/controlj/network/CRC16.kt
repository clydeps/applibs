package com.controlj.network

object CRC16 {
    val table = IntArray(256) {
        var crc = it shl 8
        repeat(8) {
            crc = (crc shl 1) xor (if ((crc and 0x8000) != 0) 0x1021 else 0)
        }
        crc and 0xFFFF
    }
}

val ByteArray.crc16: Int
    get() {
        return fold(0) { acc, b ->
            (CRC16.table[acc ushr 8] xor ((acc shl 8) and 0xFFFF) xor (b.toInt() and 0xFF)) and 0xFFFF
        }
    }

val ByteArray.addCrc16: ByteArray
    get() {
        val crc = crc16
        return this + crc.toByte() + (crc shr 8).toByte()
    }
