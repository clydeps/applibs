package com.controlj.monitor

import com.controlj.ble.BleDevice
import com.controlj.ble.BleScanResult
import com.controlj.ble.BleScanner
import com.controlj.rx.MainScheduler
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.nio.BufferUnderflowException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

/**
 * Created by clyde on 4/12/17.
 */
class BlueMon(name: String, location: String, val device: BleDevice) : MonitorDevice(name, location, device.address) {
    companion object {
        private val addressRegex = Regex("([0-9A-F]{2}:){5}[0-9A-F]{2}", RegexOption.IGNORE_CASE)
        const val SAMPLE_RATE = 60        // default sample rate is one per 60 seconds
        const val CONTROL_J_MANUF = 0x472
        const val MONITOR_MODEL: Short = 4350
        private const val MIN_BUFFER_LEN = 4
        private const val MULAW_BIAS = 33
        private const val LIGHT_SCALE = 10.0
        const val ENVIRONMENT_UUID = "773c1100-d065-45bd-890b-d960e85e0247"
        const val SLEEP_UUID = "773c1102-d065-45bd-890b-d960e85e0247"
        const val MACADDR_UUID = "773c110b-d065-45bd-890b-d960e85e0247"

        private const val TEMP_OFFS = 0
        private const val TEMP_LEN = 2
        private const val HUMID_OFFS = (TEMP_OFFS + TEMP_LEN)
        private const val HUMID_LEN = 2
        private const val PRESSURE_OFFS = (HUMID_OFFS + HUMID_LEN)
        private const val PRESSURE_LEN = 4
        private const val LIGHT_OFFS = (PRESSURE_OFFS + PRESSURE_LEN)
        private const val LIGHT_LEN = 2
        private const val ENVIRONMENT_LEN = (LIGHT_OFFS + LIGHT_LEN)
        private const val SLEEP_LEN = 2

        private fun decodeLight(data: Byte): Double {
            val position = (data.toInt() and 0xF0).shr(4) + 5
            return ((1.shl(position) or (((data.toInt() and 0x0F).shl(position - 4)) or
                (1.shl(position - 5)))) - MULAW_BIAS) / LIGHT_SCALE
        }

        /**
         * def mulaw_to_value(mudata):
        """Convert a mu-law encoded value to linear."""
        position = ((mudata & 0xF0) >> 4) + 5
        return ((1 << position) | ((mudata & 0xF) << (position - 4)) | (1 << (position - 5))) - 33
         */

        fun matches(scanResult: BleScanResult): Boolean {
            val bytes = scanResult.getManufacturerData(CONTROL_J_MANUF)
            if (bytes.size < 4)
                return false
            val buffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN)
            return buffer.short == MONITOR_MODEL
        }

        fun processScan(result: BleScanResult): MonitorData {
            val buffer = ByteBuffer.wrap(result.getManufacturerData(CONTROL_J_MANUF)).order(ByteOrder.LITTLE_ENDIAN)
            if (buffer.capacity() < MIN_BUFFER_LEN)
                throw IllegalArgumentException("Buffer too short")
            if (buffer.short != MONITOR_MODEL)
                throw IllegalArgumentException("Model number wrong")
            val temp = buffer.short.toDouble() / 100
            var humidity = 0
            var battery = 0
            var light = 0.0
            var pressure = 0.0
            var bits = 0
            try {
                humidity = buffer.get().toInt()
                light = decodeLight(buffer.get())
                battery = buffer.get().toInt()
                pressure = buffer.short.toDouble()
                bits = buffer.get().toInt()
            } catch (ex: BufferUnderflowException) {

            }
            return MonitorData(
                address = result.address,
                humidity = humidity.toDouble(), temperature = ((temp * 10.0).roundToInt() / 10.0),
                battery = battery, light = light, pressure = pressure, rssi = result.rssi,
                bits = bits
            )
        }
    }

    private fun processData(data: ByteArray): MonitorData {
        if (data.size != ENVIRONMENT_LEN)
            throw IllegalArgumentException("Invalid buffer length for environment data")
        val buffer = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN)
        val temp = buffer.short.toDouble() / 100
        val humid = buffer.short.toDouble() / 100
        val pressure = buffer.int.toDouble() / 100
        val light = buffer.short.toDouble()
        return MonitorData(address = address, temperature = temp, humidity = humid, light = light, pressure = pressure)
    }

    override fun observe(interval: Int): Observable<MonitorData> {
        return BleScanner().bleScan
            .filter { it.address.equals(address, true) }
            .throttleFirst(SAMPLE_RATE.toLong(), TimeUnit.SECONDS)
            .map { processScan(it) }
            .subscribeOn(MainScheduler())
    }

    override fun observeSingle(sleepTime: Int): Single<MonitorData> {
        val data = ByteArray(SLEEP_LEN)
        val buffer = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN)
        buffer.putShort(sleepTime.toShort())
        return device.getConnector(true)
            .subscribeOn(Schedulers.io())
            .flatMap { device.discoverServices }
            .flatMap { device.getWriter(SLEEP_UUID, data) }
            .flatMap { device.getReader(ENVIRONMENT_UUID) }
            .map { processData(it.data) }
            .doFinally { device.disconnector.onErrorReturnItem(device).subscribe() }
            .observeOn(MainScheduler())
    }

    /**
     * Get the MAC address of the device as a byte array in little-endian order.
     * Android devices have a valid mac address available immediately, iOS devices
     * need it to be read from the device.
     */

    val readAddress: Single<ByteArray>
        get() {
            if (address.matches(addressRegex))
                return Single.just(address.split(':')
                    .map { it.toInt(16).toByte() }
                    .reversed()
                    .toByteArray())
            return device.getConnector(true)
                .flatMap { device.discoverServices }
                .flatMap { device.getReader(MACADDR_UUID) }
                .map { data -> data.data }
                .doFinally { device.disconnector.onErrorReturnItem(device).subscribe() }
                .observeOn(MainScheduler())
        }


    override fun toString(): String {
        return "BlueMon:" + address.drop(address.length - 4)
    }
}
