package com.controlj.monitor

import com.controlj.utility.instantNow
import org.threeten.bp.Instant
import java.util.Locale

/**
 * Created by clyde on 30/11/17.
 */

data class MonitorData(
        val address: String,
        val timeStamp: Instant = instantNow,
        val temperature: Double,
        val humidity: Double,
        val light: Double,
        val bits: Int = 0,
        val battery: Int = 0,
        val pressure: Double = 0.0,
        val rssi: Int = -200
) {

    companion object {
    }

    val data: Map<String, Any> = mapOf("tem" to temperature, "hum" to humidity, "lig" to light, "bat" to battery, "pre" to pressure)

    val last: Map<String, Any>
        get() {
            val mm: MutableMap<String, Any> = HashMap()
            mm.putAll(data)
            mm["timestamp"] = timeStamp.toEpochMilli()
            return mm
        }

    // the data path is <addr>/<day number>/<timestamp>/
    val dataPath: String
        get() {
            return String.format(Locale.US, "data/%s/%d/%d", address.uppercase(), timeStamp.toEpochMilli() / (24 * 60 * 60 * 1000), timeStamp.toEpochMilli())
        }

    // the last data path is <addr>/last/
    val lastPath: String
        get() {
            return String.format(Locale.US, "devices/%s/last", address.uppercase())
        }

    val updateData: Map<String, Any>
        get() {
            val mm: MutableMap<String, Any> = HashMap()
            mm[dataPath] = data
            mm[lastPath] = last
            return mm
        }

}
