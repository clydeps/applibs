package com.controlj.monitor

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single


/**
 * Created by clyde on 1/12/17.
 */
abstract class MonitorDevice(val name: String, val location: String, val address: String) {
    abstract fun observe(interval: Int): Observable<MonitorData>
    abstract fun observeSingle(sleepTime: Int): Single<MonitorData>
}
