package com.controlj.dfu

import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.File
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.Locale
import java.util.UUID

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 26/3/17
 * Time: 12:07
 */

/*
This is the structure of the firmware file header, and block headers.
The file consists of the file header, followed by numblocks block headers, then the
data at the specified offsets.
typedef struct {
    unsigned char tag[4];            // magic number goes here
    unsigned char major[2];        // major version number
    unsigned char minor[2];        // minor version number
    unsigned char numblocks[2];        // the number of blocks in the file,
    unsigned char unused[6];
    unsigned char service_uuid[UUID_LEN];    // the service uuid of the bootloader
} firmware;

//Block header

typedef struct {
    unsigned char addr[4];        // the address at which to load this block
    unsigned char size[4];        // the size of this block
    unsigned char offset[4];        // offset in the file of the data
    unsigned char padding[1];   // length of padding at end
    unsigned char unused[3];
    unsigned char init_vector[IV_LEN];    // the block 0 initialization vector
	unsigned char sha256[32];       // SHA256 hash of the data
} block_header;


 */
class FirmwareLoader(val filename: String, val fileData: ByteArray = File(filename).readBytes()) {

    val info: Information
    val headers = ArrayList<DataHeader>()

    inner class DataHeader {
        var offset: Int = 0        // file offset
        var addr: Int = 0        // address in memory
        var length: Int = 0        // number of bytes
        var extra: Int = 0        // extra bytes at end
        var initVector = ByteArray(IV_LEN)
        var digest = ByteArray(DIGEST_LEN)

        @Throws(IOException::class)
        fun read(pos: Int, bufLen: Int): ByteArray {
            var len = bufLen
            if (pos + len > length + extra)
                len = length + extra - pos
            return fileData.copyOfRange(pos + offset, pos + offset + len)
        }
    }

    data class Information(
        val filename: String,
        val serviceUuid: UUID,
        val versionMajor: Int,
        val versionMinor: Int,
        var numBlocks: Int = 0,
        var totalBytes: Int = 0,
        var baseAddr: Int = Integer.MAX_VALUE
    ) {

        override fun toString(): String {
            return String.format(
                "%s: V%d.%d, %d blocks, base 0x%X, size %d bytes, uuid: %s",
                File(filename).name,
                versionMajor, versionMinor,
                numBlocks, baseAddr, totalBytes,
                serviceUuid.toString()
            )
        }
    }


    init {
        if (fileData.size < HEADER_LEN)
            throw IOException("Short read on file header")
        val bb = ByteBuffer.wrap(fileData)
        bb.order(ByteOrder.LITTLE_ENDIAN)
        if (bb.getInt(MAGIC_OFFS) != MAGIC)
            throw IOException(
                String.format(
                    "Invalid magic number - expected %X, saw %X",
                    MAGIC,
                    bb.getInt(MAGIC_OFFS)
                )
            )
        val versionMajor = bb.getChar(MAJOR_OFFS).code
        val versionMinor = bb.getChar(MINOR_OFFS).code
        val numBlocks = bb.getChar(NUMBLK_OFFS).code
        if (numBlocks == 0)
            throw IOException("Block count is zero")
        // the uuid is in big-endian format
        bb.order(ByteOrder.BIG_ENDIAN)
        val low = bb.getLong(UUID_OFFS + 8)
        val high = bb.getLong(UUID_OFFS)
        val serviceUuid = UUID(high, low)
        info = Information(filename, serviceUuid, versionMajor, versionMinor, numBlocks = numBlocks)
        bb.order(ByteOrder.LITTLE_ENDIAN)
        for (i in 0 until numBlocks) {
            val offset = HEADER_LEN + i * BLKHDR_LEN
            val hdr = DataHeader()
            hdr.length = bb.getInt(offset + LENGTH_OFFS)
            hdr.addr = bb.getInt(offset + ADDR_OFFS)
            hdr.offset = bb.getInt(offset + OFFSET_OFFS)
            hdr.extra = bb.get(offset + EXTRA_OFFS).toInt()
            bb.position(offset + IV_OFFS)
            bb.get(hdr.initVector)
            bb.position(offset + DIGEST_OFFS)
            bb.get(hdr.digest)
            info.totalBytes += hdr.length + hdr.extra
            headers.add(hdr)
            if (hdr.addr < info.baseAddr)
                info.baseAddr = hdr.addr
        }
    }

    fun getHeader(idx: Int): DataHeader {
        return headers[idx]
    }

    val version: Int
        get() = info.versionMajor * 10 + info.versionMinor

    companion object {
        internal const val UUID_LEN = 16        // length of uuid
        internal const val MAGIC = 0x55A322BF
        internal const val MAGIC_OFFS = 0
        internal const val MAJOR_OFFS = 4
        internal const val MINOR_OFFS = 6
        internal const val NUMBLK_OFFS = 8
        internal const val UUID_OFFS = 16
        internal const val HEADER_LEN = 16 + UUID_LEN

        internal const val DIGEST_LEN = 32        // length of SHA256 digest
        internal const val IV_LEN = 128 / 8        // length of initialization vector
        internal const val ADDR_OFFS = 0
        internal const val LENGTH_OFFS = 4
        internal const val OFFSET_OFFS = 8
        internal const val EXTRA_OFFS = 12
        internal const val IV_OFFS = 16
        internal const val DIGEST_OFFS = IV_OFFS + IV_LEN
        internal const val BLKHDR_LEN = DIGEST_OFFS + DIGEST_LEN

        fun getFirmwareLoader(filename: String): Single<FirmwareLoader> {
            return Single.create { e: SingleEmitter<FirmwareLoader> ->
                val loader = FirmwareLoader(filename)
                e.onSuccess(loader)
            }.subscribeOn(Schedulers.io())
        }
    }
}
