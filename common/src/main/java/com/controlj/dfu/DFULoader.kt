package com.controlj.dfu

import com.controlj.ble.BleCharacteristic
import com.controlj.ble.BleDevice
import com.controlj.data.CharacteristicData
import com.controlj.data.Progress
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedEmitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 14/3/17
 * Time: 14:12
 */

class DFULoader(
    private val device: BleDevice,
    private val loader: FirmwareLoader,
    private val chunkSize: Int = CHUNK_SIZE
) {
    private var emitter: ObservableEmitter<Progress> = DisposedEmitter()
    private var programThread: Thread? = null
    private var notificationDisposable: Disposable = Disposable.disposed()
    private var resyncVal: Int = 0
    private var connectDisposable = Disposable.disposed()

    init {
        if ((chunkSize - 1) and chunkSize != 0)
            error("Bad chunk size")
    }

    private fun sendCommand(cmd: Int, len: Int = 0, addr: Long = 0) {
        val packet = ByteBuffer.allocate(DFU_CTRL_PKT_SIZE)
        packet.order(ByteOrder.LITTLE_ENDIAN)
        packet.putShort(cmd.toShort())
        packet.putShort(len.toShort())
        packet.putInt(addr.toInt())
        @Suppress("CheckResult")
        try {
            device.getWriter(DFU_CTRL, packet.array())
                .timeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .blockingGet()
        } catch (ex: Exception) {
            error(ex)
        }
    }

    @Suppress("CheckResult")
    private fun sendData(data: ByteArray) {
        //logMsg("Sending data len %d, semaphore %d", data.length, semaphore.availablePermits());
        try {
            device.getWriter(DFU_DATA, data, BleCharacteristic.WriteType.NO_RESPONSE)
                .timeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .blockingGet()
        } catch (ex: Exception) {
            error(ex)
        }
    }

    fun upload(): Observable<Progress> {
        return Observable.create { e: ObservableEmitter<Progress> ->
            emitter = e
            connect()
        }
            .subscribeOn(Schedulers.io())
    }

    private fun connect() {
        notificationDisposable = device.notifications.subscribe({ this.onNotify(it) }, {})
        emitter.onNext(Progress("Connecting", 0, false))
        connectDisposable = device.getConnector(false)
            .flatMap { device.discoverServices }
            .flatMap { services ->
                if (services.none { it.matches(loader.info.serviceUuid) })
                    error(Throwable("Firmware does not match device"))
                device.setNotification(DFU_PROGRESS, true)
            }
            .flatMap { device.setMtu(MAX_BUFLEN + 3 + 4) }
            .observeOn(Schedulers.computation())
            .subscribe(
                { mtu: Int ->
                    logMsg("discovery complete, mtu = %d", mtu)
                    Thread { this.program() }.start()
                },
                { logException(it) }
            )
    }

    private fun onError(e: Throwable) {
        if (!emitter.isDisposed)
            emitter.tryOnError(e)
        connectDisposable.dispose()
    }

    private fun error(throwable: Throwable) {
        logException(throwable)
        notificationDisposable.dispose()
        device.disconnect()
        onError(throwable)
    }

    private fun program() {
        logMsg("Starting program();")
        var buflen = MAX_BUFLEN
        programThread = Thread.currentThread()
        device.setPriority(BleDevice.Priority.HIGH)
        val totalBytes = loader.info.totalBytes
        var totalCount = 0
        var progress = 0
        var resyncs = 0
        device.getCharacteristic(DFU_DATA)?.writeType = BleCharacteristic.WriteType.NO_RESPONSE
        try {
            if (device.mtu < MAX_BUFLEN + 4 + 3) {
                buflen = MIN_BUFLEN
            }
            logMsg("Buflen is $buflen")
            (0 until loader.info.numBlocks).forEach { i ->
                sendCommand(DFU_CMD_RESTART)
                val header = loader.getHeader(i)
                logMsg("Writing %d bytes at %X", header.length, header.addr)
                val length = header.length + header.extra
                val iv = header.initVector
                sendCommand(DFU_CMD_IV, iv.size, 0)
                sendData(iv)
                var addr = header.addr
                sendCommand(DFU_CMD_DATA, length, addr.toLong())
                var count = 0
                resyncVal = addr + length
                while (count != length && !emitter.isDisposed) {
                    val balance = (length - count).coerceAtMost(buflen)
                    val buffer = ByteBuffer.allocate(balance + 4)
                    buffer.order(ByteOrder.LITTLE_ENDIAN)
                    buffer.putInt(addr)
                    buffer.put(header.read(count, balance))
                    sendData(buffer.array())
                    Thread.sleep(5)        // allow time for the data to be written
                    count += balance
                    addr += balance
                    totalCount += balance
                    // wait for device to catch up after each chunk.
                    if (count == length || addr % chunkSize == 0) {
                        sendCommand(DFU_CMD_PING, count, 0)
                    }
                    if (totalCount * 100 / totalBytes != progress) {
                        progress = totalCount * 100 / totalBytes
                        emitter.onNext(Progress("Writing data", progress, false))
                    }
                    synchronized(this) {
                        if (resyncVal < addr) {
                            if (++resyncs > MAX_RESYNCS) {
                                onError(IOException("Too many retries"))
                                return
                            }
                            resyncVal = resyncVal and (buflen - 1).inv()
                            totalCount -= addr - resyncVal
                            addr = resyncVal
                            resyncVal = header.addr + length
                            count = addr - header.addr
                        }
                    }
                }
                val digest = header.digest
                logMsg("Sending digest, addr=%X, length=%X", header.addr, header.length + header.extra)
                sendCommand(DFU_CMD_DIGEST, header.length + header.extra, header.addr.toLong())
                sendData(digest)
            }
            emitter.onNext(Progress("Flashing complete - rebooting", 100, true))
            sendCommand(DFU_CMD_DONE)
            sendCommand(DFU_CMD_RESET)
            Thread.sleep(100)
            device.disconnect()
            emitter.onComplete()
        } catch (e: Exception) {
            logException(e)
            onError(e)
        } finally {
            logMsg("Disconnecting...")
            notificationDisposable.dispose()
            connectDisposable.dispose()
        }
    }

    private fun onNotify(chData: CharacteristicData) {
        logMsg("Characteristic %s read, length %d", chData.characteristic.uuid, chData.data.size)
        if (chData.characteristic.matches(DFU_PROGRESS)) {
            if (chData.data.size >= 1) {
                val buffer = ByteBuffer.wrap(chData.data)
                buffer.order(ByteOrder.LITTLE_ENDIAN)
                when (buffer.get().toInt()) {
                    DFU_RESYNC -> {
                        synchronized(this) {
                            resyncVal = buffer.int
                        }
                        logMsg("Resync to %x", resyncVal)
                    }

                    DFU_DIGEST_FAILED -> {
                        logMsg("Digest failed")
                        onError(IOException("Digest failed"))
                        programThread?.interrupt()
                    }

                    else -> logMsg("Received unknown command on progress characteristic: %d", chData.data[0])
                }
            }
        }
    }

    companion object {

        val DFU_DATA = "95301002-963F-46B1-B801-0B23E8904835"
        val DFU_CTRL = "95301001-963F-46B1-B801-0B23E8904835"
        val DFU_PROGRESS = "95301003-963F-46B1-B801-0B23E8904835"

        internal val DFU_CMD_RESTART = 0x1     // reset DFU system - resets data counts etc.
        internal val DFU_CMD_DATA = 0x2     // data coming on the data channel
        internal val DFU_CMD_IV = 0x3     // iv coming on data channel
        internal val DFU_CMD_DONE = 0x4     // Data transmission all done.	Response indicates success or not.
        internal val DFU_CMD_RESET = 0x5     // reset device
        internal val DFU_CMD_DIGEST = 0x6     // SHA256 digest coming
        internal val DFU_CMD_PING = 0x7     // check progress

        internal val DFU_CTRL_PKT_CMD = 0       // offset of command word
        internal val DFU_CTRL_PKT_LEN = 2       // offset of length word
        internal val DFU_CTRL_PKT_ADR = 4       // offset of address doubleword
        internal val DFU_CTRL_PKT_SIZE = 8       // total length of packet

        internal val DFU_RESYNC = 1            // resync to this address
        internal val DFU_DIGEST_FAILED = 2        // verification failed

        internal val MAX_RESYNCS = 100            // max number of times we try to resync

        internal val CHUNK_SIZE = 0x100        // send in chunks this big

        private val MAX_BUFLEN = 64
        private val MIN_BUFLEN = 16
        private val TIMEOUT = 10000L
    }
}

