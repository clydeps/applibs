package com.controlj.location

import com.controlj.data.Position

/**
 * Descriptors for region monitoring
 */
sealed class Region {

    abstract val identifier: String
    /**
     * A region defined by proximity to a beacon device
     * @param uuid The UUID to match
     * @param major The major device number
     * @param minor The minor device number
     */
    class Beacon(
            val uuid: String,
            val major: Int? = null,
            val minor: Int? = null
    ): Region() {
        override val identifier = "$uuid:$major:$minor"
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Beacon

            if (identifier != other.identifier) return false

            return true
        }

        override fun hashCode(): Int {
            return identifier.hashCode()
        }

        override fun toString(): String {
            return "Beacon('$identifier')"
        }
    }

    /**
     * A region defined by a location and radius
     * @param center The center of the region
     * @param radius The radius in meters.
     */

    class Circular(
            val center: Position,
            val radius: Double
    ): Region() {
        override val identifier = "$center:$radius"
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Circular

            if (center != other.center) return false
            if (radius != other.radius) return false

            return true
        }

        override fun hashCode(): Int {
            var result = center.hashCode()
            result = 31 * result + radius.hashCode()
            return result
        }

        override fun toString(): String {
            return "Circular(center=$center, radius=$radius)"
        }
    }

    /**
     * Generated for region enter/exit events.
     * @param region The region of interest.
     * @param inside True if the device is currently inside the region
     */
    data class Event(val region: Region, val inside: Boolean)
}


