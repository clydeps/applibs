package com.controlj.location

import com.controlj.rx.MainScheduler
import io.reactivex.rxjava3.core.Emitter
import io.reactivex.rxjava3.core.Observable
import java.util.concurrent.ConcurrentSkipListMap

interface RegionMonitor {

    /**
     * Listen for changes in the status of a location region
     * @param region The region of interest
     * @param callback A callback to invoke when the region status changes
     */
    fun startRegionMonitor(region: Region, callback: (Region.Event) -> Unit)

    /**
     * Stop monitoring for the given region
     * @param region The region of interest
     */
    fun stopRegionMonitor(region: Region)

    /**
     * Clear all regions.
     */

    fun clearAllRegions()


    companion object {
        // the singleton instance of a RegionMonitor
        lateinit var instance: RegionMonitor

        operator fun invoke() = instance

        /**
         * Manages events for a specific region
         */
        private class Monitor(val region: Region) {
            lateinit var emitter: Emitter<Region.Event>
            val observer = Observable.create<Region.Event> { emitter ->
                this.emitter = emitter
                emitter.setCancellable { instance.stopRegionMonitor(region) }
                instance.startRegionMonitor(region) { emitter.onNext(it) }
            }
                    .subscribeOn(MainScheduler.instance)
                    .share()
        }

        private val regionMap = ConcurrentSkipListMap<String, Monitor>()

        /**
         * Observe location changes for a given region.
         */

        fun observeRegion(region: Region): Observable<Region.Event> {
            return regionMap.getOrPut(region.identifier) { Monitor(region) }
                    .observer
        }

        fun stopAll() {
            instance.clearAllRegions()
            regionMap.values.forEach { it.emitter.onComplete() }
            regionMap.clear()
        }
    }
}
