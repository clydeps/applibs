package com.controlj.location

import com.controlj.ble.BleScanner
import com.controlj.data.Location
import com.controlj.viewmodel.ListenableValue
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

/**
 * An interface to be implemented by sources of Location.
 *
 */
interface LocationProvider {

    /**
     * the name of this source
     */

    val name: String

    /**
     * An Observable that will deliver a location stream.
     */
    val locationObserver: Observable<Location>

    /**
     * The priority of the source. Higher is better
     */
    var priority: Int

    @Deprecated(
        "Replace with askPermission()",
        replaceWith = ReplaceWith("askPermission(rationale).map { it >= AuthorisationStatus.WhileInUse }")
    )
    fun requestPermission(
        rationale: String = BleScanner.RATIONALE,
    ): Single<Boolean> {
        return askPermission(rationale, AuthorisationStatus.Always).map { it >= AuthorisationStatus.WhileInUse }
    }

    /**
     * Ask  for permission to use Location
     * @param rationale The reason for wanting this permission
     * @param requiredStatus The desired level of permission
     */

    fun askPermission(
        rationale: String = BleScanner.RATIONALE,
        requiredStatus: AuthorisationStatus = AuthorisationStatus.Always
    ): Single<AuthorisationStatus> {
        return Single.just(requiredStatus)      // default implementation
    }

    /**
     * Check the current location permission. Should preferably not do any request.
     */
    fun checkPermission(): Single<AuthorisationStatus> {
        return Single.just(authorisationStatus)
    }

    /**
     * An enable flag
     */

    val enabled: ListenableValue<Boolean>

    /**
     * Is it connected?
     */

    val isConnected: Boolean

    /**
     * An authorisation status
     * @param enabled If this represents some kind of enabled permission
     */
    enum class AuthorisationStatus(val enabled: Boolean) {
        Unknown(false),
        None(false),                           // location not authorised
        WhileInUse(true),                     // authorised while in use only
        Always(true)                          // includes background use
    }

    val creator: Location.Creator
        get() = Location.Creator(name, priority)

    val authorisationStatus: AuthorisationStatus

    companion object {
        lateinit var deviceProvider: LocationProvider

        operator fun invoke() = deviceProvider
    }
}
