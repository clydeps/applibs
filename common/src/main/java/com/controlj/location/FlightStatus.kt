package com.controlj.location

/**
 * Collect some data in a known place about our status.
 */
object FlightStatus {

    // True if currently in flight
    var isInFlight: Boolean = false

    // true if currently broadcasting position
    var isBroadcasting: Boolean = false

    // a battery level status
    var isBatteryLow: Boolean = false
}
