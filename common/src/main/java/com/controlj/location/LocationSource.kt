package com.controlj.location

import com.controlj.data.Location
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedEmitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.threeten.bp.Instant
import java.util.concurrent.ConcurrentHashMap

/**
 * Created by clyde on 4/9/18.
 * A global object to distribute updated location
 */
object LocationSource {

    /**
     * The last location we have seen
     */
    var lastLocation: Location = Location.invalid
        set(value) {
            if (field.creator != value.creator)
                logMsg("New location source ${Location.toString(value)}")
            field = value
            emitter.onNext(value)
        }

    // the emitter for subscriptions
    internal var emitter: ObservableEmitter<Location> = DisposedEmitter()

    /**
     * An observer to deliver location updates
     */
    val observer: Observable<Location> = Observable.create { emitter = it }
        .share()

    /**
     * Resend location to any listeners
     */

    fun refresh() {
        emitter.onNext(lastLocation)
    }

    /** here we collect locations from sources of different priorities and assess their worth.
     * A source is considered valid if it has delivered at least 5 updates in the last 10 seconds.
     */

    private const val TIME_WINDOW = 20L      // seconds over which to assess
    private const val MIN_ITEMS = 10         // minimum number of items received in that time

    /**
     * The sources to be used, mapped to a history of valid data
     */
    internal val sources: ConcurrentHashMap<LocationProvider, Provider> = ConcurrentHashMap()

    internal class Provider(val source: LocationProvider) {
        val instants = mutableListOf<Instant>()
        var last: Location = Location.invalid(source.creator)
        val disposable: Disposable = source.checkPermission()
            .observeOn(Schedulers.io())
            .filter {
                logMsg("Got requestPermission result $it for source $source")
                it.enabled
            }
            .flatMapObservable { source.locationObserver }
            .filter { it.valid }
            .subscribe(
                { location ->
                    instants.add(location.timestamp)
                    last = location
                    if (source == best()) {
                        lastLocation = location
                    }
                },
                {
                    logException(it)
                    sources.remove(source)
                }
            )

        val valid: Boolean
            get() = instants.isNotEmpty()

    }

    /**
     * Add a source
     */

    fun add(source: LocationProvider) {
        sources.getOrPut(source) { Provider(source) }
    }

    /**
     * Remove a source
     */

    fun remove(source: LocationProvider) {
        sources.remove(source)?.disposable?.dispose()
    }

    /**
     * return a map of sources to their most recent location
     */

    val sourceLocations: Map<LocationProvider, Location>
        get() = sources.map { it.key to it.value.last }.toMap()

    /**
     * Get the highest priority valid source.
     */

    private fun best(): LocationProvider? {
        val since = Instant.now().minusSeconds(TIME_WINDOW)
        synchronized(sources) {
            sources.values.forEach { it.instants.removeAll { it.isBefore(since) } }
        }
        return sources.filter { it.value.valid }.maxByOrNull { it.key.priority }?.key
    }
}
