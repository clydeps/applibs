package com.controlj.location

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.Location
import com.controlj.nmea.GpsFix
import com.controlj.nmea.GpsStatus

/**
 * A location that also carries GPS status.
 */
interface GpsLocation : Location {
    val status: GpsStatus

    class Impl(location: Location, override val status: GpsStatus) : GpsLocation, Location by location {
        override val altitude: Double =
            if (status.baroAltitude != INVALID_DATA) status.baroAltitude else location.altitude

        override fun toString(): String {
            return Location.toString(this)
        }

        override fun equals(other: Any?): Boolean {
            if(this === other)
                return true
            if(other !is Location || !Location.equals(this, other))
                return false
            when(other) {
                is GpsLocation -> return status == other.status
                else -> return true
            }
        }

        override fun hashCode(): Int {
            return super.hashCode()
        }
    }

    companion object {
        fun of(location: Location, status: GpsStatus): GpsLocation {
            return Impl(location, status)
        }
    }
}

val Location.satellitesUsed: Int
    get() {
        if (this is GpsLocation)
            return status.satellitesUsed
        return when {
            accuracyV < 15.0 -> 5
            accuracyH < 10.0 -> 4
            else -> 0
        }
    }

val Location.satellitesInView: Int
    get() {
        if (this is GpsLocation)
            return status.satellitesInView
        return when {
            accuracyV < 15.0 -> 5
            accuracyH < 10.0 -> 4
            else -> 0
        }
    }

val Location.gpsFix: GpsFix
    get() {
        if (this is GpsLocation)
            return status.gpsFix
        return when {
            accuracyV < 15.0 -> GpsFix.ThreeD
            accuracyH < 10.0 -> GpsFix.TwoD
            else -> GpsFix.None
        }
    }
