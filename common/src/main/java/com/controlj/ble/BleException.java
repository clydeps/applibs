package com.controlj.ble;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 * <p>
 * User: clyde
 * Date: 29/1/17
 * Time: 3:25 PM
 */
abstract public class BleException extends Exception {
	abstract public String getMessage();
}
