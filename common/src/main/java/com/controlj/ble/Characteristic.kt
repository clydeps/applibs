package com.controlj.ble

/**
 * Created by clyde on 2/6/18.
 *
 * The base class of a characteristic
 * @property uuid The UUID identifying this characteristic
 */

abstract class Characteristic(val uuid: String) {

    fun matches(s: String): Boolean {
        return s.equals(uuid, ignoreCase = true)
    }

    override fun toString(): String {
        return "Characteristic $uuid"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Characteristic) return false

        if (uuid != other.uuid) return false

        return true
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }
}
