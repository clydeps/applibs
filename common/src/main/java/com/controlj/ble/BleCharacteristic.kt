package com.controlj.ble

import com.controlj.rx.DisposedSingleEmitter
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter

/**
 * Created by clyde on 9/5/17.
 *
 * This class describes a Bluetooth LE chaaracteristic
 * @param device The BLE device to which the characteristic belongs
 * @param uuid The UUID for the characteristic
 * @property writeType The write type for the characteristic
 */

abstract class BleCharacteristic(open val device: BleDevice, uuid: String) : Characteristic(BleDevice.uuidCanon(uuid)) {
    open var writeType = WriteType.NORMAL

    /**
     * List of descriptors.
     * Populated by descriptor discovery
     */

    var descriptors: List<String>? = null

    /**
     * The BLE write types.
     */
    enum class WriteType {
        NORMAL,     // normal write with acknowledgement
        NO_RESPONSE,    // write with no ack.
        SIGNED
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BleCharacteristic) return false
        if (!super.equals(other)) return false

        if (device != other.device) return false
        if (writeType != other.writeType) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + device.hashCode()
        result = 31 * result + writeType.hashCode()
        return result
    }

}
