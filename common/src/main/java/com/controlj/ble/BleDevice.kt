package com.controlj.ble

import com.controlj.data.CharacteristicData
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.DisposedSingleEmitter
import com.controlj.rx.MainScheduler
import com.controlj.rx.observeBy
import com.controlj.utility.Secretary
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.IOException
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by clyde on 15/4/18.
 */

abstract class BleDevice(val address: String, var name: String) : Comparable<BleDevice> {

    companion object {

        @Suppress("NewApi")
        fun uuidCanon(uuid: String) =
            (if (uuid.length == 4) "0000$uuid-0000-1000-8000-00805F9B34FB" else uuid).uppercase()

        fun getDeviceType(result: BleScanResult): Type = result.type

        val CJ_BEACON_UUID = byteArrayOf(
            0x6D, 0xEA.toByte(), 0x3B, 0x1D, 0x43, 0x65, 0x44, 0x66,
            0x95.toByte(), 0xB9.toByte(), 0xC8.toByte(), 0x23, 0x14, 0xA0.toByte(), 0x80.toByte(), 0x73
        )

    }

    enum class Type(val model: Int, val bootloadable: Boolean = true) {
        UNKNOWN(0, false),
        BLUEMAX(2730),
        TESTDRIVER(6030),
        MONITOR(4350),
        OTA_DFU(0xFFFF, false);
    }

    protected open val maxQueue = 100
    var type = Type.UNKNOWN
    var mtu: Int = 0
        private set
    private var services: List<BleGattService>? = null
    protected var charMap: Map<String, BleCharacteristic>? = null
    internal val secretary by lazy { Secretary(Schedulers.from(Executors.newSingleThreadExecutor()), maxQueue) }
    val connectedCount = AtomicInteger(0)
    var isConnected: Boolean = false

    private fun schedule(name: String, action: () -> Unit) {
        secretary.enqueue(action, name)
    }

    /****************************************
     * Connection code
     ***************************************/
    @Volatile
    private var connectEmitter: ObservableEmitter<BleDevice> = DisposedEmitter()
    private var autoConnect: Boolean = false

    private val connector by lazy {
        BleScanner().adapterState
            .filter {
                it == BleScanner.AdapterState.Ready
            }
            .take(1)
            .flatMap {
                Observable.create<BleDevice> { emitter ->
                    connectEmitter = emitter
                    if (connectedCount.get() != 0) {
                        connectedCount.incrementAndGet()
                        emitter.onNext(this)
                    } else
                        connect(autoConnect)
                }
            }.share()
    }

    fun getConnector(automatic: Boolean): Single<BleDevice> {
        autoConnect = automatic
        return connector
            .retry(1) { logMsg("Retrying connect"); autoConnect.also { autoConnect = false } }
            .take(1).singleOrError().observeOn(MainScheduler())
    }

    fun onConnected() {
        if (connectEmitter.isDisposed)
            disconnect()
        else {
            secretary.clear()
            isConnected = true
            connectedCount.incrementAndGet()
            connectEmitter.onNext(this)
        }
    }

    /****************************************
     * Disconnection code
     ***************************************/
    @Volatile
    private var disconnectEmitter: SingleEmitter<BleDevice> = DisposedSingleEmitter()

    val disconnector: Single<BleDevice>
        get() {
            return Single.create { e ->
                if (connectedCount.get() == 0) {
                    throw IOException("Attempt to disconnect unconnected device")
                }
                if (connectedCount.decrementAndGet() == 0) {
                    disconnectEmitter = e
                    disconnect()
                } else
                    e.onSuccess(this)
            }
        }

    fun onDisconnected() {
        connectedCount.set(0)
        isConnected = false
        val e = IOException("Device disconnected")
        // if any of these subscriptions are active, they should not be at this point
        notificationEmitter.tryOnError(e)
        discoverEmitter.tryOnError(e)
        readEmitter.tryOnError(e)
        writeEmitter.tryOnError(e)
        notificationEnableEmitter.tryOnError(e)
        connectEmitter.tryOnError(e)
        services = null
        charMap = null
        disconnectEmitter.onSuccess(this)
        secretary.clear()
    }

    /****************************************
     * Service discovery code
     ***************************************/
    @Volatile
    private var discoverEmitter: ObservableEmitter<List<BleGattService>> = DisposedEmitter()

    val discoverServices: Single<List<BleGattService>>
        get() {
            services?.let { return Single.just(it) }
            return Observable.create { e: ObservableEmitter<List<BleGattService>> ->
                schedule("discover") {
                    discoverEmitter = e
                    discover()
                }
            }.take(1).singleOrError()
                .map { list ->
                    services = list
                    charMap = list.flatMap { it -> it.characteristics }.map { uuidCanon(it.uuid) to it }.toMap()
                    list
                }
        }

    protected fun onServicesDiscovered(list: List<BleGattService>) {
        discoverEmitter.onNext(list)
        secretary.onComplete()
    }

    protected fun onDiscoveryError(error: String) {
        discoverEmitter.tryOnError(IOException(error))
        secretary.onComplete()
    }

    /****************************************
     * Notifications code
     ***************************************/

    @Volatile
    private var notificationEmitter: ObservableEmitter<CharacteristicData> = DisposedEmitter()
    val notifications: Observable<CharacteristicData> by lazy {
        Observable.create { e: ObservableEmitter<CharacteristicData> ->
            notificationEmitter = e
        }.share()
    }
    private var notificationEnableEmitter: SingleEmitter<Characteristic> = DisposedSingleEmitter()

    fun setNotification(uuid: String, enable: Boolean): Single<Characteristic> {
        return Single.create { emitter ->
            schedule("notificationenable") {
                notificationEnableEmitter = emitter
                val characteristic = getCharacteristic(uuid)
                if (characteristic == null)
                    emitter.onError(IOException("Unknown characteristic $uuid"))
                else
                    setNotification(characteristic, enable)
            }
        }
    }

    protected fun onNotificationSetError(error: String) {
        notificationEnableEmitter.tryOnError(IOException(error))
        secretary.onComplete()
    }

    protected fun onNotificationSet(bleCharacteristic: BleCharacteristic) {
        notificationEnableEmitter.onSuccess(bleCharacteristic)
        secretary.onComplete()
    }

    fun getService(uuid: String): BleGattService? {
        return services?.filter { it.uuid.equals(uuid, true) }?.firstOrNull()
    }

    fun getCharacteristic(uuidString: String): BleCharacteristic? {
        services?.let {
            return getBleCharacteristic(uuidCanon(uuidString))
        }
        return null
    }

    fun onNotification(characteristic: BleCharacteristic, data: ByteArray) {
        notificationEmitter.onNext(CharacteristicData(characteristic, data))
    }

    protected fun onNotificationError(error: String) {
        logMsg("onNotificationError $error")
    }

    /****************************************
     * Descriptor discovery code
     ***************************************/

    private var descriptorEmitter: SingleEmitter<List<String>> = DisposedSingleEmitter()

    abstract fun startReadDescriptors(characteristic: BleCharacteristic)

    fun discoverDescriptors(characteristic: BleCharacteristic): Single<List<String>> {
        return Single.create { emitter ->
            secretary.enqueue({
                descriptorEmitter = emitter
                startReadDescriptors(characteristic)
            }, "discoverDescriptors")
        }
    }

    fun onDescriptorsRead(characteristic: BleCharacteristic, descriptors: List<String>) {
        characteristic.descriptors = descriptors
        descriptorEmitter.onSuccess(descriptors)
        secretary.onComplete()
    }

    fun onDescriptorsError(message: String) {
        descriptorEmitter.tryOnError(IOException(message))
        secretary.onComplete()
    }


    /****************************************
     * Reader code
     ***************************************/

    private var readEmitter: SingleEmitter<CharacteristicData> = DisposedSingleEmitter()
    private var expectedReadCharacteristic: Characteristic? = null

    fun getReader(uuid: String): Single<CharacteristicData> {

        return Single.create {
            schedule("read $uuid") {
                readEmitter = it
                val characteristic = getCharacteristic(uuid)
                if (characteristic == null) {
                    it.onError(IOException("Unknown characteristic $uuid"))
                } else {
                    expectedReadCharacteristic = characteristic
                    readCharacteristic(characteristic)
                }
            }
        }
    }

    protected fun onReadCharacteristic(characteristic: BleCharacteristic, data: ByteArray) {
        if (characteristic == expectedReadCharacteristic) {
            readEmitter.onSuccess(CharacteristicData(characteristic, data))
            secretary.onComplete()
        }
    }

    protected fun onReadError(characteristic: BleCharacteristic, error: String) {
        logMsg("Read error: $error")
        if (characteristic == expectedReadCharacteristic) {
            readEmitter.tryOnError(IOException(error))
            secretary.onComplete()
        }
    }

    /****************************************
     * Writer code
     ***************************************/

    private var writeEmitter: SingleEmitter<Characteristic> = DisposedSingleEmitter()
    private var expectedWriteCharacteristic: Characteristic? = null

    fun getWriter(
        uuid: String,
        data: ByteArray,
        @Suppress("UNUSED_PARAMETER") type: BleCharacteristic.WriteType = BleCharacteristic.WriteType.NORMAL
    ): Single<Characteristic> {
        return Single.create<Characteristic> { emitter ->
            val characteristic = getCharacteristic(uuid)
            if (characteristic == null)
                emitter.onError(IOException("Unknown characteristic $uuid"))
            else {
                //characteristic.writeType = type
                schedule("write $uuid") {
                    expectedWriteCharacteristic = characteristic
                    writeEmitter = emitter
                    writeCharacteristic(characteristic, data)
                }
            }
        }
    }

    protected fun onWriteError(error: String) {
        logMsg("onWriteError: $error")
        writeEmitter.onError(IOException(error))
        secretary.onComplete()
    }

    protected fun onWriteCharacteristic(bleCharacteristic: BleCharacteristic?) {
        if (bleCharacteristic != null && bleCharacteristic == expectedWriteCharacteristic) {
            writeEmitter.onSuccess(bleCharacteristic)
            secretary.onComplete()
        }
    }

    /****************************************
     * MTU code
     ***************************************/

    private var mtuEmitter: SingleEmitter<Int> = DisposedSingleEmitter()

    fun setMtu(reqMtu: Int): Single<Int> {
        return Single.create { e: SingleEmitter<Int> ->
            schedule("mtu $reqMtu") {
                mtuEmitter = e
                requestMtu(reqMtu)
            }
        }
    }

    protected fun onMtuChange(mtu: Int) {
        this.mtu = mtu
        mtuEmitter.onSuccess(mtu)
        secretary.onComplete()
    }

    protected fun onMtuError(message: String) {
        mtuEmitter.tryOnError(IOException(message))
        secretary.onComplete()
    }

    /**
     * Getting the RSSI of a connected device
     */

    private var rssiEmitter: ObservableEmitter<Int> = DisposedEmitter()

    val rssiObservable: Observable<Int> = Observable.create { e ->
        val disposable = Observable.interval(5, TimeUnit.SECONDS, MainScheduler())
            .observeBy {
                schedule("rssi") {
                    triggerRssiRead()
                }
            }
        e.setCancellable { disposable.dispose() }
        rssiEmitter = e
    }.share()

    protected fun onRssiRead(rssi: Int) {
        rssiEmitter.onNext(rssi)
        secretary.onComplete()
    }

    protected fun onRssiError(message: String) {
        rssiEmitter.onError(IOException(message))
        secretary.onComplete()
    }

    protected fun getBleCharacteristic(uuid: String) = charMap?.get(uuidCanon(uuid))

    override operator fun compareTo(other: BleDevice): Int {
        if (address != other.address)
            return address.compareTo(other.address)
        return if (name != other.name) name.compareTo(other.name) else 0
    }

    override fun equals(other: Any?): Boolean {
        return other is BleDevice && address == other.address && name == other.name
    }

    override fun toString(): String {
        return name
    }

    override fun hashCode(): Int {
        return address.hashCode() + name.hashCode()
    }

    /**
     * Enable or disable a notification
     *
     * @param characteristic The characteristic to be notified
     * @param enable
     */
    protected abstract fun setNotification(characteristic: BleCharacteristic, enable: Boolean)

    /**
     * Request read of a characteristic.
     *
     * @param characteristic
     */
    protected abstract fun readCharacteristic(characteristic: BleCharacteristic)

    /**
     * Request write to a characteristic.
     *
     * @param characteristic
     * @param data
     */
    protected abstract fun writeCharacteristic(characteristic: BleCharacteristic, data: ByteArray)

    /**
     * Connect this device
     * @param automatic
     */
    protected abstract fun connect(automatic: Boolean)

    /**
     * disconnect this device
     */
    open fun disconnect() {
        notificationEmitter.onComplete()
        rssiEmitter.onComplete()
    }

    /**
     * Discover services
     */
    protected abstract fun discover()

    /**
     * Request setting mtu
     *
     * @param mtu The desired mtu
     */
    protected abstract fun requestMtu(mtu: Int)

    /**
     * Trigger a read of the RSSI
     */

    protected abstract fun triggerRssiRead()

    /**
     * Set the priority for this connection, if possible.
     * Returns immediately
     *
     * @param priority
     * @return true if the request was successful
     */
    abstract fun setPriority(priority: Priority): Boolean

    // the ordinals of these correspond to the Android values
    enum class Priority {
        BALANCED,
        HIGH,
        LOW_POWER
    }

}
