package com.controlj.ble

import java.util.UUID

/**
 * Created by clyde on 9/5/17.
 */

abstract class BleGattService {
    abstract val characteristics: List<BleCharacteristic>
    abstract val uuid: String

    override fun toString(): String {
        return "Service: $uuid:\n" + characteristics.joinToString("\n    ", "    ")
    }

    fun getCharacteristic(uuidString: String): BleCharacteristic? {
        return characteristics.firstOrNull { it.matches(uuidString) }
    }

    fun matches(s: String) = s.equals(uuid, ignoreCase = true)

    fun matches(uuid: UUID) = matches(uuid.toString())

    companion object {

        fun toString(list: List<BleGattService>): String {
            return list.joinToString("")
        }
    }
}
