package com.controlj.ble

import com.controlj.logging.CJLog.debug
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedEmitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.ObservableOnSubscribe
import io.reactivex.rxjava3.core.Scheduler

abstract class BleScanner(private val scheduler: Scheduler) : ObservableOnSubscribe<BleScanResult> {

    enum class AdapterState(val message: String) {
        Unsupported("not supported on this device"),
        Unauthorised("permission is not granted"),
        PoweringOn("adapter not ready"),
        PoweredOff("turned off"),
        Ready("ready")
    }

    private var stateEmitter: ObservableEmitter<AdapterState> = DisposedEmitter()

    /**
     * Listen for adapter state
     */
    val adapterState: Observable<AdapterState> = Observable.create<AdapterState> {
        stateEmitter = it
        onStateChange()
    }.subscribeOn(scheduler).share()

    private var emitter: ObservableEmitter<BleScanResult> = DisposedEmitter()

    init {
        instance = this
    }

    override fun subscribe(e: ObservableEmitter<BleScanResult>) {
        debug("subscribe")
        emitter = e
        if (currentState != AdapterState.Ready)
            e.onError(IllegalStateException("Bluetooth ${currentState.message}"))
        else
            startScan()
    }


    /**
     * called by platform specific code when a new scan result is received
     */
    fun onNextScanResult(result: BleScanResult) {
        getDevice(result)       // update device type
        emitter.onNext(result)
    }

    /**
     * Called when an error occurs during scanning
     */
    fun onScanError(throwable: Throwable) {
        emitter.tryOnError(throwable)
    }

    /**
     * Called by platform specific code when the BT adapter state changes
     */

    protected fun onStateChange() {
        val state = currentState
        logMsg("Adapter state now $state")
        when (state) {
            AdapterState.Unsupported, AdapterState.Unauthorised, AdapterState.PoweredOff -> {
                stateEmitter.onError(IllegalStateException("Bluetooth ${currentState.message}"))
            }
            else -> stateEmitter.onNext(state)
        }
    }

    /**
     * Gets an observable to return BLE devices from a scan.
     * Mulitple advertising records from the same device will be returned only once.
     * The scan will stop after a timeout.
     *
     * @return An observable that delivers Scanned BLE devices.
     */
    val bleScan: Observable<BleScanResult> by lazy {
        Observable.create(this).doFinally { stopScan() }.share().subscribeOn(scheduler)
    }

    /**
     * Check if this device supports Bluetooth LE
     */

    abstract val isBleSupported: Boolean

    /**
     * Get a list of connected devices.
     * @param addresses If non-empty, only devices with an address in this list this will be returned.
     */
    abstract fun getConnectedDevices(vararg addresses: String): List<BleDevice>

    /**
     * Start a Ble scan.
     */
    abstract fun stopScan()

    /**
     * Stop the scan.
     */

    abstract fun startScan()

    /**
     *
     * Get a ble device
     */
    abstract fun getDevice(address: String): BleDevice?


    /**
     * Get one from a scanresult. Has the advantage of setting the type
     */

    fun getDevice(scanResult: BleScanResult): BleDevice? {
        return getDevice(scanResult.address)?.also { it.type = scanResult.type }
    }

    /**
     * Get the current state
     */

    protected abstract val currentState: AdapterState

    fun reset() {

    }

    fun init() {
        instance = this
    }

    companion object {
        private lateinit var instance: BleScanner

        operator fun invoke() = instance

        val SCAN_DURATION = 10 // seconds
        val RATIONALE = """
            Location permission is required to connect to Bluetooth LE devices.
            Location data is also used to measure groundspeed, record aircraft position in logfiles
            (including when the app is in background)
            and display location on a map
        """.trimIndent().replace("\n", " ")
    }
}
