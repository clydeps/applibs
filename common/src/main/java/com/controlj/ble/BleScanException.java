package com.controlj.ble;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 * <p>
 * User: clyde
 * Date: 29/1/17
 * Time: 3:26 PM
 */
public class BleScanException extends BleException {

	public BleScanException(ScanError scanError) {
		this.scanError = scanError;
	}

	public enum ScanError {
        UNKNOWN_ERROR("Unknown error"),
	    SCAN_ALREADY_STARTED("Scan already started"),
        UNSUPPORTED_FEATURE("Unsupported feature"),
        APP_REGISTRATION_FAILED("App registration failed"),
        SCAN_INTERNAL_ERROR("Internal error"),
		BLUETOOTH_NOT_AVAILABLE("Bluetooth not available"),
		BLUETOOTH_NOT_ENABLED("Bluetooth disabled"),
		BLUETOOTH_PERMISSION_REQUIRED("Bluetooth permission required"),
		LOCATION_PERMISSION_REQUIRED("Location permission required"),
		LOCATION_NOT_ENABLED("Location services disabled");

		String message;

		ScanError(String message) {
			this.message = message;
		}
	}

	private final ScanError scanError;

	@Override
	public String getMessage() {
		return scanError.message;
	}

	public ScanError getError() {
		return scanError;
	}
}
