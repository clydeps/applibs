package com.controlj.ble

import com.controlj.utility.ageSeconds
import org.threeten.bp.Instant

/**
 * Created by clyde on 15/4/18.
 */

abstract class BleScanResult(
    val address: String,
    var name: String,
    var rssi: Int = -200
) : Comparable<BleScanResult> {


    open val connectable: Boolean = true
    open val isConnected: Boolean = false
    open val timeStamp: Instant = Instant.now()
    val agedRssi: Int
        get() {
            return when {
                rssi == 127 -> -200
                timeStamp.ageSeconds > 5 -> -200
                else -> rssi
            }
        }

    /**
     * Get an index into the signal bar array based on RSSI.
     *
     * @return
     */
    val signal: Int
        get() {
            var signal = agedRssi + 110
            if (signal < 0)
                return 0
            signal = (signal * signalBarNames.size + 75 / 2) / 75
            if (signal >= signalBarNames.size)
                signal = signalBarNames.size - 1
            return signal
        }

    // get the type of this result
    open val type: BleDevice.Type by lazy { getType(this) }

    // is this a beacon advertisement?
    open val isBeacon: Boolean by lazy {
        getManufacturerData(BleConstants.APPLE_MANUF_ID).run {
            (size == 23 && this[0] == 0x02.toByte() && this[1] == 0x15.toByte())
        }
    }
    open val signalText: String
        get() {
            return when {
                isConnected -> "Connected"
                address == "DEMO" -> "Available"
                else -> if (agedRssi <= -200) "Offline" else rssi.toString() + " dbm"
            }
        }


    override fun compareTo(other: BleScanResult): Int {
        return address.compareTo(other.address)
    }

    open fun getManufacturerData(manuf: Int): ByteArray = byteArrayOf()

    /**
     * Return a list of the manufacturer IDs that are represented in the manufacturer data.
     * This implementation is naive and should be overridden in the platform specific code.
     */
    open val manufacturers: Iterable<Int>
        get() = (0..65535).filter { getManufacturerData(it).isNotEmpty() }

    override fun toString(): String {
        return "BleScanResult(address='$address', name='$name', rssi=$rssi, type=$type)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BleScanResult) return false

        if (isConnected != other.isConnected) return false
        if (address != other.address) return false
        if (name != other.name) return false
        if (rssi != other.rssi) return false
        if (timeStamp != other.timeStamp) return false

        return true
    }

    override fun hashCode(): Int {
        return address.hashCode()
    }

    init {
        if (name.isBlank() || name == "Unknown")
            name = nameCache.get(address) ?: "Unknown"
        else
            nameCache.put(address, name)
    }

    companion object {
        val signalBarNames = arrayOf("signal_0bar", "signal_1bar", "signal_2bar", "signal_3bar", "signal_4bar")
        // maintain a cache of names for devices that broadcast differently

        private val nameCache = mutableMapOf<String, String>()

        private fun getType(result: BleScanResult): BleDevice.Type {
            var data = result.getManufacturerData(BleConstants.CJ_MANUF_ID)
            if (data.size >= 2) {
                val value = (data[0].toInt() and 0xFF) + ((data[1].toInt() and 0xFF) shl 8)
                return BleDevice.Type.values().find { it.model == value } ?: BleDevice.Type.UNKNOWN
            }
            data = result.getManufacturerData(BleConstants.APPLE_MANUF_ID)
            if (data.size == 23 && data[0] == 0x02.toByte() && data[1] == 0x15.toByte()) {
                if (BleDevice.CJ_BEACON_UUID.contentEquals(data.copyOfRange(2, 18))) {
                    val value = (data[19].toInt() and 0xFF) + ((data[18].toInt() and 0xFF) shl 8)
                    return BleDevice.Type.values().find { it.model == value } ?: BleDevice.Type.UNKNOWN
                }
            }
            return BleDevice.Type.UNKNOWN
        }
    }
}
