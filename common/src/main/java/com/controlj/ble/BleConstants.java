package com.controlj.ble;

/**
 * Created by clyde on 9/5/17.
 */

public class BleConstants {
  static public int CJ_MANUF_ID = 0x472;      // Control-J's manufacturer ID
  static public int APPLE_MANUF_ID = 0x4C;      // Apple's manufacturer ID
  static public int BLUEMAX_MODEL = 2730;     // model number for BlueMAX adapter
  static public int TESTDRIVER_MODEL = 6030;     // model number for TestDriver
  static public int OTA_MODEL = 0xFFFF;     // model number for OTA persona
  static public String OTA_NAME = "BlueMAX OTA";  // device name for Bluemax OTA persona
}
