package com.controlj.viewmodel

import com.controlj.ui.DialogData
import com.controlj.ui.DialogItem
import com.controlj.ui.GroupDialogItem
import com.controlj.view.ListPresenter
import io.reactivex.rxjava3.core.Observable

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-07-10
 * Time: 10:17
 */
open class DialogListModel(
        val dialogData: DialogData,
        protected var groups: List<GroupDialogItem> = listOf(GroupDialogItem("").also { it.members.addAll(dialogData.items) })

) : ListPresenter<DialogItem>() {

    override val keySelector: (DialogItem) -> String = { it.text }
    override val hideHeaders: Boolean = groups.size <= 1


    init {
        groups.forEach {
            addSection(it.text, Observable.fromIterable(it.members))
        }
    }

    override fun textValues(item: DialogItem): List<String> {
        return listOf()
    }

    override fun refresh() {
        dialogData.refresh()
        super.refresh()
    }
}
