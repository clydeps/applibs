package com.controlj.viewmodel

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.Position
import com.controlj.rx.DisposedEmitter
import com.controlj.settings.StringStore
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.google.gson.annotations.Expose
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import org.ktorm.schema.typeOf
import java.io.File
import java.lang.reflect.Type

/**
 * Created by clyde on 27/8/18.
 *
 * Superclass for ViewModels
 *
 * Data in the viewmodel will be persisted across application invocations, iff annotated with @Expose.
 *
 */
interface ViewModel {

    /**
     * Implementing this interface marks the viewmodel as not needing Expose annotation
     */
    interface Exposed : ViewModel

    /**
     * Base class for persistable, observable data items.
     */
    open class Data<T: Any>(data: T): ListenableValue<T> {
        @Expose
        open var data: T = data
            set(value) {
                changeEmitter.onNext(Pair(field, value))
                field = value
                emitter.onNext(value)
            }

        /**
         * Override to comply with the listenable interface
         */
        override var value: T
            get() = data
            set(value) {
                data = value
            }
        /**
         * Overload () with the data
         */

        open operator fun invoke() = data

        @Transient
        internal var emitter: ObservableEmitter<T> = DisposedEmitter()

        @Transient
        internal var changeEmitter: ObservableEmitter<Pair<T, T>> = DisposedEmitter()


        @Transient
        private val baseObserver = Observable.create<T> { emitter = it }.share()

        /**
         * Observer for the data. Will emit current data immediately on subscribe, then the new data
         * after each change
         */
        override val listener: Observable<T> get() = baseObserver.startWithItem(data)

        /**
         * Observer for data transitions. Will emit a Pair with the old and new states on each change.
         */
        @Transient
        val changeObserver: Observable<Pair<T, T>> = Observable.create<Pair<T, T>> { changeEmitter = it }.share()
    }

    /**
     * A model for Int data
     */
    open class IntData(data: Int = 0) : Data<Int>(data)

    /**
     * A Model for String data
     */
    open class StringData(data: String = "") : Data<String>(data)

    /**
     * A Model for Boolean data
     */
    open class BooleanData(data: Boolean = false) : Data<Boolean>(data) {
        fun toggle() {
            data = !data
        }
    }

    /**
     * store a field in a lightly encrypted way
     */

    open class Obfuscated(var data: String = "")

    private class ObfuscateSerializer : JsonDeserializer<Obfuscated>, JsonSerializer<Obfuscated> {
        companion object {
            val key = "B4BD4243722D4F11985EA4F8033DE9B64C42FFA530954E8BB7FC76C7D7429F06"

            fun map(s: String): String {
                return String(s.mapIndexed { index, char -> char + key[index % key.length].code }.toCharArray())
            }

            fun unMap(s: String): String {
                return String(s.mapIndexed { index, char -> char - key[index % key.length].code }.toCharArray())
            }
        }

        override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Obfuscated {
            return Obfuscated(unMap(json.asString))
        }

        override fun serialize(src: Obfuscated, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
            return JsonPrimitive(map(src.data))
        }
    }

    /**
     * A Model for Double data
     * @property data The initial data value. Will return INVALID_DATA when disabled.
     * @property enabled Allows overriding the value with INVALID_DATA
     */
    open class DoubleData(data: Double = 0.0) : Data<Double>(data) {

        @Expose
        var enabled: Boolean = true

        override var data: Double
            get() {
                if (enabled)
                    return super.data
                return INVALID_DATA
            }
            set(value) {
                super.data = value
            }
    }

    /**
     * Save this ViewModel to persistent storage.
     * @param   dataStore   Where to save the data
     */
    fun save(dataStore: StringStore) {
        dataStore.putString(PREFIX + this.javaClass.name, getGson().toJson(this))
    }

    /**
     * Save this ViewModel to persistent storage in a file.
     * @param   file   Where to save the data
     */
    fun save(file: File) {
        file.writeText(getGson().toJson(this))
    }

    private fun getGson(): Gson {
        return if (this is Exposed) gsonExposed else gson
    }

    /**
     * Serialisers and deserialisers for Data subclasses. Have not worked out how to write a generic
     * deserialiser yet.
     */

    class StringDeserialiser : JsonDeserializer<StringData> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): StringData {
            val sd = StringData(json.asString)
            return sd
        }
    }

    class IntDeserialiser : JsonDeserializer<IntData> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): IntData {
            return IntData(json.asInt)
        }
    }

    class BooleanDeserialiser : JsonDeserializer<BooleanData> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): BooleanData {
            return BooleanData(json.asBoolean)
        }
    }

    class DoubleDeserialiser : JsonDeserializer<DoubleData> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): DoubleData {
            return DoubleData(json.asDouble)
        }
    }

    class Serialiser : JsonSerializer<Data<out Any>> {
        override fun serialize(src: Data<out Any>, typeOfSrc: Type, context: JsonSerializationContext?): JsonElement {
            val data = src.data
            return when (data) {
                is Boolean -> JsonPrimitive(data)
                is Number -> JsonPrimitive(data)
                is String -> JsonPrimitive(data)
                else -> throw IllegalArgumentException("Invalid data type")
            }
        }
    }

    /**
     * Will be called when this ViewModel's context is being destroyed.
     */
    fun onDestroy() {

    }

    companion object {

        /**
         * The prefix to use when saving data. This will be concatenated with the full class name of the
         * ViewModel subclass.
         */
        private const val PREFIX = "viewmodel_"

        /**
         * The Gson instance configured to save ViewModels.
         */
        val gson = GsonBuilder()
            .registerTypeAdapter(Obfuscated::class.java, ObfuscateSerializer())
            .registerTypeAdapter(StringData::class.java, StringDeserialiser())
            .registerTypeAdapter(IntData::class.java, IntDeserialiser())
            .registerTypeAdapter(BooleanData::class.java, BooleanDeserialiser())
            .registerTypeAdapter(DoubleData::class.java, DoubleDeserialiser())
            .registerTypeAdapter(IntData::class.java, Serialiser())
            .registerTypeAdapter(StringData::class.java, Serialiser())
            .registerTypeAdapter(DoubleData::class.java, Serialiser())
            .registerTypeAdapter(BooleanData::class.java, Serialiser())
            .registerTypeAdapter(Position::class.java, Position.Serializer())
            .excludeFieldsWithoutExposeAnnotation()
            .create()

        /**
         * A gson instance to use where all fields are to be saved.
         */
        private val gsonExposed = GsonBuilder()
            .registerTypeAdapter(Obfuscated::class.java, ObfuscateSerializer())
            .registerTypeAdapter(StringData::class.java, StringDeserialiser())
            .registerTypeAdapter(IntData::class.java, IntDeserialiser())
            .registerTypeAdapter(BooleanData::class.java, BooleanDeserialiser())
            .registerTypeAdapter(DoubleData::class.java, DoubleDeserialiser())
            .registerTypeAdapter(IntData::class.java, Serialiser())
            .registerTypeAdapter(StringData::class.java, Serialiser())
            .registerTypeAdapter(DoubleData::class.java, Serialiser())
            .registerTypeAdapter(BooleanData::class.java, Serialiser())
            .registerTypeAdapter(Position::class.java, Position.Serializer())
            .create()

        private fun <T : ViewModel> getGson(clazz: Class<T>): Gson {
            return if (Exposed::class.java.isAssignableFrom(clazz)) gsonExposed else gson
        }

        fun <T> get(value: String, type: Type): T {
            return gson.fromJson(value, type)
        }

        inline fun <reified T : Enum<T>> StringData.getEnum(): T {
            return try {
                gson.fromJson(data, typeOf<T>())
            } catch (ex: java.lang.Exception) {
                T::class.java.enumConstants.first()
            }
        }

        inline fun <reified T : Enum<T>> StringData.putEnum(value: T) {
            data = gson.toJson(value)
        }

        /**
         * Create an instance of a ViewModel subclass. This will either restore from persistent
         * storage, or if not previously persisted, instantiate a default object.
         */
        fun <T : ViewModel> load(dataStore: StringStore, clazz: Class<T>): T {
            return try {
                val s = dataStore.getString(PREFIX + clazz.name)
                getGson(clazz).fromJson(s, clazz)
            } catch (ex: Exception) {
                clazz.getConstructor().newInstance()
            }
        }

        /**
         * Load a viewmodel from a given file.
         */
        fun <T : ViewModel> load(file: File, clazz: Class<T>): T {
            return try {
                getGson(clazz).fromJson(file.readText(), clazz)
            } catch (ex: Exception) {
                clazz.getConstructor().newInstance()
            }
        }

        inline fun <reified T : ViewModel> load(file: File): T {
            return load(file, T::class.java)
        }
    }
}
