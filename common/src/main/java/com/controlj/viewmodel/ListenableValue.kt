package com.controlj.viewmodel

import com.controlj.rx.DisposedEmitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter

/**
 * A value that can be read, set and listened to for changes.
 */
interface ListenableValue<T : Any> {
    var value: T

    val listener: Observable<T>

}

open class Listenable<T : Any>(initialValue: T) : ListenableValue<T> {
    override var value: T = initialValue
        set(value) {
            field = value
            emitter.onNext(value)
        }

    private var emitter: ObservableEmitter<T> = DisposedEmitter()

    override val listener: Observable<T> = Observable.create<T> { e ->
        emitter = e
    }.share()
}
