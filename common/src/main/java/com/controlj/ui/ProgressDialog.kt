package com.controlj.ui

import com.controlj.data.Progress
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.MainScheduler
import com.controlj.rx.observeOnMainBy
import com.controlj.utility.Units
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 * Created by clyde on 1/7/17.
 */

class ProgressDialog(title: String, cancelable: Boolean = false, count: Int = -1) :
        DialogData(Type.PROGRESS, if (count < 0) title else Units.getQuantity(title, count)) {
    internal val progress: ProgressDialogItem = ProgressDialogItem()
    internal val status = StatusDialogItem()

    init {
        addItem(status)
        addItem(progress)
        if (cancelable)
            negative = CANCEL
    }

    fun onProgress(text: String) {
        status.text = text
        refresh()
    }

    fun onProgress(prog: Progress) {
        progress.progress = prog.progress
        status.text = prog.data
        status.busy = !prog.isComplete
        refresh()
        if (prog.isComplete)
            onComplete()
    }

    fun onComplete() {

    }

    private var disposable: Disposable = Disposable.disposed()

    override fun onClosed() {
        disposable.dispose()
        super.onClosed()
    }

    /**
     * Run the given observable, while showing this progress dialog and updating it
     * with progress reports. If the CANCEL button is pressed, dispose the observable - this should
     * interrupt the task if possible
     */
    fun run(observable: Observable<Progress>): Single<ButtonDialogItem> {
        return singleObserver
                .doOnSubscribe {
                    disposable = observable
                            .subscribeOn(Schedulers.io())
                            .observeOnMainBy (
                                    onNext = {
                                        onProgress(it)
                                    },
                                    onError = {
                                        logMsg(it.toString())
                                        this.dismiss()
                                        UiAction.toast(title + " failed:" + it.message)
                                    },
                                    onComplete = {
                                        UiAction.toast("$title Completed")
                                        dialogEmitter.onSuccess(OK)
                                    }
                            )
                }
                .observeOn(MainScheduler.instance)
    }
}
