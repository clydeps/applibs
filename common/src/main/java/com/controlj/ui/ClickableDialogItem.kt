package com.controlj.ui

import com.controlj.graphics.TextStyle

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-03-28
 * Time: 06:34
 */
open class NumericDialogItem(
        text: String = "",
        textStyle: TextStyle = TextStyle.DEFAULT,
        image: String = "",
        action: ((DialogItem) -> Boolean)? = null
) : DialogItem(text, textStyle, image, action) {
    override val style = Style.NUMERIC
}