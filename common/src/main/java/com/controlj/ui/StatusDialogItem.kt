package com.controlj.ui

import com.controlj.graphics.TextStyle

/**
 * A text field with a busy indicator.
 */

open class StatusDialogItem(
        text: String = " ",
        textStyle: TextStyle = TextStyle.LEFT_ALIGNED,
        action: ((DialogItem) -> Boolean)? = null
):DialogItem(text, textStyle, action=action) {
    open var busy: Boolean = false
}