package com.controlj.ui

import com.controlj.framework.SoundPlayer
import com.controlj.graphics.CColor
import com.controlj.graphics.CRect
import com.controlj.graphics.GraphicsFactory
import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedSingleEmitter
import com.controlj.rx.MainScheduler
import com.controlj.view.MenuEntry
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.core.SingleOnSubscribe

/**
 * Created by clyde on 13/6/17.
 */

open class DialogData(
    val type: Type,
    open var title: String,
    vararg buttons: ButtonDialogItem
) : SingleOnSubscribe<ButtonDialogItem>, UiAction {

    /**
     * Should a notification be shown if this dialog is queued while the application is backgrounded?
     */

    open val notifyWhenInBackground = false

    /**
     * Should the dialog title be spoken aloud if a notification is posted?
     */

    open val speakTitle = false

    /**
     * The sound to be played when this dialog is displayed
     */
    open var sound: String? = null

    /**
     * True if this dialog is busy. This will typically result in a spinner being displayed over it
     */
    open var isBusy = false

    /**
     * Default width in points.
     */

    open var dialogWidth = 360.0

    /**
     * Size of image, if present.
     */
    open var imageSize = 0.0

    /**
     * True if there should be a close button top right
     */

    open val closeButton = false

    /**
     * The optional button representing a positive action to close the dialog. Usually [OK].
     */
    open var positive: ButtonDialogItem? = buttons.getOrNull(0)

    /**
     * The optional button to close the dialog without saving, often is [CANCEL]
     */
    open var negative: ButtonDialogItem? = if (buttons.size > 1) buttons.getOrNull(buttons.size - 1) else null

    /**
     * An optional neutral action button.
     */
    var neutral: ButtonDialogItem? = if (buttons.size > 2) buttons[1] else null

    /**
     * A convenience list of buttons, ordered as we want them laid out
     */

    val buttonList: List<ButtonDialogItem> by lazy { listOfNotNull(negative, neutral, positive) }

    /**
     * The controller currently showing this dialog.
     */
    var shower: DialogShower? = null
        set(value) {
            field = value
            if (value == null)
                onClosed()
            else
                onShown()
        }

    open val labelWidth: Double
        get() {
            // calculate desired label width
            return items.filter { it.style.labelled }
                .map { GraphicsFactory().getTextSize(it.text, it.textStyle).width }.maxOrNull()
                ?: 0.0
                    .coerceAtMost(dialogWidth / 2.0)
        }

    open val isShowing: Boolean
        get() = !dialogEmitter.isDisposed

    /**
     * The list of [DialogItem]s in this dataset.
     */
    open var items = mutableListOf<DialogItem>()

    protected var dialogEmitter: SingleEmitter<ButtonDialogItem> = DisposedSingleEmitter()

    open val singleObserver: Single<ButtonDialogItem> by lazy {
        Single.create(this)
            .onErrorReturnItem(CANCEL)
            .toObservable()
            .share()
            .singleOrError()
            .doOnSuccess { logMsg("Dialog $title closed with ${it.text}") }
            .doFinally { closeView() }
            .subscribeOn(MainScheduler())
            .unsubscribeOn(MainScheduler())
    }

    interface DialogShower {
        /**
         * Refresh the dialog, i.e. update the displayed data from the model
         */
        fun refresh(source: DialogItem? = null)

        /**
         * Dismiss the dialog
         */
        fun dismiss()

        /**
         * Show a menu with the supplied [title] and list of [items]. Anchor to the [source] if provided.
         */
        fun showMenu(title: String, items: List<MenuEntry>, source: DialogItem?) {
            TODO("Not implemented")
        }

        /**
         * Show a textbox for data entry, anchored to the [source] position, with initial [value] and
         * using the [style] to control the presentation. Return a Single that will deliver an updated value.
         * A [timeout] may be supplied to have the textbox disappear after inactivity. The time is in ms
         */
        fun showTextBox(
            title: String,
            value: String,
            style: DialogItem.InputStyle,
            source: CRect,
            timeout: Long = 0
        ): Single<String> {
            TODO("not implemented")
        }
    }

    enum class Type(val closeable: Boolean) {
        ALERT(true), // May be shown as system notification, or modal dialog
        PROGRESS(true), // showing progress
        MODAL(false), // shown only as modal dialog
        CLOSEABLE(true)
    }

    /**
     *
     * Called when the dialog is subscribed to.
     *
     */

    override fun subscribe(e: SingleEmitter<ButtonDialogItem>) {
        dialogEmitter = e
        e.setCancellable(this::dismiss)
        logValues("Open")
        if (!UiAction.sendAction(this))
            dialogEmitter.onSuccess(CANCEL)     // TODO should this be onError?
        else {
            sound?.let { SoundPlayer.playSound(it) }
            if (speakTitle)
                SoundPlayer.speak(title)
        }
    }

    fun setItems(items: Iterable<DialogItem>): DialogData {
        this.items.clear()
        this.items.addAll(items)
        return this
    }

    open fun addItem(item: DialogItem): DialogData {
        items.add(item)
        return this
    }

    open fun addItem(position: Int, item: DialogItem): DialogData {
        items.add(position, item)
        return this

    }

    /**
     * Called when a button in the dialog is pressed. If the dialogitem associated with that
     * button approves, the dialog will be closed, and the subscriber notified which button was
     * pressed.
     */
    open fun onItemPressed(dialogItem: ButtonDialogItem) {
        if (dialogItem.run() && !dialogEmitter.isDisposed) {
            dialogEmitter.onSuccess(dialogItem)
        }
    }

    /**
     * Closes the displaying view.
     */
    open fun closeView() {
        //debug("dialog $title closed, shower = $shower")
        shower?.dismiss()
        shower = null
    }

    override fun toString(): String {
        return "DialogData $title:\n    " + items.filter { it.value.isNotBlank() }
            .joinToString("\n    ") { "${it.text}=${it.loggableValue}" }
    }

    fun toFullString(): String {
        return "$title: " +
            listOf(negative, neutral, positive).joinToString(":", "[", "]") { it?.text ?: "" } +
            items.joinToString { it.text + "\n    " }
    }

    protected open fun logValues(comment: String) {
        logMsg("$comment: ${toString()}")
    }

    /**
     * Called when the dialog is shown
     */

    open fun onShown() = Unit

    /**
     * Called when the dialog is closed for any reason
     */
    open fun onClosed() {
        logValues("Closed")
        items.forEach { it.onClosed() }
    }

    /**
     * Subscribe to changes for any items.
     */

    val changeObserver: Observable<DialogItem> by lazy {
        Observable.merge(items.map { it.observe }).share()
    }

    /**
     * called to dismiss the dialog from an external source

     */
    fun dismiss() {
        dialogEmitter.onSuccess(CANCEL)
    }

    private var refreshPending = false

    /**
     * Schedules a refresh of the view presenting this dialog
     */
    open fun refresh(source: DialogItem? = null) {
        if (!refreshPending) {
            refreshPending = true
            MainScheduler {
                refreshPending = false
                shower?.refresh(source)
            }
        }
    }

    companion object {
        val TINTCOLOR = CColor.rgb(0, 122, 255)
        val OK = ButtonDialogItem("OK")
        val CANCEL = ButtonDialogItem("Cancel")

        fun dialog(type: Type, title: String, config: DialogData.() -> Unit): DialogData {
            return DialogData(type, title).apply(config)
        }
    }
}
