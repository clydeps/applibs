package com.controlj.ui

/**
 * Created by clyde on 3/1/19.
 *
 * @param label The label for the item
 * @param inputStyle The style for the input.
 * @param image An optional image to show against the item
 * @param action A block to execute when the item is changed.
 */
open class TextValueItem(
        label: String,
        val inputStyle: InputStyle = InputStyle.PLAIN,
        image: String = "",
        val lines: Int = 1,
        action: ((DialogItem) -> Boolean)? = null
) : DialogItem(label, image = image, action = action) {
    override val style: Style = Style.EDITTEXT
    override var hint = inputStyle.hint
    override val loggableValue: String
        get() {
            return when(inputStyle) {
                InputStyle.PASSWORD, InputStyle.VISIBLE_PASSWORD -> "".padEnd(value.length, '*')
                else -> value
            }
        }
}
