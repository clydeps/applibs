package com.controlj.ui

import com.controlj.data.Message
import com.controlj.data.Priority

/**
 * Created by clyde on 2/7/17.
 */

interface MessageSink {
    fun postMessage(message: Message): Boolean
    fun cancelMessage(message: Message)
    fun clearMessages()
}
