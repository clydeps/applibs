package com.controlj.ui

import com.controlj.graphics.TextStyle
import com.controlj.settings.RangeSet
import com.controlj.utility.Units

/**
 * Created by clyde on 18/6/17.
 */
open class RangeUnitDialogItem(
        textStyle: TextStyle = TextStyle.DEFAULT,
        val rangeSet: () -> RangeSet
) : SpinnerDialogItem<Units.Unit>(rangeSet().setting.label, textStyle) {
    init {
        rangeSet().let {
            choices = it.setting.choices
            selected = choices.indexOf(it.setting.value)
        }
    }

    override fun onChanged() {
        val choice = choices.getOrNull(selected) ?: return
        rangeSet().setting.value = choice
        super.onChanged()
    }
}
