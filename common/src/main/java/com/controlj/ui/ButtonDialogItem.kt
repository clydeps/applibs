package com.controlj.ui

import com.controlj.graphics.CColor
import com.controlj.graphics.TextStyle

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 14/5/18
 * Time: 16:38
 */
open class ButtonDialogItem(text: String, textStyle: TextStyle = TextStyle.DEFAULT, image: String = "", action: (DialogItem) -> Boolean = { true }) : DialogItem(text, textStyle, image, action) {
    override val style: Style = Style.BUTTON
    open var tintColor = CColor.rgb(0, 122, 255)
}
