package com.controlj.ui

import com.controlj.graphics.TextStyle

/**
 * Created by clyde on 18/6/17.
 */
open class SpinnerDialogItem<T>(
    text: String = "",
    textStyle: TextStyle = TextStyle.DEFAULT,
    image: String = "",
    action: ((DialogItem) -> Boolean)? = null
) : DialogItem(text, textStyle, image, action) {

    override val style: Style = Style.SPINNER
    open var choices: List<T> = listOf()
        set(value) {
            field = value.toList()
            if (selected >= value.size)
                selected = NO_SELECTION
        }

    override var value: String
        get() = choices.getOrElse(selected) { "" }.toString()
        set(value) {
            selected = choices.map { it.toString() }.indexOf(value)
        }

    open var selected: Int = NO_SELECTION
        set(value) {
            if (field != value) {
                field = value
                onChanged()
            }
        }

    open var selectedItem: T?
        get() {
            return choices.getOrNull(selected)
        }
        set(value) {
            selected = choices.indexOf(value)
        }

    companion object {
        const val NO_SELECTION = -1
    }
}
