package com.controlj.ui

import com.controlj.logging.CJLog.logMsg
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.DisposedSingleEmitter
import com.controlj.rx.MainScheduler
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.core.SingleOnSubscribe
import java.io.File

/**
 * Actions to control the user interface
 */
interface UiAction {
    enum class ToastDuration {
        SHORT,
        LONG
    }

    /**
     * Classes for common actions
     */
    data class Screen(val state: Boolean) : UiAction        // keep screen on

    data class Toast(val message: String, val duration: ToastDuration) : UiAction   // toast
    data class OpenUrl(val url: String) : UiAction          // open a URL
    data class OpenFile(val file: File, val mimeType: String) : UiAction // open a file

    data class MenuAction(val name: String = "Hamburger") : UiAction

    /**
     * A class to request an action with a result
     */

    abstract class Result<T : Any> : SingleOnSubscribe<T>, UiAction {
        var emitter: SingleEmitter<T> = DisposedSingleEmitter()
            private set

        override fun subscribe(emitter: SingleEmitter<T>) {
            this.emitter = emitter
            if (!sendAction(this)) {
                logMsg("subscribe to $this failed")
                emitter.onError(Throwable("No subscriber for action"))
            }
        }

        fun succeed(result: T) {
            emitter.onSuccess(result)
        }

        fun fail(error: Throwable) {
            emitter.onError(error)
        }
    }

    companion object {

        private var emitter: ObservableEmitter<UiAction> = DisposedEmitter()

        /**
         * Consumers of UiActions use this observer to listen.
         */
        val observer: Observable<UiAction> = Observable.create<UiAction> { emitter = it }
            .observeOn(MainScheduler.instance)
            .share()

        /**
         * Send an action to listeners.
         * @param action The action to be sent
         * @return true if there is at least one subscriber, false if nobody is listening
         */
        fun sendAction(action: UiAction): Boolean {
            if (emitter.isDisposed) return false
            emitter.onNext(action)
            return true
        }

        /**
         * Display a toast message
         * @param message The message to display
         * @param duration The time to show the message, default is LONG
         * @return false if nobody is listening
         */
        fun toast(message: String, duration: ToastDuration = ToastDuration.LONG): Boolean {
            return sendAction(Toast(message, duration))
        }

        /**
         * Display a message from an exception
         */

        fun toast(exception: Throwable) {
            toast("Error: ${exception.message ?: exception.toString()}")
        }

        /**
         * Open a url - will generally invoke a browser
         */

        fun openUrl(url: String): Boolean {
            if (emitter.isDisposed)
                return false
            sendAction(OpenUrl(url))
            return true
        }

        /**
         * Request that a file be opened with an external app
         */

        fun openWithExternalApp(file: File, mimeType: String) {
            sendAction(OpenFile(file, mimeType))
        }

        /**
         * Send a request to keep the screen on.
         * @param state Pass true to request the screen be kept on.
         */
        fun keepScreenOn(state: Boolean) {
            sendAction(Screen(state))
        }
    }
}

