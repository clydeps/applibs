package com.controlj.ui

import com.controlj.widget.CView

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 13/5/18
 * Time: 13:55
 */
class CViewItem(val cView: CView, name: String): DialogItem(name) {
    override val style: Style = Style.CVIEW
    override fun onClosed() {
        cView.parent = null
    }
}
