package com.controlj.ui

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 14/5/18
 * Time: 16:12
 */
class ProgressDialogItem(text: String = "") : DialogItem(text) {
    override val style = Style.PROGRESS
    var progress: Int = 0
}
