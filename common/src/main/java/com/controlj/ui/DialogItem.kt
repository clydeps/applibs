package com.controlj.ui

import com.controlj.graphics.TextStyle
import com.controlj.rx.DisposedEmitter
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 14/5/18
 * Time: 15:53
 *
 * A generalised dialog item. The default type is just a plain text item. Subclasses enrich this behaviour.
 * @param text The text to show for the item
 * @param textStyle The style of the text
 * @param image An optional image to show with the text
 * @param action An action to take when the item is clicked or otherwise changed
 */
open class DialogItem(
        open var text: String = "",
        open var textStyle: TextStyle = TextStyle.DEFAULT,
        open var image: String = "",
        open var action: ((DialogItem) -> Boolean)? = null
): Comparable<DialogItem> {
    open val style: Style = Style.PLAIN
    open var value: String = ""
    open var description: String = ""
    open val isEnabled = true
    open var group: GroupDialogItem? = null     // group if it has one
    var maxval = 0.0
    var minval = 0.0
    var key: String = ""                    // used as a key into a map of values
    open val loggableValue: String          // can be overridden if the value should not appear in log files
        get() = value

    open var hint = text

    var intValue
        get() = value.toIntOrNull() ?: 0
        set(intVal) {
            value = intVal.toString()
        }
    open var doubleValue
        get() = value.toDoubleOrNull() ?: 0.0
        set(doubleVal) {
            value = doubleVal.toString()
        }
    private var emitter: ObservableEmitter<DialogItem> = DisposedEmitter()
    val observe: Observable<DialogItem> = Observable.create { emitter = it }

    // The dialog item styles. A [labelled] item will have a label on the left and data on the right.
    enum class Style(var labelled: Boolean = false) {
        PLAIN,
        NUMERIC(true),        // show numeric (right justified) value, non-editable
        CHECKBOX(true),
        SPINNER(true),        // drop-down choice
        RADIO(true),
        CHOICE,
        PROGRESS,
        BUTTON,
        EDITTEXT(true),       // editable generic text
        CVIEW,
        CLICKABLE,
        SEPARATOR,      // separator line
        GROUP,          // used in settings views, group name
        UNITVALUE(true),      // value with selectable unit, editable
    }

    enum class InputStyle(
            var hint: String,                       // a hint string to show
            val signed: Boolean = false,            // if numeric, whether signed or not
            val autoCorrect: Boolean = false,       // should autocorrect be shown?
            val validChars: Set<Char> = setOf(),   // list of valid chars - empty means any allowed
            val rightAlign: Boolean = false        // should the the text be right-aligned?
    ) {
        PLAIN("", autoCorrect = true),
        NOCORRECT(""),
        PASSWORD("*******"),
        EMAIL("user@example.com"),
        INTEGER("123", validChars = ('0'..'9').toSet(), rightAlign = true),
        SIGNEDINT("-123", signed = true, validChars = ('0'..'9').toSet() + '-', rightAlign = true),
        DECIMAL("0.0", validChars = ('0'..'9').toSet() + '.', rightAlign = true),
        SIGNEDDECIMAL("0.0", signed = true, validChars = ('0'..'9').toSet() + '.' + '-', rightAlign = true),
        PHONE("+1 123 456 789"),
        VISIBLE_PASSWORD("password"),
        MULTI_LINE("", autoCorrect = true)
    }

    open fun onChanged() {
        run()
        emitter.onNext(this)
    }

    fun run(): Boolean {
        action?.let {
            //logMsg("Run action $this")
            return it(this)
        }
        return true
    }

    override fun toString(): String {
        return "$style:$text"
    }

    /**
     * Called on each item when the dialog is closed.
     */
    open fun onClosed() {

    }

    override fun compareTo(other: DialogItem): Int {
        return 0
    }
}
