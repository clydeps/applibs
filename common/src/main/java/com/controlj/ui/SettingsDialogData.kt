package com.controlj.ui

import com.controlj.graphics.TextStyle
import com.controlj.settings.ActionSetting
import com.controlj.settings.BooleanSetting
import com.controlj.settings.ChoiceSetting
import com.controlj.settings.DoubleSetting
import com.controlj.settings.IntSetting
import com.controlj.settings.Setting
import com.controlj.settings.StringSetting
import com.controlj.settings.ValueSetting
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable

/**
 * Created by clyde on 3/1/19.
 */
open class SettingsDialogData(
        title: String = "Settings",
        settingGroups: List<Setting.Group>,
        private val showSummaries: Boolean = true,
        vararg buttons: ButtonDialogItem
) : DialogData(Type.CLOSEABLE, title, *buttons) {
    private var disposable = Disposable.disposed()
    private val summaries = mutableMapOf<Any, DialogItem>()
    override val closeButton: Boolean = true


    val groups: List<GroupDialogItem> =
            settingGroups.map { group ->
                val gdi = GroupDialogItem(group.name)
                addItem(gdi)
                group.members.forEach { setting ->
                    val item = addSetting(setting)
                    item.group = gdi
                    gdi.members.add(item)
                    summaries[setting]?.group = gdi
                }
                gdi
            }.toList()

    private val observables = Observable.merge(settingGroups.map { it.members }.flatten().map { it.observable })

    override fun onShown() {
        disposable = observables
                .subscribe { setting:Setting<*> ->
                    setting.saveValue()
                    refresh()
                }
        super.onShown()
    }

    override fun onClosed() {
        disposable.dispose()
        super.onClosed()
    }

    @Suppress("UNCHECKED_CAST")
    fun addSetting(setting: Setting<*>): DialogItem {
        val item = when (setting) {
            is BooleanSetting -> add(setting)
            is StringSetting -> add(setting)
            is IntSetting -> add(setting)
            is DoubleSetting -> add(setting)
            is ActionSetting -> add(setting)
            //is RangeUnitSetting -> add(setting)
            is ChoiceSetting<*> -> add(setting as ChoiceSetting<Any>)
            is ValueSetting -> add(setting)
            else -> {
                throw IllegalArgumentException("Unknown setting type ${setting.javaClass.simpleName}")
            }
        }
        return add(item, setting.description)
    }

    private fun add(item: DialogItem, description: String?): DialogItem {
        addItem(item)
        if (description != null)
            item.description = description
        if (showSummaries && description != null) {
            val summary = DialogItem(textStyle = TextStyle.SUMMARY)
            addItem(summary)
        }
        return item
    }


    override fun refresh(source: DialogItem?) {
        super.refresh(source)
        summaries.forEach {
            it.value.text = it.key.toString()
        }
    }

    private fun add(setting: ValueSetting): ValueDialogItem {
        return object : ValueDialogItem(setting.units, setting.label) {
            override var doubleValue: Double
                get() = setting.value
                set(value) {
                    if (value != setting.value && setting.validate(value)) {
                        setting.value = value
                        onChanged()
                    }
                }
            override val isEnabled: Boolean
                get() = setting.isEnabled
        }
    }

    private fun add(setting: ActionSetting): DialogItem {
        if(setting.isButton)
            return ButtonDialogItem(setting.label) {
                setting.action()
                true
            }
        return ClickableDialogItem(setting.label) {
            setting.action()
            true
        }
    }

    private fun add(setting: ChoiceSetting<Any>): SpinnerDialogItem<Any> {
        return object : SpinnerDialogItem<Any>(setting.label) {
            override var selected: Int
                get() = setting.choices.indexOf(setting.value)
                set(index) {
                    if (index in (0 until setting.choices.size)) {
                        val value = setting.choices[index]
                        if (value != setting.value && setting.validate(value)) {
                            setting.value = value
                            onChanged()
                        }
                    }
                }
            override val isEnabled: Boolean
                get() = setting.isEnabled

        }.also { it.choices = setting.choices }
    }

    private fun add(setting: DoubleSetting): DialogItem {
        return object : TextValueItem(setting.label, setting.style) {
            override var doubleValue: Double
                get() = setting.value
                set(value) {
                    if (value != setting.value && setting.validate(value)) {
                        setting.value = value
                        onChanged()
                    }
                }
            override val isEnabled: Boolean
                get() = setting.isEnabled

        }
    }

    private fun add(setting: IntSetting): DialogItem {
        return object : TextValueItem(setting.label, DialogItem.InputStyle.SIGNEDINT) {
            override var value: String
                get() = setting.value.toString()
                set(value) {
                    try {
                        val intValue = value.toInt()
                        if (intValue != setting.value && setting.validate(intValue)) {
                            setting.value = intValue
                            onChanged()
                        }
                    } catch (ex: java.lang.NumberFormatException) {
                    }
                }
            override val isEnabled: Boolean
                get() = setting.isEnabled
        }
    }

    private fun add(setting: StringSetting): TextValueItem {
        return object : TextValueItem(setting.label) {
            override var value: String
                get() = setting.value
                set(value) {
                    if (value != setting.value && setting.validate(value)) {
                        setting.value = value
                        onChanged()
                    }
                }
            override val isEnabled: Boolean
                get() = setting.isEnabled
        }
    }

    private fun add(setting: BooleanSetting): CheckboxItem {
        return object : CheckboxItem(setting.label) {
            override var isChecked: Boolean
                get() = setting.value
                set(value) {
                    if (value != setting.value && setting.validate(value)) {
                        setting.value = value
                        onChanged()
                    }
                }
            override val isEnabled: Boolean
                get() = setting.isEnabled
        }
    }
}
