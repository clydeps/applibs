package com.controlj.ui

/**
 * Created by clyde on 2/7/18.
 */
open class CheckboxItem(text: String) : DialogItem(text) {
    override val style = Style.CHECKBOX

    open var isChecked: Boolean = false
        set(value) {
            if (field != value) {
                field = value
                onChanged()
            }
        }

    override var value: String
        get() = isChecked.toString()
        set(value) {
            isChecked = value.toBoolean()
        }
}
