package com.controlj.ui

import com.controlj.graphics.CRect
import com.controlj.rx.DisposedEmitter
import com.controlj.rx.MainScheduler
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-03-22
 * Time: 06:44
 *
 * This object tracks the keyboard state in iOS.
 */
abstract class KeyboardStatus {

    enum class State(val closed: Boolean) {
        CLOSED(true),
        OPENING(false),
        OPEN(false),
        CLOSING(false);
    }

    companion object {
        // the emitter for events

        private var emitter: ObservableEmitter<State> = DisposedEmitter()

        /**
         * Current keyboard state
         */

        var keyboardState = State.CLOSED
            set(value) {
                if (field != value) {
                    field = value
                    emitter.onNext(value)
                }
            }
        /**
         * True if the keyboard is currently visible on screen
         */
        val isKeyboardOpen: Boolean
            get() = !keyboardState.closed

        /**
         * The terminal frame for the keyboard, after any current animations run
         */

        var keyboardFrame: CRect = CRect()

        /**
         * The animation duration of the current open or close event, in seconds
         */

        var animationDuration: Double = 0.0

        /**
         * An object describing the animation curve
         */

        var animationCurve: Any? = null

        /**
         * Observe keyboard open/close events
         */

        val observable = Observable.create<State> { emitter ->
            this.emitter = emitter
            emitter.onNext(keyboardState)
        }
                .observeOn(MainScheduler())
                .share()

        /**
         * Close the keyboard, wait for the keyboard to close and run the action.
         * If already closed it is run immediately.
         * Operates on the MainScheduler.
         * Return true if the keyboard was not open, false if the action is deferred
         */

        @Suppress("CheckResult")
        fun whenClosed(action: () -> Unit): Boolean {
            if (!isKeyboardOpen) {
                action.invoke()
                return true
            }
            observable
                    .filter { it.closed }
                    .take(1)
                    .ignoreElements()
                    .subscribe(action)
            return false
        }
    }
}
