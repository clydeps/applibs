package com.controlj.ui

import com.controlj.graphics.TextStyle

/**
 * Created by clyde on 3/1/19.
 */
open class GroupDialogItem(name: String): DialogItem(name, textStyle = TextStyle.HEADING) {
    override val style = Style.GROUP
    val members = mutableListOf<DialogItem>()
}
