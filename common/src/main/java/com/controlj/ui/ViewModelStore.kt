package com.controlj.ui

import com.controlj.logging.CJLog.logMsg
import com.controlj.settings.DataStore
import com.controlj.viewmodel.ViewModel
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.File
import java.util.concurrent.ConcurrentHashMap

/**
 * A place to store and load viewmodels. The primary store is a directory where viewmodels are stored
 * on a per-class basis. Secondary storage is in a DataStore.
 *
 * There is a single application-wide copy of each viewmodel.
 */
object ViewModelStore {

    /**
     * The last path name of the directory to store viewmodels
     */
    const val DIR_NAME = "viewModels"

    /**
     * Where viewmodel files are stored
     */
    internal lateinit var directory: File

    /**
     * A datastore as a backup storage
     */
    private lateinit var dataStore: DataStore

    /**
     * The viewmodels. One entry per class, mapped by the class itself.
     */
    private val viewModels: ConcurrentHashMap<Class<out ViewModel>, ViewModel> = ConcurrentHashMap()

    /**
     * Must be called before using this factory.
     * @param parentDir The directory in which to store viewmodel files
     */
    fun setup(parentDir: File): ViewModelStore {
        viewModels.clear()
        directory = File(parentDir, DIR_NAME)
        if (!directory.isDirectory && !directory.mkdirs())
            logMsg("Unable to create directory ${directory.path}")
        return this
    }

    /**
     * Legacy setup with a datastore
     */
    fun setup(dataStore: DataStore) {
        this.dataStore = dataStore
    }

    /**
     * Load a viewmodel synchronously. It will be fetched from cache if present, otherwise loaded from its file
     * if present, or a new one created otherwise. Subsequent calls will deliver from cache.
     * @param clazz The class of the viewmodel to be returned.
     *
     */
    @Suppress("UNCHECKED_CAST")
    fun <T : ViewModel> loadViewModel(clazz: Class<T>): T {
        return viewModels.getOrPut(clazz, {
            val file = File(directory, clazz.name)
            if (file.exists())
                ViewModel.load(file, clazz)
            else if (::dataStore.isInitialized)
                ViewModel.load(dataStore, clazz)
            else {
                logMsg("New viewmodel created for ${clazz.name}")
                clazz.getConstructor().newInstance()
            }
        }) as T
    }

    inline fun <reified T : ViewModel> loadViewModel(): T {
        return loadViewModel(T::class.java)
    }


    /**
     * Fetch a ViewModel asynchronously. Recommended when calling from the main thread. Generally will require
     * an observeOn() call to determine the thread to return the result.
     * Executes on an I/O scheduler.
     * @param clazz The class of the viewmodel to return
     * @return A single that will deliver the requested ViewModel object.
     */

    fun <T : ViewModel> fetch(clazz: Class<T>): Single<T> {
        return Single.create<T> { emitter ->
            emitter.onSuccess(loadViewModel(clazz))
        }.subscribeOn(Schedulers.io())
    }

    inline fun <reified T : ViewModel> fetch(): Single<T> {
        return fetch(T::class.java)
    }

    /**
     * Save a single object
     */

    /**
     * Save all viewmodels synchronously.
     */

    fun save() {
        viewModels.values.forEach { it.save() }
    }

    /**
     * Get a list of currently loaded viewmodels.
     */
    override fun toString(): String {
        return viewModels.values.joinToString(
                prefix = "(",
                postfix = ")",
                transform = { it.javaClass.simpleName }
        )
    }
}
fun ViewModel.save() {
    save(File(ViewModelStore.directory, javaClass.name))
}
