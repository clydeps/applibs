package com.controlj.ui

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 14/5/18
 * Time: 16:38
 */
open class SeparatorDialogItem : DialogItem() {
    override val style: Style = Style.SEPARATOR
}
