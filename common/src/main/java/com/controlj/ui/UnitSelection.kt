package com.controlj.ui

import com.controlj.logging.CJLog.logMsg
import com.controlj.utility.Units

/**
 * Created by clyde on 16/8/17.
 */

class UnitSelection(text: String, val defaultUnit: Units.Unit) : SpinnerDialogItem<Units.Unit>(text) {

    val selectedUnit: Units.Unit
        get() = choices.getOrElse(selected, {defaultUnit})

    fun setValue(unit: Units.Unit) {
        val i = choices.indexOf(unit)
        if (i >= 0 && i < choices.size)
            selected = i
        else
            logMsg("Unit %s not in choices", unit)
    }
}
