package com.controlj.ui

import com.controlj.graphics.TextStyle
import com.controlj.utility.Units

/**
 * Created by clyde on 18/6/17.
 *
 * Represents a dialog item with a value and associated unit
 */
open class ValueDialogItem(
        unitList: List<Units.Unit> = listOf(),
        text: String = "",
        textStyle: TextStyle = TextStyle.DEFAULT,
        image: String = "",
        action: ((DialogItem) -> Boolean)? = null) : DialogItem(text, textStyle, image, action) {

    override val style: Style = Style.SPINNER
    internal val _unitList = mutableListOf<Units.Unit>()

    init {
        _unitList.addAll(unitList)
    }

    open var convertedValue: String
        get() {
            return currentUnit.toString(doubleValue)
        }
        set(stringVal)  {
            doubleValue = currentUnit.fromString(stringVal)
        }
    var unitList: List<Units.Unit>
        set(value) {
            _unitList.clear()
            _unitList.addAll(value)
        }
        get() = _unitList

    var selected: Int = 0
        set(value) {
            if (field != value) {
                field = value
                onChanged()
            }
        }

    var currentUnit: Units.Unit
        get() = unitList[selected]
        set(value) {
            selected = _unitList.indexOf(value)
        }
}
