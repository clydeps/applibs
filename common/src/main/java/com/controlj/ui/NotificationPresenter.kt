package com.controlj.ui

import com.controlj.data.Message
import com.controlj.logging.CJLog.logMsg

/**
 * Created by clyde on 17/4/18.
 */
interface NotificationPresenter {

    /**
     * Implemented by concrete classes. Updates the current notification state to reflect the active
     * notifications.
     */
    fun updateNotifications()

    companion object {

        lateinit var instance: NotificationPresenter

        operator fun invoke() = instance

        // currently active notifications
        protected val activeNotifications = mutableSetOf<Message>()

        /**
         * Show a notification.
         */
        fun showNotification(message: Message): Boolean {
            if (activeNotifications.add(message)) {
                logMsg("Showing notification $message")
                instance.updateNotifications()
                return true
            }
            return false
        }

        /** Cancel a notification
         *
         */

        fun cancelNotification(message: Message): Boolean {
            if (activeNotifications.remove(message)) {
                logMsg("Cancelled notification $message")
                instance.updateNotifications()
                return true
            }
            return false
        }

        fun clearNotifications() {
            activeNotifications.clear()
            instance.updateNotifications()
        }

        val messageList: List<Message>
            get() = activeNotifications.sorted()
    }

    fun init() {
        instance = this
    }
}
