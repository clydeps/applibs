package com.controlj.ui;

import com.controlj.ble.BleDevice;

/**
 * Created by clyde on 2/6/17.
 *
 * This interface will be implemented by the application object (or its delegate.) It provides access to
 * operations with system-specific underlying implementations.
 */

public interface BmService {

    void onDestroy();

    BleDevice getDevice(String address);


}
