package com.controlj.utility

import com.controlj.data.DiskFileEntry
import com.controlj.data.FileEntry
import com.controlj.logging.CJLog.debug
import java.io.ByteArrayInputStream
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.UnsupportedEncodingException
import java.net.URLEncoder
import java.util.HashMap

/**
 * Created by clyde on 19/1/17.
 */

open class FileParameter(name: String, var file: FileEntry) : Parameter(name, file.fileName) {
    constructor(name: String, file: File) : this(name, DiskFileEntry(file, file.parentFile.name))

    var fileLength: Long = 0

    val inputStream: InputStream
        get() = file.getInputStream()

    init {
        fileLength = file.length     // get it now, in case it changes, i.e. file being written to.
        debug("filelength for ${file.name} is $fileLength")
    }

    fun formHeader(separator: String): String {
        try {
            return "--" + separator +
                    "\r\nContent-Disposition: form-data; name=\"" + URLEncoder.encode(name, "UTF-8") + "\"" +
                    "; filename=\"" + URLEncoder.encode(value, "UTF-8") + "\"\r\n" +
                    "Content-Type: application/octet-stream\r\n\r\n"
        } catch (e: UnsupportedEncodingException) {
            return ""
        }

    }
}

class FormData(params: Collection<Parameter> = listOf()) {
    private val paramList = ArrayList<Parameter>()
    val parameters: List<Parameter>
        get() = paramList.filter { !(it is FileParameter) }
    val files: List<FileParameter>
        get() = paramList.filterIsInstance<FileParameter>()
    private val map = HashMap<String, Parameter>()

    init {
        paramList.addAll(params)
    }

    /**
     * Get the content type. Simple form if no files, multipart if there are attached files.
     */
    val contentType: String
        get() = if (files.isEmpty()) WWW_FORM_URL else MULTIPART_FORM

    /**
     * Get the length of the post body.
     */
    // add 2 for trailing crlf
    val dataLength: Long
        get() {
            var len: Long = 0
            if (files.isEmpty()) {
                return wwwData().toByteArray().size.toLong()
            }
            for (p in parameters)
                len += p.formPart(SEPARATOR).toByteArray().size.toLong()
            for (p in files)
                len += p.formHeader(SEPARATOR).toByteArray().size.toLong() + p.fileLength + 2
            len += terminator.toByteArray().size.toLong()
            return len
        }

    private val terminator: String
        get() = "--$SEPARATOR--\r\n"

    // called when buffer empty
    // send non-file parameters first
    // all done, send last separator
    val stream: InputStream
        get() = if (files.isEmpty()) ByteArrayInputStream(wwwData().toByteArray()) else object : InputStream() {

            var ps = parameters.iterator()
            var fs = files.iterator()
            var inputStream: InputStream? = null
            var buffer: ByteArray? = null
            var eof = false
            var pos = 0
            var len = 0

            @Throws(IOException::class)
            private fun refill() {
                pos = 0
                len = 0
                if (eof)
                    return
                if (ps.hasNext()) {
                    val p = ps.next()
                    val data = p.formPart(SEPARATOR)
                    buffer = data.toByteArray()
                    len = buffer!!.size
                    return
                }
                if (inputStream != null) {
                    if (buffer!!.size < BUFLEN)
                        buffer = ByteArray(BUFLEN)
                    len = inputStream!!.read(buffer!!)
                    if (len > 0)
                        return
                    inputStream!!.close()
                    inputStream = null
                    System.arraycopy("\r\n".toByteArray(), 0, buffer!!, 0, 2)
                    len = 2
                    return
                }
                if (fs.hasNext()) {
                    val fp = fs.next()
                    val data = fp.formHeader(SEPARATOR).toByteArray()
                    len = data.size
                    if (buffer == null || data.size > buffer!!.size)
                        buffer = data
                    else
                        System.arraycopy(data, 0, buffer!!, 0, data.size)
                    inputStream = fp.inputStream
                    return
                }
                buffer = terminator.toByteArray()
                len = buffer!!.size
                eof = true
            }

            @Throws(IOException::class)
            override fun close() {
                if (inputStream != null) {
                    inputStream!!.close()
                    inputStream = null
                }
            }

            @Throws(IOException::class)
            override fun read(dst: ByteArray, offset: Int, length: Int): Int {
                var actual = length
                if (pos == len)
                    refill()
                if (pos == len)
                    return -1
                if (actual > len - pos)
                    actual = len - pos
                System.arraycopy(buffer!!, pos, dst, offset, actual)
                pos += actual
                return actual
            }

            @Throws(IOException::class)
            override fun read(): Int {
                if (pos == len)
                    refill()
                return if (pos == len) -1 else buffer!![pos++].toInt()
            }
        }

    fun addAll(parms: Collection<Parameter>) {
        paramList.addAll(parms)
    }

    fun add(p: Parameter) {
        paramList.add(p)
        map[p.name] = p
    }

    fun add(name: String, value: String) {
        add(Parameter(name, value))
    }

    fun wwwData(): String {
        val sb = StringBuilder()

        for (p in parameters) {
            if (sb.isNotEmpty())
                sb.append('&')
            sb.append(p.URLEncode())
        }
        return sb.toString()
    }

    override fun toString(): String {
        return wwwData()
    }

    companion object {
        internal val SEPARATOR = "3C3F786D6C2076657273696F6E2E???-???220656E636F64696E673D662D38223F3E0A3C6D616E6966"
        internal val MULTIPART_FORM = "multipart/form-data; boundary=\"$SEPARATOR\""

        internal val WWW_FORM_URL = "application/x-www-form-urlencoded"
        val BUFLEN = 4096
    }
}
