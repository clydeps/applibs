package com.controlj.utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parse coordinates
 */
public class Coordinates {

	private static final String OPTIONAL_WHITESPACE = "\\s*";
	public static final String DEGREE  = "\u00b0";
	private static final String WHITESPACE = "\\s+";
	private double latitude = 0.;
	private double longitude = 0.;
	private static Pattern decimalDegrees, dms, dm, reverseDecimal;

	static {
		reverseDecimal = Pattern.compile(
				"([NSEW])" + OPTIONAL_WHITESPACE
						+ "([+-]?\\d*\\.?\\d+)" + OPTIONAL_WHITESPACE
						+ "([NSEW])" + OPTIONAL_WHITESPACE
						+ "([+-]?\\d*\\.?\\d+)",
				Pattern.CASE_INSENSITIVE);
		decimalDegrees = Pattern.compile(
				"([+-]?\\d*\\.?\\d+)\\s*"
						+ "([NSEW+-]?)\\s*"
						+ "([+-]?\\d*\\.?\\d+)\\s*"
						+ "([NSEW+-]?)",
				Pattern.CASE_INSENSITIVE);
		dms = Pattern.compile(
				"(\\d{1,3})" // degrees, 1-3 digits	- group 1
						+ "(?:" + DEGREE + "|\\s\\s*)" // degree symbol or just whitespace
						+ "(\\d{1,2})" // minutes group 2
						+ "(?:'|\\s\\s*)"
						+ "(\\d*\\.?\\d+)" // seconds, possibly with decimal part group 3
						+ "(?:\"?\\s*)"
						+ "([NSEW]?)" // hemisphere, group 4
						+ OPTIONAL_WHITESPACE
						+ "(\\d{1,3})" // degrees, group 5
						+ "(?:" + DEGREE + "|\\s\\s*)" // degree symbol or just whitespace
						+ "(\\d{1,2})" // minutes, group 6
						+ "(?:'|\\s\\s*)"
						+ "(\\d*\\.?\\d+)" // seconds, group 7
						+ "(?:\"?\\s*)"
						+ "([NSEW]?)", // hemisphere, group 8
				Pattern.CASE_INSENSITIVE);
	}

	private Coordinates() {
	}

	public double getLatitude() {
		return this.latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	private void parseString(String input) {

		input = input.trim();
		double n1, n2;
		String q1, q2, s1, s2;
		Matcher m;
		m = dms.matcher(input);
		if(m.find()) {
			/*
			 System.out.println(String.format("String: %s Groups: %s %s %s %s %s %s %s %s", input, m.group(1),
			 m.group(2),
			 m.group(3),
			 m.group(4),
			 m.group(5),
			 m.group(6),
			 m.group(7),
			 m.group(8)
			 )); /**/
			q1 = m.group(4);
			q2 = m.group(8);
			n1 = Double.parseDouble(m.group(1))
					+ Double.parseDouble(m.group(2)) / 60.0
					+ Double.parseDouble(m.group(3)) / 3600.0;
			n2 = Double.parseDouble(m.group(5))
					+ Double.parseDouble(m.group(6)) / 60.0
					+ Double.parseDouble(m.group(7)) / 3600.0;
		} else {
			m = reverseDecimal.matcher(input);
			if(m.find()) {
				n1 = Double.parseDouble(m.group(2));
				n2 = Double.parseDouble(m.group(4));
				q1 = m.group(1);
				q2 = m.group(3);
			} else {
				m = decimalDegrees.matcher(input);
				if(m.find()) {
					n1 = Double.parseDouble(m.group(1));
					n2 = Double.parseDouble(m.group(3));
					q1 = m.group(2);
					q2 = m.group(4);
				} else
					throw new NumberFormatException("Invalid coordinate syntax");
			}
		}
		if(n1 < 0 && !q1.equals(""))
			throw new NumberFormatException("Conflicting coordinate syntax: " + input);
		else if(q1.equalsIgnoreCase("s") || q1.equalsIgnoreCase("w"))
			n1 = -n1;
		if(n2 < 0 && !q2.equals(""))
			throw new NumberFormatException("Conflicting coordinate syntax");
		else if(q2.equalsIgnoreCase("s") || q2.equalsIgnoreCase("w"))
			n2 = -n2;
		if((q1.equalsIgnoreCase("n") || q1.equalsIgnoreCase("s"))
				&& (q2.equalsIgnoreCase("n") || q2.equalsIgnoreCase("s")))
			throw new NumberFormatException("Bad coordinate syntax");
		if((q1.equalsIgnoreCase("e") || q1.equalsIgnoreCase("w"))
				&& (q2.equalsIgnoreCase("e") || q2.equalsIgnoreCase("w")))
			throw new NumberFormatException("Bad coordinate syntax");
		if(q1.equalsIgnoreCase("e") || q1.equalsIgnoreCase("w")) {
			latitude = n2;
			longitude = n1;
		} else {
			latitude = n1;
			longitude = n2;
		}
		if(latitude > 90.0 || latitude < -90.0 || longitude > 180.0 || longitude < -180.0)
			throw new NumberFormatException("Coordinates out of range");
	}

	public String toString() {
		return String.format("%f %f", getLatitude(), getLongitude());
	}

	public String toDMS() {
		StringBuilder sb = new StringBuilder();
		double lat = Math.abs(getLatitude());
		double lon = Math.abs(getLongitude());
		sb.append((int)Math.floor(lat));
		sb.append(DEGREE);
		lat -= Math.floor(lat);
		lat *= 60;
		sb.append((int)Math.floor(lat));
		sb.append("'");
		lat -= Math.floor(lat);
		lat *= 60;
		sb.append((int)Math.round(lat));
		sb.append("\"");
		if(getLatitude() < 0)
			sb.append("S ");
		else
			sb.append("N ");
		sb.append((int)Math.floor(lon));
		sb.append(DEGREE);
		lon -= Math.floor(lon);
		lon *= 60;
		sb.append((int)Math.floor(lon));
		sb.append("'");
		lon -= Math.floor(lon);
		lon *= 60;
		sb.append((int)Math.round(lon));
		sb.append("\"");
		if(getLongitude() < 0)
			sb.append("W");
		else
			sb.append("E");
		return sb.toString();
	}

	static public Coordinates parse(String s) {
		Coordinates coords = new Coordinates();
		coords.parseString(s);
		return coords;
	}

	static public String[] tests = {
			"-32.66 152.33",
			"32.66S 152.33E",
			"32.66N 152.33W",
			" 152.33W 32.66N ",
			"32" + DEGREE + "39'35\"S 152" + DEGREE + "19'48\"E",
			"32 39 35 152 19 48",
			" 152" + DEGREE + "19 48E32" + DEGREE + "39 35S",};

	static public void main(String[] args) {

		Coordinates coords;

		if(args.length != 0)
			tests = args;
		for(String s : tests) {
			coords = parse(s);
			System.out.println(String.format("String: %s: result %s, %s", s, coords.toString(), coords.toDMS()));
		}
		if(args.length != 0) {
			coords = parse(args[0]);
			System.out.println(coords.toString());
			System.out.println(coords.toDMS());
		}
	}
}