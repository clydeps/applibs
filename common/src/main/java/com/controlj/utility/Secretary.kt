package com.controlj.utility

import io.reactivex.rxjava3.core.Scheduler
import java.util.concurrent.ConcurrentLinkedQueue

/**
 * Created by clyde on 9/7/18.
 *
 * This class has a single threaded scheduler, and wil run tasks in the order submitted.
 * Each task must call onComplete() when done to trigger the running of the next task.
 */

open class Secretary(val scheduler: Scheduler, val maxQueue: Int = Integer.MAX_VALUE) {
    data class QueueItem(val action: () -> Unit, val name: String)

    /**
     * the task queue
     */
    private val queue = ConcurrentLinkedQueue<QueueItem>()
    var hiWater = 0
        private set

    /**
     * If there is a task in the queue, run it.
     */
    private fun execute() {
        queue.peek()?.let {
            scheduler.scheduleDirect {
                it.action()
            }
        }
    }

    /**
     * Add a task to the queue.
     * @param action The task runnable
     * @param name The task name, for debug purposes only
     */
    fun enqueue(action: () -> Unit, name: String) {
        synchronized(queue) {
            if (queue.size >= maxQueue)
                throw IllegalStateException("Queue is full")
            if (queue.isEmpty()) {
                queue.offer(QueueItem(action, name))
                execute()
            } else {
                queue.offer(QueueItem(action, name))
            }
            if (queue.size > hiWater)
                hiWater = queue.size
        }
    }

    /**
     * Must be called when a task is complete.
     * Will trigger the execution of the next task if any.
     */
    fun onComplete() {
        synchronized(queue) {
            queue.poll()
            execute()
        }
    }

    /**
     * Remove all pending tasks from the queue. Does not stop the running task.
     */
    fun clear() {
        queue.clear()
    }
}
