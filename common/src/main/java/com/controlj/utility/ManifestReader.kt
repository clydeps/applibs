package com.controlj.utility

import java.io.IOException
import java.net.URL
import java.util.Enumeration
import java.util.Objects
import java.util.jar.Attributes
import java.util.jar.Manifest

class ManifestReader(private val classloader: ClassLoader = Thread.currentThread().contextClassLoader) {

    @Suppress("NewApi")
    fun readManifestMapWithTitle(title: String): Map<String, String> {
        Objects.requireNonNull(title)
        val manifest = readManifestWithProperty(Attributes.Name.IMPLEMENTATION_TITLE.toString(), title)
            ?: return mapOf()
        return manifest.mainAttributes.entries.map { it.key.toString() to it.value.toString() }.toMap()
    }

    fun readManifestWithTitle(title: String): Manifest? {
        Objects.requireNonNull(title)
        return readManifestWithProperty(Attributes.Name.IMPLEMENTATION_TITLE.toString(), title)
    }

    fun readManifestWithProperty(name: String, value: String?): Manifest? {
        Objects.requireNonNull(name)
        return readManifest { candidate: Manifest ->
            val actual = candidate.mainAttributes.getValue(name)
            actual == value
        }
    }

    fun readManifest(manifestPredicate: (Manifest) -> Boolean): Manifest? {
        Objects.requireNonNull(manifestPredicate)
        return findManifestResources().toList()
            .asSequence()
            .map { toManifest(it) }
            .firstOrNull { manifestPredicate(it) }
    }

    private fun findManifestResources(): Enumeration<URL> {
        return try {
            classloader.getResources(MANIFEST_PATH)
        } catch (e: IOException) {
            throw RuntimeException("Could not open " + MANIFEST_PATH, e)
        }
    }

    private fun toManifest(manifestUrl: URL): Manifest {
        try {
            manifestUrl.openConnection().getInputStream().use { inputStream -> return Manifest(inputStream) }
        } catch (e: IOException) {
            throw RuntimeException("Unable to open manifest file: $manifestUrl", e)
        }
    }

    companion object {
        private val INSTANCE = ManifestReader()
        fun loadManifestMapWithTitle(title: String): Map<String, String>? {
            return INSTANCE.readManifestMapWithTitle(title)
        }

        fun loadManifestWithTitle(title: String): Manifest? {
            return INSTANCE.readManifestWithTitle(title)
        }

        private const val MANIFEST_PATH = "META-INF/MANIFEST.MF"
    }
}
