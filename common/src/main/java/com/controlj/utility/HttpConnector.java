package com.controlj.utility;

import com.controlj.logging.CJLog;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 * <p>
 * User: clyde
 * Date: 22/04/2015
 * Time: 9:54 PM
 */

public class HttpConnector {

    private static final int MAXFLUSH = 100000; // max number of bytes to read when flushing

    public interface Listener {
        void onProgress(int progess);
    }

    private static final int TIMEOUT = 10000;
    private static final int MAX_REDIRECTS = 5;
    static public final int TEMP_REDIRECT = 307;

    private CookieManagerOld cookieManager;
    private HttpURLConnection connection;
    private Map<String, String> headers = new HashMap<>();
    private URL redirect;
    private URL url;
    private int timeout;
    private Listener listener;

    public HttpConnector(CookieManagerOld cookieManager, int timeout) {
        this.cookieManager = cookieManager;
        this.timeout = timeout;
    }

    public HttpConnector() {
        this(new CookieManagerOld(), TIMEOUT);
    }

    public URL getRedirect() {
        return redirect;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private void setRedirect(URL redirect) {
        this.redirect = redirect;
    }

    public void setUrl(String path) throws IOException {
        try {
            url = new URL(path);
            redirect = null;
        } catch (MalformedURLException e) {
            throw new IOException("Malformed URL: " + path);
        }
    }

    public URL getUrl() {
        if (redirect != null)
            return redirect;
        return url;
    }

    public CookieManagerOld getCookieManager() {
        return cookieManager;
    }

    public void setRequestProperty(String name, String value) {
        headers.put(name, value);
    }

    public String getStringResult() throws IOException {
        return getStringResult(connection);
    }

    public void flushInput() {
        try {
            InputStream is = getInputStream(connection);
            //noinspection ResultOfMethodCallIgnored
            is.read(new byte[MAXFLUSH]);
            is.close();
        } catch (IOException e) {
            disconnect();
            e.printStackTrace();
        }
    }

    public String getHeaderField(String key) {
        return connection.getHeaderField(key);
    }

    public void disconnect() {
        connection.disconnect();
    }

    private void addHeaders() {
        for (String name : headers.keySet())
            connection.setRequestProperty(name, headers.get(name));
    }

    private void initConnection(URL connUrl) throws IOException {
        connection = (HttpURLConnection) connUrl.openConnection();
        connection.setConnectTimeout(timeout);
        connection.setReadTimeout(timeout);
        cookieManager.setCookies(connection);
        connection.setDoInput(true);
        connection.setRequestProperty("Accept-encoding", "gzip");
        connection.setInstanceFollowRedirects(false);
        connection.setUseCaches(true);
        addHeaders();
    }

    public void connect() throws IOException {

        int redirects = 0;
        int status = connection.getResponseCode();
        while (status == HttpURLConnection.HTTP_MOVED_TEMP
                || status == HttpURLConnection.HTTP_MOVED_PERM
                || status == TEMP_REDIRECT
                || status == HttpURLConnection.HTTP_SEE_OTHER) {
            if (++redirects == MAX_REDIRECTS)
                throw new IOException("Too many redirects for: " + connection.getURL().toString());
            String newloc = connection.getHeaderField("Location");
            cookieManager.storeCookies(connection);
            setRedirect(new URL(connection.getURL(), newloc));
            initConnection(redirect);
            connection.setRequestProperty("Referer", url.toString());
            status = connection.getResponseCode();
        }
        if (status != HttpURLConnection.HTTP_OK) {
            logResponse(connection);
            throw new IOException("connection status=" + status);
        }
        cookieManager.storeCookies(connection);
    }

    public void getRequest() throws IOException {
        initConnection(url);
        //CJLog.logMsg("fetching URL %s", connection.getURL().toString());
        connect();
    }

    public void getRequest(List<Parameter> params) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(url.toString());
        char sep = '?';
        if (!params.isEmpty()) {
            for (Parameter param : params) {
                sb.append(sep);
                sep = '&';
                sb.append(param.URLEncode());
            }
        }
        initConnection(new URL(sb.toString()));
        connect();
    }

    private static void setupPost(HttpURLConnection connection, FormData formdata, Listener listener) throws IOException {
        connection.setRequestMethod("POST");
        long length = formdata.getDataLength();
        if ((int) length == length)
            connection.setFixedLengthStreamingMode((int) length);
        connection.setRequestProperty("Content-length", Long.toString(length));
        connection.setRequestProperty("Content-Type", formdata.getContentType());
        connection.setDoOutput(true);
        OutputStream os = connection.getOutputStream();
        InputStream is = formdata.getStream();
        byte[] buffer = new byte[4096];
        int len;
        long count = 0;
        int last = -1;
        while ((len = is.read(buffer)) > 0) {
            os.write(buffer, 0, len);
            if (listener != null) {
                count += len;
                int prog = (int) (count * 100 / length);
                if (prog != last) {
                    last = prog;
                    listener.onProgress(prog);
                }
            }
        }
        os.close();
        is.close();
    }

    public void postRequest(FormData formdata) throws IOException {
        initConnection(url);
        setupPost(connection, formdata, listener);
        //CJLog.logd(null, TAG, "fetching URL %s", connection.getURL().toString());
        connect();
    }

    public void postRequest(List<Parameter> postData) throws IOException {
        postRequest(new FormData(postData));
    }

    public void copyToFile(File f) throws IOException {
        copyConnToFile(connection, f);
    }

    static public HttpURLConnection getConnection(URL url) throws IOException {
        HttpURLConnection conn;
        //System.out.println("New connection: " + url.getProtocol() + url.getHost() + url.getPath());
        conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(TIMEOUT);
        conn.setReadTimeout(TIMEOUT);
        //conn.setRequestProperty("Host", url.getHost());
        //conn.setRequestProperty("Cache-Control", "max-age=0");
        //conn.setRequestProperty("User-Agent", "Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.91 Safari/537.4");
        //conn.setRequestProperty("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3");
        //conn.setRequestProperty("Origin", url.getProtocol() + "://" + url.getHost());
        return conn;
    }

    static public HttpURLConnection openConnection(String path, String accept, boolean allowRedirect) throws IOException {
        return openConnection(path, accept, (FormData) null, null, allowRedirect);
    }

    static public HttpURLConnection openConnection(String path, String accept) throws IOException {
        return openConnection(path, accept, (FormData) null, null, true);
    }

    static public HttpURLConnection openConnection(String path, String accept, List<Parameter> postData, CookieManagerOld cookieManager, boolean allowRedirect) throws IOException {
        return openConnection(path, accept, new FormData(postData), cookieManager, allowRedirect);
    }

    static public HttpURLConnection openConnection(String path, String accept, FormData formdata, CookieManagerOld cookieManager, boolean allowRedirect) throws IOException {
        URL url;
        HttpURLConnection conn;
        try {
            url = new URL(path);
        } catch (MalformedURLException e) {
            CJLog.logException(e);
            return null;
        }
        int redirects = 0;
        for (; ; ) {
            conn = getConnection(url);
            if (cookieManager != null)
                cookieManager.setCookies(conn);
            conn.setDoInput(true);
            conn.setRequestProperty("Accept-encoding", "gzip");
            conn.setRequestProperty("User-Agent", "okhttp/2.3.0");
            //conn.setInstanceFollowRedirects(true);
            if (accept != null)
                conn.setRequestProperty("Accept", accept);
            //CJLog.logd(null, TAG, "fetching URL %s", url.toString());
            if (formdata != null) {
                setupPost(conn, formdata, null);
                formdata = null;    // subsequent fetches are GET only
            }
            //ResourceUtil.logd(null, TAG, "Request headers:");
            //for(Map.Entry<String, List<String>> fields : conn.getRequestProperties().entrySet())
            //ResourceUtil.logd(null, TAG, "%s=%s", fields.getKey(), fields.getValue());
            int status = conn.getResponseCode();
            if (allowRedirect && (status == HttpURLConnection.HTTP_MOVED_TEMP
                    || status == HttpURLConnection.HTTP_MOVED_PERM
                    || status == TEMP_REDIRECT
                    || status == HttpURLConnection.HTTP_SEE_OTHER)) {
                if (++redirects == MAX_REDIRECTS)
                    throw new IOException("Too many redirects for: " + url.toString());
                url = new URL(conn.getHeaderField("Location"));
                if (cookieManager != null)
                    cookieManager.storeCookies(conn);
                continue;
            }
            if (status != HttpURLConnection.HTTP_OK)
                throw new IOException("connection status=" + status);
            if (cookieManager != null)
                cookieManager.storeCookies(conn);
            return conn;
        }
    }

    static public InputStream getInputStream(HttpURLConnection conn) throws IOException {
        InputStream is;
        try {
            is = conn.getInputStream();
            String ce = conn.getContentEncoding();
            if (ce != null && ce.equals("gzip"))
                is = new GZIPInputStream(new BufferedInputStream(is));
        } catch (FileNotFoundException fne) {
            logResponse(conn);
            conn.disconnect();
            throw fne;
        }
        return is;
    }

    static public InputStream getStreamFromUrl(String url) throws IOException {
        HttpURLConnection conn = openConnection(url, "*/*");
        return getInputStream(conn);
    }

    static void logResponse(HttpURLConnection connection) throws IOException {
        InputStream inp = connection.getErrorStream();
        if (inp == null)
            return;
        BufferedReader rd = new BufferedReader(new InputStreamReader(inp));
        String line;
        StringBuilder response = new StringBuilder();
        while ((line = rd.readLine()) != null) {
            response.append(line);
            response.append('\n');
        }
        //CJLog.logd(TAG, "Response to %s: %s", connection.getURL().toString(), response.toString());
    }

    static public HttpURLConnection openConnection(String path) throws IOException {
        return openConnection(path, "*/*");
    }

    static public String postData(List<Parameter> data) {
        StringBuilder sb = new StringBuilder();

        for (Parameter p : data) {
            if (sb.length() != 0)
                sb.append('&');
            sb.append(p.URLEncode());
        }
        return sb.toString();
    }

    static public void copyConnToStream(HttpURLConnection conn, OutputStream outputStream) throws IOException {

        InputStream is = getInputStream(conn);
        byte[] buffer = new byte[8192];
        int cnt;
        while ((cnt = is.read(buffer)) >= 0)
            outputStream.write(buffer, 0, cnt);
        outputStream.close();
        is.close();
    }

    static public void copyConnToFile(HttpURLConnection conn, File f) throws IOException {

        InputStream is = getInputStream(conn);
        FileOutputStream bos = new FileOutputStream(f);
        copyConnToStream(conn, bos);
    }


    static public String getStringResult(HttpURLConnection conn) throws IOException {

        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(getInputStream(conn)));
        String s;
        while ((s = br.readLine()) != null) {
            sb.append(s);
            sb.append('\n');
        }
        br.close();
        return sb.toString();
    }

    private static final String mWalledGardenUrl = "http://clients3.google.com/generate_204";

    public static URL isWalledGardenConnection() {
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(mWalledGardenUrl); // "http://clients3.google.com/generate_204"
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setConnectTimeout(TIMEOUT);
            urlConnection.setReadTimeout(TIMEOUT);
            urlConnection.setUseCaches(false);
            urlConnection.getInputStream();
            // We got a valid response, but not from the real google
            if (urlConnection.getResponseCode() != 204)
                return urlConnection.getURL();
            return null;
        } catch (IOException e) {
            return null;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    /**
     * Set the timeout parameter. The default is 10 seconds.
     *
     * @param timeout The timeout in milliseconds.
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }
}

