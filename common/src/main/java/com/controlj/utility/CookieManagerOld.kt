package com.controlj.utility

import com.controlj.logging.CJLog.logMsg
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.format.DateTimeFormatter
import java.io.IOException
import java.net.URLConnection
import java.util.concurrent.ConcurrentSkipListSet

/**
 * CookieManager is a simple utility for handling cookies when working with java.net.URL and
 * java.net.URLConnection objects.
 */
class CookieManagerOld {

    private var store = ConcurrentSkipListSet<Cookie>()
    private val gson = Gson()

    val json: String
        get() = gson.toJson(store)

    var cookies: List<Cookie>
        get() {
            val list = ArrayList<Cookie>(store)
            return list
        }
        set(value) {
            store.clear()
            store.addAll(value)
        }

    class Cookie(
        val name: String,
        val value: String,
        val domain: String,
        val path: String,
        val isHostOnly: Boolean,
        val isSecureOnly: Boolean,
        val isHttpOnly: Boolean,
        val expiry: Instant
    ) : Comparable<Cookie> {
        val creationTime: Long
        val lastAccessTime: Long

        val isNotExpired: Boolean
            get() {
                //debug("isnotExpired: expiry=$expiry, now=${System.currentTimeMillis()}")
                return expiry.isAfter(instantNow)
            }

        init {
            creationTime = System.currentTimeMillis()
            lastAccessTime = creationTime
        }

        fun matches(hostname: String?, pathname: String?): Boolean {
            if (hostname == null || hostname.isEmpty())
                return false
            if (isHostOnly && !hostname.equals(domain, ignoreCase = true))
                return false
            if (!hostname.endsWith(domain))
                return false
            if (path == "/")
                return true
            return if (pathname == null || pathname.isEmpty()) false else pathname == path || pathname.startsWith(path) && pathname[path.length] == '/'
        }

        override fun hashCode(): Int {
            return name.hashCode() + domain.hashCode() + path.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            val result = other is Cookie && compareTo((other as Cookie?)!!) == 0
            //debug("Comparing $this with $other = $result")
            return result
        }

        override fun toString(): String {
            return String.format("%s%s: %s=%s expires %s", domain, path, name, value, expiry.toString())
        }

        override fun compareTo(other: Cookie): Int {
            var i = name.compareTo(other.name)
            if (i != 0)
                return i
            i = domain.compareTo(other.domain)
            return if (i != 0) i else path.compareTo(other.path)
        }

        companion object {

            fun createCookie(host: String, header: String): Cookie? {

                val parts = header.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                //debug("CreateCookie string=$header, parts.size=${parts.size}")
                val nameValue = parts[0].split("=".toRegex(), 2).toTypedArray()
                if (nameValue.size != 2 || nameValue[0].length == 0)
                    return null
                var domain = host
                var path = "/"
                var hostOnly = true
                var secureOnly = false
                var httpOnly = false
                var expiry = instantNow.plus(Duration.ofDays(7))
                var maxAge: Long = 0
                parts.forEach {
                    val attr = it.split("=".toRegex(), 2).toTypedArray()
                    val token = attr.first().trim { it <= ' ' }.lowercase()
                    if (attr.size == 2) {
                        val arg = attr[1].trim { it <= ' ' }
                        when (token) {
                            "expires" -> {
                                try {
                                    val newExpiry = EXPIRES_FORMAT.parse(arg.replace("-".toRegex(), " "), Instant.FROM)
                                    if (newExpiry.isAfter(instantNow))
                                        expiry = newExpiry
                                } catch (ignored: IllegalArgumentException) {
                                    logMsg(ignored.message ?: ignored.toString())
                                }

                            }
                            "max-age" -> {
                                try {
                                    maxAge = arg.toLong() * 1000
                                } catch (ignored: Exception) {
                                }
                            }
                            "domain" -> {
                                var s = arg
                                if (s.startsWith("."))
                                    s = s.substring(1, s.length)
                                if (!s.isEmpty())
                                    domain = s.lowercase()
                                hostOnly = false
                            }
                            "path" -> {
                                if (arg.startsWith("/"))
                                    path = arg
                            }
                        }
                    } else {
                        when (token) {
                            "secure" -> secureOnly = true
                            "httponly" -> httpOnly = true
                        }
                    }
                }
                // maxage takes precedence over expiry
                if (maxAge > 0L) {
                    expiry = instantNow.plusMillis(maxAge)
                }
                return Cookie(
                    nameValue[0].trim { it <= ' ' },
                    nameValue[1].trim { it <= ' ' },
                    domain,
                    path,
                    hostOnly,
                    secureOnly,
                    httpOnly,
                    expiry
                )
            }
        }
    }

    /**
     * Retrieves and stores cookies returned by the host on the other side of the the open
     * java.net.URLConnection.
     *
     *
     * The connection MUST have been opened using the connect() method or a IOException will be
     * thrown.
     *
     * @param conn a java.net.URLConnection - must be open, or IOException will be thrown
     * @throws java.io.IOException Thrown if conn is not open.
     */
    @Throws(IOException::class)
    fun storeCookies(conn: URLConnection) {

        val host = conn.url.host
        val headerList = conn.headerFields
        for (entry in headerList) {
            if (entry.key.equals(SET_COOKIE, true)) {
                for (headerValue in entry.value) {
                    val cookie = Cookie.createCookie(host, headerValue)
                    //debug("Storing cookie $cookie")
                    if (cookie != null) {
                        store.remove(cookie)
                        store.add(cookie)
                    }
                }
                break
            }
        }
    }

    fun getCookies(hostname: String): List<Cookie> {

        val list = ArrayList<Cookie>()
        for (cookie in store)
            if (cookie.domain.equals(hostname))
                list.add(cookie)
        return list

    }

    /**
     * Prior to opening a URLConnection, calling this method will set all unexpired cookies that
     * match the path or subpaths for thi underlying URL
     *
     *
     * The connection MUST NOT have been opened method or an IOException will be thrown.
     *
     * @param conn a java.net.URLConnection - must NOT be open, or IOException will be thrown
     * @throws java.io.IOException Thrown if conn has already been opened.
     */
    @Throws(IOException::class)
    fun setCookies(conn: URLConnection) {

        // let's determine the domain and path to retrieve the appropriate cookies
        val url = conn.url
        val host = url.host
        val path = url.path

        val cookieStringBuilder = StringBuilder()

        var first = true
        for (cookie in store) {
            // check cookie to ensure path matches  and cookie is not expired
            // if all is cool, add cookie to header string
            //debug("Check cookie $cookie: isNotExpired=${cookie.isNotExpired} timeLeft =${cookie.expiry-System.currentTimeMillis()}")
            if (cookie.isNotExpired && cookie.matches(host, path)) {
                if (first)
                    first = false
                else
                    cookieStringBuilder.append(SET_COOKIE_SEPARATOR)
                cookieStringBuilder.append(cookie.name)
                cookieStringBuilder.append("=")
                cookieStringBuilder.append(cookie.value)
            }
        }
        if (!first)
            try {
                conn.setRequestProperty(COOKIE, cookieStringBuilder.toString())
            } catch (ise: java.lang.IllegalStateException) {
                throw IOException("Illegal State! Cookies cannot be set on a URLConnection that is already connected. " + "Only call setCookies(java.net.URLConnection) AFTER calling java.net.URLConnection.connect().")
            }

    }

    fun restoreJSON(json: String) {
        synchronized(this) {
            try {
                store = gson.fromJson(json, object : TypeToken<ConcurrentSkipListSet<Cookie>>() {

                }.type)
            } catch (ignored: Exception) {

            }

        }
    }

    fun removeForDomain(hostName: String) {
        for (cookie in store) {
            if (cookie.domain == hostName)
                store.remove(cookie)
        }
    }

    /**
     * Returns a string representation of stored cookies organized by domain.
     */
    override fun toString(): String {
        val sb = StringBuilder()
        val domains = HashMap<String, Cookie>()
        for (cookie in store)
            domains.put(cookie.domain + cookie.path, cookie)
        for (domain in domains.values) {
            for (cookie in store) {
                if (cookie.matches(domain.domain, domain.path)) {
                    sb.append(cookie.toString())
                    sb.append('\n')
                }
            }
        }
        return sb.toString()
    }

    companion object {
        private val SET_COOKIE = "Set-Cookie"
        private val COOKIE_VALUE_DELIMITER = ";"
        private val PATH = "path"
        private val EXPIRES = "expires"
        private val DATE_FORMAT = "EEE, dd-MMM-yyyy hh:mm:ss z"
        private val SET_COOKIE_SEPARATOR = "; "
        private val COOKIE = "Cookie"
        private val NAME_VALUE_SEPARATOR = '='
        private val DOT = '.'
        private val EXPIRES_FORMAT = DateTimeFormatter
            .RFC_1123_DATE_TIME
    }
}
