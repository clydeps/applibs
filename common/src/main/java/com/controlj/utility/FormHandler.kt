package com.controlj.utility

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.io.BufferedReader
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStreamReader
import java.lang.StringBuilder
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.ArrayList
import kotlin.text.Charsets.UTF_8

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */ /**
 * @author clyde
 */
class FormHandler(protected var host: String) {
    protected var cm = CookieManagerOld()
    protected var lastUrl: String? = null
    protected var redirect: URL? = null
    @Throws(IOException::class)
    protected fun logResponse(connection: HttpURLConnection?) {
        if (connection != null) {
            val inp = connection.errorStream
            val rd = BufferedReader(InputStreamReader(inp))
            var line: String?
            val response = StringBuilder()
            while (rd.readLine().also { line = it } != null) {
                response.append(line)
                response.append('\n')
            }
            //System.out.println("Response to " + connection.getURL() + ": " + response.toString());
        }
        throw IOException(
            String.format(
                "Groups: Caught FNE: on %s: response code= %s, %s",
                connection!!.url.toExternalForm(), connection.responseMessage, connection.responseCode
            )
        )
    }

    @Throws(IOException::class)
    protected fun getConnection(url: URL): HttpURLConnection {
        return getConnection(url, "*/*")
    }

    @Throws(IOException::class)
    fun getConnection(url: URL, acceptString: String?): HttpURLConnection {
        val conn: HttpURLConnection
        //System.out.println("New connection: " + url.getProtocol() + url.getHost() + url.getPath());
        conn = url.openConnection() as HttpURLConnection
        conn.connectTimeout = TIMEOUT
        conn.readTimeout = TIMEOUT
        conn.setRequestProperty("Accept", acceptString)
        conn.setRequestProperty("Host", url.host)
        conn.setRequestProperty("Cache-Control", "max-age=0")
        conn.setRequestProperty(
            "User-Agent",
            "Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.91 Safari/537.4"
        )
        conn.setRequestProperty("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3")
        conn.setRequestProperty("Origin", url.protocol + "://" + url.host)
        redirect = null
        conn.instanceFollowRedirects = false
        if (lastUrl != null) conn.setRequestProperty("Referer", lastUrl)
        lastUrl = url.toExternalForm()
        cm.setCookies(conn)
        //System.out.println("Request headers");
        //for(String s : conn.getRequestProperties().keySet())
        //System.out.println(String.format("%s: %s", s, conn.getRequestProperty(s)));
        return conn
    }

    @Throws(IOException::class)
    protected fun getRequest(url: URL): Document? {
        return getRequest(url, null)
    }

    @Throws(IOException::class)
    protected fun getRequest(url: URL, params: String?): Document? {
        val conn = getConnection(url.let {
            if(params == null)
                it
            else
                URL(url, "${url.path}?$params")
        })
        try {
            conn.requestMethod = "GET"
            val doc = Jsoup.parse(conn.inputStream, null, conn.url.toExternalForm())
            cm.storeCookies(conn)
            val where = conn.getHeaderField("Location")
            if (where != null) redirect = URL(conn.url, where)
            conn.disconnect()
            return doc
        } catch (e: FileNotFoundException) {
            logResponse(conn)
        }
        return null
    }

    @Throws(IOException::class)
    protected fun postForm(form: HtmlForm): Document? {
        var conn: HttpURLConnection? = null
        val doc: Document?
        try {
            if (form.isPostMethod) {
                conn = getConnection(form.getAction())
                conn.requestMethod = "POST"
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                conn.doOutput = true
                val bytes = form.values.toByteArray(UTF_8)
                val out = conn.outputStream
                out.write(bytes)
                out.flush()
                doc = Jsoup.parse(conn.inputStream, null, conn.url.toExternalForm())
                cm.storeCookies(conn)
                val where = conn.getHeaderField("Location")
                if (where != null) redirect = URL(conn.url, where)
                conn.disconnect()
            } else doc = getRequest(form.getAction(), form.values)
            return doc
        } catch (e: FileNotFoundException) {
            logResponse(conn)
        }
        return null
    }

    protected fun getTable(el: Element): List<List<String>> {
        val table: MutableList<List<String>> = ArrayList()
        val rows = el.getElementsByTag("tr")
        for (row in rows) {
            val tr: MutableList<String> = ArrayList()
            table.add(tr)
            for (col in row.getElementsByTag("th")) tr.add(col.html())
            for (col in row.getElementsByTag("td")) tr.add(col.html())
        }
        return table
    }

    @Throws(MalformedURLException::class)
    protected fun getForm(formElement: Element?): HtmlForm? {
        if (formElement == null) return null
        val posturl = URL(formElement.absUrl("action"))
        val form = HtmlForm(posturl, formElement.attr("method"))
        val inputs = formElement.getElementsByTag("input")
        //System.out.println(String.format("Found form - %s", form.toString()));
        for (input in inputs) form.add(FormInput(input.attr("name"), input.attr("value"), input.attr("type")))
        return form
    }

    companion object {
        const val HTTP_STRING = "http"
        const val HTTPS_STRING = "https"
        private val TAG = FormHandler::class.java.simpleName
        private const val TIMEOUT = 10000
    }
}
