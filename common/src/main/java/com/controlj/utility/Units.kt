package com.controlj.utility

import com.controlj.data.Constants.INVALID_DATA
import java.text.NumberFormat
import java.util.HashMap
import java.util.Locale
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.roundToInt

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 21/02/2016
 * Time: 4:00 PM
 */
object Units {
    const val AUTO_DIGITS = 65536
    internal var unitMap: MutableMap<String, Unit> = HashMap()
    private val cardinalFull =
        arrayOf("NORTH", "NORTHEAST", "EAST", "SOUTHEAST", "SOUTH", "SOUTHWEST", "WEST", "NORTHWEST", "NORTH")

    private val cardinals = arrayOf("N", "NE", "E", "SE", "S", "SW", "W", "NW", "N")

    init {
        for (u in Unit.values())
            unitMap[u.label] = u
    }

    enum class Category {
        NONE,
        MASS,
        VOLUME,
        SPEED,
        DISTANCE,
        TIME,
        ANGLE,
        TEMPERATURE,
        FLOW,
        POWER,
        ECONOMY,
        VOLTAGE,
        CURRENT,
        PRESSURE,
        SG
    }

    class Formatter(val unit: Unit, val formatter: NumberFormat) {
        fun format(value: Double): String {
            return formatter.format(unit.convertTo(value))
        }
    }

    enum class Unit(
        override val label: String,
        override val ratio: Double,
        val category: Category,
        val precision: Int = 0,
        val unitName: String = label
    ) : Conversion {
        IDENTITY("", 1.0, Category.NONE),
        RPM("rpm", 1.0, Category.NONE),
        PERCENT("%", .01, Category.NONE),
        LITER("liter", 1.0, Category.VOLUME),
        LITRE("litre", 1.0, Category.VOLUME),
        USGAL("usgal", 3.7854118, Category.VOLUME, 1),
        IMPGAL("impgal", 4.54609, Category.VOLUME, 1),

        METER("m", 1.0, Category.DISTANCE),
        MM("mm", .001, Category.DISTANCE),
        INCH("in", 0.0254, Category.DISTANCE),
        FOOT("ft", 0.3048, Category.DISTANCE),
        KM("km", 1000.0, Category.DISTANCE),
        MILE("mile", 1609.344, Category.DISTANCE),
        SM("sm", 1609.344, Category.DISTANCE),
        NM("nm", 1852.0, Category.DISTANCE),

        KG("kg", 1.0, Category.MASS),
        LB("lb", 0.45359237, Category.MASS),

        MPS("m/s", 1.0, Category.SPEED),
        FPS("ft/s", 0.3048, Category.SPEED),
        FPM("fpm", 0.3048 / 60, Category.SPEED),
        FPMIN("ft/min", 0.3048 / 60, Category.SPEED),
        MPH("mph", 1609.344 / 3600, Category.SPEED),
        KMH("km/h", 1000.0 / 3600, Category.SPEED),
        KNOT("knot", 1852.0 / 3600, Category.SPEED),
        KPH("kph", 1000.0 / 3600, Category.SPEED),

        SECS("sec", 1.0, Category.TIME),
        MINS("min", 60.0, Category.TIME),
        HRS("hr", 3600.0, Category.TIME, precision = 1),
        HHMMSS("", 1.0, Category.TIME, 0, "hh:mm:ss") {
            override fun toString(value: Double, precision: Int): String {
                val hours = Math.floor(value / 3600).toInt()
                val minutes = Math.floor((value / 60) - hours * 60).toInt()
                val seconds = (value - hours * 3600 - minutes * 60).toInt()
                return String.format(Locale.US, "%02d:%02d:%02d", hours, minutes, seconds)
            }
        },
        HHMM("", 1.0, Category.TIME, 0, "hh:mm") {
            override fun toString(value: Double): String {
                val mins = (value / 60).roundToInt()
                val hours = mins / 60
                val minutes = mins - hours * 60
                return String.format(Locale.US, "%02d:%02d", hours, minutes)
            }
        },
        HMS("", 1.0, Category.TIME, 0, "h/m/s") {
            override fun toString(value: Double, precision: Int): String {
                if (value == INVALID_DATA)
                    return "----"
                val hours = Math.floor(value / 3600).toInt()
                val minutes = Math.floor((value / 60) - hours * 60).toInt()
                val seconds = (value - hours * 3600 - minutes * 60).toInt()
                if (hours == 0)
                    return "%dm%02ds".format(minutes, seconds)
                return "%dh%02dm".format(hours, minutes)
            }
        },

        DEGREE("\u00B0", 1.0, Category.ANGLE),
        RADIAN("rad", 57.29578, Category.ANGLE),

        DEGREE_WORD("", 1.0, Category.ANGLE, 0, "ddd.ddd") {
            override fun toString(value: Double): String {
                return "%7.3f\u00B0".format(value)
            }

        },
        DEGMIN("", 1.0, Category.ANGLE, 0, "ddd mm.m") {
            override fun toString(value: Double, precision: Int): String {
                val abs = Math.abs(value)
                var deg = Math.floor(abs).toInt()
                val min = (abs - deg) * 60
                if (value < 0)
                    deg = -deg
                return "%3d\u00B0%5.2f'".format(deg, min)
            }
        },
        DEGMINSEC("", 1.0, Category.ANGLE, 0, "ddd mm ss") {
            override fun toString(value: Double, precision: Int): String {
                val abs = Math.abs(value)
                var deg = Math.floor(abs).toInt()
                val min = (abs - deg) * 60
                val min1 = Math.floor(min).toInt()
                val sec = Math.round((min - min1) * 60).toInt()
                if (value < 0)
                    deg = -deg
                return "%3d\u00B0%02d'%02d\"".format(deg, min1, sec)
            }
        },

        CELSIUS("\u00B0C", 1.0, Category.TEMPERATURE),
        FARENHEIT("\u00B0F", 1.0, Category.TEMPERATURE) {
            override fun convertFrom(value: Double): Double {
                return (value - 32) * 5f / 9f
            }

            override fun convertTo(value: Double): Double {
                return value * 9f / 5f + 32
            }
        },
        KELVIN("\u00B0K", 1.0, Category.TEMPERATURE) {
            override fun convertFrom(value: Double): Double {
                return value - 273.15f
            }

            override fun convertTo(value: Double): Double {
                return value + 273.15f
            }
        },

        LPS("l/s", 1.0, Category.FLOW),
        LPH("l/h", 1.0 / 3600, Category.FLOW),
        USGPH("usg/h", 3.7854118 / 3600, Category.FLOW, 1),
        IMPGPH("impg/h", 4.54609 / 3600, Category.FLOW, 1),

        WATT("W", 1.0, Category.POWER),
        KILOWATT("kW", 1000.0, Category.POWER),
        HP("hp", 745.7, Category.POWER),

        MPL("m/l", 1.0, Category.ECONOMY),
        NMPG("nm/usgal", 1852 / 3.7854118, Category.ECONOMY),
        KMPL("km/l", 1000.0, Category.ECONOMY),
        LP100KM("l/100km", 100000.0, Category.ECONOMY) {
            override fun convertFrom(value: Double): Double {
                return 1.0 / super.convertFrom(value)
            }

            override fun convertTo(value: Double): Double {
                return 1.0 / super.convertTo(value)
            }
        },

        AMPS("A", 1.0, Category.CURRENT),
        MILLIAMPS("mA", .001, Category.CURRENT),

        VOLTS("V", 1.0, Category.VOLTAGE),
        MILLIVOLTS("mV", .001, Category.VOLTAGE, 1),

        PASCAL("Pa", 1.0, Category.PRESSURE),
        ATM("atm", 101325.0, Category.PRESSURE),
        HPA("hPa", 100.0, Category.PRESSURE),
        KPA("kPa", 1000.0, Category.PRESSURE),
        INHG("inHg", 3386.39, Category.PRESSURE, 1),
        PSI("psi", 6894.76, Category.PRESSURE),
        PALT("pALT", 1.0, Category.PRESSURE, unitName = "m") {
            override fun convertTo(value: Double): Double {
                return when (value) {
                    INVALID_DATA -> INVALID_DATA
                    else -> ((1 - (value / ATM.ratio).pow(0.190284)) * 44307.6939)
                }
            }

            override fun convertFrom(value: Double): Double {
                return when (value) {
                    INVALID_DATA -> INVALID_DATA
                    else -> (1 - value / 44307.6939).pow(5.2553026) * ATM.ratio
                }
            }
        },

        AVGAS("avgas", 0.71, Category.SG),
        AVTUR("avtur", 0.8, Category.SG),
        ULP("ulp", 0.73, Category.SG);

        /**
         * Convert a value expressed in this unit to a value expressed in the base unit
         */
        override fun convertFrom(value: Double): Double {
            return when(value) {
                INVALID_DATA -> INVALID_DATA
                else -> value * ratio
            }
        }

        /**
         * Convert a value expressed in the base unit to a value expressed in this unit
         */

        override fun convertTo(value: Double): Double {
            return when(value) {
                INVALID_DATA -> INVALID_DATA
                else -> value / ratio
            }
        }

        override fun toString(value: Double): String {
            return toString(value, AUTO_DIGITS)
        }

        /**
         * Get the string value with the specified number of decimal places.
         * using [AUTO_DIGITS] for the precision will result in 1 decimal place for values between -10.0 and 10.0,
         * zero outside that range.
         * A precision less than zero will multiply the value by 10 to that power, e.g. a precision of -2 would
         * result in values being divided by 100.
         * @param precision The required number of decimal places, [AUTO_DIGITS] for automatic
         */
        open fun toString(value: Double, precision: Int = AUTO_DIGITS): String {
            val converted = convertTo(value) * 10.0.pow(precision.coerceAtMost(0))
            val prec = when {
                precision == AUTO_DIGITS -> if (abs(converted) < 10.0) 1 else 0
                precision >= 0 -> precision
                else -> 0
            }
            return "%.${prec}f".format(converted)
        }

        /**
         * Convert a string value, expressed in this unit, into a double expressed
         * in the base unit.
         */
        fun fromString(s: String): Double {
            return convertFrom(s.toDoubleOrNull() ?: 0.0)
        }

        override fun toString(): String {
            return unitName
        }

        fun formatter(decimalPlaces: Int): Formatter {
            var places = decimalPlaces + Math.round(Math.log10(ratio)).toInt()
            if (places < 0)
                places = 0
            val formatter = NumberFormat.getNumberInstance()
            formatter.isGroupingUsed = false
            formatter.maximumFractionDigits = places
            formatter.minimumFractionDigits = places
            return Formatter(this, formatter)
        }

    }

    @JvmStatic
    fun getUnit(label: String): Unit {
        return label.lowercase().let {
            unitMap.getOrElse(it) {
                unitMap.getOrElse(it.removeSuffix("s")) {
                    throw IllegalArgumentException("No unit with label $label")
                }
            }
        }
    }

    fun convert(value: Double, from: String, to: String): Double {
        val src = getUnit(from)
        val dst = getUnit(to)
        if (src.category != dst.category)
            throw IllegalArgumentException("Incompatible units: $from/$to")
        return value * src.ratio / dst.ratio
    }

    fun convertTo(value: Double, to: String): Double {
        val dst = getUnit(to)
        return value / dst.ratio
    }

    fun convertFrom(value: Double, from: String): Double {
        val src = getUnit(from)
        return value * src.ratio
    }

    fun degreesToCardinal(degrees: Double): String {
        var angle = degrees
        angle = (angle + 360.0) % 360.0
        val card = Math.round(angle / 45).toInt()
        return cardinals[card]
    }

    fun cardinalToDegrees(card: String): Int {
        for (i in cardinals.indices)
            if (cardinals[i].equals(card, ignoreCase = true) || cardinalFull[i].equals(card, ignoreCase = true))
                return i * 45
        return -1
    }

    @JvmStatic
    fun main(args: Array<String>) {
        val weight = convert(100.0, "lbs", "kg")
        println("100 lbs = " + weight + "kg")
        var vs = convert(1.0, "knot", "fpm")
        println("1 knot = " + vs + "fpm")
        vs = convert(1.0, "m/s", "fpm")
        println("1 m/s = " + vs + "fpm")
    }

    fun getQuantity(s: String, num: Int): String {
        return s.replace("%n", Integer.toString(num))
            .replace("%s", if (num == 1) "" else "s")
            .replace("%%".toRegex(), "%")
    }
}

infix fun Double.from(unit: Units.Unit): Double {
    return unit.convertFrom(this)
}

infix fun Double.to(unit: Units.Unit): Double {
    return unit.convertTo(this)
}
