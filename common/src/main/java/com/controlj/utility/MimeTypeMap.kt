package com.controlj.utility

import java.util.regex.Pattern

/**
 * Created by clyde on 16/8/18.
 */

/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Two-way map that maps MIME-types to file extensions and vice versa.
 */
object MimeTypeMap {
    /**
     * MIME-type to file extension mapping:
     */
    private val mMimeTypeToExtensionMap: Map<String, String>

    /**
     * File extension to MIME type mapping:
     */
    private val mExtensionToMimeTypeMap: Map<String, String>

    /**
     * Return true if the given MIME type has an entry in the map.
     *
     * @param mimeType A MIME type (i.e. text/plain)
     * @return True iff there is a mimeType entry in the map.
     */
    fun hasMimeType(mimeType: String?): Boolean {
        return if (mimeType != null && mimeType.length > 0) {
            mMimeTypeToExtensionMap.containsKey(mimeType)
        } else false
    }

    /**
     * Return the MIME type for the given extension.
     *
     * @param extension A file extension without the leading '.'
     * @return The MIME type for the given extension or null iff there is none.
     */
    fun getMimeTypeFromExtension(extension: String?): String? {
        return if (extension != null && extension.length > 0) {
            mExtensionToMimeTypeMap[extension]
        } else null
    }

    /**
     * Return true if the given extension has a registered MIME type.
     *
     * @param extension A file extension without the leading '.'
     * @return True iff there is an extension entry in the map.
     */
    fun hasExtension(extension: String?): Boolean {
        return if (extension != null && extension.length > 0) {
            mExtensionToMimeTypeMap.containsKey(extension)
        } else false
    }

    /**
     * Return the registered extension for the given MIME type. Note that some
     * MIME types map to multiple extensions. This call will return the most
     * common extension for the given MIME type.
     *
     * @param mimeType A MIME type (i.e. text/plain)
     * @return The extension for the given MIME type or null iff there is none.
     */
    fun getExtensionFromMimeType(mimeType: String?): String? {
        return if (mimeType != null && mimeType.length > 0) {
            mMimeTypeToExtensionMap[mimeType]
        } else null
    }

    /**
     * Returns the file extension or an empty string iff there is no
     * extension. This method is a convenience method for obtaining the
     * extension of a url and has undefined results for other Strings.
     *
     * @param url
     * @return The file extension of the given url.
     */
    fun getFileExtensionFromUrl(url: String?): String {
        if (url != null && url.length > 0) {
            val filename = url.substringBeforeLast('?').substringAfterLast('/')
            // if the filename contains special characters, we don't
            // consider it valid for our matching purposes:
            if(filename.matches(Regex("[a-zA-Z0-9().%-]+")))
                return filename.substringAfterLast('.', "")
        }
        return ""
    }


    val entries = arrayOf(
            "ez" to "application/andrew-inset",
            "tsp" to "application/dsptype",
            "spl" to "application/futuresplash",
            "hta" to "application/hta",
            "hqx" to "application/mac-binhex40",
            "cpt" to "application/mac-compactpro",
            "nb" to "application/mathematica",
            "mdb" to "application/msaccess",
            "oda" to "application/oda",
            "ogg" to "application/ogg",
            "pdf" to "application/pdf",
            "key" to "application/pgp-keys",
            "pgp" to "application/pgp-signature",
            "prf" to "application/pics-rules",
            "rar" to "application/rar",
            "rdf" to "application/rdf+xml",
            "rss" to "application/rss+xml",
            "zip" to "application/zip",
            "apk" to "application/vnd.android.package-archive",
            "cdy" to "application/vnd.cinderella",
            "stl" to "application/vnd.ms-pki.stl",
            "odb" to "application/vnd.oasis.opendocument.database",
            "odf" to "application/vnd.oasis.opendocument.formula",
            "odg" to "application/vnd.oasis.opendocument.graphics",
            "otg" to "application/vnd.oasis.opendocument.graphics-template",
            "odi" to "application/vnd.oasis.opendocument.image",
            "ods" to "application/vnd.oasis.opendocument.spreadsheet",
            "ots" to "application/vnd.oasis.opendocument.spreadsheet-template",
            "odt" to "application/vnd.oasis.opendocument.text",
            "odm" to "application/vnd.oasis.opendocument.text-master",
            "ott" to "application/vnd.oasis.opendocument.text-template",
            "oth" to "application/vnd.oasis.opendocument.text-web",
            "doc" to "application/msword",
            "dot" to "application/msword",
            "docx" to "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "dotx" to "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
            "xls" to "application/vnd.ms-excel",
            "xlt" to "application/vnd.ms-excel",
            "xlsx" to "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "xltx" to "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
            "ppt" to "application/vnd.ms-powerpoint",
            "pot" to "application/vnd.ms-powerpoint",
            "pps" to "application/vnd.ms-powerpoint",
            "pptx" to "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "potx" to "application/vnd.openxmlformats-officedocument.presentationml.template",
            "ppsx" to "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "cod" to "application/vnd.rim.cod",
            "mmf" to "application/vnd.smaf",
            "sdc" to "application/vnd.stardivision.calc",
            "sda" to "application/vnd.stardivision.draw",
            "sdd" to "application/vnd.stardivision.impress",
            "sdp" to "application/vnd.stardivision.impress",
            "smf" to "application/vnd.stardivision.math",
            "sdw" to "application/vnd.stardivision.writer",
            "vor" to "application/vnd.stardivision.writer",
            "sgl" to "application/vnd.stardivision.writer-global",
            "sxc" to "application/vnd.sun.xml.calc",
            "stc" to "application/vnd.sun.xml.calc.template",
            "sxd" to "application/vnd.sun.xml.draw",
            "std" to "application/vnd.sun.xml.draw.template",
            "sxi" to "application/vnd.sun.xml.impress",
            "sti" to "application/vnd.sun.xml.impress.template",
            "sxm" to "application/vnd.sun.xml.math",
            "sxw" to "application/vnd.sun.xml.writer",
            "sxg" to "application/vnd.sun.xml.writer.global",
            "stw" to "application/vnd.sun.xml.writer.template",
            "vsd" to "application/vnd.visio",
            "abw" to "application/x-abiword",
            "dmg" to "application/x-apple-diskimage",
            "bcpio" to "application/x-bcpio",
            "torrent" to "application/x-bittorrent",
            "cdf" to "application/x-cdf",
            "vcd" to "application/x-cdlink",
            "pgn" to "application/x-chess-pgn",
            "cpio" to "application/x-cpio",
            "deb" to "application/x-debian-package",
            "udeb" to "application/x-debian-package",
            "dcr" to "application/x-director",
            "dir" to "application/x-director",
            "dxr" to "application/x-director",
            "dms" to "application/x-dms",
            "wad" to "application/x-doom",
            "dvi" to "application/x-dvi",
            "flac" to "application/x-flac",
            "pfa" to "application/x-font",
            "pfb" to "application/x-font",
            "gsf" to "application/x-font",
            "pcf" to "application/x-font",
            "pcf.Z" to "application/x-font",
            "mm" to "application/x-freemind",
            "spl" to "application/x-futuresplash",
            "gnumeric" to "application/x-gnumeric",
            "sgf" to "application/x-go-sgf",
            "gcf" to "application/x-graphing-calculator",
            "gtar" to "application/x-gtar",
            "tgz" to "application/x-gtar",
            "taz" to "application/x-gtar",
            "hdf" to "application/x-hdf",
            "ica" to "application/x-ica",
            "ins" to "application/x-internet-signup",
            "isp" to "application/x-internet-signup",
            "iii" to "application/x-iphone",
            "iso" to "application/x-iso9660-image",
            "jmz" to "application/x-jmol",
            "chrt" to "application/x-kchart",
            "kil" to "application/x-killustrator",
            "skp" to "application/x-koan",
            "skd" to "application/x-koan",
            "skt" to "application/x-koan",
            "skm" to "application/x-koan",
            "kpr" to "application/x-kpresenter",
            "kpt" to "application/x-kpresenter",
            "ksp" to "application/x-kspread",
            "kwd" to "application/x-kword",
            "kwt" to "application/x-kword",
            "latex" to "application/x-latex",
            "lha" to "application/x-lha",
            "lzh" to "application/x-lzh",
            "lzx" to "application/x-lzx",
            "frm" to "application/x-maker",
            "maker" to "application/x-maker",
            "frame" to "application/x-maker",
            "fb" to "application/x-maker",
            "book" to "application/x-maker",
            "fbdoc" to "application/x-maker",
            "mif" to "application/x-mif",
            "wmd" to "application/x-ms-wmd",
            "wmz" to "application/x-ms-wmz",
            "msi" to "application/x-msi",
            "pac" to "application/x-ns-proxy-autoconfig",
            "nwc" to "application/x-nwc",
            "o" to "application/x-object",
            "oza" to "application/x-oz-application",
            "p12" to "application/x-pkcs12",
            "p7r" to "application/x-pkcs7-certreqresp",
            "crl" to "application/x-pkcs7-crl",
            "qtl" to "application/x-quicktimeplayer",
            "shar" to "application/x-shar",
            "swf" to "application/x-shockwave-flash",
            "sit" to "application/x-stuffit",
            "sv4cpio" to "application/x-sv4cpio",
            "sv4crc" to "application/x-sv4crc",
            "tar" to "application/x-tar",
            "texinfo" to "application/x-texinfo",
            "texi" to "application/x-texinfo",
            "t" to "application/x-troff",
            "roff" to "application/x-troff",
            "man" to "application/x-troff-man",
            "ustar" to "application/x-ustar",
            "src" to "application/x-wais-source",
            "wz" to "application/x-wingz",
            "webarchive" to "application/x-webarchive",
            "crt" to "application/x-x509-ca-cert",
            "crt" to "application/x-x509-user-cert",
            "xcf" to "application/x-xcf",
            "fig" to "application/x-xfig",
            "xhtml" to "application/xhtml+xml",
            "3gpp" to "audio/3gpp",
            "snd" to "audio/basic",
            "mid" to "audio/midi",
            "midi" to "audio/midi",
            "kar" to "audio/midi",
            "mpga" to "audio/mpeg",
            "mpega" to "audio/mpeg",
            "mp2" to "audio/mpeg",
            "mp3" to "audio/mpeg",
            "m4a" to "audio/mpeg",
            "m3u" to "audio/mpegurl",
            "sid" to "audio/prs.sid",
            "aif" to "audio/x-aiff",
            "aiff" to "audio/x-aiff",
            "aifc" to "audio/x-aiff",
            "gsm" to "audio/x-gsm",
            "m3u" to "audio/x-mpegurl",
            "wma" to "audio/x-ms-wma",
            "wax" to "audio/x-ms-wax",
            "ra" to "audio/x-pn-realaudio",
            "rm" to "audio/x-pn-realaudio",
            "ram" to "audio/x-pn-realaudio",
            "ra" to "audio/x-realaudio",
            "pls" to "audio/x-scpls",
            "sd2" to "audio/x-sd2",
            "wav" to "audio/x-wav",
            "bmp" to "image/bmp",
            "gif" to "image/gif",
            "cur" to "image/ico",
            "ico" to "image/ico",
            "ief" to "image/ief",
            "jpeg" to "image/jpeg",
            "jpg" to "image/jpeg",
            "jpe" to "image/jpeg",
            "pcx" to "image/pcx",
            "png" to "image/png",
            "svg" to "image/svg+xml",
            "svgz" to "image/svg+xml",
            "tiff" to "image/tiff",
            "tif" to "image/tiff",
            "djvu" to "image/vnd.djvu",
            "djv" to "image/vnd.djvu",
            "wbmp" to "image/vnd.wap.wbmp",
            "ras" to "image/x-cmu-raster",
            "cdr" to "image/x-coreldraw",
            "pat" to "image/x-coreldrawpattern",
            "cdt" to "image/x-coreldrawtemplate",
            "cpt" to "image/x-corelphotopaint",
            "ico" to "image/x-icon",
            "art" to "image/x-jg",
            "jng" to "image/x-jng",
            "bmp" to "image/x-ms-bmp",
            "psd" to "image/x-photoshop",
            "pnm" to "image/x-portable-anymap",
            "pbm" to "image/x-portable-bitmap",
            "pgm" to "image/x-portable-graymap",
            "ppm" to "image/x-portable-pixmap",
            "rgb" to "image/x-rgb",
            "xbm" to "image/x-xbitmap",
            "xpm" to "image/x-xpixmap",
            "xwd" to "image/x-xwindowdump",
            "igs" to "model/iges",
            "iges" to "model/iges",
            "msh" to "model/mesh",
            "mesh" to "model/mesh",
            "silo" to "model/mesh",
            "ics" to "text/calendar",
            "icz" to "text/calendar",
            "csv" to "text/comma-separated-values",
            "css" to "text/css",
            "htm" to "text/html",
            "html" to "text/html",
            "323" to "text/h323",
            "uls" to "text/iuls",
            "mml" to "text/mathml",
            "txt" to "text/plain",
            "asc" to "text/plain",
            "text" to "text/plain",
            "diff" to "text/plain",
            "po" to "text/plain",
            "rtx" to "text/richtext",
            "rtf" to "text/rtf",
            "ts" to "text/texmacs",
            "phps" to "text/text",
            "tsv" to "text/tab-separated-values",
            "xml" to "text/xml",
            "bib" to "text/x-bibtex",
            "boo" to "text/x-boo",
            "h++" to "text/x-c++hdr",
            "hpp" to "text/x-c++hdr",
            "hxx" to "text/x-c++hdr",
            "hh" to "text/x-c++hdr",
            "c++" to "text/x-c++src",
            "cpp" to "text/x-c++src",
            "cxx" to "text/x-c++src",
            "h" to "text/x-chdr",
            "htc" to "text/x-component",
            "csh" to "text/x-csh",
            "c" to "text/x-csrc",
            "d" to "text/x-dsrc",
            "hs" to "text/x-haskell",
            "java" to "text/x-java",
            "lhs" to "text/x-literate-haskell",
            "moc" to "text/x-moc",
            "p" to "text/x-pascal",
            "pas" to "text/x-pascal",
            "gcd" to "text/x-pcs-gcd",
            "etx" to "text/x-setext",
            "tcl" to "text/x-tcl",
            "tex" to "text/x-tex",
            "ltx" to "text/x-tex",
            "sty" to "text/x-tex",
            "cls" to "text/x-tex",
            "vcs" to "text/x-vcalendar",
            "vcf" to "text/x-vcard",
            "3gpp" to "video/3gpp",
            "3gp" to "video/3gpp",
            "3g2" to "video/3gpp",
            "dl" to "video/dl",
            "dif" to "video/dv",
            "dv" to "video/dv",
            "fli" to "video/fli",
            "m4v" to "video/m4v",
            "mpeg" to "video/mpeg",
            "mpg" to "video/mpeg",
            "mpe" to "video/mpeg",
            "mp4" to "video/mp4",
            "VOB" to "video/mpeg",
            "qt" to "video/quicktime",
            "mov" to "video/quicktime",
            "mxu" to "video/vnd.mpegurl",
            "lsf" to "video/x-la-asf",
            "lsx" to "video/x-la-asf",
            "mng" to "video/x-mng",
            "asf" to "video/x-ms-asf",
            "asx" to "video/x-ms-asf",
            "wm" to "video/x-ms-wm",
            "wmv" to "video/x-ms-wmv",
            "wmx" to "video/x-ms-wmx",
            "wvx" to "video/x-ms-wvx",
            "avi" to "video/x-msvideo",
            "movie" to "video/x-sgi-movie",
            "ice" to "x-conference/x-cooltalk",
            "sisx" to "x-epoc/x-sisx-app"
    )

    init {
        mExtensionToMimeTypeMap = entries.toMap()
        mMimeTypeToExtensionMap = entries.reversed().map { it.second to it.first }.toMap()
    }
}

