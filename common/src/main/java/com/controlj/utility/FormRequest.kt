package com.controlj.utility

import com.controlj.framework.FilePaths
import java.io.File
import java.net.URL
import java.util.UUID

/**
 * Encapsulates the data for a POST request in application/x-www-form-urlencoded format
 * @param url The full url for the request
 * @param headers The headers to supply
 * @param data The form data
 */
class FormRequest(
        val url: URL,
        val data: FormData,
        val method: String = "POST",
        headers: Map<String, String> = mapOf()
) {

    val headers: Map<String, String> =
            headers + listOf("Content-type" to data.contentType, "Content-length" to data.dataLength.toString())
    /**
     * Write the request body to a file
     */
    fun fileData(): File {

        val file = File(FilePaths().tempDirectory, UUID.randomUUID().toString())
        val outputStream = file.outputStream()
        data.stream.copyTo(outputStream)
        outputStream.close()
        return file
    }
}
