package com.controlj.utility;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2/07/15
 * Time: 7:23 PM
 */

/**
 * This class implements a map, but limits the number of elements. Once the limit is reached, the oldest element is removed if a new one is added.
 * @param <K>
 * @param <V>
 */
public class LimitedMap<K, V> {

	final int limit;
	final HashMap<K, V> map;
	final LinkedList<K> list;

	public LimitedMap(int limit) {
		this.limit = limit;
		list = new LinkedList<K>();
		map = new HashMap<K, V>(limit);
	}

	public V put(K key, V object) {
		if(map.containsKey(key)) {
			list.remove(key);
			list.add(key);
			return map.put(key, object);
		}
		if(list.size() == limit) {
			K o = list.removeFirst();
			map.remove(o);
		}
		list.add(key);
		return map.put(key, object);
	}

	public V get(K key) {
		return map.get(key);
	}

	public void remove(K key) {
		if(map.containsKey(key)) {
			map.remove(key);
			list.remove(key);
		}
	}
}
