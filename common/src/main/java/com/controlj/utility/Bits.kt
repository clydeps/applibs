package com.controlj.utility

/**
 * Retrieve a single bit from an Int.
 * @receiver The value from which a bit is to be extracted
 * @param no The bit no, 0 based
 * @return True if the bit is a 1
 */
fun Int.bit(no: Int): Boolean {
    require(no in (0 until Int.SIZE_BITS)) { "Bit number $no is out of range" }
    return (ushr(no) and 1) == 1
}

fun UInt.bit(no: Int): Boolean {
    return toInt().bit(no)
}

/**
 * Set the given bit in the int.
 * @param no The bit number
 * @return the result, with the bit set
 */
fun Int.setBit(no: Int): Int {
    require(no in (0 until Int.SIZE_BITS)) { "Bit number $no is out of range" }
    return this or (1 shl no)
}

/**
 * Extract a bitfield from an Int
 */

fun Int.bitfield(pos: Int, len: Int): Int {
    require(pos >= 0 && pos + len <= Int.SIZE_BITS)
    return ushr(pos) and (1 shl len - 1)
}

fun Byte.bitfield(pos: Int, len: Int): Int {
    require(pos >= 0 && pos + len <= Byte.SIZE_BITS)
    return toInt().ushr(pos) and ((1 shl len) - 1)
}

fun Int.byte(no: Int): Byte {
    return nibble(no, 8).toByte()
}

/**
 * Get a nibble from an int. The nibble size defaults to 4.
 * @receiver The value from which a nibble is to be extracted
 * @param pos The number of the nibble, 0 based, starts with LS nibble
 * @param size The nibble size, defaults to 4
 * @return The nibble. Always treated as unsigned.
 */
fun Int.nibble(pos: Int, size: Int = 4): Int {
    require(size > 0 && pos * size in (0..Int.SIZE_BITS))
    return ushr(pos * size) and ((1 shl size) - 1)
}

/**
 * Assemble an unsigned int from a series of bits. The bits are listed LSB first. Missing bits are set to zero
 * For example fromBits(true, false, true) will return the value 5
 * @param bits The bits
 * @return The assembled UInt value
 */

fun bitsToUint(vararg bits: Boolean): UInt {
    return bits.foldIndexed(0u) { pos, acc, b ->
        if (b) acc or (1u shl pos) else acc
    }
}

/**
 * Create a byte from 4 bit nibbles
 */

fun nibblesToByte(nibble0: Int, nibble1: Int = 0): Byte {
    return (nibble0 + (nibble1 shl 4)).toByte()
}

/**
 * Retrieve a single bit from an Byte.
 * @receiver The value from which a bit is to be extracted
 * @param no The bit no, 0 based
 * @return True if the bit is a 1
 */
fun Byte.bit(no: Int): Boolean {
    require(no in (0 until Byte.SIZE_BITS)) { "Bit number $no is out of range" }
    return (toUInt().ushr(no) and 1) == 1
}

/**
 * Get a nibble from a Byte. The nibble size defaults to 4.
 * @receiver The value from which a nibble is to be extracted
 * @param pos The number of the nibble, 0 based, starts with LS nibble
 * @param size The nibble size, defaults to 4
 * @return The nibble. Always treated as unsigned.
 */
fun Byte.nibble(pos: Int, size: Int = 4): Byte {
    require(size > 0 && pos * size in (0..Byte.SIZE_BITS))
    return (asUInt().ushr(pos * size) and ((1 shl size) - 1)).toByte()
}

/**
 * Assemble an unsigned int from a series of bits. The bits are listed LSB first. Missing bits are set to zero
 * For example fromBits(true, false, true) will return the value 5
 * @param bits The bits
 * @return The assembled UInt value
 */

fun bitsToByte(vararg bits: Boolean): Byte {
    require(bits.size <= Byte.SIZE_BITS)
    return bits.foldIndexed(0.toUByte()) { pos, acc, b ->
        if (b) acc or (1u shl pos).toUByte() else acc
    }.toByte()
}

fun bytesToInt(vararg bytes: Byte): Int {
    require(bytes.size <= Int.SIZE_BYTES)
    return bytes.foldIndexed(0) { pos, acc, b ->
        acc + (b.toUInt() shl (pos * Byte.SIZE_BITS))
    }
}

/**
 * Get a byte value treated as unsigned
 */

fun Byte.asUInt(): Int = toInt() and 0xFF

/**
 * Sign extend a value based on a given sign bit
 */

fun Int.signExtend(bitno: Int): Int {
    require(bitno in (0 until Int.SIZE_BITS))
    val bits = UInt.SIZE_BITS - bitno - 1
    return shl(bits).shr(bits)
}

fun UInt.signExtend(bitno: Int): Int {
    return toInt().signExtend(bitno)
}
