/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlj.utility;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author clyde
 */

public class HtmlForm {

	public URL action;
	public String method;
	public String name = null;
	public FormInput submit = null;
	Map<String, FormInput> inputs = new HashMap<String, FormInput>();
	Map<String, FormInput> submits = new HashMap<String, FormInput>();

	public HtmlForm(URL action, String method) {
		this.action = action;
		if(method == null || method.equals(""))
			method = "GET";
		this.method = method;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<form ");
		if(name != null) {
			sb.append("name=\"");
			sb.append(name);
			sb.append("\" ");
		}
		if(isPostMethod())
			sb.append("method=\"post\"");
		sb.append(">\n\t");
		for(FormInput f : getInputs()) {
			sb.append(f.toString());
			sb.append("\n\t");
		}
		if(submit != null)
			sb.append(submit.toString());
		sb.append("\n</form>");
		return sb.toString();
	}

	public String getPath() {
		return action.toExternalForm();
	}

	public Collection<FormInput> getInputs() {
		return inputs.values();
	}

	public FormInput findInput(String regex) {
		for(FormInput f : inputs.values())
			if(f.name.matches(regex))
				return f;
		for(FormInput f : submits.values())
			if(f.name.matches(regex))
				return f;
		return null;
	}

	public Collection<FormInput> getSubmits() {
		return submits.values();
	}

	public void setSubmit(FormInput submit) {
		this.submit = submit;
	}

	public void add(FormInput f) {
		if("".equals(f.name))
			return;
		if(f.type.equalsIgnoreCase("submit") || f.type.equals("image")) {
			submits.put(f.name, f);
			submit = f;
		} else
			inputs.put(f.name, f);
	}

	void addValue(StringBuilder sb, FormInput f) {
		if(f.type.equals("image")) {
			addValue(sb, f.name + ".x", "0");
			addValue(sb, f.name + ".y", "0");
		} else if(f.value != null)
			addValue(sb, f.name, f.value);
	}

	void addValue(StringBuilder sb, String name, String value) {
		if(sb.length() != 0)
			sb.append('&');
		try {
			sb.append(URLEncoder.encode(name, "UTF-8"));
			sb.append('=');
			sb.append(URLEncoder.encode(value, "UTF-8"));
		} catch(UnsupportedEncodingException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}

	public String getValues() {
		return getValues(submit);
	}

	public String getValues(FormInput submit) {
		StringBuilder sb = new StringBuilder();

		for(FormInput f : inputs.values())
			addValue(sb, f);
		if(submit != null)
			addValue(sb, submit);
		return sb.toString();
	}

	public void setValue(String name, String value) {
		FormInput f = inputs.get(name);
		if(f == null) {
			f = new FormInput(name, value, "");
			inputs.put(name, f);
		} else
			f.value = value;
	}

	public URL getAction() {
		return action;
	}

	public void setAction(URL action) {
		this.action = action;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	boolean isPostMethod() {
		return method != null && method.equalsIgnoreCase("post");
	}
}