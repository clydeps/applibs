package com.controlj.utility

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.Disposable
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * An Observable operator that will call a lambda if the source fails to deliver an item with the specified timeout. The timeout
 * restarts on each item delivered. All items from the source observable will be delivered, and the timeout will not cause an error
 * or exception.
 *
 * If the repeat parameter is true, then the lambda will be called repeatedly while no items are delivered.
 * User: clyde
 * Date: 12/5/18
 * Time: 19:29
 */
class ObservableTimeout<T : Any>(
    private val child: Observer<in T>,
    private val timeout: Long,
    private val timeUnit: TimeUnit,
    private val lambda: () -> Unit,
    private val repeat: Boolean = false,
    private val scheduler: Scheduler? = null
) : Observer<T> {

    private var d = Disposable.disposed()
    private var disposable = Disposable.disposed()

    private fun callLambda() {
        scheduler?.scheduleDirect(lambda) ?: lambda()
    }

    @Synchronized
    private fun startTimer() {
        disposable.dispose()
        if (repeat)
            disposable = Observable.interval(timeout, timeUnit)
                .subscribe({ callLambda() }, child::onError)
        else
            disposable = Completable.timer(timeout, timeUnit)
                .subscribe(this::callLambda, child::onError)
    }

    override fun onComplete() {
        disposable.dispose()
        child.onComplete()
    }

    override fun onSubscribe(d: Disposable) {
        this.d = d
        child.onSubscribe(d)
        startTimer()
    }

    override fun onNext(t: T) {
        startTimer()
        child.onNext(t)
    }

    override fun onError(t: Throwable) {
        disposable.dispose()
        child.onError(t)
    }
}

fun <R : Any> Observable<R>.onDataMissing(
    timeout: Long,
    timeUnit: TimeUnit,
    repeat: Boolean = false,
    scheduler: Scheduler? = null,
    lambda: () -> Unit
): Observable<R> {
    return lift { ObservableTimeout(it, timeout, timeUnit, lambda, repeat, scheduler) }
}

fun <R : Any> Observable<R>.onDataMissing(timeout: Long, timeUnit: TimeUnit, lambda: () -> Unit): Observable<R> {
    return lift { ObservableTimeout(it, timeout, timeUnit, lambda, false, null) }
}

fun <R : Any> Observable<R>.doOnFirst(lambda: (item: R) -> Unit): Observable<R> {
    return compose { observable ->
        Observable.defer {
            val first = AtomicBoolean(true)
            observable.doOnNext {
                if (first.compareAndSet(true, false))
                    lambda(it)
            }

        }
    }
}
