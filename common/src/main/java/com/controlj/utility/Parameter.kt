package com.controlj.utility

import java.io.UnsupportedEncodingException
import java.net.URLEncoder

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 7/02/2016
 * Time: 4:24 PM
 */
open class Parameter(val name: String, val value: String) {
    val stamp: Boolean = false

    fun formPart(separator: String): String {
        try {
            return "--" + separator + "\r\nContent-Disposition: form-data; name=\"" + URLEncoder.encode(name, "UTF-8") + "\"\r\n\r\n" + value + "\r\n"
        } catch (e: UnsupportedEncodingException) {
            return ""
        }

    }

    fun URLEncode(): String? {
        try {
            return URLEncoder.encode(name, "UTF-8") + "=" + URLEncoder.encode(value, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            return null
        }

    }

    override fun toString(): String {
        return "$name:$value"
    }

    override fun equals(other: Any?): Boolean {
        return name == (other as Parameter).name
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}

