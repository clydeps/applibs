package com.controlj.utility;

import java.util.List;

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 23/02/2016
 * Time: 6:30 PM
 */
public class Utilities {
	static public String implode(List<? extends Object> bb, String separator) {
		StringBuilder sb = new StringBuilder();
		for(Object o : bb) {
			if(sb.length() != 0)
				sb.append(separator);
			sb.append(o.toString());
		}
		return sb.toString();
	}
}
