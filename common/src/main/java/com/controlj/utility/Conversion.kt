package com.controlj.utility

/**
 * Created by clyde on 16/8/17.
 */

interface Conversion {
    val ratio: Double

    val label: String

    fun convertFrom(value: Double): Double

    fun convertTo(value: Double): Double

    fun toString(value: Double): String
}
