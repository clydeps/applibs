package com.controlj.utility

import java.util.Locale

/**
 * Created by clyde on 16/8/17.
 */

class UnitRatio(numerator: Conversion, denominator: Conversion) : Conversion {
    override val ratio: Double
    override val label: String

    override fun toString(value: Double): String {
        val converted = convertTo(value)
        return if (converted < 10 && converted > -10) String.format(Locale.US, "%.1f", converted) else String.format(Locale.US, "%.0f", converted)
    }

    init {
        ratio = numerator.ratio / denominator.ratio
        label = numerator.label + "/" + denominator.label
    }

    override fun convertFrom(value: Double): Double {
        return value * ratio
    }

    override fun convertTo(value: Double): Double {
        return value / ratio
    }

}
