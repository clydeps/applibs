/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlj.utility

import com.controlj.logging.CJLog

import java.nio.charset.Charset
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.concurrent.TimeUnit
import kotlin.experimental.and

/**
 * @author clyde
 */
object Hash {

    val TAG = Hash::class.java.simpleName

    internal val hexArray = "0123456789ABCDEF".toCharArray()

    val MAX_JITTER = TimeUnit.MINUTES.toMillis(10)

    fun sha256(input: String): String {
        return hash(input, "SHA-256")
    }

    fun md5(input: String): String {
        return hash(input, "MD5")
    }

    fun hash(input: String, algorithm: String): String {
        try {
            val md = MessageDigest.getInstance(algorithm)
            //ResourceUtil.logd(TAG, "Hashing %s", input);
            md.update(input.toByteArray(Charset.forName("UTF-8")))
            return bytesToHex(md.digest())
        } catch (ex: NoSuchAlgorithmException) {
            CJLog.logMsg("No such hash algorithm: %s", algorithm)
            return ""
        }

    }

    fun bytesToHex(bytes: ByteArray): String {
        val hexChars = CharArray(bytes.size * 2)
        for (j in bytes.indices) {
            val v = bytes[j].toInt() and 0xFF
            hexChars[j * 2] = hexArray[v shr 4]
            hexChars[j * 2 + 1] = hexArray[v and 0x0F]
        }
        return String(hexChars)
    }

    fun hashNow(key: String): String {
        val now = java.lang.Long.toHexString(System.currentTimeMillis())
        return sha256(now + key)
    }

    fun createHash(secret: String, token: String): String {
        return sha256(token + secret)
    }

    class KeyPair(val token: String, val hash: String)

    @JvmOverloads
    fun createHash(secret: String, now: Long = System.currentTimeMillis()): KeyPair {
        val token = now.toString(16)
        return KeyPair(token, createHash(secret, token))
    }

    fun createHashString(secret: String): String {
        val pair = createHash(secret, System.currentTimeMillis())
        return pair.token + ":" + pair.hash
    }

    fun checkHash(secret: String, token: String, hash: String): Boolean {
        val now = System.currentTimeMillis()
        val then = java.lang.Long.parseLong(token, 16)
        return then < now + MAX_JITTER && then > now - MAX_JITTER && sha256(token + secret) == hash
    }
}
