package com.controlj.utility

import java.util.concurrent.ConcurrentHashMap

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2018-12-18
 * Time: 12:09
 *
 * A cache of objects, limited in size, and maintained in recent use order. Adding a new element to the cache
 * when full will purge the least recently used object.
 * Fetching an object will bring it back to the head of the cache.
 * @param maxEntries The maximum number of objects to cache
 * @param onPurge A block to be called when an item is purged from the cache. Not called by explicit remove()
 */
class LRUCache<K: Any, V:Any>(private val maxEntries: Int, private val onPurge: (V) -> Unit = {}) {
    private val map = ConcurrentHashMap<K, V>(maxEntries, 0.75f)
    private val lastUsed = LinkedHashSet<K>()

    /**
     * Move the given key to the head of the recently used queue. No-op if it is not in the queue already
     */
    @Synchronized
    fun touch(key: K) {
        update(key, false)
    }

    /**
     *
     */
    private fun update(key: K, force: Boolean = true) {
        if (lastUsed.remove(key) || force) lastUsed.add(key)
    }

    /**
     * Add an object to the cache, limiting the size to the supplied maximum.
     * @param key The object key
     * @param value The object itself
     * @return the object
     */
    @Synchronized
    fun put(key: K, value: V): V {
        update(key)
        map[key] = value
        trim()
        return value
    }

    /**
     * Remove an object from the cache.
     * @param key The object key
     */
    @Synchronized
    fun remove(key: K): V? {
        return map[key]?.also {
            map.remove(key)
            lastUsed.remove(key)
        }
    }

    /**
     * Trim the cache if required.
     */

    private fun trim() {
        if (map.size > maxEntries) {
            remove(lastUsed.first())?.let { onPurge(it) }
        }
    }

    /**
     * Get an object from cache.
     * @param key The object key. May be null, which always results in a null return.
     * @return Returns the object if found, null otherwise
     */

    @Synchronized
    fun get(key: K?): V? {
        if (key == null) return null
        return map[key]?.also { update(key) }
    }

    /**
     * Get an object from cache, populating it from the supplied block if not already present.
     * @param key The object key.
     * @param default A block to compute a default value to insert.
     */
    @Synchronized
    fun get(key: K, default: () -> V): V {
        update(key)
        return map.getOrPut(key, default).also { trim() }
    }

    /**
     * Empty the cache. Does not trigger a callback for removed items
     */
    @Synchronized
    fun clear() {
        map.clear()
        lastUsed.clear()
    }

    /**
     * Get a list of the values currently in the cache.
     */

    fun values(): List<V> {
        return map.values.toList()
    }

    val size: Int get() = map.size
}
