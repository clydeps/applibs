/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlj.utility;

/**
 * @author clyde
 */
public class FormInput {

	String name;
	String value;
	String type;

	public FormInput(String name, String value, String type) {
		this.name = name;
		this.value = value;
		if(type == null || type.equals(""))
			type = "text";
		this.type = type;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("<input type=\"");
		sb.append(type);
		sb.append("\" name=\"");
		sb.append(name);
		sb.append("\" value=\"");
		sb.append(value);
		sb.append("\">");
		return sb.toString();
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}

