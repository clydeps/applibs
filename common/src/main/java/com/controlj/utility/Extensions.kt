package com.controlj.utility

import com.controlj.data.Constants.INVALID_DATA
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.net.URI
import java.net.URLDecoder
import java.nio.ByteBuffer
import java.util.Locale
import kotlin.reflect.KProperty0
import kotlin.reflect.jvm.isAccessible

/**
 * Created by clyde on 29/3/18.
 */

fun Int.isOdd() = this and 1 == 1

fun Byte.toUInt() = toInt() and 0xff
fun Short.toUInt() = toInt() and 0xffff
fun ByteBuffer.advance(i: Int) = position(position() + i)
fun ByteBuffer.fill(len: Int, value: Byte = 0) {
    put(ByteArray(len, { value }))
}

fun ByteBuffer.addDouble(value: Double, len: Int) {
    addString(String.format(Locale.US, "%-10.5f", value), len)
}

fun ByteBuffer.getString(len: Int): String {
    val arr = ByteArray(len)
    get(arr)
    return arr.trimString()
}

fun ByteBuffer.getString(): String {
    val len = int
    val arr = ByteArray(len)
    get(arr)
    return arr.trimString()
}

fun ByteBuffer.putString(s: String): ByteBuffer {
    val bytes = s.toByteArray()
    putInt(bytes.size)
    put(bytes)
    return this
}

fun ByteArray.trimString(): String {
    for (i in 0 until size)
        if (this[i] == 0.toByte())
            return String(this, 0, i)
    return String(this)
}

// write a string to a buffer, padding to len with nulls
fun ByteBuffer.addString(s: String, len: Int) {
    val bytes = s.toByteArray()
    if (bytes.size >= len)
        put(bytes, 0, len)
    else {
        put(bytes)
        for (i in 0 until len - bytes.size)
            put(0)
    }
}
// formatting for values

const val FORMAT_HHMM = -100        // format as hh:mm, value in seconds
const val FORMAT_OPT_DEC = -101     // format value as n.n when less than 10, as integer otherwise
const val FORMAT_HMS = -102        // format as 00h00m or 00m00s
private val powers = doubleArrayOf(0.0, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, 10000000.0)

/**
 * Format a double into a string. If the value passed is invalid, or the dataValid flag is false, it will
 * return the string "---"
 *
 * Special precision arguments available are:
 *
 * FORMAT_HMS - the value is presumed to be in seconds, and will be formatted e.g. as 5m20s or 3h02m
 * FORMAT_HHMM - takes a value in minutes and formats as 01:02
 * FORMAT_OPT_DEC - will format values < 10 with one decimal place and 10 or above as whole numbers
 *
 * @param precision The number of decimal places to provide. See above for special cases
 * @param dataValid If false, just return "---"
 * @param suffix A suffix to append to the returned string
 * @param sign If true, include a + for positive values
 * @param unit An optional unit to convert to before formatting.
 */
fun Double.formatted(
    precision: Int = 0,
    dataValid: Boolean = true,
    suffix: String = "",
    sign: Boolean = false,
    unit: Units.Unit = Units.Unit.IDENTITY
): String {
    if (!dataValid || this == INVALID_DATA)
        return "---"
    val value = unit.convertTo(this)
    val signStr = if (sign) "+" else ""
    return when (precision) {
        FORMAT_HMS -> Units.Unit.HMS.toString(value)
        FORMAT_HHMM -> String.format(Locale.ROOT, "%02d:%02d", (value / 60).toInt(), (value % 60).toInt())
        FORMAT_OPT_DEC -> String.format(Locale.ROOT, if (value < 9.95) "%.1f$suffix" else "%.0f$suffix", value)
        in 0..Int.MAX_VALUE -> String.format("%$signStr.${precision}f$suffix", value)
        in -1 downTo Int.MIN_VALUE -> String.format(
            Locale.ROOT,
            "%${signStr}d$suffix",
            Math.round(value * powers[-precision - 1] * 10.0)
        )
        else -> String.format(Locale.ROOT, "%${signStr}d$suffix", Math.round(value * 10))
    }
}

/**
 * Get a map of parameters to values from a uri
 */

fun URI.queryMap(): Map<String, String> {
    if (query == null)
        return mapOf()
    return query.split('&').map {
        val param = it.split('=', limit = 2)
        URLDecoder.decode(param.getOrElse(0, { "" }), "UTF-8") to
            URLDecoder.decode(param.getOrElse(1, { "" }), "UTF-8")
    }.toMap()
}

fun Duration.hms(): String {
    return Units.Unit.HMS.toString(seconds.toDouble())
}

/**
 * Get the age of an instant in seconds
 */

val Instant.ageSeconds get() = (System.currentTimeMillis() - toEpochMilli()) / 1000

/**
 * Convert a string to title case
 */

fun String.toTitleCase(): String {
    return take(1).uppercase() + takeLast((length - 1).coerceAtLeast(0)).lowercase()
}


private val REGEX_EMAIL = Regex("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+[.][a-zA-Z]{2,}$")

fun String?.isValidEmail(): Boolean {
    return this?.matches(REGEX_EMAIL) == true
}

/**
 * Check if a lazy variable has been initialised
 */

val KProperty0<*>.isLazyInitialised: Boolean
    get() {
        // Prevent IllegalAccessException from JVM access check
        isAccessible = true
        return (getDelegate() as Lazy<*>).isInitialized()
    }

/**
 * A fast version of instantNow
 */

val instantNow: Instant get() = Instant.ofEpochMilli(System.currentTimeMillis())

/**
 * Current time in seconds
 */

val secsNow: Long get() = System.currentTimeMillis() / 1000

/**
 * Convert to/from json using a vanilla converter
 */

/**
 * Parse a Json string to an object
 * @param T The desired object type
 */

val vanillaGson = Gson()
inline fun <reified T> String.fromJson(): T {
    return vanillaGson.fromJson(this, object : TypeToken<T>() {}.type)
}

/**
 * Convert an int to a string with a minimum number of digits, zero filled.
 */

fun Int.zeroPadded(count: Int): String = toString().padStart(count, '0')

