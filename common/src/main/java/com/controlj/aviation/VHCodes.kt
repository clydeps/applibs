package com.controlj.aviation

import java.lang.StringBuilder

object VHCodes {

    private fun charCode(char: Char): Int {
        when (char) {
            in ('0'..'9') -> return char.code - '0'.code + 26
            in ('A'..'Z') -> return char.code - 'A'.code
            in ('a'..'z') -> return char.code - 'a'.code
            else -> error("Invalid char")
        }
    }

    fun codeFrom(call: String): Int {
        val shortCall = call.filter { it.isLetterOrDigit() }.uppercase().takeLast(3)
        require(shortCall.length == 3)
        return VHBase + charCode(shortCall[2]) +
            charCode(shortCall[1]) * 36 +
            charCode(shortCall[0]) * 36 * 36
    }

    fun callFrom(code: Int): String {
        val callChars = StringBuilder()
        (0..2).fold(code - VHBase) { c, _ ->
            callChars.append(codeMap[c % 36])
            c / 36
        }
        return callChars.reversed().toString()
    }


    private val codeMap = ('A'..'Z') + ('0'..'9')
    const val VHBase = 0x7C0000
}
