package com.controlj.stratux.sim

import com.controlj.aviation.VHCodes
import com.controlj.data.Location
import com.controlj.location.LocationProvider
import com.controlj.location.LocationSource
import com.controlj.traffic.TrafficProvider
import com.controlj.traffic.TrafficTarget
import com.controlj.utility.Units
import com.controlj.utility.from
import com.controlj.viewmodel.Listenable
import com.controlj.viewmodel.ListenableValue
import io.reactivex.rxjava3.core.Observable
import org.threeten.bp.Instant
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt
import kotlin.random.Random

object LocationProviderSimulation : LocationProvider {

    override val name: String = "LocationProviderSimulator"
    override var priority: Int = Location.DEMO_PRIORITY
    override val enabled: ListenableValue<Boolean> = Listenable(true)
    override val isConnected: Boolean = true
    override val authorisationStatus: LocationProvider.AuthorisationStatus = LocationProvider.AuthorisationStatus.Always

    var initialLocation = Location.of(
        latitude = -28.149444444444445,
        longitude = 151.94305555555556,
        altitude = Units.Unit.FOOT.convertFrom(2500.0),
        speed = Units.Unit.KNOT.convertFrom(150.0),
        track = 0.0,
        creator = creator,
        accuracyH = 6.0,
        accuracyV = 9.0,
    )

    override val locationObserver: Observable<Location> = Observable.interval(1L, TimeUnit.SECONDS)
        .map { initialLocation.extrapolate(it.toDouble()) }
        .startWithItem(initialLocation)
        .share()
}

object TrafficProviderSimulation : TrafficProvider {

    private const val maxDistance = 20000.0
    const val maxTargets = 8

    private val codeMap = ('A'..'Z') + ('0'..'9')
    private val categories = listOf(
        TrafficTarget.EmitterCategory.Glider,
        TrafficTarget.EmitterCategory.Large,
        TrafficTarget.EmitterCategory.Light,
        TrafficTarget.EmitterCategory.Rotorcraft,
        TrafficTarget.EmitterCategory.Heavy,
        TrafficTarget.EmitterCategory.Fighter,
        TrafficTarget.EmitterCategory.UAV,
    )

    private fun newTarget(origin: Location): TrafficTarget.of {
        val ourTrack = LocationSource.lastLocation.track.roundToInt()
        val bearing = Random.nextInt(ourTrack-80, ourTrack+80).toDouble()
        val track = (bearing + Random.nextInt(-10, 10) + 540) % 360.0
        val distance = Random.nextDouble(2000.0, maxDistance)
        val altitude = (Random.nextInt(-10, 50) * 100.0).from(Units.Unit.FOOT) + origin.altitude
        val position = origin.translate(bearing, distance)
        val speed = (Random.nextInt(10, 20) * 10.0).from(Units.Unit.KNOT)
        val vSpeed = (Random.nextInt(-10, 10) * 100.0).from(Units.Unit.FPM)
        val code = Random.nextInt(0x7C0000, 0x7CB640)
        val callSign = VHCodes.callFrom(code)
        return TrafficTarget.of(
            position.latitude,
            position.longitude,
            LocationProviderSimulation.creator,
            speed,
            track,
            altitude,
            Instant.now(),
            20.0,
            20.0,
            TrafficTarget.EmitterAddress(TrafficTarget.AddressType.AdsbIcao, code),
            categories.random(),
            callSign = callSign,
            vSpeed,
            isExtrapolated = false,
        )
    }

    private var targets = listOf<TrafficTarget>()

    private fun updateTraffic() {
        val validTargets = targets.filter { it.valid && it.distance < maxDistance }.map { it.getExtrapolated() }
        targets = validTargets +
            (validTargets.size until maxTargets).map { newTarget(LocationSource.lastLocation) }
    }

    override val name: String = "TrafficProviderSimulator"
    override val enabled: ListenableValue<Boolean> = Listenable(true)
    override val isConnected: Boolean = true
    override val trafficObserver: Observable<TrafficTarget> = Observable.interval(1, TimeUnit.SECONDS)
        .flatMap {
            updateTraffic()
            Observable.fromIterable(targets)
        }
        .share()
}
