package com.controlj.mock

import com.controlj.logging.Destination
import java.io.File

/**
 * Created by clyde on 2/7/18.
 */
class SyserrDestination: Destination {
    override fun sendMessage(deviceID: String, message: String) {
        System.err.print(message)
    }
}
