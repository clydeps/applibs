package com.controlj.framework

import io.mockk.every
import io.mockk.mockk
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.io.File

class FileUpdaterTest {

    val dbdir = File("data", "testdata")
    val file0 = File(dbdir, "Banner.png")
    val file1 = File(dbdir, "Banner1.png")
    val meta = File(dbdir, "testdatabase.meta")
    val assetDir = File("data", "assets")

    @Before
    fun setup() {
        FilePaths.init(mockk {
            every { databaseDirectory } returns dbdir
            every { openAsset(any()) } answers {
                val file = File(assetDir, firstArg())
                if (file.exists())
                    file.inputStream()
                else
                    null
            }
        })
        dbdir.deleteRecursively()
        dbdir.mkdirs()
    }

    @After
    fun tearDown() {
        dbdir.deleteRecursively()
    }

    @Test
    fun updateFileFromAssets() {
        assertFalse(file0.exists())
        assertFalse(file1.exists())
        assertFalse(meta.exists())
        FileUpdater.updateFileFromAssets(meta)
        assertTrue(meta.exists())
        assertTrue(file0.exists())
        assertTrue(file1.exists())
        assertEquals(21935, file0.length())
        assertEquals(13151, file1.length())
        val contents = File(assetDir, meta.name).readText().replace('4', '3')
        file0.delete()
        meta.writeText(contents)
        FileUpdater.updateFileFromAssets(meta)
        assertTrue(file0.exists())
    }
}
