package com.controlj.aviation

import junit.framework.TestCase
import org.junit.Assert
import org.junit.Test

class VHCodesTest : TestCase() {

    @Test
    fun testVH() {
        Assert.assertEquals(0x7C1FD7, VHCodes.codeFrom("VH-GKP"))
        Assert.assertEquals(0x7C7048, VHCodes.codeFrom("wgq"))
        Assert.assertEquals("GKP", VHCodes.callFrom(0x7C1FD7))
        Assert.assertEquals("WGQ", VHCodes.callFrom(0x7C7048))
    }
}
