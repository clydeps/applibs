package com.controlj.widget

import com.controlj.data.Priority
import com.controlj.graphics.CColor
import com.controlj.settings.Range
import com.controlj.settings.RangeSet
import com.controlj.settings.RangeState
import com.controlj.settings.Setting
import com.controlj.settings.UnitSetting
import com.controlj.utility.Units
import io.mockk.every
import io.mockk.mockkObject

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 2019-06-20
 * Time: 12:48
 */
class TextRangeViewTest {

    val pwrRanges = RangeSet.from(
            Units.Unit.IDENTITY,
            "% Power",
            0.0,
            Range(10.0, RangeState.Neutral),
            Range(100.0, RangeState.Normal),
            Range(105.0, RangeState.CautionHigh),
            Range(110.0, RangeState.AlarmHigh)
    )

    @Before
    fun before() {
        Setting.map.clear()
    }
    @Test
    fun testPwr() {
        val pwr = TextRangeView("PWR", {pwrRanges}, 0, CColor.BLACK)
        pwr.targetValue = -1.0
        assertEquals(Priority.NONE.backgroundColor, pwr.currentColor)
        pwr.targetValue = 10.1
        assertEquals(CColor.NORMAL, pwr.currentColor)
        assertEquals("10", pwr.valueText)
        pwr.targetValue = 50.0
        assertEquals(pwr.currentColor, CColor.NORMAL)
        assertEquals("50", pwr.valueText)
        pwr.targetValue = 101.0
        assertEquals(pwr.currentColor, CColor.CAUTION)
        assertEquals("101", pwr.valueText)
        pwr.targetValue = 106.0
        assertEquals(pwr.currentColor, CColor.ALARM)
        assertEquals("106", pwr.valueText)
    }

    val mpkey = "mp"

    @Test
    fun testMp() {
        val mpRanges = RangeSet.from(
                Units.Unit.INHG,
                mpkey,
                10.0,
                Range(12.0, RangeState.Neutral),
                Range(31.0, RangeState.Normal),
                Range(33.0, RangeState.CautionHigh),
                Range(40.0, RangeState.AlarmHigh)
        )

        val mpSetting = UnitSetting("mp", "MP", "Manifold Pressure unit",
                listOf(Units.Unit.INHG, Units.Unit.PSI, Units.Unit.KPA))
        mockkObject(Setting)
        every {Setting.get<UnitSetting>(mpkey)} returns mpSetting
        val unit = Units.Unit.INHG
        val mp = TextRangeView("MP", {mpRanges}, 1, CColor.BLACK)
        mp.targetValue = unit.convertFrom(0.0)
        assertEquals(Priority.NONE.backgroundColor, mp.currentColor)
        assertEquals("0.0", mp.valueText)
        mp.targetValue = unit.convertFrom(12.1)
        assertEquals(CColor.NORMAL, mp.currentColor)
        assertEquals("12.1", mp.valueText)
        mp.targetValue = unit.convertFrom(30.92)
        assertEquals(CColor.NORMAL, mp.currentColor)
        assertEquals("30.9", mp.valueText)
        mp.targetValue = unit.convertFrom(30.98)
        assertEquals(CColor.NORMAL, mp.currentColor)
        assertEquals("31.0", mp.valueText)
        mp.targetValue = unit.convertFrom(31.5)
        assertEquals(CColor.CAUTION, mp.currentColor)
        assertEquals("31.5", mp.valueText)
        mp.targetValue = unit.convertFrom(35.0)
        assertEquals(CColor.ALARM, mp.currentColor)
        assertEquals("35.0", mp.valueText)

    }
}
