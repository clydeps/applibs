package com.controlj.ui

import com.controlj.settings.MapDataStore
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import com.controlj.viewmodel.ViewModel
import com.google.gson.annotations.Expose
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import java.io.File

class ViewModelStoreTest {

    val dataStore = MapDataStore(true)
    val dataDir = File("viewmodels")

    @Before
    fun setup() {
        unmockkAll()
        MockScheduler.reset()
        dataDir.deleteRecursively()
        mockkObject(ViewModelStore)
    }

    class Model1(@Expose var value: Int = 1) : ViewModel

    class Modelx(@Expose var value: Int = 5) : ViewModel
    class Modely(@Expose var value: Int = 7) : ViewModel
    class Modelz(@Expose var value: Int = 7) : ViewModel

    @Test
    fun viewModelStoreTest() {
        val modely = Modely(10)
        modely.save(dataStore)
        assertFalse(dataDir.isDirectory)
        ViewModelStore.setup(dataDir)
        assertTrue(dataDir.isDirectory)
        ViewModelStore.setup(dataStore)
        // simple load, new instance creation
        val model = ViewModelStore.loadViewModel(Model1::class.java)
        assertEquals(1, model.value)

        // check that the returned value is a singleton
        model.value = 2
        val model1 = ViewModelStore.loadViewModel(Model1::class.java)
        assertEquals(2, model1.value)
        assertTrue(model === model1)

        // check that the datastore version is not returned
        Model1(8).save(dataStore)
        assertEquals(2, ViewModelStore.loadViewModel(Model1::class.java).value)
        Modelx(10).save(dataStore)
        // check that the datastore version is returned if nothing else is there.
        assertEquals(10, ViewModelStore.loadViewModel(Modely::class.java).value)

        ViewModelStore.save()       // save files
        ViewModelStore.setup(dataDir)
        Model1(20).save()
        // check that it gets reloaded
        assertEquals(20, ViewModelStore.loadViewModel<Model1>().value)
        // check async fetch
        val sync = ViewModelStore.loadViewModel(Model1::class.java)
        var modelFetched: Model1? = null
        ViewModelStore.fetch(Model1::class.java).subscribe({
            modelFetched = it
        },
                {
                    fail(it.toString())
                })
        testScheduler.triggerActions()
        assertTrue(modelFetched === sync)
    }
}
