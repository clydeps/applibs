package com.controlj.database

import com.controlj.framework.FilePaths
import com.controlj.test.MockBackgroundServiceProvider
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.ktorm.dsl.and
import org.ktorm.dsl.eq
import org.ktorm.dsl.insert
import org.ktorm.dsl.or
import org.ktorm.entity.Entity
import org.ktorm.entity.all
import org.ktorm.entity.count
import org.ktorm.entity.sequenceOf
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar
import java.io.File
import java.util.concurrent.CountDownLatch

class CJDatabaseTest {

    companion object {
        const val DBNAME = "test.db"
        const val OLD_DB = "old.db"
    }

    open class DatabaseV1 : CJDatabase(DBNAME) {
        override val driverClass: String = "org.sqlite.JDBC"
        override val jdbcUrl: String = "jdbc:sqlite"
        override val updateStatements = listOf(
            listOf("create table T_DEPARTMENT(id integer not null primary key autoincrement, name varchar(128))")
        )
    }

    class DatabaseV3 : DatabaseV1() {
        override val updateStatements = super.updateStatements + listOf(
            listOf("alter table T_DEPARTMENT add column location varchar(128) default ''"),
            listOf("create table personnel (id integer not null primary key autoincrement, name varchar(128))")
        )
    }

    init {
    }

    @Before
    fun setup() {
        unmockkAll()
        MockScheduler.reset()
        MockBackgroundServiceProvider
        FilePaths.init(mockk {
            every { databaseDirectory } returns File("data", "databases").also { it.mkdirs() }
        })
        File(FilePaths().databaseDirectory, DBNAME).delete()
    }

    @After
    fun tearDown() {
        unmockkAll()
    }


    object Departments1 : Table<Nothing>("t_department") {
        val id = int("id").primaryKey()
        val name = varchar("name")
    }

    interface Department : Entity<Department> {
        companion object : Entity.Factory<Department>()

        val id: Int
        val name: String
        val location: String
    }

    object Departments2 : Table<Department>("t_department") {
        val id = int("id").primaryKey().bindTo { it.id }
        val name = varchar("name").bindTo { it.name }
        val location = varchar("location").bindTo { it.location }
    }

    @Test
    fun testCreate() {
        val db1 = DatabaseV1()
        db1.waitForDb()
        db1.db.insert(Departments1)
        {
            set(it.name, "Head Office")
        }
        assertTrue(db1.db.sequenceOf(Departments1).all { it.name eq "Head Office" })
        val db3 = DatabaseV3()
        db3.waitForDb()
        db3.db.insert(Departments2) {

            set(it.name, "Brisbane")
            set(it.location, "Rocklea")
        }
        assertTrue(db3.db.sequenceOf(Departments2).all {
            (it.name eq "Head Office" and (it.location eq "")) or
                ((it.name eq "Brisbane") and (it.location eq "Rocklea"))
        })
        db3.close()
        val db4 = DatabaseV3()
        db4.waitForDb()
        db4.db.insert(Departments2) {

            set(it.name, "Cairns")
            set(it.location, "Edge Hill")
        }
        assertEquals(3, db4.db.sequenceOf(Departments2).count())
    }
}

fun CJDatabase.waitForDb() {
    val lock = CountDownLatch(1)
    scheduler.scheduleDirect { lock.countDown() }
    lock.await()
    testScheduler.triggerActions()
}
