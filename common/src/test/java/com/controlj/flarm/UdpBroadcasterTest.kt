package com.controlj.flarm

import com.controlj.nmea.UdpBroadcaster
import com.controlj.rx.observeBy
import io.reactivex.rxjava3.core.Observable
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class UdpBroadcasterTest {

    companion object {
        val data = listOf(
            "\$PFLAU,1,1,1,1,0,,0,,*62",
            "\$GPRMC,032500.00,A,2808.89912,S,15156.72301,E,0.047,175.28,190714,,,A*7F",
            "\$PGRMZ,1401,F,2*0E",
            "\$GPGGA,032500.00,2808.89912,S,15156.72301,E,1,08,1.91,468.0,M,35.3,M,,*43",
            "\$PFLAA,0,-51,-568,110,2,DDB6F0,293,,26,6.7,1*5B",
        )
    }

    @Test
    fun broadcast() {
        val latch = CountDownLatch(10)
        val broadcaster = UdpBroadcaster(address = "192.168.3.255")
        val disposable = Observable.interval(1, TimeUnit.SECONDS)
            .take(10)
            .flatMap {
                latch.countDown()
                Observable.fromIterable(data).map { it + "\r\n" }
            }
            .observeBy {
                broadcaster.broadcast(it.toByteArray())
            }
        latch.await(15, TimeUnit.SECONDS)
        disposable.dispose()
    }
}
