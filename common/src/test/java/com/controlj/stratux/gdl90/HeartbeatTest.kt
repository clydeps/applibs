package com.controlj.stratux.gdl90

import com.controlj.utility.asUInt
import org.junit.Assert.*
import org.junit.Test

class HeartbeatTest {
    @ExperimentalUnsignedTypes
    @Test
    fun heartbeatTest() {
        val beat1 = Heartbeat()
        assertFalse(beat1.utcOk)
        assertFalse(beat1.gpsPosValid)
        val beat2 = Heartbeat().fromBytes(byteArrayOf(
                0, 0x81.toByte(), 1, 0x80.toByte(), 0x70, 0x62, 100
        ))
        assertTrue(beat2.gpsPosValid)
        assertTrue(beat2.uatInitialized)
        assertEquals(28800, beat2.timeStamp)
        assertEquals(12, beat2.uplinkRecvd)
        assertEquals(612, beat2.totalRecvd)
        val bytes = beat2.databytes
        assertEquals(0, bytes[0].asUInt())
        assertEquals(0x81, bytes[1].asUInt())
        assertEquals(0x1, bytes[2].asUInt())
        assertEquals(0x80, bytes[3].asUInt())
        assertEquals(0x70, bytes[4].asUInt())
        assertEquals(0x62, bytes[5].asUInt())
        assertEquals(100, bytes[6].asUInt())
    }
}
