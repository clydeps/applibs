package com.controlj.stratux.gdl90

import com.controlj.traffic.TrafficTarget
import com.controlj.utility.Units
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import kotlin.math.roundToInt

class PositionReportTest {

    val bytes = listOf(
            0x14, 0x00, 0xAB, 0x45, 0x49, 0x1F, 0xEF, 0x15,
            0xA8, 0x89, 0x78, 0x0F, 0x09, 0xA9, 0x07, 0xB0,
            0x01, 0x20, 0x01, 0x4E, 0x38, 0x32, 0x35, 0x56,
            0x20, 0x20, 0x20, 0x00
    ).map { it.toByte() }
            .toByteArray()

    @ExperimentalUnsignedTypes
    @Test
    fun positionReportTest() {
        val report = PositionReport(20).fromBytes(bytes)
        assertEquals(20, report.type)
        assertEquals(44.907, report.latitude, .001)
        assertEquals(-122.995, report.longitude, .001)
        assertEquals(123, Units.Unit.KNOT.convertTo(report.speed).roundToInt())
        assertEquals(45, report.track.roundToInt())
        assertEquals(30, report.accuracyH.roundToInt())
        assertEquals(64, Units.Unit.FPM.convertTo(report.verticalSpeed).roundToInt())
        assertEquals(5000, Units.Unit.FOOT.convertTo(report.altitude).roundToInt())
        assertEquals(0xab4549, report.address)
        assertEquals("N825V", report.callSign.trim())
        assertEquals(TrafficTarget.EmitterCategory.Light, report.emitterCategory)
        assertTrue(report.isInFlight)
        val newBytes = report.databytes
        assertEquals(28, newBytes.size)
        newBytes.forEachIndexed { index, byte ->
            assertEquals("Byte $index doesn't match", bytes[index], byte)
        }
    }
}
