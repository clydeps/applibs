package com.controlj.stratux

import com.controlj.network.NetworkFactory
import com.controlj.network.UdpData
import com.controlj.stratux.gdl90.PositionReport
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkAll
import io.reactivex.rxjava3.core.Observable
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import java.io.File
import java.net.InetAddress
import java.nio.ByteBuffer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class StratuxTest {

    fun readData(): List<ByteArray> {
        val blocks = mutableListOf<ByteArray>()
        val file = File("data/stratux.data")
        val bytes = file.readBytes()
        val buffer = ByteBuffer.wrap(bytes)
        while (buffer.hasRemaining()) {
            val len = buffer.get().toInt()
            val arr = ByteArray(len)
            buffer.get(arr)
            blocks.add(arr)
        }
        return blocks
    }

    @Before
    fun setup() {
        unmockkAll()
        mockkObject(NetworkFactory)
        every { NetworkFactory.observeUdpBroadcast(Stratux.portSetting.value)} answers {
            Observable.fromIterable(readData()).map {
                UdpData(InetAddress.getLocalHost(), it)
            }
        }
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @ExperimentalUnsignedTypes
    @Test
    fun getObserver() {
        var last : PositionReport? = null
        val latch = CountDownLatch(1)
        Stratux.observer
                .filter { it is PositionReport }
                .subscribe({
                    //println("Received ${it}")
                    last = it as PositionReport
                }, {
                    fail(it.toString())
                }, {
                    latch.countDown()
                })
        latch.await(5, TimeUnit.SECONDS)
        assertEquals(last?.callSign, "EJA526  ")
    }
}
