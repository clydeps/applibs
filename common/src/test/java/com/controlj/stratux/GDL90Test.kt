/*
 * Copyright (c) 2021.  Control-J Pty Ltd
 * All rights reserved
 */

package com.controlj.stratux

import com.controlj.location.LocationSource
import com.controlj.nmea.FlarmGenerator
import com.controlj.nmea.GpsFix
import com.controlj.nmea.UdpBroadcaster
import com.controlj.rx.MainScheduler
import com.controlj.rx.observeBy
import com.controlj.rx.observeOnMainBy
import com.controlj.settings.MapDataStore
import com.controlj.stratux.Stratux.decode
import com.controlj.stratux.Stratux.encode
import com.controlj.stratux.gdl90.ForeFlight
import com.controlj.stratux.gdl90.GDL90
import com.controlj.stratux.gdl90.GDL90.Companion.toGdl90
import com.controlj.stratux.gdl90.GDL90TrafficReport.Companion.asGDL90Report
import com.controlj.stratux.gdl90.Heartbeat
import com.controlj.stratux.gdl90.OwnshipReport
import com.controlj.stratux.gdl90.StratuxHeartbeat
import com.controlj.stratux.gdl90.StratuxStatus
import com.controlj.stratux.sim.LocationProviderSimulation
import com.controlj.stratux.sim.TrafficProviderSimulation
import com.controlj.traffic.TrafficSettings
import com.controlj.traffic.TrafficSource
import com.controlj.traffic.TrafficTarget
import com.controlj.utility.Units
import com.controlj.utility.from
import com.controlj.utility.instantNow
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import java.net.InetAddress
import java.time.LocalTime
import java.time.ZoneOffset
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class GDL90Test {

    @Before
    fun setup() {
        MapDataStore(true)
        MainScheduler.instance = Schedulers.trampoline()
    }

    // this is meant to be run manually only.
    @Ignore("Run manually only")
    @Test
    fun gdl90Test() {
        val broadcaster = UdpBroadcaster(address = "192.168.3.255")
        val latch = CountDownLatch(1)
        TrafficSource.add(StratuxTraffic)
        LocationSource.add(Stratux)
        val disposable = FlarmGenerator.observer
            .observeBy(
                {
                    fail(it.message)
                },
                {
                    latch.countDown()
                }
            ) {
                if (it.isNotBlank()) {
                    print(it)
                    broadcaster.broadcast(it.toByteArray())
                }
            }
        val disposable2 = Stratux.observer.observeOnMainBy {
            println("Received $it")
        }
        latch.await(6000L, TimeUnit.SECONDS)
        TrafficSource.remove(StratuxTraffic)
        LocationSource.remove(Stratux)
        disposable.dispose()
        disposable2.dispose()
    }

    //@Ignore("Run manually only")
    @Test
    fun gdl90Broadcast() {
        TrafficSettings.distanceFilter.value = 25000.0
        TrafficSettings.altitudeFilter.value = 5000.0
        LocationSource.add(LocationProviderSimulation)
        TrafficSource.add(TrafficProviderSimulation)
        val broadcaster = UdpBroadcaster(4000, "255.255.255.255")
        val heartbeat = Heartbeat(InetAddress.getLocalHost()).apply {
            utcOk = true
            gpsPosValid = true
        }
        val ownshipReport = OwnshipReport().apply {
            emitterCategory = TrafficTarget.EmitterCategory.Light
            address = TrafficTarget.EmitterAddress(TrafficTarget.AddressType.AdsbSelf, 0x7C0000)
            creator = LocationProviderSimulation.creator
            isInFlight = true
        }
        val status = StratuxStatus(null).apply {
            gpsFix = GpsFix.ThreeD
            pressureValid = true
            enable1090 = true
            enableGps = true
            radioCount = 1
            satellitesLocked = 7
            satellitesTracked = 10
            num1090Targets = TrafficProviderSimulation.maxTargets
            rate1090Targets = num1090Targets
            cpuTemp = 39.0
        }
        val ffId = ForeFlight.IDMsg(null).apply {
            serialNumber = 1234UL
            deviceName = "Simul"
            longDeviceName = "Stratux Simulator"
        }
        val stratuxHeartbeat = StratuxHeartbeat(null).apply {
            gpsEnabled = true
            protocolVersion = StratuxStatus.VERSION
        }
        Observable.interval(1, TimeUnit.SECONDS).blockingSubscribe {
            heartbeat.timeStamp = LocalTime.now(ZoneOffset.UTC).toSecondOfDay()
            broadcaster.broadcast(heartbeat.encode())
            ownshipReport.apply(LocationSource.lastLocation)
            broadcaster.broadcast(ownshipReport.encode())
            val targets = TrafficSource.currentTargets
            status.num1090Targets = targets.size
            status.rate1090Targets = status.num1090Targets
            broadcaster.broadcast(status.encode())
            broadcaster.broadcast(ffId.encode())
            broadcaster.broadcast(stratuxHeartbeat.encode())
            targets.forEach {
                broadcaster.broadcast(it.asGDL90Report().encode())
            }
        }
    }

    @Test
    fun conversions() {
        val heartbeat = Heartbeat(InetAddress.getLocalHost()).apply {
            utcOk = true
            gpsPosValid = true
        }
        var decoded = toGdl90(decode(heartbeat.encode()).first(), null)
        assertEquals(heartbeat, decoded)
        val stratuxStatus = StratuxStatus(null).apply {
            gpsFix = GpsFix.ThreeD
            pressureValid = true
            enable1090 = true
            enableGps = true
            radioCount = 1
            satellitesLocked = 7
            satellitesTracked = 10
            num1090Targets = 6
            rate1090Targets = 8
            cpuTemp = 39.0
            cpuTempValid = true
        }
        decoded = toGdl90(decode(stratuxStatus.encode()).first(), null)
        assertEquals(stratuxStatus, decoded)

        val ffId = ForeFlight.IDMsg(null).apply {
            deviceName = "Short"
            longDeviceName = "Longer name"
            serialNumber = 123456789UL
        }
        decoded = toGdl90(decode(ffId.encode()).first(), null)
        assertEquals(ffId, decoded)

        val alt = 2375.0.from(Units.Unit.FOOT)      // must be a multiple of 25 ft
        val spd = 120.0.from(Units.Unit.KNOT)
        val ownshipReport = OwnshipReport(null).apply {
            latitude = 23.67
            longitude = 120.123
            altitude = alt
            speed = spd
            track = 90.0
            valid = true
            timestamp = instantNow
            creator = GDL90.creator
            isInFlight = true
            address = TrafficTarget.EmitterAddress(TrafficTarget.AddressType.AdsbIcao, 0x7C0000)
            verticalSpeed = -1280.0.from(Units.Unit.FPM)
            emitterCategory = TrafficTarget.EmitterCategory.Large
            callSign = "CALLSI"
            priorityCode = 0
            isHeading = true                     // if false, "track" is heading
            isExtrapolated = false
            isHazard = false
            nic = 10                            // navigation integrity category
            nacP = 9                           // navigation accuracy
        }
        decoded = toGdl90(decode(ownshipReport.encode()).first(), null)
        assertEquals(ownshipReport, decoded)
        decoded as OwnshipReport
        assertEquals(23.67, decoded.latitude, .001)
        assertEquals(120.123, decoded.longitude, .001)
        assertEquals(alt, decoded.altitude, .001)
        assertEquals(spd, decoded.speed, .001)
        assertEquals(90.0, decoded.track, .001)
        assertEquals(true, decoded.valid)
        //assertEquals(ownshipReport.timestamp, decoded.timestamp)  // not expected to be the same
        assertEquals(GDL90.creator, decoded.creator)
        assertEquals(true, decoded.isInFlight)
        assertEquals(TrafficTarget.EmitterAddress(TrafficTarget.AddressType.AdsbIcao, 0x7C0000), decoded.address)
        assertEquals(ownshipReport.verticalSpeed, decoded.verticalSpeed, .001)
        assertEquals(TrafficTarget.EmitterCategory.Large, decoded.emitterCategory)
        assertEquals("CALLSI", decoded.callSign)
        assertEquals(0, decoded.priorityCode)
        assertEquals(true, decoded.isHeading)                     // if false, "track" is heading
        assertEquals(false, decoded.isExtrapolated)
        assertEquals(false, decoded.isHazard)
        assertEquals(10, decoded.nic)                            // navigation integrity category
        assertEquals(9, decoded.nacP)                           // navigation accuracy

    }
}
