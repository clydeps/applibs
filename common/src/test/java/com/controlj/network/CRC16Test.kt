package com.controlj.network

import org.junit.Assert.*
import org.junit.Test

class CRC16Test {

    @Test
    fun crcTest() {
        val bytes = byteArrayOf(0x00, 0x81.toByte(), 0x41, 0xdb.toByte(), 0xd0.toByte(), 0x08, 0x02 )
        val crc = bytes.crc16
        assertEquals(0x8bb3, crc)
    }
}
