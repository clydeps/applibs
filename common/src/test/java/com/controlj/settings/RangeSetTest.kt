package com.controlj.settings

import com.controlj.data.Constants.INVALID_DATA
import com.controlj.data.Priority
import com.controlj.graphics.CColor
import com.controlj.utility.Units
import io.mockk.every
import io.mockk.mockkObject
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 2019-06-21
 * Time: 11:33
 */
class RangeSetTest {

    val egtRanges = RangeSet.from(
            Units.Unit.FARENHEIT,
            "egt",
            1000.0,
            Range(1000.0, RangeState.Neutral),
            Range(1600.0, RangeState.Normal),
            Range(1650.0, RangeState.CautionHigh),
            Range(1800.0, RangeState.AlarmHigh)
    )

    val egtUnitSetting = UnitSetting("egt", "EGT", "Exhaust Gas Temperature unit",
            listOf(Units.Unit.FARENHEIT, Units.Unit.CELSIUS, Units.Unit.KELVIN))

    @Before
    fun before() {
        mockkObject(Setting)
        every { Setting.get<UnitSetting>("egt")}  returns egtUnitSetting
    }

    @Test
    fun testRanges() {
        val unit = Units.Unit.FARENHEIT
        assertEquals(unit.convertFrom(1000.0), egtRanges.baseScaleStart, .01)
        assertEquals(1000.0, egtRanges.scaleStart, .01)
        assertEquals(1800.0, egtRanges.scaleEnd, .01)
        assertEquals(800.0, egtRanges.scaleSpan, .01)
        assertEquals(200.0, egtRanges.calculateStepSize(), .01)
        assertEquals(Priority.NONE.backgroundColor, egtRanges.colorForValue(unit.convertFrom(800.0)))
        assertEquals(CColor.NORMAL, egtRanges.colorForValue(unit.convertFrom(1001.0)))
        assertEquals(CColor.CAUTION, egtRanges.colorForValue(unit.convertFrom(1625.0)))
        assertEquals(CColor.ALARM, egtRanges.colorForValue(unit.convertFrom(1801.0)))
        assertEquals(unit.convertFrom(1000.0), egtRanges.fromCurrent(1000.0), .01)
        assertEquals(CColor.NORMAL, egtRanges.getColor(1))
        assertEquals(CColor.CAUTION, egtRanges.getColor(2))
        assertEquals(CColor.ALARM, egtRanges.getColor(3))
        assertEquals(1600.0, egtRanges.getValue(1), .01)
        assertEquals(1650.0, egtRanges.getValue(2), .01)
        assertEquals(1800.0, egtRanges.getValue(3), .01)
        assertEquals("egt", egtRanges.key)
        assertEquals(unit.convertFrom(1600.0), egtRanges.nominal, .01)
        assertEquals(egtUnitSetting, egtRanges.setting)
        assertEquals(4, egtRanges.size())
        assertEquals(1000.0, egtRanges.toCurrent(unit.convertFrom(1000.0)), .01)
        egtUnitSetting.value = Units.Unit.CELSIUS
        assertEquals(unit.convertFrom(1000.0), egtRanges.scaleStart, .01)
        assertFalse(egtRanges.valid(INVALID_DATA))
        assertFalse(egtRanges.valid(-2000.0))
        assertTrue(egtRanges.valid(0.0))
        assertTrue(egtRanges.valid(1500.0))
        assertFalse(egtRanges.valid(2000.0))

    }

}
