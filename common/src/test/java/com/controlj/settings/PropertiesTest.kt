package com.controlj.settings

import com.controlj.framework.FilePaths
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 2019-06-07
 * Time: 10:39
 */
class PropertiesTest {

    @Before
    fun before() {
        unmockkAll()
        val delegate = mockk<FilePaths>("Appdelegate")
        FilePaths.instance = delegate
        every { delegate.openAsset("properties.json") } answers {
            """
                {
                  "property1": "1234",
                  "property2": "5678"
                }
            """.trimIndent().byteInputStream()
        }
        every { delegate.openAsset("local_properties.json") } answers {
            """
                {
                  "property3": "abcd",
                  "property4": "defg"
                }
            """.trimIndent().byteInputStream()
        }
        Properties.reset()
    }

    @After
    fun after() {
        unmockkAll()
    }

    @Test
    fun testProperties() {
        assertEquals("1234", Properties.getProperty("property1"))
        assertEquals("5678", Properties.getProperty("property2"))
        assertEquals("abcd", Properties.getProperty("property3"))
        assertEquals("defg", Properties.getProperty("property4"))
        assertEquals("", Properties.getProperty("property5"))
    }
}
