package com.controlj.settings

import com.controlj.utility.MockScheduler
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class SettingTest {

    @Before
    fun setup() {
        MockScheduler
    }

    @Test
    fun toStringTest() {
        MapDataStore(true)      // provide default datastore

        val setting1 = BooleanSetting("setting1", "Setting 1")
        val setting2 = StringSetting("setting2", "Setting 2")
        Setting.Group("Group", setting1, setting2)
        setting1.value = true

        val str = Setting.asString()
        assertEquals(
            "Ungrouped:\n" +
                "    secretSauce=false\n" +
                "Group:\n" +
                "    setting2=\n" +
                "    setting1=true", str
        )
    }
}
