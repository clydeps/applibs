package com.controlj.test

import com.controlj.logging.WatchDog
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import io.mockk.unmockkAll
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by clyde on 29/6/18.
 */
class WatchDogTest {

    @Before
    fun setup() {
        unmockkAll()
        MockScheduler.reset()
    }
    @Test
    fun watchDogTest() {
        val atomic = AtomicInteger(1)
        val ioScheduler = TestScheduler()
        RxJavaPlugins.setIoSchedulerHandler { ioScheduler }
        RxJavaPlugins.setComputationSchedulerHandler { ioScheduler }
        testScheduler.triggerActions()
        val dog = WatchDog(testScheduler, 100, TimeUnit.MILLISECONDS) {
            System.out.println("Timer expired, delivered on thread ${Thread.currentThread().name}")
            assertTrue(it.contains("advanceTimeBy"))
            assertEquals(2, atomic.incrementAndGet())
        }
        dog.start()
        ioScheduler.advanceTimeBy(60, TimeUnit.MILLISECONDS)
        testScheduler.advanceTimeBy(50, TimeUnit.MILLISECONDS)
        ioScheduler.advanceTimeBy(60, TimeUnit.MILLISECONDS)
        assertEquals(1, atomic.get())
        ioScheduler.advanceTimeBy(50, TimeUnit.MILLISECONDS)
        testScheduler.triggerActions()
        assertEquals(2, atomic.get())
    }
}
