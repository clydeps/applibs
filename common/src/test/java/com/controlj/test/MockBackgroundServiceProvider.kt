package com.controlj.test

import com.controlj.data.Progress
import com.controlj.framework.BackgroundServiceProvider
import com.controlj.framework.TaskStatus
import com.controlj.utility.FormRequest
import com.controlj.view.AppPermission
import io.reactivex.rxjava3.core.ObservableEmitter
import io.reactivex.rxjava3.core.Single
import org.threeten.bp.Instant

object MockBackgroundServiceProvider: BackgroundServiceProvider {

    val requests = mutableMapOf<String, FormRequest>()
    override fun isPermitted(appPermission: AppPermission): Single<Boolean> {
        TODO("Not yet implemented")
    }

    override fun scheduleProcessingTask(
        action: String,
        atTime: Instant,
        requiresNetwork: Boolean,
        longRunning: Boolean
    ): Boolean {
        TODO("Not yet implemented")
    }

    override fun cancelProcessingTask(action: String) {
        TODO("Not yet implemented")
    }

    override fun getStatus(identifier: String): Single<TaskStatus> {
        return Single.just(TaskStatus.Completed)
    }

    override fun postRequest(postData: FormRequest, identifier: String, emitter: ObservableEmitter<Progress>?) {
        requests[identifier] = postData
        emitter?.onComplete()
    }

    override val batteryLevel: Double
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.

    init {
        BackgroundServiceProvider.instance = this
    }
}
