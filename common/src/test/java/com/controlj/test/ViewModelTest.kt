package com.controlj.test

import com.controlj.settings.MapDataStore
import com.controlj.viewmodel.ViewModel
import com.google.gson.annotations.Expose
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.schedulers.CJTestScheduler
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.File

/**
 * Created by clyde on 27/8/18.
 */
class ViewModelTest {

    val datastore = MapDataStore(true)

    var testScheduler = CJTestScheduler()

    init {
        RxJavaPlugins.setComputationSchedulerHandler { testScheduler }
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
    }


    class TestModel : ViewModel {

        @Expose
        val intData = ViewModel.IntData()
        @Expose
        val stringData = ViewModel.StringData()
        @Expose
        val hiddenData = ViewModel.Obfuscated()
        // not exposed
        var transientData: Int = 230
    }

    @Test
    fun testViewModel() {
        val testModel = TestModel()

        assertEquals(0, testModel.intData.data)
        assertEquals("", testModel.stringData.data)
        assertEquals("", testModel.hiddenData.data)

        testModel.intData.data = 1000
        testModel.stringData.data = "test data"
        testModel.hiddenData.data = "obfuscated data"
        assertEquals(1000, testModel.intData.data)
        assertEquals("test data", testModel.stringData.data)
        assertEquals("obfuscated data", testModel.hiddenData.data)

        var tested = false
        var disposable = testModel.stringData.listener
                .subscribeOn(testScheduler)
                .subscribe {
                    assertEquals("test data", it)
                    tested = true
                }
        testScheduler.triggerActions()
        assertTrue(tested)
        disposable.dispose()
        var value = -1
        disposable = testModel.intData.listener
                .subscribeOn(testScheduler)
                .subscribe {
                    value = it
                }
        testScheduler.triggerActions()
        assertEquals(value, 1000)
        testModel.intData.data = 50
        testModel.transientData = 99
        testScheduler.triggerActions()
        assertEquals(value, 50)
        disposable.dispose()
        testModel.save(datastore)
        testModel.onDestroy()
        assertEquals(datastore.allEntries.values.size, 1)
        datastore.allEntries.values.forEach { assertFalse(it.toString().contains("obfuscated", true)) }

        val newModel = ViewModel.load(datastore, TestModel::class.java)
        assertEquals(50, newModel.intData.data)
        assertEquals(230, newModel.transientData)
        assertEquals(newModel.stringData.data, "test data")
        assertEquals(newModel.hiddenData.data, "obfuscated data")
    }

    class Unexposed: ViewModel.Exposed {
        var field1: Int = 0
        var field2: Int = 100
        val list: MutableList<String> = mutableListOf()
    }

    @Test
    fun testUnExposed() {
        val unex = Unexposed()
        unex.field1 = 2000
        unex.field2 = 3000
        unex.list.add("string")
        val file = File("data/test")
        unex.save(file)
        val newex = ViewModel.load<Unexposed>(file)
        assertEquals(2000, newex.field1)
        assertEquals(3000, newex.field2)
        assertEquals(1, newex.list.size)
        assertTrue(unex.list.contains("string"))
        file.delete()
    }
}
