package com.controlj.test

import com.controlj.GIS.riemann.GeomagneticField
import org.junit.Assert.assertTrue
import org.junit.Test
import kotlin.math.abs
import kotlin.math.roundToLong

/**
 * Created by clyde on 27/6/18.
 */
class GeomagneticTest {

    data class TestData(val altitude: Float, val latitude: Float, val longitude: Float, val result: Float, val year: Float = 2015f)
    companion object {
        val data: Array<TestData> = arrayOf(
                TestData(0f,80f,0f,-3.85f,2015.0f),
                TestData(0f,0f,120f,0.57f,2015.0f),
                TestData(0f,-80f,240f,69.81f,2015.0f),
                TestData(100f,80f,0f,-4.27f,2015.0f),
                TestData(100f,0f,120f,0.56f,2015.0f),
                TestData(100f,-80f,240f,69.22f,2015.0f),
                TestData(0f,0f,120f,0.32f,2017.5f),
                TestData(0f,80f,0f,-2.75f,2017.5f),
                TestData(0f,-80f,240f,69.58f,2017.5f),
                TestData(100f,80f,0f,-3.17f,2017.5f),
                TestData(100f,0f,120f,0.32f,2017.5f),
                TestData(100f,-80f,240f,69.00f,2017.5f)
        )
    }

    @Test
    fun geoMagneticTest() {
        data.forEach {
            val declination = GeomagneticField(it.latitude, it.longitude, it.altitude*1000, ((it.year - 1970) * 31557600000L).roundToLong()).declination
            println("$it = $declination")
            assertTrue(abs((declination - it.result) ) < .05f)
        }

    }
}
