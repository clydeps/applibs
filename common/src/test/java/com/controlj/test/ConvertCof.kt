package com.controlj.test

import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import java.io.BufferedReader
import java.io.File
import java.io.FileNotFoundException

/**
 * Created by clyde on 27/6/18.
 */
class ConvertCof {

    companion object {
        const val ROWS = 13
    }

    private fun writeData(name: String, data: Array<FloatArray>): String {
        return (0 until ROWS).map { row ->
            (0..row).map { col -> data[row][col].toString() + "f" }.joinToString(",", "    floatArrayOf(", ")")
        }.joinToString(",\n", "val $name = arrayOf(\n", ")\n")
    }

    @Test
    fun convert() {
        val reader: BufferedReader
        try {
            reader = File("data/WMM2015.COF").inputStream().bufferedReader()
        } catch(e: FileNotFoundException) {
            println("did not find file WMM2015.COF, current directory = ${File(".").absolutePath}")
           fail("File not found")
            return
        }
        val header = reader.readLine()
        assertEquals("    2015.0            WMM-2015        12/15/2014", header)

        val g_coeff = Array(ROWS) { FloatArray(ROWS) { 0f } }
        val g_delta = Array(ROWS) { FloatArray(ROWS) { 0f } }
        val h_coeff = Array(ROWS) { FloatArray(ROWS) { 0f } }
        val h_delta = Array(ROWS) { FloatArray(ROWS) { 0f } }

        (1 until ROWS).forEach { index ->
            (0..index).forEach { colidx ->
                val line = reader.readLine()
                assertEquals(48, line.length)
                val row = line.substring(0, 3).trim().toInt()
                assertEquals(index, row)
                val col = line.substring(3, 6).trim().toInt()
                assertEquals(colidx, col)
                g_coeff[row][col] = line.substring(6, 16).trim().toFloat()
                h_coeff[row][col] = line.substring(16, 26).trim().toFloat()
                g_delta[row][col] = line.substring(26, 37).trim().toFloat()
                h_delta[row][col] = line.substring(37, 48).trim().toFloat()
            }
        }
        assertEquals("999999999999999999999999999999999999999999999999", reader.readLine())
        assertEquals("999999999999999999999999999999999999999999999999", reader.readLine())
        reader.close()

        val writer = File("output.txt").outputStream().bufferedWriter()
        writer.write(writeData("G_COEFF", g_coeff))
        writer.write(writeData("H_COEFF", h_coeff))
        writer.write(writeData("G_DELTA", g_delta))
        writer.write(writeData("H_DELTA", h_delta))
        writer.close()
    }

}
