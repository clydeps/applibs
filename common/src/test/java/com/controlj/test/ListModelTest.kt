package com.controlj.test

import com.controlj.settings.MapDataStore
import com.controlj.utility.DiffUtil
import com.controlj.utility.ListUpdateCallback
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import com.controlj.view.ListModel
import com.controlj.view.ListPresenter
import com.controlj.viewmodel.ViewModel
import io.mockk.unmockkAll
import io.reactivex.rxjava3.core.Observable
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test

/**
 * Created by clyde on 28/8/18.
 */
class ListModelTest {
    private val datastore = MapDataStore(true)


    @Before
    fun setup() {
        unmockkAll()
        MockScheduler.reset()
    }

    class TestItem(val key: String, var text: String, override var isChecked: Boolean = false) : Comparable<Any>, ListPresenter.Checkable {


        override fun compareTo(other: Any): Int {
            if (other is TestItem)
                return key.compareTo(other.key)
            return -1
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as TestItem

            if (key != other.key) return false
            if (text != other.text) return false
            if (isChecked != other.isChecked) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + text.hashCode()
            result = 31 * result + isChecked.hashCode()
            return result
        }

    }

    companion object {

        var startIndex = 1
    }

    class TestModel(dataStore: MapDataStore) : ListPresenter<TestItem>() {
        override fun textValues(item: TestItem): List<String> {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        init {
            addSection("section1", Observable.fromIterable(startIndex..11).map { TestItem("key$it", "data $it") })
            addSection("section2", Observable.fromIterable(21..26).map { TestItem("key$it", "data $it") })
        }

        override val comparator: Comparator<TestItem> = Comparator { _, _ -> 0 }
        override val checkable: Boolean = true
        override val keySelector = TestItem::key
        override val model = ViewModel.load(dataStore, ListModel::class.java)

        /**
         * Ensure that all observers are closed (throwing errors if any were not already closed.)
         * Should be called when this ViewModel's context is being destroyed.
         */
    }

    @Test
    fun testDiffUtil() {
        val callback = object : DiffUtil.Callback() {
            override fun getOldListSize(): Int = 10
            override fun getNewListSize(): Int = 10

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                //println("Comparing $oldItemPosition with $newItemPosition")
                if (newItemPosition == 0 || newItemPosition == 6) return false
                return newItemPosition == oldItemPosition
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                if (newItemPosition == 8)
                    return false
                return newItemPosition == oldItemPosition
            }
        }
        val diffs = DiffUtil.calculateDiff(callback)
        diffs.dispatchUpdatesTo(object : ListUpdateCallback {
            override fun onInserted(position: Int, count: Int) {
                //println("on inserted $position, count $count")
            }

            override fun onRemoved(position: Int, count: Int) {
                //println("on removed $position, count $count")
            }

            override fun onMoved(fromPosition: Int, toPosition: Int) {
                fail("onMoved")
            }

            override fun onChanged(position: Int, count: Int, payload: Any?) {
                //println("onChanged $position, count $count")
            }

        })

    }

    @Test
    fun listViewModelTest() {
        val model = TestModel(datastore)
        model.onStart()
        testScheduler.triggerActions()
        assertEquals(17, model.allItems.size)
        assertEquals(19, model.totalRows)
        assertEquals(null, model.selected)
        model.setCollapsed(0, true)
        assertEquals(8, model.totalRows)
        val item = model.itemAt(ListPresenter.RowIndex(0, 4))
        assertEquals("key4", item?.key)
        model.selected = model.itemAt(ListPresenter.RowIndex(1, 3))
        model.setChecked(model.itemAt(ListPresenter.RowIndex(0, 2)), true)
        model.setChecked(model.itemAt(ListPresenter.RowIndex(0, 5)), true)
        model.setChecked(model.itemAt(ListPresenter.RowIndex(0, 6)), false)
        model.setChecked(model.itemAt(ListPresenter.RowIndex(1, 3)), true)
        model.setChecked(model.itemAt(ListPresenter.RowIndex(1, 4)), true)
        assertEquals(4, model.checkedItems.size)
        assertEquals("key23", model.selected?.key)
        model.model.save(datastore)
        //println("datastore contains ${datastore.map.values.first()}")
        val newModel = TestModel(datastore)
        startIndex = 3
        newModel.onStart()
        testScheduler.triggerActions()
        assertEquals("key23", newModel.selected?.key)
        assertEquals(8, newModel.totalRows)
        var testObserver = newModel.changeObserver.observeOn(testScheduler).test()
        testScheduler.triggerActions()
        assertEquals(listOf(
                ListPresenter.ChangeReason.STARTED,
                ListPresenter.ChangeReason.ENDED,
                ListPresenter.ChangeReason.CHANGED
        ), testObserver.values().map { (it as? ListPresenter.ChangeRange)?.reason })
        testObserver.dispose()

        testObserver = newModel.changeObserver.observeOn(testScheduler).test()
        testScheduler.triggerActions()
        newModel.selected = null
        newModel.setCollapsed(0, false)
        testScheduler.triggerActions()
        assertEquals(19, newModel.totalRows)
        assertEquals(listOf(
                ListPresenter.ChangeReason.STARTED,
                ListPresenter.ChangeReason.ENDED,
                ListPresenter.ChangeReason.CHANGED,
                ListPresenter.ChangeReason.CHANGED,
                ListPresenter.ChangeReason.ADDED,
                ListPresenter.ChangeReason.CHANGED
        ), testObserver.values().map { (it as? ListPresenter.ChangeRange)?.reason })
        assertEquals(ListPresenter.ChangeRange(ListPresenter.ChangeReason.ADDED, 1, 11), testObserver.values()[4])
        assertEquals(ListPresenter.ChangeRange(ListPresenter.ChangeReason.CHANGED, 0, 1), testObserver.values().last())
        testObserver.dispose()
    }
}
