package com.controlj.test

import com.controlj.data.DiskFileEntry
import com.controlj.logging.CrashLogger
import com.controlj.settings.Properties
import com.controlj.settings.Setting
import com.controlj.settings.StringSetting
import com.controlj.utility.Hash
import com.controlj.utility.MockDeviceInformation
import com.controlj.utility.MockFilePaths
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkAll
import okhttp3.MultipartBody
import okhttp3.Request
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Created by clyde on 2/7/18.
 */
class LoggingTests {

    var request: Request? = null
    var parts = listOf<MultipartBody.Part>()
    val dummyUrl = "https://crashlogger.example.com"


    @Before
    fun setup() {
        unmockkAll()
        MockScheduler.reset()
        MockDeviceInformation
        MockFilePaths
        MockBackgroundServiceProvider
        mockkObject(Properties)
        every { Properties.getProperty("crashlogger.secret", any()) } returns "dummy key"
        every { Properties.getProperty("crashlogger.url", any()) } returns dummyUrl
        val crashLogger = object : CrashLogger {}
        crashLogger.initialise(mapOf())
        MockBackgroundServiceProvider.requests.clear()
    }

    @After
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun sendReportTest() {
        val setting = StringSetting("setting1", "Setting 1")
        setting.value = "aValue"
        val params = mapOf(
            "MESSAGE" to "file upload",
            "EMAIL" to "email@address.com",
            "STACK_TRACE" to "Debug Report",
            "PACKAGE_NAME" to MockDeviceInformation.appName,
            "APP_VERSION_CODE" to MockDeviceInformation.appVersion,
            "BUILD" to MockDeviceInformation.appBuild,
            "DEVICE_NAME" to MockDeviceInformation.deviceName,
            "SETTINGS" to Setting.asString()

        )
        val file0 = File("data/logs/app.log")
        val file1 = File("data/logs/app1.log")
        val file2 = File("data/logs/appx.log")
        file2.writeText("Some text to occupy the file\n")
        assertTrue(file0.exists())
        assertTrue(file1.exists())
        assertTrue(file2.exists())

        val lengths = listOf(file0, file1, file2).map { it.name to it.length() }.toMap()

        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        CrashLogger.sendReport(
            "file upload",
            "email@address.com",
            listOf(file0, file1, file2).map { DiskFileEntry(it, "") })
        file2.appendText("Some more text in the file\n")
        testScheduler.triggerActions()
        //assertTrue(body.contains("Content-Disposition: form-data; name=\"app.log\"; filename=\"app.log\""))
        //assertTrue(body.contains("Content-Disposition: form-data; name=\"app1.log\"; filename=\"app1.log\""))
        assertEquals(1, MockBackgroundServiceProvider.requests.values.size)
        val report = MockBackgroundServiceProvider.requests.values.first()
        assertEquals(dummyUrl, report.url.toString())
        report.data.parameters.all { !params.containsKey(it.name) || params[it.name] == it.value }
        val custom = report.data.parameters.first { it.name == "CUSTOM_DATA" }.value.split('\n')
        assertEquals(2, custom.size)
        val stamp = custom[0].split('=')
        assertEquals(2, stamp.size)
        val hash = custom[1].split('=')
        assertEquals(2, hash.size)
        assertTrue(Hash.checkHash("dummy key", stamp[1], hash[1]))
        val files = report.data.files
        assertEquals(3, files.size)
        assertTrue(files.all { it.fileLength == lengths[it.name] })
    }
}
