package com.controlj.navdata

import com.controlj.data.Position
import com.controlj.data.navdata.NavPoint
import com.controlj.data.navdata.NavPointType
import com.controlj.database.CJDatabase
import com.controlj.framework.FilePaths
import com.controlj.framework.FileUpdater
import com.controlj.test.MockBackgroundServiceProvider
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.io.File

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 7/11/19
 * Time: 18:07
 */
class NavPointTest {
    companion object {
        const val basedir = "data"
        const val databasedir = "navdb"
    }

    @Before
    fun setup() {
        unmockkAll()
        MockScheduler.reset()
        MockBackgroundServiceProvider
        File(basedir, databasedir).deleteRecursively()
        File(basedir, databasedir).mkdirs()
        FilePaths.instance = mockk {
            every { databaseDirectory } returns File(basedir, databasedir)
            every { openAsset(any()) } answers {
                File(File(basedir, "assets"), firstArg()).inputStream()
            }
        }
        mockkObject(NavPoint)
        every { NavPoint.database } returns CJDatabase(NavPoint.DB_FILE_NAME) {
            FileUpdater.updateFileFromAssets(File(CJDatabase.directory, NavPoint.META_FILE_NAME))
        }
    }

    @After
    fun tearDown() {
        File(basedir, databasedir).deleteRecursively()
        unmockkAll()
    }


    @Test
    fun navPointTest() {
        val point = NavPoint.of("test", "test navpoint", 100.0, -27.0, 153.0, NavPointType.AIRPORT)
        val savedPoint = point.save().blockingGet()
        testScheduler.triggerActions()
        val newPoint = NavPoint.get(savedPoint.id).blockingGet()
        assertEquals(savedPoint.id, newPoint?.id)
        val found = NavPoint.find("test").firstOrError().blockingGet()
        assertEquals(savedPoint.id, found.id)
        val ybas = NavPoint.startsWith("YBA").toList().blockingGet()
        assertEquals(18, ybas.size)
        val pos = Position.of(-31.07444444, 152.76)
        val which = NavPoint.nearest(
                pos,
                1000.0
        ).blockingGet()
        assertEquals("YKMP", which?.ident)
        val distance = pos.distanceTo(which)
        assertTrue(distance < 1000.0)
    }
}
