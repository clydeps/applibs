package com.controlj.view

import com.controlj.data.Location
import com.controlj.data.Position
import com.controlj.location.LocationProvider
import com.controlj.location.LocationSource
import com.controlj.rx.DisposedEmitter
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import com.controlj.utility.instantNow
import com.controlj.viewmodel.Listenable
import io.mockk.every
import io.mockk.mockkStatic
import io.mockk.spyk
import io.mockk.unmockkAll
import io.mockk.verifySequence
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 2019-06-06
 * Time: 20:43
 */
class MapViewModelTest {

    @Before
    fun before() {
        unmockkAll()
        MockScheduler.reset()
        mockkStatic(Instant::class)
        every { instantNow } answers { Instant.ofEpochMilli(testScheduler.now(TimeUnit.MILLISECONDS)) }
        LocationSource.lastLocation = Location.invalid
        LocationSource.sources.clear()
        if (!LocationSource.emitter.isDisposed)
            LocationSource.emitter.onComplete()
    }

    @After
    fun after() {
        unmockkAll()
    }

    @Suppress("Unused_parameter")
    class Tester {
        fun center(position: Position, zoom: Double, animation: Duration) {}
        fun heading(direction: Double, animation: Duration) {}
        fun marker(location: Location, animation: Duration) {}
        fun menu(list: List<MenuEntry>) {}
        fun offset(left: Double, top: Double) {}
    }

    @Test
    fun testMapViewModel() {
        val model = object : MapViewModel() {
            override val style: String = "style://test"
        }
        //val dur500 = Duration.ofMillis(500)
        val tester = spyk<Tester>()
        val initialZoom = model.zoom
        assertEquals(17.0, model.maximumZoomLevel, 0.00001)
        assertEquals(4.0, model.minimumZoomLevel, 0.00001)

        val provider = object : LocationProvider {
            override val name: String = "Test"
            var emitter: ObservableEmitter<Location> = DisposedEmitter()
            override val locationObserver: Observable<Location> = Observable.create {
                emitter = it
            }
            override var priority: Int = Location.DEFAULT_PRIORITY
            override val enabled = Listenable(true)
            override val isConnected: Boolean = true
            override val authorisationStatus: LocationProvider.AuthorisationStatus = LocationProvider.AuthorisationStatus.WhileInUse

        }
        LocationSource.add(provider)
        val disposable = model.observable.subscribe { event ->
            when (event) {
                is MapViewModel.CenterEvent -> {
                    tester.heading(event.direction, event.animation)
                    tester.center(event.center, event.zoom, event.animation)
                }
                is MapViewModel.OffsetEvent -> tester.offset(event.left, event.top)
                is MapViewModel.MarkerPositionEvent -> tester.marker(event.location, event.animationTime)
                is MapViewModel.MenuEvent -> tester.menu(event.entries)
                else -> fail("Unknown event ${event::class.simpleName}")
            }
        }
        testScheduler.triggerActions()
        assertFalse(LocationSource.emitter.isDisposed)

        val creator = Location.Creator("test", 10)
        // send location, verify moving map center and location marker
        val location = Location.of(2.0, 3.0, 100.0, 4.0, 90.0, creator, accuracyH = 100.0, accuracyV = 100.0)
        val loc1 = Location.of(6.0, 8.0, 100.0, 4.0, 90.0, creator, accuracyH = 100.0, accuracyV = 100.0)
        val loc2 = Location.of(6.1, 8.1, 100.0, 4.0, 90.0, creator, accuracyH = 100.0, accuracyV = 100.0)
        provider.emitter.onNext(location)
        testScheduler.advanceTimeBy(500, TimeUnit.MILLISECONDS)
        provider.emitter.onNext(location)
        testScheduler.advanceTimeBy(400, TimeUnit.MILLISECONDS)
        provider.emitter.onNext(loc1)
        testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        provider.emitter.onNext(loc2)
        testScheduler.advanceTimeBy(300, TimeUnit.MILLISECONDS)
        model.onViewChanged(Position.of(6.5, 8.5), 7.0)
        testScheduler.triggerActions()
        assertEquals(7.0, model.zoom, .000001)
        provider.emitter.onNext(loc1)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        provider.emitter.onNext(loc2)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        provider.emitter.onNext(loc1)
        testScheduler.advanceTimeBy(10000, TimeUnit.MILLISECONDS)
        model.onViewChanged(Position.of(6.5, 8.5), 7.0)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        provider.emitter.onNext(loc1)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        provider.emitter.onNext(loc1)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        model.onLocationClick(null)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        model.trackingMode = MapViewModel.TrackingMode.Center
        model.resumeTracking()
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        model.trackingMode = MapViewModel.TrackingMode.NorthUp
        model.resumeTracking()
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)

        verifySequence {
            tester.offset(0.0, MapViewModel.CENTER_OFFSET)
            tester.heading(90.0, Duration.ZERO)
            tester.center(Position.from(location), initialZoom, Duration.ZERO)
            tester.marker(location, any())
            tester.offset(0.0, MapViewModel.CENTER_OFFSET)
            tester.heading(90.0, any())
            tester.center(Position.from(loc1), initialZoom, any())
            tester.marker(loc1, any())
            tester.offset(0.0, MapViewModel.CENTER_OFFSET)
            tester.heading(90.0, any())
            tester.center(Position.from(loc2), initialZoom, any())
            tester.marker(loc2, Duration.ofMillis(300))
            tester.marker(loc1, any())
            tester.marker(loc2, any())
            tester.marker(loc1, any())
            tester.offset(0.0, MapViewModel.CENTER_OFFSET)
            tester.heading(90.0, any())
            tester.center(Position.from(loc1), 7.0, any())
            tester.offset(0.0, MapViewModel.CENTER_OFFSET)
            tester.heading(90.0, any())
            tester.center(Position.from(loc1), 7.0, any())
            tester.offset(0.0, 0.0)
            tester.heading(90.0, any())
            tester.center(Position.from(loc1), 7.0, any())
            tester.offset(0.0, 0.0)
            tester.heading(00.0, any())
            tester.center(Position.from(loc1), 7.0, any())
        }

        disposable.dispose()
        assertTrue(LocationSource.emitter.isDisposed)
    }
}
