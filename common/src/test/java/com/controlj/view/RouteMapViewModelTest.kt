package com.controlj.view

import com.controlj.data.FlightPoint
import com.controlj.data.FlightRoute
import com.controlj.data.Location
import com.controlj.data.Position
import com.controlj.framework.FilePaths
import com.controlj.location.LocationProvider
import com.controlj.location.LocationSource
import com.controlj.route.RouteSource
import com.controlj.route.TrackSource
import com.controlj.rx.DisposedEmitter
import com.controlj.settings.LayerSettings
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import com.controlj.utility.Units
import com.controlj.view.RouteMapViewModel.Companion.zoomRoute
import com.controlj.viewmodel.Listenable
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.unmockkAll
import io.mockk.verify
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableEmitter
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import org.threeten.bp.Duration
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 2019-06-07
 * Time: 13:08
 */
class RouteMapViewModelTest {

    private var flightRoute = FlightRoute(
        "Demo", Location.DEMO_PRIORITY,
        listOf(
            FlightPoint.of("YDFD", -31.42166667, 152.75833333),
            FlightPoint.of("YKMP", -31.074444444444445, 152.78972222222225),
            FlightPoint.of("YGFN", -29.759444444444444, 153.03)
        )
    )

    @Suppress("Unused_parameter")
    class Tester {
        fun center(position: Position, zoom: Double, animation: Duration) {
            this.position = position
            this.zoom = zoom
        }

        fun heading(direction: Double, animation: Duration) {
            this.direction = direction
        }

        fun marker(location: Location, animation: Duration) = Unit
        fun offset(left: Double, top: Double) = Unit
        fun showRoute(route: FlightRoute<FlightPoint>) = Unit
        fun addLayer(layer: String) {
            layers[layer] = true
        }

        fun removeLayer(layer: String) {
            layers.remove(layer)
        }

        fun showLayer(layer: String, visible: Boolean) {
            assertTrue(layers.containsKey(layer))
            layers[layer] = visible
        }

        fun addSource(name: String) = sources.add(name)
        fun removeSource(name: String) = sources.remove(name)
        fun showLine(event: RouteMapViewModel.ShowLineEvent) = lines.put(event.key, event)

        val lines = mutableMapOf<TrackSource.TrackKey, RouteMapViewModel.ShowLineEvent>()
        val layers = mutableMapOf<String, Boolean>()
        val sources = mutableSetOf<String>()
        var position: Position = Position.invalid
        var direction: Double = -1.0
        var zoom: Double = -1.0
    }

    @Before
    fun before() {
        unmockkAll()
        MockScheduler.reset()
        LocationSource.lastLocation = Location.invalid
        LocationSource.sources.clear()
        if (!LocationSource.emitter.isDisposed)
            LocationSource.emitter.onComplete()
    }

    @Test
    fun testRouteMapViewModel() {
        val tester = spyk<Tester>()
        val delegate = mockk<FilePaths>("Appdelegate") {
            every { openAsset(any()) }.answers { File("data/assets", firstArg<String>()).inputStream() }
        }
        FilePaths.instance = delegate
        val viewModel = RouteMapViewModel()

        assertEquals(3, LayerSettings.layerMap["layer_airspace_e"]?.size)
        LayerSettings.layerMap.values.flatten().forEach { tester.addLayer(it) }
        val disposable = viewModel.observable.subscribe { event ->
            //println(event)
            when (event) {
                is MapViewModel.CenterEvent -> {
                    tester.center(event.center, event.zoom, event.animation)
                    tester.heading(event.direction, event.animation)
                }
                is MapViewModel.OffsetEvent -> tester.offset(event.left, event.top)
                is MapViewModel.MarkerPositionEvent -> tester.marker(event.location, event.animationTime)
                is RouteMapViewModel.ShowRouteEvent -> tester.showRoute(event.route)
                is RouteMapViewModel.RemoveLayerEvent -> tester.removeLayer(event.name)
                is RouteMapViewModel.AddRasterLayerEvent -> tester.addLayer(event.name)
                is RouteMapViewModel.AddFillLayerEvent -> tester.addLayer(event.name)
                is RouteMapViewModel.ShowLayerEvent -> tester.showLayer(event.layer, event.visible)
                is RouteMapViewModel.AddSourceEvent -> tester.addSource(event.name)
                is RouteMapViewModel.RemoveSourceEvent -> tester.removeSource(event.name)
                is RouteMapViewModel.ShowLineEvent -> tester.showLine(event)
                else -> fail("Unknown event ${event::class.simpleName}")
            }
        }
        testScheduler.triggerActions()
        assertTrue(LayerSettings.briefingAreas.value)
        assertTrue(LayerSettings.areaFrequencies.value)
        assertFalse(LayerSettings.airspaceB.value)
        verify { tester.showLayer("airspace B-us", false) }
        verify { tester.showLayer("area-frequencies", true) }
        RouteSource.send(flightRoute)
        testScheduler.triggerActions()
        verify { tester.showRoute(flightRoute) }
        flightRoute.active = 1
        RouteSource.send(flightRoute)
        testScheduler.triggerActions()
        verify { tester.showRoute(flightRoute) }

        viewModel.trackingMode = MapViewModel.TrackingMode.TrackUp
        viewModel.zoom = 5.0
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        val provider = object : LocationProvider {
            override val enabled = Listenable(true)
            override val isConnected: Boolean = true
            override val name: String = "Test"
            var emitter: ObservableEmitter<Location> = DisposedEmitter()
            override val locationObserver: Observable<Location> = Observable.create {
                emitter = it
            }
            override var priority: Int = Location.DEFAULT_PRIORITY
            override val authorisationStatus: LocationProvider.AuthorisationStatus =
                LocationProvider.AuthorisationStatus.WhileInUse

        }
        val creator = Location.Creator("test", 0)
        LocationSource.add(provider)
        testScheduler.triggerActions()
        provider.emitter.onNext(
            Location.of(
                flightRoute.points[0].latitude,
                flightRoute.points[0].longitude,
                altitude = 2000.0,
                speed = Units.Unit.KNOT.convertFrom(120.0),
                track = 20.0,
                accuracyH = 100.0,
                accuracyV = 100.0,
                creator = creator
            )
        )
        testScheduler.triggerActions()
        assertTrue(viewModel.trackPosition)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        assertEquals(flightRoute.points[0].latitude, tester.position.latitude, .0000001)
        assertEquals(5.0, tester.zoom, .00001)
        assertEquals(20.0, tester.direction, .00001)

        zoomRoute.value = RouteMapViewModel.RouteZoom.ACTIVE
        provider.emitter.onNext(viewModel.location)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        //assertEquals(10.0, tester.zoom, 0.5)      TODO - this is broken

        zoomRoute.value = RouteMapViewModel.RouteZoom.DESTINATION
        provider.emitter.onNext(viewModel.location)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        //assertEquals(8.0, tester.zoom, 0.5)      TODO - this is broken

        zoomRoute.value = RouteMapViewModel.RouteZoom.ACTIVE
        viewModel.trackingMode = MapViewModel.TrackingMode.Center
        provider.emitter.onNext(viewModel.location)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        //assertEquals(9.5, tester.zoom, 0.5)

        viewModel.trackingMode = MapViewModel.TrackingMode.NorthUp
        provider.emitter.onNext(viewModel.location)
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        //assertEquals(11.0, tester.zoom, 0.5)
        viewModel.trackingMode = MapViewModel.TrackingMode.TrackUp

        provider.emitter.onNext(
            Location.of(
                0.0,
                flightRoute.points[0].longitude,
                altitude = 2000.0,
                speed = Units.Unit.KNOT.convertFrom(120.0),
                track = 180.0,
                accuracyH = 100.0,
                accuracyV = 100.0,
                creator = creator
            )
        )

        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        assertEquals(viewModel.minimumZoomLevel, tester.zoom, 0.5)
        provider.emitter.onNext(
            Location.of(
                flightRoute.points[1].latitude,
                flightRoute.points[0].longitude,
                altitude = 2000.0,
                speed = Units.Unit.KNOT.convertFrom(120.0),
                track = 180.0,
                accuracyH = 100.0,
                accuracyV = 100.0,
                creator = creator
            )
        )
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        //assertEquals(viewModel.maximumZoomLevel, tester.zoom, 0.5)


        LayerSettings.airspaceC.value = true
        viewModel.onLayerClick()
        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        assertEquals(true, tester.layers["ctr c"])

        disposable.dispose()
    }
}
