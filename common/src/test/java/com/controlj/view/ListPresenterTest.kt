package com.controlj.view

import com.controlj.rx.MainScheduler
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import io.reactivex.rxjava3.core.Observable
import org.junit.Before
import org.junit.Test

class ListPresenterTest {

    @Before
    fun setup() {
        MockScheduler.reset()
    }

    @Test
    fun testRefresh() {
        val presenter = object : ListPresenter<Int>() {
            override val keySelector: (Int) -> String
                get() = { it.toString() }

            init {
                addSection("Section", Observable.just(1, 3, 4, 5))
                addSection("Section2", Observable.just(6,7,8,9))
            }
        }

        val disposable = presenter.changeObserver.subscribe {
            println("Event $it")
        }
        testScheduler.triggerActions()
        disposable.dispose()
    }
}
