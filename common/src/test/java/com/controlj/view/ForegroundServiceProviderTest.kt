package com.controlj.view

import com.controlj.framework.ApplicationState
import com.controlj.framework.SystemColors
import com.controlj.ui.DialogData
import com.controlj.ui.UiAction
import com.controlj.utility.MockScheduler
import io.mockk.every
import io.mockk.mockkObject
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.io.File
import java.util.concurrent.TimeUnit

class ForegroundServiceProviderTest {

    val shown = mutableListOf<DialogData>()

    val scheduler = MockScheduler.testScheduler
    val fsp = object : ForegroundServiceProvider() {
        init {
            init()
        }
        override var keepScreenOn: Boolean
            get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
            set(value) {}

        override fun showDialog(dialogData: DialogData) {
            TODO("not implemented")
        }

        override fun openUrl(url: String): Boolean {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun openFile(file: File, mimeType: String) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun toast(message: String, duration: UiAction.ToastDuration) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override val systemColors: SystemColors = object: SystemColors {}
        override fun openAppSettings() {
            TODO("Not yet implemented")
        }
    }

    @Before
    fun setup() {
        mockkObject(UiAction)
        every { UiAction.sendAction(any<DialogData>()) } answers {
            shown.add(firstArg())
            true
        }
    }

    @Test
    fun testDialogQueueing() {
        shown.clear()
        val dialog1 = DialogData(DialogData.Type.CLOSEABLE, "dialog1")
        val dialog2 = DialogData(DialogData.Type.CLOSEABLE, "dialog2")
        ApplicationState.current.data = ApplicationState.Background
        ForegroundServiceProvider.queue(dialog1)
        ForegroundServiceProvider.queue(dialog2)
        scheduler.triggerActions()
        assertTrue(shown.isEmpty())
        ApplicationState.current.data = ApplicationState.Visible
        scheduler.triggerActions()
        assertTrue(shown.isEmpty())
        ApplicationState.current.data = ApplicationState.Active
        scheduler.triggerActions()
        assertEquals(1, shown.size)
        assertTrue(dialog1.isShowing)
        ForegroundServiceProvider.cancel(dialog1)
        scheduler.triggerActions()
        assertEquals(2, shown.size)
        assertTrue(dialog2.isShowing)
        dialog2.dismiss()
        shown.clear()
        ApplicationState.current.data = ApplicationState.Visible
        ForegroundServiceProvider.queue(dialog1)
        ForegroundServiceProvider.cancel(dialog1)
        ApplicationState.current.data = ApplicationState.Active
        scheduler.triggerActions()
        assertTrue(shown.isEmpty())
        assertFalse(dialog1.isShowing)
    }
}
