package com.controlj.ifd

import com.controlj.rx.MainScheduler
import com.controlj.rx.observeBy
import io.reactivex.rxjava3.internal.schedulers.NewThreadScheduler
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


class IfdTrafficTest {

    @Before
    fun setup() {
        MainScheduler.instance = Schedulers.trampoline()
        RxJavaPlugins.setInitIoSchedulerHandler { NewThreadScheduler() }
        RxJavaPlugins.setInitComputationSchedulerHandler { NewThreadScheduler() }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { NewThreadScheduler() }
        RxJavaPlugins.setInitSingleSchedulerHandler { NewThreadScheduler() }

    }

    @Test
    fun trafficTest() {
        val latch = CountDownLatch(20)
        IfdTraffic.trafficObserver.observeBy {
            println("$it")
            latch.countDown()
        }
        latch.await(60, TimeUnit.SECONDS)
    }
}
