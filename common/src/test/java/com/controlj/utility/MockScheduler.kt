package com.controlj.utility

import com.controlj.logging.CJLog
import com.controlj.mock.SyserrDestination
import com.controlj.rx.MainScheduler
import io.mockk.every
import io.mockk.mockkStatic
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import org.threeten.bp.Instant
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-05
 * Time: 09:35
 */
object MockScheduler {
    val testScheduler = TestScheduler()

    init {
        reset()
        CJLog.add(SyserrDestination())
    }

    fun triggerActions() {
        testScheduler.triggerActions()
    }

    fun reset() {
        testScheduler.reset()
        MainScheduler.instance = testScheduler
        mockkStatic(Instant::class)
        every { instantNow } answers { Instant.ofEpochMilli(testScheduler.now(TimeUnit.MILLISECONDS)) }
        RxJavaPlugins.setComputationSchedulerHandler { testScheduler }
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
    }

    fun advanceTimeBy(i: Long, timeUnit: TimeUnit) {
        testScheduler.advanceTimeBy(i, timeUnit)
    }
}
