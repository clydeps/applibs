package com.controlj.utility

import org.junit.Test

import org.junit.Assert.*

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 2018-12-18
 * Time: 16:38
 */
class LRUCacheTest {

    @Test
    fun testCache() {
        val cache = LRUCache<String, Int>(3)
        cache.put("1", 1)
        assertEquals(1, cache.get("1"))
        assertEquals(null, cache.get("2"))
        assertEquals(null, cache.get("3"))
        assertEquals(null, cache.get("4"))
        assertEquals(null, cache.get("5"))
        cache.put("2", 2)
        cache.put("3", 3)
        assertEquals(1, cache.get("1"))
        assertEquals(2, cache.get("2"))
        assertEquals(3, cache.get("3"))
        assertEquals(null, cache.get("4"))
        assertEquals(null, cache.get("5"))
        cache.put("4", 4)
        assertEquals(null, cache.get("1"))
        assertEquals(2, cache.get("2"))
        assertEquals(3, cache.get("3"))
        assertEquals(4, cache.get("4"))
        assertEquals(null, cache.get("5"))
        cache.remove("2")
        cache.put("1", 1)
        assertEquals(1, cache.get("1"))
        assertEquals(null, cache.get("2"))
        assertEquals(3, cache.get("3"))
        assertEquals(4, cache.get("4"))
        assertEquals(null, cache.get("5"))
        assertEquals(1, cache.get("1"))         // bring to front
        cache.put("5", 5)
        assertEquals(1, cache.get("1"))
        assertEquals(null, cache.get("2"))
        assertEquals(null, cache.get("3"))
        assertEquals(4, cache.get("4"))
        assertEquals(5, cache.get("5"))
        cache.clear()
        assertEquals(null, cache.get("1"))
        assertEquals(null, cache.get("2"))
        assertEquals(null, cache.get("3"))
        assertEquals(null, cache.get("4"))
        assertEquals(null, cache.get("5"))
    }

    @Test
    fun testGet() {
        var purged: Int? = null
        val cache = LRUCache<String, Int>(3) { purged = it}
        cache.put("1", 1)
        assertEquals(1, cache.get("1"))
        assertEquals(null, cache.get("2"))
        assertEquals(2, cache.get("2") { 2 })
        assertEquals(3, cache.get("3") { 3 })
        assertEquals(4, cache.get("4") { 4 })
        assertEquals(null, cache.get("1"))
        assertEquals(1, purged)
    }

    @Test
    fun testTouch() {
        val cache = LRUCache<String, Int>(3)
        cache.put("1", 1)
        cache.put("2", 2)
        cache.put("3", 3)
        cache.touch("1")
        cache.put("4", 4)
        assertEquals(1, cache.get("1"))
        assertEquals(null, cache.get("2"))
        cache.touch("5")
        assertEquals(null, cache.get("5"))
    }
}