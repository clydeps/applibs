package com.controlj.utility

import org.junit.After
import org.junit.Before
import org.mockserver.client.MockServerClient
import org.mockserver.integration.ClientAndServer
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import kotlin.random.Random

abstract class HttpTestBase {

    private fun randomFrom(from: Int = 10000, to: Int = 65535) : Int {
        return Random.Default.nextInt(to - from) + from
    }

    private val port = randomFrom()

    var mockServer: MockServerClient = MockServerClient("localhost", port)

    val url = "http://localhost:$port"

    @Before
    fun prepare() {
        mockServer = ClientAndServer.startClientAndServer(port)
    }

    @After
    fun tearDown() {
        mockServer.close()
    }
}
