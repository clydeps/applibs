package com.controlj.utility

import com.controlj.framework.FilePaths
import java.io.File
import java.io.InputStream

object MockFilePaths : FilePaths {
    override val databaseDirectory: File = File("data", "databases")
    override val dataDirectory: File = File("data", "data")
    override val tempDirectory: File = File("data", "temp")

    override fun listAssets(path: String): List<String> {
        return (File(File("data", "assets"), path).list() ?: arrayOf<String>()).toList()
    }

    override fun openAsset(name: String): InputStream? {
        return File(File("data", "assets"), name).inputStream()
    }

    init {
        FilePaths.init(this)
        tempDirectory.mkdirs()
        dataDirectory.mkdirs()
        databaseDirectory.mkdirs()
    }
}
