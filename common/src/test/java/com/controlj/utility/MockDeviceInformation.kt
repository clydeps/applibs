package com.controlj.utility

import com.controlj.framework.DeviceInformation

object MockDeviceInformation : DeviceInformation {

    override val appBuild: Int = 1
    override val appVersion: String = "0.1"
    override val appName: String = "applibs-test"
    override val deviceName: String = "testjig"

    init {
        DeviceInformation.instance = this
    }

}
