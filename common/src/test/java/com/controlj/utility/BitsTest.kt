package com.controlj.utility

import org.junit.Assert.*
import org.junit.Test

class BitsTest {
    @ExperimentalUnsignedTypes
    @Test
    fun intBitsTest() {
        val intval: Int = 0x9A51567EL.toInt()

        assertTrue(intval.bit(31))
        assertFalse(intval.bit(30))
        assertFalse(intval.bit(29))
        assertTrue(intval.bit(28))
        assertTrue(intval.bit(3))
        assertTrue(intval.bit(2))
        assertTrue(intval.bit(1))
        assertFalse(intval.bit(0))

        assertEquals(0x1235u, bitsToUint(
                true, false, true, false,
                true, true, false, false,
                false, true, false, false,
                true, false

        ))
    }
}

