/**
 * Copyright (c) 2016-present, RxJava Contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 */
package com.controlj.utility

import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.internal.disposables.EmptyDisposable
import java.util.Queue
import java.util.concurrent.PriorityBlockingQueue
import java.util.concurrent.TimeUnit

/**
 * A special, non thread-safe scheduler for testing operators that require
 * a scheduler without introducing real concurrency and allows manually advancing
 * a virtual time.
 */
class TestScheduler() : Scheduler() {
    /** The ordered queue for the runnable tasks.  */
    internal val queue: Queue<TimedRunnable> = PriorityBlockingQueue(11)

    /** The per-scheduler global order counter.  */
    internal var counter: Long = 0

    // Storing time in nanoseconds internally.
    @Volatile
    internal var time: Long = 0

    /**
     * Reset the scheduler
     */

    fun reset() {
        queue.clear()
        time = 0
        counter = 0
    }
    /**
     * Creates a new TestScheduler with the specified initial virtual time.
     *
     * @param delayTime
     * the point in time to move the Scheduler's clock to
     * @param unit
     * the units of time that `delayTime` is expressed in
     */
    constructor(delayTime: Long, unit: TimeUnit): this() {
        time = unit.toNanos(delayTime)
    }

    internal class TimedRunnable(val scheduler: TestWorker, val time: Long, val run: Runnable?, // for differentiating tasks at same time
                        val count: Long) : Comparable<TimedRunnable> {

        override fun toString(): String {
            return "time=$time, run=$run"
        }

        override fun compareTo(other: TimedRunnable): Int {
            return if (time == other.time) {
                java.lang.Long.compare(count, other.count)
            } else java.lang.Long.compare(time, other.time)
        }

    }

    override fun now(unit: TimeUnit): Long {
        return unit.convert(time, TimeUnit.NANOSECONDS)
    }

    /**
     * Moves the Scheduler's clock forward by a specified amount of time.
     *
     * @param delayTime
     * the amount of time to move the Scheduler's clock forward
     * @param unit
     * the units of time that `delayTime` is expressed in
     */
    fun advanceTimeBy(delayTime: Long, unit: TimeUnit) {
        advanceTimeTo(time + unit.toNanos(delayTime), TimeUnit.NANOSECONDS)
    }

    /**
     * Moves the Scheduler's clock to a particular moment in time.
     *
     * @param delayTime
     * the point in time to move the Scheduler's clock to
     * @param unit
     * the units of time that `delayTime` is expressed in
     */
    fun advanceTimeTo(delayTime: Long, unit: TimeUnit) {
        val targetTime = unit.toNanos(delayTime)
        triggerActions(targetTime)
    }

    /**
     * Triggers any actions that have not yet been triggered and that are scheduled to be triggered at or
     * before this Scheduler's present time.
     */
    fun triggerActions() {
        triggerActions(time)
    }

    private fun triggerActions(targetTimeInNanoseconds: Long) {
        while (true) {
            val current = queue.peek()
            if (current == null || current.time > targetTimeInNanoseconds) {
                break
            }
            // if scheduled time is 0 (immediate) use current virtual time
            time = if (current.time == 0L) time else current.time
            queue.remove(current)

            // Only execute if not unsubscribed
            if (!current.scheduler.disposed) {
                current.run!!.run()
            }
        }
        time = targetTimeInNanoseconds
    }

    override fun createWorker(): @NonNull Worker? {
        return TestWorker()
    }

    internal inner class TestWorker : Worker() {
        @Volatile
        var disposed = false
        override fun dispose() {
            disposed = true
        }

        override fun isDisposed(): Boolean {
            return disposed
        }

        override fun schedule(run: @NonNull Runnable?, delayTime: Long, unit: @NonNull TimeUnit?): @NonNull Disposable? {
            if (disposed) {
                return EmptyDisposable.INSTANCE
            }
            val timedAction = TimedRunnable(this, time + unit!!.toNanos(delayTime), run, counter++)
            queue.add(timedAction)
            return Disposable.fromRunnable(QueueRemove(timedAction))
        }

        override fun schedule(run: @NonNull Runnable?): @NonNull Disposable? {
            if (disposed) {
                return EmptyDisposable.INSTANCE
            }
            val timedAction = TimedRunnable(this, 0, run, counter++)
            queue.add(timedAction)
            return Disposable.fromRunnable(QueueRemove(timedAction))
        }

        override fun now(unit:  TimeUnit): Long {
            return this@TestScheduler.now(unit)
        }

        internal inner class QueueRemove(val timedAction: TimedRunnable) : Runnable {
            override fun run() {
                queue.remove(timedAction)
            }

        }
    }
}
