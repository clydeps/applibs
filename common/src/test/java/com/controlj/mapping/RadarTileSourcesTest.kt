package com.controlj.mapping

import com.controlj.comms.OKClient
import com.controlj.settings.Properties
import com.controlj.utility.Hash
import com.controlj.utility.MockScheduler
import com.controlj.utility.MockScheduler.testScheduler
import com.controlj.utility.instantNow
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import okhttp3.OkHttpClient
import okhttp3.mock.Behavior
import okhttp3.mock.MockInterceptor
import okhttp3.mock.body
import okhttp3.mock.eq
import okhttp3.mock.get
import okhttp3.mock.respond
import okhttp3.mock.rule
import okhttp3.mock.url
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import org.threeten.bp.Instant
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 9/11/18
 * Time: 21:08
 */

class RadarTileSourcesTest {

    val baseUrl = "https://server.trackdirect.net/radar"

    @Before
    fun before() {
        unmockkAll()
        MockScheduler.reset()
        mockkObject(Properties)
        every { Properties.getProperty("radarSecret") } returns ("12345678")
        every { Properties.getProperty("radarUrl") } returns ("$baseUrl/")
        mockkObject(OKClient)
        every { OKClient.builder } answers {
            OkHttpClient.Builder().addInterceptor(interceptor)
        }
    }

    val metadata = """
        {"timestamp":4332972,
        "url":"${baseUrl}/{UUID}/tile.json",
        "heatmap":"${baseUrl}/{UUID}/heatmap.json",
        "history": {"4332972a9c":1559869920000,"433297153d":1559869560000,"4332970429":1559869200000,"4332969763":1559868840000,"4332968650":1559868480000,"43329670f0":1559868120000,"4332966fdd":1559867760000},
        "parameterName":"intensity","layerName":"radar","updateInterval":10,
        "colors":{"0":16119295,"1":11842815,"2":7895295,"3":1316095,"4":55491,"5":38544,"6":26214,"7":16776960,"8":16762880,"9":16750080,"10":16737280,"11":16711680,"12":13107200,"13":7864320,"14":2621440}}
    """
    val interceptor = MockInterceptor(Behavior.UNORDERED).apply {
        rule(get, url eq "$baseUrl/meta.json", times = 10) {
            respond {
                val auth = it.header("Authorization")?.split(":")
                assertEquals(2, auth?.size)
                assertTrue(Hash.checkHash("12345678", auth!![0], auth[1]))
                code(200)
                body(metadata)
            }
        }
    }

    @After
    fun after() {
    }

    @Test
    fun getMeta() {
        var result: RadarTileSources.TileSet? = null
        val lock = CountDownLatch(1)
        RadarTileSources.getMeta()
                .take(1)
                .subscribe(
                        {
                            result = it
                            println(it)
                            lock.countDown()
                        },
                        {
                            println(it)
                            fail(it.toString())
                        }
                )
        testScheduler.triggerActions()
        lock.await(10, TimeUnit.SECONDS)

        val resnn = result!!
        result = null
        assertEquals(7, resnn.history.size)
        assertEquals("$baseUrl/{UUID}/tile.json", resnn.url)
        assertEquals("$baseUrl/{UUID}/heatmap.json", resnn.heatmap)
        assertEquals("$baseUrl/4332972a9c/tile.json", resnn.getUrl("4332972a9c", false))
        assertEquals("$baseUrl/4332972a9c/heatmap.json", resnn.getUrl("4332972a9c", true))
        assertEquals(4332972, resnn.timestamp)
        assertEquals(1559869920000L, resnn.history.get("4332972a9c"))
        assertEquals("radar", resnn.layerName)
        assertEquals("intensity", resnn.parameterName)
        assertEquals(10, resnn.updateInterval)
        assertEquals(16119295, resnn.colors.get(0))
        assertEquals("4332972a9c", resnn.historyList.first())
        mockkStatic(Instant::class)
        every { instantNow } returns Instant.ofEpochMilli(1559869920000L).plusSeconds(300)
        assertEquals(300000, resnn.getAge("4332972a9c"))
        assertTrue(resnn.isValid())

        assertEquals(1559869920000L, resnn.history.get("4332972a9c"))
    }
}
