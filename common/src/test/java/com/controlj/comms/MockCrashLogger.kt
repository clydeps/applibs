package com.controlj.comms

import com.controlj.logging.CrashLogger
import com.controlj.utility.MockDeviceInformation
import java.util.Date

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 * User: clyde
 * Date: 2019-06-18
 * Time: 13:55
 */
object MockCrashLogger : CrashLogger {
    init {
        MockDeviceInformation
        initialise(mapOf(
                "DEVICE_NAME" to "TEst",
                "SYSTEM_VERSION" to "Test version",
                "USER_CRASH_DATE" to Date().toString()))
    }

}
