package com.controlj.comms

import com.controlj.data.DiskFileEntry
import com.controlj.data.FileBody
import com.controlj.data.FileEntry
import com.controlj.logging.CrashLogger
import com.controlj.settings.Properties
import com.controlj.test.MockBackgroundServiceProvider
import com.controlj.utility.MockFilePaths
import com.controlj.utility.MockScheduler
import com.controlj.utility.instantNow
import io.mockk.every
import io.mockk.mockkObject
import okhttp3.ConnectionSpec
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.mock.MediaTypes.MEDIATYPE_JSON
import okhttp3.mock.MockInterceptor
import okhttp3.mock.body
import okhttp3.mock.eq
import okhttp3.mock.post
import okhttp3.mock.respond
import okhttp3.mock.rule
import okhttp3.mock.url
import okio.Buffer
import okio.Source
import okio.Timeout
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import org.threeten.bp.Instant
import java.io.ByteArrayOutputStream
import java.io.File
import java.nio.ByteBuffer
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Copyright (C) Control-J Pty. Ltd. ACN 103594190
 * All rights reserved
 *
 *
 * User: clyde
 * Date: 2019-06-14
 * Time: 12:37
 */
class UploaderTest {
    class StringFileEntry(override val name: String, private val data: String) : FileEntry {
        override val length: Long = -1

        override fun getSource(): Source {
            return object : Source {
                private val buffer = ByteBuffer.wrap(data.toByteArray())
                override fun read(sink: Buffer, byteCount: Long): Long = sink.write(buffer).toLong()
                override fun timeout(): Timeout = Timeout.NONE
                override fun close() = Unit
            }
        }

        override fun delete() = Unit
        override fun save() = Unit
        override val token: String = "String:$name"
        override val timeStamp: Instant = instantNow

    }

    var responded = false
    val interceptor by lazy {
        val host = CrashLogger.LOGHOST + "crashlog"
        MockInterceptor().apply {
            rule(post, url eq host) {
                respond {
                    val body = it.body as MultipartBody
                    val fileBodies = body.parts.map { it.body }.filterIsInstance<FileBody>()
                    assertEquals(2, fileBodies.size)
                    assertTrue(fileBodies.all { it.fileEntry.exact })
                    val length = it.body?.contentLength() ?: 0
                    println("Length = $length")
                    val buffer = Buffer()
                    it.body?.writeTo(buffer)
                    val out = ByteArrayOutputStream()
                    buffer.writeTo(out)
                    assertEquals(length.toInt(), out.size())
                    println(body)
                    responded = true
                    code(200)
                    body(
                        """{
                        "status": "OK",
                        "files": 1
                    }""".trimMargin(), MEDIATYPE_JSON
                    )
                }
            }
        }
    }

    @Before
    fun before() {
        MockCrashLogger
        MockFilePaths
        MockBackgroundServiceProvider
        MockScheduler.reset()
        mockkObject(Properties)
        every { Properties.getProperty("crashlogger.url") } returns "http://localhost:8280/crashlog"
        every { Properties.getProperty("crashlogger.secret") } returns "dumy secret"
        mockkObject(OKClient)
        every { OKClient.builder } answers {
            OkHttpClient.Builder()
                .connectionSpecs(
                    listOf(
                        ConnectionSpec.CLEARTEXT,
                        ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
                            .allEnabledCipherSuites()
                            .allEnabledTlsVersions()
                            .build()
                    )
                )


        }
    }

    @Test
    fun uploadTest() {
        mockkObject(OKClient)
        every { OKClient.builder } answers {
            OkHttpClient.Builder().addInterceptor(interceptor)
        }
        val latch = CountDownLatch(1)
        val uploader = Uploader("Test", "1.1")
        uploader.upload(
            "Message",
            listOf(
                DiskFileEntry(File("data/test1.log")),
                StringFileEntry("string.data", "this is string data.asdfasdfasd.fasdfalsdjfa;lsdkjasl;dkjasl;d")
            ),
            "test@test.org"
        )
            .subscribe(
                {
                    println(it)
                },
                { fail("Exception $it") },
                { latch.countDown() }
            )
        MockScheduler.triggerActions()
        latch.await(5000, TimeUnit.MILLISECONDS)
        MockScheduler.advanceTimeBy(5, TimeUnit.SECONDS)
        assertTrue(responded)
    }
}
