package com.controlj.data

import com.controlj.data.Constants.INVALID_DATA
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class PositionTest {

    @Test
    fun positionTest() {

        val pos = Position.of(-32.0, 120.0)

        assertFalse(Position.invalid.valid)
        assertTrue(pos.valid)
        assertEquals(pos.latitude, pos.latitudeString.toDouble(), 0.0001)
        assertEquals(pos.longitude, pos.longitudeString.toDouble(), 0.0001)
        assertEquals("S32°00 E120°00", pos.description)
        assertEquals("N19°11 W097°59", Position.of(19.1834, -97.9834).description)
        assertEquals(14656938.6, pos.distanceTo(Position.of(1.73, -100.0)), 0.1)
        assertEquals(120.71, pos.bearingTo(Position.of(1.73, -100.0)), 0.2)
        assertEquals(INVALID_DATA, pos.distanceTo(Position.invalid), 0.0)
        val pos1 = Position.of(-34.0, 122.0)
        val distance = pos.distanceTo(pos1)
        val bearing = pos.bearingTo(pos1)
        val pos2 = pos.translate(bearing, distance)
        assertEquals(pos1.latitude, pos2.latitude, .01)
        assertEquals(pos1.longitude, pos2.longitude, .01)

        (5..175 step 10).forEach { i ->
            var origin = pos.translate(270.0 - i, 10000.0)
            var dest = pos.translate(270.0 + i, 10000.0)
            assertFalse(pos.translate(269.0, 100.0).past(pos, origin, dest))
            assertTrue(pos.translate(271.0, 100.0).past(pos, origin, dest))
            assertFalse(pos.translate(91.0, 100.0).past(pos, origin, dest))
            assertTrue(pos.translate(89.0, 100.0).past(pos, origin, dest))

            origin = pos.translate(180.0 - i, 10000.0)
            dest = pos.translate(180.0 + i, 10000.0)
            assertFalse(pos.translate(179.0, 100.0).past(pos, origin, dest))
            assertTrue(pos.translate(181.0, 100.0).past(pos, origin, dest))
            assertFalse(pos.translate(1.0, 100.0).past(pos, origin, dest))
            assertTrue(pos.translate(359.0, 100.0).past(pos, origin, dest))

            origin = pos.translate(0.0 - i, 10000.0)
            dest = pos.translate(0.0 + i, 10000.0)
            assertFalse(pos.translate(181.0, 100.0).past(pos, origin, dest))
            assertTrue(pos.translate(179.0, 100.0).past(pos, origin, dest))
            assertFalse(pos.translate(359.0, 100.0).past(pos, origin, dest))
            assertTrue(pos.translate(1.0, 100.0).past(pos, origin, dest))
        }
    }
}
