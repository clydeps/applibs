package com.controlj.data

import com.controlj.data.navdata.NavPoint
import com.controlj.database.CJDatabase
import com.controlj.framework.FilePaths
import com.controlj.framework.FileUpdater
import com.controlj.logging.CJLog
import com.controlj.mock.SyserrDestination
import com.controlj.test.MockBackgroundServiceProvider
import com.controlj.utility.MockScheduler
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.File

class FlightRouteTest {

    companion object {
        const val basedir = "data"
        const val databasedir = "navdb"
    }

    init {
        MockBackgroundServiceProvider
        CJLog.add(SyserrDestination())
    }

    @Before
    fun setup() {
        unmockkAll()
        MockScheduler.reset()
        FilePaths.init(mockk {
            every { databaseDirectory } returns File(basedir, databasedir)
            every { openAsset(any()) } answers {
                File(File(basedir, "assets"), firstArg()).inputStream()
            }
        })
        File(basedir, databasedir).deleteRecursively()
        File(basedir, databasedir).mkdirs()
        mockkObject(NavPoint)
        every { NavPoint.database } returns CJDatabase(NavPoint.DB_FILE_NAME) {
            FileUpdater.updateFileFromAssets(File(CJDatabase.directory, NavPoint.META_FILE_NAME))
        }
    }

    @After
    fun tearDown() {
        File(basedir, databasedir).deleteRecursively()
        unmockkAll()
    }

    @Test
    fun testFromString() {
        val route = FlightRoute.fromString("YBAF,YCFS;PMQ ydfd").blockingGet()
        assertEquals(4, route.points.size)
    }
}
