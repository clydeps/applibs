package com.controlj.data

import com.controlj.database.waitForDb
import com.controlj.framework.FilePaths
import com.controlj.logging.CJLog
import com.controlj.test.MockBackgroundServiceProvider
import com.controlj.utility.MockScheduler
import com.controlj.utility.instantNow
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.io.File

class MessageRecordTest {
    init {
        MockBackgroundServiceProvider
        CJLog.isDebug = true
    }

    val scheduler = MockScheduler.testScheduler

    @Before
    fun setup() {
        unmockkAll()
        FilePaths.init(mockk {
            every { databaseDirectory } returns File("data", "databases").also { it.mkdirs() }
        })
        File(FilePaths().databaseDirectory, MessageRecord.DB_NAME).delete()
        mockkStatic(Instant::class)
        every { instantNow } returns Instant.EPOCH.plus(Duration.ofDays(365))
    }

    @Test
    fun recordTest() {
        val mr1 = MessageRecord.of("Test message", Priority.ACTION, "subtext")
        assertTrue(mr1.active)
        assertTrue(mr1.started <= instantNow)
        assertTrue(mr1.ended <= instantNow)
        assertEquals(0, mr1.id)
        val mr2 = MessageRecord.from(mr1)
        assertEquals(mr1, mr2)
        val mr3 = MessageRecord.from(mr1, 23)
        assertEquals(mr1, mr3)
        mr3.subtext = "gobble"
        mr3.ended = Instant.EPOCH
        assertEquals(mr1, mr3)
        val mr4 = MessageRecord.of("zulu", Priority.ACTION, "", started = Instant.EPOCH)
        assertEquals("1970-01-01 00:00:00 Z", mr4.zuluTime())
        val mr5 = MessageRecord.of("zulu", Priority.ACTION, "", started = Instant.EPOCH.plusSeconds(10))
        assertTrue(mr5 < mr4)       // more recent messages should come first
        val mr6 = MessageRecord.of("message", Priority.NONE, "", started = instantNow.minusSeconds(12345))
        var testSubscriber = mr6.update(false).test()
        scheduler.triggerActions()
        MessageRecord.Companion.database.waitForDb()
        Thread.sleep(1000)  // not sure why this is necessary
        testSubscriber.assertComplete()
        val result = testSubscriber.values().first()
        assertEquals("message", result.message)
        assertEquals(12345L, result.duration.seconds)
        testSubscriber = MessageRecord.getEntries().test()
        MessageRecord.Companion.database.waitForDb()
        testSubscriber.assertComplete()
        testSubscriber.assertValueCount(1)
        MessageRecord.of("message1", Priority.NONE, "subtext").save().blockingGet()
        val mr7 = MessageRecord.of("message2", Priority.NONE, "subtext").save().blockingGet()
        testSubscriber = MessageRecord.getEntries().filter { it.active }.test()
        MessageRecord.Companion.database.waitForDb()
        testSubscriber.assertComplete()
        testSubscriber.assertValueCount(2)
        mr7.update(false).blockingGet()
        assertFalse(mr7.active)
        testSubscriber = MessageRecord.getEntries().filter { it.active }.test()
        MessageRecord.Companion.database.waitForDb()
        testSubscriber.assertComplete()
        testSubscriber.assertValueCount(1)
        MessageRecord.clearActive().blockingGet()
        testSubscriber = MessageRecord.getEntries().filter { it.active }.test()
        MessageRecord.Companion.database.waitForDb()
        testSubscriber.assertComplete()
        testSubscriber.assertValueCount(0)
    }
}
