/*
 * Copyright 2008-2009 Mike Reedell / LuckyCatLabs.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.luckycatlabs.sunrisesunset.util

import java.io.File

/**
 * Simple class to read a CSV file and return a list of arrays of n strings that comprise a line in the CSV
 * file.
 */
class CSVTestDriver(testDataDirectoryName: String) {
    private val testDataDirectory: File = File(testDataDirectoryName)
    val fileNames: Array<String>
        get() = testDataDirectory.list() ?: arrayOf()

    fun getData(testDataFileName: String): List<List<String>> {
        return File("$testDataDirectory/$testDataFileName").readLines().map { it.split(',') }
    }
}
