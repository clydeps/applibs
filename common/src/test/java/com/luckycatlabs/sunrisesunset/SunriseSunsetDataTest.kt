/*
 * Copyright 2008-2009 Mike Reedell / LuckyCatLabs.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.luckycatlabs.sunrisesunset

import com.controlj.data.Position
import com.controlj.utility.SolarCalculator
import com.luckycatlabs.sunrisesunset.util.BaseTestCase
import com.luckycatlabs.sunrisesunset.util.CSVTestDriver
import org.junit.AfterClass
import org.junit.Assert.assertFalse
import org.junit.Test
import java.util.Calendar
import java.util.TimeZone

class SunriseSunsetDataTest : BaseTestCase() {

    @Test
    fun testRiseAndSetTimes() {
        assertFalse(dataSetNames.isEmpty())
        dataSetNames.forEach { dataSetName ->
            val data = driver.getData(dataSetName)
            val dataSetNameParts = dataSetName.split('#').filterNot { it.isEmpty() }
            val timeZoneName = dataSetNameParts[1].split('.').first { it.isNotEmpty() }.replace('-', '/')
            location = createLocation(dataSetNameParts[0])

            data.forEach { line ->
                val date = line[0]
                val calendar = createCalendar(date.split("/").filterNot { it.isEmpty() })
                val calc = SolarCalculator(location, timeZoneName)
                println("line = ${line.joinToString(",")}")

                assertTimeEquals(line[1], calc.getAstronomicalSunriseForDate(calendar), date)
                assertTimeEquals(line[8], calc.getAstronomicalSunsetForDate(calendar), date)
                assertTimeEquals(line[2], calc.getNauticalSunriseForDate(calendar), date)
                assertTimeEquals(line[7], calc.getNauticalSunsetForDate(calendar), date)
                assertTimeEquals(line[4], calc.getOfficialSunriseForDate(calendar), date)
                assertTimeEquals(line[5], calc.getOfficialSunsetForDate(calendar), date)
                assertTimeEquals(line[3], calc.getCivilSunriseForDate(calendar), date)
                assertTimeEquals(line[6], calc.getCivilSunsetForDate(calendar), date)
            }
        }
    }

    private fun createCalendar(dateParts: List<String>): Calendar {
        val cal = Calendar.getInstance()
        cal.set(Integer.valueOf(dateParts[2]), Integer.valueOf(dateParts[0]) - 1, Integer.valueOf(dateParts[1]))
        return cal
    }

    private fun createLocation(fileName: String): Position {
        val latlong = fileName.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var latitude = latlong[0].replace('_', '.')
        var longitude = latlong[1].replace('_', '.')

        if (latitude.endsWith("S")) {
            latitude = "-$latitude"
        }
        if (longitude.endsWith("W")) {
            longitude = "-$longitude"
        }
        latitude = latitude.substring(0, latitude.length - 1)
        longitude = longitude.substring(0, longitude.length - 1)
        return Position.of(java.lang.Double.parseDouble(latitude), java.lang.Double.parseDouble(longitude))
    }

    private val driver = CSVTestDriver("data/suntestdata")
    private val dataSetNames = driver.fileNames

    companion object {

        @AfterClass
        fun tearDownAllTests() {
            TimeZone.setDefault(TimeZone.getTimeZone("America/New_York"))
        }
    }
}
