plugins {
    kotlin("jvm")
    application
}

group = "com.control-j.applibs"
version = "2.2.10-SNAPSHOT"

repositories {
    mavenCentral()
}

application {
    mainClass.set("com.controlj.stratux.sim.GdlSimulator")
}

dependencies {
    implementation(project(":common"))

    listOf(
        Kotlin,
        Rxjava3,
        CJLog,
        ThreetenBp,
        Javalin,
        KotlinCoroutines,
        Jackson
    ).forEach { implementation(it()) }


    listOf(
        Junit,
        Mockk,
    ).forEach { testImplementation(it()) }
}
