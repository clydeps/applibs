import com.controlj.aviation.VHCodes
import org.junit.Test
import java.io.File

class VHGenerate {

    /**
     * Generate a file defining all VH registered aircraft IDs
     */
    @Test
    fun generateVH() {
        val dir = File("generated")
        dir.mkdirs()
        val file = File(dir, "xcsoar-flarm.txt")
        file.writer().use { os ->
            (VHCodes.VHBase until VHCodes.VHBase + 36 * 36 * 36).forEach {
                os.write("%06X=%s\n".format(it, VHCodes.callFrom(it)))
            }
        }
    }
}
