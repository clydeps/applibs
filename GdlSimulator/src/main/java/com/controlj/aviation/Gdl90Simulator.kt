package com.controlj.aviation

import com.controlj.location.LocationSource
import com.controlj.logging.CJLog.logException
import com.controlj.network.NetworkFactory
import com.controlj.network.UdpData
import com.controlj.nmea.GpsFix
import com.controlj.nmea.UdpBroadcaster
import com.controlj.pressure.PressureSource.toPressure
import com.controlj.rx.MainScheduler
import com.controlj.settings.MapDataStore
import com.controlj.stratux.Stratux
import com.controlj.stratux.Stratux.decode
import com.controlj.stratux.Stratux.encode
import com.controlj.stratux.gdl90.ForeFlight
import com.controlj.stratux.gdl90.GDL90
import com.controlj.stratux.gdl90.GDL90TrafficReport.Companion.asGDL90Report
import com.controlj.stratux.gdl90.Heartbeat
import com.controlj.stratux.gdl90.OwnshipReport
import com.controlj.stratux.gdl90.StratuxHeartbeat
import com.controlj.stratux.gdl90.StratuxStatus
import com.controlj.stratux.sim.LocationProviderSimulation
import com.controlj.stratux.sim.TrafficProviderSimulation
import com.controlj.traffic.TrafficSettings
import com.controlj.traffic.TrafficSource
import com.controlj.traffic.TrafficTarget
import io.javalin.Javalin
import io.javalin.websocket.WsConnectContext
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import org.threeten.bp.LocalTime
import org.threeten.bp.ZoneOffset
import java.net.InetAddress
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit
import kotlinx.coroutines.*
import java.time.Clock

object Gdl90Simulator {

    data class StatusMessage(
        val clientCount: Int,
        val status: Status
    )

    data class Status(
        var NACp: Int,
        var NIC: Int,
        var baroAltitude: Double,
        var callsign: String,
        var gnssAltitude: Double,
        var gpsFix: Int,
        var gpsSatellites: Int,
        var icaoAddress: Int,
        var latitude: Double,
        var longitude: Double
    )

    data class SkyEchoStats(
        val `batteryCapacity_%`: Int,
        val host: Host,
        val sensor: Sensor
    )

    data class Host(
        val GPS: GPS
    )

    data class Sensor(
        val baro: Baro
    )

    data class GPS(
        val FixType: Int,
        val GnssAlt: Int,
        val NumSats: Int,
        val UTCTime: Int,
        val positionAccuracy_mm: Int,
        val velocityAccuracy_mmps: Int,
        val verticalAccuracy_mm: Int
    )

    data class Baro(
        val altitude_m: Double,
        val pressure_mbar: Double,
        val temp_c: Double
    )

    const val MAX_PACKET_SIZE = 508     // max size of data packet
    const val port = 7070               // websocket port
    const val ownAddress = 0x7C0000
    // list of clients to send to

    private val clientAddresses = mutableSetOf<InetAddress>()
    private val listenPorts = listOf(47578, 63093)
    private val broadcaster by lazy { UdpBroadcaster(Stratux.portSetting.value, "255.255.255.255") }

    private val packetBuffer = ByteBuffer.allocate(MAX_PACKET_SIZE)

    private fun collect(data: UdpData) {
        if (clientAddresses.add(data.address))
            println("Added client ${data.address}")
    }

    @Suppress("CheckResult")
    private fun collectClients() {
        listenPorts.forEach { port ->
            NetworkFactory.observeUdpBroadcast(port).subscribeOn(Schedulers.io()).subscribe(::collect)
        }
    }

    /**
     * Send the packet buffer contents to all clients and reset the buffer.
     */
    private fun flushPacket() {
        packetBuffer.flip()
        if (packetBuffer.limit() != 0) {
            val data = ByteArray(packetBuffer.limit())
            packetBuffer.get(data)
            packetBuffer.clear()
            clientAddresses.forEach { broadcaster.broadcast(data, it) }
        }
    }

    private fun addPacket(message: GDL90) {
        val data = message.encode()
        if (decode(data).size != 1) {     // debug purposes
            println("Bad encode/decode of $message")
            return;
        }
        require(data.size <= MAX_PACKET_SIZE)
        if (packetBuffer.position() + data.size > MAX_PACKET_SIZE)
            flushPacket()
        packetBuffer.put(data)
    }

    @Suppress("NewApi")
    fun gdl90Broadcast() {
        TrafficSettings.distanceFilter.value = 25000.0
        TrafficSettings.altitudeFilter.value = 5000.0
        LocationSource.add(LocationProviderSimulation)
        TrafficSource.add(TrafficProviderSimulation)
        val heartbeat = Heartbeat(InetAddress.getLocalHost()).apply {
            utcOk = true
            gpsPosValid = true
        }
        val ownshipReport = OwnshipReport().apply {
            callSign = "SIM"
            emitterCategory = TrafficTarget.EmitterCategory.Light
            address = TrafficTarget.EmitterAddress(TrafficTarget.AddressType.AdsbSelf, ownAddress)
            creator = LocationProviderSimulation.creator
            isInFlight = true
        }
        val status = StratuxStatus(null).apply {
            gpsFix = GpsFix.ThreeD
            pressureValid = true
            enable1090 = true
            enableGps = true
            radioCount = 1
            satellitesLocked = 7
            satellitesTracked = 10
            num1090Targets = TrafficProviderSimulation.maxTargets
            rate1090Targets = num1090Targets
            cpuTemp = 39.0
        }
        val ffId = ForeFlight.IDMsg(null).apply {
            serialNumber = 1234UL
            deviceName = "Sim:$port"
            longDeviceName = "SkyEcho Simulator; websocket port $port"
        }
        val stratuxHeartbeat = StratuxHeartbeat(null).apply {
            gpsEnabled = true
            protocolVersion = StratuxStatus.VERSION
        }
        collectClients()
        Observable.interval(1, TimeUnit.SECONDS).blockingSubscribe({
            heartbeat.timeStamp = LocalTime.now(ZoneOffset.UTC).toSecondOfDay()
            addPacket(heartbeat)
            ownshipReport.apply(LocationSource.lastLocation)
            addPacket(ownshipReport)
            val targets = TrafficSource.currentTargets
            status.num1090Targets = targets.size
            status.rate1090Targets = status.num1090Targets
            addPacket(status)
            addPacket(ffId)
            addPacket(stratuxHeartbeat)
            targets.forEach {
                addPacket(it.asGDL90Report())
            }
            flushPacket()
        }) { logException(it) }
    }

    suspend fun sendStatus(ctx: WsConnectContext) {
        println("Started sendData: ${ctx.host()}")
        while (true) {
            val location = LocationSource.lastLocation
            val message = StatusMessage(
                1,
                Status(
                    8,
                    clientAddresses.size,
                    location.altitude,
                    "SIM",
                    location.altitude,
                    3,
                    7,
                    ownAddress,
                    location.latitude,
                    location.longitude
                )
            )
            ctx.send(message)
            delay(1000)
        }
    }

    /**
     * Get the current time as a GPS time. Seconds since 1980, without leap seconds.
     */
    fun gpsTime(): Int {
        return (Clock.systemUTC().millis() / 1000 - 315964800 + 18).toInt()
    }

    suspend fun sendStats(ctx: WsConnectContext) {
        println("Started sendStats")
        while (ctx.session.isOpen) {
            val location = LocationSource.lastLocation
            val message = SkyEchoStats(
                `batteryCapacity_%` = 75,
                host = Host(
                    GPS(
                        3,
                        GnssAlt = (location.altitude * 1000).toInt(),
                        NumSats = 7,
                        UTCTime = gpsTime(),
                        positionAccuracy_mm = (location.accuracyH * 1000).toInt(),
                        velocityAccuracy_mmps = (location.accuracyV * 1000).toInt(),
                        verticalAccuracy_mm = (location.accuracyV * 1000).toInt()
                    )
                ),

                sensor = Sensor(
                    Baro(location.altitude, toPressure(location.altitude), 25.0)
                ),
            )
            ctx.send(message)
            delay(1000)
        }
    }


    @JvmStatic
    fun main(args: Array<String>) {
        MapDataStore(true)
        MainScheduler.instance = Schedulers.trampoline()
        val app = Javalin.create(/*config*/)
            .ws("/") { ctx ->
                ctx.onConnect {
                    val job = CoroutineScope(Dispatchers.IO).launch { sendStatus(it) }
                    ctx.onClose {
                        println("Ended websocket: ${it.host()}")
                        job.cancel()
                    }
                }
            }
            .ws("/stats") { ctx ->
                ctx.onConnect {
                    val job = CoroutineScope(Dispatchers.IO).launch { sendStats(it) }
                    ctx.onClose {
                        println("Ended websocket: ${it.host()}")
                        job.cancel()
                    }
                }
            }
            .start(port)
        gdl90Broadcast()
    }
}
