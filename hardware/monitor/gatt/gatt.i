<?xml version="1.0" encoding="UTF-8"?>

<gatt prefix="GATTDB_" out="gatt_db.c" header="gatt_db.h" generic_attribute_service="false">

    <!-- Generic Access Service -->
    <!-- https://developer.bluetooth.org/gatt/services/Pages/ServiceViewer.aspx?u=org.bluetooth.service.generic_access.xml -->
    <service uuid="1800">

        <!-- Device Name -->
        <!-- https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.gap.device_name.xml -->
        <characteristic uuid="2a00" id="devname">
            <properties read="true" write="true"/>
            <value type="user" length="20"/>
        </characteristic>

        <!-- Appearance -->
        <!-- https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.gap.appearance.xml -->
        <characteristic uuid="2a01">
            <properties read="true" const="true"/>
            <value type="hex">0000</value><!-- TODO:: Set value or change type to "user" and add handling for it -->
        </characteristic>

    </service>

    <!-- Device Information Service -->
    <service uuid="180A">
        <!-- Manufacturer name string -->
        <characteristic uuid="2A29">
            <properties read="true" const="true"/>
            <value>Control-J Pty Ltd</value>
        </characteristic>
        <!-- Model number string -->
        <characteristic uuid="2A24">
            <properties read="true" const="true"/>
            <value>4370-4350</value>
        </characteristic>
        <!-- Serial number string -->
        <characteristic uuid="2A25" id="serialnum">
            <properties read="true" write="true"/>
            <value length="20" type="user">sernum</value>
        </characteristic>
        <!-- Hardware revision -->
        <characteristic uuid="2A27" id="hwrev">
            <properties read="true" const="true"/>
            <value>3</value>
        </characteristic>
        <!-- Firmware revision -->
        <characteristic uuid="2A26" id="fwrev">
            <properties read="true" const="true"/>
            <value>3.9</value>
        </characteristic>
    </service>

    <service uuid="181A">
        <description>Environmental Sensing</description>

        <characteristic uuid="2A6E" id="temperature">
            <properties notify="true" read="true"/>
            <value type="user" length="2"/>
            <description>Temperature</description>
        </characteristic>
        <characteristic uuid="2A6F" id="humidity">
            <properties notify="true" read="true"/>
            <value type="user" length="2"/>
            <description>Humidity</description>
        </characteristic>
        <characteristic uuid="2A6D" id="pressure">
            <properties notify="true" read="true"/>
            <value type="user" length="4"/>
            <description>Pressure</description>
        </characteristic>
    </service>

    <service uuid="67541000-d065-45bd-890b-d960e85e0247">
        <description>Configuration</description>

        <characteristic uuid="773c1007-d065-45bd-890b-d960e85e0247" id="ota_trigger">
            <properties type="user" write="true" />
            <value type="user" length="6"/>
            <description>DFU</description>
        </characteristic>
        <characteristic uuid="773c100B-d065-45bd-890b-d960e85e0247" id="debug">
            <properties notify="true" />
            <value type="user" length="64"/>
            <description>Debug</description>
        </characteristic>
        <characteristic uuid="773c1100-d065-45bd-890b-d960e85e0247" id="environment">
            <properties notify="true" read="true"/>
            <value type="user" length="10"/>
            <description>Environmental</description>
        </characteristic>
        <characteristic uuid="773c1101-d065-45bd-890b-d960e85e0247" id="light">
            <properties notify="true" read="true"/>
            <value type="user" length="2"/>
            <description>Light</description>
        </characteristic>
        <characteristic uuid="773c1102-d065-45bd-890b-d960e85e0247" id="sleep">
            <properties write="true" read="true"/>
            <value type="user" length="2"/>
            <description>Sleep</description>
        </characteristic>
        <characteristic uuid="773c1103-d065-45bd-890b-d960e85e0247" id="config">
            <properties write="true" read="true"/>
            <value type="user" length="4"/>
            <description>Sleep</description>
        </characteristic>



    </service>

</gatt>
