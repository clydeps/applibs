//
// Created by Clyde Stubbs on 24/1/18.
//

#include <em_cmu.h>
#include <em_cryotimer.h>
#include <em_emu.h>
#include <em_rmu.h>
#include "io.h"

void enterEM4(uint32_t duration) {

    RMU_ResetCauseClear();

// Configure EM4 behavior
    EMU_EM4Init_TypeDef init = EMU_EM4INIT_DEFAULT;

    init.em4State = emuEM4Hibernate;  // emuEM4Hibernate also works
    CMU_ClockSelectSet(cmuClock_RTCC, cmuSelect_ULFRCO);
    init.retainUlfrco = true;
    EMU_EM4Init(&init);

// Initialize and start CRYOTIMER
    startCryo(duration);
    EMU_EnterEM4();
}

// Start the cryotimer with timeout in at least duration seconds
void startCryo(uint32_t duration) {
    // calculate sleep time that is less than the requirement, specified in units of 1024ms

    uint32_t val = duration;
    CRYOTIMER_Period_TypeDef period = cryotimerPeriod_1;     // divide by 8 in conjunction with prescale of 128 is divide by 1024
    while (val > 1) {
        val >>= 1;
        period++;
    }
    val = 1UL << period;
    RTCC->RET[16].REG = duration - val;
    RTCC->RET[17].REG = TIMER_KEY;
    RTCC->RET[15].REG = (uint32_t) ~TIMER_KEY;
    printf("delay is %d, balance %d\n", val, RTCC->RET[16].REG);

    // Enable CRYOTIMER clock
    CMU_ClockEnable(cmuClock_CRYOTIMER, true);

    // Clear CRYOTIMER_IF PERIOD flag; it will be set upon EM4 wake
    CRYOTIMER_IntClear(CRYOTIMER_IF_PERIOD);

    /*
     * Set CRYOTIMER parameters.  Note that disabling the CRYOTIMER is
     * necessary after EM4 wake in order to reset the counter, otherwise
     * the next delay before wake won't be the full 2K periods.
     */
    CRYOTIMER_Init_TypeDef init = CRYOTIMER_INIT_DEFAULT;
    init.enable = false;
    init.em4Wakeup = true;
    init.osc = cryotimerOscULFRCO;
    init.presc = cryotimerPresc_128;
    init.period = period + cryotimerPeriod_8;

    CRYOTIMER_Init(&init);

    // Interrupt setup
    CRYOTIMER_IntClear(CRYOTIMER_IF_PERIOD);
    CRYOTIMER_IntEnable(CRYOTIMER_IEN_PERIOD);
    NVIC_ClearPendingIRQ(CRYOTIMER_IRQn);
    NVIC_EnableIRQ(CRYOTIMER_IRQn);

    CRYOTIMER_Enable(true);
}

bool continueSleep() {
    uint32_t delay = RTCC->RET[16].REG;
    uint32_t timer_key = RTCC->RET[17].REG;
    if (timer_key == TIMER_KEY && RTCC->RET[15].REG == (uint32_t) ~TIMER_KEY) {
        if(delay != 0)
            enterEM4((uint32_t) delay);
        RTCC->RET[15].REG = 0;
        return true;
    }
    return false;

}

void CRYOTIMER_IRQHandler(void) {
    // Clear the CRYOTIMER interrupt
    CRYOTIMER_IntClear(CRYOTIMER_IF_PERIOD);

    /*
     * Flush instructions to make sure the interrupt is not re-triggered.
     * This is generally required when the peripheral clock is slower than
     * the CPU core clock.
     */
    __DSB();
}
