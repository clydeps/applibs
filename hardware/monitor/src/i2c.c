//
// Created by Clyde Stubbs on 5/1/18.
//

#include <em_gpio.h>
#include "io.h"
#include "i2c.h"

#define I2C_PORT    gpioPortB
#define I2C_SDA_PIN 11
#define I2C_SCL_PIN 13

#define RETRY_CNT   2       // try each transfer again if it fails

void i2c_setup() {
    GPIO_PinModeSet(I2C_PORT, I2C_SDA_PIN, gpioModeWiredAndPullUp, 0);
    GPIO_PinModeSet(I2C_PORT, I2C_SCL_PIN, gpioModePushPull, 0);

    I2C_Init_TypeDef i2cInit = I2C_INIT_DEFAULT;
    I2C_Init(I2C0, &i2cInit);
    I2C0->ROUTELOC0 = I2C_ROUTELOC0_SCLLOC_LOC7 | I2C_ROUTELOC0_SDALOC_LOC6;
    I2C0->ROUTEPEN = I2C_ROUTEPEN_SCLPEN | I2C_ROUTEPEN_SDAPEN;
}

static I2C_TransferReturn_TypeDef i2c_transfer(I2C_TransferSeq_TypeDef *seq) {
    uint32 i;
    I2C_TransferReturn_TypeDef ret;
    for (i = 0; i != RETRY_CNT; i++) {
        ret = I2C_TransferInit(I2C0, seq);
        while (ret == i2cTransferInProgress)
            ret = I2C_Transfer(I2C0);
        if (ret == i2cTransferDone)
            break;
    }
    return ret;
}

uint8_t i2c_read_data[16];
uint8_t i2c_write_data[16];
I2C_TransferSeq_TypeDef seq;

I2C_TransferReturn_TypeDef i2cWrite8(uint8_t addr, uint8_t regAddr, uint8_t data) {

    seq.addr = addr;
    seq.flags = I2C_FLAG_WRITE_WRITE;
    /* Select command to issue */
    i2c_write_data[0] = regAddr;
    i2c_write_data[1] = data;
    seq.buf[0].data = i2c_write_data;
    seq.buf[0].len = 2;
    seq.buf[1].len = 0;
    return i2c_transfer(&seq);
}

int32 i2cRead8(uint8_t addr, uint8_t regAddr) {
    I2C_TransferReturn_TypeDef ret;

    seq.addr = addr;
    seq.flags = I2C_FLAG_WRITE_READ;
    /* Select command to issue */
    i2c_write_data[0] = regAddr;
    seq.buf[0].data = i2c_write_data;
    seq.buf[0].len = 1;
    /* Select location/length of data to be read */
    seq.buf[1].data = i2c_read_data;
    seq.buf[1].len = 1;

    ret = i2c_transfer(&seq);
    if (ret != i2cTransferDone) {
        return ret;
    }
    return i2c_read_data[0];
}

int32 i2cRead32(uint8_t addr, uint8_t regAddr) {
    I2C_TransferReturn_TypeDef ret;

    seq.addr = addr;
    seq.flags = I2C_FLAG_WRITE_READ;
    /* Select command to issue */
    i2c_write_data[0] = regAddr;
    seq.buf[0].data = i2c_write_data;
    seq.buf[0].len = 1;
    /* Select location/length of data to be read */
    seq.buf[1].data = i2c_read_data;
    seq.buf[1].len = 4;

    ret = i2c_transfer(&seq);
    if (ret != i2cTransferDone) {
        printf("i2cRead24 failed with %d\n", ret);
        return -1;
    }
    return i2c_read_data[0] + (i2c_read_data[1] << 8) + (i2c_read_data[2] << 16) + (i2c_read_data[3] << 24);
}

int32 i2cRead24(uint8_t addr, uint8_t regAddr) {
    I2C_TransferReturn_TypeDef ret;

    seq.addr = addr;
    seq.flags = I2C_FLAG_WRITE_READ;
    /* Select command to issue */
    i2c_write_data[0] = regAddr;
    seq.buf[0].data = i2c_write_data;
    seq.buf[0].len = 1;
    /* Select location/length of data to be read */
    seq.buf[1].data = i2c_read_data;
    seq.buf[1].len = 3;

    ret = i2c_transfer(&seq);
    if (ret != i2cTransferDone) {
        printf("i2cRead24 failed with %d\n", ret);
        return -1;
    }
    return i2c_read_data[0] + (i2c_read_data[1] << 8) + (i2c_read_data[2] << 16);
}

int32 i2cRead16(uint8_t addr, uint8_t regAddr) {
    I2C_TransferReturn_TypeDef ret;

    seq.addr = addr;
    seq.flags = I2C_FLAG_WRITE_READ;
    /* Select command to issue */
    i2c_write_data[0] = regAddr;
    seq.buf[0].data = i2c_write_data;
    seq.buf[0].len = 1;
    /* Select location/length of data to be read */
    seq.buf[1].data = i2c_read_data;
    seq.buf[1].len = 2;

    ret = i2c_transfer(&seq);
    if (ret != i2cTransferDone) {
        printf("i2cRead16 failed with %d\n", ret);
        return -1;
    }
    return i2c_read_data[0] + (i2c_read_data[1] << 8);
}

void i2cOff() {
    if (found_ltr303)
        ltr303_disable();
}