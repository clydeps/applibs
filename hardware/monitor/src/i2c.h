//
// Created by Clyde Stubbs on 5/1/18.
//

#include <stdint.h>
#include "em_i2c.h"

#ifndef MONITOR_I2C_C_H
#define MONITOR_I2C_C_H

extern void i2c_setup();

extern int32 i2cRead16(uint8_t addr, uint8_t reg);

extern int32 i2cRead8(uint8_t addr, uint8_t reg);

extern int32 i2cRead24(uint8_t addr, uint8_t regAddr);

extern I2C_TransferReturn_TypeDef i2cWrite8(uint8_t addr, uint8_t reg, uint8_t data);

extern bool lps22_detect();

extern int32_t lps22_pressure();
extern void lps22_trigger();

extern double lps22_temperature();

extern bool bmc156_detect();
extern double bmc156_temp();

extern bool hdc2010_detect();

extern double hdc2010_temp(), hdc2010_humidity();

extern void hdc2010_trigger();

extern int16_t ltr303_measure();

extern void ltr303_trigger();
extern void ltr303_disable();
extern void i2cOff();

extern bool ltr303_detect();

extern bool found_ltr303, found_lps22, found_hdc2010, found_bmc156;

#endif //MONITOR_I2C_C_H
