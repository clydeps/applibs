#include <stdint.h>
#include <stdio.h>
#include <gatt_db.h>
#include <native_gecko.h>
#include <io.h>
#include <config.h>
#include <em_device.h>
#include <em_adc.h>
#include <em_gpio.h>
#include <em_rmu.h>
#include <em_emu.h>
#include <em_cryotimer.h>
#include "i2c.h"

#define MAX_CONNECTIONS 1
#define MAX_MTU 96
#define RESET_REQUEST   0x05FA0004      // value to request system reset
#define SERNUM_KEY 0x5000
#define CONFIG_KEY 0x6000
#define NAMEKEY 0x6002
#define MODEKEY 0x6003
#define UNIQUE0 (*(uint32_t *)0xFE081F0)

#define TICK_RATE       8     // soft timer called this often per second.
#define RTCC_RATE       32768
#define TICK_PRESET    (RTCC_RATE/(TICK_RATE))

#define NOTIFY_RATE     10              // update every 10 seconds
#define TIMER_RATE      (TICK_RATE*NOTIFY_RATE)

// offsets into environmentData
#define TEMP_OFFS   0
#define TEMP_LEN    2
#define HUMID_OFFS  (TEMP_OFFS+TEMP_LEN)
#define HUMID_LEN    2
#define PRESSURE_OFFS (HUMID_OFFS+HUMID_LEN)
#define PRESSURE_LEN    4
#define LIGHT_OFFS  (PRESSURE_OFFS+PRESSURE_LEN)
#define LIGHT_LEN    2
#define ENVIRONMENT_LEN (LIGHT_OFFS+LIGHT_LEN)

#define LED_PORT    gpioPortD
#define RED_LED 14
#define GREEN_LED 13

#define GREEN_ON(ticks) (GPIO_PinOutClear(gpioPortD, GREEN_LED), greenTicks=ticks)        // turn on green led
#define GREEN_OFF()     GPIO_PinOutSet(gpioPortD, GREEN_LED)       // turn off green led
#define RED_ON(ticks)   (GPIO_PinOutClear(gpioPortD, RED_LED), redTicks = ticks)        // turn on red led
#define RED_OFF()       GPIO_PinOutSet(gpioPortD, RED_LED)         // turn off red led

typedef void( Func )(void);

#define enterDfu    ((Func **)28)

// operational modes

typedef enum {
    MODE_BROADCAST = 0,     // broadcast telemetry data in advertising packets
    MODE_TIMED,
}   op_mode_t;

static uint32_t wake_time;      // time to stay awake in ticks


// our basic advertising data

static const uint8_t advData[] = {
        0x02, 0x01, 0x06,           // flags
        0x05, 0x02, 0x1A, 0x18, 0x00, 0x18,     // incomplete list of 16 bit uuids - GATT service and ESS
};

#define MANUF_ID    0x472
//#define MODEL_ID    0x10FE

uint8 sernum[21];
uint8 deviceName[21];
uint8 currentConnection;
uint16 packetSize = 20;
uint8_t tickCount;
bool connected;
bool notify_debug, notify_temp, notify_humid, notify_pressure, notify_environment, notify_light;
bool found_ltr303, found_lps22, found_hdc2010, found_bmc156;
uint8_t environmentData[ENVIRONMENT_LEN];
uint16_t sleep_time;
uint16_t redTicks, greenTicks;

uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];
uint doReboot;
bool serviceChangeSent;

/* Gecko configuration parameters (see gecko_configuration.h) */


static const uint8_t DFU_TRIGGER[6] = {'s', 't', 'r', 'o', 'n', 'g'};
static const gecko_configuration_t gecko_configuration = {
        .config_flags=0,
        .sleep.flags=0,
        .bluetooth.max_connections=MAX_CONNECTIONS,
        .bluetooth.heap=bluetooth_stack_heap,
        .bluetooth.heap_size=sizeof(bluetooth_stack_heap),
        .ota.flags=0,
        .ota.device_name_len=0,
        .ota.device_name_ptr="",
        .gattdb=&bg_gattdb_data,
};

const static uint8_t defaultSernum[] = {6, 's', 'e', 'r', 'n', 'u', 'm'};

config_calib_t config_calib;

uint32_t adcRead(void) {
    ADC_Start(ADC0, adcStartSingle);
    while ((ADC0->STATUS & ADC_STATUS_SINGLEDV) == 0) {}
    return ADC_DataSingleGet(ADC0);
}

static int16_t convertToCentiCelsius(int32_t adcSample) {
    uint32_t calTemp0;
    uint32_t calValue0;
    int32_t readDiff;
    float temp;

    /* Factory calibration temperature from device information page. */
    calTemp0 = ((DEVINFO->CAL & _DEVINFO_CAL_TEMP_MASK)
            >> _DEVINFO_CAL_TEMP_SHIFT);

    calValue0 = ((DEVINFO->ADC0CAL3
                  /* _DEVINFO_ADC0CAL3_TEMPREAD1V25_MASK is not correct in
                      current CMSIS. This is a 12-bit value, not 16-bit. */
                  & 0xFFF0)
            >> _DEVINFO_ADC0CAL3_TEMPREAD1V25_SHIFT);

    if ((calTemp0 == 0xFF) || (calValue0 == 0xFFF)) {
        /* The temperature sensor is not calibrated */
        return -100;
    }

    /* Vref = 1250mV
       TGRAD_ADCTH = 1.835 mV/degC (from datasheet)
    */
    readDiff = calValue0 - adcSample;
    temp = ((float) readDiff * 1250);
    temp /= (4096 * -1.835);

    /* Calculate offset from calibration temperature */
    temp = (float) calTemp0 - temp;
    return (int16_t) (temp * 100);
}

static int16_t getLight() {
    if (!found_ltr303)
        return 0;
    int16_t val = (int16_t) (ltr303_measure() * 10);
    environmentData[LIGHT_OFFS + 0] = (uint8_t) (val & 0xFF);
    environmentData[LIGHT_OFFS + 1] = (uint8_t) ((val >> 8) & 0xFF);
    printf("light = %d\n", val);
    return val;

}

static int16_t getPressure() {
    int32_t pressure;
    if (!found_lps22)
        return 0;
    pressure = (int32_t) (lps22_pressure() / 100.0);
    environmentData[PRESSURE_OFFS + 0] = (uint8_t) (pressure & 0xFF);
    environmentData[PRESSURE_OFFS + 1] = (uint8_t) ((pressure >> 8) & 0xFF);
    environmentData[PRESSURE_OFFS + 2] = (uint8_t) ((pressure >> 16) & 0xFF);
    environmentData[PRESSURE_OFFS + 3] = (uint8_t) ((pressure >> 24) & 0xFF);
    printf("Pressure = %d.%02d\n", pressure / 100, pressure % 100);
    return (int16_t) (pressure / 100);
}

static int16_t getHumidity() {
    int16_t humid;
    if (!found_hdc2010)
        return 0;
    humid = (int16_t) (hdc2010_humidity() * 100);
    environmentData[HUMID_OFFS + 0] = (uint8_t) (humid & 0xFF);
    environmentData[HUMID_OFFS + 1] = (uint8_t) ((humid >> 8) & 0xFF);
    printf("Humidity = %d.%02d\n", humid / 100, humid % 100);
    return (int16_t) (humid / 100);
}

static int16_t getTemperature() {
    int16_t temp;
    temp = convertToCentiCelsius(adcRead());
    printf("BGM111 Temp = %d.%02d\n", temp / 100, temp % 100);
    if (found_bmc156) {
        temp = (int16_t) (bmc156_temp() * 100);
        printf("BMC156 Temp = %d.%02d\n", temp / 100, temp % 100);
    }
    if (found_lps22) {
        temp = (int16_t) (lps22_temperature() * 100);
        printf("Lps22 Temp = %d.%02d\n", temp / 100, temp % 100);
    }
    if (found_hdc2010) {
        temp = (int16_t) (hdc2010_temp() * 100);
        printf("HDC2010 Temp = %d.%02d\n", temp / 100, temp % 100);
    }
    environmentData[TEMP_OFFS + 0] = (uint8_t) (temp & 0xFF);
    environmentData[TEMP_OFFS + 1] = (uint8_t) ((temp >> 8) & 0xFF);
    return temp;

}

int8_t MuLaw_Encode(int16_t number) {
    static const uint16_t MULAW_MAX = 0x7FFF;
    static const uint16_t MULAW_BIAS = 33;
    uint16_t mask = 0x4000;
    uint8_t position = 14;
    uint8_t lsb = 0;
    number += MULAW_BIAS;
    if (number > MULAW_MAX)
        number = MULAW_MAX;
    for (; ((number & mask) != mask && position >= 5); mask >>= 1, position--);
    lsb = (uint8_t) ((number >> (position - 4)) & 0x0f);
    return (int8_t) ((((position - 5) << 4) | lsb));
}

static void setAdvData(int16_t degC, int16_t humidPercent, int16_t light) {
    uint8_t data[31], *dp, *cp;

    memcpy(data, advData, sizeof(advData));
    cp = dp = data + sizeof(advData);
    dp++;   // skip size
    *dp++ = 0xFF;               // MSD follows
    *dp++ = MANUF_ID & 0xFF;
    *dp++ = (MANUF_ID >> 8) & 0xFF;
    *dp++ = MODEL_ID & 0xFF;
    *dp++ = (MODEL_ID >> 8) & 0xFF;
    // store temperature into the advertising data
    *dp++ = (uint8_t) (degC & 0xFF);
    *dp++ = (uint8_t) ((degC >> 8) & 0xFF);
    *dp++ = (uint8_t) humidPercent;
    *dp++ = (uint8_t) MuLaw_Encode(light);
    *cp = (uint8_t) (dp - cp - 1);        // store size

    size_t remaining = 31 - sizeof(advData) - 2;

    // Add device name, short or long
    if (deviceName[0] <= remaining) {
        *dp++ = (uint8_t) (deviceName[0] + 1);
        *dp++ = 0x09;
        memcpy(dp, deviceName + 1, deviceName[0]);
        dp += deviceName[0];
    } else {
        *dp++ = (uint8_t) (remaining + 1);
        *dp++ = 0x08;
        memcpy(dp, deviceName + 1, remaining);
        dp += remaining;
    }
    gecko_cmd_le_gap_set_adv_data(0, (uint8) (dp - data), data);
}


static void user_write(struct gecko_cmd_packet *evt) {
    struct gecko_msg_gatt_server_user_write_request_evt_t *writeStatus;
    unsigned i;

    writeStatus = &evt->data.evt_gatt_server_user_write_request;
    printf("Write value: attr=%d, opcode=%d, offset=%d, value:\n",
           writeStatus->characteristic, writeStatus->att_opcode, writeStatus->offset);
    for (i = 0; i != writeStatus->value.len; i++)
        printf("%02X ", writeStatus->value.data[i]);
    printf("\n");
    switch (writeStatus->characteristic) {
        uint8 response;
        case GATTDB_ota_trigger:
            response = 1;
            if (writeStatus->value.len == sizeof(DFU_TRIGGER) &&
                memcmp(writeStatus->value.data, DFU_TRIGGER, sizeof(DFU_TRIGGER)) == 0) {
                response = 0;
                doReboot++;
                (*enterDfu)();
            }
            gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic,
                                                           response);
            break;

        case GATTDB_config:
            if (writeStatus->value.len <= sizeof(config_calib)) {
                memcpy(&config_calib, &writeStatus->value.data, writeStatus->value.len);
                gecko_cmd_flash_ps_save(CONFIG_KEY, sizeof(config_calib), (const uint8 *) &config_calib);
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 0);
            } else
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 1);
            break;

        case GATTDB_devname:
            if (writeStatus->value.len < sizeof(deviceName)) {
                memcpy(deviceName, &writeStatus->value, writeStatus->value.len + 1);
                gecko_cmd_flash_ps_save(NAMEKEY, deviceName[0], deviceName + 1);
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 0);
            } else
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 1);
            break;

        case GATTDB_serialnum:
            if (writeStatus->value.len >= sizeof(sernum) || memcmp(sernum, defaultSernum, sizeof defaultSernum) != 0)
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 1);
            else {
                memcpy(sernum, &writeStatus->value, writeStatus->value.len + 1);
                gecko_cmd_flash_ps_save(SERNUM_KEY, sernum[0], sernum + 1);
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 0);
            }
            break;

        case GATTDB_sleep:
            response = 1;
            if (writeStatus->value.len == sizeof(sleep_time)) {
                response = 0;
                sleep_time = writeStatus->value.data[0] + (writeStatus->value.data[1] << 8);
            }
            gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic,
                                                           response);
            break;


        default:
            gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 1);
            break;

    }
}

static void user_read(struct gecko_msg_gatt_server_user_read_request_evt_t *readStatus) {
    RED_ON(1);
    switch (readStatus->characteristic) {
        case GATTDB_serialnum:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          sernum[0], sernum + 1);
            break;

        case GATTDB_config:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          sizeof(config_calib), (const uint8 *) &config_calib);
            break;
        case GATTDB_devname:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          deviceName[0], (const uint8 *) deviceName + 1);
            break;

        case GATTDB_humidity:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          HUMID_LEN, (const uint8 *) (environmentData + HUMID_OFFS));
            break;

        case GATTDB_sleep:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          sizeof(sleep_time),
                                                          (const uint8 *) &sleep_time);
            break;

        case GATTDB_pressure:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          PRESSURE_LEN,
                                                          (const uint8 *) (environmentData + PRESSURE_OFFS));
            break;

        case GATTDB_temperature:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          TEMP_LEN, (const uint8 *) (environmentData + TEMP_OFFS));
            break;

        case GATTDB_light:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          LIGHT_LEN, (const uint8 *) (environmentData + LIGHT_OFFS));
            break;

        case GATTDB_environment:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          ENVIRONMENT_LEN, (const uint8 *) environmentData);
            break;


        default:
            printf("Read request: connection=%X, characteristic=%d, status_flags=%X, offset=%d\n",
                   readStatus->connection, readStatus->characteristic, readStatus->att_opcode, readStatus->offset);
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 1, 0,
                                                          (const uint8 *) "");
            break;
    }
}

//================================================================================
// ADC0_enter_DefaultMode_from_RESET
//================================================================================
void ADC0_enter_DefaultMode_from_RESET(void) {

    ADC_Init_TypeDef init = ADC_INIT_DEFAULT;

    init.ovsRateSel = adcOvsRateSel2;
    init.warmUpMode = adcWarmupNormal;
    init.timebase = ADC_TimebaseCalc(0);
    init.prescale = ADC_PrescaleCalc(7000000, 0);
    init.tailgate = 0;

    ADC_Init(ADC0, &init);
    // [ADC_Init]$

    // $[ADC_InitSingle]
    ADC_InitSingle_TypeDef initsingle = ADC_INITSINGLE_DEFAULT;

    initsingle.prsSel = adcPRSSELCh0;
    initsingle.acqTime = adcAcqTime32;
    initsingle.reference = adcRef1V25;
    initsingle.resolution = adcRes12Bit;
    initsingle.posSel = adcPosSelTEMP;
    initsingle.negSel = adcNegSelVSS;
    initsingle.diff = 0;
    initsingle.prsEnable = 0;
    initsingle.leftAdjust = 0;
    initsingle.rep = 0;

    /* Initialize a single sample conversion.
     * To start a conversion, use ADC_Start().
     * Conversion result can be read with ADC_DataSingleGet(). */
    ADC_InitSingle(ADC0, &initsingle);
}


bool characStatus(struct gecko_msg_gatt_server_characteristic_status_evt_t *status) {
    if (status->status_flags == gatt_server_client_config) {
        bool enable = status->client_config_flags != gatt_disable;
        printf("Notification %s on charac %d\n", enable ? "enabled" : "disabled", status->characteristic);
        switch (status->characteristic) {
            case GATTDB_environment:
                notify_environment = enable;
                break;

            case GATTDB_light:
                notify_light = enable;
                break;

            case GATTDB_debug:
                notify_debug = enable;
                break;

            case GATTDB_pressure:
                notify_pressure = enable;
                break;

            case GATTDB_humidity:
                notify_humid = enable;
                break;

            case GATTDB_temperature:
                notify_temp = enable;
                break;

            default:
                return false;
        }
    }
    return true;
}

void clearNotify() {
    notify_debug = false;
    notify_temp = false;
    notify_pressure = false;
    notify_humid = false;
    notify_environment = false;
}

void triggerMeasurement() {
    printf("triggering\n");
    GREEN_ON(1);
    if (found_hdc2010)
        hdc2010_trigger();
    if (found_ltr303)
        ltr303_trigger();
    if (found_lps22)
        lps22_trigger();
}

void sendNotifications() {
    if (!(notify_environment || notify_humid || notify_pressure || notify_light || notify_temp))
        return;
    RED_ON(1);
    if ((notify_environment || notify_light) && found_ltr303)
        getLight();
    if ((notify_environment || notify_pressure) && found_lps22)
        getPressure();
    if ((notify_environment || notify_humid) && found_hdc2010)
        getHumidity();
    if ((notify_environment || notify_temp) && (found_hdc2010 || found_lps22))
        getTemperature();
    if (notify_pressure)
        gecko_cmd_gatt_server_send_characteristic_notification(currentConnection,
                                                               GATTDB_humidity,
                                                               PRESSURE_LEN,
                                                               (const uint8 *) (environmentData +
                                                                                PRESSURE_OFFS));
    if (notify_humid)
        gecko_cmd_gatt_server_send_characteristic_notification(currentConnection,
                                                               GATTDB_humidity,
                                                               HUMID_LEN,
                                                               (const uint8 *) environmentData +
                                                               HUMID_OFFS);
    if (notify_light)
        gecko_cmd_gatt_server_send_characteristic_notification(currentConnection,
                                                               GATTDB_light,
                                                               LIGHT_LEN,
                                                               (const uint8 *) environmentData +
                                                               LIGHT_OFFS);
    if (notify_environment)
        gecko_cmd_gatt_server_send_characteristic_notification(currentConnection,
                                                               GATTDB_environment,
                                                               ENVIRONMENT_LEN,
                                                               (const uint8 *) environmentData);
    if (notify_temp)
        gecko_cmd_gatt_server_send_characteristic_notification(currentConnection,
                                                               GATTDB_temperature,
                                                               TEMP_LEN,
                                                               (const uint8 *) environmentData +
                                                               TEMP_OFFS);
}

void doSleep() {
    if (!connected && sleep_time != 0) {
        RED_OFF();
        GREEN_OFF();
        i2cOff();
        enterEM4((sleep_time * 1000U) / 1024U);                             // sleep as required
    }
}

// this is the entry point for the main program
void main(void) {
    //EMU_init();   // stack has done?
    uint32 signals;
    /*
    CMU_Select_TypeDef rtccClk = CMU_ClockSelectGet(cmuClock_RTCC);
    printf("rtccClk = %d\n", rtccClk);
     */
    CMU_init();
    GPIO_PinModeSet(LED_PORT, RED_LED, gpioModePushPull, 1);
    GPIO_PinModeSet(LED_PORT, GREEN_LED, gpioModePushPull, 1);
    EMU_UnlatchPinRetention();
    CRYOTIMER_Enable(false);        // reset the timer
    if (!continueSleep())        //
        RED_ON(TICK_RATE * NOTIFY_RATE / 2);
    ADC0_enter_DefaultMode_from_RESET();        // enable ADC to read temp
    gecko_init(&gecko_configuration);
    printf("Stack initialised\n");
    i2c_setup();
    found_hdc2010 = hdc2010_detect();
    found_ltr303 = ltr303_detect();
    found_lps22 = lps22_detect();
    if ((found_bmc156 = bmc156_detect()))
        printf("bmc156 found!\n");
    struct gecko_msg_flash_ps_load_rsp_t *resp = gecko_cmd_flash_ps_load(SERNUM_KEY);
    if (resp->result == 0) {
        memcpy(sernum, &resp->value, resp->value.len + 1);
    } else {
        memcpy(sernum, defaultSernum, sizeof defaultSernum);
    }

    resp = gecko_cmd_flash_ps_load(NAMEKEY);
    if (resp->result == 0)
        memcpy(deviceName, &resp->value, resp->value.len + 1);
    else {
        unsigned int id = DEVINFO->UNIQUEL;
        deviceName[0] = (uint8) (bm_sprintf((char *) (deviceName + 1), "Mon %04X", 0xFFFF & id));
    }

    resp = gecko_cmd_flash_ps_load(CONFIG_KEY);
    if (resp->result == 0 && resp->value.len <= sizeof(config_calib))
        memcpy(&config_calib, &resp->value.data, resp->value.len);

    for (;;) {
        /* Event pointer for handling events */
        struct gecko_cmd_packet *evt;

        evt = gecko_wait_event();

        /* Handle events */
        unsigned id = BGLIB_MSG_ID(evt->header) & ~gecko_dev_type_gecko;
        switch (BGLIB_MSG_ID(evt->header)) {
            struct gecko_msg_gatt_server_characteristic_status_evt_t *status;
            struct gecko_msg_gatt_server_user_read_request_evt_t *readStatus;
            struct gecko_msg_gatt_server_user_write_request_evt_t *write_request;
            struct gecko_msg_le_connection_parameters_evt_t parameters;

            /* This boot event is generated when the system boots up after reset.
             * Here the system is set to start advertising immediately after boot procedure. */
            case gecko_evt_system_boot_id:
                printf("system_boot\n");
                gecko_cmd_hardware_set_soft_timer(TICK_PRESET, 1, 0);

                gecko_cmd_gatt_set_max_mtu(MAX_MTU);
                /* Set advertising parameters. 100ms advertisement interval. All channels used.
             * The first two parameters are minimum and maximum advertising interval, both in
             * units of (milliseconds * 1.6). The third parameter '7' sets advertising on all channels. */
                gecko_cmd_le_gap_set_adv_parameters(1000, 1000, 7);

                // setup the advertising data
                setAdvData(getTemperature(), getHumidity(), getLight());
                getPressure();
                /* Start general advertising and enable connections. */
                gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_undirected_connectable);
                break;

            case gecko_evt_hardware_soft_timer_id:
                if (redTicks != 0)
                    redTicks--;
                if (redTicks == 0) {
                    RED_OFF();
                }
                if (greenTicks)
                    greenTicks--;
                if (greenTicks == 0)
                    GREEN_OFF();
                switch (++tickCount) {
                    default:
                        break;
                    case 1:
                        triggerMeasurement();
                        break;
                    case 2:
                        if (!connected) {
                            setAdvData(getTemperature(), getHumidity(), getLight());
                            getPressure();
                        } else
                            sendNotifications();
                        break;
                    case TIMER_RATE:
                        tickCount = 0;
                        doSleep();
                        break;
                }
                if (doReboot > 1) {
                    SCB->AIRCR = RESET_REQUEST;
                    for (;;);
                }
                break;

            case gecko_evt_le_connection_opened_id:
                gecko_cmd_gatt_set_max_mtu(MAX_MTU);    // don't think this works
                currentConnection = evt->data.evt_le_connection_opened.connection;
                gecko_cmd_le_connection_set_parameters(currentConnection, 6, 30, 0, 200);
                printf("Connection opened\n");
                connected = true;
                break;

            case gecko_evt_le_connection_parameters_id:
                parameters = evt->data.evt_le_connection_parameters;
                printf("Connection parameters: interval=%d ms, latency=%d, timeout=%d ms\n",
                       (int) (parameters.interval * 1.25f),
                       parameters.latency, parameters.timeout * 10);
                break;

            case gecko_evt_le_connection_closed_id:
                printf("Connection closed\n");
                clearNotify();
                if (doReboot) {
                    SCB->AIRCR = RESET_REQUEST;
                    for (;;);
                }
                doSleep();
                /* Restart advertising after client has disconnected */
                connected = false;
                setAdvData(getTemperature(), getHumidity(), getLight());
                gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_undirected_connectable);
                break;

            case gecko_evt_gatt_server_characteristic_status_id:
                status = &evt->data.evt_gatt_server_characteristic_status;
                /*
                // service changed notification will be sent once if requested on power up.
                if (status->status_flags == gatt_server_client_config) {
                    bool enable = status->client_config_flags != gatt_disable;
                    if (!serviceChangeSent && enable && status->characteristic == GATTDB_service_changed_char) {
                        gecko_cmd_gatt_server_send_characteristic_notification(currentConnection,
                                                                               GATTDB_service_changed_char, 4,
                                                                               serviceChangedData);
                        printf("Sent serviceChanged\n");
                        serviceChangeSent = true;
                    }
                }*/
                characStatus(status);
                break;

            case gecko_evt_gatt_server_user_read_request_id:
                user_read(&evt->data.evt_gatt_server_user_read_request);
                break;

            case gecko_evt_gatt_server_user_write_request_id:
                write_request = &evt->data.evt_gatt_server_user_write_request;
                printf("Write request: connection=%X, characteristic=%d, status_flags=%X, offset=%d\n",
                       write_request->connection, write_request->characteristic, write_request->att_opcode,
                       write_request->offset);
                user_write(evt);
                break;

            case gecko_evt_gatt_mtu_exchanged_id:
                printf("MTU exchanged: %d\n", evt->data.evt_gatt_mtu_exchanged.mtu);
                packetSize = (uint16) (evt->data.evt_gatt_mtu_exchanged.mtu - 3);
                break;

            case gecko_evt_system_external_signal_id:
                signals = evt->data.evt_system_external_signal.extsignals;
                break;

            default:
#if DEBUG
                if (id != (gecko_evt_hardware_soft_timer_id & ~gecko_dev_type_gecko)) {
                    if (id & gecko_msg_type_evt) {
                        id &= ~gecko_msg_type_evt;
                        printf("event = %X\n", id);
                    } else if (id & gecko_msg_type_rsp) {
                        id &= ~gecko_msg_type_rsp;
                        printf("response = %X\n", id);
                    }
                }
#endif
                break;
        }
    }
}


void exit(int i) {
    SCB->AIRCR = RESET_REQUEST;
    for (;;);
}
