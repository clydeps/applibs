//
// Created by Clyde Stubbs on 5/1/18.
//

#include <stddef.h>
#include <stdbool.h>
#include <io.h>
#include <config.h>
#include "em_i2c.h"
#include "i2c.h"

#define BMC156_ACC_ADDRESS      (0x11 << 1)
#define BMC156_ACC_ADDRESS0     (0x10 << 1)
#define BMC156_ACC_IDREG        0x00
#define BMC156_ACC_TEMPREG      0x08
#define BMC156_ACC_MODE_CTRL    0x11

#define BMC156_MAG_ADDRESS      (0x13 << 1)
#define BMC156_MAG_IDREG        0x40
#define BMC156_PCNTL            0x4B

#define BMC156_PARTID           0xFA


bool bmc156_detect() {
    int ret;
    ret = i2cRead8(BMC156_ACC_ADDRESS, BMC156_ACC_IDREG);
    printf("Read %X from id reg (%X) \n", ret, BMC156_ACC_ADDRESS);
    return ret == BMC156_PARTID;
}

double bmc156_temp() {
    int ret = i2cRead8(BMC156_ACC_ADDRESS, BMC156_ACC_TEMPREG);
    if(ret == -1)
        return 32786.0;
    return (signed char)ret / 2.0 + 23.0 + config_calib.lps22_temp_offset / 10.0;
}


