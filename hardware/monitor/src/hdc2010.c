//
// Created by Clyde Stubbs on 5/1/18.
//

#include <stddef.h>
#include <stdbool.h>
#include <io.h>
#include <config.h>
#include "em_i2c.h"
#include "i2c.h"

#define HDC2010_I2CADDR       0x80
#define HDC2010_TEMP_LO       0x00
#define HDC2010_TEMP_HI       0x01
#define HDC2010_HUMID_LO      0x02
#define HDC2010_HUMID_HI      0x03
#define HDC2010_IRQ_CONF      0x04
#define HDC2010_TEMP_MAX      0x05
#define HDC2010_HUMID_MAX     0x06
#define HDC2010_IRQ_MASK      0x07
#define HDC2010_TEMP_ADJ      0x08
#define HDC2010_HUMID_ADJ     0x09
#define HDC2010_RESCONF       0x0E
#define HDC2010_CONFIG        0x0F

#define HDC2010_MANUFID_LO    0xFC
#define HDC2010_MANUFID_HI    0xFD
#define HDC2010_DEVICEID_LO   0xFE
#define HDC2010_DEVICEID_HI   0xFF

#define HDC2010_TRIGGER       0x01      // trigger measurement, 9 bit res

#define HDC2010_MANUF_ID      0x5449
#define HDC2010_DEVICE_ID     0x07D0


bool hdc2010_detect() {

    int ret = i2cRead16(HDC2010_I2CADDR, HDC2010_MANUFID_LO);
    if (ret != HDC2010_MANUF_ID) {
        printf("Wrong manuf id for hdc2010 - expected %X, received %X\n", HDC2010_MANUF_ID, ret);
        return false;
    }
    ret = i2cRead16(HDC2010_I2CADDR, HDC2010_DEVICEID_LO);
    if (ret != HDC2010_DEVICE_ID) {
        printf("Wrong device id for hdc2010 - expected %X, received %X\n", HDC2010_DEVICE_ID, ret);
        return false;
    }
    printf("Detected HDC2010 on I2C bus\n");
    return true;
}

double hdc2010_humidity() {
    int ret = i2cRead16(HDC2010_I2CADDR, HDC2010_HUMID_LO);
    if (ret < 0) {
        printf("Humidity read failed\n");
        return 32768.0;
    }
    return (double) ret / 65536.0 * 100;
}

double hdc2010_temp() {
    int ret = i2cRead16(HDC2010_I2CADDR, HDC2010_TEMP_LO);
    if (ret < 0) {
        printf("Temp read failed\n");
        return 32768.0;
    }
    return (double) ret / 65536.0 * 165 - 40 + config_calib.hdc2010_temp_offset / 10.0;
}

void hdc2010_trigger() {
    if (i2cWrite8(HDC2010_I2CADDR, HDC2010_RESCONF, 0) != i2cTransferDone)
        printf("write hdc_resconf failed\n");     // ensure heater is off
    if (i2cWrite8(HDC2010_I2CADDR, HDC2010_CONFIG, HDC2010_TRIGGER) != i2cTransferDone)
        printf("write hdc_trigger failed\n");     // ensure heater is off
}
