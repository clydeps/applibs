//
// Created by Clyde Stubbs on 1/2/19.
//

#ifndef MONITOR_CONFIG_H
#define MONITOR_CONFIG_H

// config/calibration data structure

typedef struct __attribute__((packed)) config_calib_t {
    // temperature offsets are in 0.1 degree units
    signed char bmc156_temp_offset;
    signed char hdc2010_temp_offset;
    signed char lps22_temp_offset;
    // pressure offsets is in 0.1 hPa units
    signed char pressure_offset;
} config_calib_t;


extern config_calib_t config_calib;

#endif //MONITOR_CONFIG_H
