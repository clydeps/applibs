//
// Created by Clyde Stubbs on 13/2/19.
//

#include "serial_flash.h"
#include <em_gpio.h>
#include <em_usart.h>
#include <spidrv.h>
#include <dmadrv.h>
#include <io.h>

// Command bytes for the serial flash S25FL164K

#define JEDEC_ID_CMD    0x9F


#define CS_PORT gpioPortC
#define CS_PIN  7           // chip select output

#define WP_PORT      gpioPortC  // write protect port
#define WP_PIN      6           // write protect output

#define HOLD_PORT      gpioPortC  // write protect port
#define HOLD_PIN      11           // write protect output

#define SO_PORT      gpioPortC  // Serial output port (RX to us)
#define SO_PIN       8           // Serial output pin

#define SI_PORT      gpioPortC  // serial input port (TX to us)
#define SI_PIN       9           // serial input (to ROM)

#define CLK_PORT      gpioPortC  // clock port
#define CLK_PIN       10           // clock (to ROM)

static SPIDRV_HandleData_t handleData;
static SPIDRV_Handle_t handle = &handleData;


void initSrom(void) {
    GPIO_PinModeSet(WP_PORT, WP_PIN, gpioModePushPull, 1);
    GPIO_PinModeSet(HOLD_PORT, HOLD_PIN, gpioModePushPull, 1);

    SPIDRV_Init_t initData = SPIDRV_MASTER_USART0;
    initData.portLocationCs = 9;    // PC7
    initData.portLocationClk = 13;    // PC10
    initData.portLocationRx = 12;    // PC8
    initData.portLocationTx = 14;    // PC9
    SPIDRV_Init(handle, &initData);
    uint8_t result[4];
    uint8_t tx_data[4];

    tx_data[0] = JEDEC_ID_CMD;
    tx_data[1] = 0;
    tx_data[2] = 0;
    tx_data[3] = 0;

    SPIDRV_MTransferB(handle, &tx_data, &result, 4);

    printf("Read bytes from the flash ROM: %X %X %X %X\n",
           result[0], result[1], result[2], result[3]);

}

void GPIO_EVEN_IRQHandler() {
    printf("GPIO Even IRQ\n");
    GPIO_IntClear(GPIO_IntGet());
}
void GPIO_ODD_IRQHandler() {
    printf("GPIO Odd IRQ\n");
    GPIO_IntClear(GPIO_IntGet());
}


