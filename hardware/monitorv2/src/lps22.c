//
// Created by Clyde Stubbs on 5/1/18.
//

#include <stddef.h>
#include <stdbool.h>
#include <io.h>
#include <config.h>
#include "em_i2c.h"
#include "i2c.h"

#define LPS22_I2CADDR      0xB8
#define LPS22_CONTROL      0x80
#define LPS22_RATE         0x85
#define LPS22_RATE         0x85

#define LPS22_IDREG        0x0F
#define LPS22_PARTID       0xB1
#define LPS22_CTRL_REG1    0x10
#define LPS22_CTRL_REG2    0x11
#define LPS22_PRESS_XLO    0x28
#define LPS22_PRESS_LO     0x29
#define LPS22_PRESS_HI     0x2A
#define LPS22_TEMP_LO      0x2B
#define LPS22_TEMP_HI      0x2C


bool lps22_detect() {

    int ret = i2cRead8(LPS22_I2CADDR, LPS22_IDREG);
    if (ret != LPS22_PARTID) {
        printf("Wrong manuf id for lps22 - expected %X, received %X\n", LPS22_PARTID, ret);
        return false;
    }
    printf("Detected LPS22 on I2C bus\n");
    return true;
}

void lps22_trigger() {
    i2cWrite8(LPS22_I2CADDR, LPS22_CTRL_REG1, 0x00);    // default state
    i2cWrite8(LPS22_I2CADDR, LPS22_CTRL_REG2, 0x11);    // auto inc, trigger one-shot
}

double lps22_temperature() {
    return (int16_t) i2cRead16(LPS22_I2CADDR, LPS22_TEMP_LO) / 100.0 + config_calib.lps22_temp_offset / 10.0;
}

int32_t lps22_pressure() {
    int32_t val =  i2cRead24(LPS22_I2CADDR, LPS22_PRESS_XLO);
    if(val < 0)
        return val;
    return (int32_t) ((double)val / 40.96);     // LSB is 1/4096 hPa, result is in units of 0.01Pa.
}


