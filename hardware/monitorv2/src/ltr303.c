//
// Created by Clyde Stubbs on 5/1/18.
//

#include <stddef.h>
#include <stdbool.h>
#include <io.h>
#include "em_i2c.h"
#include "i2c.h"

#define LTR303_I2CADDR      0x52
#define LTR303_CONTROL      0x80
#define LTR303_RATE         0x85

#define LTR303_PARTID       0x86
#define LTR303_MANUFID      0x87
#define LTR303_DATA0_LO     0x88
#define LTR303_DATA0_HI     0x89
#define LTR303_DATA1_LO     0x8A
#define LTR303_DATA1_HI     0x8B
#define LTR303_STATUS       0x8C
#define LTR303_INTERRUPT    0x8F

#define LTR303_MANUF_ID      0x05
#define LTR303_DEVICE_ID     0xA0


bool ltr303_detect() {

    int ret = i2cRead8(LTR303_I2CADDR, LTR303_MANUFID);
    if (ret != LTR303_MANUF_ID) {
        printf("Wrong manuf id for ltr303 - expected %X, received %X\n", LTR303_MANUF_ID, ret);
        return false;
    }
    ret = i2cRead8(LTR303_I2CADDR, LTR303_PARTID);
    if (ret != LTR303_DEVICE_ID) {
        printf("Wrong device id for ltr303 - expected %X, received %X\n", LTR303_DEVICE_ID, ret);
        return false;
    }
    printf("Detected LTR303 on I2C bus\n");
    return true;
}

int16_t ltr303_measure() {
    int32_t val = i2cRead16(LTR303_I2CADDR, LTR303_DATA1_LO);
    if(val < 0)
        printf("Read ltr303 failed\n");
    return (int16_t) val;
}

void ltr303_trigger() {
    I2C_TransferReturn_TypeDef ret = i2cWrite8(LTR303_I2CADDR, LTR303_CONTROL, 0x01);  // Gain 1X
    if(ret != i2cTransferDone) {
        printf("ltr303 control write failed\n");
        return;
    }
    ret = i2cWrite8(LTR303_I2CADDR, LTR303_RATE, 0x03);  // 500ms measurement rate
    if(ret != i2cTransferDone) {
        printf("ltr303 control write failed\n");
        return;
    }
}

void ltr303_disable() {
    i2cWrite8(LTR303_I2CADDR, LTR303_CONTROL, 0x00);  // standby mode
}


