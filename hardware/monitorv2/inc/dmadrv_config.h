//
// Created by Clyde Stubbs on 13/2/19.
//

#ifndef MONITOR_DMADRV_CONFIG_H
#define MONITOR_DMADRV_CONFIG_H

#define EMDRV_DMADRV_DMA_IRQ_PRIORITY 4
#define EMDRV_DMADRV_DMA_CH_PRIORITY 0
#define EMDRV_DMADRV_DMA_CH_COUNT 4

/// DMADRV channel count configuration option.
/// Number of DMA channels to support. A lower DMA channel count will reduce
/// ram memory footprint.


#endif //MONITOR_DMADRV_CONFIG_H
