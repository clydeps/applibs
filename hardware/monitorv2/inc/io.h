//
// Created by Clyde Stubbs on 2/12/17.
//

#ifndef MONITOR_IO_H_H
#define MONITOR_IO_H_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "bg_types.h"

#define TIMER_KEY 0x67EF78DE

extern bool connected;
extern bool notify_debug;
extern uint8_t currentConnection;
extern uint16 packetSize;

extern void ioFlush(void);
extern void startCryo(uint32_t duration);
extern void enterEM4(uint32_t duration);
extern bool continueSleep();
extern int bm_printf(const char * f, ...);
extern int bm_sprintf(char * wh, const char * f, ...);

#if DEBUG
#include <SEGGER_RTT.h>
#define printf(...) bm_printf(__VA_ARGS__)
#else
#define printf(...) (notify_debug ? bm_printf(__VA_ARGS__) : 0)
#endif

#endif //MONITOR_IO_H_H
