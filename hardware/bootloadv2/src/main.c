#include <stdint.h>
#include <stdio.h>
#include <gatt_db.h>
#include <SEGGER_RTT.h>
#include <native_gecko.h>
#include <io.h>
#include <dfu.h>
#include <em_device.h>
#include <bg_types.h>
#include <aat_def.h>
#include <em_crypto.h>
#include <selftest.h>
#include <em_gpio.h>
#include <init_mcu.h>
#include <em_rmu.h>
#include "gecko_configuration.h"
#include "native_gecko.h"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmain-return-type"
#define MAX_CONNECTIONS        1   // we only talk to one device at a time
// to allow for AES decryption and flash programming

#define AAT_VALUE   ((uint32_t)&__dfu_AAT)          // word value of AAT address
#define RESET_REQUEST   0x05FA0004      // value to request system reset
#define SERNUM_KEY 0x5000
#define NAMEKEY 0x6002
#define RTCC_RATE       32768
#define TICK_RATE       2      // this many ticks per second
#define TICKS_PER_SEC    (RTCC_RATE/(TICK_RATE))     // soft timer called 8 times per second.
#define MANUF_ID    0x472

uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];
extern uint32_t __dfu_AAT;                          // our AAT address
unsigned char deKey[KEY_LEN];
uint8 currentConnection;

#define DIGIT(x, div)   (((x)/(div)) % 10 + '0')
#define DIGITS(x) DIGIT(x, 1000), DIGIT(x, 100), DIGIT(x, 10), DIGIT(x, 1)
struct {
    uint8 len;
    uint8 data[31];
} sernum = {
        6, 's', 'e', 'r', 'n', 'u', 'm'
};
struct {
    uint8 len;
    uint8 data[31];
} deviceName = {
        8, DIGITS(MODEL_NUMBER), ' ', 'O', 'T', 'A'
};
unsigned int testsRequested, testsFailed, testsSucceeded;
bool notify_testResult;
bool toggle;    // led toggle


static const uint8_t advData[] = {
        0x02, 0x01, 0x06,           // flags
        0x05, 0x03, 0x02, 0x18, 0x00, 0x18,     // complete list of 16 bit uuids
        0x05, 0xFF, MANUF_ID & 0xFF, (MANUF_ID >> 8) & 0xFF, MODEL_NUMBER & 0xFF,
        (MODEL_NUMBER >> 8) & 0xFF,     // MSD, our ID, model FFFF
};


/* Gecko configuration parameters (see gecko_configuration.h) */

static const gecko_configuration_t config = {
        .config_flags = 0,
        .sleep.flags = 0,
        .bluetooth.max_connections = MAX_CONNECTIONS,
        .bluetooth.heap = bluetooth_stack_heap,
        .bluetooth.heap_size = sizeof(bluetooth_stack_heap),
        .ota.flags = 0,
        .gattdb = &bg_gattdb_data,
};
#define lockBits        ((uint32_t *)LOCKBITS_BASE)

#define DLW             lockBits[127]                   // debug lock word



// expected first 4 words of stack AAT

static const uint32_t stack_aat[] = {0x20007BF8, 0x00020A4B, 0x0000CE87, 0x0000F393};

// random bytes

static const uint8_t dfu_key[] = {0xCE, 0x8D, 0xC1, 0x1F, 0x37, 0x7B, 0xB1, 0x9A,
                                  0x79, 0xF5, 0xE1, 0x44, 0x8C, 0xC9, 0xAD, 0x57};
// copy of random bytes
static uint8_t dfu_key_copy[sizeof(dfu_key)];
bool enterDfu;        // set if we should enter dfu mode


/**
 * This function is called from the application program to flag that we should enter DFU mode
 * after the next non-power on reset. It is accessed through
 * an entry in the vector table, indexed at ENTER_DFU_VECTOR.
 */
void EnterDFU_Handler(void) {
    memcpy(dfu_key_copy, dfu_key, sizeof(dfu_key_copy));
}

static void user_read(struct gecko_msg_gatt_server_user_read_request_evt_t *readStatus) {
    switch (readStatus->characteristic) {
        case GATTDB_serialnum:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          sernum.len, sernum.data);
            break;
        case GATTDB_devname:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          deviceName.len, deviceName.data);
            break;
        default:
            printf("Read request: connection=%X, characteristic=%d, status_flags=%X, offset=%d\n",
                   readStatus->connection, readStatus->characteristic, readStatus->att_opcode,
                   readStatus->offset);
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 1,
                                                          0,
                                                          (const uint8 *) "");
            break;
    }
}

static void check_error(char *msg, uint16 i) {
    if (i != 0)
        printf("Error %d for %s\n", i, msg);
}

static void setAdvData(void) {
    uint8_t data[31], *dp, *cp;

    memcpy(data, advData, sizeof(advData));
    cp = dp = data + sizeof(advData);
    dp++;   // skip size
    *dp++ = 0xFF;               // MSD follows
    *dp++ = MANUF_ID & 0xFF;
    *dp++ = (MANUF_ID >> 8) & 0xFF;
    *dp++ = MODEL_NUMBER & 0xFF;
    *dp++ = (MODEL_NUMBER >> 8) & 0xFF;
    *cp = (uint8_t) (dp - cp - 1);        // store size

    size_t remaining = 31 - sizeof(advData) - 2;

    // Add device name, short or long
    if (deviceName.len <= remaining) {
        *dp++ = (uint8_t) (deviceName.len + 1);
        *dp++ = 0x09;
        memcpy(dp, deviceName.data, deviceName.len);
        dp += deviceName.len;
    } else {
        *dp++ = (uint8_t) (remaining + 1);
        *dp++ = 0x08;
        memcpy(dp, deviceName.data, remaining);
        dp += remaining;
    }
    gecko_cmd_le_gap_bt5_set_adv_data(0, 0, (uint8) (dp - data), data);
}

static void user_write(struct gecko_msg_gatt_server_user_write_request_evt_t *writeStatus) {

    uint8 response;

    /*
    unsigned i;
    printf("Write value: attr=%d, opcode=%d, offset=%d, value:\n",
           writeStatus->characteristic, writeStatus->att_opcode, writeStatus->offset);
    for (i = 0; i != writeStatus->value.len; i++)
        printf("%02X ", writeStatus->value.data[i]);
    printf("\n");
        */
    switch (writeStatus->characteristic) {
        case GATTDB_test_start:
            if (writeStatus->value.len != 4) {
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection,
                                                               writeStatus->characteristic, 0);
                break;
            }
            testsRequested = writeStatus->value.data[0] +
                             (writeStatus->value.data[1] << 8) +
                             (writeStatus->value.data[2] << 16) +
                             (writeStatus->value.data[3] << 24);

            printf("Received test request %X\n", testsRequested);
            testsFailed = 0;
            testsSucceeded = 0;
            // reply ok if only tests we know about were requested
            if ((testsRequested & ~(TEST_MASK)) != 0) {
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection,
                                                               writeStatus->characteristic, 1);
                testsRequested = 0;
                printf("Mask test failed\n");
            } else
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection,
                                                               writeStatus->characteristic, 0);

            break;


        case GATTDB_ota_control:
            response = (uint8) (processCtrlPacket(writeStatus->value.data) ? 0 : 1);
            gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic,
                                                           response);
            break;

        case GATTDB_ota_data:
            // only respond if the command fails
            if (!processDataPacket(writeStatus->value.data, writeStatus->value.len))
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection,
                                                               writeStatus->characteristic, 1);
            break;

        default:
            printf("Bad write - handle %d\n", writeStatus->characteristic);
            gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic,
                                                           1);
            break;

        case GATTDB_devname: {
            if (writeStatus->value.len < sizeof(deviceName.data)) {
                memcpy(deviceName.data, writeStatus->value.data, writeStatus->value.len);
                deviceName.len = writeStatus->value.len;
                gecko_cmd_flash_ps_save(NAMEKEY, deviceName.len, deviceName.data);
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection,
                                                               writeStatus->characteristic, 0);
            } else
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection,
                                                               writeStatus->characteristic, 1);
            break;
        }

        case GATTDB_serialnum:
            if (writeStatus->value.len < sizeof(sernum.data)) {
                memcpy(sernum.data, writeStatus->value.data, writeStatus->value.len);
                sernum.len = writeStatus->value.len;
                gecko_cmd_flash_ps_save(SERNUM_KEY, sernum.len, sernum.data);
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection,
                                                               writeStatus->characteristic, 0);
            } else
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection,
                                                               writeStatus->characteristic, 1);
            break;
    }

}

void runTests() {
    if (testsRequested & TEST_GPIO) {
        printf("Running gpio test\n");
        if (testGPIO())
            testsSucceeded |= TEST_GPIO;
        else
            testsFailed |= TEST_GPIO;
        testsRequested &= ~TEST_GPIO;
        return;
    }
    if (testsRequested & TEST_SPI) {
        printf("Running SPI test\n");
        if (testSPI_ROM())
            testsSucceeded |= TEST_SPI;
        else
            testsFailed |= TEST_SPI;
        testsRequested &= ~TEST_SPI;
        return;
    }
    if (testsFailed || testsSucceeded) {
        printf("Sending test result: %X\n", testsSucceeded);
        uint8 result[4];
        result[0] = (uint8) (testsSucceeded & 0xFF);
        result[1] = (uint8) ((testsSucceeded >> 8) & 0xFF);
        result[2] = (uint8) ((testsSucceeded >> 16) & 0xFF);
        result[3] = (uint8) ((testsSucceeded >> 24) & 0xFF);
        testsFailed = 0;
        testsSucceeded = 0;
        gecko_cmd_gatt_server_send_characteristic_notification(currentConnection, GATTDB_test_result,
                                                               sizeof result,
                                                               result);
    }
}

bool characStatus(struct gecko_msg_gatt_server_characteristic_status_evt_t *status) {
    if (status->status_flags == gatt_server_client_config) {
        bool enable = status->client_config_flags != gatt_disable;
        printf("Notification %s on charac %d\n", enable ? "enabled" : "disabled", status->characteristic);
        switch (status->characteristic) {
            case GATTDB_test_result:
                notify_testResult = enable;
                break;

            default:
                return false;
        }
    }
    return true;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"

// this is the entry point for the firmware upgrader
void main(void) {

    /* Enable writing to the flash */

#if !defined(DEBUG)
    // check debug lock
    if(DLW != 0) {
    /* Unlock the MSC */
        uint32 zero = 0;
        MSC_WriteWord(&DLW, &zero, sizeof zero);
    }
#endif

    // for debug purposes, never run the user program

#if defined(DFU_DEBUG)
    enterDfu = true;
#endif
    uint32_t cause = RMU_ResetCauseGet();
    RMU_ResetCauseClear();
    // enter DFU if we got here from anything other than a power on reset, and our key
    // has been copied to RAM
    if (!(cause & RMU_RSTCAUSE_PORST) && memcmp(dfu_key, dfu_key_copy, sizeof(dfu_key)) == 0)
        enterDfu = true;
    memset(dfu_key_copy, 1, sizeof(dfu_key_copy));
    printf("Started OTA_DFU\n");
    if (USER_BLAT->type != APP_APP_ADDRESS_TYPE)
        enterDfu = true;

    if (!enterDfu) {
        SCB->VTOR = (uint32_t) USER_BLAT->vectorTable;
        //Use stack from aat
        asm("mov sp,%0"::"r" (USER_BLAT->topOfStack));
        USER_BLAT->resetVector();
    }
    initMcu();

    GPIO_PinModeSet(gpioPortD, 13, gpioModePushPull, 0);
    GPIO_PinModeSet(gpioPortD, 14, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortD, 15, gpioModeInputPull, 1);
    CRYPTO_AES_DecryptKey256(CRYPTO, deKey, ota_key);
    gecko_init(&config);
    printf("BLE Stack initialised\n");
    struct gecko_msg_flash_ps_load_rsp_t *resp = gecko_cmd_flash_ps_load(SERNUM_KEY);
    if (resp->result == 0) {
        memcpy(sernum.data, resp->value.data, resp->value.len);
        sernum.len = resp->value.len;
    }
    resp = gecko_cmd_flash_ps_load(NAMEKEY);
    if (resp->result == 0) {
        memcpy(deviceName.data, resp->value.data, resp->value.len);
        deviceName.len = resp->value.len;
    }

    gecko_cmd_hardware_set_soft_timer(TICKS_PER_SEC, 1, 0);
    for (;;) {
        /* Event pointer for handling events */
        struct gecko_cmd_packet *evt;
        struct gecko_msg_le_connection_parameters_evt_t *pp;
        struct gecko_msg_gatt_server_characteristic_status_evt_t *status;
        struct gecko_msg_gatt_server_user_read_request_evt_t *readStatus;
        uint16 i;

        /* Check for stack event. */
        evt = gecko_wait_event();

        /* Handle events */
        unsigned id = BGLIB_MSG_ID(evt->header) & ~gecko_dev_type_gecko;
        switch (BGLIB_MSG_ID(evt->header)) {

            case gecko_evt_hardware_soft_timer_id:
                if (notify_testResult)
                    runTests();
                if (toggle) {
                    if (GPIO_PinInGet(gpioPortD, 15) == 0) {
                        printf("Switch depressed\n");
                        GPIO_PinOutClear(gpioPortD, 13);
                    } else
                        GPIO_PinOutSet(gpioPortD, 13);
                    GPIO_PinOutClear(gpioPortD, 14);
                } else {
                    GPIO_PinOutSet(gpioPortD, 14);
                    GPIO_PinOutClear(gpioPortD, 13);
                }
                toggle = !toggle;
                break;


                /* This boot event is generated when the system boots up after reset.
                 * Here the system is set to start advertising immediately after boot procedure. */
            case gecko_evt_system_boot_id:
                printf("Bootloader: system_boot\n");
                gecko_cmd_gatt_set_max_mtu(MAX_MTU);
                int val = gecko_cmd_system_set_tx_power(190)->set_power;
                printf("max TX power now %d\n", val);

                // enable LE Coded advertising, include TX power
                i = gecko_cmd_le_gap_set_advertise_configuration(0, 8)->result;
                check_error("set_advertise_configuration", i);

                i = gecko_cmd_le_gap_set_advertise_phy(0, le_gap_phy_1m, le_gap_phy_coded)->result;
                check_error("set_advertise_phy", i);

                i = gecko_cmd_le_gap_set_advertise_tx_power(0, 190)->result;
                check_error("set_advertise_tx_power", i);

                /* Set advertising parameters. 500ms advertisement interval. All channels used.*/

                gecko_cmd_le_gap_set_advertise_timing(0, 200, 400, 0, 0);
                setAdvData();

                /* Start general advertising and enable connections. */
                gecko_cmd_le_gap_start_advertising(0, le_gap_user_data, le_gap_undirected_connectable);
                break;

            case gecko_evt_le_connection_opened_id:
                printf("Connection opened for a\n");
                currentConnection = evt->data.evt_le_connection_opened.connection;
                break;

            case gecko_evt_le_connection_closed_id:
                notify_testResult = 0;
                testsFailed = testsSucceeded = testsRequested = 0;
                printf("Connection closed\n");
                if (doReset) {
                    printf("Resetting....\n");
                    SCB->AIRCR = RESET_REQUEST;
                } else
                    /* Restart advertising after client has disconnected */
                    gecko_cmd_le_gap_start_advertising(0, le_gap_user_data, le_gap_undirected_connectable);
                break;

            case gecko_evt_gatt_server_characteristic_status_id:
                characStatus(&evt->data.evt_gatt_server_characteristic_status);
                break;

            case gecko_evt_gatt_server_user_read_request_id:
                user_read(&evt->data.evt_gatt_server_user_read_request);
                break;

            case gecko_evt_gatt_server_user_write_request_id:
                user_write(&evt->data.evt_gatt_server_user_write_request);
                break;

            case gecko_evt_le_connection_parameters_id:
                pp = &evt->data.evt_le_connection_parameters;
                printf("Connection parameters: interval %d, latency %d, timeout %d\n",
                       pp->interval, pp->latency, pp->timeout);
                break;

            case gecko_evt_gatt_mtu_exchanged_id:
                printf("MTU exchanged: %d\n", evt->data.evt_gatt_mtu_exchanged.mtu);
                break;


            default:
#if DEBUG
                if (id & gecko_msg_type_evt) {
                    id &= ~gecko_msg_type_evt;
                    printf("unhandled event = %X\n", id);
                } else if (id & gecko_msg_type_rsp) {
                    id &= ~gecko_msg_type_rsp;
                    printf("unhandled response = %X\n", id);
                } else
                    printf("undeciphered message = %X\n", id);
#endif
                break;
        }
    }
}

#pragma clang diagnostic pop

void exit(int i) {
    SCB->AIRCR = RESET_REQUEST;
    for (;;);
}

#pragma clang diagnostic pop