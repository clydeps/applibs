//
// Created by Clyde Stubbs on 20/4/17.
//

#include <selftest.h>
#include <stdbool.h>
#include <em_device.h>
#include <em_gpio.h>
#include <em_usart.h>
#include <em_leuart.h>
#include <em_cmu.h>

#define US0_TX_PIN  22      // selectds PD14
#define US0_RX_PIN  20      // selects PD13
#define US1_TX_PIN  00      // selectds PA0
#define US1_RX_PIN  00      // selects PA1

int waitPin(int val) {
    int i;
    for (i = 2000; --i != 0;)
        if (GPIO_PinInGet(gpioPortF, 3) == val)
            break;
    return i;
}

bool testGPIO() {
    GPIO_PinModeSet(gpioPortF, 3, gpioModeInput, 0);
    if (waitPin(1) == 0)
        return false;
    // wait for a full cycle
    if (waitPin(0) == 0)
        return false;
    if (waitPin(1) == 0)
        return false;
    return waitPin(0) != 0;
}

bool testSPI_ROM() {
    // we haven't written this test yet.
    return true;

}
