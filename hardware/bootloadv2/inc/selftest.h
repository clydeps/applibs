//
// Created by Clyde Stubbs on 20/4/17.
//

#ifndef BMBOOTLOAD_SELFTEST_H
#define BMBOOTLOAD_SELFTEST_H

#include <stdbool.h>

extern bool testSerial0();
extern bool testSerial1();
extern bool testGPIO();
extern bool testSPI_ROM();

#define TEST_SERIAL0    1
#define TEST_SERIAL1    2
#define TEST_GPIO       4
#define TEST_SPI        8
#define TEST_MASK   (TEST_SERIAL0|TEST_SERIAL1|TEST_GPIO|TEST_SPI)

#endif //BMBOOTLOAD_SELFTEST_H
