//
// Created by Clyde Stubbs on 8/3/17.
//

#include <stdint.h>

#ifndef BGM111_IO_H
#define BGM111_IO_H

#endif //BGM111_IO_H


extern void EMU_init(void);
extern void CMU_init(void);
extern int bm_printf(const char * cp, ...);
extern void putch(char c);
extern void ioFlush(void);


extern uint8_t currentConnection;


#if DEBUG
#include <SEGGER_RTT.h>
#define printf(...) bm_printf(__VA_ARGS__)
#else
#define printf(...) (0)
#endif

