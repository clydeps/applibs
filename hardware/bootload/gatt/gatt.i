<?xml version="1.0" encoding="UTF-8"?>

<gatt prefix="GATTDB_" out="gatt_db.c" header="gatt_db.h" generic_attribute_service="true">

    <!-- Generic Access Service -->
    <!-- https://developer.bluetooth.org/gatt/services/Pages/ServiceViewer.aspx?u=org.bluetooth.service.generic_access.xml -->
    <service uuid="1800">

        <!-- Device Name -->
        <!-- https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.gap.device_name.xml -->
        <characteristic uuid="2a00">
            <properties read="true" const="true"/>
            <value>BlueMAX OTA</value>
        </characteristic>

        <!-- Appearance -->
        <!-- https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.gap.appearance.xml -->
        <characteristic uuid="2a01">
            <properties read="true" const="true"/>
            <value type="hex">0000</value>
        </characteristic>

    </service>

    <!-- Device Information Service -->
    <service uuid="180A">
        <!-- Manufacturer name string -->
        <characteristic uuid="2A29">
            <properties read="true" const="true"/>
            <value>Control-J Pty Ltd</value>
        </characteristic>
        <!-- Model number string -->
        <characteristic uuid="2A24">
            <properties read="true" const="true"/>
            <value>4370-4350</value>
        </characteristic>
        <!-- Serial number string -->
        <characteristic uuid="2A25" id="serialnum">
            <properties read="true" write="true"/>
            <value type="user" length="20" variable_length="true"/>
        </characteristic>
        <!-- Hardware revision -->
        <characteristic uuid="2A27" id="hwrev">
            <properties read="true" const="true"/>
            <value>3</value>
        </characteristic>
        <!-- Firmware revision -->
        <characteristic uuid="2A26" id="fwrev">
            <properties read="true" const="true"/>
            <value>2.3</value>
        </characteristic>
    </service>

    <service uuid="C5781BF4-D6F6-11E7-9421-38C98653BB53">
        <description>EFR32BG OTA</description>
        <characteristic uuid="95301001-963F-46B1-B801-0B23E8904835" id="ota_control">
            <properties write="true"/>
            <value length="8" type="user"/>
            <description>OTA CTRL</description>
        </characteristic>
        <characteristic uuid="95301002-963F-46B1-B801-0B23E8904835" id="ota_data">
            <properties write="true" write_no_response="true"/>
            <value type="user" length="64"/>
            <description>OTA DATA</description>
        </characteristic>
        <characteristic uuid="95301003-963F-46B1-B801-0B23E8904835" id="ota_progress">
            <properties notify="true" />
            <value type="user" length="16"/>
            <description>OTA Progress</description>
        </characteristic>
        <characteristic uuid="95301004-963F-46B1-B801-0B23E8904835" id="test_start">
            <properties write="true" />
            <value type="user" length="4"/>
            <description>Start test</description>
        </characteristic>
        <characteristic uuid="95301005-963F-46B1-B801-0B23E8904835" id="test_result">
            <properties notify="true" />
            <value type="user" length="4"/>
            <description>Test result</description>
        </characteristic>
    </service>
</gatt>
