#include <stdint.h>
#include <stdio.h>
#include <gatt_db.h>
#include <SEGGER_RTT.h>
#include <native_gecko.h>
#include <io.h>
#include <dfu.h>
#include <em_device.h>
#include <bg_types.h>
#include <aat_def.h>
#include <em_crypto.h>
#include <selftest.h>
#include <em_gpio.h>
#include "gecko_configuration.h"
#include "native_gecko.h"

#define MAX_CONNECTIONS        1   // we only talk to one device at a time
// to allow for AES decryption and flash programming

#define AAT_VALUE   ((uint32_t)&__dfu_AAT)          // word value of AAT address
#define RESET_REQUEST   0x05FA0004      // value to request system reset
#define SERNUM_KEY 0x5000
#define RTCC_RATE       32768
#define TICK_RATE       2      // this many ticks per second
#define TICKS_PER_SEC    (RTCC_RATE/(TICK_RATE))     // soft timer called 8 times per second.

uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];
extern uint32_t __dfu_AAT;                          // our AAT address
unsigned char deKey[KEY_LEN];
uint8 currentConnection;
const static uint8_t defaultSernum[] = {6, 's', 'e', 'r', 'n', 'u', 'm'};
uint8 sernum[21];
unsigned int testsRequested, testsFailed, testsSucceeded;
bool notify_testResult;
bool toggle;    // led toggle


static const uint8_t advData[] = {
        0x02, 0x01, 0x06,           // flags
        0x05, 0x03, 0x02, 0x18, 0x00, 0x18,     // complete list of 16 bit uuids
        0x05, 0xFF, 0x72, 0x04, 0xFF, 0xFF,     // MSD, our ID, model FFFF
        12, 0x09, 'M', 'o', 'n', 'i', 't', 'o', 'r', ' ', 'O', 'T', 'A'     // device name
};


/* Gecko configuration parameters (see gecko_configuration.h) */

static const gecko_configuration_t config = {
        .config_flags=0,
        .sleep.flags=0,
        .bluetooth.max_connections=MAX_CONNECTIONS,
        .bluetooth.heap=bluetooth_stack_heap,
        .bluetooth.heap_size=sizeof(bluetooth_stack_heap),
        .ota.flags=0,
        .ota.device_name_len=3,
        .ota.device_name_ptr="OTA",
        .gattdb=&bg_gattdb_data,
};

static void user_read(struct gecko_msg_gatt_server_user_read_request_evt_t *readStatus) {
    switch (readStatus->characteristic) {
        case GATTDB_serialnum:
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 0,
                                                          sernum[0], sernum + 1);
            break;
        default:
            printf("Read request: connection=%X, characteristic=%d, status_flags=%X, offset=%d\n",
                   readStatus->connection, readStatus->characteristic, readStatus->att_opcode, readStatus->offset);
            gecko_cmd_gatt_server_send_user_read_response(readStatus->connection, readStatus->characteristic, 1, 0,
                                                          (const uint8 *) "");
            break;
    }
}


static void user_write(struct gecko_msg_gatt_server_user_write_request_evt_t *writeStatus) {

    uint8 response;

    /*
    unsigned i;
    printf("Write value: attr=%d, opcode=%d, offset=%d, value:\n",
           writeStatus->characteristic, writeStatus->att_opcode, writeStatus->offset);
    for (i = 0; i != writeStatus->value.len; i++)
        printf("%02X ", writeStatus->value.data[i]);
    printf("\n");
        */
    switch (writeStatus->characteristic) {
        case GATTDB_test_start:
            if (writeStatus->value.len != 4) {
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 0);
                break;
            }
            testsRequested = writeStatus->value.data[0] +
                             (writeStatus->value.data[1] << 8) +
                             (writeStatus->value.data[2] << 16) +
                             (writeStatus->value.data[3] << 24);

            printf("Received test request %X\n", testsRequested);
            testsFailed = 0;
            testsSucceeded = 0;
            // reply ok if only tests we know about were requested
            if ((testsRequested & ~(TEST_MASK)) != 0) {
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 1);
                testsRequested = 0;
                printf("Mask test failed\n");
            } else
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 0);

            break;


        case GATTDB_ota_control:
            response = (uint8) (processCtrlPacket(writeStatus->value.data) ? 0 : 1);
            gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic,
                                                           response);
            break;

        case GATTDB_ota_data:
            // only respond if the command fails
            if (!processDataPacket(writeStatus->value.data, writeStatus->value.len))
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 1);
            break;

        default:
            printf("Bad write - handle %d\n", writeStatus->characteristic);
            gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 1);
            break;

        case GATTDB_serialnum:
            if (writeStatus->value.len >= sizeof(sernum) || memcmp(sernum, defaultSernum, sizeof defaultSernum) != 0)
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 1);
            else {
                memcpy(sernum, &writeStatus->value, writeStatus->value.len + 1);
                gecko_cmd_flash_ps_save(SERNUM_KEY, sernum[0], sernum+1);
                gecko_cmd_gatt_server_send_user_write_response(writeStatus->connection, writeStatus->characteristic, 0);
            }
            break;
    }

}

void runTests() {
    if (testsRequested & TEST_GPIO) {
        printf("Running gpio test\n");
        if (testGPIO())
            testsSucceeded |= TEST_GPIO;
        else
            testsFailed |= TEST_GPIO;
        testsRequested &= ~TEST_GPIO;
        return;
    }
    if (testsRequested & TEST_SPI) {
        printf("Running SPI test\n");
        if (testSPI_ROM())
            testsSucceeded |= TEST_SPI;
        else
            testsFailed |= TEST_SPI;
        testsRequested &= ~TEST_SPI;
        return;
    }
    if(testsFailed || testsSucceeded) {
        printf("Sending test result: %X\n", testsSucceeded);
        uint8 result[4];
        result[0] = (uint8) (testsSucceeded & 0xFF);
        result[1] = (uint8) ((testsSucceeded >> 8) & 0xFF);
        result[2] = (uint8) ((testsSucceeded >> 16) & 0xFF);
        result[3] = (uint8) ((testsSucceeded >> 24) & 0xFF);
        testsFailed = 0;
        testsSucceeded = 0;
        gecko_cmd_gatt_server_send_characteristic_notification(currentConnection, GATTDB_test_result, sizeof result, result);
    }
}

bool characStatus(struct gecko_msg_gatt_server_characteristic_status_evt_t *status) {
    if (status->status_flags == gatt_server_client_config) {
        bool enable = status->client_config_flags != gatt_disable;
        printf("Notification %s on charac %d\n", enable ? "enabled" : "disabled", status->characteristic);
        switch (status->characteristic) {
            case GATTDB_test_result:
                notify_testResult = enable;
                break;

            default:
                return false;
        }
    }
    return true;
}

// this is the entry point for the firmware upgrader
void dfu_main(void) {
    //EMU_init();
    //CMU_init();

    GPIO_PinModeSet(gpioPortD, 13, gpioModePushPull, 0);
    GPIO_PinModeSet(gpioPortD, 14, gpioModePushPull, 1);
    GPIO_PinModeSet(gpioPortD, 15, gpioModeInputPull, 1);
    printf("Started OTA_DFU\n");
    if (USER_BLAT->type != APP_APP_ADDRESS_TYPE)
        enterDfu = true;

    if (!enterDfu) {
        SCB->VTOR = (uint32_t) USER_BLAT->vectorTable;
        //Use stack from aat
        asm("mov sp,%0"::"r" (USER_BLAT->topOfStack));
        USER_BLAT->resetVector();
    }
    CRYPTO_AES_DecryptKey256(CRYPTO, deKey, ota_key);
    gecko_init(&config);
    printf("BLE Stack initialised\n");
    struct gecko_msg_flash_ps_load_rsp_t *resp = gecko_cmd_flash_ps_load(SERNUM_KEY);
    if (resp->result == 0) {
        memcpy(sernum, &resp->value, resp->value.len+1);
    } else {
        memcpy(sernum, defaultSernum, sizeof defaultSernum);
    }

    gecko_cmd_hardware_set_soft_timer(TICKS_PER_SEC, 1, 0);
    for (;;) {
        /* Event pointer for handling events */
        struct gecko_cmd_packet *evt;
        struct gecko_msg_le_connection_parameters_evt_t *pp;
        struct gecko_msg_gatt_server_characteristic_status_evt_t *status;
        struct gecko_msg_gatt_server_user_read_request_evt_t *readStatus;
        uint16 i;

        /* Check for stack event. */
        evt = gecko_wait_event();

        /* Handle events */
        unsigned id = BGLIB_MSG_ID(evt->header) & ~gecko_dev_type_gecko;
        switch (BGLIB_MSG_ID(evt->header)) {

            case gecko_evt_hardware_soft_timer_id:
                if(notify_testResult)
                    runTests();
                if(toggle) {
                    if(GPIO_PinInGet(gpioPortD, 15) == 0) {
                        printf("Switch depressed\n");
                        GPIO_PinOutClear(gpioPortD, 13);
                    } else
                        GPIO_PinOutSet(gpioPortD, 13);
                    GPIO_PinOutClear(gpioPortD, 14);
                } else {
                    GPIO_PinOutSet(gpioPortD, 14);
                    GPIO_PinOutClear(gpioPortD, 13);
                }
                toggle = !toggle;
                break;


                /* This boot event is generated when the system boots up after reset.
                 * Here the system is set to start advertising immediately after boot procedure. */
            case gecko_evt_system_boot_id:
                gecko_cmd_gatt_set_max_mtu(MAX_MTU);
                printf("Bootloader: system_boot\n");

                /* Set advertising parameters. 100ms advertisement interval. All channels used.
             * The first two parameters are minimum and maximum advertising interval, both in
             * units of (milliseconds * 1.6). The third parameter '7' sets advertising on all channels. */
                gecko_cmd_le_gap_set_adv_parameters(100, 100, 7);
                gecko_cmd_le_gap_set_adv_data(0, sizeof(advData), advData);

                /* Start general advertising and enable connections. */
                gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_undirected_connectable);
                break;

            case gecko_evt_le_connection_opened_id:
                printf("Connection opened\n");
                currentConnection = evt->data.evt_le_connection_opened.connection;
                break;

            case gecko_evt_le_connection_closed_id:
                notify_testResult = 0;
                testsFailed = testsSucceeded = testsRequested = 0;
                printf("Connection closed\n");
                if (doReset) {
                    printf("Resetting....\n");
                    SCB->AIRCR = RESET_REQUEST;
                } else
                    /* Restart advertising after client has disconnected */
                    gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);
                break;

            case gecko_evt_gatt_server_characteristic_status_id:
                characStatus(&evt->data.evt_gatt_server_characteristic_status);
                break;

            case gecko_evt_gatt_server_user_read_request_id:
                user_read(&evt->data.evt_gatt_server_user_read_request);
                break;

            case gecko_evt_gatt_server_user_write_request_id:
                user_write(&evt->data.evt_gatt_server_user_write_request);
                break;

            case gecko_evt_le_connection_parameters_id:
                pp = &evt->data.evt_le_connection_parameters;
                printf("Connection parameters: interval %d, latency %d, timeout %d\n",
                       pp->interval, pp->latency, pp->timeout);
                break;

            case gecko_evt_gatt_mtu_exchanged_id:
                printf("MTU exchanged: %d\n", evt->data.evt_gatt_mtu_exchanged.mtu);
                break;

            case gecko_evt_endpoint_status_id:
                //printf("Endpoint %d status: flags %X, type %d\n",
                //evt->data.evt_endpoint_status.endpoint, evt->data.evt_endpoint_status.flags, evt->data.evt_endpoint_status.type);
                break;

            default:
#if DEBUG
                if (id & gecko_msg_type_evt) {
                    id &= ~gecko_msg_type_evt;
                    printf("unhandled event = %X\n", id);
                } else if (id & gecko_msg_type_rsp) {
                    id &= ~gecko_msg_type_rsp;
                    printf("unhandled response = %X\n", id);
                } else
                    printf("undeciphered message = %X\n", id);
#endif
                break;
        }
    }
}

void exit(int i) {
    SCB->AIRCR = RESET_REQUEST;
    for (;;);
}