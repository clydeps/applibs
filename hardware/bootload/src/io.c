//
// Created by Clyde Stubbs on 9/4/17.
//

#include <io.h>
#include <gatt_db.h>
#include <native_gecko.h>
#include <SEGGER_RTT.h>

static char debugBuf[64];
static uint8_t idx;

static void writeBuf() {
#if DEBUG
    SEGGER_RTT_Write(0, debugBuf, idx);
#endif
    idx = 0;
}

void putch(char c) {
    debugBuf[idx++] = c;
    if (idx == 32 || idx == sizeof(debugBuf)) {
        writeBuf();
    }
}

void ioFlush(void) {
    if (idx != 0)
        writeBuf();
}

