<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.0.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="61" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="Invisible" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="61" name="stand" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="Invisibles" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="origins2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="fp5" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="fp6" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="fp7" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="Text" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="2Restrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="bLogo" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tText" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bMask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="sf-bluegiga">
<description>&lt;strong&gt;Bluegiga Technologies Bluetooth and Wi-Fi modules&lt;/strong&gt;

&lt;p&gt;Includes devices for all of the following:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;BLE112-A, BLE112-E&lt;/li&gt;
&lt;li&gt;BLE113-A&lt;/li&gt;
&lt;li&gt;BLE121LR-A&lt;/li&gt;
&lt;li&gt;BT111-A&lt;/li&gt;
&lt;li&gt;WT11i-A, WT11i-E&lt;/li&gt;
&lt;li&gt;WT12-A&lt;/li&gt;
&lt;li&gt;WT21-A, WT21-N&lt;/li&gt;
&lt;li&gt;WT32-A, WT32-E&lt;/li&gt;
&lt;li&gt;WT41-A, WT41-E, WT41-N&lt;/li&gt;
&lt;li&gt;WF111-A, WF111-E&lt;/li&gt;
&lt;li&gt;WF121-A, WF121-E&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;All &lt;strong&gt;-A&lt;/strong&gt; variants have built-in chip antennas. All &lt;strong&gt;-N&lt;/strong&gt; variants have RF pins for feeding an external antenna through the PCB. All &lt;strong&gt;-E&lt;/strong&gt; variants include a small antenna connector (U.Fl or W.Fl) on the top face of the module.&lt;/p&gt;

&lt;p&gt;In addition, each module includes a logical pin layout symbol as well as a visual pin layout symbol. The corresponding packages are the same, but depending on your needs or preferences, you may prefer one over the other. For example, using a visual pin layout symbol makes the creation of breakout boards incredibly simple. Visual symbols are denoted by a &lt;strong&gt;-VISUAL&lt;/strong&gt; suffix on the part name.&lt;/p&gt;

&lt;p&gt;Created by Jeff Rowberg&lt;br /&gt;&lt;a href="mailto:jeff@rowberg.net"&gt;jeff@rowberg.net&lt;/a&gt;&lt;br /&gt;&lt;a href="http://www.sectorfej.net"&gt;www.sectorfej.net&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;em&gt;Last updated 2014-05-21&lt;/em&gt;&lt;/p&gt;</description>
<packages>
<package name="BGM111-A">
<description>Bluetooth LE module</description>
<wire x1="-7.45" y1="-8.5" x2="-7.45" y2="7.5" width="0.127" layer="21"/>
<wire x1="-7.45" y1="7.5" x2="7.45" y2="7.5" width="0.127" layer="21"/>
<wire x1="7.45" y1="7.5" x2="7.45" y2="-8.5" width="0.127" layer="21"/>
<wire x1="7.45" y1="-8.5" x2="-7.45" y2="-8.5" width="0.127" layer="21"/>
<smd name="PA4" x="-5.65" y="-2.95" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PA3" x="-5.65" y="-1.75" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PA2" x="-5.65" y="-0.55" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PA1" x="-5.65" y="0.65" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PA0" x="-5.65" y="1.85" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PD15" x="-5.65" y="3.05" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PF7" x="5.65" y="3.05" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="VDD" x="5.65" y="4.25" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="RESET" x="5.65" y="5.45" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="GND@4" x="5.65" y="6.65" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PD14" x="-5.65" y="4.25" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PD13" x="-5.65" y="5.45" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="GND" x="-5.65" y="6.65" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PC9" x="1.2" y="-6.7" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R180"/>
<smd name="PC8" x="0" y="-6.7" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R180"/>
<smd name="PC7" x="-1.2" y="-6.7" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R180"/>
<smd name="PC6" x="-2.4" y="-6.7" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R180"/>
<smd name="PB13" x="-3.6" y="-6.7" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R180"/>
<smd name="GND@2" x="-5.65" y="-6.55" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PB11" x="-5.65" y="-5.35" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PA5" x="-5.65" y="-4.15" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PC10" x="2.4" y="-6.7" dx="0.8" dy="2.4" layer="1" roundness="100"/>
<smd name="PC11" x="3.6" y="-6.7" dx="0.8" dy="2.4" layer="1" roundness="100"/>
<smd name="GND@3" x="5.65" y="-6.55" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PF0" x="5.65" y="-5.35" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<text x="-4.2" y="6.35" size="0.6096" layer="51" rot="SR0">1</text>
<text x="-3.35" y="-5.2" size="0.6096" layer="51" rot="SR90">13</text>
<text x="3.85" y="-5.2" size="0.6096" layer="51" rot="SR90">19</text>
<text x="3.2" y="6.35" size="0.6096" layer="51" rot="SR0">31</text>
<text x="-6.5" y="8" size="1.016" layer="25" rot="SR0">&gt;NAME</text>
<text x="-6.5" y="-9" size="1.016" layer="27" rot="SR0">&gt;VALUE</text>
<smd name="PF1" x="5.65" y="-4.15" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PF2" x="5.65" y="-2.95" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PF3" x="5.65" y="-1.75" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PF4" x="5.65" y="-0.55" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PF5" x="5.65" y="0.65" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<smd name="PF6" x="5.65" y="1.85" dx="0.8" dy="2.4" layer="1" roundness="100" rot="R90"/>
<rectangle x1="-3.8" y1="4" x2="3.8" y2="7.5" layer="39"/>
<rectangle x1="-3.8" y1="4" x2="3.8" y2="7.5" layer="40"/>
<rectangle x1="-3.8" y1="4" x2="3.8" y2="7.5" layer="41"/>
<rectangle x1="-3.8" y1="4" x2="3.8" y2="7.5" layer="42"/>
<rectangle x1="-3.8" y1="4" x2="3.8" y2="7.5" layer="43"/>
<rectangle x1="-2" y1="5.45" x2="1" y2="6.95" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="BGM111-A">
<wire x1="-15.24" y1="22.86" x2="15.24" y2="22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="22.86" x2="15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="-22.86" x2="-15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-22.86" x2="-15.24" y2="22.86" width="0.254" layer="94"/>
<text x="-15.24" y="24.13" size="2.54" layer="95">&gt;NAME</text>
<text x="-15.24" y="-26.67" size="2.54" layer="96">&gt;VALUE</text>
<pin name="GND" x="-17.78" y="20.32" visible="pin" length="short" direction="pwr"/>
<pin name="VDD" x="-17.78" y="17.78" visible="pin" length="short" direction="pwr"/>
<pin name="PF6" x="17.78" y="0" visible="pin" length="short" rot="R180"/>
<pin name="PB11" x="-17.78" y="-7.62" visible="pin" length="short"/>
<pin name="PD15" x="-17.78" y="-20.32" visible="pin" length="short"/>
<pin name="PF7" x="17.78" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="PD14" x="-17.78" y="-17.78" visible="pin" length="short"/>
<pin name="PD13" x="-17.78" y="-15.24" visible="pin" length="short"/>
<pin name="PA5" x="-17.78" y="-2.54" visible="pin" length="short"/>
<pin name="PA4" x="-17.78" y="0" visible="pin" length="short"/>
<pin name="PA3" x="-17.78" y="2.54" visible="pin" length="short"/>
<pin name="PA2" x="-17.78" y="5.08" visible="pin" length="short"/>
<pin name="PA1" x="-17.78" y="7.62" visible="pin" length="short"/>
<pin name="PA0" x="-17.78" y="10.16" visible="pin" length="short"/>
<pin name="RESET" x="17.78" y="20.32" visible="pin" length="short" direction="in" rot="R180"/>
<wire x1="12.7" y1="21.336" x2="5.842" y2="21.336" width="0.127" layer="97"/>
<pin name="PF5" x="17.78" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="PF4" x="17.78" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="PF3" x="17.78" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="PF2" x="17.78" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="PF1" x="17.78" y="12.7" visible="pin" length="short" rot="R180"/>
<pin name="PF0" x="17.78" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="PB13" x="-17.78" y="-10.16" visible="pin" length="short"/>
<pin name="PC6" x="17.78" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="PC7" x="17.78" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="PC8" x="17.78" y="-12.7" visible="pin" length="short" rot="R180"/>
<pin name="PC9" x="17.78" y="-15.24" visible="pin" length="short" rot="R180"/>
<pin name="PC10" x="17.78" y="-17.78" visible="pin" length="short" rot="R180"/>
<pin name="PC11" x="17.78" y="-20.32" visible="pin" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BGM111" prefix="U">
<description>Bluetooth LE Module</description>
<gates>
<gate name="G$1" symbol="BGM111-A" x="0" y="0"/>
</gates>
<devices>
<device name="-A" package="BGM111-A">
<connects>
<connect gate="G$1" pin="GND" pad="GND GND@2 GND@3 GND@4"/>
<connect gate="G$1" pin="PA0" pad="PA0"/>
<connect gate="G$1" pin="PA1" pad="PA1"/>
<connect gate="G$1" pin="PA2" pad="PA2"/>
<connect gate="G$1" pin="PA3" pad="PA3"/>
<connect gate="G$1" pin="PA4" pad="PA4"/>
<connect gate="G$1" pin="PA5" pad="PA5"/>
<connect gate="G$1" pin="PB11" pad="PB11"/>
<connect gate="G$1" pin="PB13" pad="PB13"/>
<connect gate="G$1" pin="PC10" pad="PC10"/>
<connect gate="G$1" pin="PC11" pad="PC11"/>
<connect gate="G$1" pin="PC6" pad="PC6"/>
<connect gate="G$1" pin="PC7" pad="PC7"/>
<connect gate="G$1" pin="PC8" pad="PC8"/>
<connect gate="G$1" pin="PC9" pad="PC9"/>
<connect gate="G$1" pin="PD13" pad="PD13"/>
<connect gate="G$1" pin="PD14" pad="PD14"/>
<connect gate="G$1" pin="PD15" pad="PD15"/>
<connect gate="G$1" pin="PF0" pad="PF0"/>
<connect gate="G$1" pin="PF1" pad="PF1"/>
<connect gate="G$1" pin="PF2" pad="PF2"/>
<connect gate="G$1" pin="PF3" pad="PF3"/>
<connect gate="G$1" pin="PF4" pad="PF4"/>
<connect gate="G$1" pin="PF5" pad="PF5"/>
<connect gate="G$1" pin="PF6" pad="PF6"/>
<connect gate="G$1" pin="PF7" pad="PF7"/>
<connect gate="G$1" pin="RESET" pad="RESET"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name="">
<attribute name="ANTANNA" value="On-module ceramic chip antenna"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Blue22">
<packages>
<package name="SOIC127P600X172-8N">
<smd name="1" x="-2.667" y="1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="2" x="-2.667" y="0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="3" x="-2.667" y="-0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="4" x="-2.667" y="-1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="5" x="2.667" y="-1.905" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="6" x="2.667" y="-0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="7" x="2.667" y="0.635" dx="1.6002" dy="0.5334" layer="1"/>
<smd name="8" x="2.667" y="1.905" dx="1.6002" dy="0.5334" layer="1"/>
<wire x1="-2.0066" y1="1.651" x2="-2.0066" y2="2.159" width="0" layer="51"/>
<wire x1="-2.0066" y1="2.159" x2="-3.0988" y2="2.159" width="0" layer="51"/>
<wire x1="-3.0988" y1="2.159" x2="-3.0988" y2="1.651" width="0" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.651" width="0" layer="51"/>
<wire x1="-2.0066" y1="0.381" x2="-2.0066" y2="0.889" width="0" layer="51"/>
<wire x1="-2.0066" y1="0.889" x2="-3.0988" y2="0.889" width="0" layer="51"/>
<wire x1="-3.0988" y1="0.889" x2="-3.0988" y2="0.381" width="0" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.381" width="0" layer="51"/>
<wire x1="-2.0066" y1="-0.889" x2="-2.0066" y2="-0.381" width="0" layer="51"/>
<wire x1="-2.0066" y1="-0.381" x2="-3.0988" y2="-0.381" width="0" layer="51"/>
<wire x1="-3.0988" y1="-0.381" x2="-3.0988" y2="-0.889" width="0" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.889" width="0" layer="51"/>
<wire x1="-2.0066" y1="-2.159" x2="-2.0066" y2="-1.651" width="0" layer="51"/>
<wire x1="-2.0066" y1="-1.651" x2="-3.0988" y2="-1.651" width="0" layer="51"/>
<wire x1="-3.0988" y1="-1.651" x2="-3.0988" y2="-2.159" width="0" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.159" width="0" layer="51"/>
<wire x1="2.0066" y1="-1.651" x2="2.0066" y2="-2.159" width="0" layer="51"/>
<wire x1="2.0066" y1="-2.159" x2="3.0988" y2="-2.159" width="0" layer="51"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.651" width="0" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.651" width="0" layer="51"/>
<wire x1="2.0066" y1="-0.381" x2="2.0066" y2="-0.889" width="0" layer="51"/>
<wire x1="2.0066" y1="-0.889" x2="3.0988" y2="-0.889" width="0" layer="51"/>
<wire x1="3.0988" y1="-0.889" x2="3.0988" y2="-0.381" width="0" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.381" width="0" layer="51"/>
<wire x1="2.0066" y1="0.889" x2="2.0066" y2="0.381" width="0" layer="51"/>
<wire x1="2.0066" y1="0.381" x2="3.0988" y2="0.381" width="0" layer="51"/>
<wire x1="3.0988" y1="0.381" x2="3.0988" y2="0.889" width="0" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.889" width="0" layer="51"/>
<wire x1="2.0066" y1="2.159" x2="2.0066" y2="1.651" width="0" layer="51"/>
<wire x1="2.0066" y1="1.651" x2="3.0988" y2="1.651" width="0" layer="51"/>
<wire x1="3.0988" y1="1.651" x2="3.0988" y2="2.159" width="0" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.159" width="0" layer="51"/>
<wire x1="-2.0066" y1="-2.4892" x2="2.0066" y2="-2.4892" width="0" layer="51"/>
<wire x1="2.0066" y1="-2.4892" x2="2.0066" y2="2.4892" width="0" layer="51"/>
<wire x1="2.0066" y1="2.4892" x2="0.3048" y2="2.4892" width="0" layer="51"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0" layer="51"/>
<wire x1="-0.3048" y1="2.4892" x2="-2.0066" y2="2.4892" width="0" layer="51"/>
<wire x1="-2.0066" y1="2.4892" x2="-2.0066" y2="-2.4892" width="0" layer="51"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0" layer="51" curve="-180"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<wire x1="-1.778" y1="-2.4892" x2="1.778" y2="-2.4892" width="0.1524" layer="21"/>
<wire x1="1.778" y1="2.4892" x2="0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.4892" x2="-1.778" y2="2.4892" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.4892" x2="-0.3048" y2="2.4892" width="0.1524" layer="21" curve="-180"/>
<text x="-3.4798" y="2.3114" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<text x="-4.6482" y="-5.334" size="1.4224" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="3.937" size="1.4224" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
<package name="C1812">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 1812</description>
<wire x1="-1.4732" y1="1.5002" x2="1.4732" y2="1.5002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.5002" x2="1.4732" y2="-1.5002" width="0.1016" layer="51"/>
<wire x1="-3.0605" y1="1.7768" x2="3.0606" y2="1.7769" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="1.7769" x2="3.0606" y2="-1.7766" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="-1.7766" x2="-3.0605" y2="-1.7767" width="0.0508" layer="39"/>
<wire x1="-3.0605" y1="-1.7767" x2="-3.0605" y2="1.7768" width="0.0508" layer="39"/>
<wire x1="-3.3782" y1="1.905" x2="3.3782" y2="1.905" width="0.2032" layer="21"/>
<wire x1="3.3782" y1="1.905" x2="3.3782" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="3.3782" y1="-1.905" x2="-3.3782" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-3.3782" y1="-1.905" x2="-3.3782" y2="1.905" width="0.2032" layer="21"/>
<smd name="1" x="-2.159" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.159" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-3.048" y="2.286" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.048" y="-2.794" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3" y1="-1.6" x2="-1.4" y2="1.6" layer="51"/>
<rectangle x1="1.4" y1="-1.6" x2="2.3" y2="1.6" layer="51"/>
<rectangle x1="-0.3175" y1="-0.7" x2="0.3175" y2="0.7" layer="35"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - KEMET 1812 Reflow solder&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<wire x1="-3.2192" y1="1.9355" x2="3.2193" y2="1.9355" width="0.0508" layer="39"/>
<wire x1="3.2193" y1="1.9355" x2="3.2193" y2="-1.9355" width="0.0508" layer="39"/>
<wire x1="3.2193" y1="-1.9355" x2="-3.2192" y2="-1.9355" width="0.0508" layer="39"/>
<wire x1="-3.2192" y1="-1.9355" x2="-3.2192" y2="1.9355" width="0.0508" layer="39"/>
<wire x1="-3.4131" y1="2.1431" x2="3.4131" y2="2.1431" width="0.2032" layer="21"/>
<wire x1="3.4131" y1="2.1431" x2="3.4131" y2="-2.1431" width="0.2032" layer="21"/>
<wire x1="3.4131" y1="-2.1431" x2="-3.4131" y2="-2.1431" width="0.2032" layer="21"/>
<wire x1="-3.4131" y1="-2.1431" x2="-3.4131" y2="2.1431" width="0.2032" layer="21"/>
<smd name="1" x="-2.2225" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.2225" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.8575" y="2.54" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.8575" y="-3.175" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
<rectangle x1="-0.3175" y1="-0.7" x2="0.3175" y2="0.7" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 1825</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="-3.0605" y1="3.3643" x2="3.0606" y2="3.3643" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="3.3643" x2="3.0606" y2="-3.523" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="-3.523" x2="-3.0605" y2="-3.523" width="0.0508" layer="39"/>
<wire x1="-3.0605" y1="-3.523" x2="-3.0605" y2="3.3643" width="0.0508" layer="39"/>
<wire x1="-3.556" y1="3.81" x2="3.556" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.556" y1="3.81" x2="3.556" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="3.556" y1="-3.81" x2="-3.556" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-3.556" y1="-3.81" x2="-3.556" y2="3.81" width="0.2032" layer="21"/>
<smd name="1" x="-2.159" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="2.159" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-3.556" y="4.064" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.556" y="-4.572" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - KEMET 1825 Reflow solder&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<wire x1="-2.5842" y1="3.523" x2="2.5843" y2="3.523" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="3.523" x2="2.5843" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="-3.5231" x2="-2.5842" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="-2.5842" y1="-3.5231" x2="-2.5842" y2="3.523" width="0.0508" layer="39"/>
<wire x1="-2.8575" y1="3.81" x2="2.8575" y2="3.81" width="0.2032" layer="21"/>
<wire x1="2.8575" y1="3.81" x2="2.8575" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="2.8575" y1="-3.81" x2="-2.8575" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-2.8575" y1="-3.81" x2="-2.8575" y2="3.81" width="0.2032" layer="21"/>
<smd name="1" x="-1.5875" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5875" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-2.2225" y="4.1275" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.2225" y="-5.08" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0.635" layer="35"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - KEMET 2220 Reflow solder&lt;p&gt;
Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<wire x1="-3.6955" y1="2.888" x2="4.0131" y2="2.888" width="0.0508" layer="39"/>
<wire x1="4.0131" y1="2.888" x2="4.0131" y2="-2.888" width="0.0508" layer="39"/>
<wire x1="4.0131" y1="-2.888" x2="-3.6955" y2="-2.888" width="0.0508" layer="39"/>
<wire x1="-3.6955" y1="-2.888" x2="-3.6955" y2="2.888" width="0.0508" layer="39"/>
<wire x1="-3.81" y1="3.0956" x2="4.1275" y2="3.0956" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="3.0956" x2="4.1275" y2="-3.0956" width="0.2032" layer="21"/>
<wire x1="4.1275" y1="-3.0956" x2="-3.81" y2="-3.0956" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-3.0956" x2="-3.81" y2="3.0956" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.8575" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-3.4925" y="3.4925" size="1.016" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.4925" y="-4.445" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - KEMET 2225 Reflow solder&lt;p&gt;
Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<wire x1="-3.6955" y1="3.523" x2="3.6956" y2="3.523" width="0.0508" layer="39"/>
<wire x1="3.6956" y1="3.523" x2="3.6956" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="3.6956" y1="-3.5231" x2="-3.6955" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="-3.6955" y1="-3.5231" x2="-3.6955" y2="3.523" width="0.0508" layer="39"/>
<wire x1="-3.937" y1="3.81" x2="3.937" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.937" y1="3.81" x2="3.937" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="3.937" y1="-3.81" x2="-3.937" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-3.937" y1="-3.81" x2="-3.937" y2="3.81" width="0.2032" layer="21"/>
<smd name="1" x="-2.667" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.667" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-3.81" y="4.064" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.81" y="-4.572" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 3216</description>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-2.5842" y1="0.983" x2="2.5843" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="0.983" x2="2.5843" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.5843" y1="-0.983" x2="-2.5842" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.5842" y1="-0.983" x2="-2.5842" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.794" y1="1.143" x2="2.794" y2="1.143" width="0.2032" layer="21"/>
<wire x1="2.794" y1="1.143" x2="2.794" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="2.794" y1="-1.143" x2="-2.794" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="-1.143" x2="-2.794" y2="1.143" width="0.2032" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-2.54" y="1.524" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7013" y1="-0.8509" x2="-0.9512" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 3225</description>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="-2.4255" y1="1.4593" x2="2.4255" y2="1.4593" width="0.0508" layer="39"/>
<wire x1="2.4255" y1="1.4593" x2="2.4255" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="2.4255" y1="-1.4593" x2="-2.4255" y2="-1.4593" width="0.0508" layer="39"/>
<wire x1="-2.4255" y1="-1.4593" x2="-2.4255" y2="1.4593" width="0.0508" layer="39"/>
<wire x1="-2.4606" y1="1.5875" x2="2.4606" y2="1.5875" width="0.2032" layer="21"/>
<wire x1="2.4606" y1="1.5875" x2="2.4606" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="2.4606" y1="-1.5875" x2="-2.4606" y2="-1.5875" width="0.2032" layer="21"/>
<wire x1="-2.4606" y1="-1.5875" x2="-2.4606" y2="1.5875" width="0.2032" layer="21"/>
<smd name="1" x="-1.397" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.397" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.286" y="2.032" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.54" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7013" y1="-1.2954" x2="-0.9512" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.304" x2="1.7018" y2="1.2959" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 4532</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="-3.0605" y1="1.7768" x2="3.0605" y2="1.7768" width="0.0508" layer="39"/>
<wire x1="3.0605" y1="1.7768" x2="3.0605" y2="-1.7767" width="0.0508" layer="39"/>
<wire x1="3.0605" y1="-1.7767" x2="-3.0605" y2="-1.7767" width="0.0508" layer="39"/>
<wire x1="-3.0605" y1="-1.7767" x2="-3.0605" y2="1.7768" width="0.0508" layer="39"/>
<wire x1="-3.175" y1="2.0637" x2="3.175" y2="2.0637" width="0.2032" layer="21"/>
<wire x1="3.175" y1="2.0637" x2="3.175" y2="-2.0638" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-2.0638" x2="-3.175" y2="-2.0638" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-2.0638" x2="-3.175" y2="2.0637" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.905" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-3.048" y="2.54" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.048" y="-2.794" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 4564</description>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="-3.0605" y1="3.523" x2="3.0606" y2="3.523" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="3.523" x2="3.0606" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="-3.5231" x2="-3.0605" y2="-3.5231" width="0.0508" layer="39"/>
<wire x1="-3.0605" y1="-3.5231" x2="-3.0605" y2="3.523" width="0.0508" layer="39"/>
<wire x1="-3.429" y1="3.81" x2="3.429" y2="3.81" width="0.2032" layer="21"/>
<wire x1="3.429" y1="3.81" x2="3.429" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="3.429" y1="-3.81" x2="-3.429" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-3.429" y1="-3.81" x2="-3.429" y2="3.81" width="0.2032" layer="21"/>
<smd name="1" x="-2.159" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="2.159" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-3.302" y="4.064" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.302" y="-4.572" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C0402">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 0402</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="-1.1555" y1="0.483" x2="1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="0.483" x2="1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="-0.483" x2="-1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.1555" y1="-0.483" x2="-1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.016" y1="0.508" x2="1.016" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="-1.016" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="0.508" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<text x="-1.016" y="0.889" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.254" y2="0.25" layer="51"/>
<rectangle x1="0.2588" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<rectangle x1="-0.1" y1="-0.2" x2="0.1" y2="0.2" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 0603</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="-1.473" y1="0.6655" x2="1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.6655" x2="1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.6655" x2="-1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.6655" x2="-1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1.524" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.524" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.25" x2="0.1999" y2="0.25" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 0805</description>
<wire x1="-0.51" y1="0.535" x2="0.51" y2="0.535" width="0.1016" layer="51"/>
<wire x1="-0.51" y1="-0.535" x2="0.51" y2="-0.535" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.8243" x2="1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="0.8243" x2="1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="-0.8243" x2="-1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.8243" x2="-1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8542" y1="0.889" x2="1.8542" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="0.889" x2="1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="-0.889" x2="-1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-1.8542" y1="-0.889" x2="-1.8542" y2="0.889" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-1.778" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 1206</description>
<wire x1="1.0525" y1="-0.7128" x2="-1.0652" y2="-0.7128" width="0.1016" layer="51"/>
<wire x1="1.0525" y1="0.7128" x2="-1.0652" y2="0.7128" width="0.1016" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.4731" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="0.983" x2="2.4731" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.4892" y1="1.143" x2="2.4892" y2="1.143" width="0.2032" layer="21"/>
<wire x1="2.4892" y1="1.143" x2="2.4892" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="2.4892" y1="-1.143" x2="-2.4892" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="-2.4892" y1="-1.143" x2="-2.4892" y2="1.143" width="0.2032" layer="21"/>
<smd name="2" x="1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="1" x="-1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-2.286" y="1.524" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.032" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-0.9" y2="0.8" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<rectangle x1="0.9001" y1="-0.8" x2="1.6" y2="0.8" layer="51" rot="R180"/>
</package>
<package name="C1210">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 1210</description>
<wire x1="-2.6317" y1="1.483" x2="2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.483" x2="2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.483" x2="-2.6317" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6317" y1="-1.483" x2="-2.6317" y2="1.483" width="0.0508" layer="39"/>
<wire x1="1.0525" y1="-1.1128" x2="-1.0652" y2="-1.1128" width="0.1016" layer="51"/>
<wire x1="1.0525" y1="1.1128" x2="-1.0652" y2="1.1128" width="0.1016" layer="51"/>
<wire x1="-2.413" y1="1.524" x2="2.413" y2="1.524" width="0.2032" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="2.413" y1="-1.524" x2="-2.413" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-2.413" y1="-1.524" x2="-2.413" y2="1.524" width="0.2032" layer="21"/>
<smd name="2" x="1.524" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="1" x="-1.524" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.286" y="1.778" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.286" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
<rectangle x1="-1.6" y1="-1.2" x2="-0.9" y2="1.2" layer="51"/>
<rectangle x1="0.9001" y1="-1.2" x2="1.6" y2="1.2" layer="51" rot="R180"/>
</package>
<package name="C0201">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 0201</description>
<wire x1="-0.195" y1="0.124" x2="0.195" y2="0.124" width="0.1016" layer="51"/>
<wire x1="0.195" y1="-0.124" x2="-0.195" y2="-0.124" width="0.1016" layer="51"/>
<wire x1="-0.635" y1="0.4334" x2="0.635" y2="0.4334" width="0.2032" layer="21"/>
<wire x1="0.635" y1="0.4334" x2="0.635" y2="-0.4318" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-0.4318" x2="-0.635" y2="-0.4318" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-0.4318" x2="-0.635" y2="0.4334" width="0.2032" layer="21"/>
<smd name="1" x="-0.254" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="0.254" y="0" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<text x="-0.762" y="0.762" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.762" y="-1.27" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.304" y1="-0.2" x2="-0.15" y2="0.2" layer="51"/>
<rectangle x1="0.15" y1="-0.2" x2="0.3088" y2="0.2" layer="51"/>
</package>
<package name="C1608">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 1608</description>
<wire x1="-1.473" y1="0.6655" x2="1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.6655" x2="1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.6655" x2="-1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.6655" x2="-1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="1.4732" y1="0.762" x2="1.4732" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.4732" y1="-0.762" x2="-1.4732" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.4732" y1="-0.762" x2="-1.4732" y2="0.762" width="0.2032" layer="21"/>
<wire x1="-1.4732" y1="0.762" x2="1.4732" y2="0.762" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.9" dy="1" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.9" dy="1" layer="1"/>
<text x="-1.524" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.524" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1808">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 1808</description>
<wire x1="-1.4732" y1="1.0002" x2="1.4732" y2="1.0002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.0002" x2="1.4732" y2="-1.0002" width="0.1016" layer="51"/>
<wire x1="-3.0605" y1="1.7768" x2="3.0606" y2="1.7769" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="1.7769" x2="3.0606" y2="-1.7766" width="0.0508" layer="39"/>
<wire x1="3.0606" y1="-1.7766" x2="-3.0605" y2="-1.7767" width="0.0508" layer="39"/>
<wire x1="-3.0605" y1="-1.7767" x2="-3.0605" y2="1.7768" width="0.0508" layer="39"/>
<wire x1="-3.302" y1="1.397" x2="3.302" y2="1.397" width="0.2032" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.302" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.2032" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.302" y2="1.397" width="0.2032" layer="21"/>
<smd name="1" x="-2.159" y="0" dx="1.8" dy="2.3" layer="1"/>
<smd name="2" x="2.159" y="0" dx="1.8" dy="2.3" layer="1"/>
<text x="-3.048" y="1.778" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.048" y="-2.286" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.3" y1="-1.1" x2="-1.4" y2="1.1" layer="51"/>
<rectangle x1="1.4" y1="-1.1" x2="2.3" y2="1.1" layer="51"/>
<rectangle x1="-0.3175" y1="-0.7" x2="0.3175" y2="0.7" layer="35"/>
</package>
<package name="SO-08M">
<description>&lt;B&gt;Small Outline Medium Plastic Gull Wing&lt;/B&gt;&lt;p&gt;
207-mil body, package type SM</description>
<wire x1="1.97" y1="-2.4" x2="-1.93" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-2.43" y1="-2.4" x2="-2.43" y2="2.4" width="0.2032" layer="51"/>
<wire x1="-1.93" y1="2.4" x2="1.97" y2="2.4" width="0.2032" layer="21"/>
<wire x1="2.43" y1="2.4" x2="2.43" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-1.93" y1="-2.4" x2="-1.93" y2="2.4" width="0.2032" layer="21"/>
<smd name="2" x="-3.33" y="0.635" dx="0.6" dy="2.2" layer="1" rot="R270"/>
<smd name="7" x="3.33" y="0.635" dx="0.6" dy="2.2" layer="1" rot="R270"/>
<smd name="1" x="-3.33" y="1.905" dx="0.6" dy="2.2" layer="1" rot="R270"/>
<smd name="3" x="-3.33" y="-0.635" dx="0.6" dy="2.2" layer="1" rot="R270"/>
<smd name="4" x="-3.33" y="-1.905" dx="0.6" dy="2.2" layer="1" rot="R270"/>
<smd name="8" x="3.33" y="1.905" dx="0.6" dy="2.2" layer="1" rot="R270"/>
<smd name="6" x="3.33" y="-0.635" dx="0.6" dy="2.2" layer="1" rot="R270"/>
<smd name="5" x="3.33" y="-1.905" dx="0.6" dy="2.2" layer="1" rot="R270"/>
<text x="-2.54" y="2.667" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.937" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="1.97" y1="-2.4" x2="1.97" y2="2.4" width="0.2032" layer="21"/>
<circle x="-1.35" y="1.8" radius="0.180275" width="0.3048" layer="21"/>
</package>
<package name="C2012">
<description>&lt;b&gt;SMD CHIP CAP&lt;/b&gt; - 2012</description>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="-1.6317" y1="0.8242" x2="1.6318" y2="0.8242" width="0.0508" layer="39"/>
<wire x1="1.6318" y1="0.8242" x2="1.6318" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.6318" y1="-0.8243" x2="-1.6317" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.6317" y1="-0.8243" x2="-1.6317" y2="0.8242" width="0.0508" layer="39"/>
<wire x1="-1.9558" y1="1.016" x2="1.9558" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.9558" y1="1.016" x2="1.9558" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.9558" y1="-1.016" x2="-1.9558" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-1.9558" y1="-1.016" x2="-1.9558" y2="1.016" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.778" y="1.47" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.928" y="-2.328" size="0.8" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0917" y1="-0.7239" x2="-0.3416" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="S25L">
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-10.16" y2="12.7" width="0.4064" layer="94"/>
<pin name="CS" x="-15.24" y="7.62" length="middle" direction="in"/>
<pin name="SO" x="-15.24" y="2.54" length="middle"/>
<pin name="WP" x="-15.24" y="-2.54" length="middle" function="dot"/>
<pin name="GND" x="-15.24" y="-7.62" length="middle" direction="pwr"/>
<pin name="SI" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="SCK" x="15.24" y="-2.54" length="middle" direction="in" function="clk" rot="R180"/>
<pin name="HOLD" x="15.24" y="2.54" length="middle" function="dot" rot="R180"/>
<pin name="VCC" x="15.24" y="7.62" length="middle" direction="pwr" rot="R180"/>
<text x="-4.5" y="14.15" size="2.54" layer="95">&gt;Name</text>
<text x="-3.55" y="-16" size="2.54" layer="95">&gt;Value</text>
</symbol>
<symbol name="CNP-">
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="1.5875" x2="0.635" y2="0" width="0.508" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.5875" width="0.508" layer="94"/>
<wire x1="-0.635" y1="1.5875" x2="-0.635" y2="0" width="0.508" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-1.5875" width="0.508" layer="94"/>
<text x="-2.794" y="-1.27" size="0.8636" layer="93">1</text>
<text x="2.286" y="-1.27" size="0.8636" layer="93">2</text>
<text x="1.905" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-4.1275" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="S25FL164K" prefix="U">
<description>Cypress serial flash 25L series</description>
<gates>
<gate name="G$1" symbol="S25L" x="0.35" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X172-8N">
<connects>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="HOLD" pad="7"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="SI" pad="5"/>
<connect gate="G$1" pin="SO" pad="2"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="WP" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOA008" package="SO-08M">
<connects>
<connect gate="G$1" pin="CS" pad="1"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="HOLD" pad="7"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="SI" pad="5"/>
<connect gate="G$1" pin="SO" pad="2"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="WP" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C_" prefix="C" uservalue="yes">
<description>&lt;b&gt;NON-POLARIZED CAP&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="CNP-" x="2.54" y="0"/>
</gates>
<devices>
<device name="1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TagConnect">
<packages>
<package name="TC2030-IDC-NL">
<smd name="2" x="-1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="1" x="-1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="4" x="0" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="3" x="0" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="6" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="5" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<hole x="-2.54" y="0" drill="0.9906"/>
<hole x="2.54" y="1.016" drill="0.9906"/>
<hole x="2.54" y="-1.016" drill="0.9906"/>
<text x="-3.175" y="1.905" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.175" y="-3.175" size="1.27" layer="25">&gt;NAME</text>
<polygon width="0.01" layer="41">
<vertex x="0.41" y="0.635" curve="-180"/>
<vertex x="-0.41" y="0.635"/>
<vertex x="-0.86" y="0.635" curve="-90"/>
<vertex x="-1.27" y="0.233"/>
<vertex x="-1.27" y="-0.233" curve="-90"/>
<vertex x="-0.86" y="-0.635"/>
<vertex x="-0.41" y="-0.635" curve="-180"/>
<vertex x="0.41" y="-0.635"/>
<vertex x="0.86" y="-0.635" curve="-90"/>
<vertex x="1.27" y="-0.233"/>
<vertex x="1.27" y="0.233" curve="-90"/>
<vertex x="0.86" y="0.635"/>
</polygon>
<polygon width="0.01" layer="39">
<vertex x="0.41" y="0.635" curve="-180"/>
<vertex x="-0.41" y="0.635"/>
<vertex x="-0.86" y="0.635" curve="-90"/>
<vertex x="-1.27" y="0.233"/>
<vertex x="-1.27" y="-0.233" curve="-90"/>
<vertex x="-0.86" y="-0.635"/>
<vertex x="-0.41" y="-0.635" curve="-180"/>
<vertex x="0.41" y="-0.635"/>
<vertex x="0.86" y="-0.635" curve="-90"/>
<vertex x="1.27" y="-0.233"/>
<vertex x="1.27" y="0.233" curve="-90"/>
<vertex x="0.86" y="0.635"/>
</polygon>
</package>
<package name="TC2030-IDC">
<smd name="2" x="-1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="1" x="-1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="4" x="0" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="3" x="0" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="6" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="5" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<polygon width="0.01" layer="41">
<vertex x="0.41" y="0.635" curve="-180"/>
<vertex x="-0.41" y="0.635"/>
<vertex x="-0.86" y="0.635" curve="-90"/>
<vertex x="-1.27" y="0.233"/>
<vertex x="-1.27" y="-0.233" curve="-90"/>
<vertex x="-0.86" y="-0.635"/>
<vertex x="-0.41" y="-0.635" curve="-180"/>
<vertex x="0.41" y="-0.635"/>
<vertex x="0.86" y="-0.635" curve="-90"/>
<vertex x="1.27" y="-0.233"/>
<vertex x="1.27" y="0.233" curve="-90"/>
<vertex x="0.86" y="0.635"/>
</polygon>
<polygon width="0.01" layer="39">
<vertex x="0.41" y="0.635" curve="-180"/>
<vertex x="-0.41" y="0.635"/>
<vertex x="-0.86" y="0.635" curve="-90"/>
<vertex x="-1.27" y="0.233"/>
<vertex x="-1.27" y="-0.233" curve="-90"/>
<vertex x="-0.86" y="-0.635"/>
<vertex x="-0.41" y="-0.635" curve="-180"/>
<vertex x="0.41" y="-0.635"/>
<vertex x="0.86" y="-0.635" curve="-90"/>
<vertex x="1.27" y="-0.233"/>
<vertex x="1.27" y="0.233" curve="-90"/>
<vertex x="0.86" y="0.635"/>
</polygon>
<hole x="-2.54" y="0" drill="0.9906"/>
<hole x="2.54" y="1.016" drill="0.9906"/>
<hole x="2.54" y="-1.016" drill="0.9906"/>
<hole x="-2.54" y="2.54" drill="2.3749"/>
<hole x="-2.54" y="-2.54" drill="2.3749"/>
<hole x="0.635" y="2.54" drill="2.3749"/>
<hole x="0.635" y="-2.54" drill="2.3749"/>
<rectangle x1="-3.3147" y1="-5.14985" x2="-1.7653" y2="-2.54" layer="40"/>
<rectangle x1="-0.1397" y1="-5.14985" x2="1.4097" y2="-2.54" layer="40"/>
<rectangle x1="-3.3147" y1="2.54" x2="-1.7653" y2="5.14985" layer="40"/>
<rectangle x1="-0.1397" y1="2.54" x2="1.4097" y2="5.14985" layer="40"/>
<text x="-4.445" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="5.08" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="TC2030-IDC">
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TC2030-IDC" prefix="TC">
<description>Tag-Connect In Circuit Programming &amp; Debug Cable 6 Pin
http://www.tag-connect.com</description>
<gates>
<gate name="A" symbol="TC2030-IDC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TC2030-IDC">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-NL" package="TC2030-IDC-NL">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Monitor">
<packages>
<package name="SOT89">
<description>&lt;b&gt;SOT98&lt;/b&gt; PK (R-PDSO-G3)&lt;p&gt;
Source: http://focus.ti.com/lit/ds/symlink/ua78l05.pdf</description>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-0.7874" y1="1.5748" x2="-0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="-0.3556" y1="2.0066" x2="0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="0.3556" y1="2.0066" x2="0.7874" y2="1.5748" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.5748" x2="0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="-0.7874" y1="1.2954" x2="-0.7874" y2="1.5748" width="0.1998" layer="51"/>
<smd name="1" x="-1.499" y="-1.981" dx="1" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="3" x="1.499" y="-1.981" dx="1" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="2" x="0" y="-1.727" dx="1" dy="2" layer="1" stop="no" cream="no"/>
<smd name="2@1" x="0" y="0.94" dx="2.2" dy="3.7" layer="1" roundness="100"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<rectangle x1="-0.4" y1="-2.68" x2="0.4" y2="-1.28" layer="31"/>
<rectangle x1="-2.025" y1="-2.775" x2="-0.975" y2="-1.2" layer="29"/>
<rectangle x1="0.975" y1="-2.775" x2="2.025" y2="-1.2" layer="29"/>
<rectangle x1="-0.525" y1="-2.775" x2="0.525" y2="-1.2" layer="29"/>
<rectangle x1="1.1" y1="-2.68" x2="1.9" y2="-1.28" layer="31"/>
<rectangle x1="-1.9" y1="-2.68" x2="-1.1" y2="-1.28" layer="31"/>
<polygon width="0.1998" layer="51">
<vertex x="-0.7874" y="1.3208"/>
<vertex x="-0.7874" y="1.5748"/>
<vertex x="-0.3556" y="2.0066"/>
<vertex x="0.3048" y="2.0066"/>
<vertex x="0.3556" y="2.0066"/>
<vertex x="0.7874" y="1.5748"/>
<vertex x="0.7874" y="1.2954"/>
<vertex x="-0.7874" y="1.2954"/>
</polygon>
</package>
<package name="USB_MICRO_B-HIROSE-ZX62-B-5PA(33)">
<smd name="5" x="1.3" y="4.125" dx="0.4" dy="1.35" layer="1"/>
<smd name="4" x="0.65" y="4.125" dx="0.4" dy="1.35" layer="1"/>
<smd name="3" x="0" y="4.125" dx="0.4" dy="1.35" layer="1"/>
<smd name="2" x="-0.65" y="4.125" dx="0.4" dy="1.35" layer="1"/>
<smd name="1" x="-1.3" y="4.125" dx="0.4" dy="1.35" layer="1"/>
<smd name="P$1" x="3.1" y="3.8" dx="2.1" dy="2" layer="1"/>
<smd name="P$2" x="-3.1" y="3.8" dx="2.1" dy="2" layer="1"/>
<smd name="P$3" x="-4" y="1.45" dx="1.8" dy="1.9" layer="1"/>
<smd name="P$4" x="-1.2" y="1.45" dx="1.9" dy="1.9" layer="1"/>
<smd name="P$5" x="1.2" y="1.45" dx="1.9" dy="1.9" layer="1"/>
<smd name="P$6" x="4" y="1.45" dx="1.8" dy="1.9" layer="1"/>
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0" layer="20"/>
<wire x1="-3.75" y1="-0.6" x2="-3.75" y2="4.4" width="0.127" layer="51"/>
<wire x1="-3.75" y1="4.4" x2="3.75" y2="4.4" width="0.127" layer="51"/>
<wire x1="3.75" y1="4.4" x2="3.75" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-3.75" y1="-0.6" x2="-3.7" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-3.7" y1="-0.6" x2="3.7" y2="-0.6" width="0.127" layer="51"/>
<wire x1="3.7" y1="-0.6" x2="3.75" y2="-0.6" width="0.127" layer="51"/>
<wire x1="-3.7" y1="-0.6" x2="-4.1" y2="-1.1" width="0.127" layer="51"/>
<wire x1="3.7" y1="-0.6" x2="4.1" y2="-1.1" width="0.127" layer="51"/>
<text x="0" y="5.1" size="1.016" layer="25" font="vector" ratio="12" align="bottom-center">&gt;NAME</text>
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0" layer="107"/>
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0" layer="46"/>
<wire x1="5.05" y1="5.05" x2="5.05" y2="0.05" width="0.1" layer="21"/>
<wire x1="-5.05" y1="5.05" x2="-5.05" y2="0.05" width="0.1" layer="21"/>
<wire x1="-5.05" y1="5.05" x2="5.05" y2="5.05" width="0.1" layer="21"/>
</package>
<package name="BATTCON_20MM">
<wire x1="-3.7" y1="-9.9" x2="3.7" y2="-9.9" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-9.9" x2="-10.5" y2="-5.7" width="0.2032" layer="21"/>
<wire x1="3.7" y1="-9.9" x2="10.5" y2="-5.7" width="0.2032" layer="21"/>
<wire x1="-5.3" y1="7.3" x2="5.4" y2="7.4" width="0.2032" layer="21" curve="94.031579"/>
<wire x1="10.5" y1="-5.7" x2="10.5" y2="-3" width="0.2032" layer="21"/>
<wire x1="-10.5" y1="-5.7" x2="-10.5" y2="-3" width="0.2032" layer="21"/>
<wire x1="-10.5" y1="5.3" x2="-5.3" y2="7.3" width="0.2032" layer="21" curve="-139.635474"/>
<wire x1="10.5" y1="5.3" x2="5.3" y2="7.3" width="0.2032" layer="21" curve="136.99875"/>
<wire x1="-10.5" y1="5.3" x2="-10.5" y2="3" width="0.2032" layer="21"/>
<wire x1="10.5" y1="5.3" x2="10.5" y2="3" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="10" width="0.2032" layer="51"/>
<smd name="2" x="0" y="0" dx="4.064" dy="4.064" layer="1" roundness="100"/>
<smd name="1" x="-11.45" y="0" dx="2.5" dy="5.1" layer="1"/>
<smd name="3" x="11.45" y="0" dx="2.5" dy="5.1" layer="1"/>
<text x="-6.985" y="0.635" size="0.4064" layer="25">&gt;NAME</text>
<text x="-6.985" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SOD123">
<description>&lt;b&gt;Diode&lt;/b&gt;</description>
<wire x1="-1.1" y1="0.7" x2="1.1" y2="0.7" width="0.254" layer="51"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.7" width="0.254" layer="51"/>
<wire x1="1.1" y1="-0.7" x2="-1.1" y2="-0.7" width="0.254" layer="51"/>
<wire x1="-1.1" y1="-0.7" x2="-1.1" y2="0.7" width="0.254" layer="51"/>
<smd name="1" x="-1.9" y="0" dx="1.4" dy="1.4" layer="1"/>
<smd name="2" x="1.9" y="0" dx="1.4" dy="1.4" layer="1"/>
<text x="-1.1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.1" y="-2.3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.95" y1="-0.45" x2="-1.2" y2="0.4" layer="51"/>
<rectangle x1="1.2" y1="-0.45" x2="1.95" y2="0.4" layer="51"/>
<wire x1="-2.95" y1="1" x2="3" y2="1" width="0.1" layer="21"/>
<wire x1="3" y1="1" x2="3" y2="-1" width="0.1" layer="21"/>
<wire x1="3" y1="-1" x2="-3" y2="-1" width="0.1" layer="21"/>
<wire x1="-3" y1="-1" x2="-3" y2="1" width="0.4" layer="21"/>
</package>
<package name="APTB1612">
<smd name="P$4" x="-0.825" y="-0.43" dx="0.8128" dy="0.4064" layer="1" rot="R180"/>
<smd name="P$2" x="-0.825" y="0.43" dx="0.8128" dy="0.4064" layer="1" rot="R180"/>
<smd name="P$3" x="0.825" y="-0.43" dx="0.8128" dy="0.4064" layer="1" rot="R180"/>
<smd name="P$1" x="0.825" y="0.43" dx="0.8128" dy="0.4064" layer="1" rot="R180"/>
<rectangle x1="-1.25" y1="0.8" x2="-0.7" y2="1.25" layer="21"/>
<rectangle x1="-1.25" y1="-1.25" x2="-0.7" y2="-0.8" layer="21" rot="R180"/>
<text x="-0.3" y="-1.6" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-0.1" y="1.15" size="0.4064" layer="25">&gt;NAME</text>
<wire x1="-0.9" y1="0.7" x2="0.9" y2="0.7" width="0.1" layer="51"/>
<wire x1="0.9" y1="0.7" x2="0.9" y2="-0.7" width="0.1" layer="51"/>
<wire x1="-0.9" y1="0.7" x2="-0.9" y2="-0.7" width="0.1" layer="51"/>
<wire x1="-0.9" y1="-0.7" x2="0.9" y2="-0.7" width="0.1" layer="51"/>
<wire x1="-1.5" y1="1" x2="-1.5" y2="-1" width="0.1" layer="21"/>
<wire x1="-1.5" y1="-1" x2="1.55" y2="-1" width="0.1" layer="21"/>
<wire x1="1.55" y1="-1" x2="1.55" y2="1" width="0.1" layer="21"/>
<wire x1="1.55" y1="1" x2="-1.5" y2="1" width="0.1" layer="21"/>
</package>
<package name="CHIPLED6">
<smd name="1" x="-0.9" y="0.65" dx="0.6096" dy="0.3048" layer="1"/>
<smd name="2" x="-0.9" y="0" dx="0.6096" dy="0.3048" layer="1"/>
<smd name="3" x="-0.9" y="-0.65" dx="0.6096" dy="0.3048" layer="1"/>
<smd name="4" x="0.9" y="-0.65" dx="0.6096" dy="0.3048" layer="1"/>
<smd name="5" x="0.9" y="0" dx="0.6096" dy="0.3048" layer="1"/>
<smd name="6" x="0.9" y="0.65" dx="0.6096" dy="0.3048" layer="1"/>
<circle x="-1.5" y="1.25" radius="0.1" width="0.1" layer="21"/>
<wire x1="1.05" y1="1.05" x2="1.05" y2="-1.05" width="0.1" layer="51"/>
<wire x1="1.05" y1="-1.05" x2="-1.05" y2="-1.05" width="0.1" layer="51"/>
<wire x1="-1.05" y1="-1.05" x2="-1.05" y2="1.05" width="0.1" layer="51"/>
<wire x1="-1.05" y1="1.05" x2="1.05" y2="1.05" width="0.1" layer="51"/>
<wire x1="-1.35" y1="1.05" x2="-1.35" y2="-1.05" width="0.1" layer="21"/>
<wire x1="-1.35" y1="-1.05" x2="1.35" y2="-1.05" width="0.1" layer="21"/>
<wire x1="1.35" y1="-1.05" x2="1.35" y2="1.05" width="0.1" layer="21"/>
<wire x1="1.35" y1="1.05" x2="-1.35" y2="1.05" width="0.1" layer="21"/>
<text x="-1.25" y="1.25" size="0.6096" layer="21">&gt;Name</text>
</package>
<package name="WLCSP-6">
<smd name="C1" x="-0.5" y="-0.5" dx="0.32" dy="0.32" layer="1" roundness="100"/>
<smd name="C2" x="0.5" y="-0.5" dx="0.32" dy="0.32" layer="1" roundness="100"/>
<smd name="B2" x="0.5" y="0" dx="0.32" dy="0.32" layer="1" roundness="100"/>
<smd name="B1" x="-0.5" y="0" dx="0.32" dy="0.32" layer="1" roundness="100"/>
<smd name="A1" x="-0.5" y="0.5" dx="0.32" dy="0.32" layer="1" roundness="100"/>
<smd name="A2" x="0.5" y="0.5" dx="0.32" dy="0.32" layer="1" roundness="100"/>
<wire x1="-0.85" y1="-0.85" x2="-0.85" y2="0.85" width="0.05" layer="21"/>
<wire x1="-0.85" y1="0.85" x2="0.85" y2="0.85" width="0.05" layer="21"/>
<wire x1="0.85" y1="0.85" x2="0.85" y2="-0.85" width="0.05" layer="21"/>
<wire x1="0.85" y1="-0.85" x2="-0.85" y2="-0.85" width="0.05" layer="21"/>
<circle x="-1" y="1" radius="0.1" width="0.15" layer="21"/>
<text x="-0.55" y="1.05" size="0.35" layer="25">&gt;NAME</text>
</package>
<package name="LGA10R50P2X3_200X200X80">
<wire x1="-0.9" y1="1" x2="-1" y2="1" width="0.127" layer="51"/>
<wire x1="-1" y1="1" x2="-1" y2="0.6" width="0.127" layer="51"/>
<wire x1="0.9" y1="-1" x2="1" y2="-1" width="0.127" layer="51"/>
<wire x1="1" y1="-1" x2="1" y2="-0.6" width="0.127" layer="51"/>
<wire x1="1" y1="1" x2="1" y2="0.6" width="0.127" layer="51"/>
<wire x1="-0.9" y1="-1" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.6" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="1" y1="1" x2="0.9" y2="1" width="0.127" layer="51"/>
<wire x1="-1.3" y1="1.3" x2="1.3" y2="1.3" width="0.05" layer="39"/>
<wire x1="1.3" y1="1.3" x2="1.3" y2="-1.3" width="0.05" layer="39"/>
<wire x1="1.3" y1="-1.3" x2="-1.3" y2="-1.3" width="0.05" layer="39"/>
<wire x1="-1.3" y1="-1.3" x2="-1.3" y2="1.3" width="0.05" layer="39"/>
<text x="-0.95256875" y="1.50335" size="0.8148875" layer="25">&gt;NAME</text>
<text x="-1.102790625" y="-2.454509375" size="0.81454375" layer="27">&gt;VALUE</text>
<circle x="-1.6" y="1.2" radius="0.1" width="0.2" layer="21"/>
<smd name="3" x="-0.5" y="-0.8" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="0" y="-0.8" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="0.5" y="-0.8" dx="0.4" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="0.8" y="0.25" dx="0.4" dy="0.3" layer="1" rot="R180"/>
<smd name="6" x="0.8" y="-0.25" dx="0.4" dy="0.3" layer="1" rot="R180"/>
<smd name="8" x="0.5" y="0.8" dx="0.4" dy="0.3" layer="1" rot="R270"/>
<smd name="9" x="0" y="0.8" dx="0.4" dy="0.3" layer="1" rot="R270"/>
<smd name="10" x="-0.5" y="0.8" dx="0.4" dy="0.3" layer="1" rot="R270"/>
<smd name="2" x="-0.8" y="-0.25" dx="0.4" dy="0.3" layer="1"/>
<smd name="1" x="-0.8" y="0.25" dx="0.4" dy="0.3" layer="1"/>
<wire x1="-1.3" y1="1.3" x2="-1.3" y2="-1.3" width="0.1" layer="21"/>
<wire x1="-1.3" y1="-1.3" x2="1.3" y2="-1.3" width="0.1" layer="21"/>
<wire x1="1.3" y1="-1.3" x2="1.3" y2="1.3" width="0.1" layer="21"/>
<wire x1="1.3" y1="1.3" x2="-1.3" y2="1.3" width="0.1" layer="21"/>
</package>
<package name="QFN50P200X200X100-12N">
<description>Bosch BMC156 6-axis eCompass</description>
<wire x1="-1" y1="1" x2="1" y2="1" width="0.127" layer="51"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.127" layer="51"/>
<wire x1="1" y1="-1" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="-1" y1="-1" x2="-1" y2="1" width="0.127" layer="51"/>
<circle x="-1.4" y="1.4" radius="0.1" width="0.3" layer="21"/>
<wire x1="-1.75" y1="1.75" x2="1.75" y2="1.75" width="0.05" layer="39"/>
<wire x1="1.75" y1="1.75" x2="1.75" y2="-1.75" width="0.05" layer="39"/>
<wire x1="1.75" y1="-1.75" x2="-1.75" y2="-1.75" width="0.05" layer="39"/>
<wire x1="-1.75" y1="-1.75" x2="-1.75" y2="1.75" width="0.05" layer="39"/>
<text x="-1.9912" y="1.880559375" size="0.8991375" layer="25">&gt;NAME</text>
<text x="-1.9777" y="-2.85656875" size="0.893028125" layer="27">&gt;VALUE</text>
<smd name="1" x="-0.95" y="0.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="2" x="-0.95" y="0.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="3" x="-0.95" y="-0.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="4" x="-0.95" y="-0.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="5" x="-0.25" y="-0.95" dx="0.7" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="0.25" y="-0.95" dx="0.7" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="0.95" y="-0.75" dx="0.7" dy="0.3" layer="1" rot="R180"/>
<smd name="8" x="0.95" y="-0.25" dx="0.7" dy="0.3" layer="1" rot="R180"/>
<smd name="9" x="0.95" y="0.25" dx="0.7" dy="0.3" layer="1" rot="R180"/>
<smd name="10" x="0.95" y="0.75" dx="0.7" dy="0.3" layer="1" rot="R180"/>
<smd name="11" x="0.25" y="0.95" dx="0.7" dy="0.3" layer="1" rot="R270"/>
<smd name="12" x="-0.25" y="0.95" dx="0.7" dy="0.3" layer="1" rot="R270"/>
<wire x1="-1.75" y1="1.75" x2="1.75" y2="1.75" width="0.1" layer="21"/>
<wire x1="1.75" y1="1.75" x2="1.75" y2="-1.75" width="0.1" layer="21"/>
<wire x1="1.75" y1="-1.75" x2="-1.75" y2="-1.75" width="0.1" layer="21"/>
<wire x1="-1.75" y1="-1.75" x2="-1.75" y2="1.75" width="0.1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LDK320">
<wire x1="0" y1="0" x2="0" y2="12.7" width="0.254" layer="94"/>
<wire x1="0" y1="12.7" x2="12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="IN" x="-5.08" y="10.16" length="middle" direction="pwr"/>
<pin name="GND" x="-5.08" y="2.54" length="middle" direction="pwr"/>
<pin name="OUT" x="17.78" y="10.16" length="middle" direction="pwr" rot="R180"/>
<text x="0" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="USB-B-MICRO">
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="95">&gt;VALUE</text>
<pin name="+5V" x="-7.62" y="5.08" visible="pin" length="middle" function="dot"/>
<pin name="D-" x="-7.62" y="2.54" visible="pin" length="middle" function="dot"/>
<pin name="D+" x="-7.62" y="0" visible="pin" length="middle" function="dot"/>
<pin name="ID" x="-7.62" y="-2.54" visible="pin" length="middle" function="dot"/>
<pin name="GND" x="-7.62" y="-5.08" visible="pin" length="middle" function="dot"/>
</symbol>
<symbol name="BATTERY">
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.524" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="-" x="5.08" y="0" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="+" x="-5.08" y="0" visible="off" length="short" direction="pwr"/>
</symbol>
<symbol name="SCHOTTKY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="LEDX2">
<description>Red/Green led</description>
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="4" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="3" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
<wire x1="-6.35" y1="0.05" x2="-7.62" y2="-2.49" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.49" x2="-8.89" y2="0.05" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-2.49" x2="-7.62" y2="-2.49" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.49" x2="-8.89" y2="-2.49" width="0.254" layer="94"/>
<wire x1="-6.35" y1="0.05" x2="-7.62" y2="0.05" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0.05" x2="-8.89" y2="0.05" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0.05" x2="-7.62" y2="-2.49" width="0.1524" layer="94"/>
<wire x1="-9.652" y1="-0.712" x2="-11.049" y2="-2.109" width="0.1524" layer="94"/>
<wire x1="-9.525" y1="-1.855" x2="-10.922" y2="-3.252" width="0.1524" layer="94"/>
<pin name="2" x="-7.62" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="1" x="-7.62" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-11.049" y="-2.109"/>
<vertex x="-10.668" y="-1.22"/>
<vertex x="-10.16" y="-1.728"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-10.922" y="-3.252"/>
<vertex x="-10.541" y="-2.363"/>
<vertex x="-10.033" y="-2.871"/>
</polygon>
</symbol>
<symbol name="LTR303">
<description>LTR303ALS-01 from LiteOn - visible light sensor</description>
<pin name="VDD" x="-15.24" y="5.08" length="middle" direction="pwr"/>
<pin name="NC" x="-15.24" y="0" length="middle" direction="nc"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle" direction="pwr"/>
<pin name="SCL" x="10.16" y="-5.08" length="middle" direction="in" rot="R180"/>
<pin name="INT" x="10.16" y="0" length="middle" direction="out" rot="R180"/>
<pin name="SDA" x="10.16" y="5.08" length="middle" rot="R180"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="-7.32" y="10.91" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.42" y="8.27" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="HDC2010">
<description>HDC2010 humidity sensor</description>
<pin name="VDD" x="-15.24" y="5.08" length="middle" direction="pwr"/>
<pin name="ADDR" x="-15.24" y="0" length="middle" direction="in"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle" direction="pwr"/>
<pin name="INT" x="10.16" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="SCL" x="10.16" y="0" length="middle" direction="in" rot="R180"/>
<pin name="SDA" x="10.16" y="5.08" length="middle" rot="R180"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="-7.32" y="10.91" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.42" y="8.27" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LPS22HB">
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="10.16" x2="15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-10.16" x2="-15.24" y2="10.16" width="0.254" layer="94"/>
<text x="-15.2585" y="10.681" size="2.543090625" layer="95">&gt;NAME</text>
<text x="-15.2736" y="-13.2371" size="2.545590625" layer="96">&gt;VALUE</text>
<pin name="VDD_IO" x="20.32" y="5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="SCL/SPC" x="-20.32" y="2.54" length="middle" direction="in" function="clk"/>
<pin name="RES" x="20.32" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="SDA/SDI/SDO" x="-20.32" y="0" length="middle"/>
<pin name="SDO/SA0" x="-20.32" y="-2.54" length="middle"/>
<pin name="CS" x="-20.32" y="7.62" length="middle" direction="in"/>
<pin name="INT_DRDY" x="20.32" y="0" length="middle" direction="out" rot="R180"/>
<pin name="GND" x="20.32" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="VDD" x="20.32" y="7.62" length="middle" direction="pwr" rot="R180"/>
</symbol>
<symbol name="BMC156">
<wire x1="-17.78" y1="17.78" x2="17.78" y2="17.78" width="0.41" layer="94"/>
<wire x1="17.78" y1="17.78" x2="17.78" y2="-17.78" width="0.41" layer="94"/>
<wire x1="17.78" y1="-17.78" x2="-17.78" y2="-17.78" width="0.41" layer="94"/>
<wire x1="-17.78" y1="-17.78" x2="-17.78" y2="17.78" width="0.41" layer="94"/>
<text x="-17.8003" y="18.8015" size="2.08518125" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-17.8448" y="-21.8593" size="2.09038125" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="CSB" x="-22.86" y="7.62" length="middle" direction="in"/>
<pin name="PS" x="-22.86" y="5.08" length="middle" direction="in"/>
<pin name="SCK" x="-22.86" y="0" length="middle" direction="in" function="clk"/>
<pin name="SDI" x="-22.86" y="-5.08" length="middle"/>
<pin name="VDD" x="22.86" y="15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDIO" x="22.86" y="12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="DRDY" x="22.86" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="INT2" x="22.86" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="SDO" x="-22.86" y="-10.16" length="middle" direction="out"/>
<pin name="GND" x="22.86" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="GND-1" x="22.86" y="-12.7" length="middle" direction="pwr" rot="R180"/>
<pin name="GND-2" x="22.86" y="-15.24" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LDK320">
<gates>
<gate name="G$1" symbol="LDK320" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT89">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="2 2@1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRO_USB_B_HIROSE_ZX62-B-5PA(33)" prefix="J">
<description>Connector Receptacle USB - micro B 2.0 5 Position Surface Mount, Right Angle, Horizontal &lt;br/&gt;&lt;br/&gt;
Bottom Mount</description>
<gates>
<gate name="G$1" symbol="USB-B-MICRO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USB_MICRO_B-HIROSE-ZX62-B-5PA(33)">
<connects>
<connect gate="G$1" pin="+5V" pad="1"/>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="H11634CT-ND" constant="no"/>
<attribute name="MOUSER" value="798-ZX62-B-5PA11" constant="no"/>
<attribute name="NEWARK" value="86P8793" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LINX-BAT-HLD-001" prefix="BAT" uservalue="yes">
<description>&lt;b&gt;Battery Holders&lt;/b&gt;
Linxtechnologies CR2032 battery holder: http://www.linxtechnologies.com/resources/diagrams/bat-hld-001.pdf</description>
<gates>
<gate name="G$1" symbol="BATTERY" x="0" y="0"/>
</gates>
<devices>
<device name="20SMD" package="BATTCON_20MM">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BAT54" prefix="D">
<description>&lt;b&gt;Schottky Diodes&lt;/b&gt;&lt;p&gt;
Source: Fairchild .. BAT54.pdf</description>
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="BAT54GW" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="APGB1612ESCG">
<description>Kingbright red/green led</description>
<gates>
<gate name="G$1" symbol="LEDX2" x="2.54" y="2.54"/>
</gates>
<devices>
<device name="" package="APTB1612">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
<connect gate="G$1" pin="4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTR-303ALS-01" prefix="U">
<gates>
<gate name="U$1" symbol="LTR303" x="2.05" y="-0.2"/>
</gates>
<devices>
<device name="" package="CHIPLED6">
<connects>
<connect gate="U$1" pin="GND" pad="3"/>
<connect gate="U$1" pin="INT" pad="5"/>
<connect gate="U$1" pin="NC" pad="2"/>
<connect gate="U$1" pin="SCL" pad="4"/>
<connect gate="U$1" pin="SDA" pad="6"/>
<connect gate="U$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HDC2010" prefix="U">
<description>HDC2010 Humidity and temperature sensor from TI</description>
<gates>
<gate name="G$1" symbol="HDC2010" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="WLCSP-6">
<connects>
<connect gate="G$1" pin="ADDR" pad="B1"/>
<connect gate="G$1" pin="GND" pad="C1"/>
<connect gate="G$1" pin="INT" pad="C2"/>
<connect gate="G$1" pin="SCL" pad="B2"/>
<connect gate="G$1" pin="SDA" pad="A2"/>
<connect gate="G$1" pin="VDD" pad="A1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LPS22HB" prefix="U">
<description>MEMS nano pressure sensor: 260-1260 hPa absolute digital output barometer</description>
<gates>
<gate name="G$1" symbol="LPS22HB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LGA10R50P2X3_200X200X80">
<connects>
<connect gate="G$1" pin="CS" pad="6"/>
<connect gate="G$1" pin="GND" pad="8 9"/>
<connect gate="G$1" pin="INT_DRDY" pad="7"/>
<connect gate="G$1" pin="RES" pad="3"/>
<connect gate="G$1" pin="SCL/SPC" pad="2"/>
<connect gate="G$1" pin="SDA/SDI/SDO" pad="4"/>
<connect gate="G$1" pin="SDO/SA0" pad="5"/>
<connect gate="G$1" pin="VDD" pad="10"/>
<connect gate="G$1" pin="VDD_IO" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" MEMS nano pressure sensor: 260-1260 hPa absolute digital output barometer "/>
<attribute name="MF" value="STMicroelectronics"/>
<attribute name="MP" value="LPS22HB"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BMC156" prefix="U">
<description>None</description>
<gates>
<gate name="G$1" symbol="BMC156" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN50P200X200X100-12N">
<connects>
<connect gate="G$1" pin="CSB" pad="10"/>
<connect gate="G$1" pin="DRDY" pad="5"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="GND-1" pad="8"/>
<connect gate="G$1" pin="GND-2" pad="9"/>
<connect gate="G$1" pin="INT2" pad="6"/>
<connect gate="G$1" pin="PS" pad="11"/>
<connect gate="G$1" pin="SCK" pad="12"/>
<connect gate="G$1" pin="SDI" pad="2"/>
<connect gate="G$1" pin="SDO" pad="1"/>
<connect gate="G$1" pin="VDD" pad="7"/>
<connect gate="G$1" pin="VDDIO" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Good"/>
<attribute name="DESCRIPTION" value=" BMC156 6-Axis eCompass w/Geomagnetic and 12 Bit Accelerometer Sensor - LGA-12 "/>
<attribute name="MF" value="Bosch"/>
<attribute name="MP" value="BMC156"/>
<attribute name="PACKAGE" value="LGA-12 Bosch"/>
<attribute name="PRICE" value="2.35 USD"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rc-master-smd">
<description>&lt;b&gt;R/C MASTER-SMD! - v1.01 (07/03/2007)&lt;/b&gt;&lt;p&gt;
&lt;p&gt;This library is a collection of SMD ONLY resistors and capacitors by various manufacturers. The pad sizes, spacing and silkscreen widths have been tweaked for use in dense fine pitch layouts where space, alignment and precision are critical. In general these components are designed for routing in grid increments of 5 mils&lt;/p&gt;
&lt;p&gt;* Silkscreen line elements are a minimum of 8 mils in width. All components have text sizes of 0.032"  or 0.04".&lt;/p&gt;
&lt;p&gt;* A silkscreen text values use a ratio of 18 in all cases.&lt;/p&gt;
&lt;p&gt;* ALL PADS AND PART OUTLINES ARE SNAPPED TO A 5 MIL GRID!&lt;/p&gt;
&lt;p&gt;&lt;h4&gt;All components are prefixed using the following conventions:&lt;/h4&gt;&lt;/p&gt;
&lt;table width="380" border="1" bordercolor="#000000"&gt;
  &lt;tr&gt; 
    &lt;td width="81" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Prefix&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
    &lt;td width="289" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Description&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CBP_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Bipolar Electrolytic Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CCA_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Chip Cap Array Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CP_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Polarized Electrolytic/Tantalum Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;C_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Non-Polarized Film / Chip Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;FB_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Simple Ferrite Bead Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;L_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Simple Chip Inductors&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;R_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Resistor Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;
&lt;p&gt;&lt;author&gt;THIS LIBRARY IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED.&lt;br&gt;Copyright (C) 2007, Bob Starr&lt;br&gt; http://www.bobstarr.net&lt;br&gt;
&lt;/author&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 0402</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="-1.1555" y1="0.483" x2="1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="0.483" x2="1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="-0.483" x2="-1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.1555" y1="-0.483" x2="-1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.016" y1="0.508" x2="1.016" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="-1.016" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="0.508" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<text x="-1.016" y="0.762" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.016" y="-1.27" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.254" y2="0.25" layer="51"/>
<rectangle x1="0.2588" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<rectangle x1="-0.1" y1="-0.2" x2="0.1" y2="0.2" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 0603</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="-1.473" y1="0.6655" x2="1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.6655" x2="1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.6655" x2="-1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.6655" x2="-1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1.524" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.524" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.25" x2="0.1999" y2="0.25" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 0805</description>
<wire x1="-0.51" y1="0.535" x2="0.51" y2="0.535" width="0.1016" layer="51"/>
<wire x1="-0.51" y1="-0.535" x2="0.51" y2="-0.535" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.8243" x2="1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="0.8243" x2="1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="-0.8243" x2="-1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.8243" x2="-1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8542" y1="0.889" x2="1.8542" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="0.889" x2="1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="-0.889" x2="-1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-1.8542" y1="-0.889" x2="-1.8542" y2="0.889" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-1.778" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1005">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 1005</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1016" layer="51"/>
<wire x1="-0.9967" y1="0.483" x2="0.9968" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0.9968" y1="0.483" x2="0.9968" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="0.9968" y1="-0.483" x2="-0.9967" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-0.9967" y1="-0.483" x2="-0.9967" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.27" y1="0.762" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.762" width="0.2032" layer="21"/>
<smd name="1" x="-0.635" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.635" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-1.27" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.524" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 1206</description>
<wire x1="1.0525" y1="-0.7128" x2="-1.0652" y2="-0.7128" width="0.1016" layer="51"/>
<wire x1="1.0525" y1="0.7128" x2="-1.0652" y2="0.7128" width="0.1016" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.4731" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="0.983" x2="2.4731" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.4892" y1="1.143" x2="2.4892" y2="1.143" width="0.2032" layer="21"/>
<wire x1="2.4892" y1="1.143" x2="2.4892" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="2.4892" y1="-1.143" x2="-2.4892" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="-2.4892" y1="-1.143" x2="-2.4892" y2="1.143" width="0.2032" layer="21"/>
<smd name="2" x="1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="1" x="-1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-2.286" y="1.524" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.032" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-0.9" y2="0.8" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<rectangle x1="0.9001" y1="-0.8" x2="1.6" y2="0.8" layer="51" rot="R180"/>
</package>
<package name="R1210">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 1210</description>
<wire x1="-2.6317" y1="1.483" x2="2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.483" x2="2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.483" x2="-2.6317" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6317" y1="-1.483" x2="-2.6317" y2="1.483" width="0.0508" layer="39"/>
<wire x1="1.0525" y1="-1.1128" x2="-1.0652" y2="-1.1128" width="0.1016" layer="51"/>
<wire x1="1.0525" y1="1.1128" x2="-1.0652" y2="1.1128" width="0.1016" layer="51"/>
<wire x1="-2.413" y1="1.524" x2="2.413" y2="1.524" width="0.2032" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="2.413" y1="-1.524" x2="-2.413" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-2.413" y1="-1.524" x2="-2.413" y2="1.524" width="0.2032" layer="21"/>
<smd name="2" x="1.524" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="1" x="-1.524" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.286" y="1.778" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.286" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
<rectangle x1="-1.6" y1="-1.2" x2="-0.9" y2="1.2" layer="51"/>
<rectangle x1="0.9001" y1="-1.2" x2="1.6" y2="1.2" layer="51" rot="R180"/>
</package>
<package name="R2010">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 2010</description>
<wire x1="-3.4731" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.4731" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.4731" y1="-1.483" x2="-3.4731" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.662" y1="1.118" x2="1.662" y2="1.118" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.118" x2="1.687" y2="-1.118" width="0.2032" layer="51"/>
<wire x1="-3.6322" y1="1.651" x2="3.6322" y2="1.651" width="0.2032" layer="21"/>
<wire x1="3.6322" y1="1.651" x2="3.6322" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="3.6322" y1="-1.651" x2="-3.6322" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.6322" y1="-1.651" x2="-3.6322" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-2.413" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.413" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.556" y="2.032" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.556" y="-2.54" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 2012</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.9831" x2="1.8142" y2="0.9831" width="0.0508" layer="39"/>
<wire x1="1.8142" y1="0.9831" x2="1.8142" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.8142" y1="-0.983" x2="-1.8143" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.983" x2="-1.8143" y2="0.9831" width="0.0508" layer="39"/>
<wire x1="-1.9304" y1="1.016" x2="1.9304" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.9304" y1="1.016" x2="1.9304" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.9304" y1="-1.016" x2="-1.9304" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-1.9304" y1="-1.016" x2="-1.9304" y2="1.016" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-1.778" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 2512</description>
<wire x1="-3.973" y1="1.8243" x2="3.973" y2="1.8243" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.8243" x2="3.973" y2="-1.8242" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.8242" x2="-3.973" y2="-1.8242" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.8242" x2="-3.973" y2="1.8243" width="0.0508" layer="39"/>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-4.2672" y1="1.905" x2="4.2672" y2="1.905" width="0.2032" layer="21"/>
<wire x1="4.2672" y1="1.905" x2="4.2672" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="4.2672" y1="-1.905" x2="-4.2672" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-4.2672" y1="-1.905" x2="-4.2672" y2="1.905" width="0.2032" layer="21"/>
<smd name="1" x="-3.048" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="3.048" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-4.064" y="2.286" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.064" y="-2.794" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 3216</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1016" layer="51"/>
<wire x1="-2.6318" y1="0.983" x2="2.6318" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="0.983" x2="2.6318" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-0.983" x2="-2.6318" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.6318" y1="-0.983" x2="-2.6318" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.667" y1="1.1684" x2="2.667" y2="1.1684" width="0.2032" layer="21"/>
<wire x1="2.667" y1="1.1684" x2="2.667" y2="-1.1684" width="0.2032" layer="21"/>
<wire x1="2.667" y1="-1.1684" x2="-2.667" y2="-1.1684" width="0.2032" layer="21"/>
<wire x1="-2.667" y1="-1.1684" x2="-2.667" y2="1.1684" width="0.2032" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.5" dy="1.8" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.5" dy="1.8" layer="1"/>
<text x="-2.54" y="1.524" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 3225</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1016" layer="51"/>
<wire x1="-2.6318" y1="1.483" x2="2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.483" x2="2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.483" x2="-2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6318" y1="-1.483" x2="-2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-2.7432" y1="1.651" x2="2.7432" y2="1.651" width="0.2032" layer="21"/>
<wire x1="2.7432" y1="1.651" x2="2.7432" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="2.7432" y1="-1.651" x2="-2.7432" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-2.7432" y1="-1.651" x2="-2.7432" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="2.032" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 5025</description>
<wire x1="-3.3143" y1="1.483" x2="3.3143" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.3143" y1="1.483" x2="3.3143" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.3143" y1="-1.483" x2="-3.3143" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.3143" y1="-1.483" x2="-3.3143" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.662" y1="1.118" x2="1.662" y2="1.118" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.118" x2="1.687" y2="-1.118" width="0.2032" layer="51"/>
<wire x1="-3.6322" y1="1.651" x2="3.6322" y2="1.651" width="0.2032" layer="21"/>
<wire x1="3.6322" y1="1.651" x2="3.6322" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="3.6322" y1="-1.651" x2="-3.6322" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.6322" y1="-1.651" x2="-3.6322" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-2.413" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.413" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.556" y="2.032" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.556" y="-2.54" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 6332</description>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-2.362" y1="1.346" x2="2.387" y2="1.346" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.346" x2="2.387" y2="-1.346" width="0.2032" layer="51"/>
<wire x1="-4.2672" y1="1.905" x2="4.2672" y2="1.905" width="0.2032" layer="21"/>
<wire x1="4.2672" y1="1.905" x2="4.2672" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="4.2672" y1="-1.905" x2="-4.2672" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-4.2672" y1="-1.905" x2="-4.2672" y2="1.905" width="0.2032" layer="21"/>
<smd name="1" x="-3.048" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="3.048" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-4.064" y="2.286" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.064" y="-2.794" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R0201">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 0201</description>
<wire x1="-0.195" y1="0.124" x2="0.195" y2="0.124" width="0.1016" layer="51"/>
<wire x1="0.195" y1="-0.124" x2="-0.195" y2="-0.124" width="0.1016" layer="51"/>
<wire x1="-0.8128" y1="0.4334" x2="0.8128" y2="0.4334" width="0.2032" layer="21"/>
<wire x1="0.8128" y1="0.4334" x2="0.8128" y2="-0.4318" width="0.2032" layer="21"/>
<wire x1="0.8128" y1="-0.4318" x2="-0.8128" y2="-0.4318" width="0.2032" layer="21"/>
<wire x1="-0.8128" y1="-0.4318" x2="-0.8128" y2="0.4334" width="0.2032" layer="21"/>
<smd name="1" x="-0.381" y="0" dx="0.4" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="0.381" y="0" dx="0.4" dy="0.4" layer="1" rot="R90"/>
<text x="-0.762" y="0.762" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.762" y="-1.27" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.304" y1="-0.2" x2="-0.15" y2="0.2" layer="51"/>
<rectangle x1="0.15" y1="-0.2" x2="0.3088" y2="0.2" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-">
<wire x1="-2.54" y1="0" x2="-2.2225" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="-2.2225" y1="0.9525" x2="-1.5875" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="-1.5875" y1="-0.9525" x2="-0.9525" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="-0.9525" y1="0.9525" x2="-0.3175" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="-0.3175" y1="-0.9525" x2="0.3175" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="0.3175" y1="0.9525" x2="0.9525" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="0.9525" y1="-0.9525" x2="1.5875" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="1.5875" y1="0.9525" x2="2.2225" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="2.2225" y1="-0.9525" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-2.54" y="1.5875" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="R-" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SMD-SWITCH">
<packages>
<package name="SW4-SMD-5.2X5.2X1.5MM">
<wire x1="2.6" y1="-1.46863125" x2="1.46863125" y2="-2.6" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2.6" x2="-1.1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-2.6" x2="-1.45981875" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-1.45981875" y1="-2.6" x2="-2.6" y2="-1.45981875" width="0.127" layer="51"/>
<wire x1="-2.6" y1="1.395840625" x2="-1.395840625" y2="2.6" width="0.127" layer="51"/>
<wire x1="-1.4" y1="2.6" x2="1.5" y2="2.6" width="0.127" layer="21"/>
<wire x1="1.433809375" y1="2.6" x2="2.6" y2="1.433809375" width="0.127" layer="51"/>
<rectangle x1="-2.50455" y1="-2.604740625" x2="2.6" y2="2.6" layer="39"/>
<text x="-2.303509375" y="2.80428125" size="0.890359375" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.704840625" y="-0.100284375" size="0.636809375" layer="27" ratio="11">&gt;VALUE</text>
<wire x1="-1.4" y1="2.6" x2="-2.4" y2="1.6" width="0.127" layer="21"/>
<wire x1="1.5" y1="2.6" x2="2.4" y2="1.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-2.6" x2="2.4" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-1.45981875" y1="-2.6" x2="-1.5" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2.6" x2="-2.4" y2="-1.7" width="0.127" layer="21"/>
<smd name="1" x="-3" y="1.85" dx="1" dy="0.7" layer="1"/>
<smd name="2" x="3" y="1.85" dx="1" dy="0.7" layer="1"/>
<smd name="3" x="-3" y="-1.85" dx="1" dy="0.7" layer="1"/>
<smd name="4" x="3" y="-1.85" dx="1" dy="0.7" layer="1"/>
</package>
<package name="LL3301NF">
<rectangle x1="-2.50455" y1="-2.604740625" x2="2.6" y2="2.6" layer="39"/>
<text x="-1.753509375" y="3.60428125" size="0.890359375" layer="25" ratio="11">&gt;NAME</text>
<text x="-1.704840625" y="-0.100284375" size="0.636809375" layer="27" ratio="11">&gt;VALUE</text>
<smd name="1" x="-4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="2" x="4.55" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="3" x="-4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="4" x="4.55" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.1" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.1" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.1" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.1" layer="51"/>
<wire x1="-6" y1="3.45" x2="6" y2="3.45" width="0.1" layer="21"/>
<wire x1="6" y1="3.45" x2="6" y2="-3.45" width="0.1" layer="21"/>
<wire x1="6" y1="-3.45" x2="-6" y2="-3.45" width="0.1" layer="21"/>
<wire x1="-6" y1="-3.45" x2="-6" y2="-3.5" width="0.1" layer="21"/>
<wire x1="-6" y1="-3.45" x2="-6" y2="3.45" width="0.1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="BOTTON-4P">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.090809375" y="5.090809375" size="1.2727" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.0878" y="-6.359759375" size="1.27195" layer="96" ratio="10">&gt;VALUE</text>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<circle x="-1.27" y="-0.762" radius="0.1524" width="0" layer="94"/>
<circle x="0" y="-1.27" radius="0.1524" width="0" layer="94"/>
<pin name="A0" x="-7.62" y="2.54" length="short" direction="pas"/>
<pin name="A1" x="7.62" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="B0" x="-7.62" y="-2.54" length="short" direction="pas"/>
<pin name="B1" x="7.62" y="-2.54" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD-BUTTON-SWITCH" prefix="SW">
<description>311020067</description>
<gates>
<gate name="G$1" symbol="BOTTON-4P" x="0" y="0"/>
</gates>
<devices>
<device name="-SKQGAKE010" package="SW4-SMD-5.2X5.2X1.5MM">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="B0" pad="3"/>
<connect gate="G$1" pin="B1" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Good"/>
<attribute name="DESCRIPTION" value=" Switch Tactile N.O. SPST Button Gull Wing 0.05A 12VDC 3.43N SMD T/R "/>
<attribute name="MF" value="ALPS"/>
<attribute name="MP" value="SKQGAKE010"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="0.27 USD"/>
</technology>
</technologies>
</device>
<device name="" package="LL3301NF">
<connects>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="B0" pad="3"/>
<connect gate="G$1" pin="B1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIAL">
<description>For use by pick and place machines to calibrate the vision/machine, 1mm
&lt;p&gt;By microbuilder.eu&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="sf-bluegiga" deviceset="BGM111" device="-A">
<attribute name="MF" value="Silicon Labs"/>
<attribute name="MPN" value=" BGM111A256V2"/>
</part>
<part name="C2" library="Blue22" deviceset="C_" device="0805" value="4u7">
<attribute name="MF" value="Murata"/>
<attribute name="MPN" value="GRT21BR61H475KE13L"/>
</part>
<part name="C6" library="Blue22" deviceset="C_" device="0805" value="2u2">
<attribute name="MF" value="Murata"/>
<attribute name="MPN" value="GCM21BR71H105KA03L"/>
</part>
<part name="C8" library="Blue22" deviceset="C_" device="0805" value="100nF">
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104K5RACTU"/>
</part>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="TC1" library="TagConnect" deviceset="TC2030-IDC" device="-NL" value="NOPOP"/>
<part name="U3" library="Blue22" deviceset="S25FL164K" device="SOA008" value="S25FL164K0XMFI011">
<attribute name="MF" value="Cypress"/>
<attribute name="MPN" value="S25FL164K0XMFI011"/>
</part>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="C1" library="Blue22" deviceset="C_" device="0805" value="100nF">
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104K5RACTU"/>
</part>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="C9" library="Blue22" deviceset="C_" device="0805" value="100nF">
<attribute name="MF" value="KEMET"/>
<attribute name="MPN" value="C0805C104K5RACTU"/>
</part>
<part name="U5" library="Monitor" deviceset="LDK320" device=""/>
<part name="J1" library="Monitor" deviceset="MICRO_USB_B_HIROSE_ZX62-B-5PA(33)" device="" value="USB-B "/>
<part name="BAT1" library="Monitor" deviceset="LINX-BAT-HLD-001" device="20SMD"/>
<part name="D1" library="Monitor" deviceset="BAT54" device="BAT54GW"/>
<part name="U8" library="Monitor" deviceset="APGB1612ESCG" device=""/>
<part name="R1" library="rc-master-smd" deviceset="R_" device="0603" value="470"/>
<part name="R2" library="rc-master-smd" deviceset="R_" device="0603" value="470"/>
<part name="U2" library="Monitor" deviceset="LTR-303ALS-01" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="R3" library="rc-master-smd" deviceset="R_" device="0603" value="4K7"/>
<part name="R4" library="rc-master-smd" deviceset="R_" device="0603" value="4K7"/>
<part name="U4" library="Monitor" deviceset="HDC2010" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="U6" library="Monitor" deviceset="LPS22HB" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="U7" library="Monitor" deviceset="BMC156" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="SW1" library="SMD-SWITCH" deviceset="SMD-BUTTON-SWITCH" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="U$1" library="adafruit" deviceset="FIDUCIAL" device=""/>
<part name="U$2" library="adafruit" deviceset="FIDUCIAL" device=""/>
<part name="U$3" library="adafruit" deviceset="FIDUCIAL" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="71.12" y="50.8">
<attribute name="MPN" x="71.12" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="71.12" y="50.8" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C2" gate="G$1" x="-22.86" y="-17.78" rot="R90">
<attribute name="MPN" x="-22.86" y="-17.78" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="-22.86" y="-17.78" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="C6" gate="G$1" x="-68.58" y="-10.16" rot="R90">
<attribute name="MPN" x="-68.58" y="-10.16" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MF" x="-68.58" y="-10.16" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="C8" gate="G$1" x="-12.7" y="-17.78" rot="R270">
<attribute name="MPN" x="-12.7" y="-17.78" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MF" x="-12.7" y="-17.78" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="GND4" gate="1" x="40.64" y="81.28"/>
<instance part="TC1" gate="A" x="71.12" y="88.9" smashed="yes" rot="R180">
<attribute name="VALUE" x="74.93" y="96.52" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="74.93" y="83.058" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="U3" gate="G$1" x="71.12" y="-10.16">
<attribute name="MPN" x="71.12" y="-10.16" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="71.12" y="-10.16" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND5" gate="1" x="40.64" y="-27.94"/>
<instance part="C1" gate="G$1" x="27.94" y="-2.54" rot="R90">
<attribute name="MF" x="27.94" y="-2.54" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MPN" x="27.94" y="-2.54" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND2" gate="1" x="-68.58" y="-22.86"/>
<instance part="C9" gate="G$1" x="33.02" y="86.36" rot="R180">
<attribute name="MPN" x="33.02" y="86.36" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MF" x="33.02" y="86.36" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="U5" gate="G$1" x="-55.88" y="-17.78"/>
<instance part="J1" gate="G$1" x="-109.22" y="12.7" smashed="yes" rot="MR0">
<attribute name="NAME" x="-104.14" y="21.59" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-104.14" y="2.54" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="BAT1" gate="G$1" x="-35.56" y="-25.4" rot="R270"/>
<instance part="D1" gate="G$1" x="-35.56" y="-15.24" rot="R90"/>
<instance part="U8" gate="G$1" x="15.24" y="86.36"/>
<instance part="R1" gate="G$1" x="7.62" y="73.66" rot="R90"/>
<instance part="R2" gate="G$1" x="15.24" y="73.66" rot="R90"/>
<instance part="U2" gate="U$1" x="-38.1" y="35.56"/>
<instance part="GND1" gate="1" x="-53.34" y="25.4"/>
<instance part="R3" gate="G$1" x="35.56" y="50.8" rot="R90"/>
<instance part="R4" gate="G$1" x="25.4" y="50.8" rot="R90"/>
<instance part="U4" gate="G$1" x="-38.1" y="58.42"/>
<instance part="GND3" gate="1" x="-58.42" y="48.26"/>
<instance part="U6" gate="G$1" x="-38.1" y="88.9" rot="MR0"/>
<instance part="GND6" gate="1" x="-58.42" y="76.2"/>
<instance part="GND7" gate="1" x="-17.78" y="76.2"/>
<instance part="U7" gate="G$1" x="-43.18" y="129.54" rot="MR0"/>
<instance part="GND8" gate="1" x="-71.12" y="109.22"/>
<instance part="SW1" gate="G$1" x="129.54" y="5.08"/>
<instance part="GND9" gate="1" x="119.38" y="-10.16"/>
<instance part="U$1" gate="G$1" x="35.56" y="132.08"/>
<instance part="U$2" gate="G$1" x="91.44" y="132.08"/>
<instance part="U$3" gate="G$1" x="55.88" y="132.08"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="GND"/>
<wire x1="40.64" y1="-25.4" x2="40.64" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-17.78" x2="55.88" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-17.78" x2="40.64" y2="-17.78" width="0.1524" layer="91"/>
<junction x="40.64" y="-17.78"/>
<wire x1="27.94" y1="-17.78" x2="27.94" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-27.94" x2="-12.7" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="-27.94" x2="-22.86" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-27.94" x2="-22.86" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="-20.32" x2="-12.7" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-12.7" y="-27.94"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="27.94" y1="-5.08" x2="27.94" y2="-17.78" width="0.1524" layer="91"/>
<junction x="27.94" y="-17.78"/>
<pinref part="BAT1" gate="G$1" pin="-"/>
<wire x1="-35.56" y1="-30.48" x2="-35.56" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-35.56" x2="-22.86" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-35.56" x2="-22.86" y2="-27.94" width="0.1524" layer="91"/>
<junction x="-22.86" y="-27.94"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-68.58" y1="-12.7" x2="-68.58" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="-68.58" y1="-15.24" x2="-68.58" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-15.24" x2="-68.58" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-68.58" y="-15.24"/>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="-101.6" y1="7.62" x2="-96.52" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="7.62" x2="-96.52" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-15.24" x2="-68.58" y2="-15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="53.34" y1="71.12" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<pinref part="TC1" gate="A" pin="5"/>
<wire x1="63.5" y1="86.36" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<wire x1="50.8" y1="86.36" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<wire x1="50.8" y1="86.36" x2="40.64" y2="86.36" width="0.1524" layer="91"/>
<junction x="50.8" y="86.36"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="40.64" y1="86.36" x2="40.64" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="35.56" y1="86.36" x2="40.64" y2="86.36" width="0.1524" layer="91"/>
<junction x="40.64" y="86.36"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="U2" gate="U$1" pin="GND"/>
<wire x1="-53.34" y1="27.94" x2="-53.34" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="-58.42" y1="50.8" x2="-58.42" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="-58.42" y1="53.34" x2="-53.34" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="53.34" x2="-58.42" y2="58.42" width="0.1524" layer="91"/>
<junction x="-58.42" y="53.34"/>
<pinref part="U4" gate="G$1" pin="ADDR"/>
<wire x1="-58.42" y1="58.42" x2="-53.34" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="U6" gate="G$1" pin="GND"/>
<wire x1="-58.42" y1="78.74" x2="-58.42" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="RES"/>
<wire x1="-58.42" y1="83.82" x2="-58.42" y2="81.28" width="0.1524" layer="91"/>
<junction x="-58.42" y="81.28"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="U6" gate="G$1" pin="SDO/SA0"/>
<wire x1="-17.78" y1="78.74" x2="-17.78" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="-71.12" y1="111.76" x2="-71.12" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="GND-2"/>
<wire x1="-71.12" y1="114.3" x2="-66.04" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="GND-1"/>
<wire x1="-66.04" y1="116.84" x2="-71.12" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="116.84" x2="-71.12" y2="114.3" width="0.1524" layer="91"/>
<junction x="-71.12" y="114.3"/>
<pinref part="U7" gate="G$1" pin="GND"/>
<wire x1="-66.04" y1="119.38" x2="-71.12" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="119.38" x2="-71.12" y2="116.84" width="0.1524" layer="91"/>
<junction x="-71.12" y="116.84"/>
</segment>
<segment>
<pinref part="SW1" gate="G$1" pin="B0"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="121.92" y1="2.54" x2="119.38" y2="2.54" width="0.1524" layer="91"/>
<wire x1="119.38" y1="2.54" x2="119.38" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="B1"/>
<wire x1="119.38" y1="-2.54" x2="119.38" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="137.16" y1="2.54" x2="139.7" y2="2.54" width="0.1524" layer="91"/>
<wire x1="139.7" y1="2.54" x2="139.7" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-2.54" x2="119.38" y2="-2.54" width="0.1524" layer="91"/>
<junction x="119.38" y="-2.54"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="VCC"/>
<wire x1="86.36" y1="-2.54" x2="91.44" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-2.54" x2="91.44" y2="10.16" width="0.1524" layer="91"/>
<wire x1="91.44" y1="10.16" x2="27.94" y2="10.16" width="0.1524" layer="91"/>
<wire x1="27.94" y1="10.16" x2="-5.08" y2="10.16" width="0.1524" layer="91"/>
<junction x="27.94" y="10.16"/>
<wire x1="-12.7" y1="-7.62" x2="-5.08" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-12.7" y="-7.62"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="-15.24" x2="-12.7" y2="-15.24" width="0.1524" layer="91"/>
<junction x="-12.7" y="-15.24"/>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="45.72" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="45.72" y1="73.66" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
<wire x1="27.94" y1="73.66" x2="45.72" y2="73.66" width="0.1524" layer="91"/>
<pinref part="TC1" gate="A" pin="1"/>
<wire x1="63.5" y1="91.44" x2="27.94" y2="91.44" width="0.1524" layer="91"/>
<wire x1="27.94" y1="91.44" x2="27.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="27.94" y1="86.36" x2="27.94" y2="73.66" width="0.1524" layer="91"/>
<wire x1="45.72" y1="68.58" x2="45.72" y2="58.42" width="0.1524" layer="91"/>
<wire x1="45.72" y1="58.42" x2="45.72" y2="25.4" width="0.1524" layer="91"/>
<wire x1="45.72" y1="25.4" x2="27.94" y2="25.4" width="0.1524" layer="91"/>
<junction x="45.72" y="68.58"/>
<wire x1="27.94" y1="25.4" x2="27.94" y2="10.16" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="30.48" y1="86.36" x2="27.94" y2="86.36" width="0.1524" layer="91"/>
<junction x="27.94" y="86.36"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="27.94" y1="0" x2="27.94" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="OUT"/>
<wire x1="-38.1" y1="-7.62" x2="-35.56" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="K"/>
<wire x1="-35.56" y1="-7.62" x2="-12.7" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-35.56" y1="-12.7" x2="-35.56" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-35.56" y="-7.62"/>
<pinref part="U2" gate="U$1" pin="VDD"/>
<wire x1="-53.34" y1="40.64" x2="-63.5" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="40.64" x2="-63.5" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="10.16" x2="-5.08" y2="10.16" width="0.1524" layer="91"/>
<junction x="-5.08" y="10.16"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="35.56" y1="55.88" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<wire x1="35.56" y1="58.42" x2="45.72" y2="58.42" width="0.1524" layer="91"/>
<junction x="45.72" y="58.42"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="25.4" y1="55.88" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
<wire x1="25.4" y1="58.42" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<junction x="35.56" y="58.42"/>
<wire x1="-63.5" y1="40.64" x2="-63.5" y2="63.5" width="0.1524" layer="91"/>
<junction x="-63.5" y="40.64"/>
<pinref part="U4" gate="G$1" pin="VDD"/>
<wire x1="-63.5" y1="63.5" x2="-53.34" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VDD"/>
<wire x1="-58.42" y1="96.52" x2="-63.5" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="96.52" x2="-63.5" y2="93.98" width="0.1524" layer="91"/>
<junction x="-63.5" y="63.5"/>
<pinref part="U6" gate="G$1" pin="VDD_IO"/>
<wire x1="-63.5" y1="93.98" x2="-63.5" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="73.66" x2="-63.5" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="93.98" x2="-63.5" y2="93.98" width="0.1524" layer="91"/>
<junction x="-63.5" y="93.98"/>
<pinref part="U6" gate="G$1" pin="CS"/>
<wire x1="-17.78" y1="96.52" x2="-17.78" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="104.14" x2="-63.5" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="104.14" x2="-63.5" y2="96.52" width="0.1524" layer="91"/>
<junction x="-63.5" y="96.52"/>
<pinref part="U7" gate="G$1" pin="VDDIO"/>
<wire x1="-66.04" y1="142.24" x2="-76.2" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="142.24" x2="-76.2" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="73.66" x2="-63.5" y2="73.66" width="0.1524" layer="91"/>
<junction x="-63.5" y="73.66"/>
<pinref part="U7" gate="G$1" pin="VDD"/>
<wire x1="-66.04" y1="144.78" x2="-76.2" y2="144.78" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="144.78" x2="-76.2" y2="142.24" width="0.1524" layer="91"/>
<junction x="-76.2" y="142.24"/>
<pinref part="U7" gate="G$1" pin="PS"/>
<wire x1="-20.32" y1="134.62" x2="-10.16" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="134.62" x2="-10.16" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="137.16" x2="-10.16" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="157.48" x2="-76.2" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="157.48" x2="-76.2" y2="144.78" width="0.1524" layer="91"/>
<junction x="-76.2" y="144.78"/>
<pinref part="U7" gate="G$1" pin="CSB"/>
<wire x1="-20.32" y1="137.16" x2="-10.16" y2="137.16" width="0.1524" layer="91"/>
<junction x="-10.16" y="137.16"/>
<pinref part="U7" gate="G$1" pin="SDO"/>
<wire x1="-20.32" y1="119.38" x2="-10.16" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="119.38" x2="-10.16" y2="134.62" width="0.1524" layer="91"/>
<junction x="-10.16" y="134.62"/>
<wire x1="27.94" y1="91.44" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
<junction x="27.94" y="91.44"/>
<pinref part="U8" gate="G$1" pin="1"/>
<wire x1="15.24" y1="91.44" x2="7.62" y2="91.44" width="0.1524" layer="91"/>
<wire x1="7.62" y1="88.9" x2="7.62" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="3"/>
<wire x1="15.24" y1="88.9" x2="15.24" y2="91.44" width="0.1524" layer="91"/>
<junction x="15.24" y="91.44"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<pinref part="TC1" gate="A" pin="6"/>
<wire x1="78.74" y1="86.36" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<wire x1="106.68" y1="86.36" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PF2"/>
<wire x1="106.68" y1="60.96" x2="88.9" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="TC1" gate="A" pin="4"/>
<wire x1="78.74" y1="88.9" x2="96.52" y2="88.9" width="0.1524" layer="91"/>
<wire x1="96.52" y1="88.9" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PF0"/>
<wire x1="96.52" y1="66.04" x2="88.9" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="TC1" gate="A" pin="3"/>
<wire x1="63.5" y1="88.9" x2="58.42" y2="88.9" width="0.1524" layer="91"/>
<wire x1="58.42" y1="88.9" x2="58.42" y2="78.74" width="0.1524" layer="91"/>
<wire x1="58.42" y1="78.74" x2="91.44" y2="78.74" width="0.1524" layer="91"/>
<wire x1="91.44" y1="78.74" x2="91.44" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RESET"/>
<wire x1="91.44" y1="71.12" x2="88.9" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="TC1" gate="A" pin="2"/>
<wire x1="78.74" y1="91.44" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<wire x1="101.6" y1="91.44" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PF1"/>
<wire x1="101.6" y1="63.5" x2="88.9" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PC10"/>
<wire x1="88.9" y1="33.02" x2="96.52" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="SCK"/>
<wire x1="96.52" y1="33.02" x2="96.52" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-12.7" x2="86.36" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SI"/>
<wire x1="86.36" y1="-17.78" x2="99.06" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PC9"/>
<wire x1="99.06" y1="-17.78" x2="99.06" y2="35.56" width="0.1524" layer="91"/>
<wire x1="99.06" y1="35.56" x2="88.9" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="CS"/>
<wire x1="55.88" y1="-2.54" x2="50.8" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-2.54" x2="50.8" y2="15.24" width="0.1524" layer="91"/>
<wire x1="50.8" y1="15.24" x2="106.68" y2="15.24" width="0.1524" layer="91"/>
<wire x1="106.68" y1="15.24" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PC7"/>
<wire x1="106.68" y1="40.64" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SO"/>
<wire x1="55.88" y1="-7.62" x2="45.72" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-7.62" x2="45.72" y2="17.78" width="0.1524" layer="91"/>
<wire x1="45.72" y1="17.78" x2="101.6" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PC8"/>
<wire x1="101.6" y1="17.78" x2="101.6" y2="38.1" width="0.1524" layer="91"/>
<wire x1="101.6" y1="38.1" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="WP"/>
<wire x1="55.88" y1="-12.7" x2="43.18" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-12.7" x2="43.18" y2="20.32" width="0.1524" layer="91"/>
<wire x1="43.18" y1="20.32" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<wire x1="104.14" y1="20.32" x2="104.14" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PC6"/>
<wire x1="104.14" y1="43.18" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="HOLD"/>
<pinref part="U1" gate="G$1" pin="PC11"/>
<wire x1="88.9" y1="30.48" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<wire x1="93.98" y1="30.48" x2="93.98" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-7.62" x2="86.36" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="+5V"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-101.6" y1="17.78" x2="-68.58" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="17.78" x2="-68.58" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="IN"/>
<wire x1="-60.96" y1="-7.62" x2="-68.58" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-68.58" y="-7.62"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="+"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="-35.56" y1="-20.32" x2="-35.56" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB13"/>
<wire x1="53.34" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<wire x1="35.56" y1="40.64" x2="2.54" y2="40.64" width="0.1524" layer="91"/>
<wire x1="2.54" y1="40.64" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="U$1" pin="SCL"/>
<wire x1="2.54" y1="30.48" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
<label x="-2.54" y="27.94" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="30.48" x2="-22.86" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="30.48" x2="-27.94" y2="30.48" width="0.1524" layer="91"/>
<wire x1="35.56" y1="45.72" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<junction x="35.56" y="40.64"/>
<pinref part="U4" gate="G$1" pin="SCL"/>
<wire x1="-27.94" y1="58.42" x2="-22.86" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="58.42" x2="-22.86" y2="30.48" width="0.1524" layer="91"/>
<junction x="-22.86" y="30.48"/>
<pinref part="U6" gate="G$1" pin="SCL/SPC"/>
<wire x1="-17.78" y1="91.44" x2="-2.54" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="91.44" x2="-2.54" y2="30.48" width="0.1524" layer="91"/>
<junction x="-2.54" y="30.48"/>
<pinref part="U7" gate="G$1" pin="SCK"/>
<wire x1="-20.32" y1="129.54" x2="-2.54" y2="129.54" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="129.54" x2="-2.54" y2="91.44" width="0.1524" layer="91"/>
<junction x="-2.54" y="91.44"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB11"/>
<wire x1="53.34" y1="43.18" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
<wire x1="25.4" y1="43.18" x2="0" y2="43.18" width="0.1524" layer="91"/>
<wire x1="0" y1="43.18" x2="0" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U2" gate="U$1" pin="SDA"/>
<wire x1="0" y1="40.64" x2="-12.7" y2="40.64" width="0.1524" layer="91"/>
<label x="-10.16" y="38.1" size="1.778" layer="95"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="40.64" x2="-27.94" y2="40.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="45.72" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
<junction x="25.4" y="43.18"/>
<pinref part="U4" gate="G$1" pin="SDA"/>
<wire x1="-27.94" y1="63.5" x2="-12.7" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="63.5" x2="-12.7" y2="40.64" width="0.1524" layer="91"/>
<junction x="-12.7" y="40.64"/>
<pinref part="U6" gate="G$1" pin="SDA/SDI/SDO"/>
<wire x1="-17.78" y1="88.9" x2="-12.7" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="88.9" x2="-12.7" y2="63.5" width="0.1524" layer="91"/>
<junction x="-12.7" y="63.5"/>
<pinref part="U7" gate="G$1" pin="SDI"/>
<wire x1="-20.32" y1="124.46" x2="-12.7" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="124.46" x2="-12.7" y2="88.9" width="0.1524" layer="91"/>
<junction x="-12.7" y="88.9"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="121.92" y1="22.86" x2="139.7" y2="22.86" width="0.1524" layer="91"/>
<wire x1="139.7" y1="22.86" x2="139.7" y2="7.62" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="A1"/>
<wire x1="139.7" y1="7.62" x2="137.16" y2="7.62" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="A0"/>
<wire x1="121.92" y1="7.62" x2="121.92" y2="22.86" width="0.1524" layer="91"/>
<wire x1="121.92" y1="22.86" x2="50.8" y2="22.86" width="0.1524" layer="91"/>
<junction x="121.92" y="22.86"/>
<wire x1="50.8" y1="22.86" x2="50.8" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PD15"/>
<wire x1="50.8" y1="30.48" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD13"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="53.34" y1="35.56" x2="15.24" y2="35.56" width="0.1524" layer="91"/>
<wire x1="15.24" y1="35.56" x2="15.24" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PD14"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="53.34" y1="33.02" x2="7.62" y2="33.02" width="0.1524" layer="91"/>
<wire x1="7.62" y1="33.02" x2="7.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="4"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="15.24" y1="81.28" x2="15.24" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="U8" gate="G$1" pin="2"/>
<wire x1="7.62" y1="78.74" x2="7.62" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USBD-" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PF3"/>
<wire x1="88.9" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<label x="99.06" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="D-"/>
<wire x1="-101.6" y1="15.24" x2="-88.9" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="15.24" x2="-88.9" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="12.7" x2="-83.82" y2="12.7" width="0.1524" layer="91"/>
<label x="-81.28" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="USBD+" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PF4"/>
<wire x1="88.9" y1="55.88" x2="96.52" y2="55.88" width="0.1524" layer="91"/>
<label x="99.06" y="53.34" size="1.778" layer="95"/>
<wire x1="96.52" y1="55.88" x2="96.52" y2="53.34" width="0.1524" layer="91"/>
<wire x1="96.52" y1="53.34" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="D+"/>
<wire x1="-101.6" y1="12.7" x2="-91.44" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="12.7" x2="-91.44" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="5.08" x2="-83.82" y2="5.08" width="0.1524" layer="91"/>
<label x="-81.28" y="5.08" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,86.36,-2.54,U3,VCC,VDD,,,"/>
<approved hash="104,1,-60.96,-7.62,U5,IN,N$1,,,"/>
<approved hash="104,1,-38.1,-7.62,U5,OUT,VDD,,,"/>
<approved hash="104,1,-35.56,-30.48,BAT1,-,GND,,,"/>
<approved hash="104,1,-35.56,-20.32,BAT1,+,N$2,,,"/>
<approved hash="104,1,-58.42,93.98,U6,VDD_IO,VDD,,,"/>
<approved hash="104,1,-58.42,83.82,U6,RES,GND,,,"/>
<approved hash="104,1,-66.04,142.24,U7,VDDIO,VDD,,,"/>
<approved hash="104,1,-66.04,116.84,U7,GND-1,GND,,,"/>
<approved hash="104,1,-66.04,114.3,U7,GND-2,GND,,,"/>
<approved hash="113,1,-33.8413,-25.4,BAT1,,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
