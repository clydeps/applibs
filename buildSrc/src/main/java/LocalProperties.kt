import java.io.File
import java.util.Properties

open class PropertiesFile(private val file: File) {
    val props = Properties().apply {
        file.inputStream().use { load(it) }
    }

    inline operator fun <reified T : Any> get(name: String): T {
        return (props[name] as? T) ?: error("Property $name not found: properties contains these: ${props.values}")
    }

    private fun save(comment: String) {
        file.outputStream().bufferedWriter().use {
            props.store(it, comment)
        }
    }

    operator fun set(name: String, value: Any) {
        props[name] = value.toString()
        save("Updated name")
    }

    fun set(values: Map<String, Any>) {
        values.forEach { (name, value) -> props[name] = value.toString() }
        save("Updated ${values.keys.joinToString { it }}")
    }
}

object LocalProperties : PropertiesFile(File("local.properties"))
