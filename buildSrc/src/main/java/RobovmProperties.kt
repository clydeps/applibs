/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */

import org.gradle.api.Project
import java.io.File
import java.util.Properties

/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */


private val Project.rvmPropsFile get() = File(projectDir, "robovm.properties")
private val Project.rvmProps
    get() = Properties().apply {
        load(rvmPropsFile.inputStream())
    }

fun Project.updateRobovm() {
    val props = rvmProps
    props["app.build"] = buildNumber.toString()
    props["app.version"] = appVersion
    props.store(rvmPropsFile.outputStream(), "Updated version and build")
}
