object Plugins {
    val kotlin = Dependency("org.jetbrains.kotlin", "kotlin-gradle-plugin", "1.7.20")
    val androidBuildTools = Dependency("com.android.tools.build", "gradle", "7.2.0")
    val googleServices = Dependency("com.google.gms", "google-services", "4.3.14")
    val robovmPlugin = Dependency("com.mobidevelop.robovm", "robovm-gradle-plugin", "2.3.18")
    val dokka = Dependency("org.jetbrains.dokka", "dokka-gradle-plugin", "1.7.20")
}
