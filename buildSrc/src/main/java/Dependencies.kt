/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */


open class Dependency(val prefix: String, val artifact: String, val version: String) {

    operator fun invoke(): String = "$prefix:$artifact:$version"

    fun variant(art: String, vers: String = version) = Dependency(prefix, art, vers)

    companion object {
        fun from(path: String): Dependency {
            val parts = path.split(':')
            require(parts.size == 3)
            return Dependency(parts[0], parts[1], parts[2])
        }
    }
}

val KotlinVersion: String = System.getProperty("kotlinVersion")

val PlayPlugin = Dependency("com.github.triplet.play", "", "2.6.1")

val ThreetenAbp = Dependency("com.jakewharton.threetenabp", "threetenabp", "1.2.4")
val ThreetenBp = Dependency("org.threeten", "threetenbp", "1.4.4")
val Javalin = Dependency("io.javalin","javalin","5.2.0")

val TbruyelleRxpermissions2 = Dependency("com.github.tbruyelle", "rxpermissions", "0.12")

val JavaWebSockets = Dependency("org.java-websocket", "Java-WebSocket", "1.5.1")

val Rxjava3 = Dependency("io.reactivex.rxjava3", "rxjava", "3.1.5")
val Rxjava3Android = Rxjava3.variant("rxandroid", "3.0.0")
val Rxjava3Kotlin = Rxjava3.variant("rxkotlin", "3.0.1")

val Gson = Dependency("com.google.code.gson", "gson", "2.8.6")
val Material = Dependency("com.google.android.material", "material", "1.1.0")

val OkHttp = Dependency("com.squareup.okhttp3", "okhttp", "4.10.0")

val AndroidMapboxSdk = Dependency("com.mapbox.mapboxsdk", "mapbox-android-sdk", "9.7.2")
val AndroidMapboxBuildings = Dependency("com.mapbox.mapboxsdk", "mapbox-android-plugin-building-v9", "0.7.0")
val AndroidMapboxAnnotations = Dependency("com.mapbox.mapboxsdk", "mapbox-android-plugin-annotation-v9", "0.9.0")
val AndroidMapboxScaleBar = Dependency("com.mapbox.mapboxsdk", "mapbox-android-plugin-scalebar-v9", "0.5.0")

val SqlDroid = Dependency("org.sqldroid", "sqldroid", "1.0.3")

val CJLog = Dependency("com.control-j.cjlog", "core", "2.4")

val Kotlin = Dependency("org.jetbrains.kotlin", "kotlin-stdlib-jdk8", KotlinVersion)
val KotlinReflect = Kotlin.variant("kotlin-reflect")
val KotlinPlugin = Dependency("org.jetbrains.kotlin", "kotlin-gradle-plugin", KotlinVersion)
val KotlinStdlib = Dependency("org.jetbrains.kotlin", "kotlin-stdlib-common", KotlinVersion)
val KotlinCoroutines = Dependency("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.6.4")

val Jackson = Dependency("com.fasterxml.jackson.core","jackson-databind","2.14.0")

object AndroidApp {
    const val minSdkVersion = 21
    const val targetSdkVersion = 33

}

val MaterialDrawer = Dependency("com.mikepenz", "materialdrawer", "8.1.3")

val AltBeacon = Dependency("org.altbeacon", "android-beacon-library", "2.19.4")

val AndroidBuildTools = Dependency("com.android.tools.build", "gradle", "4.1.1")
val GoogleServices = Dependency("com.google.gms", "google-services", "4.3.2")

//val AndroidXLegacy = Dependency("androidx.legacy", "legacy-support-v4", "1.0.0")
val AndroidXAnnotation = Dependency("androidx.annotation", "annotation", "1.1.0")
val AndroidXMultidex = Dependency("androidx.multidex", "multidex", "2.0.1")
val AndroidXAppcompat = Dependency("androidx.appcompat", "appcompat", "1.1.0")
val AndroidXWork = Dependency("androidx.work", "work-runtime", "2.7.1")
val RecyclerView = Dependency("androidx.recyclerview", "recyclerview", "1.1.0")
val ConstraintLayout = Dependency("androidx.constraintlayout", "constraintlayout", "1.1.3")
val ScaleImageView = Dependency("com.davemorrissey.labs", "subsampling-scale-image-view-androidx", "3.10.0")
val SwipeRefreshLayout = Dependency("androidx.swiperefreshlayout", "swiperefreshlayout", "1.1.0")
val Guava = Dependency("com.google.guava", "guava", "28.2-android")

val EspressoCore = Dependency("androidx.test.espresso", "espresso-core", "3.3.0")
val EspressoWeb = EspressoCore.variant("espresso-web")
val EspressoContrib = EspressoCore.variant("espresso-contrib")
val EspressoIntents = EspressoCore.variant("espresso-intents")
val AndroidxTestCore = Dependency("androidx.test", "core", "1.4.0")
val AndroidxTestCoreKtx = AndroidxTestCore.variant("core-ktx")
val TestRules = AndroidxTestCore.variant("rules")
val TestRunner = AndroidxTestCore.variant("runner")
val AndroidxTestExtJunit = Dependency("androidx.test.ext", "junit", "1.1.2")
val AndroidxTestExtJunitKtx = AndroidxTestExtJunit.variant("junit-ktx")
val AndroidxTestExtTruth = Dependency("androidx.test.ext", "truth", "1.3.0")

object WorkManager : Dependency("androidx.work", "work-runtime", "2.5.0") {
    val kotlin = variant("work-runtime-ktx")
}

val Jsoup = Dependency("org.jsoup", "jsoup", "1.13.1")
val KtorClient = Dependency("io.ktor", "ktor-client-apache", "1.5.1")
val Ktorm = Dependency("org.ktorm", "ktorm-core", "3.4.1")
val KtormSqlite = Ktorm.variant("ktorm-support-sqlite")
val Retrofit = Dependency("com.squareup.retrofit2", "retrofit", "2.10.0-SNAPSHOT")
val RetrofitRxJava3 = Retrofit.variant("adapter-rxjava3")
val RetrofitGson = Retrofit.variant("converter-gson")
val RobopodsMapbox = Dependency("com.mobidevelop.robovm", "robopods-mapbox-ios", "2.2.4-SNAPSHOT")
val XLayout = Dependency("com.control-j.xlayout", "xcore", "1.0.3")
val XLayoutIos = XLayout.variant("xios")
val Robovm = Dependency("com.mobidevelop.robovm", "robovm-rt", "2.3.18")
val RobovmPlugin = Robovm.variant("robovm-gradle-plugin")
val RobovmCocoaTouch = Robovm.variant("robovm-cocoatouch")
val Xerial = Dependency("org.xerial", "sqlite-jdbc", "3.28.0")

val BlessedBluez = Dependency("com.github.clydebarrow.blessed-bluez", "blessed", "master-SNAPSHOT")
//val BlessedBluez = Dependency("com.welie", "blessed", "1.0-library")

val slf4jSimple = Dependency.from("org.slf4j:slf4j-simple:1.6.1")

val Junit = Dependency("junit", "junit", "4.13.2")
val Mockk = Dependency("io.mockk", "mockk", "1.10.0")
val Roboelectric = Dependency("org.robolectric", "robolectric", "4.5.1")
val MockWebserver = Dependency("com.squareup.okhttp3", "mockwebserver", OkHttp.version)
val MockOkHttp = Dependency("com.github.gmazzo", "okhttp-mock", "1.4.1")

val MockServer = Dependency("org.mock-server", "mockserver-netty", "5.11.1")



