/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */

import org.gradle.api.Project
import java.io.File
import java.util.Properties

private val Project.versionFile get() = File(rootDir, "version.properties")
private val Project.versionProps
    get() = Properties().apply {
        load(versionFile.inputStream())
    }

private const val BUILD_NUMBER = "buildNumber"
private const val APP_VERSION = "appVersion"
fun Project.getProp(name: String): String {
    return versionProps[name] as String
}

fun Project.getIntProp(name: String): Int {
    return getProp(name).toInt()
}

fun Project.setProp(name: String, value: String) {
    val props = versionProps
    props[name] = value
    props.store(versionFile.outputStream(), "Updated $name")
}

fun Project.setProp(name: String, value: Int) = setProp(name, value.toString())

var Project.buildNumber: Int
    get() = getIntProp(BUILD_NUMBER)
    set(value) {
        setProp(BUILD_NUMBER, value)
    }

var Project.appVersion: String
    get() = getProp(APP_VERSION)
    set(value) {
        setProp(BUILD_NUMBER, value)
    }

fun Project.incrementBuildNumber() {
    val bn = buildNumber + 1
    buildNumber = bn
    println("version incremented to $bn")
}