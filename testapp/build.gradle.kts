plugins {
    //id(PlayPlugin)
    id("com.android.application")
    kotlin("android")
}

android {
    defaultConfig {
        applicationId = "com.controlj.applibs.testapp"
        versionCode = project.buildNumber
        versionName = project.appVersion
        compileSdk = AndroidApp.targetSdkVersion
        targetSdk = AndroidApp.targetSdkVersion
        minSdk = AndroidApp.minSdkVersion

        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
        multiDexEnabled = true
    }

    signingConfigs {
        create("release") {
            storeFile = file(findProperty("RELEASE_STORE_FILE") as String)
            storePassword = findProperty("RELEASE_STORE_PASSWORD") as String
            keyAlias = findProperty("RELEASE_KEY_ALIAS") as String
            keyPassword = findProperty("RELEASE_KEY_PASSWORD") as String
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            signingConfig = signingConfigs.getByName("release")
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
    packagingOptions {
        exclude("META-INF/DEPENDENCIES")
        exclude("META-INF/LICENSE")
        exclude("META-INF/LICENSE.txt")
        exclude("META-INF/license.txt")
        exclude("META-INF/NOTICE")
        exclude("META-INF/NOTICE.txt")
        exclude("META-INF/notice.txt")
        exclude("META-INF/ASL2.0")
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    sourceSets {
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("test").java.srcDirs("src/test/kotlin")
    }
    testOptions {
        unitTests.isIncludeAndroidResources = true
    }
    ndkVersion = "22.0.7026061"
}

/*
play {
    track = "alpha"
    userFraction = 1.0
    //serviceAccountEmail = "play-publisher@api-7251881088296382880-439931.iam.gserviceaccount.com"
    serviceAccountCredentials = file("publisher-credentials.json")
    resolutionStrategy = "auto"
    outputProcessor { // this: ApkVariantOutput
        versionNameOverride = "$versionNameOverride.$versionCode"
    }
} */

configurations.all() {
}

dependencies {
    implementation(project(":common"))
    implementation(project(":android"))

    listOf(
        ThreetenAbp,
        AndroidXAnnotation,
        AndroidXAppcompat,
        AndroidXMultidex,
        Rxjava3,
        Rxjava3Android,
        Rxjava3Kotlin,
        AndroidMapboxSdk,
        AndroidMapboxBuildings,
        AndroidMapboxAnnotations,
        Material,
        MaterialDrawer,
        ConstraintLayout,
        RecyclerView,
        SqlDroid,
        Kotlin,
        KotlinReflect,
        CJLog
    ).forEach { implementation(it()) }

    listOf(
        Junit,
        Roboelectric,
        Mockk
    ).forEach { testImplementation(it()) }
}

task<Exec>("asciidoc") {
    commandLine("asciidoc", "${rootProject.projectDir}/asciidoc/about.adoc")
}

//apply(mapOf("plugin" to "com.google.gms.google-services"))


//task("assemble") { dependsOn(task("asciidoc")) }

//task("assembleRelease") { finalizedBy( rootProject.incBuild) }

