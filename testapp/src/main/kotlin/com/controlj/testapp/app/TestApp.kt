/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */

package com.controlj.testapp.app

import com.controlj.appframework.BaseApplication
import com.controlj.testapp.R
import com.controlj.framework.AndroidDeviceLocationProvider
import com.controlj.framework.AndroidSoundPlayer
import com.controlj.graphics.AndroidGraphicsFactory
import com.controlj.rx.MainScheduler
import com.controlj.settings.AviationUnitSettings
import com.controlj.settings.LayerSettings
import com.controlj.ui.AndroidNotificationPresenter
import com.controlj.testapp.BuildConfig
import com.controlj.testapp.activity.TestActivity
import com.jakewharton.threetenabp.AndroidThreeTen
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers

class TestApp : BaseApplication() {
    override val identifier: String = "Testapp"
    override val isDebug: Boolean = BuildConfig.DEBUG

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        AndroidGraphicsFactory(this)
        MainScheduler.instance = AndroidSchedulers.mainThread()
        //AndroidBleScanner(this)
        AndroidSoundPlayer
        LayerSettings
        AviationUnitSettings
        // create a notficationpresenter
        AndroidNotificationPresenter(
            this,
            R.drawable.notification,
            TestActivity::class.java)
        AndroidDeviceLocationProvider(this)
    }
}
