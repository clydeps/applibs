/*
 * Copyright (c) 2021.  Control-J Pty Ltd
 * All rights reserved
 */

package com.controlj.testapp.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import com.controlj.framework.ApplicationBusy
import com.controlj.testapp.R
import com.controlj.location.LocationProvider
import com.controlj.location.LocationSource
import com.controlj.logging.CJLog
import com.controlj.settings.Properties
import com.controlj.stratux.Stratux
import com.controlj.ui.DialogItem
import com.controlj.ui.UiAction
import com.controlj.testapp.service.FlarmService
import com.controlj.testapp.view.SettingsFragment
import com.controlj.view.AppPermission
import com.controlj.view.ForegroundActivity
import com.controlj.view.RouteMapFragment
import com.controlj.view.SlideViewLayout
import com.controlj.view.SlideViewPresenter
import com.mapbox.android.telemetry.TelemetryEnabler
import com.mapbox.mapboxsdk.Mapbox
import com.mikepenz.materialdrawer.holder.ImageHolder
import com.mikepenz.materialdrawer.holder.StringHolder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem
import com.mikepenz.materialdrawer.widget.MaterialDrawerSliderView

@ExperimentalUnsignedTypes
class TestActivity : ForegroundActivity() {

    private val slideview: SlideViewLayout by lazy {
        findViewById(R.id.slideview)
    }
    private val busyIndicator: ProgressBar by lazy {
        findViewById(R.id.busyIndicator)
    }

    inner class DrawerMenuItem(val item: DialogItem) : PrimaryDrawerItem() {
        override var name: StringHolder?
            get() = StringHolder(item.text)
            set(_) {}

        override var icon: ImageHolder?
            get() {
                if (item.image.isNotBlank()) {
                    return ImageHolder(getImageId(item.image))
                }
                return null
            }
            set(@Suppress("UNUSED_PARAMETER") value) {}

        override var isEnabled: Boolean
            get() = item.isEnabled
            set(value) {}

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, Properties.getProperty("mapboxToken"))
        TelemetryEnabler.updateTelemetryState(TelemetryEnabler.State.DISABLED)
        setContentView(R.layout.activity)

        supportFragmentManager.beginTransaction().add(
            R.id.fragmentContainer,
            RouteMapFragment.createInstance(),
            "routeMap"
        ).commit()
        val slider = findViewById<MaterialDrawerSliderView>(R.id.slider)
        slider.itemAdapter.add(
            MainMenu.items.map { item ->
                DrawerMenuItem(item)
            })
        slider.onDrawerItemClickListener = { view: View?, iDrawerItem: IDrawerItem<*>, i: Int ->
            (iDrawerItem as? DrawerMenuItem)?.let {
                iDrawerItem.item.action?.invoke(iDrawerItem.item) ?: false
            }
            false
        }
        disposables.add(ApplicationBusy.listener.subscribe { setBusy(it) })
    }

    private val mapShowing: Boolean
        get() = supportFragmentManager.backStackEntryCount == 0

    private fun showFragment(clazz: Class<out Fragment>) {
        CJLog.debug("Showing fragment ${clazz.simpleName}")
        if (!mapShowing && supportFragmentManager.getBackStackEntryAt(0).javaClass == clazz) {
            slideview.invalidate()
            slideview.presenter.state = SlideViewPresenter.State.Normal
            return
        }
        if (mapShowing)
            slideview.presenter.pushState()
        else
            supportFragmentManager.popBackStackImmediate()
        val fragment = clazz.newInstance()
        slideview.presenter.state = SlideViewPresenter.State.Normal
        val transaction = supportFragmentManager.beginTransaction()
            .setCustomAnimations(
                R.animator.slide_in_left,
                R.animator.slide_out_left,
                R.animator.slide_in_left,
                R.animator.slide_out_left
            )
            .add(R.id.fragmentContainer, fragment, fragment.javaClass.name)
        if (mapShowing)
            transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun processAction(action: UiAction) {
        when (action) {
            MainMenu.Action.Map -> {
                if (!mapShowing)
                    supportFragmentManager.popBackStackImmediate()
                slideview.presenter.state = SlideViewPresenter.State.Normal
            }
            MainMenu.Action.Settings -> {
                showFragment(SettingsFragment::class.java)
            }
            MainMenu.Action.Permissions -> {
                AppPermission.requestPermissions(
                    listOf(
                        AppPermission.BLUETOOTH,
                        AppPermission.LOCATION_ALWAYS
                    )
                ).subscribe()
            }
            else -> super.processAction(action)
        }
    }

    private fun setBusy(state: Boolean) {
        if (state) {
            busyIndicator.visibility = View.VISIBLE
            busyIndicator.bringToFront()
        } else
            busyIndicator.visibility = View.GONE
    }

    override fun onStart() {
        super.onStart()
        LocationSource.add(Stratux)
        LocationSource.add(LocationProvider())
        val intent = Intent(this, FlarmService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        } else
            startService(intent)
    }

    override fun onStop() {
        LocationSource.remove(Stratux)
        LocationSource.remove(LocationProvider())
        super.onStop()
    }

    private fun getImageId(name: String): Int {
        val result = resources.getIdentifier(name.lowercase(), "drawable", packageName)
        if (result == 0) error("Did not find resource drawable \"$name\"")
        return result
    }
}
