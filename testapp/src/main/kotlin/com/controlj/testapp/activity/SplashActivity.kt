package com.controlj.testapp.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle


/**
 * Created by clyde on 2/5/17.
 */

class SplashActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(this, TestActivity::class.java)
        intent.putExtra("Startup", true)
        startActivity(intent)
        finish()
    }

}
