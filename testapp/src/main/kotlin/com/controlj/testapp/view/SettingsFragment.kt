package com.controlj.testapp.view

import android.os.Bundle
import com.controlj.settings.SettingsModel
import com.controlj.testapp.settings.TestSettings
import com.controlj.view.DialogDataFragment

class SettingsFragment : DialogDataFragment<SettingsModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = SettingsModel(TestSettings.dialogData)
    }
}
