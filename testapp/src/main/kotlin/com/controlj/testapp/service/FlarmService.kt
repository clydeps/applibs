/*
 * Copyright (c) 2021.  Control-J Pty Ltd
 * All rights reserved
 */

package com.controlj.testapp.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_IMMUTABLE
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.controlj.testapp.R
import com.controlj.testapp.activity.TestActivity
import com.controlj.location.LocationSource
import com.controlj.logging.CJLog.logException
import com.controlj.logging.CJLog.logMsg
import com.controlj.nmea.FlarmGenerator
import com.controlj.nmea.UdpBroadcaster
import com.controlj.rx.observeBy
import com.controlj.stratux.Stratux
import com.controlj.stratux.StratuxTraffic
import com.controlj.traffic.TrafficSource
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

@ExperimentalUnsignedTypes
class FlarmService : Service() {

    companion object {
        const val CHANNEL_ID = "BlueMAX.notification"
        const val NOTIFICATION_ID = 4370
    }

    private val notificationManager by lazy { getSystemService(NOTIFICATION_SERVICE) as NotificationManager }
    private val channelId: String by lazy {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.description = descriptionText
            notificationManager.createNotificationChannel(mChannel)
        }
        CHANNEL_ID
    }

    inner class LocalBinder : Binder() {
        val service: FlarmService = this@FlarmService
    }

    private val binder: IBinder = LocalBinder()

    override fun onBind(intent: Intent?): IBinder {
        logMsg("Service bound")
        return binder
    }

    private fun getNotification(message: String = "running"): Notification {
        val builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.notification)
            .setContentTitle("Testapp is $message")
            .setOnlyAlertOnce(true)
            .setContentIntent(
                Intent(this, TestActivity::class.java).let { notificationIntent ->
                    PendingIntent.getActivity(this, 0, notificationIntent, FLAG_IMMUTABLE)
                })
        //if (withSound)
        //builder.setSound(Uri.parse("android.resource://$packageName/raw/${Priority.ACTION.sound}"))
        return builder.build()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        logMsg("TestApp Service started")
        val broadcaster = UdpBroadcaster(address = "127.0.0.1")
        LocationSource.add(Stratux)
        TrafficSource.add(StratuxTraffic)
        startForeground(NOTIFICATION_ID, getNotification())
        disposable.dispose()
        disposable = FlarmGenerator.observer
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .observeBy(
                {
                    broadcaster.close()
                    logException(it)
                },
                {
                    broadcaster.close()
                }
            ) {
                if (it.isNotBlank()) {
                    //logMsg(it)
                    broadcaster.broadcast(it.toByteArray())
                }
            }
        return START_STICKY
    }

    private var disposable = Disposable.disposed()

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
        LocationSource.remove(Stratux)
        TrafficSource.remove(StratuxTraffic)
        logMsg("TestApp service destroyed")
    }
}
