package com.controlj.testapp.settings

import com.controlj.comms.FeedbackDialog
import com.controlj.settings.ActionSetting
import com.controlj.settings.AviationUnitSettings
import com.controlj.settings.Setting
import com.controlj.traffic.TrafficSettings
import com.controlj.ui.SettingsDialogData
import com.controlj.ui.SpinnerDialogItem
import com.controlj.view.ForegroundServiceProvider

object TestSettings {

    private val debugGroup = Setting.Group("Feedback",
        ActionSetting(
            "Feedback Report", "Send feedback to developer", isButton = true
        ) {
            ForegroundServiceProvider.queue(FeedbackDialog())
        })
    private val hiddenGroup = Setting.Group("Special settings")

    private val settingsGroups =
        listOf(
            debugGroup,
            AviationUnitSettings.group,
            TrafficSettings.group
        )

    val dialogData: SettingsDialogData
        get() {
            return SettingsDialogData("App Settings", settingsGroups.let {
                if (Setting.secretSauce.value)
                    it + hiddenGroup
                else
                    it
            })
        }
}
