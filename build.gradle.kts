/*
 * Copyright (c) 2020.  Control-J Pty Ltd
 * All rights reserved
 */

// Top-level build file where you can add configuration options common to all sub-projects/modules.

import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.control-j.applibs"
version = "2.2.12-SNAPSHOT"

buildscript {
    repositories {
        mavenLocal()
        mavenCentral()
        google()
        maven { setUrl("https://oss.sonatype.org/content/repositories/snapshots") }
    }

    dependencies {
        listOf(
            Plugins.kotlin,
            Plugins.googleServices,
            Plugins.androidBuildTools,
            Plugins.dokka,
            Plugins.robovmPlugin
        ).forEach { classpath(it()) }
    }
}
plugins {
    `maven-publish`
}

allprojects {
    configurations.all {
        resolutionStrategy.force(OkHttp())
    }

    repositories {
        mavenLocal()
        mavenCentral()
        google()
        maven {
            credentials {
                username = project.property("repoUser") as String
                password = project.property("repoPassword") as String
            }
            setUrl("https://mvnrepo-255701.appspot.com")
        }
        maven { setUrl("https://oss.sonatype.org/content/repositories/snapshots") }
        maven { setUrl("https://oss.sonatype.org/content/repositories/comcontrol-j-1006") }
        maven {
            credentials {
                // Do not change the username below.
                // This should always be `mapbox` (not your username).
                username = "mapbox"
                // Use the secret token you stored in gradle.properties as the password
                password = project.property("MAPBOX_DOWNLOADS_TOKEN") as String? ?: ""
            }
            authentication {
                create<BasicAuthentication>("basic")
            }
            setUrl("https://api.mapbox.com/downloads/v2/releases/maven")
        }
        maven { setUrl("https://jitpack.io") }
    }
}

subprojects {
    apply(plugin = "idea")
    apply(plugin = "maven-publish")


    publishing {
        repositories {
            maven {
                setUrl("https://mvnrepo-255701.appspot.com")
                credentials {
                    username = project.property("repoUser") as String
                    password = project.property("repoPassword") as String
                }
            }
        }

    }

    val javaVersion = "1.8"
    tasks {
        withType<KotlinCompile> {
            kotlinOptions {
                jvmTarget = javaVersion
                freeCompilerArgs = listOf(
                    "-opt-in=kotlin.ExperimentalUnsignedTypes",
                    "-opt-in=kotlin.ExperimentalStdlibApi"
                )
            }
        }
        withType<JavaCompile> {
            sourceCompatibility = javaVersion
            targetCompatibility = javaVersion
        }
    }

    tasks.register("localpub") {
        doFirst {
            tasks.findByName("dokkaJar")?.enabled = false
            tasks.findByName("dokkaHtml")?.enabled = false
        }
        finalizedBy("publishToMavenLocal")
    }
}

//--------- Version Increment ----------//


tasks.register("incBuild") {
    doLast {
        incrementBuildNumber()
    }
}

tasks.register("publishToBeta") {
    dependsOn("androidApp:publishReleaseBundle")
    dependsOn("iosApp:publishIPA")
    finalizedBy("incBuild")
}
