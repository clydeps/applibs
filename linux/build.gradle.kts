/*
 * Copyright (c) 2021.  Control-J Pty Ltd
 * All rights reserved
 */

plugins {
    kotlin("jvm")
    id("org.jetbrains.dokka")
    id("com.github.johnrengelman.shadow") version "6.1.0"	
}

val dokkaJar by tasks.creating(Jar::class) {
    description = "Assembles Kotlin docs with Dokka"
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
    dependsOn(tasks.dokkaHtml)
}

configurations.all {
    resolutionStrategy {
        cacheChangingModulesFor(1, TimeUnit.MINUTES)
    }
}


dependencies {
    implementation(project(":common"))
    listOf(
        ThreetenBp,
        Rxjava3,
        Gson,
        Ktorm,
        KtormSqlite,
        Kotlin,
        KotlinReflect,
        Retrofit,
        RetrofitGson,
        RetrofitRxJava3,
        BlessedBluez,
        slf4jSimple,
        CJLog
    ).forEach { implementation(it()) }

}

tasks.register<Exec>("deploy") {
    dependsOn(tasks.shadowJar)
    executable = "scp"
    args("${projectDir}/build/libs/linux-all.jar", "weather:")
}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
            artifact(dokkaJar)
            groupId = rootProject.group as String
            version = rootProject.version as String
            artifactId = "linux"

            pom {
                name.set("Applibs Linux")
                description.set("Linux specific application libraries")
                licenses {
                    license {
                        name.set("Copyright (C) 2021 Control-J Pty Ltd. All rights reserved")
                    }
                }
                developers {
                    developer {
                        id.set("clyde")
                        name.set("Clyde Stubbs")
                    }
                }
                scm {
                    connection.set("scm:git:git@gitlab.com:clydeps/applibs.git")
                }
            }

            versionMapping {
                usage("java-api") {
                    fromResolutionOf("runtimeClasspath")
                }
                usage("java-runtime") {
                    fromResolutionResult()
                }
            }
        }
    }
}

