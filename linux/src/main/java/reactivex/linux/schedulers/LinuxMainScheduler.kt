package reactivex.linux.schedulers

import com.controlj.rx.MainScheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.Executors

/**
 * Linux "main" scheduler.
 */
object LinuxMainScheduler {

    /**
     * Call this function from the main thread on startup.
     * It will initialise the MainScheduler with a scheduler using a dedicated thread
     */
    init {
        MainScheduler.instance = Schedulers.from(Executors.newSingleThreadExecutor())
    }

    val instance get() = MainScheduler.instance

}
