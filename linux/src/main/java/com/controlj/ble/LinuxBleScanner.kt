package com.controlj.ble

import com.controlj.logging.CJLog.logMsg
import com.welie.blessed.BluetoothCentralManager
import com.welie.blessed.BluetoothCentralManagerCallback
import com.welie.blessed.BluetoothCommandStatus
import com.welie.blessed.BluetoothPeripheral
import com.welie.blessed.BluetoothPeripheralCallback
import com.welie.blessed.ScanResult
import reactivex.linux.schedulers.LinuxMainScheduler
import java.util.concurrent.ConcurrentSkipListMap

object LinuxBleScanner : BleScanner(LinuxMainScheduler.instance) {

    private val deviceMap = ConcurrentSkipListMap<String, BleDevice>()

    private val callback = object : BluetoothCentralManagerCallback() {
        override fun onConnectedPeripheral(peripheral: BluetoothPeripheral) {
            deviceMap[peripheral.address.uppercase()]?.onConnected()
        }

        override fun onConnectionFailed(peripheral: BluetoothPeripheral, status: BluetoothCommandStatus) {
            logMsg("Connection failed: $peripheral: $status")
            deviceMap[peripheral.address.uppercase()]?.onDisconnected()
        }

        override fun onDisconnectedPeripheral(peripheral: BluetoothPeripheral, status: BluetoothCommandStatus) {
            logMsg("Device disconnected: $peripheral: $status")
            deviceMap[peripheral.address.uppercase()]?.onDisconnected()
        }

        override fun onDiscoveredPeripheral(peripheral: BluetoothPeripheral, scanResult: ScanResult) {
            onNextScanResult(
                LinuxBleScanResult(
                    getDevice(peripheral.address.uppercase())!!,
                    scanResult.manufacturerData,
                    scanResult.rssi
                )
            )
        }


    }
    private val deviceManager = BluetoothCentralManager(callback)
    override val isBleSupported: Boolean = true

    /**
     * Filter out scan reports with RSSI below the threshold.
     */

    var discoveryRssiThreshold: Int
        get() = deviceManager.discoveryRssiThreshold
        set(value) {
            deviceManager.discoveryRssiThreshold = value
        }

    override fun getConnectedDevices(vararg addresses: String): List<BleDevice> {
        if (addresses.isNotEmpty())
            return deviceManager.connectedPeripherals.filter { it.address in addresses }.map { LinuxBleDevice(it) }
        return deviceManager.connectedPeripherals.map { LinuxBleDevice(it) }
    }

    override fun stopScan() {
        deviceManager.stopScan()
    }

    override fun startScan() {
        deviceManager.scanForPeripherals()
    }

    override fun getDevice(address: String): BleDevice? {
        try {
            return deviceMap.getOrPut(address.uppercase()) {
                LinuxBleDevice(deviceManager.getPeripheral(address.uppercase()))
            }
        } catch (ex: Exception) {
            logMsg(ex.toString())
            return null
        }
    }

    override val currentState: AdapterState
        get() = when (deviceManager.state) {
            BluetoothCentralManager.State.PoweredOff -> AdapterState.PoweredOff
            BluetoothCentralManager.State.Ready -> AdapterState.Ready
            else -> AdapterState.Unsupported

        }

    val BluetoothPeripheral.isConnected: Boolean get() = deviceManager.isPeripheralConnected(address)

    fun BluetoothPeripheral.disconnect() = deviceManager.cancelConnection(this)

    fun connect(linuxBleDevice: LinuxBleDevice, callback: BluetoothPeripheralCallback) {
        deviceManager.connectPeripheral(linuxBleDevice.peripheral, callback)
    }
}
