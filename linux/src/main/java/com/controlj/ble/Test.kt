package com.controlj.ble

import com.controlj.rx.observeBy
import io.reactivex.rxjava3.core.Observable
import java.util.concurrent.TimeUnit
import kotlin.system.exitProcess


object Test {
    @JvmStatic
    fun main(args: Array<String>) {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "debug")
        println("Started")
        LinuxBleScanner.discoveryRssiThreshold = -120
        LinuxBleScanner.adapterState
            .filter { it != BleScanner.AdapterState.PoweredOff && it != BleScanner.AdapterState.PoweringOn }
            .timeout(10, TimeUnit.SECONDS)
            .take(1)
            .flatMap {
                println("Adapter state is $it")
                when (it) {
                    BleScanner.AdapterState.Ready -> LinuxBleScanner.bleScan
                    else -> Observable.error(IllegalStateException("BlueZ not supported"))
                }
            }
            .take(300, TimeUnit.SECONDS)
            .observeBy(
                onComplete = { exitProcess(0) },
                onError = { println(it.toString()); exitProcess(1) }
            ) {
                println("$it: ${it.manufacturers.map { BluetoothData.getCompanyName(it) }}")
            }
    }
}

