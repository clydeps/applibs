package com.controlj.ble

import com.welie.blessed.BluetoothGattCharacteristic


/**
 * Created by clyde on 9/5/17.
 */

class LinuxBleCharacteristic(
    override val device: LinuxBleDevice,
    val characteristic: BluetoothGattCharacteristic
) : BleCharacteristic(device, characteristic.uuid.toString())
