package com.controlj.ble

import com.welie.blessed.BluetoothGattService


/**
 * Created by clyde on 9/5/17.
 */

class LinuxBleService(private val device: LinuxBleDevice, val service: BluetoothGattService) : BleGattService() {

    override val uuid: String = service.uuid.toString()

    override val characteristics: List<BleCharacteristic>
        get() = service.characteristics.map { LinuxBleCharacteristic(device, it) }
}
