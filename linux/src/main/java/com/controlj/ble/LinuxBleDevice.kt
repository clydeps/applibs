package com.controlj.ble

import com.controlj.ble.LinuxBleScanner.disconnect
import com.controlj.ble.LinuxBleScanner.isConnected
import com.controlj.logging.CJLog.logMsg
import com.welie.blessed.BluetoothCommandStatus
import com.welie.blessed.BluetoothGattCharacteristic
import com.welie.blessed.BluetoothGattDescriptor
import com.welie.blessed.BluetoothGattService
import com.welie.blessed.BluetoothPeripheral
import com.welie.blessed.BluetoothPeripheralCallback

/**
 *
 * An object representing a BLE device as provided by the Linux library
 */

class LinuxBleDevice(val peripheral: BluetoothPeripheral) : BleDevice(peripheral.address, peripheral.name) {


    private var serviceList = listOf<LinuxBleService>()
    private var characteristicMap = mapOf<String, LinuxBleCharacteristic>()

    private val callback = object : BluetoothPeripheralCallback() {
        override fun onServicesDiscovered(
            peripheral: BluetoothPeripheral,
            services: MutableList<BluetoothGattService>
        ) {
            serviceList = services.map { LinuxBleService(this@LinuxBleDevice, it) }
            characteristicMap = services.map {
                it.characteristics.map {
                    it.uuid.toString() to LinuxBleCharacteristic(
                        this@LinuxBleDevice,
                        it
                    )
                }
            }.flatten().toMap()
            onServicesDiscovered(serviceList)
        }

        override fun onNotificationStateUpdate(
            peripheral: BluetoothPeripheral,
            characteristic: BluetoothGattCharacteristic,
            status: BluetoothCommandStatus
        ) {
            characteristicMap[characteristic.uuid.toString()]?.let {
                when (status) {
                    BluetoothCommandStatus.COMMAND_SUCCESS -> onNotificationSet(it)
                    else -> onNotificationSetError(status.name)
                }
            }
        }

        override fun onCharacteristicUpdate(
            peripheral: BluetoothPeripheral,
            value: ByteArray,
            characteristic: BluetoothGattCharacteristic,
            status: BluetoothCommandStatus
        ) {
            characteristicMap[characteristic.uuid.toString()]?.let {
                when (status) {
                    BluetoothCommandStatus.COMMAND_SUCCESS -> if (peripheral.isNotifying(characteristic))
                        onNotification(it, value)
                    else
                        onReadCharacteristic(it, value)
                    else -> if (peripheral.isNotifying(characteristic))
                        onNotificationError(status.name)
                    else
                        onReadError(it, status.name)
                }
            }
        }

        override fun onCharacteristicWrite(
            peripheral: BluetoothPeripheral,
            value: ByteArray,
            characteristic: BluetoothGattCharacteristic,
            status: BluetoothCommandStatus
        ) {
            characteristicMap[characteristic.uuid.toString()]?.let {
                when (status) {
                    BluetoothCommandStatus.COMMAND_SUCCESS -> onWriteCharacteristic(it)
                    else -> onWriteError(status.name)
                }
            }
        }

        override fun onDescriptorRead(
            peripheral: BluetoothPeripheral,
            value: ByteArray,
            descriptor: BluetoothGattDescriptor,
            status: BluetoothCommandStatus
        ) {
            super.onDescriptorRead(peripheral, value, descriptor, status)
        }

        override fun onDescriptorWrite(
            peripheral: BluetoothPeripheral,
            value: ByteArray,
            descriptor: BluetoothGattDescriptor,
            status: BluetoothCommandStatus
        ) {
            super.onDescriptorWrite(peripheral, value, descriptor, status)
        }

        override fun onReadRemoteRssi(peripheral: BluetoothPeripheral, rssi: Int, status: BluetoothCommandStatus) {
            when (status) {
                BluetoothCommandStatus.COMMAND_SUCCESS -> onRssiRead(rssi)
                else -> onRssiError(status.name)
            }
        }
    }

    override fun readCharacteristic(characteristic: BleCharacteristic) {
        characteristic as LinuxBleCharacteristic
        try {
            if (!peripheral.readCharacteristic(characteristic.characteristic))
                onReadError(characteristic, "Write error")
        } catch (ex: Exception) {
            onReadError(characteristic, ex.toString())
        }
    }

    override fun writeCharacteristic(characteristic: BleCharacteristic, data: ByteArray) {
        try {
            characteristic as LinuxBleCharacteristic
            if (!peripheral.writeCharacteristic(
                    characteristic.characteristic,
                    data,
                    BluetoothGattCharacteristic.WriteType.WITH_RESPONSE
                )
            )
                onWriteError("Write error")
        } catch (ex: Exception) {
            onWriteError(ex.toString())
        }
    }

    override fun connect(automatic: Boolean) {
        if (!peripheral.isConnected) {
            LinuxBleScanner.connect(this, callback)
        } else
            onConnected()
    }

    override fun disconnect() {
        super.disconnect()
        if (peripheral.isConnected)
            peripheral.disconnect()
        else
            onDisconnected()
    }

    /**
     * There is no specific procedure to request discovery, this happens automatically on connect.
     * If this has already completed then the serviceList will be populated.
     */
    override fun discover() {
        if (serviceList.isNotEmpty())
            onServicesDiscovered(serviceList)
    }

    override fun requestMtu(mtu: Int) {
        onMtuChange(96)    // how to increase this?
    }

    override fun triggerRssiRead() {
        peripheral.readRemoteRssi()
    }

    override fun setPriority(priority: Priority): Boolean {
        return false
    }

    override fun setNotification(characteristic: BleCharacteristic, enable: Boolean) {
        //logMsg("Setting notification %s %s", bleCharacteristic.getUuid(), Boolean.toString(b));
        characteristic as LinuxBleCharacteristic
        if (!peripheral.isConnected) {
            logMsg("$name: Attempt to enable notifications when not in connected state")
            return
        }
        peripheral.setNotify(characteristic.characteristic, enable)
    }

    override fun startReadDescriptors(characteristic: BleCharacteristic) {
        TODO("Not yet implemented")
    }
}
