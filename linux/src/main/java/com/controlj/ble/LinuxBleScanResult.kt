package com.controlj.ble

/**
 * Created by clyde on 9/5/17.
 */

class LinuxBleScanResult(
    protected val device: BleDevice,
    private val manufData: Map<Int, ByteArray>,
    rssi: Int = -200
) : BleScanResult(device.address, device.name, rssi) {

    override fun getManufacturerData(manuf: Int): ByteArray {
        return manufData[manuf] ?: byteArrayOf()
    }

    override val manufacturers: Iterable<Int>
        get() = manufData.keys

}
